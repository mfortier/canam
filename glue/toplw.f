!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TOPLW(SHB,PTOIT,ILEV)
C
C     * APR 26/2006 - M.LAZARE. DEFINES INTEGER VARIABLE "LWTOP" WHICH
C     *                         SEPARATES FULL L/W ASSUMPTIONS (BELOW
C     *                         THIS MODEL LEVEL) FROM SIMPLIFIED ONES
C     *                         ABOVE IT.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL SHB(ILEV)    !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      COMMON /ITOPLW/ LWTOP
C
C     * THRESHOLD IN MBS.
C
      DATA THRESH/0.99/
C============================================================================
C     * PRESSURE IN MBS OF UPPER INTERFACE OF TOP LAYER.
C
C
      PFULL0=0.01*PTOIT
      LWTOP=1
C
C     * IF TOP LAYER BELOW THRESHOLD, XIT WITH LWTOP=1. OTHERWISE, CALCULATE
C     * LAYAER INDEX FOR WHICH THE TOP INTERFACE PRESSURE FIRST EXCEEDS 
C     * "THRESH".
C
      IF(PFULL0.LT.THRESH)                                     THEN
        ISTEP=0
        DO L=1,ILEV
          PFULL=SHB(L)*1000.
          IF(ISTEP.EQ.0 .AND. PFULL.GE.THRESH)          THEN
            LWTOP=L+1
            ISTEP=1        
          ENDIF
        ENDDO
      ENDIF
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

