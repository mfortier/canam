#include "cppdef_config.h"
!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
C     * Mar 16/2019 - J. Cole     - Add  3-hour 2D fields (CF3hr,E3hr)
C     * Nov 29/2018 - J. Cole     - Add/adjust tendencies
C     * Nov 02/2018 - J. Cole     - Add radiative flux profiles
C     * Nov 03/2018 - M.Lazare.   - Added GSNO and FNLA.
C     *                           - Added 3HR save fields.
C     * Oct 01/2018 - S.Kharin.   - Add XTVIROS for sampled burdens.
C     * Aug 23/2018 - Vivek Arora - Remove PFHC and PEXF
C     * Aug 21/2018 - M.Lazare.     Remove {ALSW,ALLW}.
C     * Aug 14/2018 - M.Lazare.     Remove MASKROW,GTROL.
C     * Aug 14/2018 - M.Lazare.     Remove {GTM,SICP,SICM,SICNP,SICM}.
C     * Aug 01/2018 - M.Lazare.     Remove {OBEG,OBWG,RES,GC}.
C     * Jul 30/2018 - M.Lazare.   - Unused FMIROW removed.
C     *                           - Removed non-transient aerosol emission cpp blocks.
C     * Mar 09/2018 - M.Lazare.   - BEG  and BWG  changed from PAK/ROW to
C     *                             PAL/ROL.
C     * Feb 27/2018 - M.Lazare.   - QFSL and BEGL changed from PAK/ROW to
C     *                             PAL/ROL.
C     *                           - Added {BEGK,BWGK,BWGL,QFSO}.
C     * Feb 06/2018 - M.Lazare.    - Add {FTOX,FTOY} and {TDOX,TDOY}. Also PHIEROW.
C     * Nov 01/2017 - J. Cole.     - Add arrays to support CMIP6 stratospheric
C     *                              aerosols.
C     * Aug 10/2017 - M.Lazare.    - Add FNROT.
C     *                            - FLAK->{FLKR,FLKU}.
C     *                            - LICN->GICN.
C     *                            - Add lakes arrays.
C     *                            - WSNOROT dimension changed from
C     *                              to IM to support use over
C     *                              sea/lake ice.
C     * Aug 07/2017 - M.Lazare.    - Initial Git verison.
C     * Mar 26/2015 - M.Lazare.    Final version for gcm18:
C     *                            - Unused {CSTROL,CLTROL} removed.
C     *                            - Add (for NEMO/CICE support):
C     *                              HSEAROL,RAINSROL,SNOWSROL
C     *                              OBEGROL,OBWGROL,BEGOROL,BWGOROL,
C     *                              BEGIROL,HFLIROL,
C     *                              HSEAROL,RAINSROL,SNOWSROL.
C     *                            - Remove: FTILROW.
C     *                            - Add (for harmonization of field
C     *                              capacity and wilting point between
C     *                              CTEM and CLASS): THLWROT.
C     *                            - Add (for soil colour index look-up
C     *                              for land surface albedo):
C     *                              ALGDVROT,ALGDNROT,ALGWVROT,ALGWNROT,
C     *                              SOCIROT.
C     *                            - Add (for fractional land/water/ice):
C     *                              SALBROT,CSALROT,EMISROW,EMISROT,
C     *                              WRKAROL,WRKBROL,SNOROWO.
C     *                            - Add (for PLA):
C     *                              PSVVROW,PDEVROW,PDIVROW,PREVROW,PRIVROW.
C     * Feb 04/2014 - M.Lazare.    Interim version for gcm18:
C     *                            - NTLD used instead of IM for land-only
C     *                              mosaic fields.
C     *                            - FLAKROW,FLNDROW,GTROT,LICNROW added.
C     * Nov 19/2013 - M.Lazare.    Cosmetic: Remove {GFLX,GA,HBL,ILMO,PET,UE,
C     *                                      WTAB,ROFS,ROFB) "ROT" arrays.
C     * Jul 10/2013 - M.Lazare/    Previous version compak11 for gcm17:
C     *               K.Vonsalzen/ - FSF added as prognostic field
C     *               J.Cole/        rather than residual.
C     *                            - Extra diagnostic microphysics
C     *                              fields added:
C     *                              {SEDI,RLIQ,RICE,RLNC,CLIQ,CICE,
C     *                               VSEDI,RELIQ,REICE,CLDLIQ,CLDICE,CTLNC,CLLNC}.
C     *                            - New emission fields:
C     *                              {SBIO,SSHI,OBIO,OSHI,BBIO,BSHI} and
C     *                              required altitude field ALTI.
C     *                            - Many new aerosol diagnostic fields
C     *                              for PAM, including those for cpp options:
C     *                              "pfrc", "xtrapla1" and "xtrapla2".
C     *                            - The following fields were removed
C     *                              from the "aodpth" cpp option:
C     *                              {SAB1,SAB2,SAB3,SAB4,SAB5,SABT} and
C     *                              {SAS1,SAS2,SAS3,SAS4,SAS5,SAST},
C     *                              (both PAK and PAL).
C     *                            - Due to the implementation of the mosaic
C     *                              for CLASS_v3.6, prognostic fields
C     *                              were changed from PAK/ROW to PAT/ROT,
C     *                              with an extra "IM" dimension (for
C     *                              the number of mosaic tiles).
C     *                            - The instantaneous band-mean values for
C     *                              solar fields are now prognostic
C     *                              as well:{FSDB,FSFB,CSDB,CSFB,FSSB,FSSCB}.
C     *                            - Removed "histemi" fields.
C     * NB: The following are intermediate revisions to frozen gcm16 code
C     *     (upwardly compatible) to support the new CLASS version in development:
C     * Oct 19/2011 - M.Lazare.    - Add THR,THM,BI,PSIS,GRKS,THRA,HCPS,TCS,THFC,
C     *                              PSIW,ALGD,ALGW,ZBTW,ISND,IGDR.
C     * Oct 07/2011 - M.Lazare.    - Add GFLX,GA,HBL,PET,ILMO,ROFB,ROFS,UE,WTAB.
C     * Jul 13/2011 - E.Chan.      - For selected variables, add new tile
C     *                              versions (ROT) or convert from ROW to ROT.
C     *                            - Add TPND, ZPND, TAV, QAV, WSNO, TSFS.
C     * MAY 07/2012 - M.LAZARE/    PREVIOUS VERSION COMROW10 FOR GCM16:
C     *               K.VONSALZEN/ - MODIFY FIELDS TO SUPPORT A NEWER
C     *               J.COLE/        VERSION OF COSP WHICH INCLUDES
C     *               Y.PENG.        THE MODIS, MISR AND CERES AND
C     *                              SAVE THE APPROPRIATE OUTPUT.
C     *                            - REMOVE {FSA,FLA,FSTC,FLTC} AND
C     *                              REPLACE BY {FSR,OLR,FSRC,OLRC}.
C     *                            - NEW CONDITIONAL DIAGNOSTIC OUTPUT
C     *                              UNDER CONTROL OF "XTRADUST" AND
C     *                              "AODPTH".
C     *                            - ADDITIONAL MAM RADIATION OUTPUT
C     *                              FIELDS {FSAN,FLAN}.
C     *                            - ADDITIONAL CORRELATED-K OUTPUT
C     *                              FIELDS: "FLG", "FDLC" AND "FLAM".
C     *                            - "FLGROL_R" IS ACTUAL CALCULATED
C     *                              RADIATIVE FORCING TERM INSTEAD
C     *                              OF "FDLROL_R-SIGMA*T**4".
C     *                            - INCLUDE "ISEED" FOR RANDOM NUMBER
C     *                              GENERATOR SEED NOW CALCULATED
C     *                              IN MODEL DRIVER INSTEAD OF PHYSICS.
C     * MAY 02/2010 - M.LAZARE/    PREVIOUS VERSION COMROW9I FOR GCM15I:
C     *               K.VONSALZEN/ - ADD FSOPAL/FSOROL ("FSOL").
C     *               J.COLE.      - ADD FIELDS {RMIX,SMIX,RREF,SREF}
C     *                              FOR COSP INPUT, MANY FIELDS FOR
C     *                              COSP OUTPUT (WITH DIFFERENT OPTIONS)
C     *                              AND REMOVE PREVIOUS DIRECT ISCCP
C     *                              FIELDS.
C     *                            - ADD NEW DIAGNOSTIC FIELDS:
C     *                              SWA (PAK/ROW AND PAL/ROL),SWX,SWXU,
C     *                              SWXV,SRH,SRHN,SRHX,FSDC,FSSC,FDLC
C     *                              AND REMOVE: SWMX.
C     *                            - FOR CONVECTION, ADD: DMCD,DMCU AND
C     *                              REMOVE: ACMT,DCMT,SCMT,PCPS,CLDS,
C     *                              LHRD,LHRS,SHRD,SHRS.
C     *                            - ADD FIELDS FOR CTEM UNDER CONTROL
C     *                              OF NEW CPP DIRECTIVE: "COUPLER_CTEM".
C     *                            - ADD NEW FIELDS FOR COUPLER: OFSG,
C     *                              PHIS,PMSL,XSRF.
C     *                            - PREVIOUS UPDATE DIRECIVE "HISTEMI"
C     *                              CONVERTED TO CPP:
C     *                              "TRANSIENT_AEROSOL_EMISSIONS" WITH
C     *                              FURTHER CHOICE OF CPP DIRECTIVES
C     *                              "HISTEMI" FOR HISTORICAL OR
C     *                              "EMISTS" FOR FUTURE EMISSIONS.
C     *                              THE "HISTEMI" FIELDS ARE THE
C     *                              SAME AS PREVIOUSLY. FOR "EMISTS",
C     *                              THE FOLLOWING ARE ADDED (BOTH PAK/ROW
C     *                              AND PAL/ROL SINCE INTERPOLATED):
C     *                              SAIR,SSFC,SSTK,SFIR,OAIR,OSFC,OSTK,OFIR,
C     *                              BAIR,BSFC,BSTK,BFIR. THE FIRST LETTER
C     *                              INDICATES THE SPECIES ("B" FOR BLACK
C     *                              CARBON, "O" FOR ORGANIC CARBON AND
C     *                              "S" FOR SULFUR), WHILE FOR EACH OF
C     *                              THESE, THERE ARE "AIR" FOR AIRCRAFT,
C     *                              "SFC" FOR SURFACE, "STK" FOR STACK
C     *                              AND "FIR" FOR FIRE, EACH HAVING
C     *                              DIFFERENT EMISSION HEIGHTS.
C     *                            - FOR THE CHEMISTRY, THE FOLLOWING
C     *                              FIELDS HAVE BEEN ADDED:
C     *                              DDD,DDB,DDO,DDS,WDLD,WDLB,WDLO,WDLS,
C     *                              WDDD,WDDB,WDDO,WDDS,WDSD,WDSB,WDSO,
C     *                              WDSS,ESD,ESFS,EAIS,ESTS,EFIS,ESFB,
C     *                              EAIB,ESTB,EFIB,ESFO,EAIO,ESTO,EFIO
C     *                              AND THE FOLLOWING HAVE BEEN REMOVED:
C     *                              ASFS,ASHP,ASO3,AWDS,DAFX,DCFX,DDA,DDC,
C     *                              ESBT,ESFF,EOFF,EBFF,EOBB,EBBB,ESWF,
C     *                              SFD,SFS,WDDA,WDDC,WDLA,WDLC,WDSA,WDSC,
C     *                              CDPH,CLPH,CSPH.
C     *                            - FAIRPAK/FAIRROW AND FAIRPAL/FAIRROL
C     *                              ADDED FOR AIRCRAFT EMISSIONS
C     *                            - O3CPAK/O3CROW AND O3CPAL/O3CROL
C     *                              ADDED FOR CHEMICAL OZONE INTERPOLATION.
C     *                            - UPDATE DIRECTIVES TURNED INTO
C     *                              CPP DIRECTIVES.
C     * FEB 20/2009 - M.LAZARE.    PREVIOUS VERSION COMROW9H FOR GCM15H:
C     *                            - ADD NEW FIELDS FOR EMISSIONS: EOFF,
C     *                              EBFF,EOBB,EBBB.
C     *                            - ADD NEW FIELDS FOR DIAGNOSTICS OF
C     *                              CONSERVATION: QTPT,XTPT.
C     *                            - ADD NEW FIELD FOR CHEMISTRY: SFRC.
C     *                            - ADD NEW DIAGNOSTIC CLOUD FIELDS
C     *                              (USING OPTICAL DEPTH CUTOFF):
C     *                              CLDO (BOTH PAK AND PAL).
C     *                            - REORGANIZE EMISSION FORCING FIELDS
C     *                              DEPENDANT WHETHER ARE UNDER
C     *                              HISTORICAL EMISSIONS (%DF HISTEMI)
C     *                              OR NOT.
C     *                            - ADD FIELDS FOR EXPLOSIVE VOLCANOES:
C     *                              VTAU AND TROP UNDER CONTROL OF
C     *                              "%DF EXPLVOL". EACH HAVE BOTH PAK
C     *                              AND PAL.
C     * APR 21/2008 - L.SOLHEIM/   PREVIOUS VERSION COMROW9G FOR GCM15G:
C     *               M.LAZARE/    -  ADD NEW RADIATIVE FORCING ARRAYS
C     *               K.VONSALZEN/    (UNDER CONTROL OF "%DF RADFORCE").
C     *               X.MA.        - NEW DIAGNOSTIC FIELDS: WDD4,WDS4,EDSL,
C     *                              ESBF,ESFF,ESVC,ESVE,ESWF ALONG WITH
C     *                              TDEM->EDSO (UNDER CONTROL OF
C     *                              "XTRACHEM"), AS WELL AS ALMX,ALMC
C     *                              AND INSTANTANEOUS CLWT,CIDT.
C     *                            - New AEROCOM forcing fields:
C     *                              EBWA,EOWA,ESWA,EOST (each with accumulated
C     *                              and target),ESCV,EHCV,ESEV,EHEV,
C     *                              EBBT,EOBT,EBFT,EOFT,ESDT,ESIT,ESST,
C     *                              ESOT,ESPT,ESRT.
C     *                            - REMOVE UNUSED: QTPN,UTPM,VTPM,TSEM,EBC,
C     *                              EOC,EOCF,ESO2,EVOL,HVOL.
c     * Jan 11/2007 - M.Lazare/    Previous version comrow9f for gcm15f:
c     *               J.Cole.      - Add isccp simulator fields from
c     *                              Jason.
c     *                            - Two extra new fields ("dmc" and "smc")
c     *                              under control of "%if def,xtraconv".
C     *                            - Remove "buoy" (now in physicff).
C===================================================================
C     * GENERAL PHYSICS ARRAYS.
C
      REAL LUINROW,LUIMROW,LRINROW,LRIMROW
      REAL ILMOROW,LLAKROW,LDMXROW,LZICROW
#if defined (flake)
      REAL LTAVROW,LTICROW,LTSNROW,LSHPROW
      REAL LTMXROW,LTWBROW,LZSNROW
#endif
#if defined (cslm)
      REAL NLKLROW
#endif
      REAL LAMNROT,LAMXROT,LNZ0ROT,MVROT
      REAL MVROW,NO3ROW,NH3ROW,NH4ROW,NO3ROL,NH3ROL,NH4ROL
      REAL LW_EXT_SA_ROW, LW_EXT_SA_ROL,
     &     LW_SSA_SA_ROW, LW_SSA_SA_ROL,
     &     LW_G_SA_ROW,   LW_G_SA_ROL
      REAL NTROW,N20ROW,N50ROW,N100ROW,N200ROW

      COMMON /ROW/ ALMCROW (ILG,ILEV)
      COMMON /ROW/ ALMXROW (ILG,ILEV)
      COMMON /ROW/ CICROW  (ILG,ILEV)
      COMMON /ROW/ CLCVROW (ILG,ILEV)
      COMMON /ROW/ CLDROW  (ILG,ILEV)
      COMMON /ROW/ CLWROW  (ILG,ILEV)
      COMMON /ROW/ CSALROL (ILG,NBS)
      COMMON /ROW/ CSALROT (ILG,IM,NBS)
      COMMON /ROW/ CVARROW (ILG,ILEV)
      COMMON /ROW/ CVMCROW (ILG,ILEV)
      COMMON /ROW/ CVSGROW (ILG,ILEV)
      COMMON /ROW/ H2O2ROL (ILG,LEVOX)
      COMMON /ROW/ H2O2ROW (ILG,LEVOX)
      COMMON /ROW/ HNO3ROL (ILG,LEVOX)
      COMMON /ROW/ HNO3ROW (ILG,LEVOX)
      COMMON /ROW/ HRLROW  (ILG,ILEV)
      COMMON /ROW/ HRSROW  (ILG,ILEV)
      COMMON /ROW/ HRLCROW (ILG,ILEV)
      COMMON /ROW/ HRSCROW (ILG,ILEV)
      COMMON /ROW/ NH3ROL  (ILG,LEVOX)
      COMMON /ROW/ NH3ROW  (ILG,LEVOX)
      COMMON /ROW/ NH4ROL  (ILG,LEVOX)
      COMMON /ROW/ NH4ROW  (ILG,LEVOX)
      COMMON /ROW/ NO3ROL  (ILG,LEVOX)
      COMMON /ROW/ NO3ROW  (ILG,LEVOX)
      COMMON /ROW/ O3ROL   (ILG,LEVOX)
      COMMON /ROW/ O3ROW   (ILG,LEVOX)
      COMMON /ROW/ O3CROL  (ILG,LEVOZC)
      COMMON /ROW/ O3CROW  (ILG,LEVOZC)
      COMMON /ROW/ OHROL   (ILG,LEVOX)
      COMMON /ROW/ OHROW   (ILG,LEVOX)
      COMMON /ROW/ OMETROW (ILG,ILEV)
      COMMON /ROW/ OZROL   (ILG,LEVOZ)
      COMMON /ROW/ OZROW   (ILG,LEVOZ)
      COMMON /ROW/ QWF0ROL (ILG,ILEV)
      COMMON /ROW/ QWFMROL (ILG,ILEV)
      COMMON /ROW/ RHROW   (ILG,ILEV)
      COMMON /ROW/ SALBROL (ILG,NBS)
      COMMON /ROW/ SALBROT (ILG,IM,NBS)
      COMMON /ROW/ SCDNROW (ILG,ILEV)
      COMMON /ROW/ SCLFROW (ILG,ILEV)
      COMMON /ROW/ SLWCROW (ILG,ILEV)
      COMMON /ROW/ TACNROW (ILG,ILEV)
      COMMON /ROW/ TBNDROL (ILG,ILEV)
      COMMON /ROW/ TBNDROW (ILG,ILEV)
      COMMON /ROW/ EA55ROL (ILG,ILEV)
      COMMON /ROW/ EA55ROW (ILG,ILEV)
      COMMON /ROW/ WRKAROL (ILG,NBS)
      COMMON /ROW/ WRKBROL (ILG,NBS)
      COMMON /ROW/ ZDETROW (ILG,ILEV)
      COMMON /ROW/ SFRCROL (ILG,ILEV,NTRAC)
      COMMON /ROW/ ALTIROW (ILG,ILEV)
C
C     * FTOXROW,FTOYROW - X,Y COMPONENTS OF ORO GW MOMENTUM FLUX.
C     * TDOXROW,TDOYROW - X,Y COMPONENTS OF ORO GW TENDENCIES.
C
      COMMON /ROW/ FTOXROW (ILG,ILEV),FTOYROW (ILG,ILEV)
      COMMON /ROW/ TDOXROW (ILG,ILEV),TDOYROW (ILG,ILEV)
C
C     * ARRAY FOR IN ANGLE FROM DUE EAST TO LOCAL ZONAL DIRECTION
C     * HERE IN THE SPECTRAL MODEL THIS IS ALWAYS ZERO, AND SO NO PACK ARRAYS ARE NEEDED
C     * THIS ROW ARRAY WILL BE ZEROED OUT IN THE CORE ROUTINE BEFORE PHYSICS IS CALLED.
C
      COMMON /ROW/ PHIEROW(ILG)
C
      COMMON /ROW/ ALPHROW (ILG)
      COMMON /ROW/ BEGIROL (ILG)
      COMMON /ROW/ BEGKROL (ILG)
      COMMON /ROW/ BEGLROL (ILG)
      COMMON /ROW/ BEGOROL (ILG)
      COMMON /ROW/ BEGROL  (ILG)
      COMMON /ROW/ BWGIROL (ILG)
      COMMON /ROW/ BWGKROL (ILG)
      COMMON /ROW/ BWGLROL (ILG)
      COMMON /ROW/ BWGOROL (ILG)
      COMMON /ROW/ BWGROL  (ILG)
      COMMON /ROW/ CBMFROL (ILG)
      COMMON /ROW/ CHFXROW (ILG)
      COMMON /ROW/ CICTROW (ILG)
      COMMON /ROW/ CLBROL  (ILG)
      COMMON /ROW/ CLBROT (ILG,IM)
      COMMON /ROW/ CLDTROW (ILG)
      COMMON /ROW/ CLDOROL (ILG)
      COMMON /ROW/ CLDOROW (ILG)
      COMMON /ROW/ CLDAROL (ILG)
      COMMON /ROW/ CLDAROW (ILG)
      COMMON /ROW/ CLWTROW (ILG)
      COMMON /ROW/ CQFXROW (ILG)
      COMMON /ROW/ CSBROL  (ILG)
      COMMON /ROW/ CSBROT (ILG,IM)
      COMMON /ROW/ CSDROL  (ILG)
      COMMON /ROW/ CSDROT (ILG,IM)
      COMMON /ROW/ CSFROL  (ILG)
      COMMON /ROW/ CSFROT (ILG,IM)
      COMMON /ROW/ CSZROL  (ILG)
      COMMON /ROW/ DELTROW (ILG)
      COMMON /ROW/ DMSOROL (ILG)
      COMMON /ROW/ DMSOROW (ILG)
      COMMON /ROW/ EDMSROL (ILG)
      COMMON /ROW/ EDMSROW (ILG)
      COMMON /ROW/ EMISROW (ILG)
      COMMON /ROW/ EMISROT (ILG,IM)
      COMMON /ROW/ ENVROW  (ILG)
      COMMON /ROW/ FDLCROW (ILG)
      COMMON /ROW/ FDLCROT (ILG,IM)
      COMMON /ROW/ FDLROT (ILG,IM)
      COMMON /ROW/ FDLROW  (ILG)
      COMMON /ROW/ FLAMROL (ILG)
      COMMON /ROW/ FLAMROW (ILG)
      COMMON /ROW/ FLANROW (ILG)
      COMMON /ROW/ FLANROL (ILG)
      COMMON /ROW/ FLGCROW (ILG)
      COMMON /ROW/ FLGROW  (ILG)
      COMMON /ROW/ FLGROT (ILG,IM)
      COMMON /ROW/ FLKRROW (ILG)
      COMMON /ROW/ FLKUROW (ILG)
      COMMON /ROW/ FLNDROW (ILG)
      COMMON /ROW/ FSAMROL (ILG)
      COMMON /ROW/ FSAMROW (ILG)
      COMMON /ROW/ FSANROW (ILG)
      COMMON /ROW/ FSANROL (ILG)
      COMMON /ROW/ FSDCROW (ILG)
      COMMON /ROW/ FSDROL  (ILG)
      COMMON /ROW/ FSDROT (ILG,IM)
      COMMON /ROW/ FSDROW  (ILG)
      COMMON /ROW/ FSFROL  (ILG)
      COMMON /ROW/ FSFROT (ILG,IM)
      COMMON /ROW/ FSGCROW (ILG)
      COMMON /ROW/ FSGIROL(ILG)
      COMMON /ROW/ FSGOROL(ILG)
      COMMON /ROW/ FSGROT (ILG,IM)
      COMMON /ROW/ FSGROW  (ILG)
      COMMON /ROW/ FSIROL  (ILG)
      COMMON /ROW/ FSIROT (ILG,IM)
      COMMON /ROW/ FSLOROL (ILG)
      COMMON /ROW/ FSLOROW (ILG)
      COMMON /ROW/ FSOROW  (ILG)
      COMMON /ROW/ FSRROW  (ILG)
      COMMON /ROW/ FSRCROW (ILG)
      COMMON /ROW/ FSSCROW (ILG)
      COMMON /ROW/ FSSROW  (ILG)
      COMMON /ROW/ FSVROL  (ILG)
      COMMON /ROW/ FSVROW  (ILG)
      COMMON /ROW/ FSVROT (ILG,IM)
      COMMON /ROW/ FSDBROL (ILG,NBS)
      COMMON /ROW/ FSDBROT (ILG,IM,NBS)
      COMMON /ROW/ FSFBROL (ILG,NBS)
      COMMON /ROW/ FSFBROT (ILG,IM,NBS)
      COMMON /ROW/ CSDBROL (ILG,NBS)
      COMMON /ROW/ CSDBROT (ILG,IM,NBS)
      COMMON /ROW/ CSFBROL (ILG,NBS)
      COMMON /ROW/ CSFBROT (ILG,IM,NBS)
      COMMON /ROW/ FSSBROL (ILG,NBS)
      COMMON /ROW/ FSSBROT (ILG,IM,NBS)
      COMMON /ROW/ FSSCBROL(ILG,NBS)
      COMMON /ROW/ FSSCBROT(ILG,IM,NBS)
      COMMON /ROW/ GAMROW  (ILG)
      COMMON /ROW/ GICNROW (ILG)
      COMMON /ROW/ GTAROW  (ILG)
      COMMON /ROW/ GTROW   (ILG)
      COMMON /ROW/ GTROT   (ILG,IM)
      COMMON /ROW/ GTROX   (ILG,IM)
      COMMON /ROW/ HFLROW  (ILG)
      COMMON /ROW/ HFSROW  (ILG)
      COMMON /ROW/ HSEAROL (ILG)
      COMMON /ROW/ OFSGROL (ILG)
      COMMON /ROW/ OLRROW  (ILG)
      COMMON /ROW/ OLRCROW (ILG)
      COMMON /ROW/ PARROL  (ILG)
      COMMON /ROW/ PARROT (ILG,IM)
      COMMON /ROW/ PBLHROW (ILG)
      COMMON /ROW/ PBLTROW (ILG)
      COMMON /ROW/ PCHFROW (ILG)
      COMMON /ROW/ PCPCROW (ILG)
      COMMON /ROW/ PCPROW  (ILG)
      COMMON /ROW/ PCPSROW (ILG)
      COMMON /ROW/ PHISROW(ILG)
      COMMON /ROW/ PLHFROW (ILG)
      COMMON /ROW/ PMSLROL (ILG)
      COMMON /ROW/ PSAROW  (ILG)
      COMMON /ROW/ PSHFROW (ILG)
      COMMON /ROW/ PSIROW  (ILG)
      COMMON /ROW/ PWATROM (ILG)
      COMMON /ROW/ PWATROW (ILG)
      COMMON /ROW/ QFSROW  (ILG)
      COMMON /ROW/ QFSLROL (ILG)
      COMMON /ROW/ QFSOROL (ILG)
      COMMON /ROW/ QFXROW  (ILG)
      COMMON /ROW/ QTPFROM (ILG)
      COMMON /ROW/ QTPHROM (ILG)
      COMMON /ROW/ QTPTROM (ILG)
      COMMON /ROW/ RAINSROL(ILG)
      COMMON /ROW/ SICNROW (ILG)
      COMMON /ROW/ SICROW  (ILG)
      COMMON /ROW/ SIGXROW (ILG)
      COMMON /ROW/ SLIMROL (ILG)
      COMMON /ROW/ SLIMRlw(ILG),SLIMRsh(ILG),SLIMRlh(ILG)
      COMMON /ROW/ SNOROW  (ILG)
      COMMON /ROW/ SNOROT  (ILG,IM)
      COMMON /ROW/ SNOWSROL(ILG)
      COMMON /ROW/ SQROW   (ILG)
      COMMON /ROW/ SRHROW  (ILG)
      COMMON /ROW/ SRHNROW (ILG)
      COMMON /ROW/ SRHXROW (ILG)
      COMMON /ROW/ STMNROW (ILG)
      COMMON /ROW/ STMXROW (ILG)
      COMMON /ROW/ STROW   (ILG)
      COMMON /ROW/ SUROW   (ILG)
      COMMON /ROW/ SVROW   (ILG)
      COMMON /ROW/ SWAROW  (ILG)
      COMMON /ROW/ SWAROL  (ILG)
      COMMON /ROW/ SWXROW  (ILG)
      COMMON /ROW/ SWXUROW (ILG)
      COMMON /ROW/ SWXVROW (ILG)
      COMMON /ROW/ TCVROW  (ILG)
      COMMON /ROW/ TFXROW  (ILG)
      COMMON /ROW/ UFSROW  (ILG)
      COMMON /ROW/ UFSROL  (ILG)
      COMMON /ROW/ UFSIROL(ILG)
      COMMON /ROW/ UFSOROL(ILG)
      COMMON /ROW/ VFSROW  (ILG)
      COMMON /ROW/ VFSROL  (ILG)
      COMMON /ROW/ VFSIROL(ILG)
      COMMON /ROW/ VFSOROL(ILG)
      COMMON /ROW/ RKMROW (ILG,ILEV) ! Momentum eddy diffusivities
      COMMON /ROW/ RKHROW (ILG,ILEV) ! Temperature eddy diffusivities
      COMMON /ROW/ RKQROW (ILG,ILEV) ! Water vapour eddy diffusivities
C
C     * TKE ADDITIONAL FIELDS.
C
      COMMON /ROW/ TKEMROW(ILG,ILEV)
      COMMON /ROW/ XLMROW (ILG,ILEV)
      COMMON /ROW/ SVARROW(ILG,ILEV)
      COMMON /ROW/ XLMROT (ILG,IM,ILEV)

C     INSTANTANEOUS VARIABLES FOR SAMPLING

      COMMON /ROW/ SWROL(ILG)      ! Wind speed
      COMMON /ROW/ SRHROL(ILG)     ! Near-surface Relative Humidity
      COMMON /ROW/ PCPROL(ILG)     ! Total precip
      COMMON /ROW/ PCPNROL(ILG)    ! Snow precip
      COMMON /ROW/ PCPCROL(ILG)    ! Convective precip
      COMMON /ROW/ QFSIROL(ILG)    ! Evaporation including Sublimation and Transpiration
      COMMON /ROW/ QFNROL(ILG)     ! Surface Snow and Ice Sublimation Flux
      COMMON /ROW/ QFVFROL(ILG)    !
      COMMON /ROW/ UFSINSTROL(ILG) ! Surface Downward Eastward Wind Stress
      COMMON /ROW/ VFSINSTROL(ILG) ! Surface Downward Northward Wind Stress
      COMMON /ROW/ HFLIROL(ILG)    ! Surface Upward Latent Heat Flux
      COMMON /ROW/ HFSIROL(ILG)    ! Surface Upward Sensible Heat Flux
      COMMON /ROW/ FDLROL(ILG)     ! Surface Downwelling Longwave Radiation
      COMMON /ROW/ FLGROL(ILG)     ! Net Longwave Radiation (subtract from FDL to get upward flux)
      COMMON /ROW/ FSSROL(ILG)     ! Surface Downwelling Shortwave Radiation
      COMMON /ROW/ FSGROL(ILG)     ! Net Shortwave Radiation (subtract from FSS to get upward flux)
      COMMON /ROW/ FSSCROL(ILG)    ! Surface Downwelling Clear-Sky Shortwave Radiation
      COMMON /ROW/ FSGCROL(ILG)    ! Surface Upwelling Clear-Sky Shortwave Radiation (subtract from FSSC to get upward flux)
      COMMON /ROW/ FDLCROL (ILG)   ! Surface Downwelling Clear-Sky Longwave Radiation
      COMMON /ROW/ FSOROL  (ILG)   ! TOA Incident Shortwave Radiation
      COMMON /ROW/ FSRROL  (ILG)   ! Top-of-Atmosphere Outgoing Shortwave Radiation
      COMMON /ROW/ OLRROL  (ILG)   ! TOA Outgoing Longwave Radiation
      COMMON /ROW/ OLRCROL (ILG)   ! TOA Outgoing Clear-Sky Longwave Radiation
      COMMON /ROW/ FSRCROL (ILG)   ! TOA Outgoing Clear-Sky Shortwave Radiation
      COMMON /ROW/ PWATIROL(ILG)   ! Water Vapor Path
      COMMON /ROW/ CLDTROL (ILG)   ! Total Cloud Cover Percentage
      COMMON /ROW/ CLWTROL (ILG)   ! Condensed Water Path (liquid)
      COMMON /ROW/ CICTROL(ILG)    ! Condensed Water Path (ice)
      COMMON /ROW/ BALTROL(ILG)    ! Net Downward Radiative Flux at Top of Model
      COMMON /ROW/ PMSLIROL(ILG)   ! Sea Level Pressure
      COMMON /ROW/ CDCBROL(ILG)    ! Fraction of Time Convection Occurs in Cell
      COMMON /ROW/ CSCBROL(ILG)    ! Fraction of Time Shallow Convection Occurs
      COMMON /ROW/ GTROL(ILG)      ! Surface Temperature

      COMMON /ROW/ TCDROL(ILG)     ! Pressure at top of convection
      COMMON /ROW/ BCDROL(ILG)     ! Pressure at bottom of convection
      COMMON /ROW/ PSROL(ILG)      ! Surface pressure

      COMMON /ROW/ STROL  (ILG)    ! Near surface temperature
      COMMON /ROW/ SUROL  (ILG)    ! Near surface east wind
      COMMON /ROW/ SVROL  (ILG)    ! Near surface north wind

C INSTANTANEOUS 3D FIELDS
      COMMON /ROW/ DMCROL(ILG,ILEV) ! Net convective mass flux
      COMMON /ROW/ TAROL(ILG,ILEV)  ! Temperature (from physics)
      COMMON /ROW/ UAROL(ILG,ILEV)  ! Eastward wind (from physics)
      COMMON /ROW/ VAROL(ILG,ILEV)  ! Northward wind (from physics)
      COMMON /ROW/ QAROL(ILG,ILEV)  ! Specific humidity (from physics)
      COMMON /ROW/ RHROL(ILG,ILEV)  ! Relative humidity (from physics)
      COMMON /ROW/ ZGROL(ILG,ILEV)  ! Geopotential height (from physics)
      COMMON /ROW/ RKHROL(ILG,ILEV)  ! Eddy coefficient for temperature
      COMMON /ROW/ RKMROL(ILG,ILEV)  ! Eddy coefficient for momentum
      COMMON /ROW/ RKQROL(ILG,ILEV)  ! Water vapour eddy diffusivities
      COMMON /ROW/ TTLCROL(ILG,ILEV) ! Clear-sky LW heating rate
      COMMON /ROW/ TTSCROL(ILG,ILEV) ! Clear-sky SW heating rate

      COMMON /ROW/ TTPROL(ILG,ILEV)  ! Temperature tendency from model physics
      COMMON /ROW/ TTPPROL(ILG,ILEV)  !
      COMMON /ROW/ TTPVROL(ILG,ILEV)  !
      COMMON /ROW/ TTPLROL(ILG,ILEV)  ! Temperature tendency from LW heating
      COMMON /ROW/ TTPSROL(ILG,ILEV)  ! Temperature tendency from SW heating
      COMMON /ROW/ TTPCROL(ILG,ILEV)  ! Temperature tendency from convection
      COMMON /ROW/ TTPMROL(ILG,ILEV)  ! Temperature tendency from convection

      COMMON /ROW/ QTPCROL(ILG,ILEV)  ! Specific humidity tendency from convection
      COMMON /ROW/ QTPMROL(ILG,ILEV)  ! Specific humidity tendency from convection
      COMMON /ROW/ QTPPROL(ILG,ILEV)  !
      COMMON /ROW/ QTPVROL(ILG,ILEV)  !
      COMMON /ROW/ QTPROL(ILG,ILEV)  ! Specific humidity tendency from model physics

C
C     * 3-HOUR SAVE FIELDS.
C
      COMMON /ROW/ CLDT3HROW(ILG)
      COMMON /ROW/ HFL3HROW (ILG)
      COMMON /ROW/ HFS3HROW (ILG)
      COMMON /ROW/ ROF3HROL (ILG)
      COMMON /ROW/ WGL3HROW (ILG)
      COMMON /ROW/ WGF3HROW (ILG)
      COMMON /ROW/ PCP3HROW (ILG)
      COMMON /ROW/ PCPC3HROW(ILG)
      COMMON /ROW/ PCPL3HROW  (ILG)
      COMMON /ROW/ PCPS3HROW(ILG)
      COMMON /ROW/ PCPN3HROW(ILG)
      COMMON /ROW/ FDL3HROW (ILG)
      COMMON /ROW/ FDLC3HROW(ILG)
      COMMON /ROW/ FLG3HROW (ILG)
      COMMON /ROW/ FSS3HROW (ILG)
      COMMON /ROW/ FSSC3HROW(ILG)
      COMMON /ROW/ FSD3HROW (ILG)
      COMMON /ROW/ FSG3HROW (ILG)
      COMMON /ROW/ FSGC3HROW(ILG)
      COMMON /ROW/ SQ3HROW  (ILG)
      COMMON /ROW/ ST3HROW  (ILG)
      COMMON /ROW/ SU3HROW  (ILG)
      COMMON /ROW/ SV3HROW  (ILG)

      COMMON /ROW/ OLR3HROW  (ILG) ! All-sky outgoing thermal TOA
      COMMON /ROW/ FSR3HROW  (ILG) ! All-sky outgoing solar TOA
      COMMON /ROW/ OLRC3HROW (ILG) ! Clear-sky outgoing thermal TOA
      COMMON /ROW/ FSRC3HROW (ILG) ! Clear-sky outgoing solar TOA
      COMMON /ROW/ FSO3HROW  (ILG) ! Incident solar at TOA
      COMMON /ROW/ PWAT3HROW (ILG) ! Precipitable water
      COMMON /ROW/ CLWT3HROW (ILG) ! Vertically integrated non-precipitating liquid water
      COMMON /ROW/ CICT3HROW (ILG) ! Vertically integrated non-precipitating ice water
      COMMON /ROW/ PMSL3HROW (ILG) ! Sea-level pressure

C
C     * OTHERS.
C
      COMMON /ROW/ ISEEDROW(ILG,4)
C
C     * LAKES.
C
      COMMON /ROW/ BLAKROW(ILG)
      COMMON /ROW/ HLAKROW(ILG)
      COMMON /ROW/ LDMXROW(ILG)
      COMMON /ROW/ LLAKROW(ILG)
      COMMON /ROW/ LRIMROW(ILG)
      COMMON /ROW/ LRINROW(ILG)
      COMMON /ROW/ LUIMROW(ILG)
      COMMON /ROW/ LUINROW(ILG)
      COMMON /ROW/ LZICROW(ILG)
#if defined (cslm)
      COMMON /ROW/ DELUROW(ILG)
      COMMON /ROW/ DTMPROW(ILG)
      COMMON /ROW/ EXPWROW(ILG)
      COMMON /ROW/ GREDROW(ILG)
      COMMON /ROW/ NLKLROW(ILG)
      COMMON /ROW/ RHOMROW(ILG)
      COMMON /ROW/ T0LKROW(ILG)
      COMMON /ROW/ TKELROW(ILG)
      COMMON /ROW/ TLAKROW(ILG,NLKLM)
C
      COMMON /ROW/ FLGLROW(ILG)
      COMMON /ROW/ FNLROW (ILG)
      COMMON /ROW/ FSGLROW(ILG)
      COMMON /ROW/ HFCLROW(ILG)
      COMMON /ROW/ HFLLROW(ILG)
      COMMON /ROW/ HFSLROW(ILG)
      COMMON /ROW/ HMFLROW(ILG)
      COMMON /ROW/ PILROW (ILG)
      COMMON /ROW/ QFLROW (ILG)
#endif
#if defined (flake)
      COMMON /ROW/ LSHPROW (ILG)
      COMMON /ROW/ LTAVROW (ILG)
      COMMON /ROW/ LTICROW (ILG)
      COMMON /ROW/ LTMXROW (ILG)
      COMMON /ROW/ LTSNROW (ILG)
      COMMON /ROW/ LTWBROW (ILG)
      COMMON /ROW/ LZSNROW (ILG)
#endif
C
C     * EMISSIONS.
C
      COMMON /ROW/ EOSTROW (ILG)
      COMMON /ROW/ EOSTROL (ILG)
C
      COMMON /ROW/ ESCVROW (ILG)
      COMMON /ROW/ EHCVROW (ILG)
      COMMON /ROW/ ESEVROW (ILG)
      COMMON /ROW/ EHEVROW (ILG)
C
#if defined transient_aerosol_emissions
      COMMON /ROW/ FBBCROW (ILG,LEVWF)
      COMMON /ROW/ FBBCROL (ILG,LEVWF)
      COMMON /ROW/ FAIRROW (ILG,LEVAIR)
      COMMON /ROW/ FAIRROL (ILG,LEVAIR)
#if defined emists
      COMMON /ROW/  SAIRROW(ILG)
      COMMON /ROW/  SSFCROW(ILG)
      COMMON /ROW/  SBIOROW(ILG)
      COMMON /ROW/  SSHIROW(ILG)
      COMMON /ROW/  SSTKROW(ILG)
      COMMON /ROW/  SFIRROW(ILG)
      COMMON /ROW/  SAIRROL(ILG)
      COMMON /ROW/  SSFCROL(ILG)
      COMMON /ROW/  SBIOROL(ILG)
      COMMON /ROW/  SSHIROL(ILG)
      COMMON /ROW/  SSTKROL(ILG)
      COMMON /ROW/  SFIRROL(ILG)
      COMMON /ROW/  OAIRROW(ILG)
      COMMON /ROW/  OSFCROW(ILG)
      COMMON /ROW/  OBIOROW(ILG)
      COMMON /ROW/  OSHIROW(ILG)
      COMMON /ROW/  OSTKROW(ILG)
      COMMON /ROW/  OFIRROW(ILG)
      COMMON /ROW/  OAIRROL(ILG)
      COMMON /ROW/  OSFCROL(ILG)
      COMMON /ROW/  OBIOROL(ILG)
      COMMON /ROW/  OSHIROL(ILG)
      COMMON /ROW/  OSTKROL(ILG)
      COMMON /ROW/  OFIRROL(ILG)
      COMMON /ROW/  BAIRROW(ILG)
      COMMON /ROW/  BSFCROW(ILG)
      COMMON /ROW/  BBIOROW(ILG)
      COMMON /ROW/  BSHIROW(ILG)
      COMMON /ROW/  BSTKROW(ILG)
      COMMON /ROW/  BFIRROW(ILG)
      COMMON /ROW/  BAIRROL(ILG)
      COMMON /ROW/  BSFCROL(ILG)
      COMMON /ROW/  BBIOROL(ILG)
      COMMON /ROW/  BSHIROL(ILG)
      COMMON /ROW/  BSTKROL(ILG)
      COMMON /ROW/  BFIRROL(ILG)
#endif
#endif
#if (defined(pla) && defined(pfrc))
      COMMON /ROW/  AMLDFROW(ILG,ILEV)
      COMMON /ROW/  AMLDFROL(ILG,ILEV)
      COMMON /ROW/  REAMFROW(ILG,ILEV)
      COMMON /ROW/  REAMFROL(ILG,ILEV)
      COMMON /ROW/  VEAMFROW(ILG,ILEV)
      COMMON /ROW/  VEAMFROL(ILG,ILEV)
      COMMON /ROW/  FR1FROW(ILG,ILEV)
      COMMON /ROW/  FR1FROL(ILG,ILEV)
      COMMON /ROW/  FR2FROW(ILG,ILEV)
      COMMON /ROW/  FR2FROL(ILG,ILEV)
      COMMON /ROW/  SSLDFROW(ILG,ILEV)
      COMMON /ROW/  SSLDFROL(ILG,ILEV)
      COMMON /ROW/  RESSFROW(ILG,ILEV)
      COMMON /ROW/  RESSFROL(ILG,ILEV)
      COMMON /ROW/  VESSFROW(ILG,ILEV)
      COMMON /ROW/  VESSFROL(ILG,ILEV)
      COMMON /ROW/  DSLDFROW(ILG,ILEV)
      COMMON /ROW/  DSLDFROL(ILG,ILEV)
      COMMON /ROW/  REDSFROW(ILG,ILEV)
      COMMON /ROW/  REDSFROL(ILG,ILEV)
      COMMON /ROW/  VEDSFROW(ILG,ILEV)
      COMMON /ROW/  VEDSFROL(ILG,ILEV)
      COMMON /ROW/  BCLDFROW(ILG,ILEV)
      COMMON /ROW/  BCLDFROL(ILG,ILEV)
      COMMON /ROW/  REBCFROW(ILG,ILEV)
      COMMON /ROW/  REBCFROL(ILG,ILEV)
      COMMON /ROW/  VEBCFROW(ILG,ILEV)
      COMMON /ROW/  VEBCFROL(ILG,ILEV)
      COMMON /ROW/  OCLDFROW(ILG,ILEV)
      COMMON /ROW/  OCLDFROL(ILG,ILEV)
      COMMON /ROW/  REOCFROW(ILG,ILEV)
      COMMON /ROW/  REOCFROL(ILG,ILEV)
      COMMON /ROW/  VEOCFROW(ILG,ILEV)
      COMMON /ROW/  VEOCFROL(ILG,ILEV)
      COMMON /ROW/  ZCDNFROW(ILG,ILEV)
      COMMON /ROW/  ZCDNFROL(ILG,ILEV)
      COMMON /ROW/  BCICFROW(ILG,ILEV)
      COMMON /ROW/  BCICFROL(ILG,ILEV)
      COMMON /ROW/  BCDPFROW(ILG)
      COMMON /ROW/  BCDPFROL(ILG)
#endif
C
C     * GENERAL TRACER ARRAYS.
C
      COMMON /ROW/ XFSROW  (ILG,NTRAC)
      COMMON /ROW/ XSFXROL (ILG,NTRAC)
      COMMON /ROW/ XSFXROW (ILG,NTRAC)
      COMMON /ROW/ XSRFROL (ILG,NTRAC)
      COMMON /ROW/ XSRFROW (ILG,NTRAC)
      COMMON /ROW/ XTPFROM (ILG,NTRAC)
      COMMON /ROW/ XTPHROM (ILG,NTRAC)
      COMMON /ROW/ XTPTROM (ILG,NTRAC)
      COMMON /ROW/ XTVIROS (ILG,NTRAC)
      COMMON /ROW/ XTVIROW (ILG,NTRAC)
      COMMON /ROW/ XTVIROM (ILG,NTRAC)
      COMMON /ROW/ XWF0ROL (ILG,ILEV,NTRAC)
      COMMON /ROW/ XWFMROL (ILG,ILEV,NTRAC)
C
C     * LAND SURFACE SCHEME ARRAYS.
C
      COMMON /ROW/ ALICROT(ILG,NTLD,ICAN+1)
      COMMON /ROW/ ALVCROT(ILG,NTLD,ICAN+1)
      COMMON /ROW/ CLAYROT(ILG,NTLD,IGND)
      COMMON /ROW/ CMASROT(ILG,NTLD,ICAN)
      COMMON /ROW/ DZGROW (ILG,IGND)
      COMMON /ROW/ FCANROT(ILG,NTLD,ICAN+1)
      COMMON /ROW/ GFLXROW(ILG,IGND)
      COMMON /ROW/ HFCGROW(ILG,IGND)
      COMMON /ROW/ HMFGROW(ILG,IGND)
      COMMON /ROW/ LAMNROT(ILG,NTLD,ICAN)
      COMMON /ROW/ LAMXROT(ILG,NTLD,ICAN)
      COMMON /ROW/ LNZ0ROT(ILG,NTLD,ICAN+1)
      COMMON /ROW/ ORGMROT(ILG,NTLD,IGND)
      COMMON /ROW/ PORGROW(ILG,IGND)
      COMMON /ROW/ FCAPROW(ILG,IGND)
      COMMON /ROW/ DLZWROT(ILG,NTLD,IGND)
      COMMON /ROW/ PORGROT(ILG,NTLD,IGND)
      COMMON /ROW/ THFCROT(ILG,NTLD,IGND)
      COMMON /ROW/ THLWROT(ILG,NTLD,IGND)
      COMMON /ROW/ THRROT (ILG,NTLD,IGND)
      COMMON /ROW/ THMROT (ILG,NTLD,IGND)
      COMMON /ROW/ BIROT  (ILG,NTLD,IGND)
      COMMON /ROW/ PSISROT(ILG,NTLD,IGND)
      COMMON /ROW/ GRKSROT(ILG,NTLD,IGND)
      COMMON /ROW/ THRAROT(ILG,NTLD,IGND)
      COMMON /ROW/ HCPSROT(ILG,NTLD,IGND)
      COMMON /ROW/ TCSROT (ILG,NTLD,IGND)
      COMMON /ROW/ PSIWROT(ILG,NTLD,IGND)
      COMMON /ROW/ ZBTWROT(ILG,NTLD,IGND)
      COMMON /ROW/ ISNDROT(ILG,NTLD,IGND)
      COMMON /ROW/ QFVGROW(ILG,IGND)
      COMMON /ROW/ ROOTROT(ILG,NTLD,ICAN)
      COMMON /ROW/ SANDROT(ILG,NTLD,IGND)
      COMMON /ROW/ TGROW  (ILG,IGND)
      COMMON /ROW/ TGROT  (ILG,NTLD,IGND)
      COMMON /ROW/ THICROT(ILG,NTLD,IGND)
      COMMON /ROW/ THLQROT(ILG,NTLD,IGND)
      COMMON /ROW/ WGFROW (ILG,IGND)
      COMMON /ROW/ WGLROW (ILG,IGND)
C
      COMMON /ROW/ ANROW  (ILG)
      COMMON /ROW/ ANROT  (ILG,IM)
      COMMON /ROW/ DPTHROT(ILG,NTLD)
      COMMON /ROW/ ALGDVROT(ILG,NTLD)
      COMMON /ROW/ ALGDNROT(ILG,NTLD)
      COMMON /ROW/ ALGWVROT(ILG,NTLD)
      COMMON /ROW/ ALGWNROT(ILG,NTLD)
      COMMON /ROW/ IGDRROT(ILG,NTLD)
      COMMON /ROW/ DPTHROW(ILG)
      COMMON /ROW/ DRNROT (ILG,NTLD)
      COMMON /ROW/ DRROW  (ILG)
      COMMON /ROW/ FLGGROW(ILG)
      COMMON /ROW/ FLGNROW(ILG)
      COMMON /ROW/ FLGVROW(ILG)
      COMMON /ROW/ FNLAROW(ILG)
      COMMON /ROW/ FNROW  (ILG)
      COMMON /ROW/ FNROT  (ILG,IM)
      COMMON /ROW/ FSGGROW(ILG)
      COMMON /ROW/ FSGNROW(ILG)
      COMMON /ROW/ FSGVROW(ILG)
      COMMON /ROW/ FVGROW (ILG)
      COMMON /ROW/ FVNROW (ILG)
      COMMON /ROW/ GAROW  (ILG)
      COMMON /ROW/ GSNOROW(ILG)
      COMMON /ROW/ HBLROW (ILG)
      COMMON /ROW/ HFCNROW(ILG)
      COMMON /ROW/ HFCVROW(ILG)
      COMMON /ROW/ HFLGROW(ILG)
      COMMON /ROW/ HFLNROW(ILG)
      COMMON /ROW/ HFLVROW(ILG)
      COMMON /ROW/ HFSGROW(ILG)
      COMMON /ROW/ HFSNROW(ILG)
      COMMON /ROW/ HFSVROW(ILG)
      COMMON /ROW/ HMFNROW(ILG)
      COMMON /ROW/ HMFVROW(ILG)
      COMMON /ROW/ ILMOROW(ILG)
      COMMON /ROW/ MVROT  (ILG,NTLD)
      COMMON /ROW/ MVROW  (ILG)
      COMMON /ROW/ PCPNROW(ILG)
      COMMON /ROW/ PETROW (ILG)
      COMMON /ROW/ PIGROW (ILG)
      COMMON /ROW/ PINROW (ILG)
      COMMON /ROW/ PIVFROW(ILG)
      COMMON /ROW/ PIVLROW(ILG)
      COMMON /ROW/ QFGROW (ILG)
      COMMON /ROW/ QFNROW (ILG)
      COMMON /ROW/ QFVFROW(ILG)
      COMMON /ROW/ QFVLROW(ILG)
      COMMON /ROW/ RHONROW(ILG)
      COMMON /ROW/ RHONROT(ILG,IM)
      COMMON /ROW/ ROFBROW(ILG)
      COMMON /ROW/ REFROT (ILG,IM)
      COMMON /ROW/ REFROW(ILG)
      COMMON /ROW/ BCSNROT(ILG,IM)
      COMMON /ROW/ BCSNROW(ILG)
      COMMON /ROW/ ROFNROW(ILG)
      COMMON /ROW/ ROFOROL(ILG)
      COMMON /ROW/ ROFOROW(ILG)
      COMMON /ROW/ ROFROL (ILG)
      COMMON /ROW/ ROFROW (ILG)
      COMMON /ROW/ ROFSROW(ILG)
      COMMON /ROW/ ROFVROW(ILG)
      COMMON /ROW/ ROVGROW(ILG)
      COMMON /ROW/ SKYGROW(ILG)
      COMMON /ROW/ SKYNROW(ILG)
      COMMON /ROW/ SMLTROW(ILG)
      COMMON /ROW/ SOCIROT (ILG,NTLD)
      COMMON /ROW/ TBASROW(ILG)
      COMMON /ROW/ TBASROT(ILG,NTLD)
      COMMON /ROW/ TNROW  (ILG)
      COMMON /ROW/ TNROT  (ILG,IM)
      COMMON /ROW/ TTROW  (ILG)
      COMMON /ROW/ TTROT  (ILG,NTLD)
      COMMON /ROW/ TVROW  (ILG)
      COMMON /ROW/ TVROT  (ILG,NTLD)
      COMMON /ROW/ UEROW  (ILG)
      COMMON /ROW/ WTABROW(ILG)
      COMMON /ROW/ WTRGROW(ILG)
      COMMON /ROW/ WTRNROW(ILG)
      COMMON /ROW/ WTRVROW(ILG)
      COMMON /ROW/ WVFROW (ILG)
      COMMON /ROW/ WVFROT (ILG,NTLD)
      COMMON /ROW/ WVLROW (ILG)
      COMMON /ROW/ WVLROT (ILG,NTLD)
      COMMON /ROW/ ZNROW  (ILG)
      COMMON /ROW/ DEPBROW(ILG)
      COMMON /ROW/ TPNDROT(ILG,NTLD)
      COMMON /ROW/ ZPNDROW(ILG)
      COMMON /ROW/ ZPNDROT(ILG,NTLD)
      COMMON /ROW/ TAVROT (ILG,NTLD)
      COMMON /ROW/ QAVROT (ILG,NTLD)
      COMMON /ROW/ WSNOROW(ILG)
      COMMON /ROW/ WSNOROT(ILG,IM)

      COMMON /ROW/ FAREROT(ILG,IM)
      COMMON /ROW/ TSFSROT(ILG,NTLD,4)
#if defined (agcm_ctem)
C
C     * CTEM RELATED VARIABLES
C
      COMMON /ROW/ CFCANROT  (ILG,NTLD,ICANP1)
      COMMON /ROW/ CALVCROT  (ILG,NTLD,ICANP1)
      COMMON /ROW/ CALICROT  (ILG,NTLD,ICANP1)

      COMMON /ROW/ ZOLNCROT  (ILG,NTLD,ICAN)
      COMMON /ROW/ CMASCROT  (ILG,NTLD,ICAN)
      COMMON /ROW/ RMATCROT  (ILG,NTLD,ICAN,  IGND)
      COMMON /ROW/ RTCTMROT  (ILG,NTLD,ICTEM, IGND)

      COMMON /ROW/ AILCROT   (ILG,NTLD,ICAN)
      COMMON /ROW/ PAICROT   (ILG,NTLD,ICAN)
      COMMON /ROW/ SLAICROT  (ILG,NTLD,ICAN)

      COMMON /ROW/ FCANCROT  (ILG,NTLD,ICTEM)
      COMMON /ROW/ TODFCROT  (ILG,NTLD,ICTEM)
      COMMON /ROW/ AILCGROT  (ILG,NTLD,ICTEM)
      COMMON /ROW/ SLAIROT   (ILG,NTLD,ICTEM)
      COMMON /ROW/ CO2CG1ROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ CO2CG2ROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ CO2CS1ROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ CO2CS2ROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ ANCGRTL   (ILG,NTLD,ICTEM)
      COMMON /ROW/ ANCSRTL   (ILG,NTLD,ICTEM)
      COMMON /ROW/ RMLCGRTL  (ILG,NTLD,ICTEM)
      COMMON /ROW/ RMLCSRTL  (ILG,NTLD,ICTEM)

      COMMON /ROW/ FSNOWRTL  (ILG,NTLD)
      COMMON /ROW/ TCANORTL  (ILG,NTLD)
      COMMON /ROW/ TCANSRTL  (ILG,NTLD)
      COMMON /ROW/ TARTL     (ILG,NTLD)
      COMMON /ROW/ CFLUXCSROT(ILG,NTLD)
      COMMON /ROW/ CFLUXCGROT(ILG,NTLD)

      COMMON /ROW/ TBARRTL   (ILG,NTLD,IGND)
      COMMON /ROW/ TBARCRTL  (ILG,NTLD,IGND)
      COMMON /ROW/ TBARCSRTL (ILG,NTLD,IGND)
      COMMON /ROW/ TBARGRTL  (ILG,NTLD,IGND)
      COMMON /ROW/ TBARGSRTL (ILG,NTLD,IGND)
      COMMON /ROW/ THLIQCRTL (ILG,NTLD,IGND)
      COMMON /ROW/ THLIQGRTL (ILG,NTLD,IGND)
      COMMON /ROW/ THICECRTL (ILG,NTLD,IGND)
C
      REAL LGHTROW,LITRCROT,LEAFSROT,LASTRROT,LASTSROT,NEWFROT
C
C     * INVARIANT.
C
      COMMON /ROW/ WETFROW  (ILG)
      COMMON /ROW/ WETSROW  (ILG)
      COMMON /ROW/ LGHTROW  (ILG)
C
C     * TIME VARYING.
C
      COMMON /ROW/ SOILCROT (ILG,NTLD,ICTEMP1)
      COMMON /ROW/ LITRCROT (ILG,NTLD,ICTEMP1)
      COMMON /ROW/ ROOTCROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ STEMCROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ GLEAFCROT(ILG,NTLD,ICTEM)
      COMMON /ROW/ BLEAFCROT(ILG,NTLD,ICTEM)
      COMMON /ROW/ FALLHROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ POSPHROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ LEAFSROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ GROWTROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ LASTRROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ LASTSROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ THISYLROT(ILG,NTLD,ICTEM)
      COMMON /ROW/ STEMHROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ ROOTHROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ TEMPCROT (ILG,NTLD,2)
      COMMON /ROW/ AILCBROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ BMASVROT (ILG,NTLD,ICTEM)
      COMMON /ROW/ VEGHROT  (ILG,NTLD,ICTEM)
      COMMON /ROW/ ROOTDROT (ILG,NTLD,ICTEM)
C
      COMMON /ROW/ PREFROT  (ILG,NTLD,ICTEM)
      COMMON /ROW/ NEWFROT  (ILG,NTLD,ICTEM)
C
      COMMON /ROW/ CVEGROT  (ILG,NTLD)
      COMMON /ROW/ CDEBROT  (ILG,NTLD)
      COMMON /ROW/ CHUMROT  (ILG,NTLD)
      COMMON /ROW/ FCOLROT  (ILG,NTLD)
C
C     * CTEM DIAGNOSTIC OUTPUT FIELDS.
C
      COMMON /ROW/ CVEGROW(ILG)
      COMMON /ROW/ CDEBROW(ILG)
      COMMON /ROW/ CHUMROW(ILG)
      COMMON /ROW/ CLAIROW(ILG)
      COMMON /ROW/ CFNPROW(ILG)
      COMMON /ROW/ CFNEROW(ILG)
      COMMON /ROW/ CFRVROW(ILG)
      COMMON /ROW/ CFGPROW(ILG)
      COMMON /ROW/ CFNBROW(ILG)
      COMMON /ROW/ CFFVROW(ILG)
      COMMON /ROW/ CFFDROW(ILG)
      COMMON /ROW/ CFLVROW(ILG)
      COMMON /ROW/ CFLDROW(ILG)
      COMMON /ROW/ CFLHROW(ILG)
      COMMON /ROW/ CBRNROW(ILG)
      COMMON /ROW/ CFRHROW(ILG)
      COMMON /ROW/ CFHTROW(ILG)
      COMMON /ROW/ CFLFROW(ILG)
      COMMON /ROW/ CFRDROW(ILG)
      COMMON /ROW/ CFRGROW(ILG)
      COMMON /ROW/ CFRMROW(ILG)
      COMMON /ROW/ CVGLROW(ILG)
      COMMON /ROW/ CVGSROW(ILG)
      COMMON /ROW/ CVGRROW(ILG)
      COMMON /ROW/ CFNLROW(ILG)
      COMMON /ROW/ CFNSROW(ILG)
      COMMON /ROW/ CFNRROW(ILG)
      COMMON /ROW/ CH4HROW(ILG)
      COMMON /ROW/ CH4NROW(ILG)
      COMMON /ROW/ WFRAROW(ILG)
      COMMON /ROW/ CW1DROW(ILG)
      COMMON /ROW/ CW2DROW(ILG)
      COMMON /ROW/ FCOLROW(ILG)
      COMMON /ROW/ CURFROW(ILG,ICTEM)

C     Additional CTEM related CMIP6 output

      COMMON /ROW/ BRFRROW(ILG)   ! baresoilFrac
      COMMON /ROW/ C3CRROW(ILG)   ! cropFracC3
      COMMON /ROW/ C4CRROW(ILG)   ! cropFracC4
      COMMON /ROW/ CRPFROW(ILG)   ! cropFrac
      COMMON /ROW/ C3GRROW(ILG)   ! grassFracC3
      COMMON /ROW/ C4GRROW(ILG)   ! grassFracC4
      COMMON /ROW/ GRSFROW(ILG)   ! grassFrac
      COMMON /ROW/ BDTFROW(ILG)   ! treeFracBdlDcd
      COMMON /ROW/ BETFROW(ILG)   ! treeFracBdlEvg
      COMMON /ROW/ NDTFROW(ILG)   ! treeFracNdlDcd
      COMMON /ROW/ NETFROW(ILG)   ! treeFracNdlEvg
      COMMON /ROW/ TREEROW(ILG)   ! treeFrac
      COMMON /ROW/ VEGFROW(ILG)   ! vegFrac
      COMMON /ROW/ C3PFROW(ILG)   ! c3PftFrac
      COMMON /ROW/ C4PFROW(ILG)   ! c4PftFrac
      COMMON /ROW/ CLNDROW(ILG)   ! cLand

C
C Instantaneous output (used for subdaily output)
C
      COMMON /ROW/ CFGPROL (ILG) ! GPP
      COMMON /ROW/ CFRVROL (ILG) ! RA
      COMMON /ROW/ CFRHROL (ILG) ! RH
      COMMON /ROW/ CFRDROL (ILG) ! RH

#endif
C
#if defined explvol
      COMMON /ROW/ VTAUROW(ILG)
      COMMON /ROW/ VTAUROL(ILG)
      COMMON /ROW/ SW_EXT_SA_ROW(ILGZ,LEVSA,NBS)
      COMMON /ROW/ SW_EXT_SA_ROL(ILGZ,LEVSA,NBS)
      COMMON /ROW/ SW_SSA_SA_ROW(ILGZ,LEVSA,NBS)
      COMMON /ROW/ SW_SSA_SA_ROL(ILGZ,LEVSA,NBS)
      COMMON /ROW/ SW_G_SA_ROW(ILGZ,LEVSA,NBS)
      COMMON /ROW/ SW_G_SA_ROL(ILGZ,LEVSA,NBS)
      COMMON /ROW/ LW_EXT_SA_ROW(ILGZ,LEVSA,NBL)
      COMMON /ROW/ LW_EXT_SA_ROL(ILGZ,LEVSA,NBL)
      COMMON /ROW/ LW_SSA_SA_ROW(ILGZ,LEVSA,NBL)
      COMMON /ROW/ LW_SSA_SA_ROL(ILGZ,LEVSA,NBL)
      COMMON /ROW/ LW_G_SA_ROW(ILGZ,LEVSA,NBL)
      COMMON /ROW/ LW_G_SA_ROL(ILGZ,LEVSA,NBL)
      COMMON /ROW/ W055_EXT_SA_ROW(ILGZ,LEVSA)
      COMMON /ROW/ W055_EXT_SA_ROL(ILGZ,LEVSA)
      COMMON /ROW/ W055_VTAU_SA_ROW(ILG)
      COMMON /ROW/ W055_VTAU_SA_ROL(ILG)
      COMMON /ROW/ W055_EXT_GCM_SA_ROW(ILG,ILEV)
      COMMON /ROW/ W055_EXT_GCM_SA_ROL(ILG,ILEV)
      COMMON /ROW/ W110_EXT_SA_ROW(ILGZ,LEVSA)
      COMMON /ROW/ W110_EXT_SA_ROL(ILGZ,LEVSA)
      COMMON /ROW/ W110_VTAU_SA_ROW(ILG)
      COMMON /ROW/ W110_VTAU_SA_ROL(ILG)
      COMMON /ROW/ W110_EXT_GCM_SA_ROW(ILG,ILEV)
      COMMON /ROW/ W110_EXT_GCM_SA_ROL(ILG,ILEV)
      COMMON /ROW/ PRESSURE_SA_ROW(ILGZ,LEVSA)
      COMMON /ROW/ PRESSURE_SA_ROL(ILGZ,LEVSA)
      COMMON /ROW/ TROPROW(ILG)
      COMMON /ROW/ TROPROL(ILG)
#else
      COMMON /ROW/ VTAUROW(1)
      COMMON /ROW/ VTAUROL(1)
      COMMON /ROW/ SW_EXT_SA_ROW(1,1,1)
      COMMON /ROW/ SW_EXT_SA_ROL(1,1,1)
      COMMON /ROW/ SW_SSA_SA_ROW(1,1,1)
      COMMON /ROW/ SW_SSA_SA_ROL(1,1,1)
      COMMON /ROW/ SW_G_SA_ROW(1,1,1)
      COMMON /ROW/ SW_G_SA_ROL(1,1,1)
      COMMON /ROW/ LW_EXT_SA_ROW(1,1,1)
      COMMON /ROW/ LW_EXT_SA_ROL(1,1,1)
      COMMON /ROW/ LW_SSA_SA_ROW(1,1,1)
      COMMON /ROW/ LW_SSA_SA_ROL(1,1,1)
      COMMON /ROW/ LW_G_SA_ROW(1,1,1)
      COMMON /ROW/ LW_G_SA_ROL(1,1,1)
      COMMON /ROW/ W055_EXT_SA_ROW(1,1)
      COMMON /ROW/ W055_EXT_SA_ROL(1,1)
      COMMON /ROW/ W055_VTAU_SA_ROW(1)
      COMMON /ROW/ W055_VTAU_SA_ROL(1)
      COMMON /ROW/ W055_EXT_GCM_SA_ROW(1,1)
      COMMON /ROW/ W055_EXT_GCM_SA_ROL(1,1)
      COMMON /ROW/ W110_EXT_SA_ROW(1,1)
      COMMON /ROW/ W110_EXT_SA_ROL(1,1)
      COMMON /ROW/ W110_VTAU_SA_ROW(1)
      COMMON /ROW/ W110_VTAU_SA_ROL(1)
      COMMON /ROW/ W110_EXT_GCM_SA_ROW(1,1)
      COMMON /ROW/ W110_EXT_GCM_SA_ROL(1,1)
      COMMON /ROW/ PRESSURE_SA_ROW(1,1)
      COMMON /ROW/ PRESSURE_SA_ROL(1,1)
      COMMON /ROW/ TROPROW(1)
      COMMON /ROW/ TROPROL(1)
#endif
      COMMON /ROW/ SPOTROW(ILG)
      COMMON /ROW/ ST01ROW(ILG)
      COMMON /ROW/ ST02ROW(ILG)
      COMMON /ROW/ ST03ROW(ILG)
      COMMON /ROW/ ST04ROW(ILG)
      COMMON /ROW/ ST06ROW(ILG)
      COMMON /ROW/ ST13ROW(ILG)
      COMMON /ROW/ ST14ROW(ILG)
      COMMON /ROW/ ST15ROW(ILG)
      COMMON /ROW/ ST16ROW(ILG)
      COMMON /ROW/ ST17ROW(ILG)
      COMMON /ROW/ SUZ0ROW(ILG)
      COMMON /ROW/ SUZ0ROL(ILG)
      COMMON /ROW/ PDSFROW(ILG)
      COMMON /ROW/ PDSFROL(ILG)

#ifdef rad_flux_profs
! Note that for vertical profiles
! Level 1 is the top of atmosphere
! Level 2 is the top of the model
! Level ILEV+2 is the surface
      COMMON /ROW/ FSAUROW (ILG,ILEV+2) ! Upward all-sky shortwave flux profile
      COMMON /ROW/ FSADROW (ILG,ILEV+2) ! Downward all-sky shortwave flux profile
      COMMON /ROW/ FLAUROW (ILG,ILEV+2) ! Upward all-sky longwave flux profile
      COMMON /ROW/ FLADROW (ILG,ILEV+2) ! Downward all-sky longwave flux profile
      COMMON /ROW/ FSCUROW (ILG,ILEV+2) ! Upward clear-sky shortwave flux profile
      COMMON /ROW/ FSCDROW (ILG,ILEV+2) ! Downward clear-sky shortwave flux profile
      COMMON /ROW/ FLCUROW (ILG,ILEV+2) ! Upward clear-sky longwave flux profile
      COMMON /ROW/ FLCDROW (ILG,ILEV+2) ! Downward clear-sky longwave flux profile
#endif
      COMMON /ROW/ FSAUROL (ILG,ILEV+2) ! Upward all-sky shortwave flux profile
      COMMON /ROW/ FSADROL (ILG,ILEV+2) ! Downward all-sky shortwave flux profile
      COMMON /ROW/ FLAUROL (ILG,ILEV+2) ! Upward all-sky longwave flux profile
      COMMON /ROW/ FLADROL (ILG,ILEV+2) ! Downward all-sky longwave flux profile
      COMMON /ROW/ FSCUROL (ILG,ILEV+2) ! Upward clear-sky shortwave flux profile
      COMMON /ROW/ FSCDROL (ILG,ILEV+2) ! Downward clear-sky shortwave flux profile
      COMMON /ROW/ FLCUROL (ILG,ILEV+2) ! Upward clear-sky longwave flux profile
      COMMON /ROW/ FLCDROL (ILG,ILEV+2) ! Downward clear-sky longwave flux profile

#if defined radforce

      COMMON /ROW/ FSAUROW_R (ILG,ILEV+2,NRFP) ! Upward all-sky shortwave flux profile
      COMMON /ROW/ FSADROW_R (ILG,ILEV+2,NRFP) ! Downward all-sky shortwave flux profile
      COMMON /ROW/ FLAUROW_R (ILG,ILEV+2,NRFP) ! Upward all-sky longwave flux profile
      COMMON /ROW/ FLADROW_R (ILG,ILEV+2,NRFP) ! Downward all-sky longwave flux profile
      COMMON /ROW/ FSCUROW_R (ILG,ILEV+2,NRFP) ! Upward clear-sky shortwave flux profile
      COMMON /ROW/ FSCDROW_R (ILG,ILEV+2,NRFP) ! Downward clear-sky shortwave flux profile
      COMMON /ROW/ FLCUROW_R (ILG,ILEV+2,NRFP) ! Upward clear-sky longwave flux profile
      COMMON /ROW/ FLCDROW_R (ILG,ILEV+2,NRFP) ! Downward clear-sky longwave flux profile
      COMMON /ROW/ FSAUROL_R (ILG,ILEV+2,NRFP) ! Upward all-sky shortwave flux profile
      COMMON /ROW/ FSADROL_R (ILG,ILEV+2,NRFP) ! Downward all-sky shortwave flux profile
      COMMON /ROW/ FLAUROL_R (ILG,ILEV+2,NRFP) ! Upward all-sky longwave flux profile
      COMMON /ROW/ FLADROL_R (ILG,ILEV+2,NRFP) ! Downward all-sky longwave flux profile
      COMMON /ROW/ FSCUROL_R (ILG,ILEV+2,NRFP) ! Upward clear-sky shortwave flux profile
      COMMON /ROW/ FSCDROL_R (ILG,ILEV+2,NRFP) ! Downward clear-sky shortwave flux profile
      COMMON /ROW/ FLCUROL_R (ILG,ILEV+2,NRFP) ! Upward clear-sky longwave flux profile
      COMMON /ROW/ FLCDROL_R (ILG,ILEV+2,NRFP) ! Downward clear-sky longwave flux profile
C
      COMMON /ROW/ RDTROW_R  (ILG,ILEV,NRFP)
      COMMON /ROW/ RDTROL_R  (ILG,ILEV,NRFP)
      COMMON /ROW/ RDTROM_R  (ILG,ILEV,NRFP)
      COMMON /ROW/ HRSROW_R  (ILG,ILEV,NRFP)
      COMMON /ROW/ HRLROW_R  (ILG,ILEV,NRFP)
      COMMON /ROW/ HRSCROW_R (ILG,ILEV,NRFP)
      COMMON /ROW/ HRLCROW_R (ILG,ILEV,NRFP)

      COMMON /ROW/ FSGROL_R  (ILG,NRFP)
      COMMON /ROW/ CSBROL_R  (ILG,NRFP)
      COMMON /ROW/ FSSROL_R  (ILG,NRFP)
      COMMON /ROW/ FSSCROL_R (ILG,NRFP)
      COMMON /ROW/ CLBROL_R  (ILG,NRFP)
      COMMON /ROW/ FLGROL_R  (ILG,NRFP)
      COMMON /ROW/ FDLROL_R  (ILG,NRFP)
      COMMON /ROW/ FDLCROL_R (ILG,NRFP)
      COMMON /ROW/ FSRROL_R  (ILG,NRFP)
      COMMON /ROW/ FSRCROL_R (ILG,NRFP)
      COMMON /ROW/ FSOROL_R  (ILG,NRFP)
      COMMON /ROW/ OLRROL_R  (ILG,NRFP)
      COMMON /ROW/ OLRCROL_R (ILG,NRFP)
      COMMON /ROW/ FSLOROL_R (ILG,NRFP)

      COMMON /ROW/ CSBROT_R  (ILG,IM,NRFP)
      COMMON /ROW/ CLBROT_R  (ILG,IM,NRFP)
      COMMON /ROW/ FSGROT_R  (ILG,IM,NRFP)
      COMMON /ROW/ FLGROT_R  (ILG,IM,NRFP)
      COMMON /ROW/ KTHROL    (ILG)
#endif
#if defined tprhs
      COMMON /ROW/ TTPROW (ILG,ILEV)
#endif
#if defined qprhs
      COMMON /ROW/ QTPROW (ILG,ILEV)
#endif
#if defined uprhs
      COMMON /ROW/ UTPROW (ILG,ILEV)
#endif
#if defined vprhs
      COMMON /ROW/ VTPROW (ILG,ILEV)
#endif
#if defined qconsav
      COMMON /ROW/ QADDROW(ILG,2)
      COMMON /ROW/ QTPHROW(ILG,2)
      COMMON /ROW/ QTPFROW(ILG,ILEV)
#endif
#if defined tprhsc
      COMMON /ROW/ TTPCROW(ILG,ILEV)
      COMMON /ROW/ TTPKROW(ILG,ILEV)
      COMMON /ROW/ TTPMROW(ILG,ILEV)
      COMMON /ROW/ TTPNROW(ILG,ILEV)
      COMMON /ROW/ TTPPROW(ILG,ILEV)
      COMMON /ROW/ TTPVROW(ILG,ILEV)
#endif
#if (defined (tprhsc) || defined (radforce))
      COMMON /ROW/ TTPLROW(ILG,ILEV)
      COMMON /ROW/ TTPSROW(ILG,ILEV)
      COMMON /ROW/ TTSCROW(ILG,ILEV)
      COMMON /ROW/ TTLCROW(ILG,ILEV)
#endif
#if defined qprhsc
      COMMON /ROW/ QTPCROW(ILG,ILEV)
      COMMON /ROW/ QTPMROW(ILG,ILEV)
      COMMON /ROW/ QTPPROW(ILG,ILEV)
      COMMON /ROW/ QTPVROW(ILG,ILEV)
#endif
#if defined uprhsc
      COMMON /ROW/ UTPCROW(ILG,ILEV)
      COMMON /ROW/ UTPGROW(ILG,ILEV)
      COMMON /ROW/ UTPNROW(ILG,ILEV)
      COMMON /ROW/ UTPSROW(ILG,ILEV)
      COMMON /ROW/ UTPVROW(ILG,ILEV)
#endif
#if defined vprhsc
      COMMON /ROW/ VTPCROW(ILG,ILEV)
      COMMON /ROW/ VTPGROW(ILG,ILEV)
      COMMON /ROW/ VTPNROW(ILG,ILEV)
      COMMON /ROW/ VTPSROW(ILG,ILEV)
      COMMON /ROW/ VTPVROW(ILG,ILEV)
#endif
#if defined xconsav
      COMMON /ROW/ XADDROW(ILG,2,NTRAC)
      COMMON /ROW/ XTPHROW(ILG,2,NTRAC)
      COMMON /ROW/ XTPFROW(ILG,ILEV,NTRAC)
#endif
#if defined xprhs
      COMMON /ROW/ XTPROW (ILG,ILEV,NTRAC)
#endif
#if defined xprhsc
      COMMON /ROW/ XTPCROW(ILG,ILEV,NTRAC)
      COMMON /ROW/ XTPMROW(ILG,ILEV,NTRAC)
      COMMON /ROW/ XTPPROW(ILG,ILEV,NTRAC)
      COMMON /ROW/ XTPVROW(ILG,ILEV,NTRAC)
#endif
#if defined xtraconv
      COMMON /ROW/ BCDROW (ILG)
      COMMON /ROW/ BCSROW (ILG)
      COMMON /ROW/ CAPEROW(ILG)
      COMMON /ROW/ CDCBROW(ILG)
      COMMON /ROW/ CINHROW(ILG)
      COMMON /ROW/ CSCBROW(ILG)
      COMMON /ROW/ DMCROW (ILG,ILEV)
      COMMON /ROW/ DMCDROW(ILG,ILEV)
      COMMON /ROW/ DMCUROW(ILG,ILEV)
      COMMON /ROW/ SMCROW (ILG,ILEV)
      COMMON /ROW/ TCDROW (ILG)
      COMMON /ROW/ TCSROW (ILG)
#endif
#if defined xtrachem
      REAL NOXDROW
C
      COMMON /ROW/ DD4ROW (ILG)
      COMMON /ROW/ DOX4ROW(ILG)
      COMMON /ROW/ DOXDROW(ILG)
      COMMON /ROW/ ESDROW (ILG)
      COMMON /ROW/ ESFSROW(ILG)
      COMMON /ROW/ EAISROW(ILG)
      COMMON /ROW/ ESTSROW(ILG)
      COMMON /ROW/ EFISROW(ILG)
      COMMON /ROW/ ESFBROW(ILG)
      COMMON /ROW/ EAIBROW(ILG)
      COMMON /ROW/ ESTBROW(ILG)
      COMMON /ROW/ EFIBROW(ILG)
      COMMON /ROW/ ESFOROW(ILG)
      COMMON /ROW/ EAIOROW(ILG)
      COMMON /ROW/ ESTOROW(ILG)
      COMMON /ROW/ EFIOROW(ILG)
      COMMON /ROW/ EDSLROW(ILG)
      COMMON /ROW/ EDSOROW(ILG)
      COMMON /ROW/ ESVCROW(ILG)
      COMMON /ROW/ ESVEROW(ILG)
      COMMON /ROW/ NOXDROW(ILG)
      COMMON /ROW/ WDD4ROW(ILG)
      COMMON /ROW/ WDL4ROW(ILG)
      COMMON /ROW/ WDS4ROW(ILG)
#ifndef pla
      COMMON /ROW/ DDDROW (ILG)
      COMMON /ROW/ DDBROW (ILG)
      COMMON /ROW/ DDOROW (ILG)
      COMMON /ROW/ DDSROW (ILG)
      COMMON /ROW/ WDLDROW(ILG)
      COMMON /ROW/ WDLBROW(ILG)
      COMMON /ROW/ WDLOROW(ILG)
      COMMON /ROW/ WDLSROW(ILG)
      COMMON /ROW/ WDDDROW(ILG)
      COMMON /ROW/ WDDBROW(ILG)
      COMMON /ROW/ WDDOROW(ILG)
      COMMON /ROW/ WDDSROW(ILG)
      COMMON /ROW/ WDSDROW(ILG)
      COMMON /ROW/ WDSBROW(ILG)
      COMMON /ROW/ WDSOROW(ILG)
      COMMON /ROW/ WDSSROW(ILG)
      COMMON /ROW/ DD6ROW (ILG)
      COMMON /ROW/ PHDROW (ILG,ILEV)
      COMMON /ROW/ PHLROW (ILG,ILEV)
      COMMON /ROW/ PHSROW (ILG,ILEV)
      COMMON /ROW/ SDHPROW(ILG)
      COMMON /ROW/ SDO3ROW(ILG)
      COMMON /ROW/ SLHPROW(ILG)
      COMMON /ROW/ SLO3ROW(ILG)
      COMMON /ROW/ SSHPROW(ILG)
      COMMON /ROW/ SSO3ROW(ILG)
      COMMON /ROW/ WDD6ROW(ILG)
      COMMON /ROW/ WDL6ROW(ILG)
      COMMON /ROW/ WDS6ROW(ILG)
#endif
#endif
#if (defined(pla) && defined(pam))
      COMMON /ROW/ SSLDROW (ILG,ILEV)
      COMMON /ROW/ RESSROW (ILG,ILEV)
      COMMON /ROW/ VESSROW (ILG,ILEV)
      COMMON /ROW/ DSLDROW (ILG,ILEV)
      COMMON /ROW/ REDSROW (ILG,ILEV)
      COMMON /ROW/ VEDSROW (ILG,ILEV)
      COMMON /ROW/ BCLDROW (ILG,ILEV)
      COMMON /ROW/ REBCROW (ILG,ILEV)
      COMMON /ROW/ VEBCROW (ILG,ILEV)
      COMMON /ROW/ OCLDROW (ILG,ILEV)
      COMMON /ROW/ REOCROW (ILG,ILEV)
      COMMON /ROW/ VEOCROW (ILG,ILEV)
      COMMON /ROW/ AMLDROW (ILG,ILEV)
      COMMON /ROW/ REAMROW (ILG,ILEV)
      COMMON /ROW/ VEAMROW (ILG,ILEV)
      COMMON /ROW/ FR1ROW  (ILG,ILEV)
      COMMON /ROW/ FR2ROW  (ILG,ILEV)
      COMMON /ROW/ ZCDNROW (ILG,ILEV)
      COMMON /ROW/ BCICROW (ILG,ILEV)
      COMMON /ROW/ OEDNROW (ILG,ILEV,KEXTT)
      COMMON /ROW/ OERCROW (ILG,ILEV,KEXTT)
      COMMON /ROW/ OIDNROW (ILG,ILEV)
      COMMON /ROW/ OIRCROW (ILG,ILEV)
      COMMON /ROW/ SVVBROW (ILG,ILEV)
      COMMON /ROW/ PSVVROW (ILG,ILEV)
      COMMON /ROW/ SVMBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ SVCBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ PNVBROW (ILG,ILEV,ISAINTT)
      COMMON /ROW/ PNMBROW (ILG,ILEV,ISAINTT,NRMFLD)
      COMMON /ROW/ PNCBROW (ILG,ILEV,ISAINTT,NRMFLD)
      COMMON /ROW/ PSVBROW (ILG,ILEV,ISAINTT,KINTT)
      COMMON /ROW/ PSMBROW (ILG,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /ROW/ PSCBROW (ILG,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /ROW/ QNVBROW (ILG,ILEV,ISAINTT)
      COMMON /ROW/ QNMBROW (ILG,ILEV,ISAINTT,NRMFLD)
      COMMON /ROW/ QNCBROW (ILG,ILEV,ISAINTT,NRMFLD)
      COMMON /ROW/ QSVBROW (ILG,ILEV,ISAINTT,KINTT)
      COMMON /ROW/ QSMBROW (ILG,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /ROW/ QSCBROW (ILG,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /ROW/ QGVBROW (ILG,ILEV)
      COMMON /ROW/ QGMBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ QGCBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ QDVBROW (ILG,ILEV)
      COMMON /ROW/ QDMBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ QDCBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ ONVBROW (ILG,ILEV,ISAINTT)
      COMMON /ROW/ ONMBROW (ILG,ILEV,ISAINTT,NRMFLD)
      COMMON /ROW/ ONCBROW (ILG,ILEV,ISAINTT,NRMFLD)
      COMMON /ROW/ OSVBROW (ILG,ILEV,ISAINTT,KINTT)
      COMMON /ROW/ OSMBROW (ILG,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /ROW/ OSCBROW (ILG,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /ROW/ OGVBROW (ILG,ILEV)
      COMMON /ROW/ OGMBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ OGCBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ DEVBROW (ILG,ILEV,KEXTT)
      COMMON /ROW/ PDEVROW (ILG,ILEV,KEXTT)
      COMMON /ROW/ DEMBROW (ILG,ILEV,KEXTT,NRMFLD)
      COMMON /ROW/ DECBROW (ILG,ILEV,KEXTT,NRMFLD)
      COMMON /ROW/ DIVBROW (ILG,ILEV)
      COMMON /ROW/ PDIVROW (ILG,ILEV)
      COMMON /ROW/ DIMBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ DICBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ REVBROW (ILG,ILEV,KEXTT)
      COMMON /ROW/ PREVROW (ILG,ILEV,KEXTT)
      COMMON /ROW/ REMBROW (ILG,ILEV,KEXTT,NRMFLD)
      COMMON /ROW/ RECBROW (ILG,ILEV,KEXTT,NRMFLD)
      COMMON /ROW/ RIVBROW (ILG,ILEV)
      COMMON /ROW/ PRIVROW (ILG,ILEV)
      COMMON /ROW/ RIMBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ RICBROW (ILG,ILEV,NRMFLD)
      COMMON /ROW/ SULIROW (ILG,ILEV)
      COMMON /ROW/ RSUIROW (ILG,ILEV)
      COMMON /ROW/ VSUIROW (ILG,ILEV)
      COMMON /ROW/ F1SUROW (ILG,ILEV)
      COMMON /ROW/ F2SUROW (ILG,ILEV)
      COMMON /ROW/ BCLIROW (ILG,ILEV)
      COMMON /ROW/ RBCIROW (ILG,ILEV)
      COMMON /ROW/ VBCIROW (ILG,ILEV)
      COMMON /ROW/ F1BCROW (ILG,ILEV)
      COMMON /ROW/ F2BCROW (ILG,ILEV)
      COMMON /ROW/ OCLIROW (ILG,ILEV)
      COMMON /ROW/ ROCIROW (ILG,ILEV)
      COMMON /ROW/ VOCIROW (ILG,ILEV)
      COMMON /ROW/ F1OCROW (ILG,ILEV)
      COMMON /ROW/ F2OCROW (ILG,ILEV)
#if defined (xtrapla1)
      COMMON /ROW/ SNCNROW (ILG,ILEV)
      COMMON /ROW/ SSUNROW (ILG,ILEV)
      COMMON /ROW/ SCNDROW (ILG,ILEV)
      COMMON /ROW/ SGSPROW (ILG,ILEV)
      COMMON /ROW/ CCNROW  (ILG,ILEV)
      COMMON /ROW/ CC02ROW (ILG,ILEV)
      COMMON /ROW/ CC04ROW (ILG,ILEV)
      COMMON /ROW/ CC08ROW (ILG,ILEV)
      COMMON /ROW/ CC16ROW (ILG,ILEV)
      COMMON /ROW/ CCNEROW (ILG,ILEV)
      COMMON /ROW/ ACASROW (ILG,ILEV)
      COMMON /ROW/ ACOAROW (ILG,ILEV)
      COMMON /ROW/ ACBCROW (ILG,ILEV)
      COMMON /ROW/ ACSSROW (ILG,ILEV)
      COMMON /ROW/ ACMDROW (ILG,ILEV)
      COMMON /ROW/ NTROW   (ILG,ILEV)
      COMMON /ROW/ N20ROW  (ILG,ILEV)
      COMMON /ROW/ N50ROW  (ILG,ILEV)
      COMMON /ROW/ N100ROW (ILG,ILEV)
      COMMON /ROW/ N200ROW (ILG,ILEV)
      COMMON /ROW/ WTROW   (ILG,ILEV)
      COMMON /ROW/ W20ROW  (ILG,ILEV)
      COMMON /ROW/ W50ROW  (ILG,ILEV)
      COMMON /ROW/ W100ROW (ILG,ILEV)
      COMMON /ROW/ W200ROW (ILG,ILEV)
      COMMON /ROW/ RCRIROW (ILG,ILEV)
      COMMON /ROW/ SUPSROW (ILG,ILEV)
      COMMON /ROW/ HENRROW (ILG,ILEV)
      COMMON /ROW/ O3FRROW (ILG,ILEV)
      COMMON /ROW/ H2O2FRROW(ILG,ILEV)
      COMMON /ROW/ WPARROW (ILG,ILEV)
      COMMON /ROW/ PM25ROW (ILG,ILEV)
      COMMON /ROW/ PM10ROW (ILG,ILEV)
      COMMON /ROW/ DM25ROW (ILG,ILEV)
      COMMON /ROW/ DM10ROW (ILG,ILEV)
      COMMON /ROW/ VNCNROW (ILG)
      COMMON /ROW/ VASNROW (ILG)
      COMMON /ROW/ VSCDROW (ILG)
      COMMON /ROW/ VGSPROW (ILG)
      COMMON /ROW/ VOAEROW (ILG)
      COMMON /ROW/ VBCEROW (ILG)
      COMMON /ROW/ VASEROW (ILG)
      COMMON /ROW/ VMDEROW (ILG)
      COMMON /ROW/ VSSEROW (ILG)
      COMMON /ROW/ VOAWROW (ILG)
      COMMON /ROW/ VBCWROW (ILG)
      COMMON /ROW/ VASWROW (ILG)
      COMMON /ROW/ VMDWROW (ILG)
      COMMON /ROW/ VSSWROW (ILG)
      COMMON /ROW/ VOADROW (ILG)
      COMMON /ROW/ VBCDROW (ILG)
      COMMON /ROW/ VASDROW (ILG)
      COMMON /ROW/ VMDDROW (ILG)
      COMMON /ROW/ VSSDROW (ILG)
      COMMON /ROW/ VOAGROW (ILG)
      COMMON /ROW/ VBCGROW (ILG)
      COMMON /ROW/ VASGROW (ILG)
      COMMON /ROW/ VMDGROW (ILG)
      COMMON /ROW/ VSSGROW (ILG)
      COMMON /ROW/ VOACROW (ILG)
      COMMON /ROW/ VBCCROW (ILG)
      COMMON /ROW/ VASCROW (ILG)
      COMMON /ROW/ VMDCROW (ILG)
      COMMON /ROW/ VSSCROW (ILG)
      COMMON /ROW/ VASIROW (ILG)
      COMMON /ROW/ VAS1ROW (ILG)
      COMMON /ROW/ VAS2ROW (ILG)
      COMMON /ROW/ VAS3ROW (ILG)
      COMMON /ROW/ VCCNROW (ILG)
      COMMON /ROW/ VCNEROW (ILG)
#endif
#if defined (xtrapla2)
      COMMON /ROW/ CORNROW (ILG,ILEV)
      COMMON /ROW/ CORMROW (ILG,ILEV)
      COMMON /ROW/ RSN1ROW (ILG,ILEV)
      COMMON /ROW/ RSM1ROW (ILG,ILEV)
      COMMON /ROW/ RSN2ROW (ILG,ILEV)
      COMMON /ROW/ RSM2ROW (ILG,ILEV)
      COMMON /ROW/ RSN3ROW (ILG,ILEV)
      COMMON /ROW/ RSM3ROW (ILG,ILEV)
      COMMON /ROW/ SDNUROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SDMAROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SDACROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SDCOROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SSSNROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SMDNROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SIANROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SSSMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SMDMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SEWMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SSUMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SOCMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SBCMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SIWMROW (ILG,ILEV,ISDNUM)
      COMMON /ROW/ SDVLROW (ILG,ISDIAG)
      COMMON /ROW/ VRN1ROW (ILG)
      COMMON /ROW/ VRM1ROW (ILG)
      COMMON /ROW/ VRN2ROW (ILG)
      COMMON /ROW/ VRM2ROW (ILG)
      COMMON /ROW/ VRN3ROW (ILG)
      COMMON /ROW/ VRM3ROW (ILG)
      COMMON /ROW/ DEFAROW(ILG)
      COMMON /ROW/ DEFCROW(ILG)
      COMMON /ROW/ DMACROW(ILG,ISDUST)
      COMMON /ROW/ DMCOROW(ILG,ISDUST)
      COMMON /ROW/ DNACROW(ILG,ISDUST)
      COMMON /ROW/ DNCOROW(ILG,ISDUST)
      COMMON /ROW/ DEFXROW(ILG,ISDIAG)
      COMMON /ROW/ DEFNROW(ILG,ISDIAG)
      COMMON /ROW/ TNSSROW(ILG)           ! total number for sea salt
      COMMON /ROW/ TNMDROW(ILG)           ! total number for mineral dust
      COMMON /ROW/ TNIAROW(ILG)           ! total number for internally mixed aerosol
      COMMON /ROW/ TMSSROW(ILG)           ! total number for sea salt
      COMMON /ROW/ TMMDROW(ILG)           ! total mass for mineral dust
      COMMON /ROW/ TMOCROW(ILG)           ! total mass for organic carbon
      COMMON /ROW/ TMBCROW(ILG)           ! total mass for black carbon
      COMMON /ROW/ TMSPROW(ILG)           ! total mass for annonia sulphate
      COMMON /ROW/ SNSSROW(ILG,ISDNUM)    ! number size distribution for sea salt
      COMMON /ROW/ SNMDROW(ILG,ISDNUM)    ! number size distribution for mineral
      COMMON /ROW/ SNIAROW(ILG,ISDNUM)    ! number size distribution for internally mixed aerosol
      COMMON /ROW/ SMSSROW(ILG,ISDNUM)    ! mass size distribution for sea salt
      COMMON /ROW/ SMMDROW(ILG,ISDNUM)    ! mass size distribution for mineral dust
      COMMON /ROW/ SMOCROW(ILG,ISDNUM)    ! mass size distribution for organic carbon
      COMMON /ROW/ SMBCROW(ILG,ISDNUM)    ! mass size distribution for black carbon
      COMMON /ROW/ SMSPROW(ILG,ISDNUM)    ! mass size distribution for annonia sulphate
      COMMON /ROW/ SIWHROW(ILG,ISDNUM)    ! water for internally mixed aerosols
      COMMON /ROW/ SEWHROW(ILG,ISDNUM)    ! water for externally mixed aerosols
#endif
#endif
#if defined xtrals
      REAL MLTIROW,MLTSROW
      COMMON /ROW/ AGGROW (ILG,ILEV)
      COMMON /ROW/ AUTROW (ILG,ILEV)
      COMMON /ROW/ CNDROW (ILG,ILEV)
      COMMON /ROW/ DEPROW (ILG,ILEV)
      COMMON /ROW/ EVPROW (ILG,ILEV)
      COMMON /ROW/ FRHROW (ILG,ILEV)
      COMMON /ROW/ FRKROW (ILG,ILEV)
      COMMON /ROW/ FRSROW (ILG,ILEV)
      COMMON /ROW/ MLTIROW(ILG,ILEV)
      COMMON /ROW/ MLTSROW(ILG,ILEV)
      COMMON /ROW/ RACLROW(ILG,ILEV)
      COMMON /ROW/ RAINROW(ILG,ILEV)
      COMMON /ROW/ SACIROW(ILG,ILEV)
      COMMON /ROW/ SACLROW(ILG,ILEV)
      COMMON /ROW/ SNOWROW(ILG,ILEV)
      COMMON /ROW/ SUBROW (ILG,ILEV)
      COMMON /ROW/ SEDIROW (ILG,ILEV)
      COMMON /ROW/ RLIQROW(ILG,ILEV)
      COMMON /ROW/ RICEROW(ILG,ILEV)
      COMMON /ROW/ RLNCROW(ILG,ILEV)
      COMMON /ROW/ CLIQROW(ILG,ILEV)
      COMMON /ROW/ CICEROW(ILG,ILEV)
      COMMON /ROW/ RLIQROL(ILG,ILEV)
      COMMON /ROW/ RICEROL(ILG,ILEV)
      COMMON /ROW/ RLNCROL(ILG,ILEV)
      COMMON /ROW/ CLIQROL(ILG,ILEV)
      COMMON /ROW/ CICEROL(ILG,ILEV)
C
      COMMON /ROW/ VAGGROW(ILG)
      COMMON /ROW/ VAUTROW(ILG)
      COMMON /ROW/ VCNDROW(ILG)
      COMMON /ROW/ VDEPROW(ILG)
      COMMON /ROW/ VEVPROW(ILG)
      COMMON /ROW/ VFRHROW(ILG)
      COMMON /ROW/ VFRKROW(ILG)
      COMMON /ROW/ VFRSROW(ILG)
      COMMON /ROW/ VMLIROW(ILG)
      COMMON /ROW/ VMLSROW(ILG)
      COMMON /ROW/ VRCLROW(ILG)
      COMMON /ROW/ VSCIROW(ILG)
      COMMON /ROW/ VSCLROW(ILG)
      COMMON /ROW/ VSUBROW(ILG)
      COMMON /ROW/ VSEDIROW(ILG)
      COMMON /ROW/ RELIQROW(ILG)
      COMMON /ROW/ REICEROW(ILG)
      COMMON /ROW/ CLDLIQROW(ILG)
      COMMON /ROW/ CLDICEROW(ILG)
      COMMON /ROW/ CTLNCROW(ILG)
      COMMON /ROW/ CLLNCROW(ILG)
      COMMON /ROW/ RELIQROL(ILG)
      COMMON /ROW/ REICEROL(ILG)
      COMMON /ROW/ CLDLIQROL(ILG)
      COMMON /ROW/ CLDICEROL(ILG)
      COMMON /ROW/ CTLNCROL(ILG)
      COMMON /ROW/ CLLNCROL(ILG)

#endif
#if defined (aodpth)
C
      COMMON /ROW/ EXB1ROW(ILG)
      COMMON /ROW/ EXB2ROW(ILG)
      COMMON /ROW/ EXB3ROW(ILG)
      COMMON /ROW/ EXB4ROW(ILG)
      COMMON /ROW/ EXB5ROW(ILG)
      COMMON /ROW/ EXBTROW(ILG)
      COMMON /ROW/ ODB1ROW(ILG)
      COMMON /ROW/ ODB2ROW(ILG)
      COMMON /ROW/ ODB3ROW(ILG)
      COMMON /ROW/ ODB4ROW(ILG)
      COMMON /ROW/ ODB5ROW(ILG)
      COMMON /ROW/ ODBTROW(ILG)
      COMMON /ROW/ ODBVROW(ILG)
      COMMON /ROW/ OFB1ROW(ILG)
      COMMON /ROW/ OFB2ROW(ILG)
      COMMON /ROW/ OFB3ROW(ILG)
      COMMON /ROW/ OFB4ROW(ILG)
      COMMON /ROW/ OFB5ROW(ILG)
      COMMON /ROW/ OFBTROW(ILG)
      COMMON /ROW/ ABB1ROW(ILG)
      COMMON /ROW/ ABB2ROW(ILG)
      COMMON /ROW/ ABB3ROW(ILG)
      COMMON /ROW/ ABB4ROW(ILG)
      COMMON /ROW/ ABB5ROW(ILG)
      COMMON /ROW/ ABBTROW(ILG)
C
      COMMON /ROW/ EXB1ROL(ILG)
      COMMON /ROW/ EXB2ROL(ILG)
      COMMON /ROW/ EXB3ROL(ILG)
      COMMON /ROW/ EXB4ROL(ILG)
      COMMON /ROW/ EXB5ROL(ILG)
      COMMON /ROW/ EXBTROL(ILG)
      COMMON /ROW/ ODB1ROL(ILG)
      COMMON /ROW/ ODB2ROL(ILG)
      COMMON /ROW/ ODB3ROL(ILG)
      COMMON /ROW/ ODB4ROL(ILG)
      COMMON /ROW/ ODB5ROL(ILG)
      COMMON /ROW/ ODBTROL(ILG)
      COMMON /ROW/ ODBVROL(ILG)
      COMMON /ROW/ OFB1ROL(ILG)
      COMMON /ROW/ OFB2ROL(ILG)
      COMMON /ROW/ OFB3ROL(ILG)
      COMMON /ROW/ OFB4ROL(ILG)
      COMMON /ROW/ OFB5ROL(ILG)
      COMMON /ROW/ OFBTROL(ILG)
      COMMON /ROW/ ABB1ROL(ILG)
      COMMON /ROW/ ABB2ROL(ILG)
      COMMON /ROW/ ABB3ROL(ILG)
      COMMON /ROW/ ABB4ROL(ILG)
      COMMON /ROW/ ABB5ROL(ILG)
      COMMON /ROW/ ABBTROL(ILG)
C
      COMMON /ROW/ EXS1ROW(ILG)
      COMMON /ROW/ EXS2ROW(ILG)
      COMMON /ROW/ EXS3ROW(ILG)
      COMMON /ROW/ EXS4ROW(ILG)
      COMMON /ROW/ EXS5ROW(ILG)
      COMMON /ROW/ EXSTROW(ILG)
      COMMON /ROW/ ODS1ROW(ILG)
      COMMON /ROW/ ODS2ROW(ILG)
      COMMON /ROW/ ODS3ROW(ILG)
      COMMON /ROW/ ODS4ROW(ILG)
      COMMON /ROW/ ODS5ROW(ILG)
      COMMON /ROW/ ODSTROW(ILG)
      COMMON /ROW/ ODSVROW(ILG)
      COMMON /ROW/ OFS1ROW(ILG)
      COMMON /ROW/ OFS2ROW(ILG)
      COMMON /ROW/ OFS3ROW(ILG)
      COMMON /ROW/ OFS4ROW(ILG)
      COMMON /ROW/ OFS5ROW(ILG)
      COMMON /ROW/ OFSTROW(ILG)
      COMMON /ROW/ ABS1ROW(ILG)
      COMMON /ROW/ ABS2ROW(ILG)
      COMMON /ROW/ ABS3ROW(ILG)
      COMMON /ROW/ ABS4ROW(ILG)
      COMMON /ROW/ ABS5ROW(ILG)
      COMMON /ROW/ ABSTROW(ILG)
C
      COMMON /ROW/ EXS1ROL(ILG)
      COMMON /ROW/ EXS2ROL(ILG)
      COMMON /ROW/ EXS3ROL(ILG)
      COMMON /ROW/ EXS4ROL(ILG)
      COMMON /ROW/ EXS5ROL(ILG)
      COMMON /ROW/ EXSTROL(ILG)
      COMMON /ROW/ ODS1ROL(ILG)
      COMMON /ROW/ ODS2ROL(ILG)
      COMMON /ROW/ ODS3ROL(ILG)
      COMMON /ROW/ ODS4ROL(ILG)
      COMMON /ROW/ ODS5ROL(ILG)
      COMMON /ROW/ ODSTROL(ILG)
      COMMON /ROW/ ODSVROL(ILG)
      COMMON /ROW/ OFS1ROL(ILG)
      COMMON /ROW/ OFS2ROL(ILG)
      COMMON /ROW/ OFS3ROL(ILG)
      COMMON /ROW/ OFS4ROL(ILG)
      COMMON /ROW/ OFS5ROL(ILG)
      COMMON /ROW/ OFSTROL(ILG)
      COMMON /ROW/ ABS1ROL(ILG)
      COMMON /ROW/ ABS2ROL(ILG)
      COMMON /ROW/ ABS3ROL(ILG)
      COMMON /ROW/ ABS4ROL(ILG)
      COMMON /ROW/ ABS5ROL(ILG)
      COMMON /ROW/ ABSTROL(ILG)
C
#endif
#if defined use_cosp
! COSP INPUT
      COMMON /ROW/ RMIXROW(ILG,ILEV) ! RAIN MIXING RATIO
      COMMON /ROW/ SMIXROW(ILG,ILEV) ! SNOW MIXING RATIO
      COMMON /ROW/ RREFROW(ILG,ILEV) ! RAIN EFFECTIVE PARTICLE RADIUS
      COMMON /ROW/ SREFROW(ILG,ILEV) ! SNOW EFFECTIVE PARTICLE RADIUS
#endif
#if defined (xtradust)
      COMMON /ROW/ DUWDROW(ILG)
      COMMON /ROW/ DUSTROW(ILG)
      COMMON /ROW/ DUTHROW(ILG)
      COMMON /ROW/ USMKROW(ILG)
      COMMON /ROW/ FALLROW(ILG)
      COMMON /ROW/ FA10ROW(ILG)
      COMMON /ROW/ FA2ROW(ILG)
      COMMON /ROW/ FA1ROW(ILG)
      COMMON /ROW/ GUSTROW(ILG)
      COMMON /ROW/ ZSPDROW(ILG)
      COMMON /ROW/ VGFRROW(ILG)
      COMMON /ROW/ SMFRROW(ILG)
#endif
#if defined x01
      COMMON /ROW/ X01ROW (ILG,ILEV,NTRAC)
#endif
#if defined x02
      COMMON /ROW/ X02ROW (ILG,ILEV,NTRAC)
#endif
#if defined x03
      COMMON /ROW/ X03ROW (ILG,ILEV,NTRAC)
#endif
#if defined x04
      COMMON /ROW/ X04ROW (ILG,ILEV,NTRAC)
#endif
#if defined x05
      COMMON /ROW/ X05ROW (ILG,ILEV,NTRAC)
#endif
#if defined x06
      COMMON /ROW/ X06ROW (ILG,ILEV,NTRAC)
#endif
#if defined x07
      COMMON /ROW/ X07ROW (ILG,ILEV,NTRAC)
#endif
#if defined x08
      COMMON /ROW/ X08ROW (ILG,ILEV,NTRAC)
#endif
#if defined x09
      COMMON /ROW/ X09ROW (ILG,ILEV,NTRAC)
#endif
!$OMP THREADPRIVATE (/ROW/)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
