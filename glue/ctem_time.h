!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * MAY 05/2015 - M.Lazare.

C     * Define some timekeeping variable types for inter-communication
C     * between CTEM routines in AGCM driver and those inside the
C     * surfaces processes driver.

      real c_clim_time(3,1:2)
      integer month,lndcvryr1,lndcvryr2
C
      COMMON /CTEMT/ month,lndcvryr1,lndcvryr2,c_clim_time
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
