!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      MODULE SOLVAR2
        INTEGER, PARAMETER :: SV_NTS_MAX=5414
        INTEGER, PARAMETER :: SV_SUBANDS=12
        REAL, DIMENSION(SV_SUBANDS,SV_NTS_MAX) :: SOLVTS
        REAL*8, DIMENSION(SV_NTS_MAX) :: SOLVTIMES
        REAL*8 :: SV_CURR_TIME
        REAL, SAVE, DIMENSION(SV_SUBANDS) :: SOLV=0.0
        REAL, DIMENSION(SV_NTS_MAX) :: SV_WRK
        INTEGER, SAVE :: SV_START_YEAR, SV_STOP_YEAR
        INTEGER, SAVE :: SV_START_MONTH, SV_STOP_MONTH
        INTEGER, SAVE :: SV_NTS
        INTEGER, SAVE :: SV_IBUF2, SV_IBUF7
        REAL :: SV_DAYS
        CHARACTER*4 :: SV_IB3
        LOGICAL :: FOUND
        REAL, PARAMETER, DIMENSION(12) :: SV_MM_DAYS =
     &  (/ 15.5,  45.0,  74.5, 105.0, 135.5, 166.0,
     &    196.5, 227.5, 258.0, 288.5, 319.0, 349.5/)
      END MODULE SOLVAR2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
