#PBS -N CanAM_CI
#PBS -l select=6:ncpus=40:vntype=cray_compute
#PBS -l walltime=0:30:00

export OMP_NUM_THREADS=2
export F_UFMTENDIAN=big
export OMP_STACKSIZE=1G
export FORT_BUFFERED=TRUE
export BUFFERED=yes
export KMP_AFFINITY=disabled
export PMI_NO_FORK=1
export PMI_CONNECT_RETRIES=100
export MPICH_GNI_MAX_EAGER_MSG_SIZE=13107  

echo "CONFIG_DIR: $CONFIG_DIR"
echo "Testing: $CONFIGURATIONS"

for config in $CONFIGURATIONS
do
  base_config=$( echo $config | cut -d'.' -f1 )
  cd $CONFIG_DIR/$base_config/run_directory
  cp executables/AGCM.$config ./GCM
  cp executables/CPL.$config ./cpl_main

  rm -f forcing_directory restart_directory
  ln -s /fs/xc3lustre/dev/eccc/crd/ccrn/users/ras003/canesm_ci_aux/configuration/spbc-agcm restart_directory
  ln -s /fs/xc3lustre/dev/eccc/crd/ccrn/forcing/ forcing_directory

  ./prunsc &> run.log
  if [ $? -ne 0 ]; then
    cat run.log
    exit -1
  fi
  md5sum NEWRS NEWTS > ../answers/$config
done