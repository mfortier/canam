!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PUTSPTND(NF ,PS,P,C,T,ES,
     1        REF_PS,REF_P,REF_C,REF_T,REF_ES,
     2                   KOUNT,LA,LRLMT,ILEV,LEVS,LS,LH,
     3                   LSRTOTAL,LATOTAL,LMTOTAL,GLL)

C     * NOV 04/03 - M.LAZARE. NEW VERSION TO SUPPORT MPI, WHICH USES
C     *                       NEW ROUTINE "PUTSPN" AND WORK ARRAY "GLL".
C     * MAY 29/95 - M.LAZARE. PREVIOUS VERSION PUTSTG8.
C
C     * SAVES GLOBAL SPECTRAL FORECAST ON SEQUENTIAL FILE NF.
C     * ALL FIELDS ARE WRITTEN UNPACKED.
C     * LS,LH = OUTPUT LABEL VALUES FOR FULL,HALF LEVELS.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX PS(LA)            !<Variable description\f$[units]\f$
      COMPLEX T(LA,ILEV)        !<Variable description\f$[units]\f$
      COMPLEX P(LA,ILEV)        !<Variable description\f$[units]\f$
      COMPLEX C(LA,ILEV)        !<Variable description\f$[units]\f$
      COMPLEX ES(LA,LEVS)       !<Variable description\f$[units]\f$
      COMPLEX REF_PS(LA)        !<Variable description\f$[units]\f$
      COMPLEX REF_T(LA,ILEV)    !<Variable description\f$[units]\f$
      COMPLEX REF_P(LA,ILEV)    !<Variable description\f$[units]\f$
      COMPLEX REF_C(LA,ILEV)    !<Variable description\f$[units]\f$
      COMPLEX REF_ES(LA,LEVS)   !<Variable description\f$[units]\f$
      COMPLEX GLL(LATOTAL)      !<Variable description\f$[units]\f$

      INTEGER LSRTOTAL(2,LMTOTAL+1)     !<Variable description\f$[units]\f$
      INTEGER LS(ILEV)          !<Variable description\f$[units]\f$
      INTEGER LH(ILEV)          !<Variable description\f$[units]\f$
      INTEGER IBUF(8)           !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      LOGICAL OK

      COMMON /IPARIO/  IPIO
      COMMON /KEEPTIM/ IYEAR,IMDH,MYRSSTI,ISAVDTS
C--------------------------------------------------------------------
      MAX=2*LATOTAL
C
      ILNSP=NC4TO8("LSPN")
       ITMP=NC4TO8("TMPN")
      IVORT=NC4TO8("VORN")
       IDIV=NC4TO8("DIVN")
        IES=NC4TO8(" ESN")
      ILNSPR=NC4TO8("LSPR")
       ITMPR=NC4TO8("TMPR")
      IVORTR=NC4TO8("VORR")
       IDIVR=NC4TO8("DIVR")
        IESR=NC4TO8(" ESR")

C     * DETERMINE PROPER IBUF(2) TO USE FOR SAVED FIELDS, BASED ON
C     * VALUE OF OPTION SWITCH "ISAVDTS".
C
      IF(ISAVDTS.NE.0)                   THEN
C        * IN 32-BIT, THIS ONLY WORKS UNTIL IYEAR=2147!
         IBUF2=1000000*IYEAR + IMDH
      ELSE
         IBUF2=KOUNT
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("SPEC"),IBUF2,-1,1,-1,1,LRLMT,0)
C
C     * SURFACE GEOPOTENTIAL (MOUNTAINS) ARE SAVED EVERY TIME.
C
cccc      IBUF(3)=IPHIS
cccc      CALL PUTSPN (NF,PHIS,LA,
cccc     1             LSRTOTAL,LATOTAL,LMTOTAL,GLL,
cccc     2             IBUF,MAX,IPIO,OK)
C
C     * SAVE LN(PS) IN PASCALS.
C
      IBUF(3)=ILNSP
      CALL PUTSPN (NF,PS,LA,
     1             LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2             IBUF,MAX,IPIO,OK)
C
C     * SAVE TEMPERATURE FOR ILEV LEVELS.
C
      IBUF(3)=ITMP
      DO 310 L=1,ILEV
         IBUF(4)=LH(L)
         CALL PUTSPN (NF,T(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  310 CONTINUE
C
C     * SAVE VORTICITY AND DIVERGENCE IN PAIRS FOR EACH LEVEL.
C
      DO 410 L=1,ILEV
         IBUF(4)=LS(L)

         IBUF(3)=IVORT
         CALL PUTSPN (NF,P(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)

         IBUF(3)=IDIV
         CALL PUTSPN (NF,C(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  410 CONTINUE
C
C     * MOISTURE VARIABLE SAVED FOR LEVS LEVELS.
C
      IF(LEVS.EQ.0) RETURN
      IBUF(3)=IES
      DO 510 N=1,LEVS
         L=(ILEV-LEVS)+N
         IBUF(4)=LH(L)
         CALL PUTSPN (NF,ES(1,N),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  510 CONTINUE
C
C     * SAVE LN(PS) IN PASCALS.
C
      IBUF(3)=ILNSPR
      CALL PUTSPN (NF,REF_PS,LA,
     1             LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2             IBUF,MAX,IPIO,OK)
C
C     * SAVE TEMPERATURE FOR ILEV LEVELS.
C
      IBUF(3)=ITMPR
      DO 315 L=1,ILEV
         IBUF(4)=LH(L)
         CALL PUTSPN (NF,REF_T(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  315 CONTINUE
C
C     * SAVE VORTICITY AND DIVERGENCE IN PAIRS FOR EACH LEVEL.
C
      DO 415 L=1,ILEV
         IBUF(4)=LS(L)

         IBUF(3)=IVORTR
         CALL PUTSPN (NF,REF_P(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)

         IBUF(3)=IDIVR
         CALL PUTSPN (NF,REF_C(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  415 CONTINUE
C
C     * MOISTURE VARIABLE SAVED FOR LEVS LEVELS.
C
      IF(LEVS.EQ.0) RETURN
      IBUF(3)=IESR
      DO 515 N=1,LEVS
         L=(ILEV-LEVS)+N
         IBUF(4)=LH(L)
         CALL PUTSPN (NF,REF_ES(1,N),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  515 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
