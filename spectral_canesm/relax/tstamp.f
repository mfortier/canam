!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      subroutine tstamp(kount,delt,iyr,imn,idy,ihr,iymdh)

      integer iymdh     !<Variable description\f$[units]\f$
      integer kount     !<Variable description\f$[units]\f$
      integer iyearst
      integer iyr       !<Variable description\f$[units]\f$
      integer imn       !<Variable description\f$[units]\f$
      integer idy       !<Variable description\f$[units]\f$
      integer ihr       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      integer kyears,kmonths,kdays,khours
      integer month(365),mthbnd(12)

      data month/31*1,28*2,31*3,30*4,31*5,30*6,31*7,31*8,30*9,
     1     31*10,30*11,31*12/
      data mthbnd/0,31,59,90,120,151,181,212,243,273,304,334/

      integer daysec,yearsec,idelt
      data daysec/86400/

      idelt=nint(delt)
      itspy=365*daysec/idelt
      itspd=daysec/idelt
      itsph=3600/idelt

      kyears=kount/itspy
      kdays = (kount-kyears*itspy)/itspd
      khours= (kount-kyears*itspy-kdays*itspd)/itsph

      iyr=kyears+1
      imn=month(kdays+1)
      idy=kdays-mthbnd(imn)+1
      ihr=khours

c     * in 32-bit this works only until year 2147
      iymdh=iyr*1000000+imn*10000+idy*100+ihr


      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
