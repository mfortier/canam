!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE ALPST3X(ALP,LSR,LM,SINLAT,EPSI,NLAT,IRAM) 
C 
C     * OCT 29/03 - M. LAZARE. REVERSE ORDER OF POLYNOMIALS
C     *                        AND DO OVER ALL LATITUDES.
C     * OCT 15/92 - PREVIOUS VERSION ALPST3.
C     * JUL 14/92 - PREVIOUS VERSION ALPST2.
C 
C     * PUTS LEGENDRE POLYNOMIALS IN ALP FOR ONE LATITUDE.
C     * SINLAT IS THE SINE OF THE LATITUDE(S). 
C     * EPSI IS A FIELD OF PRECOMPUTED CONSTANTS. 
C     * LSR CONTAINS ROW LENGTH INFO FOR ALP,EPSI.
C 
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSR (2,LM+1) 
      REAL*8 ALP(NLAT,IRAM)     !<Variable description\f$[units]\f$
      REAL*8 EPSI(IRAM)         !<Variable description\f$[units]\f$
      REAL*8 SINLAT(NLAT)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
      DO 300 NJ=1,NLAT
        COS2=1.-SINLAT(NJ)**2 
        PROD=1. 
        A=1.
        B=0.
C 
C       * LOOP OVER LONGITUDINAL WAVE NUMBERS.
C 
        DO 230 M=1,LM 
          FM=FLOAT(M-1) 
          IF(M.EQ.1) GO TO 210
          A=A+2.
          B=B+2.
          PROD=PROD*COS2*A/B
C 
C         * COMPUTE THE FIRST VALUE IN THE ROW. 
C         * COMPUTE THE SECOND ELEMENT ONLY IF NEEDED.
C 
  210     KL=LSR(2,M) 
          KR=LSR(2,M+1)-1 
          ALP(NJ,KL)=SQRT(.5*PROD) 
          IF(KR.GT.KL) ALP(NJ,KL+1)=SQRT(2.*FM+3.)*SINLAT(NJ)*ALP(NJ,KL)
C 
C         * COMPUTE THE REST OF THE VALUES IN THE ROW IF NEEDED.
C 
          KL2=KL+2
          IF(KL2.GT.KR) GO TO 230 
          DO 220 K=KL2,KR 
            ALP(NJ,K)=(SINLAT(NJ)*ALP(NJ,K-1)-EPSI(K-1)*ALP(NJ,K-2))/
     1                EPSI(K) 
  220     CONTINUE
  230   CONTINUE
  300 CONTINUE
C    
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
