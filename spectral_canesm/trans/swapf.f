!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SWAPF(SPEC1,SPEC2,
     1                 PS,P,C,T,ES,TRAC,FVORT,
     2                 LA,ILEV,LEVS,NTRAC,WRKT,HOSKF)
C
C     * DEC 10/2004 - J.SCINOCCA. CALLS REVISED ROUTINE SWAPS2F.
C     *                           USES HOSKINS FILTER.
C     * NOV 05/2003 - M.LAZARE.   PREVIOUS VERSION SWAP. 
C
C     * SWAPS ORDER OF SPECTRAL FIELDS IN PREPARATION FOR TRANSFORMS.
C     * THIS IS DONE OUTSIDE OF MAIN PARALLEL LOOPS SINCE IT OPERATES
C     * ON SPECTRAL FIELDS, THUS AVOIDING MEMORY BANK CONFLICTS FROM
C     * PARALLEL TASKS ACCESSING SAME MEMORY LOCATIONS.
C
C     * ALGORITHM
C     * ---------
C     *
C     * ON ENTRY TO THIS ROUTINE, THE COMPLEX FIELDS TO BE TRANSFORMED ARE
C     * NOT ORGANIZED FOR OPTIMAL PERFORMANCE.  THE INDIVIDUAL TRANSFORMS
C     * ARE ORGANIZED IN COLUMN-MAJOR FORMAT.  SINCE VECTORIZATION IN THE
C     * LEGENDRE AND FOURIER TRANSFORMS IS ACROSS PARALLEL TRANSFORMS, IT'S
C     * DESIRABLE TO TRANSPOSE THE "SPEC"  ARRAYS SO THAT THE
C     * TRANSFORMS ARE IN ROW MAJOR FORMAT AND VECTORIZATION IS ACROSS
C     * INDEPENDENT LEVELS, LATITUDES, AND VARIABLES WITH UNIT-STRIDES
C     * BETWEEN VECTOR ELEMENTS FOR BOTH THE LEGENDRE AND FOURIER TRANSFORMS.
C     *
C     * INPUT FIELDS  
C     * ------------
C     * THE COMPLEX SPECTRAL FIELDS TO BE TRANSFORMED ARE ALIGNED AS FOLLOWS:
C     *
C     *     PS (LA)
C     *      P (LA,ILEV)
C     *      C (LA,ILEV)
C     *      T (LA,ILEV)
C     *     ES (LA,LEVS)
C     *   TRAC (LA,ILEV,NTRAC)      if ITRAC .ne. 0
C     *
C     * THE ABOVE FIELDS ARE COMPLEX AND CONTIGUOUS IN MEMORY.  THEY
C     * ARE THEREFORE EQUIVALENT TO A SINGLE REAL ARRAY DIMENSIONED AS
C     *
C     *   SPEC1( 2, LA, 1+3*ILEV+LEVS+NTRAC*ILEV )
C     *
C     * HOWEVER, THE SPEC1 ARRAY IS NOT USED IN THIS FORM IN THE 
C     * TRANSFORMS.  RATHER, IT IS REFERENCED AS A TRANSPOSED ARRAY:
C     *
C     *   SPEC1( 2, 1+3*ILEV+LEVS+NTRAC*ILEV, LA)
C     *
C     * THIS STORAGE CONVENTION ALLOWS UNIT-STRIDE REFERENCES TO THE "SPEC1"
C     * ARRAY FOR BETTER PERFORMANCE.  
C     *
C     * THE P AND C ARRAYS MUST ALSO BE COPIED INTO A SEPARATE ARRAY OF
C     * SPECTRAL COEFFICIENTS CALLED
C     *
C     *   SPEC2( 2, LA, 2*ILEV)
C     *
C     * AS BEFORE, THESE ARRAYS ARE ACTUALLY REFERENCED IN THESE AND LOWER
C     * ROUTINES AS THE TRANSPOSED ARRAY:
C     *
C     *   SPEC2( 2, 2*ILEV, LA)
C     *
C     * FOR VECTORIZATION REASONS.  THE "SPEC1" AND "SPEC2" ARRAYS ARE
C     * THEREFORE SWAPPED INTO IN THEIR TRANSPOSED FORMATS IN THIS ROUTINE.
C
C     * NOTE ALSO THE INCLUSION OF THE EARTH'S ROTATION TO CONVERT TO
C     * ABSOLUTE VORTICITY.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
C     * OUTPUT ARRAYS:
C
      real spec1(2, 1+3*ilev+levs+ntrac*ilev, la)       !<Variable description\f$[units]\f$
      real spec2(2, 2*ilev,                   la)       !<Variable description\f$[units]\f$
C
C     * INPUT SPECTRAL ARRAYS:
C
      complex p(la,ilev)        !<Variable description\f$[units]\f$
      complex c(la,ilev)        !<Variable description\f$[units]\f$
      complex t(la,ilev)        !<Variable description\f$[units]\f$
      complex es(la,levs)       !<Variable description\f$[units]\f$
      complex ps(la)            !<Variable description\f$[units]\f$
      complex trac(la,ilev,ntrac)!<Variable description\f$[units]\f$
      complex hoskf(la)         !<Variable description\f$[units]\f$
C
C     * WRKT is work space for calls to swaps2, which is used only
C     * if LA is a multiple of 32 to avoid bank conflicts.
C
      real wrkt(1)              !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================      
c
c     * node information.
c
      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE 
C-----------------------------------------------------------------------
      nlev1                   = 1 + 3*ilev + levs + ntrac*ilev
      nlev2                   = 2*ilev
      call swaps2f(ps, spec1, wrkt, la, nlev1, hoskf) 
      call swaps2f(p,  spec2, wrkt, la, nlev2, hoskf)
c
c     * include effect of earth's rotation in streamfunction.
c     * since this is for the "true" (0,1) location in the triangle,
c     * this must only be done for node 0 which always contains the first
c     * m=0 row.
c
      if(mynode.eq.0)                                  then
        fpsi=-.5*fvort
        do lev=1,ilev
           spec2(1,lev,2) = spec2(1,lev,2) + fpsi
        enddo
      endif
c
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
