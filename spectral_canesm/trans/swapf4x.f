!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SWAPF4X(FOUT,FIN,ILEV,NLAT,ILAT,JOFF,ILH)

C     * AUG 3/2003 - M.LAZARE. LIKE SWAPF4 EXCEPT INPUT ARRAY
C     *                        IS FIN(2,ILH,NLAT,ILEV) INSTEAD OF
C     *                        F(ILEV,NLAT,2,ILH), AND OUTPUT ARRAY
C     *                        IS OVER ALL ILAT LATITUDES (NO
C     *                        WORKS WORK ARRAY).
C     * NOV 12/92. -  A.J.Stacey. PREVIOUS VERSION SWAPF4.
C
C     * Transpose FIN(2,ilh,nlat,ilev) to FOUT(2,ilev,ilat,ilh).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real fin (2,ilh,nlat,ilev)        !<Variable description\f$[units]\f$
      real fout(2,ilev,ilat,ilh)        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
      do i=1,ilh
      do j=1,nlat
      do l=1,ilev
        fout(1,l,joff+j,i) = fin(1,i,j,l)
        fout(2,l,joff+j,i) = fin(2,i,j,l)
      enddo
      enddo
      enddo 
C
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
