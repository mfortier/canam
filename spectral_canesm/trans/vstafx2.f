!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      subroutine vstafx2(f,s,alp,
     1                   nlev,nlat,ntot)

c     * sep 11/2006 - f.majaess. New version for gcm15f:
c     *                          Revised to call "sgemm" for r4i4 mode. 
c     * oct 29/2003 - r.mclay/  Previous version vstafx from IBM conversion:
c     *               m.lazare. New Legendre transform, like vstaf except
c     *                         uses vendor-supplied DGEMM.
c     *                         ** note ** : ALP order must be reversed
c     *                         to use this! 
c     * oct 30/92. -  a.j.stacey. previous version vstaf.
c
c     * Note: The usual input to routines such as blas is 32-bit reals
c     *       and integers. Since, however, we are using "dgemm", the
c     *       input is 64-bit reals and 32-bit integers. Hence, the
c     *       required interface changes are to use 4-byte integers for
c     *       any integers passed to "dgemm".
c     *
c     * Also note that since "S" and "F" are complex in the calling
c     * routine and since the level dimension is the innermost, all
c     * sizes referring to level are passed as "2*...".
c
      implicit none
      real s(2*nlev,ntot)       !<Variable description\f$[units]\f$
      real f(2*nlev,nlat)       !<Variable description\f$[units]\f$
      real*8  alp(nlat,ntot+1)  !<Variable description\f$[units]\f$
      real alpha
      real beta                 !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
c
c     * internal work array for 32-bit case.
c     * this is required since sgemm requires 32-bit arrays and
c     * "alp" is 64-bit from agcm driver!
c
      real*4  alp4(nlat,ntot+1)
c
      integer*4 ntot4, nlat4, nlev4
      integer nlev, nlat, ntot
      integer machine,intsize
c
c     * common block to hold word size.
c
      common /machtyp/ machine,intsize
c-----------------------------------------------------------------------
      alpha  = 1.
      beta   = 0.
      ntot4  = ntot
      nlat4  = nlat
      nlev4  = 2*nlev
c
      if (machine.eq.2) then
       alp4(1:nlat,1:ntot+1)=alp(1:nlat,1:ntot+1) 
       call sgemm('N', 'T', nlev4 , nlat4, ntot4, alpha, s, 
     1           nlev4, alp4, nlat4, beta, f, nlev4)
      else
c$$$       call dgemm('N', 'T', nlev4 , nlat4, ntot4, alpha, s,
c$$$     1           nlev4, alp, nlat4, beta, f, nlev4)
       call dgemm('N', 'T', nlev*2 , nlat, ntot, alpha, s,
     1           nlev*2, alp, nlat, beta, f, nlev*2)
      endif
c
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
