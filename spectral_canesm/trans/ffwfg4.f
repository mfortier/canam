!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE FFWFG4 (FC,ILH, GD,ILG, IR,LON,WRK,ILEV,IFAX,TRIGS) 
C   
C     * AUG 01/03 - M.LAZARE. - GENERALIZED USING "MAXLOT" PASSED IN
C     *                         COMMON BLOCK, INSTEAD OF HARD-CODED
C     *                         VALUE OF 256 ONLY VALID FOR THE NEC.
C     *                         THIS IS CONSISTENT WITH WHAT IS DONE IN VFFT3.
C     *                       - CALLS NEW VFFT3 INSTEAD OF VFFT2.
C     * NOV 01/92 - J.STACEY. PREVIOUS VERSION FFWFG3.
C
C     * DRIVING ROUTINE FOR THE FOURIER TRANSFORM,   
C     * GRID TO COEFFICIENTS.      
C     * ====================      
C     * FC    = FOURIER COEFFICIENTS,     
C     * ILH   = FIRST DIMENSION OF COMPLEX FC,    
C     * GD    = GRID DATA,      
C     * ILG   = FIRST DIMENSION OF REAL GD, NOTE THAT IT IS  
C     *         ASSUMED THAT GD AND FC ARE EQUIVALENCED IN MAIN, 
C     *         OBVIOUSLY THEN ILG MUST EQUAL 2*ILH.   
C     *         ACTUALLY FC IS DECLARED REAL FOR CONVENIENCE.  
C     * IR    = MAXIMUM EAST-WEST WAVE NUMBER (M=0,IR),   
C     * LON   = NUMBER OF DISTINCT LONGITUDES,    
C     * WRK   = WORK SPACE, OF SIZE (LON+2)*LENGTH.   
C     * ILEV  = NUMBER OF LEVELS.     
C         
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL FC(ILG,ILEV)         !<Variable description\f$[units]\f$
      REAL GD(ILG,ILEV)         !<Variable description\f$[units]\f$
      REAL WRK(ILG*MAXLOT)      !<Variable description\f$[units]\f$
      REAL TRIGS(LON)           !<Variable description\f$[units]\f$
      INTEGER IFAX(*)           !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      COMMON /ITRANS/ MAXLOT
C----------------------------------------------------------------- 
      ISIGN=-1        
      INC  = 1        
      LON1 = LON+1       
      IF(ILG.LT.LON+2) CALL XIT('FFWFG4',-1)    
C  
C     * ABORT IF WORKSPACE SIZE OF "WRK" INSUFFICIENT.
C
      IF(MAXLOT.GT.ILEV) CALL XIT('FFWFG4',-2) 
C         
C     * DO AS MANY AS MAXLOT AT ONCE FOR VECTORIZATION.   
C         
      NTIMES=ILEV/MAXLOT       
      NREST =ILEV-NTIMES*MAXLOT      
      NSTART=1        
C         
      IF(NREST.NE.0) THEN      
         LENGTH=NREST       
         NTIMES=NTIMES+1      
      ELSE        
         LENGTH=MAXLOT       
      ENDIF        
C         
C     * DO THE FOURIER TRANSFORMS.     
C         
      DO 300 N=1,NTIMES       
       CALL VFFT3(GD(1,NSTART),WRK,TRIGS,IFAX,INC,ILG,LON,LENGTH,ISIGN)
       NSTART=NSTART+LENGTH      
       LENGTH=MAXLOT
  300 CONTINUE        
C         
      RETURN        
C-----------------------------------------------------------------------
      END        
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
