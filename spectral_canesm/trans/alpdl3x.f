!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE ALPDL3X(DELALP,ALP,LSR,LM,NLAT,IRAM)
C 
C     * OCT 29/03 - M. LAZARE. REVERSE ORDER OF POLYNOMIALS
C     *                        AND DO OVER ALL LATITUDES.
C     * OCT 15/92 - PREVIOUS VERSION ALPDL3.
C     * JUL 14/92 - PREVIOUS VERSION ALPDL2.
C 
C     * COMPUTES LAPLACIAN OF LEGENDRE POLYNOMIALS. 
C     * LSR CONTAINS ROW LENGTH INFO. 
C 
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DELALP(NLAT,IRAM)  !<Variable description\f$[units]\f$
      REAL*8 ALP(NLAT,IRAM)     !<Variable description\f$[units]\f$
      INTEGER LSR(2,LM+1)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
      DO 300 M=1,LM 
        KL=LSR(2,M) 
        KR=LSR(2,M+1)-1 
        DO 220 K=KL,KR
          NS=(M-1)+(K-KL) 
          FNS=FLOAT(NS) 
          DO 210 NJ=1,NLAT
            DELALP(NJ,K)=-FNS*(FNS+1.)*ALP(NJ,K)
  210     CONTINUE
  220   CONTINUE   
  300 CONTINUE    
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
