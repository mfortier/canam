!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE FFGFW4 (GD, ILG,FC,ILH,IR,LON,WRK,ILEV,IFAX,TRIGS) 
C   
C     * AUG 01/03 - M.LAZARE. - GENERALIZED USING "MAXLOT" PASSED IN
C     *                         COMMON BLOCK, INSTEAD OF HARD-CODED
C     *                         VALUE OF 256 ONLY VALID FOR THE NEC.
C     *                         THIS IS CONSISTENT WITH WHAT IS DONE IN VFFT3.
C     *                       - CALLS NEW VFFT3 INSTEAD OF VFFT2.
C     * NOV 01/92 - J.STACEY. PREVIOUS VERSION FFGFW3.
C
C     * DRIVING ROUTINE FOR THE FOURIER TRANSFORM,   
C     * COEFFICIENTS TO GRID.      
C     * ====================      
C     * FC    = FOURIER COEFFICIENTS,     
C     * ILH   = FIRST DIMENSION OF COMPLEX FC,    
C     * GD    = GRID DATA,      
C     * ILG   = FIRST DIMENSION OF REAL GD, NOTE THAT IT IS  
C     *         ASSUMED THAT GD AND FC ARE EQUIVALENCED IN MAIN, 
C     *         SO OBVIOUSLY ILG MUST EQUAL 2*ILH,   
C     *         ACTUALLY FC IS DECLARED REAL FOR CONVENIENCE.  
C     * IR    = MAXIMUM EAST-WEST WAVE NUMBER (M=0,IR),   
C     * LON   = NUMBER OF DISTINCT LONGITUDES,    
C     * WRK   = WORK SPACE,      
C     * ILEV  = NUMBER OF LEVELS.     
C         
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL FC(ILG,ILEV) !<Variable description\f$[units]\f$
      REAL GD(ILG,ILEV) !<Variable description\f$[units]\f$
      REAL WRK(ILG*MAXLOT)   !<Variable description\f$[units]\f$
      REAL TRIGS(LON)   !<Variable description\f$[units]\f$
      INTEGER IFAX(*)   !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      COMMON /ITRANS/ MAXLOT
C----------------------------------------------------------------- 
      ISIGN = +1       
      INC   =  1       
      IR121 =  (IR+1)*2 +1      
      IF(ILG.LT.LON+2) CALL XIT('FFGFW4',-1)    
C  
C     * ABORT IF WORKSPACE SIZE OF "WRK" INSUFFICIENT.
C
      IF(MAXLOT.GT.ILEV) CALL XIT('FFGFW4',-2) 
C         
C     * SET TO ZERO FOURIER COEFFICIENTS BEYOND TRUNCATION.  
C
      DO 100 L=1,ILEV                
      DO 100 I=IR121,LON+2      
  100 FC(I,L) =0.       
C         
C     * AS MANY AS MAXLOT ARE DONE AT ONCE FOR VECTORIZATION.  
C         
      NSTART=1        
      NTIMES=ILEV/MAXLOT       
      NREST =ILEV-NTIMES*MAXLOT      
C         
      IF(NREST.NE.0) THEN      
         LENGTH=NREST       
         NTIMES=NTIMES+1      
      ELSE        
         LENGTH=MAXLOT       
      ENDIF        
C         
C     * DO THE FOURIER TRANSFORMS.     
C         
      DO 300 N=1,NTIMES       
       CALL VFFT3(FC(1,NSTART),WRK,TRIGS,IFAX,INC,ILG,LON,LENGTH,ISIGN)
       NSTART=NSTART+LENGTH      
       LENGTH=MAXLOT       
  300 CONTINUE        
C         
      RETURN        
C-------------------------------------------------------------------- 
      END        
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
