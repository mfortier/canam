!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SUMSPTP (S, SPTP,NEL)
C
C     * APR 02/98 - B.DENIS  - SUMSPTP VERSION. TO BE USED IN CONJONCTION 
C     *                        OF MHANL7/MHALP3 SPECTRAL FORWARD 
C     *                        ROUTINES.
C
C     * THIS ROUTINE FINALIZES FORWARD LEGENDRE TRANSFORM.
C     * THIS IS THE LAST STEP OF THE TRANSFORM. IT ADDS SPECTRAL TENDENCIES 
C     * CONTRIBUTION (VECTOR SPTP) FORM EACH LATITUDE SET TO THE GLOBAL SPECTRAL
C     * SPECTRAL TENDENCIES (VECTOR S). ALL THE SPECTRAL TENDENCIES VARIABLES 
C     * THROUGH ALL LEVELS ARE LINED-UP IN S AND SPTP.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL S(NEL)       !<Variable description\f$[units]\f$
      REAL SPTP(NEL)    !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

*vdir nodep
      DO 100 I=1,NEL
         S(I)= S(I) + SPTP(I)
 100  CONTINUE

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
