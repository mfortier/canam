!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SWAPTF(S,WRKS,WRKS1,LA,ILEV,HOSKF)

C     * DEC 10/2004 - J.SCINOCCA. ADD HOSKINS SPECTRAL FILTER OF
C     *                           TENDENCIES.  
C     * JUN 25/2003 - M.LAZARE. PREVIOUS VERSION SWAPT. 
C     *                         LIKE ROUTINE SWAPS1 EXCEPT
C     *                         ADDS "WRKS" INTO "S" INSTEAD
C     *                         OF OVERWRITING "S". THIS IS
C     *                         NOW USED IN THE POST-NEC
C     *                         CODE WHERE THE CONVERSION
C     *                         FROM "SPEC3" TO THE /SP/ COMMON
C     *                         BLOCK IS DONE FOLLOWING THE GLOBAL
C     *                         SUMS.  
C
C     * Update S(2,LA,ILEV) with transpose from WRKS(2,ILEV,LA)
C
      real s(2,la,ilev)         !<Variable description\f$[units]\f$
      real wrks(2,ilev,la)      !<Variable description\f$[units]\f$
      real wrks1(2,ilev,la+1)   !<Variable description\f$[units]\f$
      real hoskf(2,la)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
C     * first determine if LA is an exact multiple of 32 to
C     * see if bank conflicts may be a problem.
C
      if(mod(la,32).ne.0) then
        do 10 j=1,ilev
        do 10 i=1,la
          flt=HOSKF(1,i)
          s(1,i,j) = s(1,i,j) + wrks(1,j,i)*flt
          s(2,i,j) = s(2,i,j) + wrks(2,j,i)*flt
   10 continue
      else
        do 20 i=1,la
        do 20 j=1,ilev
          wrks1(1,j,i) = wrks(1,j,i)
          wrks1(2,j,i) = wrks(2,j,i)
   20   continue
C
        do 30 j=1,ilev
        do 30 i=1,la
          flt=HOSKF(1,i)
          s(1,i,j) = s(1,i,j) + wrks1(1,j,i)*flt
          s(2,i,j) = s(2,i,j) + wrks1(2,j,i)*flt
   30   continue 
      endif
C
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
