!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE ALPWEI3X(ALPX,DALPX,DELALPX,
     1                    ALP,DALP,DELALP,
     2                    WL,WOSL,LSR,LM,NLAT,IRAM) 

C     * SEP 02/03 - M.LAZARE. REVERSE ORDER OF ALPX,DALPX,DELALPX.
C     * FEB 02/94 - M.LAZARE. PREVIOUS VERSION ALPWEI3.
C     * OCT 15/92 - M.LAZARE. PREVIOUS VERSION ALPWEI2.
C     * JUL 14/92 - R.LAPRISE.PREVIOUS VERSION ALPWEI.
C  
C     * NORMALIZE THE LEGENDRE POLYNOMIALS AND THEIR DERIVATIVES
C     * BY THE GAUSSIAN WEIGTHS AS REQUIRED FOR THE DIRECT
C     * TRANSFORM FROM FOURIER TO SPECTRAL. 
C     * DELALP ALSO INCLUDES THE FACTOR -0.5 REQUIRED FOR THE 
C     * DIVERGENCE TENDENCY TERM INVOLVING THE KINETIC ENERGY.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * OUTPUT FIELDS:
C
      REAL*8 ALPX(NLAT,IRAM)    !<Variable description\f$[units]\f$
      REAL*8 DALPX(NLAT,IRAM)   !<Variable description\f$[units]\f$
      REAL*8 DELALPX(NLAT,IRAM) !<Variable description\f$[units]\f$
C
C     * INPUT FIELDS:
C
      REAL*8 ALP (NLAT,IRAM)    !<Variable description\f$[units]\f$
      REAL*8 DALP (NLAT,IRAM)   !<Variable description\f$[units]\f$
      REAL*8 DELALP (NLAT,IRAM) !<Variable description\f$[units]\f$
      REAL*8 WL(NLAT)           !<Variable description\f$[units]\f$
      REAL*8 WOSL(NLAT)         !<Variable description\f$[units]\f$
      INTEGER LSR(2,LM+1)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
      NR=LSR(2,LM+1)-1
      DO 100 I=1,NR 
      DO 100 NJ=1,NLAT
          ALPX(NJ,I)=   ALP(NJ,I)*WL(NJ)
         DALPX(NJ,I)=  DALP(NJ,I)*WOSL(NJ)
       DELALPX(NJ,I)=DELALP(NJ,I)*WOSL(NJ)*(-0.5) 
  100 CONTINUE
C     
      RETURN
C-----------------------------------------------------------------------
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
