!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE ALPDY3X(DALP,ALP,LSR,LM,EPSI,NLAT,IRAM) 
C
C     * OCT 29/03 - M. LAZARE. REVERSE ORDER OF POLYNOMIALS
C     *                        AND DO OVER ALL LATITUDES.
C     * OCT 15/92 - PREVIOUS VERSION ALPDY3.
C     * JUL 14/92 - PREVIOUS VERSION ALPDY2.
C 
C     * SETS DALP TO N-S DERIVATIVE OF LEGENDRE POLYNOMIALS IN ALP. 
C     * EPSI CONTAINS PRECOMPUTED CONSTANTS.
C     * LSR CONTAINS ROW LENGTH ONFO. 
C 
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DALP(NLAT,IRAM)    !<Variable description\f$[units]\f$
      REAL*8 ALP(NLAT,IRAM)     !<Variable description\f$[units]\f$
      REAL*8 EPSI(IRAM)         !<Variable description\f$[units]\f$
      INTEGER LSR(2,LM+1)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
      DO 300 M=1,LM 
        MS=M-1
        KL=LSR(2,M) 
        KR=LSR(2,M+1)-2 
        DO 210 K=KL,KR
          FNS=FLOAT(MS+K-KL)
          DO 200 NJ=1,NLAT
            ALPILM=0. 
            IF(K.GT.KL) ALPILM=ALP(NJ,K-1) 
            DALP(NJ,K)=(FNS+1)*EPSI(K)*ALPILM-FNS*EPSI(K+1)*ALP(NJ,K+1) 
  200     CONTINUE
  210   CONTINUE   
C
        DO 220 NJ=1,NLAT  
          DALP(NJ,KR+1)=0. 
  220   CONTINUE
  300 CONTINUE     
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
