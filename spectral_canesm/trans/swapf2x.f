!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SWAPF2X(F,ILEV,NLAT,ILH,LON,G)
C
C     * AUG 26/2003 - M.LAZARE. New version for revised FFT's.
C     * NOV 12/92 A.J. STACEY.  Previous version SWAPF2.
C     *
C     * Store F(2,ILH,NLAT,ILEV) into G(2*ILH*NLAT+1,ILEV).
C     *
C     * NOTE: This means that the 2 extra memory locations for each 
C     * transform, unused by the physics, are extracted here before the 
C     * data is passed to the physics (note that LON+2 = 2*ILH = ILG).
C     *
C     * Note that it is very important that the output array be dimensioned
C     * with the unused memory locations explicitly allocated, or else 
C     * the starting addresses according to level and variable will not be
C     * maintained. To avoid memory bank conflicts, the inner dimension of
C     * "g" is augmented by one over what is used.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      real f(2*ilh,nlat,ilev)   !<Variable description\f$[units]\f$
      real g(2*ilh*nlat+1,ilev) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================      
C--------------------------------------------------------------------------
      do 200 lev=1,ilev
        k = 1
        do 100 lat=1,nlat
        do 100 j=1,lon
          g(k,lev) = f(j,lat,lev)
          k = k + 1
  100   continue
  200 continue
C
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
