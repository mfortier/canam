!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE LEGBS2(SPEC,FOUR1,ALP,
     1                  WL,WOCSL,LEVS,NLAT,
     2                  M,NTOT)
C
C     * SEP 11/06 - M. LAZARE. NEW VERSION FOR GCM15F:
C     *                        - CALLS NEW VFASTX2 TO SUPPORT 32-BIT DRIVER.
C     * OCT 29/03 - M. LAZARE. PREVIOUS VERSION LEGBS2 FROM IBM CONVERSION:
C     * OCT 29/03 - M. LAZARE. NEW ROUTINE TO DO EXCLUSIVELY THE BACKWARD
C     *                        LEGENDRE TRANSFORM FOR MOISTURE, BASED ON
C     *                        OLD CODE IN MHANLQ_ AND USING DGEMM.
C     *                        THIS IS USED ONLY IN SEMI-LAG INITIALIZTION
C     *                        LOOP.  
C     *  
C     * TRANSFORM FOURIER COEFFICIENTS FOR MOISTURE TO SPECTRAL TENDENCIES
C     * FOR THIS (POSSIBLY CHAINED) GAUSSIAN LATITUDE.
C
C     * THIS IS CALLED FOR A GIVEN ZONAL WAVENUMBER INDEX "M", IE INSIDE
C     * A LOOP OVER M.
C     *
C     *    ALP = LEGENDRE POLYNOMIALS *GAUSSIAN WEIGHT
C     *
C     * INPUT FIELDS
C     * ------------
C     *
C     * THIS ROUTINE IS CALLED WITHIN A LOOP OVER ZONAL INDEX "M" AND
C     * THUS ACCESSES THE INPUT FOR THAT PARTICULAR ZONAL INDEX FROM
C     * THE CALLING ARRAY.
C     *
C     *    ESG --> FOUR1(2,LEVS,NLAT)
C     *
C     * OUTPUT FIELDS
C     * -------------
C     *
C     * THE OUTPUT DATA FIELDS ARE THE SPECTRAL MOISTURE WORK FIELD,
C     * ACCUMULATED INTO THE FOLLOWING DATA STRUCTURE:
C     *
C     *           SPEC(2,LEVS,NTOT)
C     *
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
C     * Input Fourier Coefficients.
C
      real four1(2,levs,nlat)   !<Variable description\f$[units]\f$
C
C     * Output spectral tendencies.
C
      real spec(2,levs,ntot)    !<Variable description\f$[units]\f$
C
C     * Other arrays.   
C
      REAL*8 ALP(nlat,ntot+1)   !<Variable description\f$[units]\f$
      REAL*8 WL(nlat)           !<Variable description\f$[units]\f$
      REAL*8 WOCSL(nlat)        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
      beta=0.
      call vfastx2(four1,spec,alp,
     1             beta,levs,nlat,ntot)
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
