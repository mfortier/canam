! Replicated code for all the different types/kinds of 1d arrays

  integer(kind=int32) :: array_chksum, global_chksum, write_unit
  integer(kind=MPI_INT_KIND) :: mpi_error

  if (present(is) .and. present(ie) .and. present(js) .and. present(je)) then
    array_chksum = checksum_low(array, is_in=is, ie_in=ie, js_in=js, je_in=je)
  elseif (present(is) .and. present(ie)) then
    array_chksum = checksum_low(array, is_in=is, ie_in=ie)
  elseif (present(js) .and. present(je)) then
    array_chksum = checksum_low(array, js_in=js, je_in=je)
  else
    array_chksum = checksum_low(array)
  endif

  call MPI_REDUCE(array_chksum, global_chksum, 1, MPI_INTEGER, MPI_SUM, AGCM_COMM_ROOT, AGCM_COMM_WORLD, mpi_error)
  if (AGCM_RANK == AGCM_COMM_ROOT) then
    write_unit = STDOUT
    if (present(out_unit)) write_unit = out_unit

    global_chksum = mod(global_chksum, CHKSUM_MOD)
    write(write_unit,'(A,I8.8,X,A)') "chksum=", array_chksum, TRIM(msg)
    call flush(write_unit)
  endif