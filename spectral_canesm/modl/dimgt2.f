!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE DIMGT2(LSRTOT,LMTOT,LSR,LA,LM,MINDEX)
C
C     * NOV 04/03 - M.LAZARE, BASED ON DIMGT, EXCEPT THAT NOW LM
C     *                       IS KNOWN (PASSED FROM GCMPARM) AND NO
C     *                       LRLMT. RESULTS ARE APPLICABLE FOR THE
C     *                       COMPLETE TRIANGLE (AS BEFORE) AS WELL AS
C     *                       THE NODE VALUES (IE SUBSET OF M-SPACE).  
C     * DEC 03/93 - G. J. B.  - PREVIOUS VERSION DIMGT.
C 
C     * COMPUTES ROW LENGTH INFORMATION FOR SPECTRAL ARRAYS.
C
C     * LSR(1,M),LSR(2,M) = FIRST WORD IN ROW M OF SPECTRAL,ALP ARRAYS. 
C     * LM = NUMBER OF SPECTRAL ROWS. 
C     * LA = TOTAL LENGTH OF SPECTRAL ARRAY (COMPLEX WORDS).
C     * THE ABOVE IS FOR EACH NODE'S VALUES. THE RESPECTIVE "TOT"
C     * ARRAYS IS FOR THE TOTAL TRIANGLE. BOTH ARE REQUIRED BY THE MODEL.
C
C     * IN ADDITION, THE "TRUE" VALUES OF "M" FOR THE LOCAL NODE
C     * ARRAYS ARE CALCULATED AND STORED RESPECTIVELY IN "MINDEX".
C
      INTEGER LSRTOT(2,LMTOT+1) !<Variable description\f$[units]\f$
      INTEGER LSR(2,LM+1)       !<Variable description\f$[units]\f$
      INTEGER MINDEX(LM)        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER*4 MYNODE
C
      COMMON /MPINFO/ MYNODE
C---------------------------------------------------------------------
C     * FIRST DO THE CALCULATIONS FOR THE COMPLETE TRIANGLE.
C
      LRTOT=LMTOT   ! FOR TRIANGULAR TRUNCATION.
      LSRTOT(1,1)=1
      LSRTOT(2,1)=1
      LMPTOT=LMTOT+1
      DO 100 M=2,LMPTOT
        LSRTOT(1,M)=LSRTOT(1,M-1)+LRTOT-(M-2)
        LSRTOT(2,M)=LSRTOT(2,M-1)+LRTOT-(M-3)
  100 CONTINUE
C
      LMH=LMTOT/2       
      MC=0
      MN=0
C
C     * MPI HOOK.
C
      MSTART=MYNODE*LM+1
      MEND  =MYNODE*LM+LM
C
C     * CONSTRUCT "RECTANGLE" FROM PAIRS OF LOW/HIGH ZONAL WAVENUMBERS.
C
      DO 150 M=1,LMH
C
        MVAL=M
        MC=MC+1
        IF(MC.GE.MSTART .AND. MC. LE. MEND)           THEN
          MN=MN+1
          MINDEX(MN)=MVAL
        ENDIF
C
        MVAL=LMTOT-M+1
        MC=MC+1
        IF(MC.GE.MSTART .AND. MC. LE. MEND)           THEN
          MN=MN+1
          MINDEX(MN)=MVAL
        ENDIF
C
  150 CONTINUE
C
C     * NOW THE VALUES APPLICABLE TO EACH NODE.
C
      LSR(1,1)=1
      LSR(2,1)=1
      LMP=LM+1
      DO 200 M=2,LMP
        MVAL=MINDEX(M-1)
        LSRINC=LMTOT-MVAL+1 
        LSR(1,M)=LSR(1,M-1)+LSRINC
        LSR(2,M)=LSR(2,M-1)+LSRINC+1
  200 CONTINUE
C
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
