!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CALCW (WJ, OMEGJ,
     1                  DJ, UJ, VJ, TJ, PSLMJ, PSTHJ, PSJ,
     2                  SGJ, DSGJ, DSHJ, DLNSGJ, 
     3                  A1SGJ, B1SGJ, A2SGJ, B2SGJ, DB,
     4                  ILEV, ILG, IL1, IL2)
C
C     * SEP 07/2009 - M.LAZARE. NEW ROUTINE FOR GCM15I.
C  
C     * CALCULATES VERTICAL VELOCITY ON PHYSICS GRID (M/SEC).
C     * CODE PARALLELS CALCULATION ON DYNAMICS GRID IN DYNCAL4D. 
C
      IMPLICIT NONE
C
C     * OUTPUT FIELDS:
C
      REAL WJ(ILG,ILEV)         !<Variable description\f$[units]\f$
      REAL OMEGJ(ILG,ILEV)      !<Variable description\f$[units]\f$
C  
C     * PRIMARY INPUT FIELDS:
C  
      REAL  DJ (ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL  TJ (ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL  UJ (ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL  VJ (ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL  PSLMJ(ILG)          !<Variable description\f$[units]\f$
      REAL  PSTHJ(ILG)          !<Variable description\f$[units]\f$
      REAL  PSJ(ILG)            !<Variable description\f$[units]\f$
C  
C     * VERTICAL DISCRETIZATION INPUT FIELDS:
C  
      REAL  DSGJ  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  DSHJ  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  SGJ   (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  DLNSGJ(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  A1SGJ (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  B1SGJ (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  A2SGJ (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  B2SGJ (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  DB (ILEV)           !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C     * WORK FIELDS:
C  
      REAL VGRPSJ(ILG,ILEV)
      REAL SSDRJK(ILG)
C
C     * SCALARS:
C
      REAL OMSPJ,DRJKI,DRJKPI
      REAL WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,RGASV,CPRESV
      INTEGER K,I,ILEV,ILG,IL1,IL2
C
      COMMON /PARAMS/ WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1                RGASV,CPRESV
C-----------------------------------------------------------------------
      SSDRJK=0.
C
      DO 150 K = 1,ILEV
      DO 150 I = IL1,IL2
         VGRPSJ(I,K) =  UJ(I,K)*PSLMJ(I) + VJ(I,K)*PSTHJ(I)
  150 CONTINUE

C     ----------------------------------------------------------------- 
C     CALCULATE OMSPJ (OMEGA SUR P) AND DIAGNOSTIC W
C     ----------------------------------------------------------------- 
  
      DO 200 K=1,ILEV-1
      DO 200 I = IL1,IL2
         DRJKI      =  DJ(I,K) + VGRPSJ(I,K)*DB(K)/DSGJ(I,K) 
         DRJKPI     =  DJ(I,K+1) + VGRPSJ(I,K+1)*DB(K+1)/DSGJ(I,K+1) 
  
         OMSPJ      = ( - SSDRJK(I)*DLNSGJ(I,K)
     1                  - DRJKI*A1SGJ(I,K) - DRJKPI*A2SGJ(I,K+1) 
     2                  + VGRPSJ(I,K)*B1SGJ(I,K) 
     3                  + VGRPSJ(I,K+1)*B2SGJ(I,K+1) )/DSHJ(I,K) 
         OMEGJ(I,K) = OMSPJ * PSJ(I) * SGJ(I,K)
         WJ(I,K)    = -1. * (RGAS*TJ(I,K)/GRAV) * OMSPJ
         SSDRJK(I)  = SSDRJK(I) +DRJKI*DSGJ(I,K) 
  200 CONTINUE
  
C     ----------------------------------------------------------------- 
  
      K=ILEV
      DO 250 I = IL1,IL2
         DRJKI      =  DJ(I,K) + VGRPSJ(I,K)*DB(K)/DSGJ(I,K) 
         OMSPJ      = ( - SSDRJK(I)*DLNSGJ(I,K) - DRJKI*A1SGJ(I,K) 
     1                  + VGRPSJ(I,K)*B1SGJ(I,K) )/DSHJ(I,K) 
         OMEGJ(I,K) = OMSPJ * PSJ(I) * SGJ(I,K)
         WJ   (I,K) = -1. * (RGAS*TJ(I,K)/GRAV) * OMSPJ
  250 CONTINUE
  
      RETURN
C-----------------------------------------------------------------------
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
