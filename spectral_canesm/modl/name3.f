!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE NAME3(NAM,IC,INTEG)

C     * APR 17/15 - M.LAZARE/   NEW VERSION FOR GCM18+:
C     *             J.COLE/     - RENAME "INT" AS "INTEG"
C     *             F.MAJAESS.    TO AVOID CONFLICT WITH INTRINSIC
C     *                           OF SAME NAME. THIS IS A PURELY
C     *                           COSMETIC MODIFICATION SO THE SAME
C     *                           NAME IS KEPT.    
C     * DEC 30/09 - K.VONSALZEN - NEW, BASED ON "NAME2", SIMILAR TO "NOM".
C
C     * SUBROUTINE QUI DEFINIE LE NOM DES CHAMPS ASSOCIE AUX DIFFERENTS
C     * TRACEURS EN FONCTION DU NOMBRE DE TRACEURS.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      CHARACTER CNAM*4
      CHARACTER*1 IC(1) !<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C----------------------------------------------------------------------
      N3=INTEG/100
      NN=INTEG-N3*100
      N2=NN/10
      N1=NN-N2*10
      IF(INTEG.GT.999) CALL                          XIT('NAME3',-1)
      WRITE(CNAM,1000) IC,N3,N2,N1
      NAM=NC4TO8(CNAM)

 1000 FORMAT(1A1,3I1)
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
