!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!      
        SUBROUTINE MONSAV(ISMON,IDAY,DELT)
C
C     * APR 01/2005 - M.LAZARE. NEW ROUTINE TO CALCULATE NUMBER OF
C     *                         TIMESTEPS IN A MONTH.
C
C     * DAYSINMON = NUMBER OF DAYS IN THE CURRENT MONTH.
C     * DELT      = MODEL TIMESTEP LENGTH (SECONDS). 
C     * IDAY      = DAY OF THE YEAR
C     * ISMON     = NUMBER OF TIMESTEPS IN A MONTH.
C     * MONTH     = CURRENT MONTH (3-CHARACTER NAME).
C
      IMPLICIT NONE

      INTEGER ISMON     !<Variable description\f$[units]\f$
      INTEGER IDAY      !<Variable description\f$[units]\f$
      INTEGER MDUM,DAYSINMON
      REAL DELT         !<Variable description\f$[units]\f$
      CHARACTER*3 MONTH
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
C     * GET CURRENT MONTH. MDUM IS A DUMMY RETURNED ARGUMENT NOT USED.
C     * ABORT IF MONTH.EQ.'XXX'.
C 
      CALL IDATEC(MONTH,MDUM,IDAY) 
      IF(MONTH.EQ.'XXX')   CALL XIT('MONSAV',-1)
C 
C     * DETERMINE THE NUMBER OF DAYS IN THE MONTH.
C 
      IF(MONTH.EQ.'JAN' .OR. MONTH.EQ.'MAR' .OR. MONTH.EQ.'MAY' .OR.
     1   MONTH.EQ.'JUL' .OR. MONTH.EQ.'AUG' .OR. MONTH.EQ.'OCT' .OR.            
     2   MONTH.EQ.'DEC')                               THEN
        DAYSINMON=31
      ELSE IF(MONTH.EQ.'APR' .OR. MONTH.EQ.'JUN' .OR. 
     1        MONTH.EQ.'SEP' .OR. MONTH.EQ.'NOV')      THEN
        DAYSINMON=30
      ELSE IF(MONTH.EQ.'FEB')                          THEN
        DAYSINMON=28
      ELSE
        CALL XIT('MONSAV',-2)
      ENDIF 
C
C     * FINALLY, DETERMINE NUMBER OF TIMESTEPS IN MONTH, FOR NORMALIZTION
C     * FACTOR OF MONTHLY ACCUMULATED OUTPUT FIELDS.
C
      ISMON=DAYSINMON*NINT(86400./DELT)
    
      RETURN
C-------------------------------------------------------------------- 
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

