!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!        
      SUBROUTINE SAVINGX (LSAV,ISAV,IDT,NUMSEC) 
C 
C     * DEC 03/2007 - L.SOLHEIM/M.LAZARE. NEW VERSION WITH "IMPLICT NONE"
C     *                                   AND MOST VARIABLES PROMOTED
C     *                                   TO INTEGER*8 TO PREVENT OVERFLOW
C     *                                   IN 32-BIT MODE
C     * NOV 18/82 - R.LAPRISE. PREVIOUS VERSION SAVING.
C
C     * SET LOGICAL SWITCH ON THE BASIS OF
C     * ISAV, THE SAVING INTERVAL IN NUMBER OF TIMESTEP,
C     * IDT, THE LENGTH OF THE TIMESTEP, AND
C     * NUMSEC, THE ELAPSED TIME FROM THE BEGINNING OF THE SIMULATION.
C     * AT THE BEGINNING OF A NEW RUN, NO FIELD IS SAVED 
C 
      IMPLICIT NONE

      INTEGER*8 :: NUMSEC       !<Variable description\f$[units]\f$
      INTEGER*8 :: NUMSECI,INTSAV,MODNOW,MODNXT
      INTEGER   :: ISAV         !<Variable description\f$[units]\f$
      INTEGER   :: IDT          !<Variable description\f$[units]\f$
      LOGICAL   :: LSAV         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C---------------------------------------------------------------------
      LSAV=.FALSE.
      IF(ISAV.LE.0) GO TO 100 
C 
      INTSAV=ISAV*IDT
      NUMSECI=NUMSEC+IDT 
      MODNOW=MOD(NUMSEC ,INTSAV) 
      MODNXT=MOD(NUMSECI,INTSAV) 
      IF  ( ISAV  .EQ.1 
     1 .OR. MODNOW.LE.0 
     2 .OR.(MODNXT-MODNOW.LT.IDT .AND. MODNXT.NE.0)) LSAV=.TRUE.
C 
  100 CONTINUE
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
