!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE VRAIMA3 (VTG,UTG,TUTG,TVTG,EG,TTG,SUTG,SVTG,XUTG,XVTG, 
     1                    ITRAC,NTRAC,ILG,LON,ILEV,LEVS,COSJ,A) 
C
C     * OCT 15/92 - M.LAZARE. COSJ NOW A PASSED ARRAY FOR MULTI-
C     *                       LATITUDE FORMULATION.  
C     * JUL 14/92 - PREVIOUS VERSION VRAIMA2.
C 
C     * CONVERT REAL TENDENCIES TO IMAGES ONE (IN THE OLD TRADITION). 
C     * NOTE THAT IN LOOP 100, THE L.H.S.'S OF (VTG,UTG) ARE MEANT TO 
C     * CORRESPOND TO OLD MODEL VERSION (PUTG,PVTG) TO ALLOW OLD MODEL
C     * VERSION MHANL4 TO BE USED. (SEE APPENDIX III-1 OF "ADIABATIC
C     * FORMULATION OF THE AES/CCC MULTI-LEVEL SPECTRAL GENERAL 
C     * CIRCULATION MODEL" FOR REFERENCE).
C     * SIMILARILY, (TUTG,TVTG) ARE MULTIPLIED BY (-1) BECAUSE THE
C     * OLD MODEL VERSION USED "BIG PEE" WHICH ACTED LIKE "-T". 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL VTG(ILG,ILEV)        !<Variable description\f$[units]\f$
      REAL UTG(ILG,ILEV)        !<Variable description\f$[units]\f$
      REAL TUTG(ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL TVTG(ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL EG  (ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL TTG(ILG,ILEV)        !<Variable description\f$[units]\f$
      REAL XUTG(ILG,ILEV,NTRAC) !<Variable description\f$[units]\f$
      REAL XVTG(ILG,ILEV,NTRAC) !<Variable description\f$[units]\f$
      REAL SUTG(ILG,LEVS)       !<Variable description\f$[units]\f$
      REAL SVTG(ILG,LEVS)       !<Variable description\f$[units]\f$
      REAL*8 COSJ(ILG)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-------------------------------------------------------------------
      DO 200 L=1,ILEV 
         DO 100 I=1,LON 
            VTG (I,L) =-VTG (I,L) * (COSJ(I)/A)
            UTG (I,L) = UTG (I,L) * (COSJ(I)/A)
            TUTG (I,L) =-TUTG (I,L) * (COSJ(I)/A)
            TVTG (I,L) =-TVTG (I,L) * (COSJ(I)/A)
            EG   (I,L) = EG   (I,L) *((COSJ(I)/A)**2)*2. 
  100    CONTINUE 
  200 CONTINUE
      IF(ITRAC.NE.0) THEN 
         DO 210 N=1,NTRAC 
         DO 210 L=1,ILEV
         DO 210 I=1,LON 
            XUTG(I,L,N)=XUTG(I,L,N)*(COSJ(I)/A)
            XVTG(I,L,N)=XVTG(I,L,N)*(COSJ(I)/A)
  210    CONTINUE 
      ENDIF 
C 
      DO 400 L=1,LEVS 
         DO 300 I=1,LON 
            SUTG (I,L) = SUTG (I,L) * (COSJ(I)/A)
            SVTG (I,L) = SVTG (I,L) * (COSJ(I)/A)
  300    CONTINUE 
  400 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
