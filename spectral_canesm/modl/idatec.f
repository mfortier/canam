!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE IDATEC(MONTH,MDAY,IDAY) 
C 
C     * OCT 05/93 - M.LAZARE. NEW NAME FOR IDATE TO AVOID CONFLICT
C     *                       WITH SYSTEM ROUTINE OF SAME NAME.
C     * FEB 20/81 - J.D.HENDERSON. PREVIOUS NAME IDATE.
C
C     * DETERMINES THE DATE FROM THE DAY OF THE YEAR. 
C     * MONTH = MONTH (JAN,FEB,...,ETC.)
C     *  MDAY = DAY OF THE MONTH (1 TO 31)
C     *  IDAY = DAY OF THE YEAR (1 TO 365)
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      INTEGER NFDM(13)
      CHARACTER*3 MON(12)
      CHARACTER*3 MONTH !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      DATA MON/'JAN','FEB','MAR','APR','MAY','JUN',
     1         'JUL','AUG','SEP','OCT','NOV','DEC'/ 
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335,366/ 
C-------------------------------------------------------------------- 
C     * IF IDAY IS NOT BETWEEN 1 AND 365 (INCLUSIVE)
C     * THE MONTH RETURNS AS XXX AND MDAY IS SET TO IDAY. 
C 
      MONTH='XXX' 
      MDAY=IDAY 
      IF(IDAY.LT.1) RETURN
C 
      DO 210 I=1,12 
      IF(IDAY.GE.NFDM(I+1)) GO TO 210 
      MDAY=IDAY-NFDM(I)+1 
      MONTH=MON(I)
      RETURN
  210 CONTINUE
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
