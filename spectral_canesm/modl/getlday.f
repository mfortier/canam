!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETLDAY(LDAY,IDAY)
C
C     * JUL 29/07 - M.LAZARE. NEW ROUTINE TO GET "LDAY", BASED
C     *                       ON "GETZON9" WITHOUT THE OZONE.
C
C     * SETS "LDAY", IE FIRST DAY OFNEXT TARGET MONTH.
C
      IMPLICIT NONE
C
      INTEGER*4 MYNODE
      INTEGER NFDM(12)
      INTEGER L,N
      INTEGER IDAY      !<Variable description\f$[units]\f$
      INTEGER LDAY      !<Variable description\f$[units]\f$
      LOGICAL OK
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      COMMON /MPINFO/ MYNODE
C
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335/ 
C---------------------------------------------------------------------
C     * RESET LDAY TO FIRST DAY OF THE NEXT MONTH.
C 
      L=0 
      DO 150 N=1,12 
  150 IF(IDAY.GE.NFDM(N)) L=N+1 
      IF(L.EQ.13) L=1 
      LDAY=NFDM(L)
      IF(MYNODE.EQ.0) WRITE(6,6020) LDAY
C---------------------------------------------------------------------
 6020 FORMAT('0',3X,' LDAY RESET TO',I6) 
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
