!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE NOUVALP
     1                 ( ZP, DP, TP, QP, XP, 
     2                   LA, NK, NM, DT, ITRAC, NTRAC,
     3                   RZ, RD, RT, RQ, RX) 
C 
C     * FEB 28/94 - M.LAZARE.
C 
C     * MODELE SPECTRAL,SEMI-IMPLICITE,ELEMENTS FINIS CONSTANTS NOUVAL
C     * SOLUTION DU SCHEME EXPLICITE POUR LES TERMES PHYSICS.
C     * ADAPTE DE ROUTINE NOUVAL2.
C 
C     ON CALCULE LES QUANTITES
C 
C S          ZP(MN,K)        : Z+, TOURBILLON A T+DT. 
C S          DP(MN,K)        : D+, DIVERGENCE A T+DT. 
C S          TP(MN,K)        : T+, TEMPERATURE A T+DT.
C S          QP(MN,KM)       : Q+, VARIABLE HUMIDITE A T+DT.
C S          XP(MN,K)        : X+, F(TRACEUR) A T+DT. 
C 
C    POUR CHAQUE COMPOSANTE SPECTRALE MN ET CHAQUE COUCHE K 
C 
C E          LA              : NOMBRE TOTAL DE COMPOSANTES SPECTRALES 
C E          NK              : NOMBRE DE COUCHES. 
C E          NM              : NOMBRE DE COUCHES D'HUMIDITIE. 
C 
C     A PARTIR DES DONNEES D'ENTREE 
C 
C E          RZ(MN,K)        : COTE DROIT, EQUATION DU TOURBILLON.
C E          RD(MN,K)        : COTE DROIT, EQUATION DE LA DIVERGENCE. 
C E          RT(MN,K)        : COTE DROIT, EQUATION THERMODYNAMIQUE.
C E          RQ(MN,KM)       : COTE DROIT, EQUATION D'HUMIDITE. 
C E          RX(MN,K)        : COTE DROIT, EQUATION DU TRACEUR. 
C
C E          DT              : LONGEUR DU PAS DE TEMPS EN SECONDE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX ZP (LA,NK)        !<Variable description\f$[units]\f$
      COMPLEX DP (LA,NK)        !<Variable description\f$[units]\f$
      COMPLEX TP (LA,NK)        !<Variable description\f$[units]\f$
      COMPLEX QP (LA,NM)        !<Variable description\f$[units]\f$
      COMPLEX RZ (LA,NK)        !<Variable description\f$[units]\f$
      COMPLEX RD (LA,NM)        !<Variable description\f$[units]\f$
      COMPLEX RT (LA,NK)        !<Variable description\f$[units]\f$
      COMPLEX RQ (LA,NM)        !<Variable description\f$[units]\f$
      COMPLEX XP(LA,NK,NTRAC)   !<Variable description\f$[units]\f$
      COMPLEX RX(LA,NK,NTRAC)   !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
C     * PERFORM AN EXPLICIT TIMESTEP.  
C     * (T-1) TENDENCIES HAVE ALREADY BEEN INCLUDED IN R.H.S.'S.
C     ------------------------------------------------------------------
C 
      DO 4001 K=1,NK                                                    
           DO 4001 MN=1,LA
C 
                ZP(MN,K) = RZ(MN,K)*2.*DT 
                DP(MN,K) = RD(MN,K)*2.*DT 
                TP(MN,K) = RT(MN,K)*2.*DT 
C 
 4001 CONTINUE
C 
      DO 4002 K=1,NM                                                    
           DO 4002 MN=1,LA
C 
                QP(MN,K) = RQ(MN,K)*2.*DT 
C 
 4002 CONTINUE
C 
C 
      IF(ITRAC.NE.0) THEN 
  
      DO 4003 N=1,NTRAC 
      DO 4003 K=1,NK
           DO 4003 MN=1,LA
C 
                XP(MN,K,N)=RX(MN,K,N)*2.*DT 
C 
 4003 CONTINUE
  
      ENDIF 
C 
      RETURN
C-----------------------------------------------------------------------
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
