!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SPTEMP(IN,P,  C,  T,  ES,  PS,  TRAC,
     1                     PMB,CMB,TMB,ESMB,PSMB,TRACMB,
     2                     ITRAC,NTRAC,ILEV,LEVS,LA)
C
C     * NOV 25/94 - M.LAZARE. MEMORY STORAGE REPLACEMENT FOR INTERNAL
C     *                       SPECTRAL I/O DONE IN GCM.
C
C     *                       IN = +1 => WRITE (STORE).  
C     *                       IN = -1 => READ  (FETCH).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX P  (LA,ILEV)      !<Variable description\f$[units]\f$
      COMPLEX C  (LA,ILEV)      !<Variable description\f$[units]\f$
      COMPLEX T  (LA,ILEV)      !<Variable description\f$[units]\f$
      COMPLEX ES (LA,LEVS)      !<Variable description\f$[units]\f$
      COMPLEX PS (LA)           !<Variable description\f$[units]\f$
      COMPLEX TRAC  (LA,ILEV,NTRAC)     !<Variable description\f$[units]\f$
C
      COMPLEX PMB(LA,ILEV)      !<Variable description\f$[units]\f$
      COMPLEX CMB(LA,ILEV)      !<Variable description\f$[units]\f$
      COMPLEX TMB(LA,ILEV)      !<Variable description\f$[units]\f$
      COMPLEX ESMB(LA,LEVS)     !<Variable description\f$[units]\f$
      COMPLEX PSMB(LA)          !<Variable description\f$[units]\f$
      COMPLEX TRACMB(LA,ILEV,NTRAC)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C=====================================================================
      IF(IN.EQ.+1)                                THEN
C
C       * WRITE INTO HOLDING ARRAYS.
C
        DO 100 MN=1,LA
          PSMB(MN)=PS  (MN)
  100   CONTINUE
C
        DO 120 L=1,ILEV
        DO 120 MN=1,LA
          PMB(MN,L)=P  (MN,L)
          CMB(MN,L)=C  (MN,L)
          TMB(MN,L)=T  (MN,L)
  120   CONTINUE
C
        DO 140 L=1,LEVS
        DO 140 MN=1,LA
          ESMB(MN,L)=ES  (MN,L)
  140   CONTINUE
C
        IF(ITRAC.NE.0)                                 THEN
          DO 160 N=1,NTRAC 
          DO 160 L=1,ILEV
          DO 160 MN=1,LA
            TRACMB(MN,L,N)=TRAC  (MN,L,N)
  160     CONTINUE
        ENDIF

      ELSE
C
C       * READ FROM HOLDING ARRAYS.
C
        DO 200 MN=1,LA
          PS(MN)=PSMB(MN)
  200   CONTINUE
C
        DO 220 L=1,ILEV
        DO 220 MN=1,LA
          P  (MN,L)=PMB(MN,L)
          C  (MN,L)=CMB(MN,L)
          T  (MN,L)=TMB(MN,L)
  220   CONTINUE
C
        DO 240 L=1,LEVS
        DO 240 MN=1,LA
          ES  (MN,L)=ESMB(MN,L)
  240   CONTINUE
C
        IF(ITRAC.NE.0)                                 THEN
          DO 260 N=1,NTRAC 
          DO 260 L=1,ILEV
          DO 260 MN=1,LA
            TRAC  (MN,L,N)=TRACMB(MN,L,N)
  260     CONTINUE
        ENDIF

      ENDIF
C==================================================================
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
