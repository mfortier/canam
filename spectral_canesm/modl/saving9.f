!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SAVING9(LSSP,LSGG,LSRAD,LSBEG,LSTR,LSHF,LCSW,LCLW,
     1                   LSCOSP,
     2                   ISSP,ISGG,ISRAD,ISBEG,ISTR,ISHF,ICSW,ICLW,
     3                   ISCOSP,ISST,
     4                   LSDAY,LS3HR,LSST,DELT,NDAYS,NSECS)
C
C     * MAR 22/19 - J. COLE    ADD "LSST/ISST" TO SAVE SITE DATA
C     * NOV 03/18 - M.LAZARE.  ADD "LS3HR" FOR CMIP6 DATA OUTPUT.
C     * APR 25/10 - J.COLE.    NEW VERSION FOR GCM15I:
C     *                        - ADD ISCOSP/LSCOSP FOR COSP SIMULATOR.
C     * DEC 03/07 - L.SOLHEIM/ PREVIOUS VERSION SAVING8 FOR GCM15G/H:
C     *             M.LAZARE.  - CALLS REVISED SAVINGX INSTEAD OF SAVING.
C     *                        - IMPLICIT NONE IMPOSED WITH PROMOTION
C     *                          AS NECESSARY.
C     * FEB 15/05 - M.LAZARE.  PREVIOUS VERSION SAVING7 BEFORE GCM15G.
C
C     * SET LOGICAL SWITCHES FOR SAVING OR NOT THE SP,GG,BEG AND RAD FIELDS
C     * ALSO TO SAVE CERTAIN FIELDS EVERY DAY AND POSSIBLY AT HIGHER
C     * FREQUENCY, SAVE THE TRACERS AT DIFFERENT FREQUENCIES
C     * AND COMPUTE OR NOT THE LONGWAVE AND SHORTWAVE RADIATION.
C
      IMPLICIT NONE
C
      REAL      :: DELT         !<Variable description\f$[units]\f$	
      INTEGER*8 :: NUMSEC       !<Variable description\f$[units]\f$	
      INTEGER*8 :: NDAYS8       !<Variable description\f$[units]\f$	
      INTEGER*8 :: NSECS8       !<Variable description\f$[units]\f$	
      INTEGER   :: NDAYS        !<Variable description\f$[units]\f$	
      INTEGER   :: NSECS        !<Variable description\f$[units]\f$	
      INTEGER   :: IDT          !<Variable description\f$[units]\f$	
      INTEGER   :: ISSP         !<Variable description\f$[units]\f$	
      INTEGER   :: ISGG         !<Variable description\f$[units]\f$	
      INTEGER   :: ISRAD        !<Variable description\f$[units]\f$	
      INTEGER   :: ISBEG        !<Variable description\f$[units]\f$	
      INTEGER   :: ISDAY        !<Variable description\f$[units]\f$	
      INTEGER   :: ISTR         !<Variable description\f$[units]\f$	
      INTEGER   :: ISHF         !<Variable description\f$[units]\f$	
      INTEGER   :: ICSW         !<Variable description\f$[units]\f$	
      INTEGER   :: ICLW         !<Variable description\f$[units]\f$	
      INTEGER   :: IS3HR        !<Variable description\f$[units]\f$	
      INTEGER   :: ISCOSP       !<Variable description\f$[units]\f$	
      INTEGER   :: ISST         !<Variable description\f$[units]\f$	
      LOGICAL   :: LSSP         !<Variable description\f$[units]\f$	
      LOGICAL   :: LSGG         !<Variable description\f$[units]\f$	
      LOGICAL   :: LSRAD        !<Variable description\f$[units]\f$	
      LOGICAL   :: LSBEG        !<Variable description\f$[units]\f$	
      LOGICAL   :: LSDAY        !<Variable description\f$[units]\f$	
      LOGICAL   :: LSTR         !<Variable description\f$[units]\f$	
      LOGICAL   :: LSHF         !<Variable description\f$[units]\f$	
      LOGICAL   :: LCSW         !<Variable description\f$[units]\f$	
      LOGICAL   :: LCLW         !<Variable description\f$[units]\f$	
      LOGICAL   :: LS3HR        !<Variable description\f$[units]\f$	
      LOGICAL   :: LSCOSP       !<Variable description\f$[units]\f$	
      LOGICAL   :: LSST         !<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      NDAYS8=NDAYS
      NSECS8=NSECS
      NUMSEC=NDAYS8*86400_8+NSECS8
      IF(NUMSEC.LT.0)            CALL XIT('SAVING9',-1)  !trap overflow
      IDT   =INT(DELT)
      ISDAY =24*INT(3600./DELT)
      IS3HR = 3*INT(3600./DELT)
C
      CALL SAVINGX(LSSP ,ISSP ,IDT,NUMSEC)
      CALL SAVINGX(LSGG ,ISGG ,IDT,NUMSEC)
      CALL SAVINGX(LSRAD,ISRAD,IDT,NUMSEC)
      CALL SAVINGX(LSDAY,ISDAY,IDT,NUMSEC)
      CALL SAVINGX(LCSW, ICSW, IDT,NUMSEC)
      CALL SAVINGX(LCLW, ICLW, IDT,NUMSEC)
      CALL SAVINGX(LSBEG,ISBEG,IDT,NUMSEC)
      CALL SAVINGX(LSTR, ISTR, IDT,NUMSEC)
      CALL SAVINGX(LSHF, ISHF, IDT,NUMSEC)
      CALL SAVINGX(LS3HR,IS3HR,IDT,NUMSEC)
      CALL SAVINGX(LSCOSP, ISCOSP, IDT,NUMSEC)
      CALL SAVINGX(LSST, ISST, IDT,NUMSEC)
C
C     * TRACER SAVING FREQUENCY MUST BE A MULTIPLE OF GENERAL
C     * SPECTRAL SAVING SO THAT DIAGNOSTIC OPERATIONS SUCH AS
C     * INTERPOLATION TO PRESSURE (THROUGH THE USE OF LNSP) WILL
C     * FUNCTION.
C
      IF(MOD(ISTR,ISSP).NE.0)            CALL XIT('SAVING9',-2)
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
