!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TRACD2 (TRAC,LA,ILEV,S,GRAV,KOUNT,IEPR,ITRAC,TRACOL) 
C 
C     * SEP 03/88 - M.LAZARE.  TRACOL IS NOW WORK ARRAY INSTEAD OF
C     *                        HARD-COATED DIMENSION. 
C     * MAR 03/85 - R.LAPRISE. ORIGINAL ROUTINE TRACDIA.
C 
C     * COMPUTE AND PRINT LAYER AND GLOBAL AVERAGES OF TRACER 
C     * WHEN ITRAC.NE.0.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX TRAC(LA,ILEV)     !<Variable description\f$[units]\f$
C      
      REAL TRACOL(ILEV)         !<Variable description\f$[units]\f$
      REAL S(1)                 !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-------------------------------------------------------------------------
      IF(ITRAC.EQ.0 .OR. IEPR.EQ.0)RETURN 
      IF(MOD(KOUNT,IEPR).NE.0)     RETURN 
C 
      TRACTOT=0.
      DO 200 L=1,ILEV 
      TRACOL(L)=REAL(TRAC(1,L))/(SQRT(2.)*GRAV) 
      TRACTOT=TRACTOT+TRACOL(L)*(S(L+1)-S(L)) 
  200 CONTINUE
C 
      WRITE(6,6000)TRACTOT,(L,TRACOL(L),L=1,ILEV) 
      RETURN
C-------------------------------------------------------------------------
 6000 FORMAT('0TOTAL TRACER=',7X,E15.7,/,(I6,2X,E15.7))
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
