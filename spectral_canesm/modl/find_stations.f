!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE FIND_STATIONS(LOND, LONU, LATD, LATU, ! INPUT
     1                         INVAR, ILG1,ILAT,
     2                         NSTATIONS, LON_STAT, LAT_STAT,
     3                         STN_LOC ) ! OUTPUT

        IMPLICIT NONE

!
! LOCATE THE POSITION OF THE STATIONS ON THE GAUSSIAN GRID
! THIS SUBROUTINE CAN BE CALLED IN DIFFERENT WAYS
! IF THE POINTS ARE NOT MOVING WITH TIME (FIXED OBSERVING STATIONS)
! CALL ONCE AT THE BEGINNING OF THE RUN BUT IF THE POINTS ARE MOVING
! (SATELLITE GROUND TRACK OR MAYBE A SHIP) CALL AS FREQUENTLY AS NEEDED
!

!
! PARAMETERS
!
      INTEGER, PARAMETER :: NG = 205120 ! NUMBER OF GRID POINTS IN A T213 GRID

!
! INPUT DATA
!

      INTEGER, INTENT(IN) :: INVAR !< FLAG IF THE POINTS ARE NOT CHANGING (GROUND SITES)
                                   !INVAR=0 OR THEY ARE MOVING (SATELLITE PATH) INVAR=1\f$[units]\f$
      INTEGER, INTENT(IN) :: NSTATIONS !< NUMBER OF STATIONS\f$[units]\f$
      INTEGER, INTENT(IN) :: ILG1!<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: ILAT!<Variable description\f$[units]\f$

      REAL, INTENT(IN) :: LATD(NG) !<LOWER LATITUDE IN DEGREES\f$[units]\f$
      REAL, INTENT(IN) :: LATU(NG)  !<UPPER LATITUDE IN DEGREES\f$[units]\f$

      REAL, INTENT(IN) :: LOND(NG) !<LOWER LONGITUDE IN DEGREES\f$[units]\f$
      REAL, INTENT(IN) :: LONU(NG)  !<UPPER LONGITUDE IN DEGREES\f$[units]\f$

      REAL, INTENT(IN) :: LON_STAT(NSTATIONS) !<STATION LONGITUDES (DEGREES)\f$[units]\f$
      REAL, INTENT(IN) :: LAT_STAT(NSTATIONS)  !<STATION LATITUDES (DEGREES)\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


!
! OUTPUT DATA
!

      INTEGER, INTENT(OUT) ::STN_LOC(NSTATIONS) !<INDEX OF STATION, BASED ON LIST\f$[units]\f$

!
! LOCAL DATA
!

      INTEGER ::
     1 ISTAT,
     2 IJ,
     3 MAXX

      REAL ::
     1 LOND_MAX,
     2 LONU_MIN

! COMMON BLOCK DATA

      REAL :: PTOIT,MOIST
      INTEGER :: LONSL,NLAT,ILEV,LEVS,LRLMT,ICOORD,LAY

      COMMON /SIZES/  LONSL,NLAT,ILEV,LEVS,LRLMT,ICOORD,LAY,PTOIT,MOIST

      MAXX = ILG1*NLAT

! Find the maximum and minimum points
      LOND_MAX = MAXVAL(LOND(1:MAXX))
      LONU_MIN = MINVAL(LONU(1:MAXX))

!
! Try to find the stations in the section of the earth on the local processor
      DO ISTAT = 1, NSTATIONS
         DO IJ = 1, MAXX
            IF (LAT_STAT(ISTAT) .LE. LATU(IJ) .AND.
     1           LAT_STAT(ISTAT) .GT. LATD(IJ)) THEN
! Works for most points
               IF (LON_STAT(ISTAT) .GE. LOND(IJ) .AND.
     1              LON_STAT(ISTAT) .LT. LONU(IJ)) THEN
                  STN_LOC(ISTAT) = IJ
               END IF
! Special cases for a longitude straddling zero longitude
               IF (LON_STAT(ISTAT) .LE. LOND_MAX .AND.
     1             LON_STAT(ISTAT) .LT. LONU_MIN) THEN
                  STN_LOC(ISTAT) = IJ
               END IF
               IF (LON_STAT(ISTAT) .GE. LOND_MAX) THEN
                  STN_LOC(ISTAT) = IJ
               END IF
            END IF
         END DO! IJ
      END DO ! ISTAT

      RETURN
      END SUBROUTINE FIND_STATIONS
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
