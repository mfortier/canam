!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE HYDLAB(LC,LG,ICANP1,IGND)
  
C     * AUG 13/91 - M.LAZARE. NEW VERSION FOR GCM7 IHYD=2 (ICAN=4). 
  
C     * JAN 20/91 - M.LAZARE. PREVIOUS VERSION GETGG7U FOR GCM7U. 
C 
C     * DEFINES LEVEL INDEX VALUES FOR CANOPY AND SUB-SURFACE LAND- 
C     * SURFACE SCHEME (IHYD=2) ARRAYS. 
C     * CANOPY ARRAY IS EXTENDED TO INCLUDE "URBAN" CLASS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      INTEGER LC(ICANP1)!<Variable description\f$[units]\f$
      INTEGER LG(IGND)  !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
      DO 100 L=1,ICANP1 
        LC(L)=L 
  100 CONTINUE
  
      DO 200 L=1,IGND 
        LG(L)=L 
  200 CONTINUE
  
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
