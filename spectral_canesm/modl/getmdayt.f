!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETMDAYT(MDAYT,MDAYT1,IDAY,MON,MON1)
C
C     * MAY 10/12 - M.LAZARE. NEW ROUTINE TO GET "MDAYT", 
C     *                       AND PREVIOUS VALUE "MDAYT1",
C     *                       (LATTER REQUIRED AT KOUNT=0
C     *                       ONLY). "MON" AND "MON1" ARE
C     *                       ALSO OUTPUT FOR USE IN
C     *                       GETWFH2,GETACH2 WHERE MONTH
C     *                       INDEX (1-12) IS USED INSTEAD
C     *                       IN IBUF(2).
C
C     * SETS "MDAYT", IE MID-MONTH DAY OF NEXT TARGET MONTH.
C     * ALSO, FOR USE AT KOUNT=0 ONLY, DETERMINE PREVIOUS
C     * MID-MONTH VALUE.
C     * "MON" AND "MON1" ARE ALSO OUTPUT FOR USE IN
C     * GETWFH2,GETACH2 WHERE MONTH INDEX (1-12) IS USED 
C     * INSTEAD IN IBUF(2).
C
      IMPLICIT NONE
C
      INTEGER*4 MYNODE
      INTEGER MMD(12)
      INTEGER L,M,N
      INTEGER IDAY      !<Variable description\f$[units]\f$
      INTEGER MDAYT     !<Variable description\f$[units]\f$
      INTEGER MDAYT1    !<Variable description\f$[units]\f$
      INTEGER MON       !<Variable description\f$[units]\f$
      INTEGER MON1      !<Variable description\f$[units]\f$
      LOGICAL OK
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      COMMON /MPINFO/ MYNODE
C
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
C----------------------------------------------------------------------
C     * GET NEW MID-MONTH DAY.
C
      M=0 
      DO 5 N=1,12 
        IF(IDAY.GE.MMD(N)) M=N+1
    5 CONTINUE
      IF(M.EQ.0 .OR. M.EQ.13) M=1 
      MDAYT=MMD(M) 
      MON=M
      IF(MYNODE.EQ.0) WRITE(6,6020) MDAYT
C
C     * NOW, GET PREVIOUS MID-MONTH TARGET FOR INITIALIZATION AT KOUNT=0.
C
      L=M-1
      IF(L.EQ.0) L=12
      MDAYT1=MMD(L)
      MON1=L
C---------------------------------------------------------------------
 6020 FORMAT('0',3X,' MDAYT RESET TO',I6) 
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
