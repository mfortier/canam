!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TF2 (X,XM,F,ISIZ)
C 
C     * OCT 26/87 - R.LAPRISE.
C     * APPLY SECOND PART OF TIME FILTER TO SPECTRAL VARIABLE X(ISIZ).
C     * F  IS THE TIME FILTER COEFFICIENT.
C     * XM IS X'(K-1) ON INPUT AND X''(K-1) ON OUTPUT.
C     * X  IS X (K).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX X(ISIZ)   !<Variable description\f$[units]\f$
      COMPLEX XM(ISIZ)  !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
      DO 100 I=1,ISIZ 
         XM(I) = XM(I) + F*X(I) 
  100 CONTINUE
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
