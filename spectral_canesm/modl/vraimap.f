!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE VRAIMAP (VTG,UTG,ILG,LON,ILEV,COSJ,A) 
C
C     * AUG 20/2003 - M.LAZARE. CONVERT WIND TENDENCIES FROM REAL TO
C     *                         "IMAGINARY" PRIOR TO CONVERSION TO
C     *                         SPECTRAL SPACE. SIMPLIFIED VERSION OF
C     *                         VRAIMA3 APPLICABLE FOR PHYSICS.
C 
C     * CONVERT REAL TENDENCIES TO IMAGES ONE (IN THE OLD TRADITION). 
C     * NOTE THAT IN LOOP 100, THE L.H.S.'S OF (VTG,UTG) ARE MEANT TO 
C     * CORRESPOND TO OLD MODEL VERSION (PUTG,PVTG) TO ALLOW OLD MODEL
C     * VERSION MHANLP4 TO BE USED. (SEE APPENDIX III-1 OF "ADIABATIC
C     * FORMULATION OF THE AES/CCC MULTI-LEVEL SPECTRAL GENERAL 
C     * CIRCULATION MODEL" FOR REFERENCE).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL VTG(ILG,ILEV)        !<Variable description\f$[units]\f$
      REAL UTG(ILG,ILEV)        !<Variable description\f$[units]\f$
      REAL*8 COSJ(ILG)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-------------------------------------------------------------------
      DO 100 L=1,ILEV 
      DO 100 I=1,LON 
         VTG (I,L) =-VTG (I,L) * (COSJ(I)/A)
         UTG (I,L) = UTG (I,L) * (COSJ(I)/A)
  100 CONTINUE 
C
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
