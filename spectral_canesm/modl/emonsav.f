!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!    
      SUBROUTINE EMONSAV(LSEMON,IDAY,NSECS,DELT)
C
C     * DEC 12/2018 - S.KHARIN. NEW ROUTINE TO CALCULATE LOGICAL
C     *                         SWITCH FOR END OF MONTH.
C
C     * LSEMON = LOGICAL SWITCH FOR END OF A MONTH.
C     * IDAY   = DAY OF THE YEAR.
C     * NSECS  = NUMBER OF SECONDS ELAPSED IN THE CURRENT DAY. 
C     * DELT   = MODEL TIMESTEP LENGTH (SECONDS). 
C
      IMPLICIT NONE

      LOGICAL LSEMON    !<Variable description\f$[units]\f$
      INTEGER IDAY      !<Variable description\f$[units]\f$
      INTEGER NSECS     !<Variable description\f$[units]\f$
      REAL DELT         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER MONTHEND(12),IDT,I
      DATA MONTHEND/31,59,90,120,151,181,212,243,273,304,334,365/
C-------------------------------------------------------------------- 
      LSEMON=.FALSE.
      IDT=NINT(DELT)
      DO I=1,12
        IF((IDAY-1)*86400+NSECS.EQ.MONTHEND(I)*86400-IDT)THEN
          LSEMON=.TRUE.
          EXIT
        ENDIF
      ENDDO
      RETURN
C-------------------------------------------------------------------- 
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
