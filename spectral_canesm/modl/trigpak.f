!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TRIGPAK(ILSLPAK,JLPAK,WJPAK,RADJPAK,DLONPAK,LATUPAK,
     +                   LATDPAK,WL,RADL,RADBU,RADBD,ILAT,LONSL,IJPAK)
C
C     * MAR 22/19 - J. COLE   ADD CODE TO COMPUTE BOUNDARIES.
C     * APR 10/03 - M.LAZARE. NEW VERSION WHICH CREATES FULLY 2-D
C     *                       TRIG FIELDS IN NEW CHAINED S-N PAIRS,
C     *                       FROM ZONAL VALUES.
C
C     * GETS PAK FIELDS OF SELECTED TRIG FIELDS FROM ZONAL VALUES.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * OUTPUT PAK FIELDS.
C
      REAL*8 WJPAK  (IJPAK)     !<Variable description\f$[units]\f$
      REAL*8 RADJPAK(IJPAK)     !<Variable description\f$[units]\f$
      REAL   DLONPAK(IJPAK)     !<Variable description\f$[units]\f$
      REAL   ILSLPAK(IJPAK)     !<Variable description\f$[units]\f$
      REAL   JLPAK  (IJPAK)     !<Variable description\f$[units]\f$
      REAL LATUPAK  (IJPAK)     !<Variable description\f$[units]\f$
      REAL LATDPAK  (IJPAK)     !<Variable description\f$[units]\f$
C
C     * INPUT ZONAL ARRAYS.
C
      REAL*8 WL(ILAT)           !<Variable description\f$[units]\f$
      REAL*8 RADL(ILAT)         !<Variable description\f$[units]\f$
      REAL*8 RADBU  (ILAT)      !<Variable description\f$[units]\f$
      REAL*8 RADBD  (ILAT)      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      INTEGER*4 MYNODE
C
      COMMON /SIZES/  ILG,NLAT,ILEV,LEVS,LRLMT,ICOORD,LAY,PTOIT,MOIST
      COMMON /MPINFO/ MYNODE
C---------------------------------------------------------------------
C     * MPI HOOK.
C
      JSTART=MYNODE*ILAT+1
      JEND  =MYNODE*ILAT+ILAT
C
C     * FIRST CALCULATE "TRUE" LATITUDE INDEX ARRAY.
C
      IF(MOD(NLAT,4).NE.0)          CALL XIT('TRIGPAK',-1)
      NLATH=NLAT/2
      NLATQ=NLAT/4
      NI=0
      J=0
      DO JQ=1,NLATQ
C
        JSP = JQ
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JSP)
          ENDDO
        ENDIF
C
        JNP = NLAT-JQ+1
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JNP)
          ENDDO
        ENDIF
C
        JSE = NLATH-JQ+1
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JSE)
          ENDDO
        ENDIF
C
        JNE = NLATH+JQ
        J=J+1
        IF(J.GE.JSTART .AND. J.LE.JEND) THEN
          DO I=1,LONSL
            NI=NI+1
            JLPAK(NI)=REAL(JNE)
          ENDDO
        ENDIF
      ENDDO
C
C     * NOW THE REMAINING ARRAYS.
C
      NI=0
      DO J=1,ILAT
         DO I=1,LONSL
            NI=NI+1
            RADJPAK(NI) = RADL (J)
            WJPAK  (NI) = WL   (J)
            ILSLPAK(NI) = REAL(I)
            DLONPAK(NI) = 360.*((ILSLPAK(NI)-1.)/REAL(LONSL))
            LATUPAK(NI) = RADBU(J)
            LATDPAK(NI) = RADBD(J)
         ENDDO
      ENDDO
C---------------------------------------------------------------------
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
