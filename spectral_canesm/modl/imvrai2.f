!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE IMVRAI2 (UG,VG,PSDLG,PSDPG, COSJ,ILG,LON,ILEV,A) 
C
C     * OCT 15/92 - M.LAZARE. COSJ NOW A PASSED ARRAY FOR MULTI-
C     *                       LATITUDE FORMULATION.   
C     * JUL 14/92 - PREVIOUS VERSION IMAVRAI.
C
C     * CONVERT WIND IMAGES AND D(PS)/DX AND /DY TO REAL VALUES.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL   UG(ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL   VG(ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL   PSDLG(ILG)         !<Variable description\f$[units]\f$
      REAL   PSDPG(ILG)         !<Variable description\f$[units]\f$
      REAL*8 COSJ(ILG)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C---------------------------------------------------------------- 
      DO 200 L=1,ILEV 
         DO 100 I=1,LON 
            UG(I,L)   = UG(I,L)     *(A/COSJ(I)) 
            VG(I,L)   = VG(I,L)     *(A/COSJ(I)) 
  100    CONTINUE 
  200 CONTINUE
C 
      DO 300 I=1,LON
            PSDLG(I)  = PSDLG(I)*(1./(A*COSJ(I)))
            PSDPG(I)  = PSDPG(I)*(1./(A*COSJ(I)))
  300 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
