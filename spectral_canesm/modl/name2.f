!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE NAME2(NAM,IC,INTEG)

C     * APR 17/15 - M.LAZARE/   NEW VERSION FOR GCM18+:
C     *             J.COLE/     - RENAME "INT" AS "INTEG"
C     *             F.MAJAESS.    TO AVOID CONFLICT WITH INTRINSIC
C     *                           OF SAME NAME. THIS IS A PURELY
C     *                           COSMETIC MODIFICATION SO THE SAME
C     *                           NAME IS KEPT.    
C     * AUG 25/06 - F.MAJAESS - NEW VERSION FOR GCM15F: 
C     *                         MAKE IT WORK IN 32-BITS MODE AS WELL.
C     * NOV 04/97 - M.HOLZER,R.HARVEY. PREVIOUS VERSION NAME.
C     *                                LIKE "NOM" EXCEPT WRITES OUT IN
C     *                                FORMAT "XM01". 
C     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.)
C
C     * SUBROUTINE QUI DEFINIE LE NOM DES CHAMPS ASSOCIE AUX DIFFERENTS
C     * TRACEURS EN FONCTION DU NOMBRE DE TRACEURS.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      CHARACTER CNAM*4
      CHARACTER*1 IC(2) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C----------------------------------------------------------------------
      N2=INTEG/10
      N1=INTEG-N2*10
      IF(INTEG.GT.99) CALL                           XIT('NAME2',-1)
      IF(IC(2).EQ.' ') IC(2)='0'
      WRITE(CNAM,1000) IC,N2,N1
      NAM=NC4TO8(CNAM)

 1000 FORMAT(2A1,2I1)
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
