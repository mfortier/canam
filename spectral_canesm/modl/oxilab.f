!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE OXILAB(LX,LEVOX,IOXTYP)

C     * AUG 15/2020 - M.LAZARE. NEW ROUTINE BASED ON OZLAB2.
C
C     * DEFINES LEVEL INDEX VALUES FOR INPUT OXIDANTS FIELDS.
C
      implicit none
C
      INTEGER, INTENT(in) :: levox      !<Variable description\f$[units]\f$
      INTEGER, INTENT(in) :: ioxtyp     !<Variable description\f$[units]\f$
      INTEGER, INTENT(out) :: lx(levox) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C     * PRESSURE-LEVEL DATA FOR IOZTYP=1.
C
      INTEGER, PARAMETER, DIMENSION(19) :: 
     &  p1 = (/ -100, -500,   10,   20,   30,
     &            50,   70,  100,  150,  200,
     &           250,  300,  400,  500,  600,
     &           700,  850,  925, 1000 /)
C
      INTEGER l
C
C-----------------------------------------------------------------------
      IF(ioxtyp.eq.1)     THEN
        DO l=1,levox
          lx(l)=p1(l)
        ENDDO
      ELSE
        CALL XIT('OXILAB',-1)
      ENDIF
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
