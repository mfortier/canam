!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE QDTFPC3(Q,D,P,C,RNS2LA,LA,ILEV,KASE)
C
C     * NOV 12/03 - M.LAZARE. ACCOUNT FOR FACT THAT RNS2LA(1) MAY NOT
C     *                       BE ZERO IN A MULTI-NODE ENVIRONMENT, IE
C     *                       IT IS ONLY ZERO ON NODE 0.
C     * DEC 10/97 - B.DENIS.  PREVIOUS VERSION QDTFPC2.
C 
C     * APR 11/79 - J.D.HENDERSON. ORIGINAL VERSION QDTFPC.
C
C     * CONVERTS BETWEEN (P,C) AND (Q,D) BASED ON VALUE OF KASE.
C 
C     * Q(LA,ILEV) = SPECTRAL VORTICITY 
C     * D(LA,ILEV) = SPECTRAL DIVERGENCE
C     * P(LA,ILEV) = SPECTRAL STREAMFUNCTION
C     * C(LA,ILEV) = SPECTRAL VELOCITY POTENTIAL
C 
C     * KASE = +1 CONVERTS P,C TO Q,D.
C     * KASE = -1 CONVERTS Q,D TO P,C.
C 
C     * LEVEL 2,Q,D,P,C 

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL RNS2LA(LA)           !<Variable description\f$[units]\f$
      COMPLEX Q(LA,ILEV)        !<Variable description\f$[units]\f$
      COMPLEX D(LA,ILEV)        !<Variable description\f$[units]\f$
      COMPLEX P(LA,ILEV)        !<Variable description\f$[units]\f$
      COMPLEX C(LA,ILEV)        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-------------------------------------------------------------------
C     * KASE = +1 CONVERTS P,C TO Q,D.
C 
      IF(KASE.LT.0) GO TO 310 
      DO 290 L=1,ILEV 
      DO 290 K=1,LA
        Q(K,L)=-1.*RNS2LA(K)*P(K,L) 
        D(K,L)=-1.*RNS2LA(K)*C(K,L) 
  290 CONTINUE
      RETURN
C 
C     * KASE = -1 CONVERTS Q,D TO P,C.
C 
  310 CONTINUE
      DO 390 L=1,ILEV 
        IF(RNS2LA(1).EQ.0.) THEN
          P(1,L)=0.
          C(1,L)=0.
        ELSE
          RFNS1 =1./RNS2LA(1)
          P(1,L)=-RFNS1*Q(1,L)
          C(1,L)=-RFNS1*D(1,L)
        ENDIF
C 
        DO 380 K=2,LA
          RFNS1 =1./RNS2LA(K)
          P(K,L)=-RFNS1*Q(K,L)
          C(K,L)=-RFNS1*D(K,L)
  380   CONTINUE
  390 CONTINUE
C 
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
