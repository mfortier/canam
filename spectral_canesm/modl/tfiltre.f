!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TFILTRE(P,C,T,Q,PS, PM,CM,TM,QM,PSM, 
     1                   FP,FC,FT,FQ,FPS, ILEV,LEVS,LA, 
     2                   LTF2)
C
C     * FEB 26/96 - M.LAZARE. BRACKET CALLS FOR MOISTURE SO NOT
C     *                       CALLED IF LEVS PASSED IN AS ZERO (TO
C     *                       SUPPORT SEMILAGRANGIAN CODE). 
C     * OCT 26/87 - R.LAPRISE.
C
C     * APPLY A ROBERT TIME FILTER ON MODEL VARIABLES IN TWO STEPS AS...
C     * TF1: X' (K  ) = (1-2F) X (K  ) + F X''(K-1),
C     * TF2: X''(K-1) =        X'(K-1) + F X  (K  ).
C     * 
C     * TF2 SHOULD BE ACTIVATED 2 STEPS FOLLOWING A FORWARD TIMESTEP, 
C     * TF1 SHOULD BE ACTIVATED 1 STEP  AFTER     A FORWARD TIMESTEP. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      LOGICAL LTF2              !<Variable description\f$[units]\f$
      COMPLEX P (LA,ILEV)       !<Variable description\f$[units]\f$
      COMPLEX C (LA,ILEV)       !<Variable description\f$[units]\f$
      COMPLEX T (LA,ILEV)       !<Variable description\f$[units]\f$
      COMPLEX Q (LA,LEVS)       !<Variable description\f$[units]\f$
      COMPLEX PS (LA)           !<Variable description\f$[units]\f$
      COMPLEX PM(LA,ILEV)       !<Variable description\f$[units]\f$
      COMPLEX CM(LA,ILEV)       !<Variable description\f$[units]\f$
      COMPLEX TM(LA,ILEV)       !<Variable description\f$[units]\f$
      COMPLEX QM(LA,LEVS)       !<Variable description\f$[units]\f$
      COMPLEX PSM(LA)           !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
      IF(LTF2)THEN
         CALL TF2 (  P,  PM,  FP, LA*ILEV ) 
         CALL TF2 (  C,  CM,  FC, LA*ILEV ) 
         CALL TF2 (  T,  TM,  FT, LA*ILEV ) 
         IF(LEVS.GT.0)                  THEN
            CALL TF2 (  Q,  QM,  FQ, LA*LEVS ) 
         ENDIF
         CALL TF2 ( PS, PSM, FPS, LA      ) 
      ENDIF 
C 
      CALL TF1 (  P,  PM,  FP, LA*ILEV )
      CALL TF1 (  C,  CM,  FC, LA*ILEV )
      CALL TF1 (  T,  TM,  FT, LA*ILEV )
      IF(LEVS.GT.0)                  THEN
         CALL TF1 (  Q,  QM,  FQ, LA*LEVS )
      ENDIF
      CALL TF1 ( PS, PSM, FPS, LA      )
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
