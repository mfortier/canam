!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE FILLNAB(TRACNA,FSLICE,WFS,FAC,IOFF,
     1                   NTRAC,NTRACN,NTRSPEC,ITRADV,
     2                   ILEV,IJPAK,ILG,IL1,IL2)

C     * DEC 10/03 - M.LAZARE.   CONSISTENT WITH NEW METHODOLOGY FOR
C     *                         IBM PORT.
C     * OCT 29/02 - J.SCINOCCA. PREVIOUS VERSION FILNAB.

C     * UPDATES TRACNA WITH PHYSICS TENDENCIES (FSLICE) FOR NON-ADVECTED
C     * TRACERS. OTHERWISE, TENDENCIES ARE STORED INTO "WFS".
C     * "IOFF" IS OFFSET INTO TRACNA FOR THE PARTICULAR TASK.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL TRACNA (IJPAK,ILEV,NTRACN)   !<Variable description\f$[units]\f$
      REAL FSLICE (ILG,ILEV,NTRAC)      !<Variable description\f$[units]\f$
      REAL WFS(ILG,ILEV,NTRSPEC)        !<Variable description\f$[units]\f$
      INTEGER ITRADV(NTRAC)             !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C--------------------------------------------------------------------
      NA=0
      NNA=0
      DO 300 N=1,NTRAC
         IF(ITRADV(N).EQ.1) THEN
            NA=NA+1
            DO L=1,ILEV
               DO I=IL1,IL2
                  WFS(I,L,NA)=FSLICE(I,L,N)
               ENDDO
            ENDDO
         ELSE
            NNA=NNA+1
            DO L=1,ILEV
               DO I=IL1,IL2
                  TRACNAO=TRACNA(IOFF+I,L,NNA)
                  TRACNA(IOFF+I,L,NNA)=MAX(TRACNAO+FAC*FSLICE(I,L,N),0.)
               ENDDO
            ENDDO
         ENDIF
 300     CONTINUE

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
