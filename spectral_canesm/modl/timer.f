!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!        
      function timer(date_time)
c
c     * timing function called by gcm driver.
c
c     * sep 06/2006 - m.lazare.
c
      implicit none

      real*8 timer
      integer date_time(8)      !<Variable description\f$[units]\f$
      real*8 dp001,done,dsim,dmih,dhid
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      data dp001,done,dsim,dmih,dhid  /0.001, 1., 60., 3600., 86400./
C====================================================================
      timer = date_time(8)*dp001 + date_time(7)*done 
     &      + date_time(6)*dsim  + date_time(5)*dmih
     &      + date_time(4)*dhid
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
