!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CALCNS3(RNS2LA,LSR,MINDEX,LM,LA)
C 
C     * NOV 03/03 - M.LAZARE. NEW VERSION WHICH USES "MINDEX" (LOCAL
C     *                       TO EACH NODE), TO DETERMINE THE LOCAL NODE
C     *                       VALUES FOR "RNS2LA".  
C     * JUL 14/92 - E.CHAN.   PREVIOUS VERSION CALCNS2.
C
C     * CALCULATES ARRAY (LENGTH LA) OF N*N+1 VALUES FOR SUBSEQUENT
C     * USE IN MODEL. SINCE THIS ARRAY IS INVARIANT DURING THE COURSE
C     * OF A MODEL SIMULATION, IT IS PRE-COMPUTED ONLY ONCE AT THE 
C     * BEGINNING OF A JOB.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      INTEGER LSR(2,LM+1)       !<Variable description\f$[units]\f$
      INTEGER MINDEX(LM)        !<Variable description\f$[units]\f$
      REAL RNS2LA(LA)           !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------
      DO 300 M=1,LM 
        MS=MINDEX(M)-1
        KL=LSR(1,M) 
        KR=LSR(1,M+1)-1 
        DO 290 K=KL,KR
          NS=MS+(K-KL) 
          RNS2LA(K)=REAL(NS*(NS+1)) 
  290   CONTINUE
  300 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
