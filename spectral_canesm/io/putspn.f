!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PUTSPN(NF,G,LA,
     1                  LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                  JBUF,MAXJ,IPIOSW,OK)

C     * OCT 11/06 - F.MAJAESS (REPLACE DIRECT BINARY "DATA" RECORD 
C     *                        WRITES BY "PUTFLD2" CALLS).
C     * NOV 04/03 - M.LAZARE. NEW VERSION TO SUPPORT MPI, WHICH USES
C     *                       NEW SWITCH "IPIOSW" AND WORK ARRAY "GLL".
C
C     * SAVES GLOBAL SPECTRAL FORECAST ON RESTART FILE.
C     * ALL FIELDS ARE WRITTEN UNPACKED.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX G(LA)                     !<Variable description\f$[units]\f$
      COMPLEX GLL(LATOTAL)              !<Variable description\f$[units]\f$

      INTEGER LSRTOTAL(2,LMTOTAL+1)     !<Variable description\f$[units]\f$
      INTEGER JBUF(8)                   !<Variable description\f$[units]\f$
  
      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE 

      INTEGER*4 NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE, MY_REAL_TYPE
      INTEGER*4 MY_INT_TYPE
      COMMON /MPICOMM/ NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE,MY_REAL_TYPE,
     1                 MY_INT_TYPE

      INTEGER*4 SZ, MASTER, IERR

      LOGICAL OK                        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


C     * ICOM IS A SHARED I/O AREA. IT MUST BE LARGE ENOUGH
C     * FOR AN 8 WORD LABEL FOLLOWED BY A PACKED GAUSSIAN GRID.
C
      COMMON /ICOM/ IBUF(8),IDAT(1)
C-------------------------------------------------------------------- 


      IF (NNODE.GT.1 .AND. IPIOSW.EQ.0) THEN
C
C       * MPI CALL TO GATHER DATA IN LOAD-BALANCED (NON-TRIANGULAR) ORDER.
C
        MASTER =  0
        SZ     =  LA
        CALL WRAP_GATHER(G,      SZ,            MY_CMPLX_TYPE,
     1                   GLL,    SZ,            MY_CMPLX_TYPE,
     2                   MASTER, AGCM_COMMWRLD, IERR)
  
        IF(MYNODE.EQ.0) THEN

          IBUF(1:8)=JBUF(1:8)
          IBUF(5)  =LATOTAL
          MAXX=2*LATOTAL
          CALL PUTFLD2(NF,GLL,IBUF,MAXX)

        ENDIF

      ELSE
         
         IBUF(1:8)=JBUF(1:8)
         IBUF(5)=LA
         MAXX=2*LA
         CALL PUTFLD2(NF,G,IBUF,MAXX)

      ENDIF
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
