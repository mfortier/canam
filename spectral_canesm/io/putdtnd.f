!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PUTDTND(NF ,PTD,CTD,TTD,ESTD,
     1                   KOUNT,LA,LRLMT,ILEV,LEVS,LS,LH,
     2                   LSRTOTAL,LATOTAL,LMTOTAL,GLL)

C     * FEB 29/2012 - M.LAZARE. 
C
C     * SAVES GLOBAL SPECTRAL DYNAMICS TENDENCIES ON SEQUENTIAL FILE NF. 
C     * ALL FIELDS ARE WRITTEN UNPACKED.
C     * LS,LH = OUTPUT LABEL VALUES FOR FULL,HALF LEVELS. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX TTD(LA,ILEV)          !<Variable description\f$[units]\f$
      COMPLEX PTD(LA,ILEV)          !<Variable description\f$[units]\f$
      COMPLEX CTD(LA,ILEV)          !<Variable description\f$[units]\f$
      COMPLEX ESTD(LA,LEVS)         !<Variable description\f$[units]\f$
      COMPLEX GLL(LATOTAL)          !<Variable description\f$[units]\f$

      INTEGER LSRTOTAL(2,LMTOTAL+1) !<Variable description\f$[units]\f$
      INTEGER LS(ILEV)              !<Variable description\f$[units]\f$
      INTEGER LH(ILEV)              !<Variable description\f$[units]\f$
      INTEGER IBUF(8)           
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
  
      LOGICAL OK

      COMMON /IPARIO/  IPIO
      COMMON /KEEPTIM/ IYEAR,IYMDH,MYRSSTI,ISAVDTS
C-------------------------------------------------------------------- 
      MAX=2*LATOTAL
C
       ITMP=NC4TO8(" TTD")
      IVORT=NC4TO8(" PTD")
       IDIV=NC4TO8(" CTD")
        IES=NC4TO8("ESTD")

C     * DETERMINE PROPER IBUF(2) TO USE FOR SAVED FIELDS, BASED ON
C     * VALUE OF OPTION SWITCH "ISAVDTS".
C
      IF(ISAVDTS.NE.0)                   THEN
         IBUF2=IYMDH
      ELSE
         IBUF2=KOUNT
      ENDIF
C
      CALL SETLAB(IBUF,NC4TO8("SPEC"),IBUF2,-1,1,-1,1,LRLMT,0)
C 
C     * SAVE TEMPERATURE TENDENCIES FOR ILEV LEVELS. 
C
      IBUF(3)=ITMP 
      DO 310 L=1,ILEV 
         IBUF(4)=LH(L) 
         CALL PUTSPN (NF,TTD(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  310 CONTINUE
C 
C     * SAVE VORTICITY AND DIVERGENCE TENDENCIES IN PAIRS FOR EACH LEVEL.
C 
      DO 410 L=1,ILEV 
         IBUF(4)=LS(L) 

         IBUF(3)=IVORT
         CALL PUTSPN (NF,PTD(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)

         IBUF(3)=IDIV
         CALL PUTSPN (NF,CTD(1,L),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  410 CONTINUE
C 
C     * MOISTURE VARIABLE TENDENCIES SAVED FOR LEVS LEVELS.
C 
      IF(LEVS.EQ.0) RETURN
      IBUF(3)=IES
      DO 510 N=1,LEVS 
         L=(ILEV-LEVS)+N 
         IBUF(4)=LH(L) 
         CALL PUTSPN (NF,ESTD(1,N),LA,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2                IBUF,MAX,IPIO,OK)
  510 CONTINUE
C 
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
