!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PUTNATR3(NF,TRACNAP,LON1,ILAT,IJPAK,ILEV,LH,KOUNT,LSTR,
     1                    KHEM,NTRAC,ITRNAM,ITRLVS,ITRLVF,
     2                    INDXNA,NTRACN,GLL,WRKS) 
C
C     * JAN 05/04 - M.LAZARE.   MODIFIED FOR REVISED MODEL STRUCTURE
C     *                         FOLLOWING IBM CONVERSION.
C     * OCT 29/02 - J.SCINOCCA. PREVIOUS VERSION PUTNATR.
C  
C     * SAVE TRACER FIELD ONTO MODEL GRID HISTORY FILE, 
C     * IF NTRACN.NE.0 (WHEN SPECTRAL MODEL OPTION SELECTED).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C 
      REAL TRACNAP(IJPAK,ILEV,NTRACN) !<Variable description\f$[units]\f$
      REAL GLL(*)                     !<Variable description\f$[units]\f$
      REAL WRKS(1)                    !<Variable description\f$[units]\f$
      INTEGER LH(ILEV)                !<Variable description\f$[units]\f$  
      INTEGER ITRNAM(NTRAC)           !<Variable description\f$[units]\f$  
      INTEGER ITRLVS(NTRAC)           !<Variable description\f$[units]\f$  
      INTEGER ITRLVF(NTRAC)           !<Variable description\f$[units]\f$
      INTEGER INDXNA(NTRACN)          !<Variable description\f$[units]\f$
      LOGICAL LSTR                    !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      COMMON /KEEPTIM/ IYEAR,IMDH,MYRSSTI,ISAVDTS
C-------------------------------------------------------------------------
      IF(NTRACN.EQ.0 .OR. .NOT.LSTR) RETURN
C
C     * DETERMINE PROPER IBUF(2) TO USE FOR SAVED FIELDS, BASED ON
C     * VALUE OF OPTION SWITCH "ISAVDTS".
C
      IF(ISAVDTS.NE.0)                   THEN
C        * IN 32-BIT, THIS ONLY WORKS UNTIL IYEAR=2147!
         IBUF2=1000000*IYEAR + IMDH
      ELSE
         IBUF2=KOUNT
      ENDIF
      NPGG=0
C
      DO 300 NN=1,NTRACN
         N=INDXNA(NN)
         IBUF3=ITRNAM(N)
         DO 200 L=itrlvs(n),itrlvf(n)
           CALL PUTGG(TRACNAP(1,L,NN),LON1,ILAT,KHEM,NPGG,IBUF2,NF,
     1                  IBUF3,LH(L),GLL,WRKS)
  200    CONTINUE
  300 CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
