!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RPKNAPHS(NF,G,NLEV,IBUF,LON,NLAT,ILAT,LEV,
     1                    ITRLVS,ITRLVF,LH,GG,OK) 

C     *                        NEW ROUTINE, BASED ON RPKPHS4, FOR NA TRACERS
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
  
      INTEGER IBUF(8)           !<Variable description\f$[units]\f$
      INTEGER KBUF(8)
      INTEGER LH(NLEV)          !<Variable description\f$[units]\f$
      REAL G ( LON*ILAT+1, NLEV)!<Variable description\f$[units]\f$
      REAL GG( (LON+1)*NLAT)    !<Variable description\f$[units]\f$
      LOGICAL OK                !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
      OK = .FALSE. 
      LON1=LON+1
C
      DO 30 I=1,8
          KBUF(I)=IBUF(I)
   30 CONTINUE
C
      DO 100 L = ITRLVS,ITRLVF
         KBUF(4) = LH(L)
         CALL GETGGBX(G(1,L),KBUF(3),NF,LON1,NLAT,KBUF(2),KBUF(4),GG)
  100 CONTINUE
C
      DO 300 I=1,8
          IBUF(I)=KBUF(I)
  300 CONTINUE
C
      OK = .TRUE. 
      RETURN
C-------------------------------------------------------------------- 
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
