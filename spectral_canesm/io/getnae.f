!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETNAE(NF,ESCVPAK,EHCVPAK,ESEVPAK,EHEVPAK,
     1                  IJPAK,NLON,NLAT,GG)
C
C     * KNUT VON SALZEN - FEB 07,2009. NEW ROUTINE FOR GCM15H TO READ IN
C     *                                VOLCANIC FIELDS: ESCV,EHCV,ESEV,
C     *                                EHEV. (USED TO BE DONE BEFORE IN
C     *                                GETCHEM2).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * INVARIANT FIELDS:
C
      REAL ESCVPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL EHCVPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ESEVPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL EHEVPAK(IJPAK) !<Variable description\f$[units]\f$
C
C     * WORK FIELD:
C
      REAL GG(1)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


C-----------------------------------------------------------------------
C     * GRIDS READ FROM FILE NF.

      REWIND NF
C
      CALL GETGGBX(ESCVPAK,NC4TO8("ESCV"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(EHCVPAK,NC4TO8("EHCV"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ESEVPAK,NC4TO8("ESEV"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(EHEVPAK,NC4TO8("EHEV"),NF,NLON,NLAT,0,1,GG)
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
