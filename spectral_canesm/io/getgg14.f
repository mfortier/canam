!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETGG14(NF,TBARPAT,THLQPAT,THICPAT,
     1                   SICNPAK,SICPAK,GTPAK,SNOPAK,
     1                   LUINPAK,LUIMPAK,LRINPAK,LRIMPAK,
     2                   ENVPAK,GAMPAK,PSIPAK,ALPHPAK,DELTPAK,SIGXPAK,
     3                   ALSWPAK,ALLWPAK,PHISPAK,
     4                   SANDPAT,CLAYPAT,ORGMPAT,
     5                   DPTHPAT,DRNPAT,SOCIPAT,
     6                   FCANPAT,ALVCPAT,ALICPAT,LNZ0PAT,                   
     7                   LAMXPAT,LAMNPAT,CMASPAT,ROOTPAT,
     8                   HLAKPAK,LLAKPAK,BLAKPAK,
     9                   FLKUPAK,FLKRPAK,GICNPAK,FLNDPAK,
     A                   LC,LG,LCT,LGT,ICAN,ICANP1,IGND,NTLD,IJPAK,
     B                   NLON,NLAT,IDAY,GG)          
C
C     * Jun 05/20 - M.Lazare. For initialization, {GT,SIC,SICN} added.      
C     * AUG 14/18 - M.LAZARE. REMOVE MASKPAK.
C     * NOV 29/16 - M.LAZARE. NEW VERSION FOR GCM19+:
C     *                       - ADD "LLAK" AND "BLAK" FOR DIANA.
C     *                       - "FLAK" -> "FLKU" AND ADD "FLKR".
C     *                       - {"LICN","LIC"} -> {"LUIN","LUIM","LRIN","LRIM"}.
C     *                       - ADD "HLAK" FIELD.
C     *                       - "LUIN" -> "GICN".
C     *                       - ADD {"LGT","LUIN","LIC"} IN CONJUNCTION
C     *                         WITH CHANGES TO NEW INITIALIZATION
C     *                         PROGRAM INITG14.
C     * AUG 08/14 - M.LAZARE. NEW VERSION FOR GCM18:
C     *                       - ADD {FLAK,LICN,FLND)PAK AND SOCIPAT IN 
C     *                         CONJUNCTION WITH CHANGES TO INITIALIZATION.
C     *                       - TBAR,THLQ,THIC ARE ALSO "PAT" ARRAYS NOW.
C     * FEB 14/13 - M.LAZARE. PREVIOUS VERSION GETGG13 FOR GCM17 USING
C     *                       CLASS_V3.6 AND TILES.
C     * MAR 30/09 - M.LAZARE. PREVIOUS VERSION GETGG12 FOR GCM15I:
C     *                       - PHISPAK ADDED.
C     * JAN 17/08 - M.LAZARE. PREVIOUS VERSION GETGG11 FOR GCM15G/H.
C     *                       - EVOL,HVOL,ESO2 REMOVED (NOT
C     *                         USED SINCE NEW AEROCOM).
C     * DEC 15/03 - M.LAZARE. PREVIOUS VERSION GETGG10X FOR GCM15B->F.
C     *                       - "EVOL","HVOL" AND "ESO2" ADDED.
C     * MAY 30/03 - M.LAZARE. PREVIOUS VERSION GETGG9X FOR GCM13B.
C                                                                              
C     * GETS GAUSSIAN GRIDS (NLON,NLAT) FROM FILE NF FOR THE GCM.              
C     * IDAY IS THE DAY OF THE YEAR.                                           
C     * INPUT GRIDS CAN BE IN ANY ORDER ON THE FILE.                           
C     * ROW LENGTH IN THE MODEL IS NLON-1.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
C     * FIELDS READ-IN FOR INITIAL IDAY:
C
      REAL TBARPAT(IJPAK,NTLD,IGND) !<Variable description\f$[units]\f$
      REAL THLQPAT(IJPAK,NTLD,IGND) !<Variable description\f$[units]\f$
      REAL THICPAT(IJPAK,NTLD,IGND) !<Variable description\f$[units]\f$

      REAL SICNPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL SICPAK (IJPAK)           !<Variable description\f$[units]\f$
      REAL GTPAK  (IJPAK)           !<Variable description\f$[units]\f$
      REAL SNOPAK (IJPAK)           !<Variable description\f$[units]\f$
      REAL LUINPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL LUIMPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL LRINPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL LRIMPAK(IJPAK)           !<Variable description\f$[units]\f$
C
C     * INVARIANT FIELDS:
C
      REAL SANDPAT(IJPAK,NTLD,IGND) !<Variable description\f$[units]\f$
      REAL CLAYPAT(IJPAK,NTLD,IGND) !<Variable description\f$[units]\f$
      REAL ORGMPAT(IJPAK,NTLD,IGND) !<Variable description\f$[units]\f$
      REAL FCANPAT(IJPAK,NTLD,ICANP1) !<Variable description\f$[units]\f$
      REAL ALVCPAT(IJPAK,NTLD,ICANP1) !<Variable description\f$[units]\f$
      REAL ALICPAT(IJPAK,NTLD,ICANP1) !<Variable description\f$[units]\f$
      REAL LNZ0PAT(IJPAK,NTLD,ICANP1) !<Variable description\f$[units]\f$
      REAL LAMXPAT(IJPAK,NTLD,ICAN) !<Variable description\f$[units]\f$
      REAL LAMNPAT(IJPAK,NTLD,ICAN) !<Variable description\f$[units]\f$
      REAL CMASPAT(IJPAK,NTLD,ICAN) !<Variable description\f$[units]\f$
      REAL ROOTPAT(IJPAK,NTLD,ICAN) !<Variable description\f$[units]\f$

      REAL DPTHPAT(IJPAK,NTLD)      !<Variable description\f$[units]\f$
      REAL DRNPAT (IJPAK,NTLD)      !<Variable description\f$[units]\f$
      REAL SOCIPAT(IJPAK,NTLD)      !<Variable description\f$[units]\f$

      REAL ALSWPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL ALLWPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL ENVPAK (IJPAK)           !<Variable description\f$[units]\f$
      REAL GAMPAK (IJPAK)           !<Variable description\f$[units]\f$
      REAL PSIPAK (IJPAK)           !<Variable description\f$[units]\f$
      REAL ALPHPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL DELTPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL SIGXPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL GICNPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL FLNDPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL HLAKPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL LLAKPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL BLAKPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL FLKUPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL FLKRPAK(IJPAK)           !<Variable description\f$[units]\f$
      REAL PHISPAK(IJPAK)           !<Variable description\f$[units]\f$
C
C     * WORK FIELD:
C
      REAL GG(1)                    !<Variable description\f$[units]\f$                                               
C
C     * LEVEL INDEX INPUT ARRAYS:
C
      INTEGER LC(ICANP1)            !<Variable description\f$[units]\f$
      INTEGER LG(IGND)              !<Variable description\f$[units]\f$
      INTEGER LCT(NTLD*ICANP1)      !<Variable description\f$[units]\f$
      INTEGER LGT(NTLD*IGND)        !<Variable description\f$[units]\f$                                    
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------      
C     * GRIDS READ FROM FILE NF.                                               

      REWIND NF                 

      CALL GETGGBX(SICNPAK,NC4TO8("SICN"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(SICPAK ,NC4TO8(" SIC"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(LUINPAK,NC4TO8("LUIN"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(LUIMPAK,NC4TO8("LUIM"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(LRINPAK,NC4TO8("LRIN"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(LRIMPAK,NC4TO8("LRIM"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(GTPAK  ,NC4TO8("  GT"),NF,NLON,NLAT,IDAY,1,GG)
      CALL GETGGBX(SNOPAK ,NC4TO8(" SNO"),NF,NLON,NLAT,IDAY,1,GG)
C
      ML=0
      DO 10 L=1,IGND 
      DO 10 M=1,NTLD
        ML=ML+1
        CALL GETGGBX(TBARPAT(1,M,L),NC4TO8("TBAR"),NF,NLON,NLAT,IDAY,
     1               LGT(ML),GG)
        CALL GETGGBX(THLQPAT(1,M,L),NC4TO8("THLQ"),NF,NLON,NLAT,IDAY,
     1               LGT(ML),GG)  
        CALL GETGGBX(THICPAT(1,M,L),NC4TO8("THIC"),NF,NLON,NLAT,IDAY,
     1               LGT(ML),GG)    
   10 CONTINUE                    
C
      CALL GETGGBX( ENVPAK,NC4TO8("  SD"),NF,NLON,NLAT,0,1,GG) 
      CALL GETGGBX( GAMPAK,NC4TO8(" GAM"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX( PSIPAK,NC4TO8(" PSI"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ALPHPAK,NC4TO8("ALPH"),NF,NLON,NLAT,0,1,GG) 
      CALL GETGGBX(DELTPAK,NC4TO8("DELT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(SIGXPAK,NC4TO8("SIGX"),NF,NLON,NLAT,0,1,GG)
C
      DO M=1,NTLD
        CALL GETGGBX(DPTHPAT(1,M),NC4TO8("DPTH"),NF,NLON,NLAT,0,M,GG)
        CALL GETGGBX( DRNPAT(1,M),NC4TO8(" DRN"),NF,NLON,NLAT,0,M,GG)
        CALL GETGGBX(SOCIPAT(1,M),NC4TO8("SOCI"),NF,NLON,NLAT,0,M,GG)
      ENDDO
C
      DO 20 L=1,IGND
        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(SANDPAT(1,M,L),NC4TO8("SAND"),NF,NLON,NLAT,0,
     1                 LGT(ML),GG)  
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(CLAYPAT(1,M,L),NC4TO8("CLAY"),NF,NLON,NLAT,0,
     1                 LGT(ML),GG)  
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(ORGMPAT(1,M,L),NC4TO8("ORGM"),NF,NLON,NLAT,0,
     1                 LGT(ML),GG)  
        ENDDO
   20 CONTINUE                          
C
      DO 30 L=1,ICAN                                                          
        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(FCANPAT(1,M,L),NC4TO8("FCAN"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)      
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(ALVCPAT(1,M,L),NC4TO8("ALVC"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)    
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(ALICPAT(1,M,L),NC4TO8("ALIC"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)      
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(LNZ0PAT(1,M,L),NC4TO8("LNZ0"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)      
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(LAMXPAT(1,M,L),NC4TO8("LAMX"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)      
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(LAMNPAT(1,M,L),NC4TO8("LAMN"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)    
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(CMASPAT(1,M,L),NC4TO8("CMAS"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)      
        ENDDO

        DO M=1,NTLD
          ML=(L-1)*NTLD+M
          CALL GETGGBX(ROOTPAT(1,M,L),NC4TO8("ROOT"),NF,NLON,NLAT,0,
     1                 LCT(ML),GG)      
        ENDDO
   30 CONTINUE                                        
C
      L=ICANP1
      DO M=1,NTLD
        ML=(L-1)*NTLD+M
        CALL GETGGBX(FCANPAT(1,M,L),NC4TO8("FCAN"),NF,NLON,NLAT,0,
     1               LCT(ML),GG)        
      ENDDO

      DO M=1,NTLD
        ML=(L-1)*NTLD+M
        CALL GETGGBX(ALVCPAT(1,M,L),NC4TO8("ALVC"),NF,NLON,NLAT,0,
     1               LCT(ML),GG)        
      ENDDO

      DO M=1,NTLD
        ML=(L-1)*NTLD+M
        CALL GETGGBX(ALICPAT(1,M,L),NC4TO8("ALIC"),NF,NLON,NLAT,0,
     1               LCT(ML),GG)         
      ENDDO

      DO M=1,NTLD
        ML=(L-1)*NTLD+M
        CALL GETGGBX(LNZ0PAT(1,M,L),NC4TO8("LNZ0"),NF,NLON,NLAT,0,
     1               LCT(ML),GG)
      ENDDO
C
      CALL GETGGBX(ALSWPAK,NC4TO8("ALSW"),NF,NLON,NLAT,0,1,GG)          
      CALL GETGGBX(ALLWPAK,NC4TO8("ALLW"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(GICNPAK,NC4TO8("GICN"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(FLNDPAK,NC4TO8("FLND"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(FLKUPAK,NC4TO8("FLKU"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(HLAKPAK,NC4TO8("LDEP"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(LLAKPAK,NC4TO8("LLAK"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(BLAKPAK,NC4TO8("BLAK"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(FLKRPAK,NC4TO8("FLKR"),NF,NLON,NLAT,0,1,GG)
C
      CALL GETGGBX(PHISPAK,NC4TO8("PHIS"),NF,NLON,NLAT,0,1,GG)

      RETURN                                                                   
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}


