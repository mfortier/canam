!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RGTOCG2(PAKO,PAKI,LON,NLAT,ILAT)

C     * JUNE 30, 2003 - M.LAZARE.
C
C     * THIS ROUTINE TAKES A NORMAL S-N GRID FIELD, "PAKI" 
C     * AND CONVERTS IT INTO ORDERED S-N PAIRS, "PAKO",
C     * WHICH ARE USED WITHIN THE MODEL ** ON EACH NODE **.

C     * ** NOTE ** THE CYCLIC LONGITUDE IS STRIPPED OFF!
C     * ** NOTE ** THE VARIABLE "MYNODE" PASSED THROUGH THE
C     *            COMMON DECK "MPINFO" IS USED TO SELECT THE
C     *            CHUNK FOR A GIVEN NODE. 

C     * THIS ROUTINE IS TYPICALLY CALLED WHEN READING-IN EXTERNAL
C     * FIELDS FROM THE "AN" FILE.
C     * "RGTOCG" SHOULD ** NOT ** BE USED.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)                                         
C
      REAL PAKO(LON*ILAT)       !<Variable description\f$[units]\f$
      REAL PAKI(LON+1,NLAT)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER*4 MYNODE
      LOGICAL LRCM
C
      COMMON /MPINFO/ MYNODE
      COMMON /MODEL/  LRCM
C==================================================================
      IF(.NOT.LRCM)                                  THEN
C
C**************************************************************************
C     * GCM WITH REORGANIZED LATITUDES AND POSSIBLE MPI.
C**************************************************************************
C
C      * "NI" REPRESENTS THE COUNTER FOR EACH GRID POINT WRITTEN INTO "PAKO".
C      * "NL" REPRESENTS THE COUNTER FOR LATITUDE IN THE MODEL STRUCTURE
C      * (IE S-N QUARTETS STARTING AT POLES,EQUATOR).
C 
       NI=0
       NL=0
       IF(MOD(NLAT,4).NE.0)       CALL XIT('RGTOCG2',-1)
       NLATH=NLAT/2 
       NLATQ=NLAT/4
C
C      * MPI HOOK.
C
       JSTART=MYNODE*ILAT+1
       JEND  =MYNODE*ILAT+ILAT
C
       DO J=1,NLATQ
         JSP=J
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JSP)
            ENDDO
         ENDIF
C
         JNP=NLAT-J+1 
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JNP)
            ENDDO
         ENDIF
C
         JSE=NLATH-J+1
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JSE)
            ENDDO
         ENDIF 
C
         JNE=NLATH+J
         NL=NL+1
         IF(NL.GE.JSTART .AND. NL.LE.JEND) THEN  
            DO I=1,LON
               NI=NI+1
               PAKO(NI) = PAKI (I,JNE)
            ENDDO
         ENDIF
       ENDDO 
C
      ELSE
C
C**************************************************************************
C      * RCM WITH USUAL LATITUDES AND NO MPI.
C**************************************************************************
C
       NI=0
       DO J=1,NLAT
         DO I=1,LON
           NI=NI+1
           PAKO(NI)=PAKI(I,J)
         ENDDO
       ENDDO
      ENDIF
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

