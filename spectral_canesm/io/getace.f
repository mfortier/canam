!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETACE(NF,EBBTPAK,EOBTPAK,EBFTPAK,EOFTPAK,
     1                  ESDTPAK,ESITPAK,ESSTPAK,ESOTPAK,ESPTPAK,
     2                  ESRTPAK,IJPAK,NLON,NLAT,GG)
C
C     * KNUT VON SALZEN - FEB 07,2009. NEW ROUTINE FOR GCM15H TO READ IN
C     *                                EBBT,EOBT,EBFT,EOFT
C     *                                (USED TO BE DONE BEFORE IN
C     *                                GETCHEM2).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * INVARIANT FIELDS:
C
      REAL EBBTPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL EOBTPAK(IJPAK)!<Variable description\f$[units]\f$
      REAL EBFTPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL EOFTPAK(IJPAK) !<Variable description\f$[units]\f$
C
      REAL ESDTPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ESITPAK(IJPAK)!<Variable description\f$[units]\f$
      REAL ESSTPAK(IJPAK) !<Variable description\f$[units]\f$
C
      REAL ESOTPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ESPTPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ESRTPAK(IJPAK) !<Variable description\f$[units]\f$
C
C     * WORK FIELD:
C
      REAL GG(1) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
C     * GRIDS READ FROM FILE NF.

      REWIND NF

      CALL GETGGBX(EBBTPAK,NC4TO8("EBBT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(EOBTPAK,NC4TO8("EOBT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(EBFTPAK,NC4TO8("EBFT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(EOFTPAK,NC4TO8("EOFT"),NF,NLON,NLAT,0,1,GG)
C
      CALL GETGGBX(ESDTPAK,NC4TO8("ESDT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ESITPAK,NC4TO8("ESIT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ESSTPAK,NC4TO8("ESST"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ESOTPAK,NC4TO8("ESOT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ESPTPAK,NC4TO8("ESPT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ESRTPAK,NC4TO8("ESRT"),NF,NLON,NLAT,0,1,GG)
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

