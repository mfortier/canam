!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PKZEROS2(PKFLD,IJPAK,ILEV) 
 
C     * MAY 27, 2003 - M. LAZARE. NEW VERSION WITH "ROW" FIELDS,
C     *                           INTERNAL PACKING AND "WRKS" REMOVED.
C     * NOV 13/87 - R. MENARD - PREVIOUS VERSION PKZEROS.
C
C     * INITIALIZE PKFLD FIELD TO ZERO IN THE GCM PACKING FORMAT. 
C  
      IMPLICIT NONE
      INTEGER L,IJ
      INTEGER IJPAK !<Variable description\f$[units]\f$
      INTEGER ILEV  !<Variable description\f$[units]\f$
      REAL PKFLD(IJPAK,ILEV) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------
      DO 100 L = 1,ILEV
      DO 100 IJ = 1,IJPAK
          PKFLD(IJ,L)=0.
  100 CONTINUE     
C 
      RETURN
      END      
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
