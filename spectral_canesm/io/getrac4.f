!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
       SUBROUTINE GETRAC4(NF,TRAC,LA,LM,ILEV,LH,ITRAC,NTRAC,NTRACA,
     1                   INDXA,ITRNAM,ITRIC,
     2                   LATOTAL,LMTOTAL,LSRTOTAL,GLL)
C
C     * NOV 03/03 - M.LAZARE.   NEW VERSION BASED ON GETRAC3, WHICH
C     *                         CALLS NEW ROUTINE "GETSPEN" USING
C     *                         WORK ARRAY "GLL", TO GET LOAD-BALANCED
C     *                         SPECTRAL DATA APPLICABLE TO EACH NODE.
C     * OCT 31/02 - J.SCINOCCA. PREVIOUS VERSION GETRAC3.
C  
C     * SELECTIVELY GETS THE INPUT SPECTRAL TRACER FIELDS, UNDER CONTROL
C     * OF "ITRIC".
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX TRAC(LA,ILEV,NTRACA) !<Variable description\f$[units]\f$
      COMPLEX GLL(LATOTAL)         !<Variable description\f$[units]\f$
C
      INTEGER INDXA(NTRAC)         !<Variable description\f$[units]\f$
      INTEGER ITRNAM(NTRAC)        !<Variable description\f$[units]\f$
      INTEGER ITRIC(NTRAC)         !<Variable description\f$[units]\f$
      INTEGER LH(ILEV)             !<Variable description\f$[units]\f$
      INTEGER LSRTOTAL(2,LMTOTAL+1) !<Variable description\f$[units]\f$
C 
      LOGICAL OK
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
      
C-------------------------------------------------------------------------- 
      IF(ITRAC.EQ.0)RETURN
C 
      DO 300 NA=1,NTRACA
       N=INDXA(NA)
       IF(ITRIC(N).EQ.1) THEN
        NAM=ITRNAM(N)
        DO 200 L=1,ILEV
         CALL GETSPN(NF,TRAC(1,L,NA),GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1               NC4TO8("SPEC"),0,NAM,LH(L),OK)
  200   CONTINUE
       ENDIF
  300 CONTINUE
      RETURN
C-------------------------------------------------------------------------- 
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

