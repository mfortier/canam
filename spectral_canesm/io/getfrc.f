!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETFRC  (AMLDFPAK,REAMFPAK,VEAMFPAK,FR1FPAK,FR2FPAK,
     1                    SSLDFPAK,RESSFPAK,VESSFPAK,DSLDFPAK,REDSFPAK,
     2                    VEDSFPAK,BCLDFPAK,REBCFPAK,VEBCFPAK,OCLDFPAK,
     3                    REOCFPAK,VEOCFPAK,ZCDNFPAK,BCICFPAK,BCDPFPAK,
     4                    AMLDFPAL,REAMFPAL,VEAMFPAL,FR1FPAL,FR2FPAL,
     5                    SSLDFPAL,RESSFPAL,VESSFPAL,DSLDFPAL,REDSFPAL,
     6                    VEDSFPAL,BCLDFPAL,REBCFPAL,VEBCFPAL,OCLDFPAL,
     7                    REOCFPAL,VEOCFPAL,ZCDNFPAL,BCICFPAL,BCDPFPAL,
     8                    NLON,NLAT,ILEV,INCD,IDAY,MDAY,MDAY1,
     9                    KOUNT,IJPAK,NUPF,LH,GG)
C
C     * MAR 26/13 - K.VON SALZEN. NEW.
C
C     * READ IN AEROSOL FORCING FIELDS BASED ON PLA CALCULATIONS.
C
C     * TIME LABEL IS THE JULIAN DATE OF THE FIRST OF THE MONTH 
C     * (NFDM) FOR WHICH THE SST ARE THE MONTHLY AVERAGE. 
C
C     * WHEN THE MODEL IS MOVING THROUGH THE YEAR (INCD.NE.0),
C     * THEN NEW FIELDS ARE READ ON EVERY MID MONTH DAY (MMD), 
C     * AND INTERPOLATION WILL BE DONE BETWEEN THE TWO ADJACENT 
C     * MID MONTH DAYS. "PAK" IS THE PRECEDENT, "PAL" IS THE TARGET.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
C     * SINGLE-LEVEL FIELDS.
C
      REAL BCDPFPAK(IJPAK)        !<Variable description\f$[units]\f$
      REAL BCDPFPAL(IJPAK)        !<Variable description\f$[units]\f$
C
C     * MULTI-LEVEL FIELDS.
C
      REAL   AMLDFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   AMLDFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REAMFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REAMFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEAMFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEAMFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   FR1FPAK (IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   FR1FPAL (IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   FR2FPAK (IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   FR2FPAL (IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   SSLDFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   SSLDFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   RESSFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   RESSFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VESSFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VESSFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   DSLDFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   DSLDFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REDSFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REDSFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEDSFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEDSFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   BCLDFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   BCLDFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REBCFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REBCFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEBCFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEBCFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   OCLDFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   OCLDFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REOCFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   REOCFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEOCFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   VEOCFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   ZCDNFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   ZCDNFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   BCICFPAK(IJPAK,ILEV) !<Variable description\f$[units]\f$
      REAL   BCICFPAL(IJPAK,ILEV) !<Variable description\f$[units]\f$
C
      REAL GG(*)                  !<Variable description\f$[units]\f$
C
      INTEGER LH(ILEV)            !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      INTEGER*4 MYNODE
C
      COMMON /MPINFO/ MYNODE 
C----------------------------------------------------------------------
      JOUR =MDAY
      JOUR1=MDAY1
C
      IF(INCD.EQ.0) THEN
C 
C       * MODEL IS STATIONARY.
C
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. READ-IN FIELDS FOR AVERAGE OF MONTH. 
C         * INITIALIZE TARGET FIELDS AS WELL, ALTHOUGH NOT USED.
C
          REWIND NUPF
          DO L=1,ILEV
           CALL GETGGBX(AMLDFPAK(1,L),NC4TO8("AMLD"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(REAMFPAK(1,L),NC4TO8("REAM"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(VEAMFPAK(1,L),NC4TO8("VEAM"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(FR1FPAK(1,L),NC4TO8(" FR1"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(FR2FPAK(1,L),NC4TO8(" FR2"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(SSLDFPAK(1,L),NC4TO8("SSLD"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(RESSFPAK(1,L),NC4TO8("RESS"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(VESSFPAK(1,L),NC4TO8("VESS"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(DSLDFPAK(1,L),NC4TO8("DSLD"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(REDSFPAK(1,L),NC4TO8("REDS"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(VEDSFPAK(1,L),NC4TO8("VEDS"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(BCLDFPAK(1,L),NC4TO8("BCLD"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(REBCFPAK(1,L),NC4TO8("REBC"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(VEBCFPAK(1,L),NC4TO8("VEBC"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(OCLDFPAK(1,L),NC4TO8("OCLD"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(REOCFPAK(1,L),NC4TO8("REOC"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(VEOCFPAK(1,L),NC4TO8("VEOC"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(ZCDNFPAK(1,L),NC4TO8("ZCDN"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
           CALL GETGGBX(BCICFPAK(1,L),NC4TO8("BCIC"),NUPF,NLON,NLAT,
     1                  JOUR,LH(L),GG)
          ENDDO
C
          DO L=1,ILEV
          DO I=1,IJPAK
             AMLDFPAL(I,L) = AMLDFPAK(I,L)
             REAMFPAL(I,L) = REAMFPAK(I,L)
             VEAMFPAL(I,L) = VEAMFPAK(I,L)
             FR1FPAL (I,L) = FR1FPAK (I,L)
             FR2FPAL (I,L) = FR2FPAK (I,L)
             SSLDFPAL(I,L) = SSLDFPAK(I,L)
             RESSFPAL(I,L) = RESSFPAK(I,L)
             VESSFPAL(I,L) = VESSFPAK(I,L)
             DSLDFPAL(I,L) = DSLDFPAK(I,L)
             REDSFPAL(I,L) = REDSFPAK(I,L)
             VEDSFPAL(I,L) = VEDSFPAK(I,L)
             BCLDFPAL(I,L) = BCLDFPAK(I,L)
             REBCFPAL(I,L) = REBCFPAK(I,L)
             VEBCFPAL(I,L) = VEBCFPAK(I,L)
             OCLDFPAL(I,L) = OCLDFPAK(I,L)
             REOCFPAL(I,L) = REOCFPAK(I,L)
             VEOCFPAL(I,L) = VEOCFPAK(I,L)
             ZCDNFPAL(I,L) = ZCDNFPAK(I,L)
             BCICFPAL(I,L) = BCICFPAK(I,L)
          ENDDO
          ENDDO
C
          CALL GETAGBX(BCDPFPAK,NC4TO8("DEPB"),NUPF,NLON,NLAT,JOUR,GG) 
          DO I=1,IJPAK
             BCDPFPAL(I) = BCDPFPAK(I)
          ENDDO

        ELSE
C         
C         * NOT A NEW INTEGRATION. NO NEW FIELDS REQUIRED. 
C         
        ENDIF 

      ELSE
C      
C       * THE MODEL IS MOVING.
C      
        IF(KOUNT.EQ.0) THEN 
C         
C        * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
         REWIND NUPF         
         DO L=1,ILEV
          CALL GETGGBX(AMLDFPAK(1,L),NC4TO8("AMLD"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REAMFPAK(1,L),NC4TO8("REAM"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEAMFPAK(1,L),NC4TO8("VEAM"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(FR1FPAK (1,L),NC4TO8(" FR1"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(FR2FPAK (1,L),NC4TO8(" FR2"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(SSLDFPAK(1,L),NC4TO8("SSLD"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(RESSFPAK(1,L),NC4TO8("RESS"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VESSFPAK(1,L),NC4TO8("VESS"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(DSLDFPAK(1,L),NC4TO8("DSLD"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REDSFPAK(1,L),NC4TO8("REDS"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEDSFPAK(1,L),NC4TO8("VEDS"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(BCLDFPAK(1,L),NC4TO8("BCLD"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REBCFPAK(1,L),NC4TO8("REBC"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEBCFPAK(1,L),NC4TO8("VEBC"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(OCLDFPAK(1,L),NC4TO8("OCLD"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REOCFPAK(1,L),NC4TO8("REOC"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEOCFPAK(1,L),NC4TO8("VEOC"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(ZCDNFPAK(1,L),NC4TO8("ZCDN"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(BCICFPAK(1,L),NC4TO8("BCIC"),NUPF,NLON,NLAT,
     1                 JOUR1,LH(L),GG)
         ENDDO
C
         CALL GETGGBX(BCDPFPAK,NC4TO8("DEPB"),NUPF,NLON,NLAT,JOUR1,1,
     1                GG)
C
         REWIND NUPF
C
         DO L=1,ILEV
          CALL GETGGBX(AMLDFPAL(1,L),NC4TO8("AMLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REAMFPAL(1,L),NC4TO8("REAM"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEAMFPAL(1,L),NC4TO8("VEAM"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(FR1FPAL (1,L),NC4TO8(" FR1"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(FR2FPAL (1,L),NC4TO8(" FR2"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(SSLDFPAL(1,L),NC4TO8("SSLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(RESSFPAL(1,L),NC4TO8("RESS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VESSFPAL(1,L),NC4TO8("VESS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(DSLDFPAL(1,L),NC4TO8("DSLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REDSFPAL(1,L),NC4TO8("REDS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEDSFPAL(1,L),NC4TO8("VEDS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(BCLDFPAL(1,L),NC4TO8("BCLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REBCFPAL(1,L),NC4TO8("REBC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEBCFPAL(1,L),NC4TO8("VEBC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(OCLDFPAL(1,L),NC4TO8("OCLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REOCFPAL(1,L),NC4TO8("REOC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEOCFPAL(1,L),NC4TO8("VEOC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(ZCDNFPAL(1,L),NC4TO8("ZCDN"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(BCICFPAL(1,L),NC4TO8("BCIC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         CALL GETGGBX(BCDPFPAL,NC4TO8("DEPB"),NUPF,NLON,NLAT,JOUR,1,
     1                GG)
C
         LON=NLON-1
         DAY1=REAL(MDAY1) 
         DAY2=REAL(IDAY)
         DAY3=REAL(MDAY)
         IF(DAY2.LT.DAY1) DAY2=DAY2+365. 
         IF(DAY3.LT.DAY2) DAY3=DAY3+365. 
         W1=(DAY2-DAY1)/(DAY3-DAY1)
         W2=(DAY3-DAY2)/(DAY3-DAY1)
         IF(MYNODE.EQ.0) WRITE(6,6000) IDAY,MDAY1,MDAY,W1,W2 
C 
         DO I=1,IJPAK
           BCDPFPAK(I)=W1*BCDPFPAL(I) + W2*BCDPFPAK(I)
         ENDDO
C
         DO L=1,ILEV
         DO I=1,IJPAK
           AMLDFPAK(I,L)=W1*AMLDFPAL(I,L) + W2*AMLDFPAK(I,L)
           REAMFPAK(I,L)=W1*REAMFPAL(I,L) + W2*REAMFPAK(I,L)
           VEAMFPAK(I,L)=W1*VEAMFPAL(I,L) + W2*VEAMFPAK(I,L)
           FR1FPAK (I,L)=W1*FR1FPAL (I,L) + W2*FR1FPAK (I,L)
           FR2FPAK (I,L)=W1*FR2FPAL (I,L) + W2*FR2FPAK (I,L)
           SSLDFPAK(I,L)=W1*SSLDFPAL(I,L) + W2*SSLDFPAK(I,L)
           RESSFPAK(I,L)=W1*RESSFPAL(I,L) + W2*RESSFPAK(I,L)
           VESSFPAK(I,L)=W1*VESSFPAL(I,L) + W2*VESSFPAK(I,L)
           DSLDFPAK(I,L)=W1*DSLDFPAL(I,L) + W2*DSLDFPAK(I,L)
           REDSFPAK(I,L)=W1*REDSFPAL(I,L) + W2*REDSFPAK(I,L)
           VEDSFPAK(I,L)=W1*VEDSFPAL(I,L) + W2*VEDSFPAK(I,L)
           BCLDFPAK(I,L)=W1*BCLDFPAL(I,L) + W2*BCLDFPAK(I,L)
           REBCFPAK(I,L)=W1*REBCFPAL(I,L) + W2*REBCFPAK(I,L)
           VEBCFPAK(I,L)=W1*VEBCFPAL(I,L) + W2*VEBCFPAK(I,L)
           OCLDFPAK(I,L)=W1*OCLDFPAL(I,L) + W2*OCLDFPAK(I,L)
           REOCFPAK(I,L)=W1*REOCFPAL(I,L) + W2*REOCFPAK(I,L)
           VEOCFPAK(I,L)=W1*VEOCFPAL(I,L) + W2*VEOCFPAK(I,L)
           ZCDNFPAK(I,L)=W1*ZCDNFPAL(I,L) + W2*ZCDNFPAK(I,L)
           BCICFPAK(I,L)=W1*BCICFPAL(I,L) + W2*BCICFPAK(I,L)
         ENDDO
         ENDDO

        ELSE
C 
C        * THIS IS IN THE MIDDLE OF A RUN. 
C
         REWIND NUPF
         DO L=1,ILEV
          CALL GETGGBX(AMLDFPAL(1,L),NC4TO8("AMLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REAMFPAL(1,L),NC4TO8("REAM"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEAMFPAL(1,L),NC4TO8("VEAM"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(FR1FPAL (1,L),NC4TO8(" FR1"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(FR2FPAL (1,L),NC4TO8(" FR2"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(SSLDFPAL(1,L),NC4TO8("SSLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(RESSFPAL(1,L),NC4TO8("RESS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VESSFPAL(1,L),NC4TO8("VESS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(DSLDFPAL(1,L),NC4TO8("DSLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REDSFPAL(1,L),NC4TO8("REDS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEDSFPAL(1,L),NC4TO8("VEDS"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(BCLDFPAL(1,L),NC4TO8("BCLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REBCFPAL(1,L),NC4TO8("REBC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEBCFPAL(1,L),NC4TO8("VEBC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(OCLDFPAL(1,L),NC4TO8("OCLD"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(REOCFPAL(1,L),NC4TO8("REOC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(VEOCFPAL(1,L),NC4TO8("VEOC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(ZCDNFPAL(1,L),NC4TO8("ZCDN"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         DO L=1,ILEV
          CALL GETGGBX(BCICFPAL(1,L),NC4TO8("BCIC"),NUPF,NLON,NLAT,
     1                 JOUR,LH(L),GG)
         ENDDO
C
         CALL GETGGBX(BCDPFPAL,NC4TO8("DEPB"),NUPF,NLON,NLAT,JOUR,1,
     1                GG)
C
        ENDIF

      ENDIF 
      RETURN
C---------------------------------------------------------------------
 6000 FORMAT(' INTERPOLATING FOR', I5, ' BETWEEN', I5, ' AND',
     1       I5, ' WITH WEIGHTS=', 2F7.3)
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

