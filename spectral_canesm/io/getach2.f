!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETACH2(FAIRPAK,FAIRPAL,NLON,NLAT,LEVAIR,INCD,IDAY,
     1                   MDAY,MDAY1,MON,MON1,KOUNT,IJPAK,NUAN,LA,GG)
C
C     * MAY 10/2012 - M.LAZARE. NEW VERSION FOR GCM16:
C     *                         - MDAYT,MDAYT1 PASSED IN (AS
C     *                           "MDAY" AND "MDAY1", RESPECTIVELY),
C     *                           FROM MODEL DRIVER, INSTEAD OF
C     *                           BEING CALCULATED INSIDE THIS ROUTINE.
C     * KNUT VON SALZEN - SEP 22,2009. PREVIOUS ROUTINE GETACH FOR
C     *                                GCM15I TO READ IN AIRCRAFT 
C     *                                EMISSIONS.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * AIRCRAFT EMISSIONS.
C
C     * MULTI-LEVEL SPECIES.
C
      REAL FAIRPAK(IJPAK,LEVAIR) !<Variable description\f$[units]\f$
      REAL FAIRPAL(IJPAK,LEVAIR) !<Variable description\f$[units]\f$
C
      REAL GG(*)                 !<Variable description\f$[units]\f$
C
      INTEGER LA(LEVAIR)         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      INTEGER*4 MYNODE
C
      COMMON /MPINFO/ MYNODE
C----------------------------------------------------------------------
      IF(INCD.EQ.0) THEN
C
C       * MODEL IS STATIONARY.
C
        IF(KOUNT.EQ.0) THEN
C
C         * START-UP TIME. READ-IN FIELDS FOR AVERAGE OF MONTH.
C         * INITIALIZE TARGET FIELDS AS WELL, ALTHOUGH NOT USED.
C
          REWIND NUAN
          DO  8  L=1,LEVAIR
           CALL GETGGBX(FAIRPAK(1,L),NC4TO8("FAIR"),NUAN,NLON,NLAT,MON1,
     1                  LA(L),GG)
    8     CONTINUE
C
          DO 16 L=1,LEVAIR
          DO 16 I=1,IJPAK
             FAIRPAL(I,L) = FAIRPAK(I,L)
   16     CONTINUE
        ELSE
C
C         * NOT A NEW INTEGRATION. NO NEW FIELDS REQUIRED.
C
        ENDIF
      ELSE
C
C       * THE MODEL IS MOVING.
C
        IF(KOUNT.EQ.0) THEN
C
C        * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
         REWIND NUAN
C
         DO 20  L=1,LEVAIR
          CALL GETGGBX(FAIRPAK(1,L),NC4TO8("FAIR"),NUAN,NLON,NLAT,MON1,
     1                 LA(L),GG)
   20    CONTINUE
C
         REWIND NUAN
C
         DO 30  L=1,LEVAIR
          CALL GETGGBX(FAIRPAL(1,L),NC4TO8("FAIR"),NUAN,NLON,NLAT,MON,
     1                 LA(L),GG)
   30    CONTINUE
C
         LON=NLON-1
         DAY1=REAL(MDAY1)
         DAY2=REAL(IDAY)
         DAY3=REAL(MDAY)
         IF(DAY2.LT.DAY1) DAY2=DAY2+365.
         IF(DAY3.LT.DAY2) DAY3=DAY3+365.
         W1=(DAY2-DAY1)/(DAY3-DAY1)
         W2=(DAY3-DAY2)/(DAY3-DAY1)
         IF(MYNODE.EQ.0) WRITE(6,6000) IDAY,MDAY1,MDAY,W1,W2
C
         DO 150 L=1,LEVAIR
         DO 150 I=1,IJPAK
           FAIRPAK(I,L)=W1*FAIRPAL(I,L) + W2*FAIRPAK(I,L)
  150    CONTINUE
        ELSE
C
C         * THIS IS IN THE MIDDLE OF A RUN.
C
          REWIND NUAN
C
          DO 230  L=1,LEVAIR
           CALL GETGGBX(FAIRPAL(1,L),NC4TO8("FAIR"),NUAN,NLON,NLAT,MON,
     1                  LA(L),GG)
  230     CONTINUE
        ENDIF
C
      ENDIF
      RETURN
C---------------------------------------------------------------------
 6000 FORMAT(' INTERPOLATING FOR', I5, ' BETWEEN', I5, ' AND',
     1       I5, ' WITH WEIGHTS=', 2F7.3)
C
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}


