!>\putgg.f90
!>\If desired, unpack packed gaussian grid array from the gcm row by row, and write it to the given file unit. 
!>\Also capable of writing out the packed array directly.
!!
!> @author 
!> Originally written by Fouad Majaess, Mike Lazare and others. Updated
!> to free form, modern fortran syntax by Clint Seinen
!
subroutine putgg(GGPAK,ILG1,ILAT,KHEM,NPGG,KOUNT, &
                     NF,NAME,LEV,GLL,WRKS)
    
    ! OCT 11/06 - F.MAJAESS (REPLACE DIRECT BINARY "DATA" RECORD
    !                       WRITES BY "PUTFLD2" CALLS).
    ! DEC 19/03 - M.LAZARE. HARD CODE "XPAK" LOCAL ARRAY DIMENSION
    !                       DUE TO ENCOUNTERED PROBLEM WITH "NANS"
    !                       INITIALIZATION UNDER AIX.
    ! DEC 05/03 - R.MCLAY/  SUPPORT FOR MPI I/O ADDED, AS AN OPTION
    !             M.LAZARE. BASED ON SWITCH "IPIO" PASSED THROUGH
    !                        COMMON BLOCK.
    ! MAY 29/95 - M.LAZARE. PREVIOUS VERSION PUTGGB2.

    !==================================================================
    ! Doxygen example
    !
    ! REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
    !           !! It is compute differently when using bulk or PAM aerosols.
    !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
    !==================================================================

    implicit none

    !========
    ! Inputs
    !========
    ! CSS : do we want to assign a kind for these?
    integer, intent(in) :: ILG1     !< Number of longitudes to write out
    integer, intent(in) :: ILAT     !< Number of latitudes to write out
    integer, intent(in) :: KHEM     !< Switch for domain associated with array to write out - 0: global, 1: N. Hem., 2: S. Hem.
    integer, intent(in) :: NPGG     !< Internal packing density (generally 0) CSS: Doesn't look used.
    integer, intent(in) :: KOUNT    !< Time step counter
    integer, intent(in) :: NF       !< File unit
    integer, intent(in) :: NAME     !< Integer name identifier (see nc4to8) CSS: Documentation needs to be improved for this..
    integer, intent(in) :: LEV      !< Level to be output CSS: CONFIRM

    real, dimension(ILAT*(ILG1-1)), intent(in) :: GGPAK !< Packed gaussian grid array to be output in gridded format

    !=================
    ! Modified Inputs 
    !=================
    real, dimension(ILG1*ILAT), intent(inout) :: GLL    !< Work array to contain the unpacked input array
    real, dimension(1),         intent(inout) :: WRKS   !< Unused work arrray (SHOULD BE DELETED)
                            
    !=====================
    ! Common Block Vars
    !=====================
    ! sizes
    integer :: LONSL
    integer :: NLAT
    integer :: ILEV
    integer :: LEVS
    integer :: LRLMT
    integer :: ICOORD
    integer :: LAY
    integer :: MOIST
    real    :: PTOIT
    common /SIZES/  LONSL,NLAT,ILEV,LEVS,LRLMT,ICOORD,LAY,PTOIT,MOIST

    ! MPI/Communicator vars
    integer*4 :: MYNODE
    integer*4 :: NNODE
    integer*4 :: AGCM_COMMWRLD
    integer*4 :: MY_CMPLX_TYPE
    integer*4 :: MY_REAL_TYPE
    integer*4 :: MY_INT_TYPE
    common /MPINFO/ MYNODE 
    common /MPICOMM/ NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE,MY_REAL_TYPE, &
                       MY_INT_TYPE

    ! Time vars
    integer :: IYEAR
    integer :: IMDH
    integer :: MYRSSTI
    integer :: ISAVDTS
    common /KEEPTIM/ IYEAR,IMDH,MYRSSTI,ISAVDTS

    ! parallel IO
    integer :: IPIO !< Switch determining parallel I/O is used. 0: Off, Any other value: On
    common /IPARIO/ IPIO     

    ! rcm params
    logical :: LRCM !< True: the RCM is being used. False: Global model
    common /MODEL/  LRCM

    ! general I/O vars
    integer, dimension(8) :: IBUF !< Buffer containing integer switches for word labels
    integer, dimension(1) :: IDAT !< Not used! Can be deleted?
    common /ICOM/ IBUF,IDAT

    !====================
    ! Internal Variables
    !====================
    integer :: I
    integer :: J
    integer :: IJ
    integer :: ILG
    integer :: IOFF
    integer :: IGRID                !< Integer identifier for "GRID" string
    integer :: NPACK
    integer :: IBUF2
    integer :: MAXX
    integer :: NC4TO8               !< external function to translate string to integers

    ! used in parallel i/o off
    !     REAL, TARGET  :: XPAK(NLAT*ILG1)
    !     * HARD CODE "XPAK" DIMENSION DUE TO PROBLEM WITH "NANS" INITIALIZATION
    !     * UNDER AIX. IT'S SET TO HANDLE UP TO 641X320 (T213 GRID).
    real, target  :: XPAK(205120)   !< (if parallel i/o off) container to receive global field
    real, pointer :: GBUF(:)        !< (if parallel i/o off) pointer to buffer that will recieve the global field
    integer*4 :: SZ                 !< (if parallel i/o off) Size of array to be received 
    integer*4 :: MASTER             !< (if parallel i/o off) Rank of receiving process if gather is required
    integer*4 :: IERR               !< (if parallel i/o off) Integer container for MPI error

    !---------------------------------------------------------------------
    if (ILG1*NLAT > 205120) then
        call XIT('putgg', -1)
    end if
    IGRID=NC4TO8("GRID")
    NPACK=0 

    ! DETERMINE PROPER IBUF(2) TO USE FOR SAVED FIELDS, BASED ON
    ! VALUE OF OPTION SWITCH "ISAVDTS".
    ! NOTE THAT THE VALUE OF "NF" IS CHECKED TO SEE IF WE ARE
    ! WRITING TO THE INTERACTIVE FILE; IF SO KOUNT IS USED IN
    ! IBUF(2) REGARDLESS OF WHETHER WE ARE RUNNING WITH THE
    ! DATE-TIME STAMP OR NOT. 
    ! THIS ENABLES US TO JUST USE PUTGGB3 AND NOT REQUIRE A SEPARATE
    ! SUBROUTINE PUTGGBX TO WRITE TO THE INTERACTIVE FILE.
 
      if ((ISAVDTS /= 0) .and. (NF /= 19)) then
    ! IN 32-BIT, THIS ONLY WORKS UNTIL IYEAR=2147!
         IBUF2=1000000*IYEAR + IMDH
      else
         IBUF2=KOUNT
      end if
 
    ! STORE VALUES OF GGPAK INTO GLL, ADDING CYCLIC LONGITUDE.
    ! NOTE THAT THIS IS NOT DONE FOR THE RCM!

      ILG = ILG1-1
      if ( .not. LRCM ) then      
        do J=1,ILAT 
          IJ   = (J-1)*ILG1
          IOFF = (J-1)*ILG
          do I=1,ILG
            GLL(IJ+I)=GGPAK(IOFF+I)
          end do
          GLL(IJ+ILG1)=GLL(IJ+1)
        end do
      end if 

      ! if running over multiple nodes, and parallel io is turned off,
      ! we only write while on the main process
      if ((NNODE > 1) .and. (IPIO == 0)) then

        ! abort if trying to run the rcm with mpi - NOT SUPPORTED.
        if (LRCM) then
            call XIT('putgg',-2)
        end if

        ! gather GLL into GBUF (=>XBUF) FROM ALL NBR NODES.
        MASTER = 0
        SZ     = ILAT*ILG1
        GBUF => XPAK(1:ILG1*NLAT)
        call WRAP_GATHER(GLL , SZ, MY_REAL_TYPE, &
                         GBUF, SZ, MY_REAL_TYPE, &
                         MASTER, AGCM_COMMWRLD, IERR)
 
        ! write out global gathered field
        if (MYNODE == 0) then
          call SETLAB(IBUF,IGRID,IBUF2,NAME,LEV,ILG1,NLAT,KHEM,NPACK)
          MAXX=ILG1*NLAT
          call PUTFLD2(NF,GBUF,IBUF,MAXX)
        end if
      else

        if (LRCM) then
          call SETLAB(IBUF,IGRID,IBUF2,NAME,LEV,ILG,ILAT,KHEM,NPACK)
          MAXX=ILG*ILAT
          call PUTFLD2(NF,GGPAK,IBUF,MAXX)
        else
          call SETLAB(IBUF,IGRID,IBUF2,NAME,LEV,ILG1,ILAT,KHEM,NPACK)
          MAXX=ILG1*ILAT
          call PUTFLD2(NF,GLL,IBUF,MAXX)
        end if

      end if
      return
end subroutine putgg
!> \file putgg.f90
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

