!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETRACNA3(NF,TRACNA,ILEV,LH,ITRAC,NTRAC,NTRACN,
     1                     INDXNA,ITRNAM,ITRIC,ITRLVS,ITRLVF,
     2                     IJPAK,NLON,NLAT,GG)
C
C     * FEB 20/2004 - J.SCINOCCA. NEW VERSION (GETRACNA3) FOR GCM13D
C     *                           ONWARD, PASSING IN AND USING ITRLVS
C     *                           AND ITRLVF AS SUBSET OF COMPLETE
C     *                           VERTICAL DOMAIN FOR NON-ADVECTED
C     *                           TRACERS. 
C     * DEC 10/2003 - M.LAZARE.   NEW VERSION (GETRACNA2) FOR GCM13C:
C     *                           MODIFIED FOR MPI.
C     * OCT 31/2002 - J.SCINOCCA. PREVIOUS VERSION GETRACNA.
C  
C     * SELECTIVELY GETS NON-ADVECTED TRACERS FOR SPECTRAL CASE.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL TRACNA(IJPAK,ILEV,NTRACN) !<Variable description\f$[units]\f$
      INTEGER ITRNAM(NTRAC)          !<Variable description\f$[units]\f$
      INTEGER INDXNA(NTRAC)          !<Variable description\f$[units]\f$
      INTEGER ITRIC(NTRAC)           !<Variable description\f$[units]\f$
      INTEGER ITRLVS(NTRAC)          !<Variable description\f$[units]\f$
      INTEGER ITRLVF(NTRAC)          !<Variable description\f$[units]\f$
      INTEGER LH(ILEV)               !<Variable description\f$[units]\f$
C
C     * WORK FIELD:
C
      REAL GG(1)                     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------------- 
      DO 300 NN=1,NTRACN
        N=INDXNA(NN)
        IF(ITRIC(N).EQ.1)            THEN
          NAM=ITRNAM(N)
          DO 200 L=ITRLVS(N),ITRLVF(N)
            CALL GETGGBX(TRACNA(1,L,NN),NAM,NF,NLON,NLAT,0,LH(L),GG)
  200     CONTINUE
        ENDIF
  300 CONTINUE
      RETURN
C-------------------------------------------------------------------------- 
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
