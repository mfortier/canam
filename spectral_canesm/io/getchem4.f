!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETCHEM4(DMSOPAK,DMSOPAL,EDMSPAK,EDMSPAL,
     1                    SUZ0PAK,SUZ0PAL,PDSFPAK,PDSFPAL,  
     2                      OHPAK,  OHPAL,
     3                    H2O2PAK,H2O2PAL,  O3PAK,  O3PAL,
     4                     NO3PAK, NO3PAL,HNO3PAK,HNO3PAL,
     5                     NH3PAK, NH3PAL, NH4PAK, NH4PAL,
     6                    NLON,NLAT,LEVOX,INCD,IDAY,MDAY,MDAY1,
     7                    KOUNT,IJPAK,NUCH,NUOX,LX,GG)
C
C     * JUN 11, 2018 - M.LAZARE. Read from unit number NUCH ("llchem" file)
C     *                          instead of NUGG for non-transient chemistry
C     *                          forcing (several calls). Also read from unit
C     *                          number NUOX ("oxi" file) for non-transient
C     *                          oxidants in GETCHEM4 (subroutine also changed).
C     * MAY 10/2012 - Y.PENG/   NEW VERSION FOR GCM16:
C     *               M.LAZARE. - ADD SUZ0,PDSF INPUT FIELDS FOR NEW
C     *                           DUST SCHEME.
C     *                         - MDAY AND MDAY1 PASSED IN (CALCULATED
C     *                           IN "GETMDAY") RATHER THAN BEING
C     *                           DETERMINED HERE.
C     * KNUT VON SALZEN - FEB 07,2009. PREVIOUS VERSION GETCHEM3 FOR 
C     *                                GCM15H/I:
C     *                                REMOVE EOST (NOW DONE IN GETCAC).
C     * JAN 17/08 - X.MA/     PREVIOUS VERSION GETCHEM2 FOR GCM15G:
C     *             M.LAZARE. ADD EOST AND REMOVE EBC,EOC,EOCF.
C     * DEC 15/03 - M.LAZARE. PREVIOUS VERSION GETCHEMX UP TO GCM15F:
C     *                       - ROW FIELDS REMOVED.
C     *                       - USES MPI-READY I/O INTERFACE ROUTINES. 
C     *                       - ONLY PRINTS OUT ON NODE ZERO.
C     *                       - "IFIZ" REMOVED.
C     * JUL 16/01 - M.LAZARE. PREVIOUS VERSION GETCHEM FOR GCM15.
C
C     * READ IN CHEMISTRY FIELDS (SURFACE EMISSIONS AND ATMOSPHERIC
C     * SPECIES CONCENTRATIONS) AND INTERPOLATE TO CURRENT IDAY.
C
C     * TIME LABEL IS THE JULIAN DATE OF THE FIRST OF THE MONTH 
C     * (NFDM) FOR WHICH THE SST ARE THE MONTHLY AVERAGE. 
C
C     * WHEN THE MODEL IS MOVING THROUGH THE YEAR (INCD.NE.0),
C     * THEN NEW FIELDS ARE READ ON EVERY MID MONTH DAY (MMD), 
C     * AND INTERPOLATION WILL BE DONE BETWEEN THE TWO ADJACENT 
C     * MID MONTH DAYS. "PAK" IS THE PRECEDENT, "PAL" IS THE TARGET.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
C     * SURFACE EMISSIONS/CONCENTRATIONS.
C
      REAL DMSOPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL DMSOPAL(IJPAK) !<Variable description\f$[units]\f$
      REAL EDMSPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL EDMSPAL(IJPAK) !<Variable description\f$[units]\f$
      REAL SUZ0PAK(IJPAK) !<Variable description\f$[units]\f$ 
      REAL SUZ0PAL(IJPAK) !<Variable description\f$[units]\f$
      REAL PDSFPAK(IJPAK) !<Variable description\f$[units]\f$
      REAL PDSFPAL(IJPAK) !<Variable description\f$[units]\f$
C
C     * MULTI-LEVEL SPECIES.
C
      REAL OHPAK(IJPAK,LEVOX)   !<Variable description\f$[units]\f$
      REAL OHPAL(IJPAK,LEVOX)   !<Variable description\f$[units]\f$
      REAL H2O2PAK(IJPAK,LEVOX) !<Variable description\f$[units]\f$
      REAL H2O2PAL(IJPAK,LEVOX) !<Variable description\f$[units]\f$
      REAL O3PAK(IJPAK,LEVOX)   !<Variable description\f$[units]\f$
      REAL O3PAL(IJPAK,LEVOX)   !<Variable description\f$[units]\f$
      REAL NO3PAK(IJPAK,LEVOX)  !<Variable description\f$[units]\f$
      REAL NO3PAL(IJPAK,LEVOX)  !<Variable description\f$[units]\f$
      REAL HNO3PAK(IJPAK,LEVOX) !<Variable description\f$[units]\f$
      REAL HNO3PAL(IJPAK,LEVOX) !<Variable description\f$[units]\f$
      REAL NH3PAK(IJPAK,LEVOX)  !<Variable description\f$[units]\f$
      REAL NH3PAL(IJPAK,LEVOX)  !<Variable description\f$[units]\f$
      REAL NH4PAK(IJPAK,LEVOX)  !<Variable description\f$[units]\f$
      REAL NH4PAL(IJPAK,LEVOX)  !<Variable description\f$[units]\f$
C
      REAL GG(*)               !<Variable description\f$[units]\f$
C
      INTEGER LX(LEVOX)         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      INTEGER*4 MYNODE
C
      COMMON /MPINFO/ MYNODE 
C----------------------------------------------------------------------
      JOUR =MDAY
      JOUR1=MDAY1
C
      IF(INCD.EQ.0) THEN
C 
C       * MODEL IS STATIONARY.
C
        IF(KOUNT.EQ.0) THEN 
C         
C         * START-UP TIME. READ-IN FIELDS FOR AVERAGE OF MONTH. 
C         * INITIALIZE TARGET FIELDS AS WELL, ALTHOUGH NOT USED.
C
          REWIND NUCH
C
          CALL GETAGBX(DMSOPAK,NC4TO8("DMSO"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(DMSOPAL,NC4TO8("DMSO"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(EDMSPAK,NC4TO8("EDMS"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(EDMSPAL,NC4TO8("EDMS"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(SUZ0PAK,NC4TO8("SUZ0"),NUCH,NLON,NLAT,IDAY,GG)
          CALL GETAGBX(SUZ0PAL,NC4TO8("SUZ0"),NUCH,NLON,NLAT,IDAY,GG) 
          CALL GETAGBX(PDSFPAK,NC4TO8("SLAI"),NUCH,NLON,NLAT,IDAY,GG)
          CALL GETAGBX(PDSFPAL,NC4TO8("SLAI"),NUCH,NLON,NLAT,IDAY,GG)
C
          REWIND NUOX
C
          DO  8  L=1,LEVOX
           CALL GETGGBX(  OHPAK(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
    8     CONTINUE     
C
          DO  9  L=1,LEVOX
           CALL GETGGBX(H2O2PAK(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
    9     CONTINUE     
C
          DO 10  L=1,LEVOX
           CALL GETGGBX(  O3PAK(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
   10     CONTINUE     
C
          DO 11  L=1,LEVOX
           CALL GETGGBX( NO3PAK(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
   11     CONTINUE     
C
          DO 12  L=1,LEVOX
           CALL GETGGBX(HNO3PAK(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
   12     CONTINUE    
C
          DO 13  L=1,LEVOX
           CALL GETGGBX( NH3PAK(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
   13     CONTINUE     
C
          DO 14  L=1,LEVOX
           CALL GETGGBX( NH4PAK(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,IDAY,
     1                  LX(L),GG)
   14     CONTINUE     
C
          DO 16 L=1,LEVOX
          DO 16 I=1,IJPAK
             OHPAL  (I,L) = OHPAK  (I,L)
             H2O2PAL(I,L) = H2O2PAK(I,L)
             O3PAL  (I,L) = O3PAK  (I,L)
             NO3PAL (I,L) = NO3PAK (I,L)
             HNO3PAL(I,L) = HNO3PAK(I,L)
             NH3PAL (I,L) = NH3PAK (I,L)
             NH4PAL (I,L) = NH4PAK (I,L)
   16     CONTINUE     

        ELSE
C         
C         * NOT A NEW INTEGRATION. NO NEW FIELDS REQUIRED. 
C         
        ENDIF 

      ELSE
C      
C       * THE MODEL IS MOVING.
C      
        IF(KOUNT.EQ.0) THEN 
C         
C        * START-UP TIME. GET FIELDS FOR PREVIOUS AND TARGET MID-MONTH DAYS.
C
         REWIND NUCH         
C
         CALL GETGGBX(DMSOPAK,NC4TO8("DMSO"),NUCH,NLON,NLAT,JOUR1,1,GG)
         CALL GETGGBX(EDMSPAK,NC4TO8("EDMS"),NUCH,NLON,NLAT,JOUR1,1,GG)
         CALL GETGGBX(SUZ0PAK,NC4TO8("SUZ0"),NUCH,NLON,NLAT,JOUR1,1,GG)   
         CALL GETGGBX(PDSFPAK,NC4TO8("SLAI"),NUCH,NLON,NLAT,JOUR1,1,GG)
C
         REWIND NUOX
C
         DO 20  L=1,LEVOX
          CALL GETGGBX(  OHPAK(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   20    CONTINUE     
C
         DO 21  L=1,LEVOX
          CALL GETGGBX(H2O2PAK(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   21    CONTINUE     
C
         DO 22  L=1,LEVOX
          CALL GETGGBX(  O3PAK(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   22    CONTINUE     
C
         DO 23  L=1,LEVOX
          CALL GETGGBX( NO3PAK(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   23    CONTINUE     
C
         DO 24  L=1,LEVOX
          CALL GETGGBX(HNO3PAK(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   24    CONTINUE     
C
         DO 25  L=1,LEVOX
          CALL GETGGBX( NH3PAK(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   25    CONTINUE     
C
         DO 26  L=1,LEVOX
          CALL GETGGBX( NH4PAK(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,JOUR1,
     1                 LX(L),GG)
   26    CONTINUE     
C
         REWIND NUCH
C
         CALL GETGGBX(DMSOPAL,NC4TO8("DMSO"),NUCH,NLON,NLAT,JOUR,1,GG)
         CALL GETGGBX(EDMSPAL,NC4TO8("EDMS"),NUCH,NLON,NLAT,JOUR,1,GG)
         CALL GETGGBX(SUZ0PAL,NC4TO8("SUZ0"),NUCH,NLON,NLAT,JOUR,1,GG)  
         CALL GETGGBX(PDSFPAL,NC4TO8("SLAI"),NUCH,NLON,NLAT,JOUR,1,GG)   
C
         REWIND NUOX
C
         DO 30  L=1,LEVOX
          CALL GETGGBX(  OHPAL(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   30    CONTINUE     
C
         DO 31  L=1,LEVOX
          CALL GETGGBX(H2O2PAL(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   31    CONTINUE     
C
         DO 32  L=1,LEVOX
          CALL GETGGBX(  O3PAL(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   32    CONTINUE     
C
         DO 33  L=1,LEVOX
          CALL GETGGBX( NO3PAL(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   33    CONTINUE     
C
         DO 34  L=1,LEVOX
          CALL GETGGBX(HNO3PAL(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   34    CONTINUE     
C
         DO 35  L=1,LEVOX
          CALL GETGGBX( NH3PAL(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   35    CONTINUE     
C
         DO 36  L=1,LEVOX
          CALL GETGGBX( NH4PAL(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,JOUR,
     1                 LX(L),GG)
   36    CONTINUE     
C
         LON=NLON-1
         DAY1=REAL(MDAY1) 
         DAY2=REAL(IDAY)
         DAY3=REAL(MDAY)
         IF(DAY2.LT.DAY1) DAY2=DAY2+365. 
         IF(DAY3.LT.DAY2) DAY3=DAY3+365. 
         W1=(DAY2-DAY1)/(DAY3-DAY1)
         W2=(DAY3-DAY2)/(DAY3-DAY1)
         IF(MYNODE.EQ.0) WRITE(6,6000) IDAY,MDAY1,MDAY,W1,W2 
C 
         DO 125 I=1,IJPAK
           DMSOPAK(I)=W1*DMSOPAL(I) + W2*DMSOPAK(I)
           EDMSPAK(I)=W1*EDMSPAL(I) + W2*EDMSPAK(I)
           SUZ0PAK(I)=W1*SUZ0PAL(I) + W2*SUZ0PAK(I)
           PDSFPAK(I)=W1*PDSFPAL(I) + W2*PDSFPAK(I)
  125    CONTINUE
C
         DO 150 L=1,LEVOX
         DO 150 I=1,IJPAK
           OHPAK  (I,L)=W1*OHPAL  (I,L) + W2*OHPAK  (I,L)
           H2O2PAK(I,L)=W1*H2O2PAL(I,L) + W2*H2O2PAK(I,L)
           O3PAK  (I,L)=W1*O3PAL  (I,L) + W2*O3PAK  (I,L)
           NO3PAK (I,L)=W1*NO3PAL (I,L) + W2*NO3PAK (I,L)
           HNO3PAK(I,L)=W1*HNO3PAL(I,L) + W2*HNO3PAK(I,L)
           NH3PAK (I,L)=W1*NH3PAL (I,L) + W2*NH3PAK (I,L)
           NH4PAK (I,L)=W1*NH4PAL (I,L) + W2*NH4PAK (I,L)
  150    CONTINUE    

        ELSE
C 
C         * THIS IS IN THE MIDDLE OF A RUN. 
C
          REWIND NUCH
C
          CALL GETGGBX(DMSOPAL,NC4TO8("DMSO"),NUCH,NLON,NLAT,JOUR,1,GG)
          CALL GETGGBX(EDMSPAL,NC4TO8("EDMS"),NUCH,NLON,NLAT,JOUR,1,GG)
          CALL GETGGBX(SUZ0PAL,NC4TO8("SUZ0"),NUCH,NLON,NLAT,JOUR,1,GG) 
          CALL GETGGBX(PDSFPAL,NC4TO8("SLAI"),NUCH,NLON,NLAT,JOUR,1,GG)
C
          REWIND NUOX
C
          DO 230  L=1,LEVOX
           CALL GETGGBX(  OHPAL(1,L),NC4TO8("  OH"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  230     CONTINUE     
C
          DO 231  L=1,LEVOX
           CALL GETGGBX(H2O2PAL(1,L),NC4TO8("H2O2"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  231     CONTINUE     
C
          DO 232  L=1,LEVOX
           CALL GETGGBX(  O3PAL(1,L),NC4TO8("  O3"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  232     CONTINUE     
C
          DO 233  L=1,LEVOX
           CALL GETGGBX( NO3PAL(1,L),NC4TO8(" NO3"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  233     CONTINUE     
C
          DO 234  L=1,LEVOX
           CALL GETGGBX(HNO3PAL(1,L),NC4TO8("HNO3"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  234     CONTINUE     
C
          DO 235  L=1,LEVOX
           CALL GETGGBX( NH3PAL(1,L),NC4TO8(" NH3"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  235     CONTINUE     
C
          DO 236  L=1,LEVOX
           CALL GETGGBX( NH4PAL(1,L),NC4TO8(" NH4"),NUOX,NLON,NLAT,JOUR,
     1                  LX(L),GG)
  236     CONTINUE     
        ENDIF

      ENDIF 
      RETURN
C---------------------------------------------------------------------
 6000 FORMAT(' INTERPOLATING FOR', I5, ' BETWEEN', I5, ' AND',
     1       I5, ' WITH WEIGHTS=', 2F7.3)
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}


