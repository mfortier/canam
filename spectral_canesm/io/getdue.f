!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
        SUBROUTINE GETDUE(NF,SPOTPAK,ST01PAK,ST02PAK,
     1                  ST03PAK,ST04PAK,ST06PAK,ST13PAK,
     2                  ST14PAK,ST15PAK,ST16PAK,ST17PAK,
     3                  IJPAK,NLON,NLAT,GG)
C
C     * MAY/08 - Y.PENG. NEW ROUTINE FOR GCM16 TO GET
C     *                  FIELDS REQUIRED FOR NEW DUST
C     *                  EMISSION SCHEME.   
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C     * INVARIANT FIELDS:
C
      REAL SPOTPAK(IJPAK) !<Variable description\f$[units]\f$ 
      REAL ST01PAL(IJPAK) !<Variable description\f$[units]\f$
      REAL ST02PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST03PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST04PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST06PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST13PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST14PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST15PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST16PAK(IJPAK) !<Variable description\f$[units]\f$
      REAL ST17PAK(IJPAK) !<Variable description\f$[units]\f$

C
C     * WORK FIELD:
C
      REAL GG(1) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


C-----------------------------------------------------------------------
C     * GRIDS READ FROM FILE NF.

      REWIND NF
C
      CALL GETGGBX(SPOTPAK,NC4TO8("SPOT"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST01PAK,NC4TO8("ST01"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST02PAK,NC4TO8("ST02"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST03PAK,NC4TO8("ST03"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST04PAK,NC4TO8("ST04"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST06PAK,NC4TO8("ST06"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST13PAK,NC4TO8("ST13"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST14PAK,NC4TO8("ST14"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST15PAK,NC4TO8("ST15"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST16PAK,NC4TO8("ST16"),NF,NLON,NLAT,0,1,GG)
      CALL GETGGBX(ST17PAK,NC4TO8("ST17"),NF,NLON,NLAT,0,1,GG)
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

