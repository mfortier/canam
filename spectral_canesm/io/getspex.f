!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETSPEX(NF,PHIS,PS,T,P,C,ES,LA,LM,ILEV,LEVS,LS,LH,IDIV,
     1                   LATOTAL,LMTOTAL,LSRTOTAL,GLL             )
C
C     * NOV 03/03 - M.LAZARE. NEW VERSION BASED ON GETSPE, WHICH
C     *                       CALLS NEW ROUTINE "GETSPN" USING
C     *                       WORK ARRAY "GLL", TO GET LOAD-BALANCED
C     *                       SPECTRAL DATA APPLICABLE TO EACH NODE.
C     * SEP 27/92 - M.LAZARE. - PREVIOUS VERSION GETSPE.
C
C     * GETS INITIAL SPECTRAL VARIABLES FOR THE G.C.M. FROM FILE NF.
C     * LS,LH = LABEL SIGMA VALUES FOR FULL,HALF LEVELS.
C 
C     * INPUT FILE NF1 MUST CONTAIN SPECTRAL FIELDS ....
C     * MOUNTAINS            - 1 LEVEL
C     * LN(SURFACE PRESSURE) - 1 LEVEL
C     * TEMPERATURE          - HALF LEVELS 1 TO ILEV. 
C     * VORTICITY,DIVERGENCE - FULL LEVELS 1 TO ILEV (IN PAIRS) 
C     * MOISTURE VARIABLE    - HALF LEVELS (ILEV-LEVS+1) TO ILEV
C 
C     * NOTE - IF ANY FIELD IS MISSING, THE PROGRAM STOPS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX PHIS(1)     !<Variable description\f$[units]\f$
      COMPLEX PS(1)       !<Variable description\f$[units]\f$
      COMPLEX T(LA,1)     !<Variable description\f$[units]\f$
      COMPLEX P(LA,1)     !<Variable description\f$[units]\f$
      COMPLEX C(LA,1)     !<Variable description\f$[units]\f$
      COMPLEX ES(LA,1)    !<Variable description\f$[units]\f$
      COMPLEX GLL(LATOTAL)!<Variable description\f$[units]\f$

      INTEGER LS(1)       !<Variable description\f$[units]\f$
      INTEGER LH(1)       !<Variable description\f$[units]\f$
      INTEGER LSRTOTAL(2,LMTOTAL+1) !<Variable description\f$[units]\f$
      INTEGER IBUF(8)
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
C     * NODE INFORMATION.
C
      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE 

      LOGICAL OK,IDIV 
C------------------------------------------------------------------ 
C     * LOG OF SURFACE PRESSURE (NEWTONS /M**2) CONVERTED FROM MB.
C 
      CALL GETSPN(NF,PS,GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1            NC4TO8("SPEC"),0,NC4TO8("LNSP"),1,OK)
      IF(MYNODE.EQ.0) PS(1)=PS(1)+LOG(100.)*SQRT(2.0)
C 
C     * MOUNTAINS (M/SEC)**2. 
C 
      CALL GETSPN(NF,PHIS,GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1            NC4TO8("SPEC"),0,NC4TO8("PHIS"),1,OK)
C 
C     * TEMPERATURE  (DEG K). 
C 
      DO 250 L=1,ILEV 
      CALL GETSPN(NF,T(1,L),GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1            NC4TO8("SPEC"),0,NC4TO8("TEMP"),LH(L),OK)
  250 CONTINUE
C 
C     * VORTICITY AND DIVERGENCE (1./SEC).
C     * IF IDIV IS FALSE SET DIVERGENCE TO ZERO.
C 
      DO 350 L=1,ILEV 
      CALL GETSPN(NF,P(1,L),GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1            NC4TO8("SPEC"),0,NC4TO8("VORT"),LS(L),OK)
C
      CALL GETSPN(NF,C(1,L),GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1            NC4TO8("SPEC"),0,NC4TO8(" DIV"),LS(L),OK)
      IF(.NOT.IDIV) CALL SCOF2(C(1,L),LA,1,0) 
  350 CONTINUE
C 
C     * MOISTURE VARIABLE.
C 
      IF(LEVS.EQ.0) RETURN
      DO 450 N=1,LEVS 
      L=(ILEV-LEVS) + N 
      CALL GETSPN(NF, ES(1,N),GLL,LATOTAL,LMTOTAL,LA,LM,LSRTOTAL,
     1            NC4TO8("SPEC"),0,NC4TO8("  ES"),LH(L),OK)
  450 CONTINUE
      RETURN
C------------------------------------------------------------------ 
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

