!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * FEB 26/2009 - M.LAZARE/    NEW FOR GCM15H.
C     *               K.VONSALZEN.
C
C     * DEFINES EMISSION FIELDS USED BY GCM15H.
C
C=========================================================================
C     * Multi-month, single level: (read/interpolated by GETCHEM3/INTCHEM3)
C     * -------------------------------------------------------------------
C     * DMSO    KETTLE OCEANIC DMS CONCENTRATIONS (NANOMOL/LITRE)       
C     * EDMS    LAND DMS FLUXES (KG/M2-S)                                


C     * Multi-month, 6 "levels": (read/interpolated by GETWILDF/INTWF) 
C     * --------------------------------------------------------------
C     * EBWA    AEROCOM WILDFIRE BCC EMISSIONS (KG/M2-S) (1997-2002)      
C     * EOWA    AEROCOM WILDFIRE POM EMISSIONS (KG/M2-S) (1997-2002)      
C     * ESWA    AEROCOM WILDFIRE SO2 EMISSIONS (KG/M2-S) (1997-2002) 


C     * Annual Value, single level: (read by GETACE)
C     * --------------------------------------------
C     * EBBT    AEROCOM BIOFUEL BC EMISSIONS (KG/M2-S) (2000)             
C     * EOBT    AEROCOM BIOFUEL POM EMISSIONS (KG/M2-S) (2000)            
C     * EBFT    AEROCOM FOSSIL FUEL BC EMISSIONS (KG/M2-S) (2000)         
C     * EOFT    AEROCOM FOSSIL FUEL POM EMISSIONS (KG/M2-S) (2000)        
C     * ESDT    AEROCOM DOMESTIC SO2 EMISSIONS (KG/M2-S) (2000)           
C     * ESIT    AEROCOM INDUSTRIAL SO2 EMISSIONS (KG/M2-S) (2000)         
C     * ESST    AEROCOM SHIPPING SO2 EMISSIONS (KG/M2-S) (2000)           
C     * ESOT    AEROCOM OFF-ROAD SO2 EMISSIONS (KG/M2-S) (2000)           
C     * ESPT    AEROCOM POWER PLANT SO2 EMISSIONS (KG/M2-S) (2000)        
C     * ESRT    AEROCOM ROAD SO2 EMISSIONS (KG/M2-S) (2000) 


C     * Multi-month, single level: (read/interpolated by GETCAC/INTCAC)
C     * -------------------------------------------------------------------
C     * EOST    AEROCOM SECONDARY ORGANIC EMISSIONS (KG/M2-S) (2000) 


C     * Volcano fields (annual value, single level): (read by GETNAE)
C     * -------------------------------------------------------------
C     * ESCV    AEROCOM CONTINUOUS VOLCANO SO2 EMISSIONS (KG/M2-S)        
C     * EHCV    AEROCOM CONTINUOUS VOLCANO HEIGHT (M)                     
C     * ESEV    AEROCOM EXPLOSIVE  VOLCANO SO2 EMISSIONS (KG/M2-S)        
C     * EHEV    AEROCOM EXPLOSIVE  VOLCANO HEIGHT (M)                     

C     * Multi-month, single level: (read/interpolated by GETCH/INTHEM)
C     * These are for "transient historic emissions" (1850-2000)
C     * AND activated by "%df histemi"
C     * --------------------------------------------------------------
C     * EBOC    OPEN VEGETATION BURNING ORGANIC CARBON EMISSIONS (KG/M2-S)  
C     * EBBC    OPEN VEGETATION BURNING BLACK   CARBON EMISSIONS (KG/M2-S)
C     * EAOC    FOSSIL AND BIO FUEL ORGANIC CARBON EMISSIONS (KG/M2-S)
C     * EABC    FOSSIL AND BIO FUEL BLACK CARBON EMISSIONS (KG/M2-S)
C     * EASL    ANTHROPOGENIC SULPHUR EMISSIONS FOR LOW LEVEL (KG/M2-S)
C     * EASH    ANTHROPOGENIC SULPHUR EMISSIONS FOR HIGH LEVEL (KG/M2-S)

C     * Multi-month, single level: (read/interpolated by GETCH/INTHEM)
C     * These are for "transient historic emissions" (1850-2000)
C     * AND activated by "%df emists", in KG/M2-S (S or C).
C     * --------------------------------------------------------------

C     * SAIR    AIRCRAFT EMISSIONS OF SULPHUR DIOXIDE
C     * SSFC    ANTHROPOGENIC SURFACE EMISSIONS OF SULPHUR DIOXDE
C     * SSTK    STACK EMISSIONS OF SULPHUR DIOXIDE
C     * SFIR    FOREST FIRE EMISSIONS OF SULPHUR DIOXIDE
C     * OAIR    AIRCRAFT EMISSIONS OF ORGANIC CARBON AEROSOL
C     * OSFC    ANTHROPOGENIC SURFACE EMISSIONS OF ORGANIC CARBON AEROSOL
C     * OSTK    STACK EMISSIONS OF ORGANIC CARBON AEROSOL
C     * OFIR    FOREST FIRE EMISSIONS OF ORGANIC CARBON AEROSOL
C     * BAIR    AIRCRAFT EMISSIONS OF BLACK CARBON AEROSOL
C     * BSFC    ANTHROPOGENIC SURFACE EMISSIONS OF BLACK CARBON AEROSOL
C     * BSTK    STACK EMISSIONS OF BLACK CARBON AEROSOL
C     * BFIR    FOREST FIRE EMISSIONS OF BLACK CARBON AEROSOL

C     * The following are for "transient historic emissions" (1850-2000)
C     * REGARDLESS of the "%df histemi" or "%df emists" settings
 
C     * Multi-month, LEVWF levels: (read/interpolated by GETWFH/INTPRF)
C     * ---------------------------------------------------------------
C     * FBBC    FRACTION OF EMISSION BY LEVEL FOR OPEN VEGETATION BURNING  (2000)

C     * Multi-month, LEVAIR levels: (read/interpolated by GETACH/INTPRF)
C     * ---------------------------------------------------------------
C     * FAIR   FRACTION OF EMISSION BY LEVEL FOR AIRCRAFT EMISSIONS
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

