#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * NOV 14/2003 - D.ROBITAILLE 

C     Define some variable types for MPI communication

      integer*4 me, mpiproc, proc
      integer*4 coupler, atmos, ocean
      character*8 member(0:1000)

      common /mpimodules/ coupler, atmos, ocean, member

      integer*4 status(MPI_status_size), ierr

      integer*4 ibuflen
      parameter (ibuflen=8)


      character*3 cmodel
      integer*4 cpl_rank_min, cpl_rank_max
      integer*4 ocn_rank_min, ocn_rank_max
      integer*4 atm_rank_min, atm_rank_max
      integer*4 icolor,ikey
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
