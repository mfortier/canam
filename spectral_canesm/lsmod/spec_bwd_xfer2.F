#include "cppdef_config.h"
!!--- NEW: #define with_MPI_

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      subroutine spec_bwd_xfer2(nl2, ilat, nlat, LM, LMTotal, yy, xx,
     1                          sbuf,sz,soffset,roffset)
c
c     * Sep 12/2006 - M.Lazare. New version for gcm15f, based on old
c     *                         spec_bwd,xfer, using native reals
c     *                         instead of explicit real(8), to
c     *                         support 32-bit driver.
c     * Jan 23/2004 - R.Mclay.  Previous version spec_bwd_xfer
c     *                         developed for conversion to IBM.
c
      implicit none
C
C     * COMMON BLOCK TO HOLD THE NUMBER OF NODES AND COMMUNICATOR.
C
      INTEGER(4) :: MYNODE 
      common /mpinfo/MYNODE 

      INTEGER(4) :: NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE, MY_REAL_TYPE
      INTEGER(4) :: MY_INT_TYPE
      COMMON /MPICOMM/ NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE,MY_REAL_TYPE, 
     &                 MY_INT_TYPE

      integer(4) :: ierr, offset, inode, sz(0:nnode-1)
      integer(4) :: soffset(0:nnode-1), roffset(0:nnode-1)
      integer    :: nl2  	!<Variable description\f$[units]\f$
      integer    :: ilat 	!<Variable description\f$[units]\f$
      integer    :: nlat	!<Variable description\f$[units]\f$
      integer    :: LM		!<Variable description\f$[units]\f$
      integer    :: LMTotal	!<Variable description\f$[units]\f$
      integer    :: LMHalf, m
      integer    :: mm, jj, j
      real       :: xx(  nl2, nlat, LM)			!<Variable description\f$[units]\f$
      real       :: yy(  nl2, ilat, LM,   0:nnode-1)	!<Variable description\f$[units]\f$
      real       :: sbuf(nl2, ilat, LM/2, 0:nnode-1)	!<Variable description\f$[units]\f$
C===============================================================

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

#ifdef with_MPI_
      LMHalf = LM/2
      
      offset = 0
      do inode = 0, nnode - 1
        sz(inode)      = nl2*ilat*LMHalf
        soffset(inode) = offset
        roffset(inode) = offset
        offset         = offset + sz(inode)
      enddo

      call MPI_ALLtoALLV( yy(1,1,1,0),   sz, soffset, MY_REAL_TYPE, 
     &                    sbuf(1,1,1,0), sz, roffset, MY_REAL_TYPE, 
     &                    AGCM_COMMWRLD, ierr)

      do mm = 1, LMHalf
        jj = 1
        do inode = 0, nnode - 1
          xx(:, jj:jj+ilat-1, 2*mm-1) = sbuf(:, :, mm, inode)
          jj = jj + ilat
        enddo
      enddo


      do inode = 0, nnode - 1
        soffset(nnode - inode - 1) = offset
        offset                     = offset + sz(inode)
      enddo


      call MPI_ALLtoALLV( yy(1,1,1,0),   sz, soffset, MY_REAL_TYPE, 
     &                    sbuf(1,1,1,0), sz, roffset, MY_REAL_TYPE, 
     &                    AGCM_COMMWRLD, ierr)

      do mm = 1, LMHalf
        jj = 1
        do inode = 0, nnode - 1
          xx(:, jj:jj+ilat-1, LM - 2*mm + 2) = sbuf(:, :, mm, inode)
          jj = jj + ilat
        enddo
      enddo
#endif
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

