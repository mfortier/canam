#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * Apr 20/2019 - S.Kharin    - Fix bug IOFFZ->IOFF in *_EXT_GCM_SA_* fields
C     * Mar 16/2019 - J. Cole     - Add  3-hour 2D fields (CF3hr,E3hr)
C     * Nov 29/2018 - J. Cole     - Add/adjust tendencies
C     * Nov 02/2018 - J. Cole     - Add radiative flux profiles
C     * Nov 03/2018 - M.Lazare.   - Added GSNO and FNLA.
C     *                           - Added 3HR save fields.
C     * Oct 01/2018 - S.Kharin.   - Add XTVIPAS/XTVIROS for sample burdens.
C     * Aug 23/2018 - Vivek Arora - Remove PFHC and PEXF
C     * Aug 14/2018 - M.Lazare.     Remove MASKPAK,GTPAL.
C     * Aug 14/2018 - M.Lazare.     Remove {GTM,SICP,SICM,SICNP,SICM}.
C     * Aug 01/2018 - M.Lazare.     Remove {OBEG,OBWG,RES,GC}.
C     * Jul 30/2018 - M.Lazare.   - Unused FMIROW removed.
C     *                           - Removed non-transient aerosol emission cpp blocks.
C     * Mar 09/2018 - M.Lazare.   - BEG  and BWG  changed from PAK/ROW to
C     *                             PAL/ROL.
C     * Feb 27/2018 - M.Lazare.   - QFSL and BEGL changed from PAK/ROW to
C     *                             PAL/ROL.
C     *                           - Added {BEGK,BWGK,BWGL,QFSO}.
C     * Nov 01/2017 - J. Cole.     - Update for CMIP6 stratospheric aerosols
C     * Aug 09/2017 - M.Lazare.    - Add FNPAT/FNROT.
C     *                            - FLAK->{FLKR,FLKU}.
C     *                            - LICN->GICN.
C     *                            - Add lakes arrays.
C     * Aug 07/2017 - M.Lazare.    - Initial Git verison.
C     * Mar 26/2015 - M.Lazare.    Final version for gcm18:
C     *                            - Unused {CSTROL,CLTROL} removed.
C     *                            - Add (for NEMO/CICE support):
C     *                              HSEAROL,RAINSROL,SNOWSROL
C     *                              OBEGROL,OBWGROL,BEGOROL,BWGOROL,
C     *                              BEGIROL,HFLIROL,
C     *                              HSEAROL,RAINSROL,SNOWSROL.
C     *                            - Remove: FTILPAK/FTILROW.
C     *                            - Add (for harmonization of field
C     *                              capacity and wilting point between
C     *                              CTEM and CLASS):
C     *                              THLWPAT/THLWROT.
C     *                            - Add (for soil colour index look-up
C     *                              for land surface albedo):
C     *                              ALGDVPAT/ALGDVROT, ALGDNPAT/ALGDNROT,
C     *                              ALGWVPAT/ALGWVROT, ALGWNPAT/ALGWNROT,
C     *                              SOCIPAT/SOCIROT.
C     *                            - Add (for fractional land/water/ice):
C     *                              SALBPAT/SALBROT, CSALPAT/CSALROT,
C     *                              EMISPAK/EMISROW, EMISPAT/EMISROT,
C     *                              WRKAPAL/WRKAROL, WRKBPAL/WRKBROL,
C     *                              SNOPAKO/SNOROWO.
C     *                            - Add (for PLA):
C     *                              PSVVPAK/PSVVROW, PDEVPAK/PDEVROW,
C     *                              PDIVPAK/PDIVROW, PREVPAK/PREVROW,
C     *                              PRIVPAK/PRIVROW.
C     *                            - Bugfixes for ISCCP fields and additions
C     *                              for CALIPSO and MODIS.
C     * Feb 04/2014 - M.Lazare.    Interim version for gcm18:
C     *                            - FLAKPAK,FLNDPAK,GTPAT,LICNPAK added.
C     * Nov 19/2013 - M.Lazare.    Cosmetic: Remove {GFLX,GA,HBL,ILMO,PET,UE,
C     *                                      WTAB,ROFS,ROFB) "PAT"/"ROT" arrays.
C     * Jul 10/2013 - M.Lazare/    Previous version unpack9 for gcm17:
C     *               K.Vonsalzen/ - FSF added as prognostic field
C     *               J.Cole/        rather than residual.
C     *                            - Extra diagnostic microphysics
C     *                              fields added:
C     *                              {SEDI,RLIQ,RICE,RLNC,CLIQ,CICE,
C     *                               VSEDI,RELIQ,REICE,CLDLIQ,CLDICE,CTLNC,CLLNC}.
C     *                            - New emission fields:
C     *                              {SBIO,SSHI,OBIO,OSHI,BBIO,BSHI} and
C     *                              required altitude field ALTI.
C     *                            - Many new aerosol diagnostic fields
C     *                              for PAM, including those for cpp options:
C     *                              "pfrc", "xtrapla1" and "xtrapla2".
C     *                            - The following fields were removed
C     *                              from the "aodpth" cpp option:
C     *                              {SAB1,SAB2,SAB3,SAB4,SAB5,SABT} and
C     *                              {SAS1,SAS2,SAS3,SAS4,SAS5,SAST},
C     *                              (both PAK and PAL).
C     *                            - Due to the implementation of the mosaic
C     *                              for CLASS_v3.6, prognostic fields
C     *                              were changed from PAK/ROW to PAT/ROT,
C     *                              with an extra "IM" dimension (for
C     *                              the number of mosaic tiles).
C     *                            - The instantaneous band-mean values for
C     *                              solar fields are now prognostic
C     *                              as well: {FSDB,FSFB,CSDB,CSFB,FSSB,FSSCB}.
C     *                            - Removed "histemi" fields.
C     *                            - "xtraconv" cpp option moved to before
C     *                              "xtrachem", to be consistent with others.
C     * NB: The following are intermediate revisions to frozen gcm16 code
C     *     (upwardly compatible) to support the new CLASS version in development:
C     * Oct 18/2011 - M.Lazare.    - Add THR,THM,BI,PSIS,GRKS,THRA,HCPS,TCS,THFC,
C     *                              PSIW,ALGD,ALGW,ZBTW,ISND,IGDR but only for
C     *                              KOUNT.GT.KSTART.
C     * Oct 07/2011 - M.Lazare.    - Add GFLX,GA,HBL,PET,ILMO,ROFB,ROFS,UE,WTAB.
C     * Jul 13/2011 - E.Chan.      - Add FARE, THLQ, THIC. Change selected
C     *                              ROW variables to tile versions (ROT).
C     *                            - Add selected ROT variables in
C     *                              addition to their ROW counterparts.
C     *                            - Add TPND, ZPND, TAV, QAV, WSNO, TSFS.
C     * MAY 03/2012 - M.LAZARE/    PREVIOUS VERSION UNPACK8 FOR GCM16:
C     *               K.VONSALZEN/ - MODIFY FIELDS TO SUPPORT A NEWER
C     *               J.COLE/        VERSION OF COSP WHICH INCLUDES
C     *               Y.PENG.        THE MODIS, MISR AND CERES AND
C     *                              SAVE THE APPROPRIATE OUTPUT.
C     *                            - REMOVE {FSA,FLA,FSTC,FLTC} AND
C     *                              REPLACE BY {FSR,OLR,FSRC,OLRC}.
C     *                            - NEW CONDITIONAL DIAGNOSTIC OUTPUT
C     *                              UNDER CONTROL OF "XTRADUST" AND
C     *                              "AODPTH".
C     *                            - ADDITIONAL MAM RADIATION OUTPUT
C     *                              FIELDS {FSAN,FLAN}.
C     *                            - ADDITIONAL CORRELATED-K OUTPUT
C     *                              FIELDS: "FLG", "FDLC" AND "FLAM".
C     *                            - "FLGROL_R" IS ACTUAL CALCULATED
C     *                              RADIATIVE FORCING TERM INSTEAD
C     *                              OF "FDLROL_R-SIGMA*T**4".
C     *                            - INCLUDE "ISEED" FOR RANDOM NUMBER
C     *                              GENERATOR SEED NOW CALCULATED
C     *                              IN MODEL DRIVER INSTEAD OF PHYSICS.
C     * MAY 02/2010 - M.LAZARE/    PREVIOUS VERSION UNPACK7I FOR GCM15I:
C     *               K.VONSALZEN/ - ADD FSOPAL/FSOROL ("FSOL").
C     *               J.COLE.      - ADD FIELDS {RMIX,SMIX,RREF,SREF}
C     *                              FOR COSP INPUT, MANY FIELDS FOR
C     *                              COSP OUTPUT (WITH DIFFERENT OPTIONS)
C     *                              AND REMOVE PREVIOUS DIRECT ISCCP
C     *                              FIELDS.
C     *                            - ADD NEW DIAGNOSTIC FIELDS:
C     *                              SWA (PAK/ROW AND PAL/ROL),SWX,SWXU,
C     *                              SWXV,SRH,SRHN,SRHX,FSDC,FSSC,FDLC
C     *                              AND REMOVE: SWMX.
C     *                            - FOR CONVECTION, ADD: DMCD,DMCU AND
C     *                              REMOVE: ACMT,DCMT,SCMT,PCPS,CLDS,
C     *                              LHRD,LHRS,SHRD,SHRS.
C     *                            - ADD FIELDS FOR CTEM UNDER CONTROL
C     *                              OF NEW CPP DIRECTIVE: "COUPLER_CTEM".
C     *                            - ADD NEW FIELDS FOR COUPLER: OFSG,
C     *                              PHIS,PMSL,XSRF.
C     *                            - PREVIOUS UPDATE DIRECIVE "HISTEMI"
C     *                              CONVERTED TO CPP:
C     *                              "TRANSIENT_AEROSOL_EMISSIONS" WITH
C     *                              FURTHER CHOICE OF CPP DIRECTIVES
C     *                              "HISTEMI" FOR HISTORICAL OR
C     *                              "EMISTS" FOR FUTURE EMISSIONS.
C     *                              THE "HISTEMI" FIELDS ARE THE
C     *                              SAME AS PREVIOUSLY. FOR "EMISTS",
C     *                              THE FOLLOWING ARE ADDED (BOTH PAK/ROW
C     *                              AND PAL/ROL SINCE INTERPOLATED):
C     *                              SAIR,SSFC,SSTK,SFIR,OAIR,OSFC,OSTK,OFIR,
C     *                              BAIR,BSFC,BSTK,BFIR. THE FIRST LETTER
C     *                              INDICATES THE SPECIES ("B" FOR BLACK
C     *                              CARBON, "O" FOR ORGANIC CARBON AND
C     *                              "S" FOR SULFUR), WHILE FOR EACH OF
C     *                              THESE, THERE ARE "AIR" FOR AIRCRAFT,
C     *                              "SFC" FOR SURFACE, "STK" FOR STACK
C     *                              AND "FIR" FOR FIRE, EACH HAVING
C     *                              DIFFERENT EMISSION HEIGHTS.
C     *                            - FOR THE CHEMISTRY, THE FOLLOWING
C     *                              FIELDS HAVE BEEN ADDED:
C     *                              DDD,DDB,DDO,DDS,WDLD,WDLB,WDLO,WDLS,
C     *                              WDDD,WDDB,WDDO,WDDS,WDSD,WDSB,WDSO,
C     *                              WDSS,ESD,ESFS,EAIS,ESTS,EFIS,ESFB,
C     *                              EAIB,ESTB,EFIB,ESFO,EAIO,ESTO,EFIO
C     *                              AND THE FOLLOWING HAVE BEEN REMOVED:
C     *                              ASFS,ASHP,ASO3,AWDS,DAFX,DCFX,DDA,DDC,
C     *                              ESBT,ESFF,EOFF,EBFF,EOBB,EBBB,ESWF,
C     *                              SFD,SFS,WDDA,WDDC,WDLA,WDLC,WDSA,WDSC,
C     *                              CDPH,CLPH,CSPH.
C     *                            - FAIRPAK/FAIRROW AND FAIRPAL/FAIRROL
C     *                              ADDED FOR AIRCRAFT EMISSIONS
C     *                            - O3CPAK/O3CROW AND O3CPAL/O3CROL
C     *                              ADDED FOR CHEMICAL OZONE INTERPOLATION.
C     *                            - UPDATE DIRECTIVES TURNED INTO
C     *                              CPP DIRECTIVES.
C     * FEB 19/2009 - M.LAZARE.    PREVIOUS VERSION UNPACK7H FOR GCM15H:
C     *                            - ADD NEW FIELDS FOR EMISSIONS: EOFF,
C     *                              EBFF,EOBB,EBBB.
C     *                            - ADD NEW FIELDS FOR DIAGNOSTICS OF
C     *                              CONSERVATION: QTPT,XTPT.
C     *                            - ADD NEW FIELD FOR CHEMISTRY: SFRC.
C     *                            - ADD NEW DIAGNOSTIC CLOUD FIELDS
C     *                              (USING OPTICAL DEPTH CUTOFF):
C     *                              CLDO (BOTH PAK AND PAL).
C     *                            - REORGANIZE EMISSION FORCING FIELDS
C     *                              DEPENDANT WHETHER ARE UNDER
C     *                              HISTORICAL EMISSIONS (%DF HISTEMI)
C     *                              OR NOT.
C     *                            - ADD FIELDS FOR EXPLOSIVE VOLCANOES:
C     *                              VTAU AND TROP UNDER CONTROL OF
C     *                              "%DF EXPLVOL". EACH HAVE BOTH PAK
C     *                              AND PAL.
C     * APR 21/2008 - L.SOLHEIM/   PREVIOUS VERSION UNPACK7G FOR GCM15G:
C     *               M.LAZARE/    -  ADD NEW RADIATIVE FORCING ARRAYS
C     *               K.VONSALZEN/    (UNDER CONTROL OF "%DF RADFORCE").
C     *               X.MA.        - NEW DIAGNOSTIC FIELDS: WDD4,WDS4,EDSL,
C     *                              ESBF,ESFF,ESVC,ESVE,ESWF ALONG WITH
C     *                              TDEM->EDSO (UNDER CONTROL OF
C     *                              "XTRACHEM"), AS WELL AS ALMX,ALMC
C     *                              AND INSTANTANEOUS CLWT,CIDT.
C     *                            - NEW AEROCOM FORCING FIELDS:
C     *                              EBWA,EOWA,ESWA,EOST (EACH WITH ACCUMULATED
C     *                              AND TARGET),ESCV,EHCV,ESEV,EHEV,
C     *                              EBBT,EOBT,EBFT,EOFT,ESDT,ESIT,ESST,
C     *                              ESOT,ESPT,ESRT.
C     *                            - REMOVE UNUSED: QTPN,UTPM,VTPM,TSEM,EBC,
C     *                              EOC,EOCF,ESO2,EVOL,HVOL.
C     * JAN 11/2006 - J.COLE/      PREVIOUS VERSION UNPACK7F FOR GCM15F:
C     *               M.LAZARE.    - ADD ISCCP SIMULATOR FIELDS FROM JASON.
C     * NOV 26/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - TWO EXTRA NEW FIELDS ("DMC" AND "SMC")
C     *                              UNDER CONTROL OF "%IF DEF,XTRACONV".
C     * MAY 07/2006 - M.LAZARE.    PREVIOUS VERSION UNPACK7E FOR GCM15E.
C========================================================================
C     * GENERAL PHYSICS ARRAYS.
C
      DO L=1,ILEV
        DO I=IL1,IL2
          ALMXROW(I,L)  = ALMXPAK(IOFF+I,L)
          ALMCROW(I,L)  = ALMCPAK(IOFF+I,L)
          CICROW (I,L)  = CICPAK (IOFF+I,L)
          CLCVROW(I,L)  = CLCVPAK(IOFF+I,L)
          CLDROW (I,L)  = CLDPAK (IOFF+I,L)
          CLWROW (I,L)  = CLWPAK (IOFF+I,L)
          CVARROW(I,L)  = CVARPAK(IOFF+I,L)
          CVMCROW(I,L)  = CVMCPAK(IOFF+I,L)
          CVSGROW(I,L)  = CVSGPAK(IOFF+I,L)
          HRLROW (I,L)  = HRLPAK (IOFF+I,L)
          HRSROW (I,L)  = HRSPAK (IOFF+I,L)
          HRLCROW (I,L) = HRLCPAK (IOFF+I,L)
          HRSCROW (I,L) = HRSCPAK (IOFF+I,L)
          OMETROW(I,L)  = OMETPAK(IOFF+I,L)
          QWF0ROL(I,L)  = QWF0PAL(IOFF+I,L)
          QWFMROL(I,L)  = QWFMPAL(IOFF+I,L)
          RHROW  (I,L)  = RHPAK  (IOFF+I,L)
          SCDNROW(I,L)  = SCDNPAK(IOFF+I,L)
          SCLFROW(I,L)  = SCLFPAK(IOFF+I,L)
          SLWCROW(I,L)  = SLWCPAK(IOFF+I,L)
          TACNROW(I,L)  = TACNPAK(IOFF+I,L)
          ZDETROW(I,L)  = ZDETPAK(IOFF+I,L)
        ENDDO
      ENDDO
C
      DO N=1,NTRAC
        DO L=1,ILEV
          DO I=IL1,IL2
            SFRCROL(I,L,N) = SFRCPAL(IOFF+I,L,N)
          ENDDO
        ENDDO
      ENDDO
      DO L=1,ILEV
      DO I=IL1,IL2
        ALTIROW(I,L)   = ALTIPAK(IOFF+I,L)
      ENDDO
      ENDDO
C
      DO L=1,LEVOZ
        DO I=IL1,IL2
          OZROL  (I,L) = OZPAL  (IOFF+I,L)
          OZROW  (I,L) = OZPAK  (IOFF+I,L)
        ENDDO
      ENDDO
C
      DO L=1,LEVOZC
        DO I=IL1,IL2
          O3CROL (I,L) = O3CPAL (IOFF+I,L)
          O3CROW (I,L) = O3CPAK (IOFF+I,L)
        ENDDO
      ENDDO
C
      CSALROL(IL1:IL2,1:NBS)     =CSALPAL(IOFF+IL1:IOFF+IL2,1:NBS)
      CSALROT(IL1:IL2,1:IM,1:NBS)=CSALPAT(IOFF+IL1:IOFF+IL2,1:IM,1:NBS)
      SALBROL(IL1:IL2,1:NBS)     =SALBPAL(IOFF+IL1:IOFF+IL2,1:NBS)
      SALBROT(IL1:IL2,1:IM,1:NBS)=SALBPAT(IOFF+IL1:IOFF+IL2,1:IM,1:NBS)
      CSDROL(IL1:IL2) = CSDPAL(IOFF+IL1:IOFF+IL2)
      CSFROL(IL1:IL2) = CSFPAL(IOFF+IL1:IOFF+IL2)
C
      DO I=IL1,IL2
        ALPHROW(I) = ALPHPAK(IOFF+I)
        BEGIROL(I) = BEGIPAL(IOFF+I)
        BEGKROL(I) = BEGKPAL(IOFF+I)
        BEGLROL(I) = BEGLPAL(IOFF+I)
        BEGOROL(I) = BEGOPAL(IOFF+I)
        BEGROL (I) = BEGPAL (IOFF+I)
        BWGIROL(I) = BWGIPAL(IOFF+I)
        BWGKROL(I) = BWGKPAL(IOFF+I)
        BWGLROL(I) = BWGLPAL(IOFF+I)
        BWGOROL(I) = BWGOPAL(IOFF+I)
        BWGROL (I) = BWGPAL (IOFF+I)
        CBMFROL(I) = CBMFPAL(IOFF+I)
        CHFXROW(I) = CHFXPAK(IOFF+I)
        CICTROW(I) = CICTPAK(IOFF+I)
        CLBROL (I) = CLBPAL (IOFF+I)
        CLDTROW(I) = CLDTPAK(IOFF+I)
        CLDOROW(I) = CLDOPAK(IOFF+I)
        CLDOROL(I) = CLDOPAL(IOFF+I)
        CLDAROW(I) = CLDAPAK(IOFF+I)
        CLDAROL(I) = CLDAPAL(IOFF+I)
        CLWTROW(I) = CLWTPAK(IOFF+I)
        CQFXROW(I) = CQFXPAK(IOFF+I)
        CSBROL (I) = CSBPAL (IOFF+I)
        CSZROL (I) = CSZPAL (IOFF+I)
        DELTROW(I) = DELTPAK(IOFF+I)
        DMSOROL(I) = DMSOPAL(IOFF+I)
        DMSOROW(I) = DMSOPAK(IOFF+I)
        EDMSROL(I) = EDMSPAL(IOFF+I)
        EDMSROW(I) = EDMSPAK(IOFF+I)
        EMISROW(I) = EMISPAK(IOFF+I)
        ENVROW (I) = ENVPAK (IOFF+I)
        FDLCROW(I) = FDLCPAK(IOFF+I)
        FDLROW (I) = FDLPAK (IOFF+I)
        FLAMROL(I) = FLAMPAL(IOFF+I)
        FLAMROW(I) = FLAMPAK(IOFF+I)
        FLANROW(I) = FLANPAK(IOFF+I)
        FLANROL(I) = FLANPAL(IOFF+I)
        FLGCROW(I) = FLGCPAK(IOFF+I)
        FLGROW (I) = FLGPAK (IOFF+I)
        FLKRROW(I) = FLKRPAK(IOFF+I)
        FLKUROW(I) = FLKUPAK(IOFF+I)
        FLNDROW(I) = FLNDPAK(IOFF+I)
        FSAMROL(I) = FSAMPAL(IOFF+I)
        FSAMROW(I) = FSAMPAK(IOFF+I)
        FSANROL(I) = FSANPAL(IOFF+I)
        FSANROW(I) = FSANPAK(IOFF+I)
        FSDCROW(I) = FSDCPAK(IOFF+I)
        FSDROL (I) = FSDPAL (IOFF+I)
        FSDROW (I) = FSDPAK (IOFF+I)
        FSFROL (I) = FSFPAL (IOFF+I)
        FSGCROW(I) = FSGCPAK(IOFF+I)
        FSGIROL(I) = FSGIPAL(IOFF+I)
        FSGOROL(I) = FSGOPAL(IOFF+I)
        FSGROW (I) = FSGPAK (IOFF+I)
        FSIROL (I) = FSIPAL (IOFF+I)
        FSLOROL(I) = FSLOPAL(IOFF+I)
        FSLOROW(I) = FSLOPAK(IOFF+I)
        FSOROW (I) = FSOPAK (IOFF+I)
        FSRCROW(I) = FSRCPAK(IOFF+I)
        FSRROW (I) = FSRPAK (IOFF+I)
        FSSCROW(I) = FSSCPAK(IOFF+I)
        FSSROW (I) = FSSPAK (IOFF+I)
        FSVROL (I) = FSVPAL (IOFF+I)
        FSVROW (I) = FSVPAK (IOFF+I)
        GAMROW (I) = GAMPAK (IOFF+I)
        GICNROW(I) = GICNPAK(IOFF+I)
        GTAROW (I) = GTAPAK (IOFF+I)
        GTROW  (I) = GTPAK  (IOFF+I)
        HFLROW (I) = HFLPAK (IOFF+I)
        HFSROW (I) = HFSPAK (IOFF+I)
        HSEAROL(I) = HSEAPAL(IOFF+I)
        OFSGROL(I) = OFSGPAL(IOFF+I)
        OLRROW (I) = OLRPAK (IOFF+I)
        OLRCROW(I) = OLRCPAK(IOFF+I)
        PARROL (I) = PARPAL (IOFF+I)
        PBLHROW(I) = PBLHPAK(IOFF+I)
        PBLTROW(I) = PBLTPAK(IOFF+I)
        PCHFROW(I) = PCHFPAK(IOFF+I)
        PCPCROW(I) = PCPCPAK(IOFF+I)
        PCPROW (I) = PCPPAK (IOFF+I)
        PCPSROW(I) = PCPSPAK(IOFF+I)
        PHISROW(I) = PHISPAK(IOFF+I)
        PLHFROW(I) = PLHFPAK(IOFF+I)
        PMSLROL(I) = PMSLPAL(IOFF+I)
        PSAROW (I) = PSAPAK (IOFF+I)
        PSHFROW(I) = PSHFPAK(IOFF+I)
        PSIROW (I) = PSIPAK (IOFF+I)
        PWATROW(I) = PWATPAK(IOFF+I)
        PWATROM(I) = PWATPAM(IOFF+I)
        QFSROW (I) = QFSPAK (IOFF+I)
        QFSLROL(I) = QFSLPAL(IOFF+I)
        QFSOROL(I) = QFSOPAL(IOFF+I)
        QFXROW (I) = QFXPAK (IOFF+I)
        QTPHROM(I) = QTPHPAM(IOFF+I)
        QTPFROM(I) = QTPFPAM(IOFF+I)
        QTPTROM(I) = QTPTPAM(IOFF+I)
        RAINSROL(I)= RAINSPAL(IOFF+I)
        SICNROW(I) = SICNPAK(IOFF+I)
        SICROW (I) = SICPAK (IOFF+I)
        SIGXROW(I) = SIGXPAK(IOFF+I)
        SLIMROL(I) = SLIMPAL(IOFF+I)
        SLIMRlw(I) = SLIMPlw(IOFF+I)
        SLIMRsh(I) = SLIMPsh(IOFF+I)
        SLIMRlh(I) = SLIMPlh(IOFF+I)
        SNOROW (I) = SNOPAK (IOFF+I)
        SNOWSROL(I)= SNOWSPAL(IOFF+I)
        SQROW  (I) = SQPAK  (IOFF+I)
        SRHROW (I) = SRHPAK (IOFF+I)
        SRHNROW(I) = SRHNPAK(IOFF+I)
        SRHXROW(I) = SRHXPAK(IOFF+I)
        STMNROW(I) = STMNPAK(IOFF+I)
        STMXROW(I) = STMXPAK(IOFF+I)
        STROW  (I) = STPAK  (IOFF+I)
        SUROW  (I) = SUPAK  (IOFF+I)
        SVROW  (I) = SVPAK  (IOFF+I)
        SWAROW (I) = SWAPAK (IOFF+I)
        SWAROL (I) = SWAPAL (IOFF+I)
        SWXROW (I) = SWXPAK (IOFF+I)
        SWXUROW(I) = SWXUPAK(IOFF+I)
        SWXVROW(I) = SWXVPAK(IOFF+I)
        TCVROW (I) = TCVPAK (IOFF+I)
        TFXROW (I) = TFXPAK (IOFF+I)
        UFSROL (I) = UFSPAL (IOFF+I)
        UFSROW (I) = UFSPAK (IOFF+I)
        UFSIROL(I) = UFSIPAL(IOFF+I)
        UFSOROL(I) = UFSOPAL(IOFF+I)
        VFSROL (I) = VFSPAL (IOFF+I)
        VFSROW (I) = VFSPAK (IOFF+I)
        VFSIROL(I) = VFSIPAL(IOFF+I)
        VFSOROL(I) = VFSOPAL(IOFF+I)
      ENDDO
      SNOROT(IL1:IL2,:)=SNOPAT(IOFF+IL1:IOFF+IL2,:)
      GTROT (IL1:IL2,:)=GTPAT (IOFF+IL1:IOFF+IL2,:)
      GTROX (IL1:IL2,:)=GTPAX (IOFF+IL1:IOFF+IL2,:)
      EMISROT(IL1:IL2,:)=EMISPAT(IOFF+IL1:IOFF+IL2,:)
      XLMROT(IL1:IL2,:,:)=XLMPAT(IOFF+IL1:IOFF+IL2,:,:)
C
      DO L = 1, NBS
         DO I = IL1, IL2
            FSDBROL (I,L)  = FSDBPAL (IOFF+I,L)
            FSFBROL (I,L)  = FSFBPAL (IOFF+I,L)
            CSDBROL (I,L)  = CSDBPAL (IOFF+I,L)
            CSFBROL (I,L)  = CSFBPAL (IOFF+I,L)
            FSSBROL (I,L)  = FSSBPAL (IOFF+I,L)
            FSSCBROL (I,L) = FSSCBPAL (IOFF+I,L)
            WRKAROL (I,L)  = WRKAPAL (IOFF+I,L)
            WRKBROL (I,L)  = WRKBPAL (IOFF+I,L)
         END DO
      END DO
      CLBROT (IL1:IL2,:)=CLBPAT (IOFF+IL1:IOFF+IL2,:)
      CSBROT (IL1:IL2,:)=CSBPAT (IOFF+IL1:IOFF+IL2,:)
      CSDROT (IL1:IL2,:)=CSDPAT (IOFF+IL1:IOFF+IL2,:)
      CSFROT (IL1:IL2,:)=CSFPAT (IOFF+IL1:IOFF+IL2,:)
      FDLROT (IL1:IL2,:)=FDLPAT (IOFF+IL1:IOFF+IL2,:)
      FDLCROT(IL1:IL2,:)=FDLCPAT(IOFF+IL1:IOFF+IL2,:)
      FLGROT (IL1:IL2,:)=FLGPAT (IOFF+IL1:IOFF+IL2,:)
      FSDROT (IL1:IL2,:)=FSDPAT (IOFF+IL1:IOFF+IL2,:)
      FSFROT (IL1:IL2,:)=FSFPAT (IOFF+IL1:IOFF+IL2,:)
      FSGROT (IL1:IL2,:)=FSGPAT (IOFF+IL1:IOFF+IL2,:)
      FSIROT (IL1:IL2,:)=FSIPAT (IOFF+IL1:IOFF+IL2,:)
      FSVROT (IL1:IL2,:)=FSVPAT (IOFF+IL1:IOFF+IL2,:)
      PARROT (IL1:IL2,:)=PARPAT (IOFF+IL1:IOFF+IL2,:)
C
      DO L = 1, NBS
      DO M = 1, IM
      DO I = IL1, IL2
        FSDBROT (I,M,L)  = FSDBPAT (IOFF+I,M,L)
        FSFBROT (I,M,L)  = FSFBPAT (IOFF+I,M,L)
        CSDBROT (I,M,L)  = CSDBPAT (IOFF+I,M,L)
        CSFBROT (I,M,L)  = CSFBPAT (IOFF+I,M,L)
        FSSBROT (I,M,L)  = FSSBPAT (IOFF+I,M,L)
        FSSCBROT(I,M,L)  = FSSCBPAT(IOFF+I,M,L)
      END DO
      END DO
      END DO
C
C
C     * LAND SURFACE ARRAYS.
C
      ALICROT(IL1:IL2,:,:)=ALICPAT(IOFF+IL1:IOFF+IL2,:,:)
      ALVCROT(IL1:IL2,:,:)=ALVCPAT(IOFF+IL1:IOFF+IL2,:,:)
      FCANROT(IL1:IL2,:,:)=FCANPAT(IOFF+IL1:IOFF+IL2,:,:)
      LNZ0ROT(IL1:IL2,:,:)=LNZ0PAT(IOFF+IL1:IOFF+IL2,:,:)
      CMASROT(IL1:IL2,:,:)=CMASPAT(IOFF+IL1:IOFF+IL2,:,:)
      LAMNROT(IL1:IL2,:,:)=LAMNPAT(IOFF+IL1:IOFF+IL2,:,:)
      LAMXROT(IL1:IL2,:,:)=LAMXPAT(IOFF+IL1:IOFF+IL2,:,:)
      ROOTROT(IL1:IL2,:,:)=ROOTPAT(IOFF+IL1:IOFF+IL2,:,:)

      GFLXROW(IL1:IL2,:)  =GFLXPAK (IOFF+IL1:IOFF+IL2,:)
C
      DO L=1,IGND
        DO I=IL1,IL2
          HFCGROW(I,L) = HFCGPAK(IOFF+I,L)
          HMFGROW(I,L) = HMFGPAK(IOFF+I,L)
          QFVGROW(I,L) = QFVGPAK(IOFF+I,L)
          TGROW  (I,L) = TGPAK  (IOFF+I,L)
          WGFROW (I,L) = WGFPAK (IOFF+I,L)
          WGLROW (I,L) = WGLPAK (IOFF+I,L)
        ENDDO
      ENDDO
      SANDROT(IL1:IL2,:,:)=SANDPAT(IOFF+IL1:IOFF+IL2,:,:)
      CLAYROT(IL1:IL2,:,:)=CLAYPAT(IOFF+IL1:IOFF+IL2,:,:)
      ORGMROT(IL1:IL2,:,:)=ORGMPAT(IOFF+IL1:IOFF+IL2,:,:)
      TGROT  (IL1:IL2,:,:)=TGPAT  (IOFF+IL1:IOFF+IL2,:,:)
      THLQROT(IL1:IL2,:,:)=THLQPAT(IOFF+IL1:IOFF+IL2,:,:)
      THICROT(IL1:IL2,:,:)=THICPAT(IOFF+IL1:IOFF+IL2,:,:)
      IF(KOUNT.GT.KSTART) THEN
        DLZWROT(IL1:IL2,:,:)=DLZWPAT(IOFF+IL1:IOFF+IL2,:,:)
        PORGROT(IL1:IL2,:,:)=PORGPAT(IOFF+IL1:IOFF+IL2,:,:)
        THFCROT(IL1:IL2,:,:)=THFCPAT(IOFF+IL1:IOFF+IL2,:,:)
        THLWROT(IL1:IL2,:,:)=THLWPAT(IOFF+IL1:IOFF+IL2,:,:)
        THRROT (IL1:IL2,:,:)=THRPAT (IOFF+IL1:IOFF+IL2,:,:)
        THMROT (IL1:IL2,:,:)=THMPAT (IOFF+IL1:IOFF+IL2,:,:)
        BIROT  (IL1:IL2,:,:)=BIPAT (IOFF+IL1:IOFF+IL2,:,:)
        PSISROT(IL1:IL2,:,:)=PSISPAT(IOFF+IL1:IOFF+IL2,:,:)
        GRKSROT(IL1:IL2,:,:)=GRKSPAT(IOFF+IL1:IOFF+IL2,:,:)
        THRAROT(IL1:IL2,:,:)=THRAPAT(IOFF+IL1:IOFF+IL2,:,:)
        HCPSROT(IL1:IL2,:,:)=HCPSPAT(IOFF+IL1:IOFF+IL2,:,:)
        TCSROT (IL1:IL2,:,:)=TCSPAT (IOFF+IL1:IOFF+IL2,:,:)
        PSIWROT(IL1:IL2,:,:)=PSIWPAT(IOFF+IL1:IOFF+IL2,:,:)
        ZBTWROT(IL1:IL2,:,:)=ZBTWPAT(IOFF+IL1:IOFF+IL2,:,:)
        ISNDROT(IL1:IL2,:,:)=ISNDPAT(IOFF+IL1:IOFF+IL2,:,:)
C
        ALGDVROT(IL1:IL2,:) =ALGDVPAT(IOFF+IL1:IOFF+IL2,:)
        ALGDNROT(IL1:IL2,:) =ALGDNPAT(IOFF+IL1:IOFF+IL2,:)
        ALGWVROT(IL1:IL2,:) =ALGWVPAT(IOFF+IL1:IOFF+IL2,:)
        ALGWNROT(IL1:IL2,:) =ALGWNPAT(IOFF+IL1:IOFF+IL2,:)
        IGDRROT(IL1:IL2,:)  =IGDRPAT(IOFF+IL1:IOFF+IL2,:)
      ENDIF
C
C     * TKE fields.
C
      DO L=1,ILEV
      DO I=IL1,IL2
        TKEMROW (I,L) = TKEMPAK (IOFF+I,L)
        XLMROW  (I,L) = XLMPAK  (IOFF+I,L)
        SVARROW (I,L) = SVARPAK (IOFF+I,L)
      ENDDO
      ENDDO
C
      DO I=IL1,IL2
        ANROW  (I) = ANPAK  (IOFF+I)
        DRROW  (I) = DRPAK  (IOFF+I)
        FLGGROW(I) = FLGGPAK(IOFF+I)
        FLGNROW(I) = FLGNPAK(IOFF+I)
        FLGVROW(I) = FLGVPAK(IOFF+I)
        FNLAROW(I) = FNLAPAK(IOFF+I)
        FNROW  (I) = FNPAK  (IOFF+I)
        FSGGROW(I) = FSGGPAK(IOFF+I)
        FSGNROW(I) = FSGNPAK(IOFF+I)
        FSGVROW(I) = FSGVPAK(IOFF+I)
        FVGROW (I) = FVGPAK (IOFF+I)
        FVNROW (I) = FVNPAK (IOFF+I)
        GSNOROW(I) = GSNOPAK(IOFF+I)
        HFCNROW(I) = HFCNPAK(IOFF+I)
        HFCVROW(I) = HFCVPAK(IOFF+I)
        HFLGROW(I) = HFLGPAK(IOFF+I)
        HFLNROW(I) = HFLNPAK(IOFF+I)
        HFLVROW(I) = HFLVPAK(IOFF+I)
        HFSGROW(I) = HFSGPAK(IOFF+I)
        HFSNROW(I) = HFSNPAK(IOFF+I)
        HFSVROW(I) = HFSVPAK(IOFF+I)
        HMFNROW(I) = HMFNPAK(IOFF+I)
        HMFVROW(I) = HMFVPAK(IOFF+I)
        MVROW  (I) = MVPAK  (IOFF+I)
        PCPNROW(I) = PCPNPAK(IOFF+I)
        PIGROW (I) = PIGPAK (IOFF+I)
        PINROW (I) = PINPAK (IOFF+I)
        PIVFROW(I) = PIVFPAK(IOFF+I)
        PIVLROW(I) = PIVLPAK(IOFF+I)
        QFGROW (I) = QFGPAK (IOFF+I)
        QFNROW (I) = QFNPAK (IOFF+I)
        QFVFROW(I) = QFVFPAK(IOFF+I)
        QFVLROW(I) = QFVLPAK(IOFF+I)
        RHONROW(I) = RHONPAK(IOFF+I)
        ROFNROW(I) = ROFNPAK(IOFF+I)
        ROFOROL(I) = ROFOPAL(IOFF+I)
        ROFOROW(I) = ROFOPAK(IOFF+I)
        ROFROL (I) = ROFPAL (IOFF+I)
        ROFROW (I) = ROFPAK (IOFF+I)
        ROFVROW(I) = ROFVPAK(IOFF+I)
        ROVGROW(I) = ROVGPAK(IOFF+I)
        SKYGROW(I) = SKYGPAK(IOFF+I)
        SKYNROW(I) = SKYNPAK(IOFF+I)
        SMLTROW(I) = SMLTPAK(IOFF+I)
        TBASROW(I) = TBASPAK(IOFF+I)
        TNROW  (I) = TNPAK  (IOFF+I)
        TTROW  (I) = TTPAK  (IOFF+I)
        TVROW  (I) = TVPAK  (IOFF+I)
        WTRGROW(I) = WTRGPAK(IOFF+I)
        WTRNROW(I) = WTRNPAK(IOFF+I)
        WTRVROW(I) = WTRVPAK(IOFF+I)
        WVFROW (I) = WVFPAK (IOFF+I)
        WVLROW (I) = WVLPAK (IOFF+I)
        ZNROW  (I) = ZNPAK  (IOFF+I)
        GAROW  (I) = GAPAK  (IOFF+I)
        HBLROW (I) = HBLPAK (IOFF+I)
        ILMOROW(I) = ILMOPAK(IOFF+I)
        PETROW(I)  = PETPAK (IOFF+I)
        ROFBROW(I) = ROFBPAK(IOFF+I)
        ROFSROW(I) = ROFSPAK(IOFF+I)
        UEROW  (I) = UEPAK  (IOFF+I)
        WSNOROW(I) = WSNOPAK(IOFF+I)
        WTABROW(I) = WTABPAK(IOFF+I)
        ZPNDROW(I) = ZPNDPAK(IOFF+I)
        REFROW (I) = REFPAK (IOFF+I)
        BCSNROW(I) = BCSNPAK(IOFF+I)
        DEPBROW(I) = DEPBPAK(IOFF+I)
      ENDDO
C
      CLDT3HROW(IL1:IL2)=CLDT3HPAK(IOFF+IL1:IOFF+IL2)
      HFL3HROW (IL1:IL2)=HFL3HPAK (IOFF+IL1:IOFF+IL2)
      HFS3HROW (IL1:IL2)=HFS3HPAK (IOFF+IL1:IOFF+IL2)
      ROF3HROL (IL1:IL2)=ROF3HPAL (IOFF+IL1:IOFF+IL2)
      WGL3HROW (IL1:IL2)=WGL3HPAK (IOFF+IL1:IOFF+IL2)
      WGF3HROW (IL1:IL2)=WGF3HPAK (IOFF+IL1:IOFF+IL2)
      PCP3HROW (IL1:IL2)=PCP3HPAK (IOFF+IL1:IOFF+IL2)
      PCPC3HROW(IL1:IL2)=PCPC3HPAK(IOFF+IL1:IOFF+IL2)
      PCPL3HROW(IL1:IL2)=PCPL3HPAK(IOFF+IL1:IOFF+IL2)
      PCPS3HROW(IL1:IL2)=PCPS3HPAK(IOFF+IL1:IOFF+IL2)
      PCPN3HROW(IL1:IL2)=PCPN3HPAK(IOFF+IL1:IOFF+IL2)
      FDL3HROW (IL1:IL2)=FDL3HPAK (IOFF+IL1:IOFF+IL2)
      FDLC3HROW(IL1:IL2)=FDLC3HPAK(IOFF+IL1:IOFF+IL2)
      FLG3HROW (IL1:IL2)=FLG3HPAK (IOFF+IL1:IOFF+IL2)
      FSS3HROW (IL1:IL2)=FSS3HPAK (IOFF+IL1:IOFF+IL2)
      FSSC3HROW(IL1:IL2)=FSSC3HPAK(IOFF+IL1:IOFF+IL2)
      FSD3HROW (IL1:IL2)=FSD3HPAK (IOFF+IL1:IOFF+IL2)
      FSG3HROW (IL1:IL2)=FSG3HPAK (IOFF+IL1:IOFF+IL2)
      FSGC3HROW(IL1:IL2)=FSGC3HPAK(IOFF+IL1:IOFF+IL2)
      SQ3HROW  (IL1:IL2)=SQ3HPAK  (IOFF+IL1:IOFF+IL2)
      ST3HROW  (IL1:IL2)=ST3HPAK  (IOFF+IL1:IOFF+IL2)
      SU3HROW  (IL1:IL2)=SU3HPAK  (IOFF+IL1:IOFF+IL2)
      SV3HROW  (IL1:IL2)=SV3HPAK  (IOFF+IL1:IOFF+IL2)

      OLR3HROW(IL1:IL2)  = OLR3HPAK(IOFF+IL1:IOFF+IL2)
      FSR3HROW(IL1:IL2)  = FSR3HPAK(IOFF+IL1:IOFF+IL2)
      OLRC3HROW(IL1:IL2) = OLRC3HPAK(IOFF+IL1:IOFF+IL2)
      FSRC3HROW(IL1:IL2) = FSRC3HPAK(IOFF+IL1:IOFF+IL2)
      FSO3HROW(IL1:IL2)  = FSO3HPAK(IOFF+IL1:IOFF+IL2)
      PWAT3HROW(IL1:IL2) = PWAT3HPAK(IOFF+IL1:IOFF+IL2)
      CLWT3HROW(IL1:IL2) = CLWT3HPAK(IOFF+IL1:IOFF+IL2)
      CICT3HROW(IL1:IL2) = CICT3HPAK(IOFF+IL1:IOFF+IL2)
      PMSL3HROW(IL1:IL2) = PMSL3HPAK(IOFF+IL1:IOFF+IL2)
C
C  * INSTANTANEOUS VARIABLES FOR SAMPLING
C
      DO I=IL1,IL2
         SWROL(I)    = SWPAL(IOFF+I)
         SRHROL(I)     = SRHPAL(IOFF+I)
         PCPROL(I)     = PCPPAL(IOFF+I)
         PCPNROL(I)    = PCPNPAL(IOFF+I)
         PCPCROL(I)    = PCPCPAL(IOFF+I)
         QFSIROL(I)    = QFSIPAL(IOFF+I)
         QFNROL(I)     = QFNPAL(IOFF+I)
         QFVFROL(I)    = QFVFPAL(IOFF+I)
         UFSINSTROL(I) = UFSINSTPAL(IOFF+I)
         VFSINSTROL(I) = VFSINSTPAL(IOFF+I)
         HFLIROL(I)    = HFLIPAL(IOFF+I)
         HFSIROL(I)    = HFSIPAL(IOFF+I)
         FDLROL(I)     = FDLPAL(IOFF+I)
         FLGROL(I)     = FLGPAL(IOFF+I)
         FSSROL(I)     = FSSPAL(IOFF+I)
         FSGROL(I)     = FSGPAL(IOFF+I)
         FSSCROL(I)    = FSSCPAL(IOFF+I)
         FSGCROL(I)    = FSGCPAL(IOFF+I)
         FDLCROL(I)    = FDLCPAL(IOFF+I)
         FSOROL(I)     = FSOPAL(IOFF+I)
         FSRROL(I)     = FSRPAL(IOFF+I)
         OLRROL(I)     = OLRPAL(IOFF+I)
         OLRCROL(I)    = OLRCPAL(IOFF+I)
         FSRCROL(I)    = FSRCPAL(IOFF+I)
         PWATIROL(I)   = PWATIPAL(IOFF+I)
         CLDTROL(I)    = CLDTPAL(IOFF+I)
         CLWTROL(I)    = CLWTPAL(IOFF+I)
         CICTROL(I)    = CICTPAL(IOFF+I)
         BALTROL(I)    = BALTPAL(IOFF+I)
         PMSLIROL(I)   = PMSLIPAL(IOFF+I)
         CDCBROL(I)    = CDCBPAL(IOFF+I)
         CSCBROL(I)    = CSCBPAL(IOFF+I)
         GTROL(I)      = GTPAL(IOFF+I)
         TCDROL(I)     = TCDPAL(IOFF+I)
         BCDROL(I)     = BCDPAL(IOFF+I)
         PSROL(I)      = PSPAL(IOFF+I)
         STROL(I)      = STPAL(IOFF+I)
         SUROL(I)      = SUPAL(IOFF+I)
         SVROL(I)      = SVPAL(IOFF+I)
      END DO ! I

      DO L = 1, ILEV
         DO I = IL1, IL2
           DMCROL(I,L)  = DMCPAL(I+IOFF,L)
           TAROL(I,L)   = TAPAL(I+IOFF,L)
           UAROL(I,L)   = UAPAL(I+IOFF,L)
           VAROL(I,L)   = VAPAL(I+IOFF,L)
           QAROL(I,L)   = QAPAL(I+IOFF,L)
           RHROL(I,L)   = RHPAL(I+IOFF,L)
           ZGROL(I,L)   = ZGPAL(I+IOFF,L)
           RKHROL(I,L)  = RKHPAL(I+IOFF,L)
           RKMROL(I,L)  = RKMPAL(I+IOFF,L)
           RKQROL(I,L)  = RKQPAL(I+IOFF,L)

           TTPROL(I,L)  = TTPPAL(I+IOFF,L)
           TTPPROL(I,L) = TTPPPAL(I+IOFF,L)
           TTPVROL(I,L) = TTPVPAL(I+IOFF,L)
           TTPLROL(I,L) = TTPLPAL(I+IOFF,L)
           TTPSROL(I,L) = TTPSPAL(I+IOFF,L)
           TTLCROL(I,L) = TTLCPAL(I+IOFF,L)
           TTSCROL(I,L) = TTSCPAL(I+IOFF,L)
           TTPCROL(I,L) = TTPCPAL(I+IOFF,L)
           TTPMROL(I,L) = TTPMPAL(I+IOFF,L)

           QTPROL(I,L)  = QTPPAL(I+IOFF,L)
           QTPPROL(I,L) = QTPPPAL(I+IOFF,L)
           QTPVROL(I,L) = QTPVPAL(I+IOFF,L)
           QTPCROL(I,L) = QTPCPAL(I+IOFF,L)
           QTPMROL(I,L) = QTPMPAL(I+IOFF,L)
        END DO ! I
      END DO ! L

      ANROT(IL1:IL2,:)=ANPAT(IOFF+IL1:IOFF+IL2,:)
      DPTHROT(IL1:IL2,:)=DPTHPAT(IOFF+IL1:IOFF+IL2,:)
      DRNROT(IL1:IL2,:)=DRNPAT(IOFF+IL1:IOFF+IL2,:)
      FNROT(IL1:IL2,:)=FNPAT(IOFF+IL1:IOFF+IL2,:)
      MVROT(IL1:IL2,:)=MVPAT(IOFF+IL1:IOFF+IL2,:)
      QAVROT(IL1:IL2,:)=QAVPAT(IOFF+IL1:IOFF+IL2,:)
      RHONROT(IL1:IL2,:)=RHONPAT(IOFF+IL1:IOFF+IL2,:)
      SOCIROT(IL1:IL2,:)=SOCIPAT(IOFF+IL1:IOFF+IL2,:)
      TAVROT(IL1:IL2,:)=TAVPAT(IOFF+IL1:IOFF+IL2,:)
      TBASROT(IL1:IL2,:)=TBASPAT(IOFF+IL1:IOFF+IL2,:)
      TNROT(IL1:IL2,:)=TNPAT(IOFF+IL1:IOFF+IL2,:)
      TPNDROT(IL1:IL2,:)=TPNDPAT(IOFF+IL1:IOFF+IL2,:)
      TTROT(IL1:IL2,:)=TTPAT(IOFF+IL1:IOFF+IL2,:)
      TVROT(IL1:IL2,:)=TVPAT(IOFF+IL1:IOFF+IL2,:)
      WSNOROT(IL1:IL2,:)=WSNOPAT(IOFF+IL1:IOFF+IL2,:)
      WVFROT(IL1:IL2,:)=WVFPAT(IOFF+IL1:IOFF+IL2,:)
      WVLROT(IL1:IL2,:)=WVLPAT(IOFF+IL1:IOFF+IL2,:)
      ZPNDROT(IL1:IL2,:)=ZPNDPAT(IOFF+IL1:IOFF+IL2,:)
      REFROT(IL1:IL2,:)=REFPAT(IOFF+IL1:IOFF+IL2,:)
      BCSNROT(IL1:IL2,:)=BCSNPAT(IOFF+IL1:IOFF+IL2,:)
C
      FAREROT(IL1:IL2,:)=FAREPAT(IOFF+IL1:IOFF+IL2,:)
      TSFSROT(IL1:IL2,:,:)=TSFSPAT(IOFF+IL1:IOFF+IL2,:,:)
#if defined (agcm_ctem)

      RMATCROT  (IL1:IL2,:,:,:) = RMATCPAT  (IOFF+IL1:IOFF+IL2,:,:,:)
      RTCTMROT  (IL1:IL2,:,:,:) = RTCTMPAT  (IOFF+IL1:IOFF+IL2,:,:,:)

      CFCANROT  (IL1:IL2,:,:) = CFCANPAT  (IOFF+IL1:IOFF+IL2,:,:)
      ZOLNCROT  (IL1:IL2,:,:) = ZOLNCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      AILCROT   (IL1:IL2,:,:) = AILCPAT   (IOFF+IL1:IOFF+IL2,:,:)
      CMASCROT  (IL1:IL2,:,:) = CMASCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      CALVCROT  (IL1:IL2,:,:) = CALVCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      CALICROT  (IL1:IL2,:,:) = CALICPAT  (IOFF+IL1:IOFF+IL2,:,:)
      PAICROT   (IL1:IL2,:,:) = PAICPAT   (IOFF+IL1:IOFF+IL2,:,:)
      SLAICROT  (IL1:IL2,:,:) = SLAICPAT  (IOFF+IL1:IOFF+IL2,:,:)
      FCANCROT  (IL1:IL2,:,:) = FCANCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      TODFCROT  (IL1:IL2,:,:) = TODFCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      AILCGROT  (IL1:IL2,:,:) = AILCGPAT  (IOFF+IL1:IOFF+IL2,:,:)
      SLAIROT   (IL1:IL2,:,:) = SLAIPAT   (IOFF+IL1:IOFF+IL2,:,:)

      FSNOWRTL  (IL1:IL2,:)     = FSNOWPTL  (IOFF+IL1:IOFF+IL2,:)
      TCANORTL  (IL1:IL2,:)     = TCANOPTL  (IOFF+IL1:IOFF+IL2,:)
      TCANSRTL  (IL1:IL2,:)     = TCANSPTL  (IOFF+IL1:IOFF+IL2,:)
      TARTL     (IL1:IL2,:)     = TAPTL     (IOFF+IL1:IOFF+IL2,:)
      CFLUXCSROT(IL1:IL2,:)     = CFLUXCSPAT(IOFF+IL1:IOFF+IL2,:)
      CFLUXCGROT(IL1:IL2,:)     = CFLUXCGPAT(IOFF+IL1:IOFF+IL2,:)
      CO2CG1ROT (IL1:IL2,:,:)   = CO2CG1PAT (IOFF+IL1:IOFF+IL2,:,:)
      CO2CG2ROT (IL1:IL2,:,:)   = CO2CG2PAT (IOFF+IL1:IOFF+IL2,:,:)
      CO2CS1ROT (IL1:IL2,:,:)   = CO2CS1PAT (IOFF+IL1:IOFF+IL2,:,:)
      CO2CS2ROT (IL1:IL2,:,:)   = CO2CS2PAT (IOFF+IL1:IOFF+IL2,:,:)
      ANCGRTL   (IL1:IL2,:,:)   = ANCGPTL   (IOFF+IL1:IOFF+IL2,:,:)
      ANCSRTL   (IL1:IL2,:,:)   = ANCSPTL   (IOFF+IL1:IOFF+IL2,:,:)
      RMLCGRTL  (IL1:IL2,:,:)   = RMLCGPTL  (IOFF+IL1:IOFF+IL2,:,:)
      RMLCSRTL  (IL1:IL2,:,:)   = RMLCSPTL  (IOFF+IL1:IOFF+IL2,:,:)

      TBARRTL   (IL1:IL2,:,:)   = TBARPTL   (IOFF+IL1:IOFF+IL2,:,:)
      TBARCRTL  (IL1:IL2,:,:)   = TBARCPTL  (IOFF+IL1:IOFF+IL2,:,:)
      TBARCSRTL (IL1:IL2,:,:)   = TBARCSPTL (IOFF+IL1:IOFF+IL2,:,:)
      TBARGRTL  (IL1:IL2,:,:)   = TBARGPTL  (IOFF+IL1:IOFF+IL2,:,:)
      TBARGSRTL (IL1:IL2,:,:)   = TBARGSPTL (IOFF+IL1:IOFF+IL2,:,:)
      THLIQCRTL (IL1:IL2,:,:)   = THLIQCPTL (IOFF+IL1:IOFF+IL2,:,:)
      THLIQGRTL (IL1:IL2,:,:)   = THLIQGPTL (IOFF+IL1:IOFF+IL2,:,:)
      THICECRTL (IL1:IL2,:,:)   = THICECPTL (IOFF+IL1:IOFF+IL2,:,:)
C
C     * INVARIANT.
C
      WETFROW   (IL1:IL2)       = WETFPAK   (IOFF+IL1:IOFF+IL2)
      WETSROW   (IL1:IL2)       = WETSPAK   (IOFF+IL1:IOFF+IL2)
      LGHTROW   (IL1:IL2)       = LGHTPAK   (IOFF+IL1:IOFF+IL2)
C
C     * TIME VARYING.
C
      SOILCROT  (IL1:IL2,:,:)   = SOILCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      LITRCROT  (IL1:IL2,:,:)   = LITRCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      ROOTCROT  (IL1:IL2,:,:)   = ROOTCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      STEMCROT  (IL1:IL2,:,:)   = STEMCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      GLEAFCROT (IL1:IL2,:,:)   = GLEAFCPAT (IOFF+IL1:IOFF+IL2,:,:)
      BLEAFCROT (IL1:IL2,:,:)   = BLEAFCPAT (IOFF+IL1:IOFF+IL2,:,:)
      FALLHROT  (IL1:IL2,:,:)   = FALLHPAT  (IOFF+IL1:IOFF+IL2,:,:)
      POSPHROT  (IL1:IL2,:,:)   = POSPHPAT  (IOFF+IL1:IOFF+IL2,:,:)
      LEAFSROT  (IL1:IL2,:,:)   = LEAFSPAT  (IOFF+IL1:IOFF+IL2,:,:)
      GROWTROT  (IL1:IL2,:,:)   = GROWTPAT  (IOFF+IL1:IOFF+IL2,:,:)
      LASTRROT  (IL1:IL2,:,:)   = LASTRPAT  (IOFF+IL1:IOFF+IL2,:,:)
      LASTSROT  (IL1:IL2,:,:)   = LASTSPAT  (IOFF+IL1:IOFF+IL2,:,:)
      THISYLROT (IL1:IL2,:,:)   = THISYLPAT (IOFF+IL1:IOFF+IL2,:,:)
      STEMHROT  (IL1:IL2,:,:)   = STEMHPAT  (IOFF+IL1:IOFF+IL2,:,:)
      ROOTHROT  (IL1:IL2,:,:)   = ROOTHPAT  (IOFF+IL1:IOFF+IL2,:,:)
      TEMPCROT  (IL1:IL2,:,:)   = TEMPCPAT  (IOFF+IL1:IOFF+IL2,:,:)
      AILCBROT  (IL1:IL2,:,:)   = AILCBPAT  (IOFF+IL1:IOFF+IL2,:,:)
      BMASVROT  (IL1:IL2,:,:)   = BMASVPAT  (IOFF+IL1:IOFF+IL2,:,:)
      VEGHROT   (IL1:IL2,:,:)   = VEGHPAT   (IOFF+IL1:IOFF+IL2,:,:)
      ROOTDROT  (IL1:IL2,:,:)   = ROOTDPAT  (IOFF+IL1:IOFF+IL2,:,:)
C
      PREFROT   (IL1:IL2,:,:)   = PREFPAT   (IOFF+IL1:IOFF+IL2,:,:)
      NEWFROT   (IL1:IL2,:,:)   = NEWFPAT   (IOFF+IL1:IOFF+IL2,:,:)
C
      CVEGROT   (IL1:IL2,:)     = CVEGPAT   (IOFF+IL1:IOFF+IL2,:)
      CDEBROT   (IL1:IL2,:)     = CDEBPAT   (IOFF+IL1:IOFF+IL2,:)
      CHUMROT   (IL1:IL2,:)     = CHUMPAT   (IOFF+IL1:IOFF+IL2,:)
      FCOLROT   (IL1:IL2,:)     = FCOLPAT   (IOFF+IL1:IOFF+IL2,:)
C
C     * CTEM DIAGNOSTIC OUTPUT FIELDS.
C     * NOTE THAT SAMPLED FIELDS DON'T HAVE TO BE
C     * UNPACKED SINCE THEY ARE ZEROED OUT IN
C     * SFCPROC2 PRIOR TO ACCUMULATION OVER TILES!
C
      CFNPROW(IL1:IL2) = CFNPPAK(IOFF+IL1:IOFF+IL2)
      CFNEROW(IL1:IL2) = CFNEPAK(IOFF+IL1:IOFF+IL2)
      CFRVROW(IL1:IL2) = CFRVPAK(IOFF+IL1:IOFF+IL2)
      CFGPROW(IL1:IL2) = CFGPPAK(IOFF+IL1:IOFF+IL2)
      CFNBROW(IL1:IL2) = CFNBPAK(IOFF+IL1:IOFF+IL2)
      CFFVROW(IL1:IL2) = CFFVPAK(IOFF+IL1:IOFF+IL2)
      CFFDROW(IL1:IL2) = CFFDPAK(IOFF+IL1:IOFF+IL2)
      CFLVROW(IL1:IL2) = CFLVPAK(IOFF+IL1:IOFF+IL2)
      CFLDROW(IL1:IL2) = CFLDPAK(IOFF+IL1:IOFF+IL2)
      CFLHROW(IL1:IL2) = CFLHPAK(IOFF+IL1:IOFF+IL2)
      CBRNROW(IL1:IL2) = CBRNPAK(IOFF+IL1:IOFF+IL2)
      CFRHROW(IL1:IL2) = CFRHPAK(IOFF+IL1:IOFF+IL2)
      CFHTROW(IL1:IL2) = CFHTPAK(IOFF+IL1:IOFF+IL2)
      CFLFROW(IL1:IL2) = CFLFPAK(IOFF+IL1:IOFF+IL2)
      CFRDROW(IL1:IL2) = CFRDPAK(IOFF+IL1:IOFF+IL2)
      CFRGROW(IL1:IL2) = CFRGPAK(IOFF+IL1:IOFF+IL2)
      CFRMROW(IL1:IL2) = CFRMPAK(IOFF+IL1:IOFF+IL2)
      CFNLROW(IL1:IL2) = CFNLPAK(IOFF+IL1:IOFF+IL2)
      CFNSROW(IL1:IL2) = CFNSPAK(IOFF+IL1:IOFF+IL2)
      CFNRROW(IL1:IL2) = CFNRPAK(IOFF+IL1:IOFF+IL2)
      CH4HROW(IL1:IL2) = CH4HPAK(IOFF+IL1:IOFF+IL2)
      CH4NROW(IL1:IL2) = CH4NPAK(IOFF+IL1:IOFF+IL2)
      WFRAROW(IL1:IL2) = WFRAPAK(IOFF+IL1:IOFF+IL2)
      CW1DROW(IL1:IL2) = CW1DPAK(IOFF+IL1:IOFF+IL2)
      CW2DROW(IL1:IL2) = CW2DPAK(IOFF+IL1:IOFF+IL2)
      FCOLROW(IL1:IL2) = FCOLPAK(IOFF+IL1:IOFF+IL2)

C
C Instantaneous output (used for subdaily output)
C
      CFGPROL(IL1:IL2) = CFGPPAL(IOFF+IL1:IOFF+IL2)
      CFRVROL(IL1:IL2) = CFRVPAL(IOFF+IL1:IOFF+IL2)
      CFRHROL(IL1:IL2) = CFRHPAL(IOFF+IL1:IOFF+IL2)
      CFRDROL(IL1:IL2) = CFRDPAL(IOFF+IL1:IOFF+IL2)

#endif
#if defined explvol
C
C     * EXPLOSIVE VOLCANO FIELDS.
C
      VTAUROW(IL1:IL2) = VTAUPAK(IOFF+IL1:IOFF+IL2)
      VTAUROL(IL1:IL2) = VTAUPAL(IOFF+IL1:IOFF+IL2)
      TROPROW(IL1:IL2) = TROPPAK(IOFF+IL1:IOFF+IL2)
      TROPROL(IL1:IL2) = TROPPAL(IOFF+IL1:IOFF+IL2)

      DO IB = 1, NBS
         DO L = 1, LEVSA
            DO I = IL1Z, IL2Z
               SW_EXT_SA_ROW(I,L,IB) = SW_EXT_SA_PAK(IOFFZ+I,L,IB)
               SW_EXT_SA_ROL(I,L,IB) = SW_EXT_SA_PAL(IOFFZ+I,L,IB)
               SW_SSA_SA_ROW(I,L,IB) = SW_SSA_SA_PAK(IOFFZ+I,L,IB)
               SW_SSA_SA_ROL(I,L,IB) = SW_SSA_SA_PAL(IOFFZ+I,L,IB)
               SW_G_SA_ROW(I,L,IB)   = SW_G_SA_PAK(IOFFZ+I,L,IB)
               SW_G_SA_ROL(I,L,IB)   = SW_G_SA_PAL(IOFFZ+I,L,IB)
            END DO ! I
         END DO ! L
      END DO ! IB

      DO IB = 1, NBL
         DO L = 1, LEVSA
            DO I = IL1Z, IL2Z
               LW_EXT_SA_ROW(I,L,IB) = LW_EXT_SA_PAK(IOFFZ+I,L,IB)
               LW_EXT_SA_ROL(I,L,IB) = LW_EXT_SA_PAL(IOFFZ+I,L,IB)
               LW_SSA_SA_ROW(I,L,IB) = LW_SSA_SA_PAK(IOFFZ+I,L,IB)
               LW_SSA_SA_ROL(I,L,IB) = LW_SSA_SA_PAL(IOFFZ+I,L,IB)
               LW_G_SA_ROW(I,L,IB)   = LW_G_SA_PAK(IOFFZ+I,L,IB)
               LW_G_SA_ROL(I,L,IB)   = LW_G_SA_PAL(IOFFZ+I,L,IB)
            END DO ! I
         END DO ! L
      END DO ! IB

      DO L = 1, LEVSA
         DO I = IL1Z, IL2Z
            W055_EXT_SA_ROW(I,L) = W055_EXT_SA_PAK(IOFFZ+I,L)
            W055_EXT_SA_ROL(I,L) = W055_EXT_SA_PAL(IOFFZ+I,L)
            W110_EXT_SA_ROW(I,L) = W110_EXT_SA_PAK(IOFFZ+I,L)
            W110_EXT_SA_ROL(I,L) = W110_EXT_SA_PAL(IOFFZ+I,L)
            PRESSURE_SA_ROW(I,L) = PRESSURE_SA_PAK(IOFFZ+I,L)
            PRESSURE_SA_ROL(I,L) = PRESSURE_SA_PAL(IOFFZ+I,L)
         END DO ! I
      END DO ! L

      DO I = IL1, IL2
         W055_VTAU_SA_ROW(I) = W055_VTAU_SA_PAK(IOFF+I)
         W055_VTAU_SA_ROL(I) = W055_VTAU_SA_PAL(IOFF+I)
         W110_VTAU_SA_ROW(I) = W110_VTAU_SA_PAK(IOFF+I)
         W110_VTAU_SA_ROL(I) = W110_VTAU_SA_PAL(IOFF+I)
      END DO ! I

      DO L = 1, ILEV
         DO I = IL1, IL2
            W055_EXT_GCM_SA_ROW(I,L) = W055_EXT_GCM_SA_PAK(IOFF+I,L)
            W055_EXT_GCM_SA_ROL(I,L) = W055_EXT_GCM_SA_PAL(IOFF+I,L)
            W110_EXT_GCM_SA_ROW(I,L) = W110_EXT_GCM_SA_PAK(IOFF+I,L)
            W110_EXT_GCM_SA_ROL(I,L) = W110_EXT_GCM_SA_PAL(IOFF+I,L)
         END DO ! I
      END DO ! L

#endif
C
C     * LAKES FIELDS.
C
      DO I=IL1,IL2
        BLAKROW(I) = BLAKPAK(IOFF+I)
        HLAKROW(I) = HLAKPAK(IOFF+I)
        LDMXROW(I) = LDMXPAK(IOFF+I)
        LLAKROW(I) = LLAKPAK(IOFF+I)
        LRIMROW(I) = LRIMPAK(IOFF+I)
        LRINROW(I) = LRINPAK(IOFF+I)
        LUIMROW(I) = LUIMPAK(IOFF+I)
        LUINROW(I) = LUINPAK(IOFF+I)
        LZICROW(I) = LZICPAK(IOFF+I)
#if defined (cslm)
        DELUROW(I) = DELUPAK(IOFF+I)
        DTMPROW(I) = DTMPPAK(IOFF+I)
        EXPWROW(I) = EXPWPAK(IOFF+I)
        GREDROW(I) = GREDPAK(IOFF+I)
        NLKLROW(I) = NLKLPAK(IOFF+I)
        RHOMROW(I) = RHOMPAK(IOFF+I)
        T0LKROW(I) = T0LKPAK(IOFF+I)
        TKELROW(I) = TKELPAK(IOFF+I)
        FLGLROW(I) = FLGLPAK(IOFF+I)
        FNLROW (I) = FNLPAK (IOFF+I)
        FSGLROW(I) = FSGLPAK(IOFF+I)
        HFCLROW(I) = HFCLPAK(IOFF+I)
        HFLLROW(I) = HFLLPAK(IOFF+I)
        HFSLROW(I) = HFSLPAK(IOFF+I)
        HMFLROW(I) = HMFLPAK(IOFF+I)
        PILROW (I) = PILPAK (IOFF+I)
        QFLROW (I) = QFLPAK (IOFF+I)
C
        DO L=1,NLKLM
          TLAKROW(I,L) = TLAKPAK(IOFF+I,L)
        ENDDO
#endif
#if defined (flake)
        LSHPROW(I) = LSHPPAK(IOFF+I)
        LTAVROW(I) = LTAVPAK(IOFF+I)
        LTICROW(I) = LTICPAK(IOFF+I)
        LTMXROW(I) = LTMXPAK(IOFF+I)
        LTSNROW(I) = LTSNPAK(IOFF+I)
        LTWBROW(I) = LTWBPAK(IOFF+I)
        LZSNROW(I) = LZSNPAK(IOFF+I)
#endif
      ENDDO
C
C     * EMISSION FIELDS.
C
      DO I=IL1,IL2
        EOSTROW(I) = EOSTPAK(IOFF+I)
        EOSTROL(I) = EOSTPAL(IOFF+I)
C
        ESCVROW(I) = ESCVPAK(IOFF+I)
        EHCVROW(I) = EHCVPAK(IOFF+I)
        ESEVROW(I) = ESEVPAK(IOFF+I)
        EHEVROW(I) = EHEVPAK(IOFF+I)
      ENDDO
#if defined transient_aerosol_emissions
      DO L=1,LEVWF
      DO I=IL1,IL2
        FBBCROW(I,L) = FBBCPAK(IOFF+I,L)
        FBBCROL(I,L) = FBBCPAL(IOFF+I,L)
      ENDDO
      ENDDO
      DO L=1,LEVAIR
      DO I=IL1,IL2
        FAIRROW(I,L) = FAIRPAK(IOFF+I,L)
        FAIRROL(I,L) = FAIRPAL(IOFF+I,L)
      ENDDO
      ENDDO
#if defined emists
      DO I=IL1,IL2
        SAIRROW(I) = SAIRPAK(IOFF+I)
        SSFCROW(I) = SSFCPAK(IOFF+I)
        SBIOROW(I) = SBIOPAK(IOFF+I)
        SSHIROW(I) = SSHIPAK(IOFF+I)
        SSTKROW(I) = SSTKPAK(IOFF+I)
        SFIRROW(I) = SFIRPAK(IOFF+I)
        SAIRROL(I) = SAIRPAL(IOFF+I)
        SSFCROL(I) = SSFCPAL(IOFF+I)
        SBIOROL(I) = SBIOPAL(IOFF+I)
        SSHIROL(I) = SSHIPAL(IOFF+I)
        SSTKROL(I) = SSTKPAL(IOFF+I)
        SFIRROL(I) = SFIRPAL(IOFF+I)
        OAIRROW(I) = OAIRPAK(IOFF+I)
        OSFCROW(I) = OSFCPAK(IOFF+I)
        OBIOROW(I) = OBIOPAK(IOFF+I)
        OSHIROW(I) = OSHIPAK(IOFF+I)
        OSTKROW(I) = OSTKPAK(IOFF+I)
        OFIRROW(I) = OFIRPAK(IOFF+I)
        OAIRROL(I) = OAIRPAL(IOFF+I)
        OSFCROL(I) = OSFCPAL(IOFF+I)
        OBIOROL(I) = OBIOPAL(IOFF+I)
        OSHIROL(I) = OSHIPAL(IOFF+I)
        OSTKROL(I) = OSTKPAL(IOFF+I)
        OFIRROL(I) = OFIRPAL(IOFF+I)
        BAIRROW(I) = BAIRPAK(IOFF+I)
        BSFCROW(I) = BSFCPAK(IOFF+I)
        BBIOROW(I) = BBIOPAK(IOFF+I)
        BSHIROW(I) = BSHIPAK(IOFF+I)
        BSTKROW(I) = BSTKPAK(IOFF+I)
        BFIRROW(I) = BFIRPAK(IOFF+I)
        BAIRROL(I) = BAIRPAL(IOFF+I)
        BSFCROL(I) = BSFCPAL(IOFF+I)
        BBIOROL(I) = BBIOPAL(IOFF+I)
        BSHIROL(I) = BSHIPAL(IOFF+I)
        BSTKROL(I) = BSTKPAL(IOFF+I)
        BFIRROL(I) = BFIRPAL(IOFF+I)
      ENDDO
#endif
#endif
#if (defined(pla) && defined(pfrc))
      DO I=IL1,IL2
        BCDPFROW(I) = BCDPFPAK(IOFF+I)
        BCDPFROL(I) = BCDPFPAL(IOFF+I)
      ENDDO
      DO L=1,ILEV
      DO I=IL1,IL2
        AMLDFROW(I,L) = AMLDFPAK(IOFF+I,L)
        AMLDFROL(I,L) = AMLDFPAL(IOFF+I,L)
        REAMFROW(I,L) = REAMFPAK(IOFF+I,L)
        REAMFROL(I,L) = REAMFPAL(IOFF+I,L)
        VEAMFROW(I,L) = VEAMFPAK(IOFF+I,L)
        VEAMFROL(I,L) = VEAMFPAL(IOFF+I,L)
        FR1FROW (I,L) = FR1FPAK (IOFF+I,L)
        FR1FROL (I,L) = FR1FPAL (IOFF+I,L)
        FR2FROW (I,L) = FR2FPAK (IOFF+I,L)
        FR2FROL (I,L) = FR2FPAL (IOFF+I,L)
        SSLDFROW(I,L) = SSLDFPAK(IOFF+I,L)
        SSLDFROL(I,L) = SSLDFPAL(IOFF+I,L)
        RESSFROW(I,L) = RESSFPAK(IOFF+I,L)
        RESSFROL(I,L) = RESSFPAL(IOFF+I,L)
        VESSFROW(I,L) = VESSFPAK(IOFF+I,L)
        VESSFROL(I,L) = VESSFPAL(IOFF+I,L)
        DSLDFROW(I,L) = DSLDFPAK(IOFF+I,L)
        DSLDFROL(I,L) = DSLDFPAL(IOFF+I,L)
        REDSFROW(I,L) = REDSFPAK(IOFF+I,L)
        REDSFROL(I,L) = REDSFPAL(IOFF+I,L)
        VEDSFROW(I,L) = VEDSFPAK(IOFF+I,L)
        VEDSFROL(I,L) = VEDSFPAL(IOFF+I,L)
        BCLDFROW(I,L) = BCLDFPAK(IOFF+I,L)
        BCLDFROL(I,L) = BCLDFPAL(IOFF+I,L)
        REBCFROW(I,L) = REBCFPAK(IOFF+I,L)
        REBCFROL(I,L) = REBCFPAL(IOFF+I,L)
        VEBCFROW(I,L) = VEBCFPAK(IOFF+I,L)
        VEBCFROL(I,L) = VEBCFPAL(IOFF+I,L)
        OCLDFROW(I,L) = OCLDFPAK(IOFF+I,L)
        OCLDFROL(I,L) = OCLDFPAL(IOFF+I,L)
        REOCFROW(I,L) = REOCFPAK(IOFF+I,L)
        REOCFROL(I,L) = REOCFPAL(IOFF+I,L)
        VEOCFROW(I,L) = VEOCFPAK(IOFF+I,L)
        VEOCFROL(I,L) = VEOCFPAL(IOFF+I,L)
        ZCDNFROW(I,L) = ZCDNFPAK(IOFF+I,L)
        ZCDNFROL(I,L) = ZCDNFPAL(IOFF+I,L)
        BCICFROW(I,L) = BCICFPAK(IOFF+I,L)
        BCICFROL(I,L) = BCICFPAL(IOFF+I,L)
      ENDDO
      ENDDO
#endif
C
C     * TRACER ARRAYS.
C
      DO N=1,NTRAC
        DO I=IL1,IL2
          XFSROW (I,N) = XFSPAK (IOFF+I,N)
          XSFXROL(I,N) = XSFXPAL(IOFF+I,N)
          XSFXROW(I,N) = XSFXPAK(IOFF+I,N)
          XSRFROW(I,N) = XSRFPAK(IOFF+I,N)
          XSRFROL(I,N) = XSRFPAL(IOFF+I,N)
          XTVIROS(I,N) = XTVIPAS(IOFF+I,N)
          XTVIROW(I,N) = XTVIPAK(IOFF+I,N)
          XTVIROM(I,N) = XTVIPAM(IOFF+I,N)
          XTPFROM(I,N) = XTPFPAM(IOFF+I,N)
          XTPHROM(I,N) = XTPHPAM(IOFF+I,N)
          XTPTROM(I,N) = XTPTPAM(IOFF+I,N)
        ENDDO
        DO L=1,ILEV
        DO I=IL1,IL2
          XWF0ROL(I,L,N) = XWF0PAL(IOFF+I,L,N)
          XWFMROL(I,L,N) = XWFMPAL(IOFF+I,L,N)
        ENDDO
        ENDDO
      ENDDO
C
      DO L=1,LEVOX
        DO I=IL1,IL2
          H2O2ROL(I,L) = H2O2PAL(IOFF+I,L)
          H2O2ROW(I,L) = H2O2PAK(IOFF+I,L)
          HNO3ROL(I,L) = HNO3PAL(IOFF+I,L)
          HNO3ROW(I,L) = HNO3PAK(IOFF+I,L)
          NH3ROL (I,L) = NH3PAL (IOFF+I,L)
          NH3ROW (I,L) = NH3PAK (IOFF+I,L)
          NH4ROL (I,L) = NH4PAL (IOFF+I,L)
          NH4ROW (I,L) = NH4PAK (IOFF+I,L)
          NO3ROL (I,L) = NO3PAL (IOFF+I,L)
          NO3ROW (I,L) = NO3PAK (IOFF+I,L)
          O3ROL  (I,L) = O3PAL  (IOFF+I,L)
          O3ROW  (I,L) = O3PAK  (IOFF+I,L)
          OHROL  (I,L) = OHPAL  (IOFF+I,L)
          OHROW  (I,L) = OHPAK  (IOFF+I,L)
        ENDDO
      ENDDO
C
      DO L=1,ILEV
        DO I=IL1,IL2
          TBNDROL(I,L) = TBNDPAL(IOFF+I,L)
          TBNDROW(I,L) = TBNDPAK(IOFF+I,L)
          EA55ROL(I,L) = EA55PAL(IOFF+I,L)
          EA55ROW(I,L) = EA55PAK(IOFF+I,L)
          RKMROW (I,L) = RKMPAK (IOFF+I,L)
          RKHROW (I,L) = RKHPAK (IOFF+I,L)
          RKQROW (I,L) = RKQPAK (IOFF+I,L)
        ENDDO
      ENDDO
C
      DO L = 1, 4
         DO I=IL1,IL2
            ISEEDROW(I,L) = ISEEDPAK(IOFF+I,L)
         END DO ! I
      END DO ! L
C
      DO I=IL1,IL2
        SPOTROW(I) = SPOTPAK(IOFF+I)
        ST01ROW(I) = ST01PAK(IOFF+I)
        ST02ROW(I) = ST02PAK(IOFF+I)
        ST03ROW(I) = ST03PAK(IOFF+I)
        ST04ROW(I) = ST04PAK(IOFF+I)
        ST06ROW(I) = ST06PAK(IOFF+I)
        ST13ROW(I) = ST13PAK(IOFF+I)
        ST14ROW(I) = ST14PAK(IOFF+I)
        ST15ROW(I) = ST15PAK(IOFF+I)
        ST16ROW(I) = ST16PAK(IOFF+I)
        ST17ROW(I) = ST17PAK(IOFF+I)
        SUZ0ROW(I) = SUZ0PAK(IOFF+I)
        SUZ0ROL(I) = SUZ0PAL(IOFF+I)
        PDSFROW(I) = PDSFPAK(IOFF+I)
        PDSFROL(I) = PDSFPAL(IOFF+I)
      ENDDO
C
C     * CONDITIONAL ARRAYS.
C
#if defined rad_flux_profs
      DO L = 1, ILEV+2
         DO I = IL1, IL2
            FSAUROW(I,L) = FSAUPAK(IOFF+I,L)
            FSADROW(I,L) = FSADPAK(IOFF+I,L)
            FLAUROW(I,L) = FLAUPAK(IOFF+I,L)
            FLADROW(I,L) = FLADPAK(IOFF+I,L)
            FSCUROW(I,L) = FSCUPAK(IOFF+I,L)
            FSCDROW(I,L) = FSCDPAK(IOFF+I,L)
            FLCUROW(I,L) = FLCUPAK(IOFF+I,L)
            FLCDROW(I,L) = FLCDPAK(IOFF+I,L)
         END DO ! I
      END DO ! L
#endif
      DO L = 1, ILEV+2
        DO I = IL1, IL2
            FSAUROL(I,L) = FSAUPAL(IOFF+I,L)
            FSADROL(I,L) = FSADPAL(IOFF+I,L)
            FLAUROL(I,L) = FLAUPAL(IOFF+I,L)
            FLADROL(I,L) = FLADPAL(IOFF+I,L)
            FSCUROL(I,L) = FSCUPAL(IOFF+I,L)
            FSCDROL(I,L) = FSCDPAL(IOFF+I,L)
            FLCUROL(I,L) = FLCUPAL(IOFF+I,L)
            FLCDROL(I,L) = FLCDPAL(IOFF+I,L)
         END DO ! I
      END DO ! L

#if defined radforce

      !--- Upward and downward all-sky and clear-sky fluxes
      FSAUROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSAUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FSADROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSADPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLAUROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLAUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLADROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLADPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FSCUROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSCUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FSCDROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSCDPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLCUROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLCUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLCDROW_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLCDPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)

      FSAUROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSAUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FSADROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSADPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLAUROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLAUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLADROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLADPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FSCUROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSCUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FSCDROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FSCDPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLCUROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLCUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)
      FLCDROL_R(IL1:IL2,1:ILEV+2,1:NRFP) =
     &          FLCDPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP)

      !--- temperature perturbation (T_* - T_o) for each perturbation
      RDTROW_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          RDTPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)
      RDTROL_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          RDTPAL_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)
      RDTROM_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          RDTPAM_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)

      !--- heating rates for each perturbation
      HRSROW_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          HRSPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)
      HRLROW_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          HRLPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)

      HRSCROW_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          HRSCPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)
      HRLCROW_R (IL1:IL2,1:ILEV,1:NRFP) =
     &          HRLCPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP)

      !--- surface fluxes from secondary radiation calls
      CSBROL_R(IL1:IL2,1:NRFP)  = CSBPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSGROL_R(IL1:IL2,1:NRFP)  = FSGPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSSROL_R(IL1:IL2,1:NRFP)  = FSSPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSSCROL_R(IL1:IL2,1:NRFP) = FSSCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      CLBROL_R(IL1:IL2,1:NRFP)  = CLBPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FLGROL_R(IL1:IL2,1:NRFP)  = FLGPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FDLROL_R(IL1:IL2,1:NRFP)  = FDLPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FDLCROL_R(IL1:IL2,1:NRFP) = FDLCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSRROL_R(IL1:IL2,1:NRFP)  = FSRPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSRCROL_R(IL1:IL2,1:NRFP) = FSRCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSOROL_R(IL1:IL2,1:NRFP)  = FSOPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      OLRROL_R(IL1:IL2,1:NRFP)  = OLRPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      OLRCROL_R(IL1:IL2,1:NRFP) = OLRCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)
      FSLOROL_R(IL1:IL2,1:NRFP) = FSLOPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)

      DO M = 1, IM
       CSBROT_R(IL1:IL2,M,1:NRFP) = CSBPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP)
       CLBROT_R(IL1:IL2,M,1:NRFP) = CLBPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP)
       FSGROT_R(IL1:IL2,M,1:NRFP) = FSGPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP)
       FLGROT_R(IL1:IL2,M,1:NRFP) = FLGPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP)
      END DO ! M

      !--- Vertical level index of tropopause height
      KTHROL(IL1:IL2) = KTHPAL(IOFF+IL1:IOFF+IL2)
#endif
#if defined qconsav
      DO I=IL1,IL2
          QADDROW(I,1) = QADDPAK(IOFF+I,1)
          QADDROW(I,2) = QADDPAK(IOFF+I,2)
          QTPHROW(I,1) = QTPHPAK(IOFF+I,1)
          QTPHROW(I,2) = QTPHPAK(IOFF+I,2)
      ENDDO
#endif
C
      DO L=1,ILEV
        DO I=IL1,IL2
#if defined tprhs
          TTPROW (I,L) = TTPPAK (IOFF+I,L)
#endif
#if defined qprhs
          QTPROW (I,L) = QTPPAK (IOFF+I,L)
#endif
#if defined uprhs
          UTPROW (I,L) = UTPPAK (IOFF+I,L)
#endif
#if defined vrphs
          VTPROW (I,L) = VTPPAK (IOFF+I,L)
#endif
#if defined qconsav
          QTPFROW(I,L) = QTPFPAK(IOFF+I,L)
#endif
#if defined tprhsc
          TTPCROW(I,L) = TTPCPAK(IOFF+I,L)
          TTPKROW(I,L) = TTPKPAK(IOFF+I,L)
          TTPMROW(I,L) = TTPMPAK(IOFF+I,L)
          TTPNROW(I,L) = TTPNPAK(IOFF+I,L)
          TTPPROW(I,L) = TTPPPAK(IOFF+I,L)
          TTPVROW(I,L) = TTPVPAK(IOFF+I,L)
#endif
#if (defined (tprhsc) || defined (radforce))
          TTPLROW(I,L) = TTPLPAK(IOFF+I,L)
          TTPSROW(I,L) = TTPSPAK(IOFF+I,L)
          TTSCROW(I,L) = TTSCPAK(IOFF+I,L)
          TTLCROW(I,L) = TTLCPAK(IOFF+I,L)
#endif
#if defined qprhsc
          QTPCROW(I,L) = QTPCPAK(IOFF+I,L)
          QTPMROW(I,L) = QTPMPAK(IOFF+I,L)
          QTPPROW(I,L) = QTPPPAK(IOFF+I,L)
          QTPVROW(I,L) = QTPVPAK(IOFF+I,L)
#endif
#if defined uprhsc
          UTPCROW(I,L) = UTPCPAK(IOFF+I,L)
          UTPGROW(I,L) = UTPGPAK(IOFF+I,L)
          UTPNROW(I,L) = UTPNPAK(IOFF+I,L)
          UTPSROW(I,L) = UTPSPAK(IOFF+I,L)
          UTPVROW(I,L) = UTPVPAK(IOFF+I,L)
#endif
#if defined vrphsc
          VTPCROW(I,L) = VTPCPAK(IOFF+I,L)
          VTPGROW(I,L) = VTPGPAK(IOFF+I,L)
          VTPNROW(I,L) = VTPNPAK(IOFF+I,L)
          VTPSROW(I,L) = VTPSPAK(IOFF+I,L)
          VTPVROW(I,L) = VTPVPAK(IOFF+I,L)
#endif
        ENDDO
      ENDDO
C
      DO N=1,NTRAC
#if defined xconsav
        DO I=IL1,IL2
            XADDROW(I,1,N) = XADDPAK(IOFF+I,1,N)
            XADDROW(I,2,N) = XADDPAK(IOFF+I,2,N)
            XTPHROW(I,1,N) = XTPHPAK(IOFF+I,1,N)
            XTPHROW(I,2,N) = XTPHPAK(IOFF+I,2,N)
        ENDDO
#endif
        DO L=1,ILEV
          DO I=IL1,IL2
#if defined xprhs
            XTPROW (I,L,N) = XTPPAK (IOFF+I,L,N)
#endif
#if defined xconsav
            XTPFROW(I,L,N) = XTPFPAK(IOFF+I,L,N)
#endif
#if defined xprhsc
            XTPCROW(I,L,N) = XTPCPAK(IOFF+I,L,N)
            XTPMROW(I,L,N) = XTPMPAK(IOFF+I,L,N)
            XTPPROW(I,L,N) = XTPPPAK(IOFF+I,L,N)
            XTPVROW(I,L,N) = XTPVPAK(IOFF+I,L,N)
#endif
#if defined x01
            X01ROW (I,L,N) = X01PAK (IOFF+I,L,N)
#endif
#if defined x02
            X02ROW (I,L,N) = X02PAK (IOFF+I,L,N)
#endif
#if defined x03
            X03ROW (I,L,N) = X03PAK (IOFF+I,L,N)
#endif
#if defined x04
            X04ROW (I,L,N) = X04PAK (IOFF+I,L,N)
#endif
#if defined x05
            X05ROW (I,L,N) = X05PAK (IOFF+I,L,N)
#endif
#if defined x06
            X06ROW (I,L,N) = X06PAK (IOFF+I,L,N)
#endif
#if defined x07
            X07ROW (I,L,N) = X07PAK (IOFF+I,L,N)
#endif
#if defined x08
            X08ROW (I,L,N) = X08PAK (IOFF+I,L,N)
#endif
#if defined x09
            X09ROW (I,L,N) = X09PAK (IOFF+I,L,N)
#endif
          ENDDO
        ENDDO
      ENDDO
#if defined xtraconv
      DO I=IL1,IL2
        BCDROW (I) = BCDPAK (IOFF+I)
        BCSROW (I) = BCSPAK (IOFF+I)
        CAPEROW(I) = CAPEPAK(IOFF+I)
        CDCBROW(I) = CDCBPAK(IOFF+I)
        CINHROW(I) = CINHPAK(IOFF+I)
        CSCBROW(I) = CSCBPAK(IOFF+I)
        TCDROW (I) = TCDPAK (IOFF+I)
        TCSROW (I) = TCSPAK (IOFF+I)
      ENDDO
C
      DO L=1,ILEV
      DO I=IL1,IL2
          DMCROW (I,L) = DMCPAK (IOFF+I,L)
          SMCROW (I,L) = SMCPAK (IOFF+I,L)
          DMCDROW(I,L) = DMCDPAK(IOFF+I,L)
          DMCUROW(I,L) = DMCUPAK(IOFF+I,L)
      ENDDO
      ENDDO
#endif
#if defined xtrachem
      DO I=IL1,IL2
        DD4ROW (I) = DD4PAK (IOFF+I)
        DOX4ROW(I) = DOX4PAK(IOFF+I)
        DOXDROW(I) = DOXDPAK(IOFF+I)
        ESDROW (I) = ESDPAK (IOFF+I)
        ESFSROW(I) = ESFSPAK(IOFF+I)
        EAISROW(I) = EAISPAK(IOFF+I)
        ESTSROW(I) = ESTSPAK(IOFF+I)
        EFISROW(I) = EFISPAK(IOFF+I)
        ESFBROW(I) = ESFBPAK(IOFF+I)
        EAIBROW(I) = EAIBPAK(IOFF+I)
        ESTBROW(I) = ESTBPAK(IOFF+I)
        EFIBROW(I) = EFIBPAK(IOFF+I)
        ESFOROW(I) = ESFOPAK(IOFF+I)
        EAIOROW(I) = EAIOPAK(IOFF+I)
        ESTOROW(I) = ESTOPAK(IOFF+I)
        EFIOROW(I) = EFIOPAK(IOFF+I)
        EDSLROW(I) = EDSLPAK(IOFF+I)
        EDSOROW(I) = EDSOPAK(IOFF+I)
        ESVCROW(I) = ESVCPAK(IOFF+I)
        ESVEROW(I) = ESVEPAK(IOFF+I)
        NOXDROW(I) = NOXDPAK(IOFF+I)
        WDD4ROW(I) = WDD4PAK(IOFF+I)
        WDL4ROW(I) = WDL4PAK(IOFF+I)
        WDS4ROW(I) = WDS4PAK(IOFF+I)
      ENDDO
#ifndef pla
      DO I=IL1,IL2
        DDDROW (I) = DDDPAK (IOFF+I)
        DDBROW (I) = DDBPAK (IOFF+I)
        DDOROW (I) = DDOPAK (IOFF+I)
        DDSROW (I) = DDSPAK (IOFF+I)
        WDLDROW(I) = WDLDPAK(IOFF+I)
        WDLBROW(I) = WDLBPAK(IOFF+I)
        WDLOROW(I) = WDLOPAK(IOFF+I)
        WDLSROW(I) = WDLSPAK(IOFF+I)
        WDDDROW(I) = WDDDPAK(IOFF+I)
        WDDBROW(I) = WDDBPAK(IOFF+I)
        WDDOROW(I) = WDDOPAK(IOFF+I)
        WDDSROW(I) = WDDSPAK(IOFF+I)
        WDSDROW(I) = WDSDPAK(IOFF+I)
        WDSBROW(I) = WDSBPAK(IOFF+I)
        WDSOROW(I) = WDSOPAK(IOFF+I)
        WDSSROW(I) = WDSSPAK(IOFF+I)
        DD6ROW (I) = DD6PAK (IOFF+I)
        SDHPROW(I) = SDHPPAK(IOFF+I)
        SDO3ROW(I) = SDO3PAK(IOFF+I)
        SLHPROW(I) = SLHPPAK(IOFF+I)
        SLO3ROW(I) = SLO3PAK(IOFF+I)
        SSHPROW(I) = SSHPPAK(IOFF+I)
        SSO3ROW(I) = SSO3PAK(IOFF+I)
        WDD6ROW(I) = WDD6PAK(IOFF+I)
        WDL6ROW(I) = WDL6PAK(IOFF+I)
        WDS6ROW(I) = WDS6PAK(IOFF+I)
      ENDDO
C
      DO L=1,ILEV
        DO I=IL1,IL2
           PHDROW (I,L) = PHDPAK (IOFF+I,L)
           PHLROW (I,L) = PHLPAK (IOFF+I,L)
           PHSROW (I,L) = PHSPAK (IOFF+I,L)
        ENDDO
      ENDDO
#endif
#endif
#if defined (xtradust)
      DO I=IL1,IL2
        DUWDROW(I) = DUWDPAK(IOFF+I)
        DUSTROW(I) = DUSTPAK(IOFF+I)
        DUTHROW(I) = DUTHPAK(IOFF+I)
        USMKROW(I) = USMKPAK(IOFF+I)
        FALLROW(I) = FALLPAK(IOFF+I)
        FA10ROW(I) = FA10PAK(IOFF+I)
         FA2ROW(I) =  FA2PAK(IOFF+I)
         FA1ROW(I) =  FA1PAK(IOFF+I)
        GUSTROW(I) = GUSTPAK(IOFF+I)
        ZSPDROW(I) = ZSPDPAK(IOFF+I)
        VGFRROW(I) = VGFRPAK(IOFF+I)
        SMFRROW(I) = SMFRPAK(IOFF+I)
      ENDDO
#endif
#if (defined(pla) && defined(pam))
      DO L=1,ILEV
      DO I=IL1,IL2
        SSLDROW(I,L)   = SSLDPAK(IOFF+I,L)
        RESSROW(I,L)   = RESSPAK(IOFF+I,L)
        VESSROW(I,L)   = VESSPAK(IOFF+I,L)
        DSLDROW(I,L)   = DSLDPAK(IOFF+I,L)
        REDSROW(I,L)   = REDSPAK(IOFF+I,L)
        VEDSROW(I,L)   = VEDSPAK(IOFF+I,L)
        BCLDROW(I,L)   = BCLDPAK(IOFF+I,L)
        REBCROW(I,L)   = REBCPAK(IOFF+I,L)
        VEBCROW(I,L)   = VEBCPAK(IOFF+I,L)
        OCLDROW(I,L)   = OCLDPAK(IOFF+I,L)
        REOCROW(I,L)   = REOCPAK(IOFF+I,L)
        VEOCROW(I,L)   = VEOCPAK(IOFF+I,L)
        AMLDROW(I,L)   = AMLDPAK(IOFF+I,L)
        REAMROW(I,L)   = REAMPAK(IOFF+I,L)
        VEAMROW(I,L)   = VEAMPAK(IOFF+I,L)
        FR1ROW (I,L)   = FR1PAK (IOFF+I,L)
        FR2ROW (I,L)   = FR2PAK (IOFF+I,L)
        ZCDNROW(I,L)   = ZCDNPAK(IOFF+I,L)
        BCICROW(I,L)   = BCICPAK(IOFF+I,L)
        OEDNROW(I,L,:) = OEDNPAK(IOFF+I,L,:)
        OERCROW(I,L,:) = OERCPAK(IOFF+I,L,:)
        OIDNROW(I,L)   = OIDNPAK(IOFF+I,L)
        OIRCROW(I,L)   = OIRCPAK(IOFF+I,L)
        SVVBROW(I,L)   = SVVBPAK(IOFF+I,L)
        PSVVROW(I,L)   = PSVVPAK(IOFF+I,L)
        SVMBROW(I,L,:) = SVMBPAK(IOFF+I,L,:)
        SVCBROW(I,L,:) = SVCBPAK(IOFF+I,L,:)
        PNVBROW(I,L,:)  =PNVBPAK(IOFF+I,L,:)
        PNMBROW(I,L,:,:)=PNMBPAK(IOFF+I,L,:,:)
        PNCBROW(I,L,:,:)=PNCBPAK(IOFF+I,L,:,:)
        PSVBROW(I,L,:,:)  =PSVBPAK(IOFF+I,L,:,:)
        PSMBROW(I,L,:,:,:)=PSMBPAK(IOFF+I,L,:,:,:)
        PSCBROW(I,L,:,:,:)=PSCBPAK(IOFF+I,L,:,:,:)
        QNVBROW(I,L,:)  =QNVBPAK(IOFF+I,L,:)
        QNMBROW(I,L,:,:)=QNMBPAK(IOFF+I,L,:,:)
        QNCBROW(I,L,:,:)=QNCBPAK(IOFF+I,L,:,:)
        QSVBROW(I,L,:,:)  =QSVBPAK(IOFF+I,L,:,:)
        QSMBROW(I,L,:,:,:)=QSMBPAK(IOFF+I,L,:,:,:)
        QSCBROW(I,L,:,:,:)=QSCBPAK(IOFF+I,L,:,:,:)
        QGVBROW(I,L)   = QGVBPAK(IOFF+I,L)
        QGMBROW(I,L,:) = QGMBPAK(IOFF+I,L,:)
        QGCBROW(I,L,:) = QGCBPAK(IOFF+I,L,:)
        QDVBROW(I,L)   = QDVBPAK(IOFF+I,L)
        QDMBROW(I,L,:) = QDMBPAK(IOFF+I,L,:)
        QDCBROW(I,L,:) = QDCBPAK(IOFF+I,L,:)
        ONVBROW(I,L,:)  =ONVBPAK(IOFF+I,L,:)
        ONMBROW(I,L,:,:)=ONMBPAK(IOFF+I,L,:,:)
        ONCBROW(I,L,:,:)=ONCBPAK(IOFF+I,L,:,:)
        OSVBROW(I,L,:,:)  =OSVBPAK(IOFF+I,L,:,:)
        OSMBROW(I,L,:,:,:)=OSMBPAK(IOFF+I,L,:,:,:)
        OSCBROW(I,L,:,:,:)=OSCBPAK(IOFF+I,L,:,:,:)
        OGVBROW(I,L)   = OGVBPAK(IOFF+I,L)
        OGMBROW(I,L,:) = OGMBPAK(IOFF+I,L,:)
        OGCBROW(I,L,:) = OGCBPAK(IOFF+I,L,:)
        DEVBROW(I,L,:) = DEVBPAK(IOFF+I,L,:)
        PDEVROW(I,L,:) = PDEVPAK(IOFF+I,L,:)
        DEMBROW(I,L,:,:)=DEMBPAK(IOFF+I,L,:,:)
        DECBROW(I,L,:,:)=DECBPAK(IOFF+I,L,:,:)
        DIVBROW(I,L)   = DIVBPAK(IOFF+I,L)
        PDIVROW(I,L)   = PDIVPAK(IOFF+I,L)
        DIMBROW(I,L,:) = DIMBPAK(IOFF+I,L,:)
        DICBROW(I,L,:) = DICBPAK(IOFF+I,L,:)
        REVBROW(I,L,:) = REVBPAK(IOFF+I,L,:)
        PREVROW(I,L,:) = PREVPAK(IOFF+I,L,:)
        REMBROW(I,L,:,:)=REMBPAK(IOFF+I,L,:,:)
        RECBROW(I,L,:,:)=RECBPAK(IOFF+I,L,:,:)
        RIVBROW(I,L)   = RIVBPAK(IOFF+I,L)
        PRIVROW(I,L)   = PRIVPAK(IOFF+I,L)
        RIMBROW(I,L,:) = RIMBPAK(IOFF+I,L,:)
        RICBROW(I,L,:) = RICBPAK(IOFF+I,L,:)
        SULIROW(I,L)   = SULIPAK(IOFF+I,L)
        RSUIROW(I,L)   = RSUIPAK(IOFF+I,L)
        VSUIROW(I,L)   = VSUIPAK(IOFF+I,L)
        F1SUROW(I,L)   = F1SUPAK(IOFF+I,L)
        F2SUROW(I,L)   = F2SUPAK(IOFF+I,L)
        BCLIROW(I,L)   = BCLIPAK(IOFF+I,L)
        RBCIROW(I,L)   = RBCIPAK(IOFF+I,L)
        VBCIROW(I,L)   = VBCIPAK(IOFF+I,L)
        F1BCROW(I,L)   = F1BCPAK(IOFF+I,L)
        F2BCROW(I,L)   = F2BCPAK(IOFF+I,L)
        OCLIROW(I,L)   = OCLIPAK(IOFF+I,L)
        ROCIROW(I,L)   = ROCIPAK(IOFF+I,L)
        VOCIROW(I,L)   = VOCIPAK(IOFF+I,L)
        F1OCROW(I,L)   = F1OCPAK(IOFF+I,L)
        F2OCROW(I,L)   = F2OCPAK(IOFF+I,L)
      ENDDO
      ENDDO
#if defined (xtrapla1)
      DO L=1,ILEV
      DO I=IL1,IL2
        SNCNROW(I,L) = SNCNPAK(IOFF+I,L)
        SSUNROW(I,L) = SSUNPAK(IOFF+I,L)
        SCNDROW(I,L) = SCNDPAK(IOFF+I,L)
        SGSPROW(I,L) = SGSPPAK(IOFF+I,L)
        CCNROW (I,L) = CCNPAK (IOFF+I,L)
        CC02ROW(I,L) = CC02PAK(IOFF+I,L)
        CC04ROW(I,L) = CC04PAK(IOFF+I,L)
        CC08ROW(I,L) = CC08PAK(IOFF+I,L)
        CC16ROW(I,L) = CC16PAK(IOFF+I,L)
        CCNEROW(I,L) = CCNEPAK(IOFF+I,L)
        ACASROW(I,L) = ACASPAK(IOFF+I,L)
        ACOAROW(I,L) = ACOAPAK(IOFF+I,L)
        ACBCROW(I,L) = ACBCPAK(IOFF+I,L)
        ACSSROW(I,L) = ACSSPAK(IOFF+I,L)
        ACMDROW(I,L) = ACMDPAK(IOFF+I,L)
        NTROW  (I,L) = NTPAK(IOFF+I,L)
        N20ROW (I,L) = N20PAK(IOFF+I,L)
        N50ROW (I,L) = N50PAK(IOFF+I,L)
        N100ROW(I,L) = N100PAK(IOFF+I,L)
        N200ROW(I,L) = N200PAK(IOFF+I,L)
        WTROW  (I,L) = WTPAK(IOFF+I,L)
        W20ROW (I,L) = W20PAK(IOFF+I,L)
        W50ROW (I,L) = W50PAK(IOFF+I,L)
        W100ROW(I,L) = W100PAK(IOFF+I,L)
        W200ROW(I,L) = W200PAK(IOFF+I,L)
        RCRIROW(I,L) = RCRIPAK(IOFF+I,L)
        SUPSROW(I,L) = SUPSPAK(IOFF+I,L)
        HENRROW(I,L) = HENRPAK(IOFF+I,L)
        O3FRROW(I,L) = O3FRPAK(IOFF+I,L)
        H2O2FRROW(I,L) = H2O2FRPAK(IOFF+I,L)
        WPARROW(I,L) = WPARPAK(IOFF+I,L)
        PM25ROW(I,L) = PM25PAK(IOFF+I,L)
        PM10ROW(I,L) = PM10PAK(IOFF+I,L)
        DM25ROW(I,L) = DM25PAK(IOFF+I,L)
        DM10ROW(I,L) = DM10PAK(IOFF+I,L)
      ENDDO
      ENDDO
      DO I=IL1,IL2
        VNCNROW(I) = VNCNPAK(IOFF+I)
        VASNROW(I) = VASNPAK(IOFF+I)
        VSCDROW(I) = VSCDPAK(IOFF+I)
        VGSPROW(I) = VGSPPAK(IOFF+I)
        VOAEROW(I) = VOAEPAK(IOFF+I)
        VBCEROW(I) = VBCEPAK(IOFF+I)
        VASEROW(I) = VASEPAK(IOFF+I)
        VMDEROW(I) = VMDEPAK(IOFF+I)
        VSSEROW(I) = VSSEPAK(IOFF+I)
        VOAWROW(I) = VOAWPAK(IOFF+I)
        VBCWROW(I) = VBCWPAK(IOFF+I)
        VASWROW(I) = VASWPAK(IOFF+I)
        VMDWROW(I) = VMDWPAK(IOFF+I)
        VSSWROW(I) = VSSWPAK(IOFF+I)
        VOADROW(I) = VOADPAK(IOFF+I)
        VBCDROW(I) = VBCDPAK(IOFF+I)
        VASDROW(I) = VASDPAK(IOFF+I)
        VMDDROW(I) = VMDDPAK(IOFF+I)
        VSSDROW(I) = VSSDPAK(IOFF+I)
        VOAGROW(I) = VOAGPAK(IOFF+I)
        VBCGROW(I) = VBCGPAK(IOFF+I)
        VASGROW(I) = VASGPAK(IOFF+I)
        VMDGROW(I) = VMDGPAK(IOFF+I)
        VSSGROW(I) = VSSGPAK(IOFF+I)
        VOACROW(I) = VOACPAK(IOFF+I)
        VBCCROW(I) = VBCCPAK(IOFF+I)
        VASCROW(I) = VASCPAK(IOFF+I)
        VMDCROW(I) = VMDCPAK(IOFF+I)
        VSSCROW(I) = VSSCPAK(IOFF+I)
        VASIROW(I) = VASIPAK(IOFF+I)
        VAS1ROW(I) = VAS1PAK(IOFF+I)
        VAS2ROW(I) = VAS2PAK(IOFF+I)
        VAS3ROW(I) = VAS3PAK(IOFF+I)
        VCCNROW(I) = VCCNPAK(IOFF+I)
        VCNEROW(I) = VCNEPAK(IOFF+I)
      ENDDO
#endif
#if defined (xtrapla2)
      DO L=1,ILEV
      DO I=IL1,IL2
        CORNROW(I,L) = CORNPAK(IOFF+I,L)
        CORMROW(I,L) = CORMPAK(IOFF+I,L)
        RSN1ROW(I,L) = RSN1PAK(IOFF+I,L)
        RSM1ROW(I,L) = RSM1PAK(IOFF+I,L)
        RSN2ROW(I,L) = RSN2PAK(IOFF+I,L)
        RSM2ROW(I,L) = RSM2PAK(IOFF+I,L)
        RSN3ROW(I,L) = RSN3PAK(IOFF+I,L)
        RSM3ROW(I,L) = RSM3PAK(IOFF+I,L)
        DO ISF=1,ISDNUM
          SDNUROW(I,L,ISF) = SDNUPAK(IOFF+I,L,ISF)
          SDMAROW(I,L,ISF) = SDMAPAK(IOFF+I,L,ISF)
          SDACROW(I,L,ISF) = SDACPAK(IOFF+I,L,ISF)
          SDCOROW(I,L,ISF) = SDCOPAK(IOFF+I,L,ISF)
          SSSNROW(I,L,ISF) = SSSNPAK(IOFF+I,L,ISF)
          SMDNROW(I,L,ISF) = SMDNPAK(IOFF+I,L,ISF)
          SIANROW(I,L,ISF) = SIANPAK(IOFF+I,L,ISF)
          SSSMROW(I,L,ISF) = SSSMPAK(IOFF+I,L,ISF)
          SMDMROW(I,L,ISF) = SMDMPAK(IOFF+I,L,ISF)
          SEWMROW(I,L,ISF) = SEWMPAK(IOFF+I,L,ISF)
          SSUMROW(I,L,ISF) = SSUMPAK(IOFF+I,L,ISF)
          SOCMROW(I,L,ISF) = SOCMPAK(IOFF+I,L,ISF)
          SBCMROW(I,L,ISF) = SBCMPAK(IOFF+I,L,ISF)
          SIWMROW(I,L,ISF) = SIWMPAK(IOFF+I,L,ISF)
        ENDDO
      ENDDO
      ENDDO
      DO I=IL1,IL2
        DO ISF=1,ISDIAG
          SDVLROW(I,ISF) = SDVLPAK(IOFF+I,ISF)
        ENDDO
        VRN1ROW(I) = VRN1PAK(IOFF+I)
        VRM1ROW(I) = VRM1PAK(IOFF+I)
        VRN2ROW(I) = VRN2PAK(IOFF+I)
        VRM2ROW(I) = VRM2PAK(IOFF+I)
        VRN3ROW(I) = VRN3PAK(IOFF+I)
        VRM3ROW(I) = VRM3PAK(IOFF+I)
      ENDDO
      DO I=IL1,IL2
        DEFAROW(I) = DEFAPAK(IOFF+I)
        DEFCROW(I) = DEFCPAK(IOFF+I)
        DO ISF=1,ISDUST
          DMACROW(I,ISF) = DMACPAK(IOFF+I,ISF)
          DMCOROW(I,ISF) = DMCOPAK(IOFF+I,ISF)
          DNACROW(I,ISF) = DNACPAK(IOFF+I,ISF)
          DNCOROW(I,ISF) = DNCOPAK(IOFF+I,ISF)
        ENDDO
        DO ISF=1,ISDIAG
          DEFXROW(I,ISF) = DEFXPAK(IOFF+I,ISF)
          DEFNROW(I,ISF) = DEFNPAK(IOFF+I,ISF)
        ENDDO
      ENDDO
      DO I=IL1,IL2
        TNSSROW(I) = TNSSPAK(IOFF+I)
        TNMDROW(I) = TNMDPAK(IOFF+I)
        TNIAROW(I) = TNIAPAK(IOFF+I)
        TMSSROW(I) = TMSSPAK(IOFF+I)
        TMMDROW(I) = TMMDPAK(IOFF+I)
        TMOCROW(I) = TMOCPAK(IOFF+I)
        TMBCROW(I) = TMBCPAK(IOFF+I)
        TMSPROW(I) = TMSPPAK(IOFF+I)
        DO N=1,ISDNUM
          SNSSROW(I,N) = SNSSPAK(IOFF+I,N)
          SNMDROW(I,N) = SNMDPAK(IOFF+I,N)
          SNIAROW(I,N) = SNIAPAK(IOFF+I,N)
          SMSSROW(I,N) = SMSSPAK(IOFF+I,N)
          SMMDROW(I,N) = SMMDPAK(IOFF+I,N)
          SMOCROW(I,N) = SMOCPAK(IOFF+I,N)
          SMBCROW(I,N) = SMBCPAK(IOFF+I,N)
          SMSPROW(I,N) = SMSPPAK(IOFF+I,N)
          SIWHROW(I,N) = SIWHPAK(IOFF+I,N)
          SEWHROW(I,N) = SEWHPAK(IOFF+I,N)
        ENDDO
      ENDDO
#endif
#endif
#if defined xtrals
      DO L=1,ILEV
      DO I=IL1,IL2
        AGGROW (I,L) = AGGPAK (IOFF+I,L)
        AUTROW (I,L) = AUTPAK (IOFF+I,L)
        CNDROW (I,L) = CNDPAK (IOFF+I,L)
        DEPROW (I,L) = DEPPAK (IOFF+I,L)
        EVPROW (I,L) = EVPPAK (IOFF+I,L)
        FRHROW (I,L) = FRHPAK (IOFF+I,L)
        FRKROW (I,L) = FRKPAK (IOFF+I,L)
        FRSROW (I,L) = FRSPAK (IOFF+I,L)
        MLTIROW(I,L) = MLTIPAK(IOFF+I,L)
        MLTSROW(I,L) = MLTSPAK(IOFF+I,L)
        RACLROW(I,L) = RACLPAK(IOFF+I,L)
        RAINROW(I,L) = RAINPAK(IOFF+I,L)
        SACIROW(I,L) = SACIPAK(IOFF+I,L)
        SACLROW(I,L) = SACLPAK(IOFF+I,L)
        SNOWROW(I,L) = SNOWPAK(IOFF+I,L)
        SUBROW (I,L) = SUBPAK (IOFF+I,L)
        SEDIROW(I,L) = SEDIPAK(IOFF+I,L)
        RLIQROW(I,L) = RLIQPAK(IOFF+I,L)
        RICEROW(I,L) = RICEPAK(IOFF+I,L)
        CLIQROW(I,L) = CLIQPAK(IOFF+I,L)
        CICEROW(I,L) = CICEPAK(IOFF+I,L)
        RLNCROW(I,L) = RLNCPAK(IOFF+I,L)

        RLIQROL(I,L) = RLIQPAL(IOFF+I,L)
        RICEROL(I,L) = RICEPAL(IOFF+I,L)
        CLIQROL(I,L) = CLIQPAL(IOFF+I,L)
        CICEROL(I,L) = CICEPAL(IOFF+I,L)
        RLNCROL(I,L) = RLNCPAL(IOFF+I,L)

      ENDDO
      ENDDO
C
      DO I=IL1,IL2
        VAGGROW(I) = VAGGPAK(IOFF+I)
        VAUTROW(I) = VAUTPAK(IOFF+I)
        VCNDROW(I) = VCNDPAK(IOFF+I)
        VDEPROW(I) = VDEPPAK(IOFF+I)
        VEVPROW(I) = VEVPPAK(IOFF+I)
        VFRHROW(I) = VFRHPAK(IOFF+I)
        VFRKROW(I) = VFRKPAK(IOFF+I)
        VFRSROW(I) = VFRSPAK(IOFF+I)
        VMLIROW(I) = VMLIPAK(IOFF+I)
        VMLSROW(I) = VMLSPAK(IOFF+I)
        VRCLROW(I) = VRCLPAK(IOFF+I)
        VSCIROW(I) = VSCIPAK(IOFF+I)
        VSCLROW(I) = VSCLPAK(IOFF+I)
        VSUBROW(I) = VSUBPAK(IOFF+I)
        VSEDIROW(I)  = VSEDIPAK(IOFF+I)
        RELIQROW(I)  = RELIQPAK(IOFF+I)
        REICEROW(I)  = REICEPAK(IOFF+I)
        CLDLIQROW(I) = CLDLIQPAK(IOFF+I)
        CLDICEROW(I) = CLDICEPAK(IOFF+I)
        CTLNCROW(I)  = CTLNCPAK(IOFF+I)
        CLLNCROW(I)  = CLLNCPAK(IOFF+I)

        RELIQROL(I)  = RELIQPAL(IOFF+I)
        REICEROL(I)  = REICEPAL(IOFF+I)
        CLDLIQROL(I) = CLDLIQPAL(IOFF+I)
        CLDICEROL(I) = CLDICEPAL(IOFF+I)
        CTLNCROL(I)  = CTLNCPAL(IOFF+I)
        CLLNCROL(I)  = CLLNCPAL(IOFF+I)
      ENDDO
#endif
#if defined (aodpth)
       DO I=IL1,IL2
C
        EXB1ROW(I) = EXB1PAK(IOFF+I)
        EXB2ROW(I) = EXB2PAK(IOFF+I)
        EXB3ROW(I) = EXB3PAK(IOFF+I)
        EXB4ROW(I) = EXB4PAK(IOFF+I)
        EXB5ROW(I) = EXB5PAK(IOFF+I)
        EXBTROW(I) = EXBTPAK(IOFF+I)
        ODB1ROW(I) = ODB1PAK(IOFF+I)
        ODB2ROW(I) = ODB2PAK(IOFF+I)
        ODB3ROW(I) = ODB3PAK(IOFF+I)
        ODB4ROW(I) = ODB4PAK(IOFF+I)
        ODB5ROW(I) = ODB5PAK(IOFF+I)
        ODBTROW(I) = ODBTPAK(IOFF+I)
        ODBVROW(I) = ODBVPAK(IOFF+I)
        OFB1ROW(I) = OFB1PAK(IOFF+I)
        OFB2ROW(I) = OFB2PAK(IOFF+I)
        OFB3ROW(I) = OFB3PAK(IOFF+I)
        OFB4ROW(I) = OFB4PAK(IOFF+I)
        OFB5ROW(I) = OFB5PAK(IOFF+I)
        OFBTROW(I) = OFBTPAK(IOFF+I)
        ABB1ROW(I) = ABB1PAK(IOFF+I)
        ABB2ROW(I) = ABB2PAK(IOFF+I)
        ABB3ROW(I) = ABB3PAK(IOFF+I)
        ABB4ROW(I) = ABB4PAK(IOFF+I)
        ABB5ROW(I) = ABB5PAK(IOFF+I)
        ABBTROW(I) = ABBTPAK(IOFF+I)
C
        EXB1ROL(I) = EXB1PAL(IOFF+I)
        EXB2ROL(I) = EXB2PAL(IOFF+I)
        EXB3ROL(I) = EXB3PAL(IOFF+I)
        EXB4ROL(I) = EXB4PAL(IOFF+I)
        EXB5ROL(I) = EXB5PAL(IOFF+I)
        EXBTROL(I) = EXBTPAL(IOFF+I)
        ODB1ROL(I) = ODB1PAL(IOFF+I)
        ODB2ROL(I) = ODB2PAL(IOFF+I)
        ODB3ROL(I) = ODB3PAL(IOFF+I)
        ODB4ROL(I) = ODB4PAL(IOFF+I)
        ODB5ROL(I) = ODB5PAL(IOFF+I)
        ODBTROL(I) = ODBTPAL(IOFF+I)
        ODBVROL(I) = ODBVPAL(IOFF+I)
        OFB1ROL(I) = OFB1PAL(IOFF+I)
        OFB2ROL(I) = OFB2PAL(IOFF+I)
        OFB3ROL(I) = OFB3PAL(IOFF+I)
        OFB4ROL(I) = OFB4PAL(IOFF+I)
        OFB5ROL(I) = OFB5PAL(IOFF+I)
        OFBTROL(I) = OFBTPAL(IOFF+I)
        ABB1ROL(I) = ABB1PAL(IOFF+I)
        ABB2ROL(I) = ABB2PAL(IOFF+I)
        ABB3ROL(I) = ABB3PAL(IOFF+I)
        ABB4ROL(I) = ABB4PAL(IOFF+I)
        ABB5ROL(I) = ABB5PAL(IOFF+I)
        ABBTROL(I) = ABBTPAL(IOFF+I)
C
        EXS1ROW(I) = EXS1PAK(IOFF+I)
        EXS2ROW(I) = EXS2PAK(IOFF+I)
        EXS3ROW(I) = EXS3PAK(IOFF+I)
        EXS4ROW(I) = EXS4PAK(IOFF+I)
        EXS5ROW(I) = EXS5PAK(IOFF+I)
        EXSTROW(I) = EXSTPAK(IOFF+I)
        ODS1ROW(I) = ODS1PAK(IOFF+I)
        ODS2ROW(I) = ODS2PAK(IOFF+I)
        ODS3ROW(I) = ODS3PAK(IOFF+I)
        ODS4ROW(I) = ODS4PAK(IOFF+I)
        ODS5ROW(I) = ODS5PAK(IOFF+I)
        ODSTROW(I) = ODSTPAK(IOFF+I)
        ODSVROW(I) = ODSVPAK(IOFF+I)
        OFS1ROW(I) = OFS1PAK(IOFF+I)
        OFS2ROW(I) = OFS2PAK(IOFF+I)
        OFS3ROW(I) = OFS3PAK(IOFF+I)
        OFS4ROW(I) = OFS4PAK(IOFF+I)
        OFS5ROW(I) = OFS5PAK(IOFF+I)
        OFSTROW(I) = OFSTPAK(IOFF+I)
        ABS1ROW(I) = ABS1PAK(IOFF+I)
        ABS2ROW(I) = ABS2PAK(IOFF+I)
        ABS3ROW(I) = ABS3PAK(IOFF+I)
        ABS4ROW(I) = ABS4PAK(IOFF+I)
        ABS5ROW(I) = ABS5PAK(IOFF+I)
        ABSTROW(I) = ABSTPAK(IOFF+I)
C
        EXS1ROL(I) = EXS1PAL(IOFF+I)
        EXS2ROL(I) = EXS2PAL(IOFF+I)
        EXS3ROL(I) = EXS3PAL(IOFF+I)
        EXS4ROL(I) = EXS4PAL(IOFF+I)
        EXS5ROL(I) = EXS5PAL(IOFF+I)
        EXSTROL(I) = EXSTPAL(IOFF+I)
        ODS1ROL(I) = ODS1PAL(IOFF+I)
        ODS2ROL(I) = ODS2PAL(IOFF+I)
        ODS3ROL(I) = ODS3PAL(IOFF+I)
        ODS4ROL(I) = ODS4PAL(IOFF+I)
        ODS5ROL(I) = ODS5PAL(IOFF+I)
        ODSTROL(I) = ODSTPAL(IOFF+I)
        ODSVROL(I) = ODSVPAL(IOFF+I)
        OFS1ROL(I) = OFS1PAL(IOFF+I)
        OFS2ROL(I) = OFS2PAL(IOFF+I)
        OFS3ROL(I) = OFS3PAL(IOFF+I)
        OFS4ROL(I) = OFS4PAL(IOFF+I)
        OFS5ROL(I) = OFS5PAL(IOFF+I)
        OFSTROL(I) = OFSTPAL(IOFF+I)
        ABS1ROL(I) = ABS1PAL(IOFF+I)
        ABS2ROL(I) = ABS2PAL(IOFF+I)
        ABS3ROL(I) = ABS3PAL(IOFF+I)
        ABS4ROL(I) = ABS4PAL(IOFF+I)
        ABS5ROL(I) = ABS5PAL(IOFF+I)
        ABSTROL(I) = ABSTPAL(IOFF+I)
C
      ENDDO
#endif
#if defined use_cosp
! COSP INPUT
      DO L = 1, ILEV
         DO I = IL1, IL2
            RMIXROW(I,L)  = RMIXPAK(IOFF+I,L)
            SMIXROW(I,L)  = SMIXPAK(IOFF+I,L)
            RREFROW(I,L)  = RREFPAK(IOFF+I,L)
            SREFROW(I,L)  = SREFPAK(IOFF+I,L)
         END DO ! I
      END DO ! L


! COSP OUTPUT
      DO I = IL1, IL2

! ISCCP fields
         IF (Lalbisccp)
     1        albisccp(I) = o_albisccp(IOFF+I)

         IF (Ltauisccp)
     1        tauisccp(I) = o_tauisccp(IOFF+I)

         IF (Lpctisccp)
     1        pctisccp(I) = o_pctisccp(IOFF+I)

         IF (Lcltisccp)
     1        cltisccp(I) = o_cltisccp(IOFF+I)


         IF (Lmeantbisccp)
     1        meantbisccp(I) = o_meantbisccp(IOFF+I)

         IF (Lmeantbclrisccp)
     1        meantbclrisccp(I) = o_meantbclrisccp(IOFF+I)

         IF (Lisccp_sim)
     1        sunl(I) = o_sunl(IOFF+I)

! CALIPSO fields

         IF (Lclhcalipso) THEN
            clhcalipso(I)    = o_clhcalipso(IOFF+I)
            cnt_clhcalipso(I) = ocnt_clhcalipso(IOFF+I)
         END IF

         IF (Lclmcalipso) THEN
            clmcalipso(I)    = o_clmcalipso(IOFF+I)
            cnt_clmcalipso(I) = ocnt_clmcalipso(IOFF+I)
         END IF

         IF (Lcllcalipso) THEN
            cllcalipso(I)    = o_cllcalipso(IOFF+I)
            cnt_cllcalipso(I) = ocnt_cllcalipso(IOFF+I)
         END IF

         IF (Lcltcalipso) THEN
            cltcalipso(I)     = o_cltcalipso(IOFF+I)
            cnt_cltcalipso(I) = ocnt_cltcalipso(IOFF+I)
         END IF

         IF (Lclhcalipsoliq) THEN
            clhcalipsoliq(I)    = o_clhcalipsoliq(IOFF+I)
            cnt_clhcalipsoliq(I) = ocnt_clhcalipsoliq(IOFF+I)
         END IF

         IF (Lclmcalipsoliq) THEN
            clmcalipsoliq(I)    = o_clmcalipsoliq(IOFF+I)
            cnt_clmcalipsoliq(I) = ocnt_clmcalipsoliq(IOFF+I)
         END IF

         IF (Lcllcalipsoliq) THEN
            cllcalipsoliq(I)    = o_cllcalipsoliq(IOFF+I)
            cnt_cllcalipsoliq(I) = ocnt_cllcalipsoliq(IOFF+I)
         END IF

         IF (Lcltcalipsoliq) THEN
            cltcalipsoliq(I)     = o_cltcalipsoliq(IOFF+I)
            cnt_cltcalipsoliq(I) = ocnt_cltcalipsoliq(IOFF+I)
         END IF

         IF (Lclhcalipsoice) THEN
            clhcalipsoice(I)    = o_clhcalipsoice(IOFF+I)
            cnt_clhcalipsoice(I) = ocnt_clhcalipsoice(IOFF+I)
         END IF

         IF (Lclmcalipsoice) THEN
            clmcalipsoice(I)    = o_clmcalipsoice(IOFF+I)
            cnt_clmcalipsoice(I) = ocnt_clmcalipsoice(IOFF+I)
         END IF

         IF (Lcllcalipsoice) THEN
            cllcalipsoice(I)    = o_cllcalipsoice(IOFF+I)
            cnt_cllcalipsoice(I) = ocnt_cllcalipsoice(IOFF+I)
         END IF

         IF (Lcltcalipsoice) THEN
            cltcalipsoice(I)     = o_cltcalipsoice(IOFF+I)
            cnt_cltcalipsoice(I) = ocnt_cltcalipsoice(IOFF+I)
         END IF

         IF (Lclhcalipsoun) THEN
            clhcalipsoun(I)    = o_clhcalipsoun(IOFF+I)
            cnt_clhcalipsoun(I) = ocnt_clhcalipsoun(IOFF+I)
         END IF

         IF (Lclmcalipsoun) THEN
            clmcalipsoun(I)    = o_clmcalipsoun(IOFF+I)
            cnt_clmcalipsoun(I) = ocnt_clmcalipsoun(IOFF+I)
         END IF

         IF (Lcllcalipsoun) THEN
            cllcalipsoun(I)    = o_cllcalipsoun(IOFF+I)
            cnt_cllcalipsoun(I) = ocnt_cllcalipsoun(IOFF+I)
         END IF

         IF (Lcltcalipsoun) THEN
            cltcalipsoun(I)     = o_cltcalipsoun(IOFF+I)
            cnt_cltcalipsoun(I) = ocnt_cltcalipsoun(IOFF+I)
         END IF

! CloudSat+CALIPSO fields
         IF (Lcltlidarradar) THEN
            cltlidarradar(I)    = o_cltlidarradar(IOFF+I)
            cnt_cltlidarradar(I) = ocnt_cltlidarradar(IOFF+I)
         END IF
      END DO


! Several special fields > 1D

! ISCCP
      IF (Lclisccp) THEN
         DO IP = 1, 7           !NPTOP
            DO IT = 1, 7        !NTAUCLD
               DO I = IL1, IL2
                  clisccp(I,IT,IP) = o_clisccp(IOFF+I,IT,IP)

               END DO
            END DO
         END DO
      END IF

! PARASOL
      IF (LparasolRefl) THEN
         DO IP = 1, PARASOL_NREFL
            DO I = IL1, IL2
               parasol_refl(I,IP)     =  o_parasol_refl(IOFF+I,IP)
               cnt_parasol_refl(I,IP) =  ocnt_parasol_refl(IOFF+I,IP)
            END DO
         END DO
      END IF

! 3D fields that might be interpolated to special levels or not
      IF (use_vgrid) THEN       ! Interpolate to specific heights
         DO N = 1, Nlr
            DO I = IL1, IL2
               IF (Lclcalipso) THEN
                  clcalipso(I,N)     = o_clcalipso(IOFF+I,N)
                  cnt_clcalipso(I,N) = ocnt_clcalipso(IOFF+I,N)
               END IF

               IF (Lclcalipsoliq) THEN
                  clcalipsoliq(I,N)     = o_clcalipsoliq(IOFF+I,N)
                  cnt_clcalipsoliq(I,N) = ocnt_clcalipsoliq(IOFF+I,N)
               END IF

               IF (Lclcalipsoice) THEN
                  clcalipsoice(I,N)     = o_clcalipsoice(IOFF+I,N)
                  cnt_clcalipsoice(I,N) = ocnt_clcalipsoice(IOFF+I,N)
               END IF

               IF (Lclcalipsoun) THEN
                  clcalipsoun(I,N)     = o_clcalipsoun(IOFF+I,N)
                  cnt_clcalipsoun(I,N) = ocnt_clcalipsoun(IOFF+I,N)
               END IF

               IF (Lclcalipso2) THEN
                  clcalipso2(I,N)     = o_clcalipso2(IOFF+I,N)
                  cnt_clcalipso2(I,N) = ocnt_clcalipso2(IOFF+I,N)
               END IF

               IF (LcfadDbze94 .OR. LcfadLidarsr532) THEN
                  cosp_height_mask(I,N) = o_cosp_height_mask(IOFF+I,N)
               END IF
            END DO
         END DO
      ELSE
         DO N = 1, ILEV
            DO I = IL1, IL2
               IF (Lclcalipso) THEN
                   clcalipso(I,N)     =  o_clcalipso(IOFF+I,N)
                   cnt_clcalipso(I,N) = ocnt_clcalipso(IOFF+I,N)
               END IF

               IF (Lclcalipso2) THEN
                   clcalipso2(I,N)     = o_clcalipso2(IOFF+I,N)
                   cnt_clcalipso2(I,N) = ocnt_clcalipso2(IOFF+I,N)
               END IF

               IF (LcfadDbze94 .OR. LcfadLidarsr532) THEN
                  cosp_height_mask(I,N) = o_cosp_height_mask(IOFF+I,N)
               END IF
            END DO
         END DO
      END IF

         DO N = 1, LIDAR_NTEMP
            DO I = IL1, IL2
               IF (Lclcalipsotmp) THEN
                  clcalipsotmp(I,N)     = o_clcalipsotmp(IOFF+I,N)
                  cnt_clcalipsotmp(I,N) = ocnt_clcalipsotmp(IOFF+I,N)
               END IF

               IF (Lclcalipsotmpliq) THEN
                  clcalipsotmpliq(I,N)     = o_clcalipsotmpliq(IOFF+I,N)
                  cnt_clcalipsotmpliq(I,N) =
     1                                    ocnt_clcalipsotmpliq(IOFF+I,N)
               END IF

               IF (Lclcalipsotmpice) THEN
                  clcalipsotmpice(I,N)     = o_clcalipsotmpice(IOFF+I,N)
                  cnt_clcalipsotmpice(I,N) =
     1                                    ocnt_clcalipsotmpice(IOFF+I,N)
               END IF

               IF (Lclcalipsotmpun) THEN
                  clcalipsotmpun(I,N)     = o_clcalipsotmpun(IOFF+I,N)
                  cnt_clcalipsotmpun(I,N) =
     1                                    ocnt_clcalipsotmpun(IOFF+I,N)
               END IF
            END DO ! I
         END DO ! N
! CFADS (2D histograms)

! CALIPSO

      IF (use_vgrid) THEN       ! Interpolate to specific heights
         IF (LcfadLidarsr532) THEN
            DO isr = 1, SR_BINS
               DO N = 1, Nlr
                  DO I = IL1, IL2
                     cfad_lidarsr532(I,N,isr) =
     1                    o_cfad_lidarsr532(IOFF+I,N,isr)
                  END DO
               END DO
            END DO
         ENDIF
      ELSE
         IF (LcfadLidarsr532) THEN
            DO isr = 1, SR_BINS
               DO N = 1, ILEV
                  DO I = IL1, IL2
                     cfad_lidarsr532(I,N,isr) =
     1                    o_cfad_lidarsr532(IOFF+I,N,isr)
                  END DO
               END DO
            END DO
         ENDIF
      END IF

! CloudSat

      IF (use_vgrid) THEN       ! Interpolate to specific heights
         IF (LcfadDbze94) THEN
            DO ize = 1, DBZE_BINS
               DO N = 1, Nlr
                  DO I = IL1, IL2
                     cfad_dbze94(I,N,ize)=
     1                    o_cfad_dbze94(IOFF+I,N,ize)
                  END DO
               END DO
            END DO
         ENDIF
      ELSE
         IF (LcfadDbze94) THEN
            DO ize = 1, DBZE_BINS
               DO N = 1, ILEV
                  DO I = IL1, IL2
                     cfad_dbze94(I,N,ize) =
     1                    o_cfad_dbze94(IOFF+I,N,ize)
                  END DO
               END DO
            END DO
         ENDIF
      END IF
C
      IF (Lceres_sim) THEN
         DO IP = 1, NCERES
            DO I = IL1, IL2
               ceres_cf(I,IP)    = o_ceres_cf(IOFF+I,IP)
               ceres_cnt(I,IP)   = o_ceres_cnt(IOFF+I,IP)
               ceres_ctp(I,IP)   = o_ceres_ctp(IOFF+I,IP)
               ceres_tau(I,IP)   = o_ceres_tau(IOFF+I,IP)
               ceres_lntau(I,IP) = o_ceres_lntau(IOFF+I,IP)
               ceres_lwp(I,IP)   = o_ceres_lwp(IOFF+I,IP)
               ceres_iwp(I,IP)   = o_ceres_iwp(IOFF+I,IP)
               ceres_cfl(I,IP)   = o_ceres_cfl(IOFF+I,IP)
               ceres_cfi(I,IP)   = o_ceres_cfi(IOFF+I,IP)
               ceres_cntl(I,IP)  = o_ceres_cntl(IOFF+I,IP)
               ceres_cnti(I,IP)  = o_ceres_cnti(IOFF+I,IP)
               ceres_rel(I,IP)   = o_ceres_rel(IOFF+I,IP)
               ceres_cdnc(I,IP)  = o_ceres_cdnc(IOFF+I,IP)
               ceres_clm(I,IP)   = o_ceres_clm(IOFF+I,IP)
               ceres_cntlm(I,IP) = o_ceres_cntlm(IOFF+I,IP)
            END DO ! I
         END DO ! IP
      END IF
      IF (Lceres_sim .AND. Lswath) THEN
         DO IP = 1, NCERES
            DO I = IL1, IL2
               s_ceres_cf(I,IP)    = os_ceres_cf(IOFF+I,IP)
               s_ceres_cnt(I,IP)   = os_ceres_cnt(IOFF+I,IP)
               s_ceres_ctp(I,IP)   = os_ceres_ctp(IOFF+I,IP)
               s_ceres_tau(I,IP)   = os_ceres_tau(IOFF+I,IP)
               s_ceres_lntau(I,IP) = os_ceres_lntau(IOFF+I,IP)
               s_ceres_lwp(I,IP)   = os_ceres_lwp(IOFF+I,IP)
               s_ceres_iwp(I,IP)   = os_ceres_iwp(IOFF+I,IP)
               s_ceres_cfl(I,IP)   = os_ceres_cfl(IOFF+I,IP)
               s_ceres_cfi(I,IP)   = os_ceres_cfi(IOFF+I,IP)
               s_ceres_cntl(I,IP)  = os_ceres_cntl(IOFF+I,IP)
               s_ceres_cnti(I,IP)  = os_ceres_cnti(IOFF+I,IP)
               s_ceres_rel(I,IP)   = os_ceres_rel(IOFF+I,IP)
               s_ceres_cdnc(I,IP)  = os_ceres_cdnc(IOFF+I,IP)
               s_ceres_clm(I,IP)   = os_ceres_clm(IOFF+I,IP)
               s_ceres_cntlm(I,IP) = os_ceres_cntlm(IOFF+I,IP)
            END DO ! I
         END DO ! IP
      END IF

! MISR
      IF (LclMISR) THEN
         DO I = IL1, IL2
            MISR_cldarea(I)   = o_MISR_cldarea(IOFF+I)
            MISR_mean_ztop(I) = o_MISR_mean_ztop(IOFF+I)
         END DO ! I

         DO IP = 1,MISR_N_CTH
            DO I = IL1, IL2
               dist_model_layertops(I,IP) =
     1                               o_dist_model_layertops(IOFF+I,IP)
            END DO ! I
         END DO ! IP

         IBOX = 1
         DO IP = 1,MISR_N_CTH
            DO IT = 1, 7
               DO I = IL1, IL2
                  fq_MISR_TAU_v_CTH(I,IBOX) =
     1                               o_fq_MISR_TAU_v_CTH(IOFF+I,IBOX)
               END DO ! I
               IBOX = IBOX + 1
            END DO ! IT
         END DO ! IP
      ENDIF ! LclMISR

! MODIS
      IF (Lcltmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Fraction_Total_Mean(I) =
     1                         o_MODIS_Cloud_Fraction_Total_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lclwmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Fraction_Water_Mean(I) =
     1                        o_MODIS_Cloud_Fraction_Water_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lclimodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Fraction_Ice_Mean(I) =
     1                          o_MODIS_Cloud_Fraction_Ice_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lclhmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Fraction_High_Mean(I) =
     1                          o_MODIS_Cloud_Fraction_High_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lclmmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Fraction_Mid_Mean(I) =
     1                          o_MODIS_Cloud_Fraction_Mid_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lcllmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Fraction_Low_Mean(I) =
     1                          o_MODIS_Cloud_Fraction_Low_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Ltautmodis) THEN
         DO I = IL1, IL2
            MODIS_Optical_Thickness_Total_Mean(I) =
     1                      o_MODIS_Optical_Thickness_Total_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Ltauwmodis) THEN
         DO I = IL1, IL2
            MODIS_Optical_Thickness_Water_Mean(I) =
     1                      o_MODIS_Optical_Thickness_Water_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Ltauimodis) THEN
         DO I = IL1, IL2
            MODIS_Optical_Thickness_Ice_Mean(I) =
     1                        o_MODIS_Optical_Thickness_Ice_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Ltautlogmodis) THEN
         DO I = IL1, IL2
            MODIS_Optical_Thickness_Total_LogMean(I) =
     1                  o_MODIS_Optical_Thickness_Total_LogMean(IOFF+I)
         END DO ! I
      END IF

      IF (Ltauwlogmodis) THEN
         DO I = IL1, IL2
            MODIS_Optical_Thickness_Water_LogMean(I) =
     1                  o_MODIS_Optical_Thickness_Water_LogMean(IOFF+I)
         END DO ! I
      END IF

      IF (Ltauilogmodis) THEN
         DO I = IL1, IL2
            MODIS_Optical_Thickness_Ice_LogMean(I) =
     1                    o_MODIS_Optical_Thickness_Ice_LogMean(IOFF+I)
         END DO ! I
      END IF

      IF (Lreffclwmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Particle_Size_Water_Mean(I) =
     1                    o_MODIS_Cloud_Particle_Size_Water_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lreffclimodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Particle_Size_Ice_Mean(I) =
     1                     o_MODIS_Cloud_Particle_Size_Ice_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lpctmodis) THEN
         DO I = IL1, IL2
            MODIS_Cloud_Top_Pressure_Total_Mean(I) =
     1                     o_MODIS_Cloud_Top_Pressure_Total_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Llwpmodis) THEN
         DO I = IL1, IL2
            MODIS_Liquid_Water_Path_Mean(I) =
     1                            o_MODIS_Liquid_Water_Path_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Liwpmodis) THEN
         DO I = IL1, IL2
            MODIS_Ice_Water_Path_Mean(I) =
     1                             o_MODIS_Ice_Water_Path_Mean(IOFF+I)
         END DO ! I
      END IF

      IF (Lclmodis) THEN
         IBOX = 1
         DO IP = 1, a_numModisPressureBins
            DO IT = 1, a_numModisTauBins+1
               DO I = IL1, IL2
          MODIS_Optical_Thickness_vs_Cloud_Top_Pressure(I,IBOX)=
     1     o_MODIS_Optical_Thickness_vs_Cloud_Top_Pressure(IOFF+I,IBOX)
                END DO ! I
                IBOX = IBOX + 1
             END DO ! IT
          END DO ! IP
      END IF

      IF (Lcrimodis) THEN
         IBOX = 1
         DO IP = 1, a_numModisReffIceBins
            DO IT = 1, a_numModisTauBins+1
               DO I = IL1, IL2
                  MODIS_Optical_Thickness_vs_reffICE(I,IBOX)=
     1                 o_MODIS_Optical_Thickness_vs_reffICE(IOFF+I,IBOX)
                END DO ! I
                IBOX = IBOX + 1
             END DO ! IT
          END DO ! IP
      END IF

      IF (Lcrlmodis) THEN
         IBOX = 1
         DO IP = 1, a_numModisReffLiqBins
            DO IT = 1, a_numModisTauBins+1
               DO I = IL1, IL2
                  MODIS_Optical_Thickness_vs_reffLiq(I,IBOX)=
     1                 o_MODIS_Optical_Thickness_vs_reffLiq(IOFF+I,IBOX)
                END DO ! I
                IBOX = IBOX + 1
             END DO ! IT
          END DO ! IP
      END IF

      IF (Llcdncmodis) THEN
         DO I = IL1, IL2
            MODIS_Liq_CDNC_Mean(I) =
     1                             o_MODIS_Liq_CDNC_Mean(IOFF+I)
            MODIS_Liq_CDNC_GCM_Mean(I) =
     1                             o_MODIS_Liq_CDNC_GCM_Mean(IOFF+I)
            MODIS_Cloud_Fraction_Warm_Mean(I) =
     1                  o_MODIS_Cloud_Fraction_Warm_Mean(IOFF+I)
         END DO ! I
      END IF
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
