#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * SECTION 1.   INPUT FOR INITIAL START OF THE MODEL, I.E. KOUNT=0.
C     *              GET GRID PHYSICS FROM NUGG AND
C     *              SPECTRAL FIELDS FROM NUSP.

      CALL GETGG14(NUGG,TGPAT,THLQPAT,THICPAT,
     1             SICNPAK,SICPAK,GTPAK,SNOPAK,
     2             LUINPAK,LUIMPAK,LRINPAK,LRIMPAK,
     3             ENVPAK,GAMPAK,PSIPAK,ALPHPAK,DELTPAK,SIGXPAK,
     4             ALSWPAK,ALLWPAK,PHISPAK,
     5             SANDPAT,CLAYPAT,ORGMPAT,DPTHPAT,DRNPAT,SOCIPAT,
     6             FCANPAT,ALVCPAT,ALICPAT,LNZ0PAT,
     7             LAMXPAT,LAMNPAT,CMASPAT,ROOTPAT,
     8             HLAKPAK,LLAKPAK,BLAKPAK,
     9             FLKUPAK,FLKRPAK,GICNPAK,FLNDPAK,
     A             LC,LG,LCT,LGT,ICAN,ICANP1,IGND,NTLD,IJPAK,
     B             LON1,NLAT,IDAY,GLL)
C
      CALL GETNAE (NUCH,ESCVPAK,EHCVPAK,ESEVPAK,EHEVPAK,
     1             IJPAK,LON1,NLAT,GLL)
      CALL GETDUE (NUCH,SPOTPAK,ST01PAK,ST02PAK,
     1             ST03PAK,ST04PAK,ST06PAK,ST13PAK,
     2             ST14PAK,ST15PAK,ST16PAK,ST17PAK,
     3             IJPAK,LON1,NLAT,GLL)
C
C     * GET LDAY VALUE.
C
      CALL GETLDAY(LDAY,IDAY)

      CALL GETSPEX (NUSP,PHIS,PS, T ,P,C,ES,LA,LM,ILEV,LEVSPEC,
     1              LS,LH,IDIV,LATOTAL,LMTOTAL,LSRTOTAL,GLL)
C
C     * INITIALIZE TRACER FIELDS ABOUT TO BE READ.
C     * THEN READ FROM START FILE IF NOT USING SURFACE INPUT.
C
      CALL PUTZERO(TRAC,LA*2*ILEV*NTRACA)
      CALL PUTZERO(TRACNA,IP0J*ILEV*NTRACN*2)
      IF(ITRINIT.NE.0)                                         THEN
        IF(NTRACA.GT.0)                                        THEN
          CALL GETRAC4(NUSP,TRAC,LA,LM,ILEV,LH,ITRAC,NTRAC,NTRACA,
     1                 INDXA,ITRNAM,ITRIC,
     2                 LATOTAL,LMTOTAL,LSRTOTAL,GLL)
        ENDIF
C
        IF(NTRACN.GT.0)                                        THEN
          CALL GETRACNA3(NUSP,TRACNA,ILEV,LH,ITRAC,NTRAC,NTRACN,
     1                     INDXNA,ITRNAM,ITRIC,ITRLVS,ITRLVF,
     2                     IP0J,LON1,NLAT,GLL)
        ENDIF
      ENDIF
C
      IF(NTRACN.GT.0)                                          THEN
C
C       * COPY INP TIME  LEVEL INTO INM TIME LEVEL
C
        DO II=1,IP0J*ILEV*NTRACN
          TRACNA(II,INM)=TRACNA(II,INP)
        ENDDO
      ENDIF
C
C     * INITIALIZE GRID FIELDS.
C
#include "init12.h"
C
C     * READ SPECIFIED CHEMISTRY FORCING FROM A FILE AT START
C     * OF RUN.
C
        CALL GETMDAYT(MDAYT,MDAYT1,IDAY,MON,MON1)
#include "getemip2.h"
#if defined (pla)
#if defined (pfrc)
      CALL GETFRC (AMLDFPAK,REAMFPAK,VEAMFPAK,FR1FPAK,FR2FPAK,
     1             SSLDFPAK,RESSFPAK,VESSFPAK,DSLDFPAK,REDSFPAK,
     2             VEDSFPAK,BCLDFPAK,REBCFPAK,VEBCFPAK,OCLDFPAK,
     3             REOCFPAK,VEOCFPAK,ZCDNFPAK,BCICFPAK,BCDPFPAK,
     4             AMLDFPAL,REAMFPAL,VEAMFPAL,FR1FPAL,FR2FPAL,
     5             SSLDFPAL,RESSFPAL,VESSFPAL,DSLDFPAL,REDSFPAL,
     6             VEDSFPAL,BCLDFPAL,REBCFPAL,VEBCFPAL,OCLDFPAL,
     7             REOCFPAL,VEOCFPAL,ZCDNFPAL,BCICFPAL,BCDPFPAL,
     8             LON1,NLAT,ILEV,INCD,IDAY,MDAYT,MDAYT1,
     9             KOUNT,IJPAK,NUPF,LH,GLL)
#endif
#endif
C
#if defined transient_ozone_concentrations
      CALL GETRANOZ(OZPAK,OZPAL,NC4TO8("  OZ"),LO,LEVOZ,
     1              LON1,NLAT,INCD,IREFYRO,IDAY,KOUNT,IJPAK,
     2              NUOZ_RAD,GLL)
C
c     CALL GETRANOZ(O3CPAK,O3CPAL,NC4TO8("  O3"),LOCHEM,LEVOZC,
c    1              LON1,NLAT,INCD,IREFYRO,IDAY,KOUNT,IJPAK,
c    2              NUOZ_CHEM,GLL)
C
C     * THESE ARE ZEROED OUT BECAUSE USING OXIDANT OZONE FOR "OZCHM".
C
      O3CPAK=0.
      O3CPAL=0.
#endif
C
C     * INITIALIZE T-1 SPECTRAL FIELDS.
C
      CALL CCOPY (LA*ILEV,  P ,1,  PM  ,1)
      CALL CCOPY (LA*ILEV,  C ,1,  CM  ,1)
      CALL CCOPY (LA*ILEV,  T ,1,  TM  ,1)
      CALL CCOPY (LA*LEVS, ES ,1, ESM  ,1)
      CALL CCOPY (LA     , PS ,1, PSM  ,1)
      CALL CCOPY (LA*ILEV*NTRSPEC,TRAC,1,TRACM,1)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
