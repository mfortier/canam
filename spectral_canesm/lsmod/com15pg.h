!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * JUL 27/2019 - M.LAZARE. Remove {COM,CSP,CSN} parmsub variables.
C     * DEC 23/2007 - M.LAZARE. NEW VERSION FOR GCM15G:
C     *                         - "SL" AND "SLSAVE" CONDITIONAL
C     *                            CODE REMOVED.
C     * JUN 27/2006 - M.LAZARE. PREVIOUS VERSION COM15PF FOR GCM15F.
C     *                         - NO REAL*8 FOR TRIG FUNCTIONS.
C     * NOV 28/03 - M.LAZARE/ PREVIOUS VERSION COM13PC FOR GCM13C, WITH GCMPARM
C     *            L.SOLHEIM. VARIABLES REPLACED BY EXPLICIT PARAMETER 
C     *                       STATEMENT VALUES.
C     * MAY 26/03 - M.LAZARE. NEW COMMON DECK (COM13PB) CONTAINING DECLARATIONS
C     *                       PERTAINING ONLY TO PHYSICS OR S/L
C     *                       PREPARATION PARALLEL SUBROUTINES.
C     *                       LONGITUDE AND LEVEL ARE EXPLICITLY SPLIT
C     *                       TO FACILITATE CONVERSION TO/FROM POINTERS.
C     *                       NOTE THAT ALL THE COMMON BLOCKS HAVE
C     *                       TO BE LOCAL (PRIVATE) TO EACH TASK.
C==================================================================
C     * GRID SLICE WORK FIELDS.
C
C     * FOR I/O PURPOSES, (U,V,T,ES,(TRAC))'TG MUST BE CONSECUTIVE.
C     * THE FOLLOWING FIELDS ARE MAPPED INTO THE "FOUR1" WORK ARRAY
C     * IN MHANLP4.
C
      COMMON /GR/ TTG   (ILG_TP,ILEV)
      COMMON /GR/ ESTG  (ILG_TP,LEVS)
      COMMON /GR/ TRACTG(ILG_TP,ILEV,NTRACA)
      COMMON /GR/ VTG   (ILG_TP,ILEV)
      COMMON /GR/ UTG   (ILG_TP,ILEV)
      COMMON /GR/ PSTG  (ILG_TP)
C
      COMMON /GR/ PRESSG(ILG_TP)
      COMMON /GR/ PG    (ILG_TP,ILEV),CG    (ILG_TP,ILEV)
      COMMON /GR/ TG    (ILG_TP,ILEV),ESG   (ILG_TP,LEVS)
      COMMON /GR/ TRACG (ILG_TP,ILEV,NTRACA)
      COMMON /GR/ PSDPG (ILG_TP)
      COMMON /GR/ UG    (ILG_TP,ILEV),VG    (ILG_TP,ILEV)
      COMMON /GR/ PSDLG (ILG_TP)
!$OMP THREADPRIVATE (/GR/)
C
C     * NOTE THAT /GR1/ CONTAINS FIELDS USED IN BOTH THE PHYSICS
C     * AND DYNAMICS PARALLEL REGIONS.
C
      COMMON /GR1/ DSGJ (IDLM), DSHJ (IDLM), DLNSGJ(IDLM)
      COMMON /GR1/ D1SGJ(IDLM), A1SGJ(IDLM), B1SGJ (IDLM)
      COMMON /GR1/ D2SGJ(IDLM), A2SGJ(IDLM), B2SGJ (IDLM)
!$OMP THREADPRIVATE (/GR1/)
C==================================================================
C     * /GR2/ CONTAINS WORK FIELDS ONLY FOR PHYSICS.
C
      COMMON /GR2/ SGJ (ILG,ILEV),  SHJ  (ILG,ILEV) 
      COMMON /GR2/ SHTJ(ILG,ILEVP1), SGBJ (ILG,ILEV)
!$OMP THREADPRIVATE (/GR2/)
C==================================================================
C     * ASSOCIATED "ROW" VECTORS FOR MULTI-LATITUDE FORMULATION
C     * (BOTH PHYSICS AND S/L INIT).
C     * *** NOTE THAT SINCE THESE ARE USED ON BOTH THE TRANSFORM
C     *     AND GENERAL COMPUTATION "ROWS", THE LARGEST, NAMELY
C     *     THE TRANSFORM, IS CHOSEN. 
C
      REAL*8  COSJ,WJ,RADJ
      REAL    DLON
      INTEGER JL, ILSL

      COMMON /GAUSSJ/ COSJ(ILG_TP), WJ(ILG_TP)
      COMMON /GAUSSJ/ RADJ(ILG_TP), DLON(ILG_TP)
      COMMON /GAUSSJ/ JL(ILG_TP), ILSL(ILG_TP)
!$OMP THREADPRIVATE (/GAUSSJ/)
C======================================================================
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
