!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      subroutine INIT_PHYS_PARM(mynode)
c***********************************************************************
c Read physical parameter values from a file named PHYS_PARM
c
c Input
c   integer*4 mynode  ...mpi node (used to restrict I/O to node 0)
c
c Jason Cole ...8 Dec 2017
c***********************************************************************

      use phys_parm_defs

      implicit none

      !--- Input
      integer*4 :: mynode	!<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      !--- Local
      INTEGER PHYS_PARM_IU
      COMMON/PHYSPARMIU/ PHYS_PARM_IU
      logical :: ok
      integer, parameter :: verbose=2

c-----------------------------------------------------------------------
      !--- Set the physical parameter defaults
      pp_solar_const = 1361.0         ! Units => W/m^2
      pp_rdm_num_pert = 0             ! Unitless
      pp_mam_damp_gw_time = 1209600.0 ! Seconds

      !--- Read physical parameter set in parmsub from a namelist

      inquire(file='PHYS_PARM',exist=ok)
      if (ok) then
        rewind(PHYS_PARM_IU)
        read(PHYS_PARM_IU,nml=phys_parm)
        if (mynode.eq.0) then
          write(6,*)' '
          write(6,phys_parm)
          write(6,*)' '
          call flush(6)
        endif
      else
        write(6,*)'Namelist file PHYS_PARM is missing.'
        call xit("INIT_PHYS_PARM",-1)
      endif

      end subroutine init_phys_parm
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
