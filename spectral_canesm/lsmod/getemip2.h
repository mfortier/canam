#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * JUL 30, 2018 - M.LAZARE.     Remove non-transient cpp if blocks.
C     * JUN 11, 2018 - M.LAZARE.     Read from unit number NUCH ("llchem" file)
C     *                              instead of NUGG for non-transient chemistry
C     *                              forcing (several calls). Also read from unit
C     *                              number NUOX ("oxi" file) for non-transient
C     *                              oxidants in GETCHEM4 (subroutine also changed).
C     * NOV 19, 2012 - K.VONSALZEN.  New version for gcm17+:
C     *                              CLEAN-UP AND MERGING WITH PLA CODE.
C     * MAY 10, 2012 - K.VONSALZEN/  Previous version GETEMIP for gcm16:
C     *                Y.PENG/       - REVISED CALL TO NEW GETCHEM4.
C     *                M.LAZARE.     - Rationalize use of MDAYT.
C     *                                This is now calculated outside
C     *                                of this common deck and
C     *                                used in new versions: {GETCHEM4,
C     *                                GETCAC2,GETWILDF2,GETWFH2,GETACH2}
C     *                                where MDAYT is no longer calculated.
C     * APR 27, 2010 - K.VONSALZEN. PREVIOUS VERSION GETEMI FOR GCM15I.
C     * INPUT OF TRACER EMISSIONS.
C--------------------------------------------------------    
C     * 3D ATMOSPHERIC CHEMICAL FIELDS.
C
      CALL GETCHEM4(DMSOPAK,DMSOPAL,EDMSPAK,EDMSPAL,
     1              SUZ0PAK,SUZ0PAL,PDSFPAK,PDSFPAL,
     1                OHPAK,  OHPAL,
     2              H2O2PAK,H2O2PAL,  O3PAK,  O3PAL,
     3               NO3PAK, NO3PAL,HNO3PAK,HNO3PAL,
     4               NH3PAK, NH3PAL, NH4PAK, NH4PAL,
     5              LON1,NLAT,LEVOX,INCD,IDAY,MDAYT,MDAYT1,
     6              KOUNT,IJPAK,NUCH,NUOX,LX,GLL)
C
C     * SECONDARY ORGANIC AEROSOL FROM VOA PRODUCTION.
C
      CALL GETCAC2 (EOSTPAK,EOSTPAL,
     1              LON1,NLAT,INCD,IDAY,MDAYT,MDAYT1,
     2              KOUNT,IJPAK,NUCH,GLL)
#if defined transient_aerosol_emissions
C
C     * 3D WILD FIRE EMISSION FACTORS.
C
      CALL GETWFH2(FBBCPAK,FBBCPAL,LON1,NLAT,LEVWF,INCD,IDAY,
     1             MDAYT,MDAYT1,MON,MON1,KOUNT,IJPAK,NUCH,LF,GLL)
C
C     * 3D AIRCRAFT EMISSION FACTORS.
C
      CALL GETACH2(FAIRPAK,FAIRPAL,LON1,NLAT,LEVAIR,INCD,IDAY,
     1             MDAYT,MDAYT1,MON,MON1,KOUNT,IJPAK,NUCH,LAIR,GLL)
#if defined emists
C
C     * FUTURE, ANTHROPOGENIC AND ANTHROPOGENICALLY-INFLUENCED 
C     * EMISSIONS.
C
      CALL GETTEMI2(SAIRPAK,SSFCPAK,SBIOPAK,SSHIPAK,SSTKPAK,SFIRPAK,
     1              SAIRPAL,SSFCPAL,SBIOPAL,SSHIPAL,SSTKPAL,SFIRPAL,
     2              OAIRPAK,OSFCPAK,OBIOPAK,OSHIPAK,OSTKPAK,OFIRPAK,
     3              OAIRPAL,OSFCPAL,OBIOPAL,OSHIPAL,OSTKPAL,OFIRPAL,
     4              BAIRPAK,BSFCPAK,BBIOPAK,BSHIPAK,BSTKPAK,BFIRPAK,
     5              BAIRPAL,BSFCPAL,BBIOPAL,BSHIPAL,BSTKPAL,BFIRPAL,
     6              LON1,NLAT,INCD,IREFYRA,IDAY,KOUNT,IJPAK,NUTR,GLL)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
