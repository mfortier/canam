#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * Apr 20/2019 - S.Kharin    - Fix bug IOFFZ->IOFF in *_EXT_GCM_SA_* fields
C     * Mar 16/2019 - J. Cole     - Add  3-hour 2D fields (CF3hr,E3hr)
C     * Nov 29/2018 - J. Cole     - Add/adjust tendencies
C     * Nov 02/2018 - J. Cole     - Add radiative flux profiles
C     * Nov 03/2018 - M.Lazare.   - Added GSNO and FNLA.
C     *                           - Added 3HR save fields.
C     * Oct 01/2018 - S.Kharin.   - Add XTVIPAS/XTVIROS for sampled burdens.
C     * Aug 14/2018 - M.Lazare.     Remove MASKPAK,GTPAL.
C     * Aug 14/2018 - M.Lazare.     Remove {GTM,SICP,SICM,SICNP,SICM}.
C     * Aug 01/2018 - M.Lazare.     Remove {OBEG,OBWG,RES,GC}.
C     * Jul 30/2018 - M.Lazare.   - Unused FMIROW removed.
C     *                           - Removed non-transient aerosol emission cpp blocks.
C     * Mar 09/2018 - M.Lazare.   - BEG  and BWG  changed from PAK/ROW to
C     *                             PAL/ROL.
C     * Feb 27/2018 - M.Lazare.   - QFSL and BEGL changed from PAK/ROW to
C     *                             PAL/ROL.
C     *                           - Added {BEGK,BWGK,BWGL,QFSO}.
C     * Feb 06/2018 - M.Lazare.    - Add {FTOX,FTOY} and {TDOX,TDOY}.
C     * Nov 01/2017 - J. Cole.     - Update for CMIP6 stratospheric aerosols
C     * Aug 09/2017 - M.Lazare.    - Add FNPAT/FNROT.
C     *                            - FLAK->{FLKR,FLKU}.
C     *                            - LICN->GICN.
C     *                            - Add lakes arrays.
C     * Aug 07/2017 - M.Lazare.    - Initial Git verison.
C     * Mar 26/2015 - M.Lazare.    Final version for gcm18:
C     *                            - Unused {CSTROL,CLTROL} removed.
C     *                            - Add (for NEMO/CICE support):
C     *                              HSEAROL,RAINSROL,SNOWSROL
C     *                              OBEGROL,OBWGROL,BEGOROL,BWGOROL,
C     *                              BEGIROL,HFLIROL,
C     *                              HSEAROL,RAINSROL,SNOWSROL.
C     *                            - Remove: FTILPAK/FTILROW.
C     *                            - Add (for harmonization of field
C     *                              capacity and wilting point between
C     *                              CTEM and CLASS):
C     *                              THLWPAT/THLWROT.
C     *                            - Add (for soil colour index look-up
C     *                              for land surface albedo):
C     *                              ALGDVPAT/ALGDVROT, ALGDNPAT/ALGDNROT,
C     *                              ALGWVPAT/ALGWVROT, ALGWNPAT/ALGWNROT,
C     *                              SOCIPAT/SOCIROT.
C     *                            - Add (for fractional land/water/ice):
C     *                              SALBPAT/SALBROT, CSALPAT/CSALROT,
C     *                              EMISPAK/EMISROW, EMISPAT/EMISROT,
C     *                              WRKAPAL/WRKAROL, WRKBPAL/WRKBROL,
C     *                              SNOPAKO/SNOROWO.
C     *                            - Add (for PLA):
C     *                              PSVVPAK/PSVVROW, PDEVPAK/PDEVROW,
C     *                              PDIVPAK/PDIVROW, PREVPAK/PREVROW,
C     *                              PRIVPAK/PRIVROW.
C     *                            - Bugfixes for ISCCP fields and additions
C     *                              for CALIPSO and MODIS.
C     * Feb 04/2014 - M.Lazare.    Interim version for gcm18:
C     *                            - FLAKPAK,FLNDPAK,GTPAT,LICNPAK added.
C     *                            - Duplicate calls for {EDSO,EDSO,ESVC,ESVE}
C     *                              removed.
C     * Nov 19/2013 - M.Lazare.    Cosmetic: Remove {GFLX,GA,HBL,ILMO,PET,UE,
C     *                                      WTAB,ROFS,ROFB) "PAT"/"ROT" arrays.
C     * Jul 10/2013 - M.Lazare/    Previous version pack9 for gcm17:
C     *               K.Vonsalzen/ - FSF added as prognostic field
C     *               J.Cole/        rather than residual.
C     *                            - Extra diagnostic microphysics
C     *                              fields added:
C     *                              {SEDI,RLIQ,RICE,RLNC,CLIQ,CICE,
C     *                               VSEDI,RELIQ,REICE,CLDLIQ,CLDICE,CTLNC,CLLNC}.
C     *                            - New emission fields:
C     *                              {SBIO,SSHI,OBIO,OSHI,BBIO,BSHI} and
C     *                              required altitude field ALTI.
C     *                            - Many new aerosol diagnostic fields
C     *                              for PAM, including those for cpp options:
C     *                              "pfrc", "xtrapla1" and "xtrapla2".
C     *                            - The following fields were removed
C     *                              from the "aodpth" cpp option:
C     *                              {SAB1,SAB2,SAB3,SAB4,SAB5,SABT} and
C     *                              {SAS1,SAS2,SAS3,SAS4,SAS5,SAST},
C     *                              (both PAK and PAL).
C     *                            - Due to the implementation of the mosaic
C     *                              for CLASS_v3.6, prognostic fields
C     *                              were changed from PAK/ROW to PAT/ROT,
C     *                              with an extra "IM" dimension (for
C     *                              the number of mosaic tiles).
C     *                            - The instantaneous band-mean values for
C     *                              solar fields are now prognostic
C     *                              as well: {FSDB,FSFB,CSDB,CSFB,FSSB,FSSCB}.
C     *                            - Removed "histemi" fields.
C     *                            - "xtraconv" cpp option moved to before
C     *                              "xtrachem", to be consistent with others.
C     * NB: The following are intermediate revisions to frozen gcm16 code
C     *     (upwardly compatible) to support the new CLASS version in development:
C     * Oct 18/2011 - M.Lazare.    - Add THR,THM,BI,PSIS,GRKS,THRA,HCPS,TCS,THFC,
C     *                              PSIW,ALGD,ALGW,ZBTW,ISND,IGDR but only for
C     *                              KOUNT.GT.KSTART.
C     * Oct 07/2011 - M.Lazare.    - Add GFLX,GA,HBL,PET,ILMO,ROFB,ROFS,UE,WTAB.
C     * Jul 13/2011 - E.Chan.      - Add FARE, THLQ, THIC. Change selected
C     *                            - PAK variables to packed tile versions
C     *                              (PAT). Add selected PAT variables in
C     *                              addition to their PAK counterparts.
C     *                            - Add TPND, ZPND, TAV, QAV, WSNO, TSFS.
C     * MAY 03/2012 - M.LAZARE/    PREVIOUS VERSION PACK8 FOR GCM16:
C     *               K.VONSALZEN/ - MODIFY FIELDS TO SUPPORT A NEWER
C     *               J.COLE/        VERSION OF COSP WHICH INCLUDES
C     *               Y.PENG.        THE MODIS, MISR AND CERES AND
C     *                              SAVE THE APPROPRIATE OUTPUT.
C     *                            - REMOVE {FSA,FLA,FSTC,FLTC} AND
C     *                              REPLACE BY {FSR,OLR,FSRC,OLRC}.
C     *                            - NEW CONDITIONAL DIAGNOSTIC OUTPUT
C     *                              UNDER CONTROL OF "XTRADUST" AND
C     *                              "AODPTH".
C     *                            - ADDITIONAL MAM RADIATION OUTPUT
C     *                              FIELDS {FSAN,FLAN}.
C     *                            - ADDITIONAL CORRELATED-K OUTPUT
C     *                              FIELDS: "FLG", "FDLC" AND "FLAM".
C     *                            - "FLGROL_R" IS ACTUAL CALCULATED
C     *                              RADIATIVE FORCING TERM INSTEAD
C     *                              OF "FDLROL_R-SIGMA*T**4".
C     *                            - INCLUDE "ISEED" FOR RANDOM NUMBER
C     *                              GENERATOR SEED NOW CALCULATED
C     *                              IN MODEL DRIVER INSTEAD OF PHYSICS.
C     * MAY 02/2010 - M.LAZARE/    PREVIOUS VERSION PACK7I FOR GCM15I:
C     *               K.VONSALZEN/ - ADD FSOPAL/FSOROL ("FSOL").
C     *               J.COLE.      - ADD FIELDS {RMIX,SMIX,RREF,SREF}
C     *                              FOR COSP INPUT, MANY FIELDS FOR
C     *                              COSP OUTPUT (WITH DIFFERENT OPTIONS)
C     *                              AND REMOVE PREVIOUS DIRECT ISCCP
C     *                              FIELDS.
C     *                            - ADD NEW DIAGNOSTIC FIELDS:
C     *                              SWA (PAK/ROW AND PAL/ROL),SWX,SWXU,
C     *                              SWXV,SRH,SRHN,SRHX,FSDC,FSSC,FDLC
C     *                              AND REMOVE: SWMX.
C     *                            - FOR CONVECTION, ADD: DMCD,DMCU AND
C     *                              REMOVE: ACMT,DCMT,SCMT,PCPS,CLDS,
C     *                              LHRD,LHRS,SHRD,SHRS.
C     *                            - ADD FIELDS FOR CTEM UNDER CONTROL
C     *                              OF NEW CPP DIRECTIVE: "COUPLER_CTEM".
C     *                            - ADD NEW FIELDS FOR COUPLER: OFSG,
C     *                              PHIS,PMSL,XSRF.
C     *                            - PREVIOUS UPDATE DIRECIVE "HISTEMI"
C     *                              CONVERTED TO CPP:
C     *                              "TRANSIENT_AEROSOL_EMISSIONS" WITH
C     *                              FURTHER CHOICE OF CPP DIRECTIVES
C     *                              "HISTEMI" FOR HISTORICAL OR
C     *                              "EMISTS" FOR FUTURE EMISSIONS.
C     *                              THE "HISTEMI" FIELDS ARE THE
C     *                              SAME AS PREVIOUSLY, EXCEPT THAT
C     *                              ??? HAS BEEN REMOVED. FOR "EMISTS",
C     *                              THE FOLLOWING ARE ADDED (BOTH PAK/ROW
C     *                              AND PAL/ROL SINCE INTERPOLATED):
C     *                              SAIR,SSFC,SSTK,SFIR,OAIR,OSFC,OSTK,OFIR,
C     *                              BAIR,BSFC,BSTK,BFIR. THE FIRST LETTER
C     *                              INDICATES THE SPECIES ("B" FOR BLACK
C     *                              CARBON, "O" FOR ORGANIC CARBON AND
C     *                              "S" FOR SULFUR), WHILE FOR EACH OF
C     *                              THESE, THERE ARE "AIR" FOR AIRCRAFT,
C     *                              "SFC" FOR SURFACE, "STK" FOR STACK
C     *                              AND "FIR" FOR FIRE, EACH HAVING
C     *                              DIFFERENT EMISSION HEIGHTS.
C     *                            - FOR THE CHEMISTRY, THE FOLLOWING
C     *                              FIELDS HAVE BEEN ADDED:
C     *                              DDD,DDB,DDO,DDS,WDLD,WDLB,WDLO,WDLS,
C     *                              WDDD,WDDB,WDDO,WDDS,WDSD,WDSB,WDSO,
C     *                              WDSS,ESD,ESFS,EAIS,ESTS,EFIS,ESFB,
C     *                              EAIB,ESTB,EFIB,ESFO,EAIO,ESTO,EFIO
C     *                              AND THE FOLLOWING HAVE BEEN REMOVED:
C     *                              ASFS,ASHP,ASO3,AWDS,DAFX,DCFX,DDA,DDC,
C     *                              ESBT,ESFF,EOFF,EBFF,EOBB,EBBB,ESWF,
C     *                              SFD,SFS,WDDA,WDDC,WDLA,WDLC,WDSA,WDSC,
C     *                              CDPH,CLPH,CSPH.
C     *                            - FAIRPAK/FAIRROW AND FAIRPAL/FAIRROL
C     *                              ADDED FOR AIRCRAFT EMISSIONS
C     *                            - O3CPAK/O3CROW AND O3CPAL/O3CROL
C     *                              ADDED FOR CHEMICAL OZONE INTERPOLATION.
C     *                            - DON'T PACK CLASS INPUT FIELDS
C     *                              (FCAN,ALIC,ALVC,CMAS,LNZO,ROOT)
C     *                              SINCE ARE INVARIANT.
C     *                            - UPDATE DIRECTIVES TURNED INTO
C     *                              CPP DIRECTIVES.
C     * FEB 19/2009 - M.LAZARE.    PREVIOUS VERSION PACK7H FOR GCM15H:
C     *                            - ADD NEW FIELDS FOR EMISSIONS: EOFF,
C     *                              EBFF,EOBB,EBBB.
C     *                            - ADD NEW FIELDS FOR DIAGNOSTICS OF
C     *                              CONSERVATION: QTPT,XTPT.
C     *                            - ADD NEW FIELD FOR CHEMISTRY: SFRC.
C     *                            - ADD NEW DIAGNOSTIC CLOUD FIELDS
C     *                              (USING OPTICAL DEPTH CUTOFF):
C     *                              CLDO (BOTH PAK AND PAL).
C     *                            - REORGANIZE EMISSION FORCING FIELDS
C     *                              DEPENDANT WHETHER ARE UNDER
C     *                              HISTORICAL EMISSIONS (%DF HISTEMI)
C     *                              OR NOT.
C     *                            - ADD FIELDS FOR EXPLOSIVE VOLCANOES:
C     *                              VTAU AND TROP UNDER CONTROL OF
C     *                              "%DF EXPLVOL". EACH HAVE BOTH PAK
C     *                              AND PAL.
C     * APR 21/2008 - L.SOLHEIM/   PREVIOUS VERSION PACK7G FOR GCM15G:
C     *               M.LAZARE/    -  ADD NEW RADIATIVE FORCING ARRAYS
C     *               K.VONSALZEN/    (UNDER CONTROL OF "%DF RADFORCE").
C     *               X.MA.        - NEW DIAGNOSTIC FIELDS: WDD4,WDS4,EDSL,
C     *                              ESBF,ESFF,ESVC,ESVE,ESWF ALONG WITH
C     *                              TDEM->EDSO (UNDER CONTROL OF
C     *                              "XTRACHEM"), AS WELL AS ALMX,ALMC
C     *                              AND INSTANTANEOUS CLWT,CIDT.
C     *                            - NEW AEROCOM FORCING FIELDS:
C     *                              EBWA,EOWA,ESWA,EOST (EACH WITH ACCUMULATED
C     *                              AND TARGET),ESCV,EHCV,ESEV,EHEV,
C     *                              EBBT,EOBT,EBFT,EOFT,ESDT,ESIT,ESST,
C     *                              ESOT,ESPT,ESRT.
C     *                            - CODE RELATED TO S/L REMOVED.
C     *                            - REMOVE UNUSED: QTPN,UTPM,VTPM,TSEM,EBC,
C     *                              EOC,EOCF,ESO2,EVOL,HVOL.
C     * JAN 11/2006 - J.COLE/      PREVIOUS VERSION PACK7F FOR GCM15F:
C     *               M.LAZARE.    - ADD ISCCP SIMULATOR FIELDS FROM JASON.
C     * NOV 26/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - TWO EXTRA NEW FIELDS ("DMC" AND "SMC")
C     *                              UNDER CONTROL OF "%IF DEF,XTRACONV".
C     * MAY 07/2006 - M.LAZARE.    PREVIOUS VERSION FOR PACK7E FOR GCM15E.
C========================================================================
C     * GENERAL PHYSICS ARRAYS.
C
      DO L=1,ILEV
        DO I=IL1,IL2
          ALMCPAK(IOFF+I,L)  = ALMCROW(I,L)
          ALMXPAK(IOFF+I,L)  = ALMXROW(I,L)
          CICPAK (IOFF+I,L)  = CICROW (I,L)
          CLCVPAK(IOFF+I,L)  = CLCVROW(I,L)
          CLDPAK (IOFF+I,L)  = CLDROW (I,L)
          CLWPAK (IOFF+I,L)  = CLWROW (I,L)
          CVARPAK(IOFF+I,L)  = CVARROW(I,L)
          CVMCPAK(IOFF+I,L)  = CVMCROW(I,L)
          CVSGPAK(IOFF+I,L)  = CVSGROW(I,L)
          FTOXPAK(IOFF+I,L)  = FTOXROW(I,L)
          FTOYPAK(IOFF+I,L)  = FTOYROW(I,L)
          HRLPAK (IOFF+I,L)  = HRLROW (I,L)
          HRSPAK (IOFF+I,L)  = HRSROW (I,L)
          HRLCPAK (IOFF+I,L) = HRLCROW (I,L)
          HRSCPAK (IOFF+I,L) = HRSCROW (I,L)
          OMETPAK(IOFF+I,L)  = OMETROW(I,L)
          QWF0PAL(IOFF+I,L)  = QWF0ROL(I,L)
          QWFMPAL(IOFF+I,L)  = QWFMROL(I,L)
          RHPAK  (IOFF+I,L)  = RHROW  (I,L)
          SCDNPAK(IOFF+I,L)  = SCDNROW(I,L)
          SCLFPAK(IOFF+I,L)  = SCLFROW(I,L)
          SLWCPAK(IOFF+I,L)  = SLWCROW(I,L)
          TACNPAK(IOFF+I,L)  = TACNROW(I,L)
          TDOXPAK(IOFF+I,L)  = TDOXROW(I,L)
          TDOYPAK(IOFF+I,L)  = TDOYROW(I,L)
          ZDETPAK(IOFF+I,L)  = ZDETROW(I,L)
        ENDDO
      ENDDO
C
      DO N=1,NTRAC
        DO L=1,ILEV
          DO I=IL1,IL2
            SFRCPAL (IOFF+I,L,N) = SFRCROL (I,L,N)
          ENDDO
        ENDDO
      ENDDO
      DO L=1,ILEV
      DO I=IL1,IL2
        ALTIPAK(IOFF+I,L)   = ALTIROW(I,L)
      ENDDO
      ENDDO
C
      DO L=1,LEVOZ
        DO I=IL1,IL2
          OZPAK  (IOFF+I,L) = OZROW  (I,L)
          OZPAL  (IOFF+I,L) = OZROL  (I,L)
        ENDDO
      ENDDO
C
      DO L=1,LEVOZC
        DO I=IL1,IL2
          O3CPAK (IOFF+I,L) = O3CROW (I,L)
          O3CPAL (IOFF+I,L) = O3CROL (I,L)
        ENDDO
      ENDDO

C
      CSALPAL(IOFF+IL1:IOFF+IL2,1:NBS)     =CSALROL(IL1:IL2,1:NBS)
      CSALPAT(IOFF+IL1:IOFF+IL2,1:IM,1:NBS)=CSALROT(IL1:IL2,1:IM,1:NBS)
      SALBPAL(IOFF+IL1:IOFF+IL2,1:NBS)     =SALBROL(IL1:IL2,1:NBS)
      SALBPAT(IOFF+IL1:IOFF+IL2,1:IM,1:NBS)=SALBROT(IL1:IL2,1:IM,1:NBS)
      CSDPAL(IOFF+IL1:IOFF+IL2) = CSDROL(IL1:IL2)
      CSFPAL(IOFF+IL1:IOFF+IL2) = CSFROL(IL1:IL2)
C
      DO I=IL1,IL2
        ALPHPAK(IOFF+I) = ALPHROW(I)
        BEGIPAL(IOFF+I) = BEGIROL(I)
        BEGKPAL(IOFF+I) = BEGKROL(I)
        BEGLPAL(IOFF+I) = BEGLROL(I)
        BEGOPAL(IOFF+I) = BEGOROL(I)
        BEGPAL (IOFF+I) = BEGROL (I)
        BWGIPAL(IOFF+I) = BWGIROL(I)
        BWGKPAL(IOFF+I) = BWGKROL(I)
        BWGLPAL(IOFF+I) = BWGLROL(I)
        BWGOPAL(IOFF+I) = BWGOROL(I)
        BWGPAL (IOFF+I) = BWGROL (I)
        CBMFPAL(IOFF+I) = CBMFROL(I)
        CHFXPAK(IOFF+I) = CHFXROW(I)
        CICTPAK(IOFF+I) = CICTROW(I)
        CLBPAL (IOFF+I) = CLBROL (I)
        CLDTPAK(IOFF+I) = CLDTROW(I)
        CLDOPAK(IOFF+I) = CLDOROW(I)
        CLDOPAL(IOFF+I) = CLDOROL(I)
        CLDAPAK(IOFF+I) = CLDAROW(I)
        CLDAPAL(IOFF+I) = CLDAROL(I)
        CLWTPAK(IOFF+I) = CLWTROW(I)
        CQFXPAK(IOFF+I) = CQFXROW(I)
        CSBPAL (IOFF+I) = CSBROL (I)
        CSZPAL (IOFF+I) = CSZROL (I)
        DELTPAK(IOFF+I) = DELTROW(I)
        DMSOPAK(IOFF+I) = DMSOROW(I)
        DMSOPAL(IOFF+I) = DMSOROL(I)
        EDMSPAK(IOFF+I) = EDMSROW(I)
        EDMSPAL(IOFF+I) = EDMSROL(I)
        EMISPAK(IOFF+I) = EMISROW(I)
        ENVPAK (IOFF+I) = ENVROW (I)
        FDLPAK (IOFF+I) = FDLROW (I)
        FDLCPAK(IOFF+I) = FDLCROW(I)
        FLAMPAL(IOFF+I) = FLAMROL(I)
        FLAMPAK(IOFF+I) = FLAMROW(I)
        FLANPAK(IOFF+I) = FLANROW(I)
        FLANPAL(IOFF+I) = FLANROL(I)
        FLGCPAK(IOFF+I) = FLGCROW(I)
        FLGPAK (IOFF+I) = FLGROW (I)
        FSAMPAK(IOFF+I) = FSAMROW(I)
        FSAMPAL(IOFF+I) = FSAMROL(I)
        FSANPAK(IOFF+I) = FSANROW(I)
        FSANPAL(IOFF+I) = FSANROL(I)
        FSDCPAK(IOFF+I) = FSDCROW(I)
        FSDPAK (IOFF+I) = FSDROW (I)
        FSDPAL (IOFF+I) = FSDROL (I)
        FSFPAL (IOFF+I) = FSFROL (I)
        FSGCPAK(IOFF+I) = FSGCROW(I)
        FSGPAK (IOFF+I) = FSGROW (I)
        FSGIPAL(IOFF+I) = FSGIROL(I)
        FSGOPAL(IOFF+I) = FSGOROL(I)
        FSIPAL (IOFF+I) = FSIROL (I)
        FSLOPAK(IOFF+I) = FSLOROW(I)
        FSLOPAL(IOFF+I) = FSLOROL(I)
        FSOPAK (IOFF+I) = FSOROW (I)
        FSRCPAK(IOFF+I) = FSRCROW(I)
        FSRPAK (IOFF+I) = FSRROW (I)
        FSSCPAK(IOFF+I) = FSSCROW(I)
        FSSPAK (IOFF+I) = FSSROW (I)
        FSVPAK (IOFF+I) = FSVROW (I)
        FSVPAL (IOFF+I) = FSVROL (I)
        GAMPAK (IOFF+I) = GAMROW (I)
        GICNPAK(IOFF+I) = GICNROW(I)
        GTAPAK (IOFF+I) = GTAROW (I)
        GTPAK  (IOFF+I) = GTROW  (I)
        HFLPAK (IOFF+I) = HFLROW (I)
        HFSPAK (IOFF+I) = HFSROW (I)
        HSEAPAL(IOFF+I) = HSEAROL(I)
        OFSGPAL(IOFF+I) = OFSGROL(I)
        OLRPAK (IOFF+I) = OLRROW (I)
        OLRCPAK(IOFF+I) = OLRCROW(I)
        PARPAL (IOFF+I) = PARROL (I)
        PBLHPAK(IOFF+I) = PBLHROW(I)
        PBLTPAK(IOFF+I) = PBLTROW(I)
        PCHFPAK(IOFF+I) = PCHFROW(I)
        PCPCPAK(IOFF+I) = PCPCROW(I)
        PCPPAK (IOFF+I) = PCPROW (I)
        PCPSPAK(IOFF+I) = PCPSROW(I)
        PLHFPAK(IOFF+I) = PLHFROW(I)
        PMSLPAL(IOFF+I) = PMSLROL(I)
        PSAPAK (IOFF+I) = PSAROW (I)
        PSHFPAK(IOFF+I) = PSHFROW(I)
        PSIPAK (IOFF+I) = PSIROW (I)
        PWATPAK(IOFF+I) = PWATROW(I)
        PWATPAM(IOFF+I) = PWATROM(I)
        QFSPAK (IOFF+I) = QFSROW (I)
        QFSLPAL(IOFF+I) = QFSLROL(I)
        QFSOPAL(IOFF+I) = QFSOROL(I)
        QFXPAK (IOFF+I) = QFXROW (I)
        QTPFPAM(IOFF+I) = QTPFROM(I)
        QTPHPAM(IOFF+I) = QTPHROM(I)
        QTPTPAM(IOFF+I) = QTPTROM(I)
        RAINSPAL(IOFF+I)= RAINSROL(I)
        SICNPAK(IOFF+I) = SICNROW(I)
        SICPAK (IOFF+I) = SICROW (I)
        SIGXPAK(IOFF+I) = SIGXROW(I)
        SLIMPAL(IOFF+I) = SLIMROL(I)
        SLIMPlw(IOFF+I) = SLIMRlw(I)
        SLIMPsh(IOFF+I) = SLIMRsh(I)
        SLIMPlh(IOFF+I) = SLIMRlh(I)
        SNOPAK (IOFF+I) = SNOROW (I)
        SNOWSPAL(IOFF+I)= SNOWSROL(I)
        SQPAK  (IOFF+I) = SQROW  (I)
        SRHPAK (IOFF+I) = SRHROW (I)
        SRHNPAK(IOFF+I) = SRHNROW(I)
        SRHXPAK(IOFF+I) = SRHXROW(I)
        STMNPAK(IOFF+I) = STMNROW(I)
        STMXPAK(IOFF+I) = STMXROW(I)
        STPAK  (IOFF+I) = STROW  (I)
        SUPAK  (IOFF+I) = SUROW  (I)
        SVPAK  (IOFF+I) = SVROW  (I)
        SWAPAK (IOFF+I) = SWAROW (I)
        SWAPAL (IOFF+I) = SWAROL (I)
        SWXPAK (IOFF+I) = SWXROW (I)
        SWXUPAK(IOFF+I) = SWXUROW(I)
        SWXVPAK(IOFF+I) = SWXVROW(I)
        TCVPAK (IOFF+I) = TCVROW (I)
        TFXPAK (IOFF+I) = TFXROW (I)
        UFSPAK (IOFF+I) = UFSROW (I)
        UFSPAL (IOFF+I) = UFSROL (I)
        UFSIPAL(IOFF+I) = UFSIROL(I)
        UFSOPAL(IOFF+I) = UFSOROL(I)
        VFSPAK (IOFF+I) = VFSROW (I)
        VFSPAL (IOFF+I) = VFSROL (I)
        VFSIPAL(IOFF+I) = VFSIROL(I)
        VFSOPAL(IOFF+I) = VFSOROL(I)
      ENDDO
      SNOPAT(IOFF+IL1:IOFF+IL2,:) = SNOROT(IL1:IL2,:)
      GTPAT (IOFF+IL1:IOFF+IL2,:) = GTROT (IL1:IL2,:)
      GTPAX (IOFF+IL1:IOFF+IL2,:) = GTROX (IL1:IL2,:)
      EMISPAT(IOFF+IL1:IOFF+IL2,:) = EMISROT(IL1:IL2,:)
      XLMPAT(IOFF+IL1:IOFF+IL2,:,:)=XLMROT(IL1:IL2,:,:)
C
      DO L = 1, NBS
         DO I = IL1, IL2
            FSDBPAL (IOFF+I,L)  = FSDBROL (I,L)
            FSFBPAL (IOFF+I,L)  = FSFBROL (I,L)
            CSDBPAL (IOFF+I,L)  = CSDBROL (I,L)
            CSFBPAL (IOFF+I,L)  = CSFBROL (I,L)
            FSSBPAL (IOFF+I,L)  = FSSBROL (I,L)
            FSSCBPAL (IOFF+I,L) = FSSCBROL (I,L)
            WRKAPAL (IOFF+I,L)  = WRKAROL (I,L)
            WRKBPAL (IOFF+I,L)  = WRKBROL (I,L)
         END DO
      END DO
      CLBPAT (IOFF+IL1:IOFF+IL2,:)=CLBROT (IL1:IL2,:)
      CSBPAT (IOFF+IL1:IOFF+IL2,:)=CSBROT (IL1:IL2,:)
      CSDPAT (IOFF+IL1:IOFF+IL2,:)=CSDROT (IL1:IL2,:)
      CSFPAT (IOFF+IL1:IOFF+IL2,:)=CSFROT (IL1:IL2,:)
      FDLPAT (IOFF+IL1:IOFF+IL2,:)=FDLROT (IL1:IL2,:)
      FDLCPAT(IOFF+IL1:IOFF+IL2,:)=FDLCROT(IL1:IL2,:)
      FLGPAT (IOFF+IL1:IOFF+IL2,:)=FLGROT (IL1:IL2,:)
      FSDPAT (IOFF+IL1:IOFF+IL2,:)=FSDROT (IL1:IL2,:)
      FSFPAT (IOFF+IL1:IOFF+IL2,:)=FSFROT (IL1:IL2,:)
      FSGPAT (IOFF+IL1:IOFF+IL2,:)=FSGROT (IL1:IL2,:)
      FSIPAT (IOFF+IL1:IOFF+IL2,:)=FSIROT (IL1:IL2,:)
      FSVPAT (IOFF+IL1:IOFF+IL2,:)=FSVROT (IL1:IL2,:)
      PARPAT (IOFF+IL1:IOFF+IL2,:)=PARROT (IL1:IL2,:)
C
      DO L = 1, NBS
      DO M = 1, IM
      DO I = IL1, IL2
        FSDBPAT (IOFF+I,M,L)  = FSDBROT (I,M,L)
        FSFBPAT (IOFF+I,M,L)  = FSFBROT (I,M,L)
        CSDBPAT (IOFF+I,M,L)  = CSDBROT (I,M,L)
        CSFBPAT (IOFF+I,M,L)  = CSFBROT (I,M,L)
        FSSBPAT (IOFF+I,M,L)  = FSSBROT (I,M,L)
        FSSCBPAT(IOFF+I,M,L)  = FSSCBROT(I,M,L)
      END DO
      END DO
      END DO
C
C     * LAND SURFACE ARRAYS.
C
      DO L=1,IGND
        DO I=IL1,IL2
          DZGPAK (IOFF+I,L) = DZGROW (I,L)
          HFCGPAK(IOFF+I,L) = HFCGROW(I,L)
          HMFGPAK(IOFF+I,L) = HMFGROW(I,L)
          PORGPAK(IOFF+I,L) = PORGROW(I,L)
          FCAPPAK(IOFF+I,L) = FCAPROW(I,L)
          QFVGPAK(IOFF+I,L) = QFVGROW(I,L)
          TGPAK  (IOFF+I,L) = TGROW  (I,L)
          WGFPAK (IOFF+I,L) = WGFROW (I,L)
          WGLPAK (IOFF+I,L) = WGLROW (I,L)
        ENDDO
      ENDDO
      GFLXPAK(IOFF+IL1:IOFF+IL2,:)  =GFLXROW(IL1:IL2,:)
      TGPAT  (IOFF+IL1:IOFF+IL2,:,:)=TGROT  (IL1:IL2,:,:)
      THICPAT(IOFF+IL1:IOFF+IL2,:,:)=THICROT(IL1:IL2,:,:)
      THLQPAT(IOFF+IL1:IOFF+IL2,:,:)=THLQROT(IL1:IL2,:,:)
C
C
      DO I=IL1,IL2
        ANPAK  (IOFF+I) = ANROW  (I)
        DRPAK  (IOFF+I) = DRROW  (I)
        FLGGPAK(IOFF+I) = FLGGROW(I)
        FLGNPAK(IOFF+I) = FLGNROW(I)
        FLGVPAK(IOFF+I) = FLGVROW(I)
        FNLAPAK(IOFF+I) = FNLAROW(I)
        FNPAK  (IOFF+I) = FNROW  (I)
        FSGGPAK(IOFF+I) = FSGGROW(I)
        FSGNPAK(IOFF+I) = FSGNROW(I)
        FSGVPAK(IOFF+I) = FSGVROW(I)
        FVGPAK (IOFF+I) = FVGROW (I)
        FVNPAK (IOFF+I) = FVNROW (I)
        GSNOPAK(IOFF+I) = GSNOROW(I)
        HFCNPAK(IOFF+I) = HFCNROW(I)
        HFCVPAK(IOFF+I) = HFCVROW(I)
        HFLGPAK(IOFF+I) = HFLGROW(I)
        HFLNPAK(IOFF+I) = HFLNROW(I)
        HFLVPAK(IOFF+I) = HFLVROW(I)
        HFSGPAK(IOFF+I) = HFSGROW(I)
        HFSNPAK(IOFF+I) = HFSNROW(I)
        HFSVPAK(IOFF+I) = HFSVROW(I)
        HMFNPAK(IOFF+I) = HMFNROW(I)
        HMFVPAK(IOFF+I) = HMFVROW(I)
        MVPAK  (IOFF+I) = MVROW  (I)
        PCPNPAK(IOFF+I) = PCPNROW(I)
        PIGPAK (IOFF+I) = PIGROW (I)
        PINPAK (IOFF+I) = PINROW (I)
        PIVFPAK(IOFF+I) = PIVFROW(I)
        PIVLPAK(IOFF+I) = PIVLROW(I)
        QFGPAK (IOFF+I) = QFGROW (I)
        QFNPAK (IOFF+I) = QFNROW (I)
        QFVFPAK(IOFF+I) = QFVFROW(I)
        QFVLPAK(IOFF+I) = QFVLROW(I)
        RHONPAK(IOFF+I) = RHONROW(I)
        ROFNPAK(IOFF+I) = ROFNROW(I)
        ROFOPAK(IOFF+I) = ROFOROW(I)
        ROFOPAL(IOFF+I) = ROFOROL(I)
        ROFPAK (IOFF+I) = ROFROW (I)
        ROFPAL (IOFF+I) = ROFROL (I)
        ROFVPAK(IOFF+I) = ROFVROW(I)
        ROVGPAK(IOFF+I) = ROVGROW(I)
        SKYGPAK(IOFF+I) = SKYGROW(I)
        SKYNPAK(IOFF+I) = SKYNROW(I)
        SMLTPAK(IOFF+I) = SMLTROW(I)
        TBASPAK(IOFF+I) = TBASROW(I)
        TNPAK  (IOFF+I) = TNROW  (I)
        TTPAK  (IOFF+I) = TTROW  (I)
        TVPAK  (IOFF+I) = TVROW  (I)
        WTRGPAK(IOFF+I) = WTRGROW(I)
        WTRNPAK(IOFF+I) = WTRNROW(I)
        WTRVPAK(IOFF+I) = WTRVROW(I)
        WVFPAK (IOFF+I) = WVFROW (I)
        WVLPAK (IOFF+I) = WVLROW (I)
        ZNPAK  (IOFF+I) = ZNROW  (I)
        GAPAK  (IOFF+I) = GAROW  (I)
        HBLPAK (IOFF+I) = HBLROW (I)
        ILMOPAK(IOFF+I) = ILMOROW(I)
        PETPAK (IOFF+I) = PETROW (I)
        ROFBPAK(IOFF+I) = ROFBROW(I)
        ROFSPAK(IOFF+I) = ROFSROW(I)
        UEPAK  (IOFF+I) = UEROW  (I)
        WSNOPAK(IOFF+I) = WSNOROW(I)
        WTABPAK(IOFF+I) = WTABROW(I)
        ZPNDPAK(IOFF+I) = ZPNDROW(I)
        REFPAK (IOFF+I) = REFROW (I)
        BCSNPAK(IOFF+I) = BCSNROW(I)
        DEPBPAK(IOFF+I) = DEPBROW(I)
      ENDDO
C
C     * ACCUMULATED 3H variables
C

      CLDT3HPAK(IOFF+IL1:IOFF+IL2)=CLDT3HROW(IL1:IL2)
      HFL3HPAK (IOFF+IL1:IOFF+IL2)=HFL3HROW (IL1:IL2)
      HFS3HPAK (IOFF+IL1:IOFF+IL2)=HFS3HROW (IL1:IL2)
      ROF3HPAL (IOFF+IL1:IOFF+IL2)=ROF3HROL (IL1:IL2)
      WGL3HPAK (IOFF+IL1:IOFF+IL2)=WGL3HROW (IL1:IL2)
      WGF3HPAK (IOFF+IL1:IOFF+IL2)=WGF3HROW (IL1:IL2)
      PCP3HPAK (IOFF+IL1:IOFF+IL2)=PCP3HROW (IL1:IL2)
      PCPC3HPAK(IOFF+IL1:IOFF+IL2)=PCPC3HROW(IL1:IL2)
      PCPL3HPAK(IOFF+IL1:IOFF+IL2)=PCPL3HROW(IL1:IL2)
      PCPS3HPAK(IOFF+IL1:IOFF+IL2)=PCPS3HROW(IL1:IL2)
      PCPN3HPAK(IOFF+IL1:IOFF+IL2)=PCPN3HROW(IL1:IL2)
      FDL3HPAK (IOFF+IL1:IOFF+IL2)=FDL3HROW (IL1:IL2)
      FDLC3HPAK(IOFF+IL1:IOFF+IL2)=FDLC3HROW(IL1:IL2)
      FLG3HPAK (IOFF+IL1:IOFF+IL2)=FLG3HROW (IL1:IL2)
      FSS3HPAK (IOFF+IL1:IOFF+IL2)=FSS3HROW (IL1:IL2)
      FSSC3HPAK(IOFF+IL1:IOFF+IL2)=FSSC3HROW(IL1:IL2)
      FSD3HPAK (IOFF+IL1:IOFF+IL2)=FSD3HROW (IL1:IL2)
      FSG3HPAK (IOFF+IL1:IOFF+IL2)=FSG3HROW (IL1:IL2)
      FSGC3HPAK(IOFF+IL1:IOFF+IL2)=FSGC3HROW(IL1:IL2)
      SQ3HPAK  (IOFF+IL1:IOFF+IL2)=SQ3HROW  (IL1:IL2)
      ST3HPAK  (IOFF+IL1:IOFF+IL2)=ST3HROW  (IL1:IL2)
      SU3HPAK  (IOFF+IL1:IOFF+IL2)=SU3HROW  (IL1:IL2)
      SV3HPAK  (IOFF+IL1:IOFF+IL2)=SV3HROW  (IL1:IL2)

      OLR3HPAK(IOFF+IL1:IOFF+IL2)  = OLR3HROW(IL1:IL2)
      FSR3HPAK(IOFF+IL1:IOFF+IL2)  = FSR3HROW(IL1:IL2)
      OLRC3HPAK(IOFF+IL1:IOFF+IL2) = OLRC3HROW(IL1:IL2)
      FSRC3HPAK(IOFF+IL1:IOFF+IL2) = FSRC3HROW(IL1:IL2)
      FSO3HPAK(IOFF+IL1:IOFF+IL2)  = FSO3HROW(IL1:IL2)
      PWAT3HPAK(IOFF+IL1:IOFF+IL2) = PWAT3HROW(IL1:IL2)
      CLWT3HPAK(IOFF+IL1:IOFF+IL2) = CLWT3HROW(IL1:IL2)
      CICT3HPAK(IOFF+IL1:IOFF+IL2) = CICT3HROW(IL1:IL2)
      PMSL3HPAK(IOFF+IL1:IOFF+IL2) = PMSL3HROW(IL1:IL2)

C
C  * INSTANTANEOUS VARIABLES FOR SAMPLING
C
      DO I=IL1,IL2
         SWPAL(IOFF+I)      = SWROL(I)
         SRHPAL(IOFF+I)     = SRHROL(I)
         PCPPAL(IOFF+I)     = PCPROL(I)
         PCPNPAL(IOFF+I)    = PCPNROL(I)
         PCPCPAL(IOFF+I)    = PCPCROL(I)
         QFSIPAL(IOFF+I)    = QFSIROL(I)
         QFNPAL(IOFF+I)     = QFNROL(I)
         QFVFPAL(IOFF+I)    = QFVFROL(I)
         UFSINSTPAL(IOFF+I) = UFSINSTROL(I)
         VFSINSTPAL(IOFF+I) = VFSINSTROL(I)
         HFLIPAL(IOFF+I)    = HFLIROL(I)
         HFSIPAL(IOFF+I)    = HFSIROL(I)
         FDLPAL(IOFF+I)     = FDLROL(I)
         FLGPAL(IOFF+I)     = FLGROL(I)
         FSSPAL(IOFF+I)     = FSSROL(I)
         FSGPAL(IOFF+I)     = FSGROL(I)
         FSSCPAL(IOFF+I)    = FSSCROL(I)
         FSGCPAL(IOFF+I)    = FSGCROL(I)
         FDLCPAL(IOFF+I)    = FDLCROL(I)
         FSOPAL(IOFF+I)     = FSOROL(I)
         FSRPAL(IOFF+I)     = FSRROL(I)
         OLRPAL(IOFF+I)     = OLRROL(I)
         OLRCPAL(IOFF+I)    = OLRCROL(I)
         FSRCPAL(IOFF+I)    = FSRCROL(I)
         PWATIPAL(IOFF+I)   = PWATIROL(I)
         CLDTPAL(IOFF+I)    = CLDTROL(I)
         CLWTPAL(IOFF+I)    = CLWTROL(I)
         CICTPAL(IOFF+I)    = CICTROL(I)
         BALTPAL(IOFF+I)    = BALTROL(I)
         PMSLIPAL(IOFF+I)   = PMSLIROL(I)
         CDCBPAL(IOFF+I)    = CDCBROL(I)
         CSCBPAL(IOFF+I)    = CSCBROL(I)
         GTPAL(IOFF+I)      = GTROL(I)
         TCDPAL(IOFF+I)     = TCDROL(I)
         BCDPAL(IOFF+I)     = BCDROL(I)
         PSPAL(IOFF+I)      = PSROL(I)
         STPAL(IOFF+I)      = STROL(I)
         SUPAL(IOFF+I)      = SUROL(I)
         SVPAL(IOFF+I)      = SVROL(I)
      END DO ! I

      DO L = 1, ILEV
         DO I = IL1, IL2
           DMCPAL(IOFF+I,L)  = DMCROL(I,L)
           TAPAL(IOFF+I,L)   = TAROL(I,L)
           UAPAL(IOFF+I,L)   = UAROL(I,L)
           VAPAL(IOFF+I,L)   = VAROL(I,L)
           QAPAL(IOFF+I,L)   = QAROL(I,L)
           RHPAL(IOFF+I,L)   = RHROL(I,L)
           ZGPAL(IOFF+I,L)   = ZGROL(I,L)
           RKHPAL(IOFF+I,L)  = RKHROL(I,L)
           RKMPAL(IOFF+I,L)  = RKMROL(I,L)
           RKQPAL(IOFF+I,L)  = RKQROL(I,L)

           TTPPAL(IOFF+I,L)  = TTPROL(I,L)
           TTPPPAL(IOFF+I,L) = TTPPROL(I,L)
           TTPVPAL(IOFF+I,L) = TTPVROL(I,L)
           TTPLPAL(IOFF+I,L) = TTPLROL(I,L)
           TTPSPAL(IOFF+I,L) = TTPSROL(I,L)
           TTLCPAL(IOFF+I,L) = TTLCROL(I,L)
           TTSCPAL(IOFF+I,L) = TTSCROL(I,L)
           TTPCPAL(IOFF+I,L) = TTPCROL(I,L)
           TTPMPAL(IOFF+I,L) = TTPMROL(I,L)

           QTPPAL(IOFF+I,L)  = QTPROL(I,L)
           QTPPPAL(IOFF+I,L) = QTPPROL(I,L)
           QTPVPAL(IOFF+I,L) = QTPVROL(I,L)
           QTPCPAL(IOFF+I,L) = QTPCROL(I,L)
           QTPMPAL(IOFF+I,L) = QTPMROL(I,L)
        END DO ! I
      END DO ! L
C
      ANPAT(IOFF+IL1:IOFF+IL2,:) = ANROT(IL1:IL2,:)
      DRNPAT(IOFF+IL1:IOFF+IL2,:) = DRNROT(IL1:IL2,:)
      FNPAT(IOFF+IL1:IOFF+IL2,:) = FNROT(IL1:IL2,:)
      MVPAT(IOFF+IL1:IOFF+IL2,:) = MVROT(IL1:IL2,:)
      QAVPAT(IOFF+IL1:IOFF+IL2,:) = QAVROT(IL1:IL2,:)
      RHONPAT(IOFF+IL1:IOFF+IL2,:) = RHONROT(IL1:IL2,:)
      TAVPAT(IOFF+IL1:IOFF+IL2,:) = TAVROT(IL1:IL2,:)
      TBASPAT(IOFF+IL1:IOFF+IL2,:) = TBASROT(IL1:IL2,:)
      TNPAT(IOFF+IL1:IOFF+IL2,:) = TNROT(IL1:IL2,:)
      TPNDPAT(IOFF+IL1:IOFF+IL2,:) = TPNDROT(IL1:IL2,:)
      TTPAT(IOFF+IL1:IOFF+IL2,:) = TTROT(IL1:IL2,:)
      TVPAT(IOFF+IL1:IOFF+IL2,:) = TVROT(IL1:IL2,:)
      WSNOPAT(IOFF+IL1:IOFF+IL2,:) = WSNOROT(IL1:IL2,:)
      WVFPAT(IOFF+IL1:IOFF+IL2,:) = WVFROT(IL1:IL2,:)
      WVLPAT(IOFF+IL1:IOFF+IL2,:) = WVLROT(IL1:IL2,:)
      ZPNDPAT(IOFF+IL1:IOFF+IL2,:) = ZPNDROT(IL1:IL2,:)
      REFPAT (IOFF+IL1:IOFF+IL2,:) = REFROT (IL1:IL2,:)
      BCSNPAT(IOFF+IL1:IOFF+IL2,:) = BCSNROT(IL1:IL2,:)
C
      FAREPAT(IOFF+IL1:IOFF+IL2,:) = FAREROT(IL1:IL2,:)
      TSFSPAT(IOFF+IL1:IOFF+IL2,:,:)=TSFSROT(IL1:IL2,:,:)
C
      IF(KOUNT.EQ.KSTART) THEN
        DLZWPAT(IOFF+IL1:IOFF+IL2,:,:)=DLZWROT(IL1:IL2,:,:)
        PORGPAT(IOFF+IL1:IOFF+IL2,:,:)=PORGROT(IL1:IL2,:,:)
        THFCPAT(IOFF+IL1:IOFF+IL2,:,:)=THFCROT(IL1:IL2,:,:)
        THLWPAT(IOFF+IL1:IOFF+IL2,:,:)=THLWROT(IL1:IL2,:,:)
        THRPAT (IOFF+IL1:IOFF+IL2,:,:)=THRROT (IL1:IL2,:,:)
        THMPAT (IOFF+IL1:IOFF+IL2,:,:)=THMROT (IL1:IL2,:,:)
        BIPAT  (IOFF+IL1:IOFF+IL2,:,:)=BIROT  (IL1:IL2,:,:)
        PSISPAT(IOFF+IL1:IOFF+IL2,:,:)=PSISROT(IL1:IL2,:,:)
        GRKSPAT(IOFF+IL1:IOFF+IL2,:,:)=GRKSROT(IL1:IL2,:,:)
        THRAPAT(IOFF+IL1:IOFF+IL2,:,:)=THRAROT(IL1:IL2,:,:)
        HCPSPAT(IOFF+IL1:IOFF+IL2,:,:)=HCPSROT(IL1:IL2,:,:)
        TCSPAT (IOFF+IL1:IOFF+IL2,:,:)=TCSROT (IL1:IL2,:,:)
        PSIWPAT(IOFF+IL1:IOFF+IL2,:,:)=PSIWROT(IL1:IL2,:,:)
        ZBTWPAT(IOFF+IL1:IOFF+IL2,:,:)=ZBTWROT(IL1:IL2,:,:)
        ISNDPAT(IOFF+IL1:IOFF+IL2,:,:)=ISNDROT(IL1:IL2,:,:)
C
        ALGDVPAT(IOFF+IL1:IOFF+IL2,:) =ALGDVROT(IL1:IL2,:)
        ALGDNPAT(IOFF+IL1:IOFF+IL2,:) =ALGDNROT(IL1:IL2,:)
        ALGWVPAT(IOFF+IL1:IOFF+IL2,:) =ALGWVROT(IL1:IL2,:)
        ALGWNPAT(IOFF+IL1:IOFF+IL2,:) =ALGWNROT(IL1:IL2,:)
        IGDRPAT(IOFF+IL1:IOFF+IL2,:)  =IGDRROT(IL1:IL2,:)
      ENDIF
C
C     * TKE fields.
C
      DO L=1,ILEV
      DO I=IL1,IL2
        TKEMPAK (IOFF+I,L) = TKEMROW (I,L)
        XLMPAK  (IOFF+I,L) = XLMROW  (I,L)
        SVARPAK (IOFF+I,L) = SVARROW (I,L)
      ENDDO
      ENDDO
#if defined (agcm_ctem)

      RMATCPAT  (IOFF+IL1:IOFF+IL2,:,:,:) = RMATCROT  (IL1:IL2,:,:,:)
      RTCTMPAT  (IOFF+IL1:IOFF+IL2,:,:,:) = RTCTMROT  (IL1:IL2,:,:,:)

      CFCANPAT  (IOFF+IL1:IOFF+IL2,:,:) = CFCANROT  (IL1:IL2,:,:)
      ZOLNCPAT  (IOFF+IL1:IOFF+IL2,:,:) = ZOLNCROT  (IL1:IL2,:,:)
      AILCPAT   (IOFF+IL1:IOFF+IL2,:,:) = AILCROT   (IL1:IL2,:,:)
      CMASCPAT  (IOFF+IL1:IOFF+IL2,:,:) = CMASCROT  (IL1:IL2,:,:)
      CALVCPAT  (IOFF+IL1:IOFF+IL2,:,:) = CALVCROT  (IL1:IL2,:,:)
      CALICPAT  (IOFF+IL1:IOFF+IL2,:,:) = CALICROT  (IL1:IL2,:,:)
      PAICPAT   (IOFF+IL1:IOFF+IL2,:,:) = PAICROT   (IL1:IL2,:,:)
      SLAICPAT  (IOFF+IL1:IOFF+IL2,:,:) = SLAICROT  (IL1:IL2,:,:)
      FCANCPAT  (IOFF+IL1:IOFF+IL2,:,:) = FCANCROT  (IL1:IL2,:,:)
      TODFCPAT  (IOFF+IL1:IOFF+IL2,:,:) = TODFCROT  (IL1:IL2,:,:)
      AILCGPAT  (IOFF+IL1:IOFF+IL2,:,:) = AILCGROT  (IL1:IL2,:,:)
      SLAIPAT   (IOFF+IL1:IOFF+IL2,:,:) = SLAIROT   (IL1:IL2,:,:)
      FSNOWPTL  (IOFF+IL1:IOFF+IL2,:)   = FSNOWRTL  (IL1:IL2,:)
      TCANOPTL  (IOFF+IL1:IOFF+IL2,:)   = TCANORTL  (IL1:IL2,:)
      TCANSPTL  (IOFF+IL1:IOFF+IL2,:)   = TCANSRTL  (IL1:IL2,:)
      TAPTL     (IOFF+IL1:IOFF+IL2,:)   = TARTL     (IL1:IL2,:)
      CFLUXCSPAT(IOFF+IL1:IOFF+IL2,:)   = CFLUXCSROT(IL1:IL2,:)
      CFLUXCGPAT(IOFF+IL1:IOFF+IL2,:)   = CFLUXCGROT(IL1:IL2,:)

      CO2CG1PAT (IOFF+IL1:IOFF+IL2,:,:) = CO2CG1ROT (IL1:IL2,:,:)
      CO2CG2PAT (IOFF+IL1:IOFF+IL2,:,:) = CO2CG2ROT (IL1:IL2,:,:)
      CO2CS1PAT (IOFF+IL1:IOFF+IL2,:,:) = CO2CS1ROT (IL1:IL2,:,:)
      CO2CS2PAT (IOFF+IL1:IOFF+IL2,:,:) = CO2CS2ROT (IL1:IL2,:,:)
      ANCGPTL   (IOFF+IL1:IOFF+IL2,:,:) = ANCGRTL   (IL1:IL2,:,:)
      ANCSPTL   (IOFF+IL1:IOFF+IL2,:,:) = ANCSRTL   (IL1:IL2,:,:)
      RMLCGPTL  (IOFF+IL1:IOFF+IL2,:,:) = RMLCGRTL  (IL1:IL2,:,:)
      RMLCSPTL  (IOFF+IL1:IOFF+IL2,:,:) = RMLCSRTL  (IL1:IL2,:,:)

      TBARPTL   (IOFF+IL1:IOFF+IL2,:,:) = TBARRTL   (IL1:IL2,:,:)
      TBARCPTL  (IOFF+IL1:IOFF+IL2,:,:) = TBARCRTL  (IL1:IL2,:,:)
      TBARCSPTL (IOFF+IL1:IOFF+IL2,:,:) = TBARCSRTL (IL1:IL2,:,:)
      TBARGPTL  (IOFF+IL1:IOFF+IL2,:,:) = TBARGRTL  (IL1:IL2,:,:)
      TBARGSPTL (IOFF+IL1:IOFF+IL2,:,:) = TBARGSRTL (IL1:IL2,:,:)
      THLIQCPTL (IOFF+IL1:IOFF+IL2,:,:) = THLIQCRTL (IL1:IL2,:,:)
      THLIQGPTL (IOFF+IL1:IOFF+IL2,:,:) = THLIQGRTL (IL1:IL2,:,:)
      THICECPTL (IOFF+IL1:IOFF+IL2,:,:) = THICECRTL (IL1:IL2,:,:)
C
C     * TIME VARYING.
C
      SOILCPAT  (IOFF+IL1:IOFF+IL2,:,:)   = SOILCROT  (IL1:IL2,:,:)
      LITRCPAT  (IOFF+IL1:IOFF+IL2,:,:)   = LITRCROT  (IL1:IL2,:,:)
      ROOTCPAT  (IOFF+IL1:IOFF+IL2,:,:)   = ROOTCROT  (IL1:IL2,:,:)
      STEMCPAT  (IOFF+IL1:IOFF+IL2,:,:)   = STEMCROT  (IL1:IL2,:,:)
      GLEAFCPAT (IOFF+IL1:IOFF+IL2,:,:)   = GLEAFCROT (IL1:IL2,:,:)
      BLEAFCPAT (IOFF+IL1:IOFF+IL2,:,:)   = BLEAFCROT (IL1:IL2,:,:)
      FALLHPAT  (IOFF+IL1:IOFF+IL2,:,:)   = FALLHROT  (IL1:IL2,:,:)
      POSPHPAT  (IOFF+IL1:IOFF+IL2,:,:)   = POSPHROT  (IL1:IL2,:,:)
      LEAFSPAT  (IOFF+IL1:IOFF+IL2,:,:)   = LEAFSROT  (IL1:IL2,:,:)
      GROWTPAT  (IOFF+IL1:IOFF+IL2,:,:)   = GROWTROT  (IL1:IL2,:,:)
      LASTRPAT  (IOFF+IL1:IOFF+IL2,:,:)   = LASTRROT  (IL1:IL2,:,:)
      LASTSPAT  (IOFF+IL1:IOFF+IL2,:,:)   = LASTSROT  (IL1:IL2,:,:)
      THISYLPAT (IOFF+IL1:IOFF+IL2,:,:)   = THISYLROT (IL1:IL2,:,:)
      STEMHPAT  (IOFF+IL1:IOFF+IL2,:,:)   = STEMHROT  (IL1:IL2,:,:)
      ROOTHPAT  (IOFF+IL1:IOFF+IL2,:,:)   = ROOTHROT  (IL1:IL2,:,:)
      TEMPCPAT  (IOFF+IL1:IOFF+IL2,:,:)   = TEMPCROT  (IL1:IL2,:,:)
      AILCBPAT  (IOFF+IL1:IOFF+IL2,:,:)   = AILCBROT  (IL1:IL2,:,:)
      BMASVPAT  (IOFF+IL1:IOFF+IL2,:,:)   = BMASVROT  (IL1:IL2,:,:)
      VEGHPAT   (IOFF+IL1:IOFF+IL2,:,:)   = VEGHROT   (IL1:IL2,:,:)
      ROOTDPAT  (IOFF+IL1:IOFF+IL2,:,:)   = ROOTDROT  (IL1:IL2,:,:)
C
      PREFPAT   (IOFF+IL1:IOFF+IL2,:,:)   = PREFROT   (IL1:IL2,:,:)
      NEWFPAT   (IOFF+IL1:IOFF+IL2,:,:)   = NEWFROT   (IL1:IL2,:,:)
C
      CVEGPAT   (IOFF+IL1:IOFF+IL2,:)     = CVEGROT   (IL1:IL2,:)
      CDEBPAT   (IOFF+IL1:IOFF+IL2,:)     = CDEBROT   (IL1:IL2,:)
      CHUMPAT   (IOFF+IL1:IOFF+IL2,:)     = CHUMROT   (IL1:IL2,:)
      FCOLPAT   (IOFF+IL1:IOFF+IL2,:)     = FCOLROT   (IL1:IL2,:)
C
C     * CTEM DIAGNOSTIC OUTPUT FIELDS.
C
      CVEGPAK(IOFF+IL1:IOFF+IL2) = CVEGROW(IL1:IL2)
      CDEBPAK(IOFF+IL1:IOFF+IL2) = CDEBROW(IL1:IL2)
      CHUMPAK(IOFF+IL1:IOFF+IL2) = CHUMROW(IL1:IL2)
      CLAIPAK(IOFF+IL1:IOFF+IL2) = CLAIROW(IL1:IL2)
      CFNPPAK(IOFF+IL1:IOFF+IL2) = CFNPROW(IL1:IL2)
      CFNEPAK(IOFF+IL1:IOFF+IL2) = CFNEROW(IL1:IL2)
      CFRVPAK(IOFF+IL1:IOFF+IL2) = CFRVROW(IL1:IL2)
      CFGPPAK(IOFF+IL1:IOFF+IL2) = CFGPROW(IL1:IL2)
      CFNBPAK(IOFF+IL1:IOFF+IL2) = CFNBROW(IL1:IL2)
      CFFVPAK(IOFF+IL1:IOFF+IL2) = CFFVROW(IL1:IL2)
      CFFDPAK(IOFF+IL1:IOFF+IL2) = CFFDROW(IL1:IL2)
      CFLVPAK(IOFF+IL1:IOFF+IL2) = CFLVROW(IL1:IL2)
      CFLDPAK(IOFF+IL1:IOFF+IL2) = CFLDROW(IL1:IL2)
      CFLHPAK(IOFF+IL1:IOFF+IL2) = CFLHROW(IL1:IL2)
      CBRNPAK(IOFF+IL1:IOFF+IL2) = CBRNROW(IL1:IL2)
      CFRHPAK(IOFF+IL1:IOFF+IL2) = CFRHROW(IL1:IL2)
      CFHTPAK(IOFF+IL1:IOFF+IL2) = CFHTROW(IL1:IL2)
      CFLFPAK(IOFF+IL1:IOFF+IL2) = CFLFROW(IL1:IL2)
      CFRDPAK(IOFF+IL1:IOFF+IL2) = CFRDROW(IL1:IL2)
      CFRGPAK(IOFF+IL1:IOFF+IL2) = CFRGROW(IL1:IL2)
      CFRMPAK(IOFF+IL1:IOFF+IL2) = CFRMROW(IL1:IL2)
      CVGLPAK(IOFF+IL1:IOFF+IL2) = CVGLROW(IL1:IL2)
      CVGSPAK(IOFF+IL1:IOFF+IL2) = CVGSROW(IL1:IL2)
      CVGRPAK(IOFF+IL1:IOFF+IL2) = CVGRROW(IL1:IL2)
      CFNLPAK(IOFF+IL1:IOFF+IL2) = CFNLROW(IL1:IL2)
      CFNSPAK(IOFF+IL1:IOFF+IL2) = CFNSROW(IL1:IL2)
      CFNRPAK(IOFF+IL1:IOFF+IL2) = CFNRROW(IL1:IL2)
      CH4HPAK(IOFF+IL1:IOFF+IL2) = CH4HROW(IL1:IL2)
      CH4NPAK(IOFF+IL1:IOFF+IL2) = CH4NROW(IL1:IL2)
      WFRAPAK(IOFF+IL1:IOFF+IL2) = WFRAROW(IL1:IL2)
      CW1DPAK(IOFF+IL1:IOFF+IL2) = CW1DROW(IL1:IL2)
      CW2DPAK(IOFF+IL1:IOFF+IL2) = CW2DROW(IL1:IL2)
      FCOLPAK(IOFF+IL1:IOFF+IL2) = FCOLROW(IL1:IL2)
      CURFPAK(IOFF+IL1:IOFF+IL2,:) = CURFROW(IL1:IL2,:)

C     Additional CTEM related CMIP6 output
      BRFRPAK(IOFF+IL1:IOFF+IL2) = BRFRROW(IL1:IL2)   ! baresoilFrac
      C3CRPAK(IOFF+IL1:IOFF+IL2) = C3CRROW(IL1:IL2)   ! cropFracC3
      C4CRPAK(IOFF+IL1:IOFF+IL2) = C4CRROW(IL1:IL2)   ! cropFracC4
      CRPFPAK(IOFF+IL1:IOFF+IL2) = CRPFROW(IL1:IL2)   ! cropFrac
      C3GRPAK(IOFF+IL1:IOFF+IL2) = C3GRROW(IL1:IL2)   ! grassFracC3
      C4GRPAK(IOFF+IL1:IOFF+IL2) = C4GRROW(IL1:IL2)   ! grassFracC4
      GRSFPAK(IOFF+IL1:IOFF+IL2) = GRSFROW(IL1:IL2)   ! grassFrac
      BDTFPAK(IOFF+IL1:IOFF+IL2) = BDTFROW(IL1:IL2)   ! treeFracBdlDcd
      BETFPAK(IOFF+IL1:IOFF+IL2) = BETFROW(IL1:IL2)   ! treeFracBdlEvg
      NDTFPAK(IOFF+IL1:IOFF+IL2) = NDTFROW(IL1:IL2)   ! treeFracNdlDcd
      NETFPAK(IOFF+IL1:IOFF+IL2) = NETFROW(IL1:IL2)   ! treeFracNdlEvg
      TREEPAK(IOFF+IL1:IOFF+IL2) = TREEROW(IL1:IL2)   ! treeFrac
      VEGFPAK(IOFF+IL1:IOFF+IL2) = VEGFROW(IL1:IL2)   ! vegFrac
      C3PFPAK(IOFF+IL1:IOFF+IL2) = C3PFROW(IL1:IL2)   ! c3PftFrac
      C4PFPAK(IOFF+IL1:IOFF+IL2) = C4PFROW(IL1:IL2)   ! c4PftFrac

      CLNDPAK(IOFF+IL1:IOFF+IL2) = CLNDROW(IL1:IL2)   ! cLand

C
C Instantaneous output (used for subdaily output)
C
      CFGPPAL(IOFF+IL1:IOFF+IL2) = CFGPROL(IL1:IL2)
      CFRVPAL(IOFF+IL1:IOFF+IL2) = CFRVROL(IL1:IL2)
      CFRHPAL(IOFF+IL1:IOFF+IL2) = CFRHROL(IL1:IL2)
      CFRDPAL(IOFF+IL1:IOFF+IL2) = CFRDROL(IL1:IL2)

#endif
#if defined explvol
C
C     * EXPLOSIVE VOLCANO FIELDS.
C
      VTAUPAK(IOFF+IL1:IOFF+IL2) = VTAUROW(IL1:IL2)
      VTAUPAL(IOFF+IL1:IOFF+IL2) = VTAUROL(IL1:IL2)
      TROPPAK(IOFF+IL1:IOFF+IL2) = TROPROW(IL1:IL2)
      TROPPAL(IOFF+IL1:IOFF+IL2) = TROPROL(IL1:IL2)

      DO IB = 1, NBS
         DO L = 1, LEVSA
            DO I = IL1Z, IL2Z
               SW_EXT_SA_PAK(IOFFZ+I,L,IB) = SW_EXT_SA_ROW(I,L,IB)
               SW_EXT_SA_PAL(IOFFZ+I,L,IB) = SW_EXT_SA_ROL(I,L,IB)
               SW_SSA_SA_PAK(IOFFZ+I,L,IB) = SW_SSA_SA_ROW(I,L,IB)
               SW_SSA_SA_PAL(IOFFZ+I,L,IB) = SW_SSA_SA_ROL(I,L,IB)
               SW_G_SA_PAK(IOFFZ+I,L,IB)   = SW_G_SA_ROW(I,L,IB)
               SW_G_SA_PAL(IOFFZ+I,L,IB)   = SW_G_SA_ROL(I,L,IB)
            END DO ! I
         END DO ! L
      END DO ! IB

      DO IB = 1, NBL
         DO L = 1, LEVSA
            DO I = IL1Z, IL2Z
               LW_EXT_SA_PAK(IOFFZ+I,L,IB) = LW_EXT_SA_ROW(I,L,IB)
               LW_EXT_SA_PAL(IOFFZ+I,L,IB) = LW_EXT_SA_ROL(I,L,IB)
               LW_SSA_SA_PAK(IOFFZ+I,L,IB) = LW_SSA_SA_ROW(I,L,IB)
               LW_SSA_SA_PAL(IOFFZ+I,L,IB) = LW_SSA_SA_ROL(I,L,IB)
               LW_G_SA_PAK(IOFFZ+I,L,IB)   = LW_G_SA_ROW(I,L,IB)
               LW_G_SA_PAL(IOFFZ+I,L,IB)   = LW_G_SA_ROL(I,L,IB)
            END DO ! I
         END DO ! L
      END DO ! IB

      DO L = 1, LEVSA
         DO I = IL1Z, IL2Z
            W055_EXT_SA_PAK(IOFFZ+I,L) = W055_EXT_SA_ROW(I,L)
            W055_EXT_SA_PAL(IOFFZ+I,L) = W055_EXT_SA_ROL(I,L)
            W110_EXT_SA_PAK(IOFFZ+I,L) = W110_EXT_SA_ROW(I,L)
            W110_EXT_SA_PAL(IOFFZ+I,L) = W110_EXT_SA_ROL(I,L)
            PRESSURE_SA_PAK(IOFFZ+I,L) = PRESSURE_SA_ROW(I,L)
            PRESSURE_SA_PAL(IOFFZ+I,L) = PRESSURE_SA_ROL(I,L)
         END DO ! I
      END DO ! L

      DO I = IL1, IL2
         W055_VTAU_SA_PAK(IOFF+I) = W055_VTAU_SA_ROW(I)
         W055_VTAU_SA_PAL(IOFF+I) = W055_VTAU_SA_ROL(I)
         W110_VTAU_SA_PAK(IOFF+I) = W110_VTAU_SA_ROW(I)
         W110_VTAU_SA_PAL(IOFF+I) = W110_VTAU_SA_ROL(I)
      END DO ! I

      DO L = 1, ILEV
         DO I = IL1, IL2
            W055_EXT_GCM_SA_PAK(IOFF+I,L) = W055_EXT_GCM_SA_ROW(I,L)
            W055_EXT_GCM_SA_PAL(IOFF+I,L) = W055_EXT_GCM_SA_ROL(I,L)
            W110_EXT_GCM_SA_PAK(IOFF+I,L) = W110_EXT_GCM_SA_ROW(I,L)
            W110_EXT_GCM_SA_PAL(IOFF+I,L) = W110_EXT_GCM_SA_ROL(I,L)
         END DO ! I
      END DO ! L

#endif
C
C     * LAKES FIELDS.
C
      DO I=IL1,IL2
        LDMXPAK(IOFF+I) = LDMXROW(I)
        LRIMPAK(IOFF+I) = LRIMROW(I)
        LRINPAK(IOFF+I) = LRINROW(I)
        LUIMPAK(IOFF+I) = LUIMROW(I)
        LUINPAK(IOFF+I) = LUINROW(I)
        LZICPAK(IOFF+I) = LZICROW(I)
#if defined (cslm)
        DELUPAK(IOFF+I) = DELUROW(I)
        DTMPPAK(IOFF+I) = DTMPROW(I)
        EXPWPAK(IOFF+I) = EXPWROW(I)
        GREDPAK(IOFF+I) = GREDROW(I)
        RHOMPAK(IOFF+I) = RHOMROW(I)
        T0LKPAK(IOFF+I) = T0LKROW(I)
        TKELPAK(IOFF+I) = TKELROW(I)
        FLGLPAK(IOFF+I) = FLGLROW(I)
        FNLPAK (IOFF+I) = FNLROW (I)
        FSGLPAK(IOFF+I) = FSGLROW(I)
        HFCLPAK(IOFF+I) = HFCLROW(I)
        HFLLPAK(IOFF+I) = HFLLROW(I)
        HFSLPAK(IOFF+I) = HFSLROW(I)
        HMFLPAK(IOFF+I) = HMFLROW(I)
        PILPAK (IOFF+I) = PILROW (I)
        QFLPAK (IOFF+I) = QFLROW (I)
C
        DO L=1,NLKLM
          TLAKPAK(IOFF+I,L) = TLAKROW(I,L)
        ENDDO
#endif
#if defined (flake)
        LSHPPAK(IOFF+I) = LSHPROW(I)
        LTAVPAK(IOFF+I) = LTAVROW(I)
        LTICPAK(IOFF+I) = LTICROW(I)
        LTMXPAK(IOFF+I) = LTMXROW(I)
        LTSNPAK(IOFF+I) = LTSNROW(I)
        LTWBPAK(IOFF+I) = LTWBROW(I)
        LZSNPAK(IOFF+I) = LZSNROW(I)
#endif
      ENDDO
C
C     * EMISSION FIELDS.
C
      DO I=IL1,IL2
        EOSTPAK(IOFF+I) = EOSTROW(I)
        EOSTPAL(IOFF+I) = EOSTROL(I)
C
        ESCVPAK(IOFF+I) = ESCVROW(I)
        EHCVPAK(IOFF+I) = EHCVROW(I)
        ESEVPAK(IOFF+I) = ESEVROW(I)
        EHEVPAK(IOFF+I) = EHEVROW(I)
      ENDDO
#if defined transient_aerosol_emissions
      DO L=1,LEVWF
      DO I=IL1,IL2
        FBBCPAK(IOFF+I,L) = FBBCROW(I,L)
        FBBCPAL(IOFF+I,L) = FBBCROL(I,L)
      ENDDO
      ENDDO
      DO L=1,LEVAIR
      DO I=IL1,IL2
        FAIRPAK(IOFF+I,L) = FAIRROW(I,L)
        FAIRPAL(IOFF+I,L) = FAIRROL(I,L)
      ENDDO
      ENDDO
#if defined emists
      DO I=IL1,IL2
        SAIRPAK(IOFF+I) = SAIRROW(I)
        SSFCPAK(IOFF+I) = SSFCROW(I)
        SBIOPAK(IOFF+I) = SBIOROW(I)
        SSHIPAK(IOFF+I) = SSHIROW(I)
        SSTKPAK(IOFF+I) = SSTKROW(I)
        SFIRPAK(IOFF+I) = SFIRROW(I)
        SAIRPAL(IOFF+I) = SAIRROL(I)
        SSFCPAL(IOFF+I) = SSFCROL(I)
        SBIOPAL(IOFF+I) = SBIOROL(I)
        SSHIPAL(IOFF+I) = SSHIROL(I)
        SSTKPAL(IOFF+I) = SSTKROL(I)
        SFIRPAL(IOFF+I) = SFIRROL(I)
        OAIRPAK(IOFF+I) = OAIRROW(I)
        OSFCPAK(IOFF+I) = OSFCROW(I)
        OBIOPAK(IOFF+I) = OBIOROW(I)
        OSHIPAK(IOFF+I) = OSHIROW(I)
        OSTKPAK(IOFF+I) = OSTKROW(I)
        OFIRPAK(IOFF+I) = OFIRROW(I)
        OAIRPAL(IOFF+I) = OAIRROL(I)
        OSFCPAL(IOFF+I) = OSFCROL(I)
        OBIOPAL(IOFF+I) = OBIOROL(I)
        OSHIPAL(IOFF+I) = OSHIROL(I)
        OSTKPAL(IOFF+I) = OSTKROL(I)
        OFIRPAL(IOFF+I) = OFIRROL(I)
        BAIRPAK(IOFF+I) = BAIRROW(I)
        BSFCPAK(IOFF+I) = BSFCROW(I)
        BBIOPAK(IOFF+I) = BBIOROW(I)
        BSHIPAK(IOFF+I) = BSHIROW(I)
        BSTKPAK(IOFF+I) = BSTKROW(I)
        BFIRPAK(IOFF+I) = BFIRROW(I)
        BAIRPAL(IOFF+I) = BAIRROL(I)
        BSFCPAL(IOFF+I) = BSFCROL(I)
        BBIOPAL(IOFF+I) = BBIOROL(I)
        BSHIPAL(IOFF+I) = BSHIROL(I)
        BSTKPAL(IOFF+I) = BSTKROL(I)
        BFIRPAL(IOFF+I) = BFIRROL(I)
      ENDDO
#endif
#endif
#if (defined(pla) && defined(pfrc))
      DO I=IL1,IL2
        BCDPFPAK(IOFF+I) = BCDPFROW(I)
        BCDPFPAL(IOFF+I) = BCDPFROL(I)
      ENDDO
      DO L=1,ILEV
      DO I=IL1,IL2
        AMLDFPAK(IOFF+I,L) = AMLDFROW(I,L)
        AMLDFPAL(IOFF+I,L) = AMLDFROL(I,L)
        REAMFPAK(IOFF+I,L) = REAMFROW(I,L)
        REAMFPAL(IOFF+I,L) = REAMFROL(I,L)
        VEAMFPAK(IOFF+I,L) = VEAMFROW(I,L)
        VEAMFPAL(IOFF+I,L) = VEAMFROL(I,L)
        FR1FPAK (IOFF+I,L) = FR1FROW (I,L)
        FR1FPAL (IOFF+I,L) = FR1FROL (I,L)
        FR2FPAK (IOFF+I,L) = FR2FROW (I,L)
        FR2FPAL (IOFF+I,L) = FR2FROL (I,L)
        SSLDFPAK(IOFF+I,L) = SSLDFROW(I,L)
        SSLDFPAL(IOFF+I,L) = SSLDFROL(I,L)
        RESSFPAK(IOFF+I,L) = RESSFROW(I,L)
        RESSFPAL(IOFF+I,L) = RESSFROL(I,L)
        VESSFPAK(IOFF+I,L) = VESSFROW(I,L)
        VESSFPAL(IOFF+I,L) = VESSFROL(I,L)
        DSLDFPAK(IOFF+I,L) = DSLDFROW(I,L)
        DSLDFPAL(IOFF+I,L) = DSLDFROL(I,L)
        REDSFPAK(IOFF+I,L) = REDSFROW(I,L)
        REDSFPAL(IOFF+I,L) = REDSFROL(I,L)
        VEDSFPAK(IOFF+I,L) = VEDSFROW(I,L)
        VEDSFPAL(IOFF+I,L) = VEDSFROL(I,L)
        BCLDFPAK(IOFF+I,L) = BCLDFROW(I,L)
        BCLDFPAL(IOFF+I,L) = BCLDFROL(I,L)
        REBCFPAK(IOFF+I,L) = REBCFROW(I,L)
        REBCFPAL(IOFF+I,L) = REBCFROL(I,L)
        VEBCFPAK(IOFF+I,L) = VEBCFROW(I,L)
        VEBCFPAL(IOFF+I,L) = VEBCFROL(I,L)
        OCLDFPAK(IOFF+I,L) = OCLDFROW(I,L)
        OCLDFPAL(IOFF+I,L) = OCLDFROL(I,L)
        REOCFPAK(IOFF+I,L) = REOCFROW(I,L)
        REOCFPAL(IOFF+I,L) = REOCFROL(I,L)
        VEOCFPAK(IOFF+I,L) = VEOCFROW(I,L)
        VEOCFPAL(IOFF+I,L) = VEOCFROL(I,L)
        ZCDNFPAK(IOFF+I,L) = ZCDNFROW(I,L)
        ZCDNFPAL(IOFF+I,L) = ZCDNFROL(I,L)
        BCICFPAK(IOFF+I,L) = BCICFROW(I,L)
        BCICFPAL(IOFF+I,L) = BCICFROL(I,L)
      ENDDO
      ENDDO
#endif
C
C     * TRACER ARRAYS.
C
      DO N=1,NTRAC
        DO I=IL1,IL2
          XFSPAK (IOFF+I,N) = XFSROW (I,N)
          XSFXPAK(IOFF+I,N) = XSFXROW(I,N)
          XSFXPAL(IOFF+I,N) = XSFXROL(I,N)
          XSRFPAK(IOFF+I,N) = XSRFROW(I,N)
          XSRFPAL(IOFF+I,N) = XSRFROL(I,N)

          XTPFPAM(IOFF+I,N) = XTPFROM(I,N)
          XTPHPAM(IOFF+I,N) = XTPHROM(I,N)
          XTVIPAS(IOFF+I,N) = XTVIROS(I,N)
          XTVIPAK(IOFF+I,N) = XTVIROW(I,N)
          XTVIPAM(IOFF+I,N) = XTVIROM(I,N)
          XTPTPAM(IOFF+I,N) = XTPTROM(I,N)
        ENDDO
        DO L=1,ILEV
        DO I=IL1,IL2
          XWF0PAL(IOFF+I,L,N) = XWF0ROL(I,L,N)
          XWFMPAL(IOFF+I,L,N) = XWFMROL(I,L,N)
        ENDDO
        ENDDO
      ENDDO
C
      DO L=1,LEVOX
        DO I=IL1,IL2
          H2O2PAK(IOFF+I,L) = H2O2ROW(I,L)
          H2O2PAL(IOFF+I,L) = H2O2ROL(I,L)
          HNO3PAK(IOFF+I,L) = HNO3ROW(I,L)
          HNO3PAL(IOFF+I,L) = HNO3ROL(I,L)
          NH3PAK (IOFF+I,L) = NH3ROW (I,L)
          NH3PAL (IOFF+I,L) = NH3ROL (I,L)
          NH4PAK (IOFF+I,L) = NH4ROW (I,L)
          NH4PAL (IOFF+I,L) = NH4ROL (I,L)
          NO3PAK (IOFF+I,L) = NO3ROW (I,L)
          NO3PAL (IOFF+I,L) = NO3ROL (I,L)
          O3PAK  (IOFF+I,L) = O3ROW  (I,L)
          O3PAL  (IOFF+I,L) = O3ROL  (I,L)
          OHPAK  (IOFF+I,L) = OHROW  (I,L)
          OHPAL  (IOFF+I,L) = OHROL  (I,L)
        ENDDO
      ENDDO
C
      DO L=1,ILEV
        DO I=IL1,IL2
          TBNDPAK(IOFF+I,L) = TBNDROW(I,L)
          TBNDPAL(IOFF+I,L) = TBNDROL(I,L)
          EA55PAK(IOFF+I,L) = EA55ROW(I,L)
          EA55PAL(IOFF+I,L) = EA55ROL(I,L)
          RKMPAK (IOFF+I,L) = RKMROW (I,L)
          RKHPAK (IOFF+I,L) = RKHROW (I,L)
          RKQPAK (IOFF+I,L) = RKQROW (I,L)
        ENDDO
      ENDDO
C
      DO I=IL1,IL2
        SPOTPAK(IOFF+I) = SPOTROW(I)
        ST01PAK(IOFF+I) = ST01ROW(I)
        ST02PAK(IOFF+I) = ST02ROW(I)
        ST03PAK(IOFF+I) = ST03ROW(I)
        ST04PAK(IOFF+I) = ST04ROW(I)
        ST06PAK(IOFF+I) = ST06ROW(I)
        ST13PAK(IOFF+I) = ST13ROW(I)
        ST14PAK(IOFF+I) = ST14ROW(I)
        ST15PAK(IOFF+I) = ST15ROW(I)
        ST16PAK(IOFF+I) = ST16ROW(I)
        ST17PAK(IOFF+I) = ST17ROW(I)
        SUZ0PAK(IOFF+I) = SUZ0ROW(I)
        SUZ0PAL(IOFF+I) = SUZ0ROL(I)
        PDSFPAK(IOFF+I) = PDSFROW(I)
        PDSFPAL(IOFF+I) = PDSFROL(I)
      ENDDO
C
C     * CONDITIONAL ARRAYS.
C
#if defined rad_flux_profs
      DO L = 1, ILEV+2
         DO I = IL1, IL2
            FSAUPAK(IOFF+I,L) = FSAUROW(I,L)
            FSADPAK(IOFF+I,L) = FSADROW(I,L)
            FLAUPAK(IOFF+I,L) = FLAUROW(I,L)
            FLADPAK(IOFF+I,L) = FLADROW(I,L)
            FSCUPAK(IOFF+I,L) = FSCUROW(I,L)
            FSCDPAK(IOFF+I,L) = FSCDROW(I,L)
            FLCUPAK(IOFF+I,L) = FLCUROW(I,L)
            FLCDPAK(IOFF+I,L) = FLCDROW(I,L)
         END DO ! I
      END DO ! L
#endif
      DO L = 1, ILEV+2
        DO I = IL1, IL2
          FSAUPAL(IOFF+I,L) = FSAUROL(I,L)
          FSADPAL(IOFF+I,L) = FSADROL(I,L)
          FLAUPAL(IOFF+I,L) = FLAUROL(I,L)
          FLADPAL(IOFF+I,L) = FLADROL(I,L)
          FSCUPAL(IOFF+I,L) = FSCUROL(I,L)
          FSCDPAL(IOFF+I,L) = FSCDROL(I,L)
          FLCUPAL(IOFF+I,L) = FLCUROL(I,L)
          FLCDPAL(IOFF+I,L) = FLCDROL(I,L)
        END DO ! I
      END DO ! L


#if defined radforce

      !--- Upward and downward all-sky and clear-sky fluxes
      !--- FL* thermal, FS* solar

      FSAUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSAUROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FSADPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSADROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLAUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLAUROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLADPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLADROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FSCUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSCUROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FSCDPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSCDROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLCUPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLCUROW_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLCDPAK_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLCDROW_R (IL1:IL2,1:ILEV+2,1:NRFP)

      FSAUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSAUROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FSADPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSADROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLAUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLAUROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLADPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLADROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FSCUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSCUROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FSCDPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FSCDROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLCUPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLCUROL_R (IL1:IL2,1:ILEV+2,1:NRFP)
      FLCDPAL_R(IOFF+IL1:IOFF+IL2,1:ILEV+2,1:NRFP) =
     &         FLCDROL_R (IL1:IL2,1:ILEV+2,1:NRFP)

      !--- temperature perturbation (T_* - T_o) for each perturbation
      RDTPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          RDTROW_R (IL1:IL2,1:ILEV,1:NRFP)
      RDTPAL_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          RDTROL_R (IL1:IL2,1:ILEV,1:NRFP)
      RDTPAM_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          RDTROM_R (IL1:IL2,1:ILEV,1:NRFP)

      !--- heating rates for each perturbation
      HRSPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          HRSROW_R (IL1:IL2,1:ILEV,1:NRFP)
      HRLPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          HRLROW_R (IL1:IL2,1:ILEV,1:NRFP)

      HRSCPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          HRSCROW_R (IL1:IL2,1:ILEV,1:NRFP)
      HRLCPAK_R (IOFF+IL1:IOFF+IL2,1:ILEV,1:NRFP) =
     &          HRLCROW_R (IL1:IL2,1:ILEV,1:NRFP)

      !--- surface fluxes from secondary radiation calls
      CSBPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = CSBROL_R(IL1:IL2,1:NRFP)
      FSGPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = FSGROL_R(IL1:IL2,1:NRFP)
      FSSPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = FSSROL_R(IL1:IL2,1:NRFP)
      FSSCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP) = FSSCROL_R(IL1:IL2,1:NRFP)
      CLBPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = CLBROL_R(IL1:IL2,1:NRFP)
      FLGPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = FLGROL_R(IL1:IL2,1:NRFP)
      FDLPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = FDLROL_R(IL1:IL2,1:NRFP)
      FDLCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP) = FDLCROL_R(IL1:IL2,1:NRFP)
      FSRPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = FSRROL_R(IL1:IL2,1:NRFP)
      FSRCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP) = FSRCROL_R(IL1:IL2,1:NRFP)
      FSOPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = FSOROL_R(IL1:IL2,1:NRFP)
      OLRPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP)  = OLRROL_R(IL1:IL2,1:NRFP)
      OLRCPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP) = OLRCROL_R(IL1:IL2,1:NRFP)
      FSLOPAL_R(IOFF+IL1:IOFF+IL2,1:NRFP) = FSLOROL_R(IL1:IL2,1:NRFP)

      DO M = 1, IM
       CSBPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP) = CSBROT_R(IL1:IL2,M,1:NRFP)
       CLBPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP) = CLBROT_R(IL1:IL2,M,1:NRFP)
       FSGPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP) = FSGROT_R(IL1:IL2,M,1:NRFP)
       FLGPAT_R(IOFF+IL1:IOFF+IL2,M,1:NRFP) = FLGROT_R(IL1:IL2,M,1:NRFP)
      END DO ! M

      !--- Vertical level index of tropopause height
      KTHPAL(IOFF+IL1:IOFF+IL2) = KTHROL(IL1:IL2)
#endif
#if defined qconsav
      DO I=IL1,IL2
          QADDPAK(IOFF+I,1) = QADDROW(I,1)
          QADDPAK(IOFF+I,2) = QADDROW(I,2)
          QTPHPAK(IOFF+I,1) = QTPHROW(I,1)
          QTPHPAK(IOFF+I,2) = QTPHROW(I,2)
      ENDDO
#endif
C
      DO L=1,ILEV
        DO I=IL1,IL2
#if defined tprhs
          TTPPAK (IOFF+I,L) = TTPROW (I,L)
#endif
#if defined qprhs
          QTPPAK (IOFF+I,L) = QTPROW (I,L)
#endif
#if defined uprhs
          UTPPAK (IOFF+I,L) = UTPROW (I,L)
#endif
#if defined vprhs
          VTPPAK (IOFF+I,L) = VTPROW (I,L)
#endif
#if defined qconsav
          QTPFPAK(IOFF+I,L) = QTPFROW(I,L)
#endif
#if defined tprhsc
          TTPCPAK(IOFF+I,L) = TTPCROW(I,L)
          TTPKPAK(IOFF+I,L) = TTPKROW(I,L)
          TTPMPAK(IOFF+I,L) = TTPMROW(I,L)
          TTPNPAK(IOFF+I,L) = TTPNROW(I,L)
          TTPPPAK(IOFF+I,L) = TTPPROW(I,L)
          TTPVPAK(IOFF+I,L) = TTPVROW(I,L)
#endif
#if (defined (tprhsc) || defined (radforce))
          TTPLPAK(IOFF+I,L) = TTPLROW(I,L)
          TTPSPAK(IOFF+I,L) = TTPSROW(I,L)
          TTSCPAK(IOFF+I,L) = TTSCROW(I,L)
          TTLCPAK(IOFF+I,L) = TTLCROW(I,L)
#endif
#if defined qprhsc
          QTPCPAK(IOFF+I,L) = QTPCROW(I,L)
          QTPMPAK(IOFF+I,L) = QTPMROW(I,L)
          QTPPPAK(IOFF+I,L) = QTPPROW(I,L)
          QTPVPAK(IOFF+I,L) = QTPVROW(I,L)
#endif
#if defined uprhsc
          UTPCPAK(IOFF+I,L) = UTPCROW(I,L)
          UTPGPAK(IOFF+I,L) = UTPGROW(I,L)
          UTPNPAK(IOFF+I,L) = UTPNROW(I,L)
          UTPSPAK(IOFF+I,L) = UTPSROW(I,L)
          UTPVPAK(IOFF+I,L) = UTPVROW(I,L)
#endif
#if defined vprhsc
          VTPCPAK(IOFF+I,L) = VTPCROW(I,L)
          VTPGPAK(IOFF+I,L) = VTPGROW(I,L)
          VTPNPAK(IOFF+I,L) = VTPNROW(I,L)
          VTPSPAK(IOFF+I,L) = VTPSROW(I,L)
          VTPVPAK(IOFF+I,L) = VTPVROW(I,L)
#endif
        ENDDO
      ENDDO
C
      DO N=1,NTRAC
#if defined xconsav
        DO I=IL1,IL2
            XADDPAK(IOFF+I,1,N) = XADDROW(I,1,N)
            XADDPAK(IOFF+I,2,N) = XADDROW(I,2,N)
            XTPHPAK(IOFF+I,1,N) = XTPHROW(I,1,N)
            XTPHPAK(IOFF+I,2,N) = XTPHROW(I,2,N)
        ENDDO
#endif
        DO L=1,ILEV
          DO I=IL1,IL2
#if defined xprhs
            XTPPAK (IOFF+I,L,N) = XTPROW (I,L,N)
#endif
#if defined xconsav
            XTPFPAK(IOFF+I,L,N) = XTPFROW(I,L,N)
#endif
#if defined xprhsc
            XTPCPAK(IOFF+I,L,N) = XTPCROW(I,L,N)
            XTPMPAK(IOFF+I,L,N) = XTPMROW(I,L,N)
            XTPPPAK(IOFF+I,L,N) = XTPPROW(I,L,N)
            XTPVPAK(IOFF+I,L,N) = XTPVROW(I,L,N)
#endif
#if defined x01
            X01PAK (IOFF+I,L,N) = X01ROW (I,L,N)
#endif
#if defined x02
            X02PAK (IOFF+I,L,N) = X02ROW (I,L,N)
#endif
#if defined x03
            X03PAK (IOFF+I,L,N) = X03ROW (I,L,N)
#endif
#if defined x04
            X04PAK (IOFF+I,L,N) = X04ROW (I,L,N)
#endif
#if defined x05
            X05PAK (IOFF+I,L,N) = X05ROW (I,L,N)
#endif
#if defined x06
            X06PAK (IOFF+I,L,N) = X06ROW (I,L,N)
#endif
#if defined x07
            X07PAK (IOFF+I,L,N) = X07ROW (I,L,N)
#endif
#if defined x08
            X08PAK (IOFF+I,L,N) = X08ROW (I,L,N)
#endif
#if defined x09
            X09PAK (IOFF+I,L,N) = X09ROW (I,L,N)
#endif
          ENDDO
        ENDDO
      ENDDO
#if defined xtraconv
      DO I=IL1,IL2
        BCDPAK (IOFF+I) = BCDROW (I)
        BCSPAK (IOFF+I) = BCSROW (I)
        CAPEPAK(IOFF+I) = CAPEROW(I)
        CDCBPAK(IOFF+I) = CDCBROW(I)
        CINHPAK(IOFF+I) = CINHROW(I)
        CSCBPAK(IOFF+I) = CSCBROW(I)
        TCDPAK (IOFF+I) = TCDROW (I)
        TCSPAK (IOFF+I) = TCSROW (I)
      ENDDO
C
      DO L=1,ILEV
      DO I=IL1,IL2
          DMCPAK (IOFF+I,L) = DMCROW (I,L)
          SMCPAK (IOFF+I,L) = SMCROW (I,L)
          DMCDPAK(IOFF+I,L) = DMCDROW(I,L)
          DMCUPAK(IOFF+I,L) = DMCUROW(I,L)

      ENDDO
      ENDDO
#endif
#if defined xtrachem
      DO I=IL1,IL2
        DD4PAK (IOFF+I) = DD4ROW (I)
        DOX4PAK(IOFF+I) = DOX4ROW(I)
        DOXDPAK(IOFF+I) = DOXDROW(I)
        ESDPAK (IOFF+I) = ESDROW (I)
        ESFSPAK(IOFF+I) = ESFSROW(I)
        EAISPAK(IOFF+I) = EAISROW(I)
        ESTSPAK(IOFF+I) = ESTSROW(I)
        EFISPAK(IOFF+I) = EFISROW(I)
        ESFBPAK(IOFF+I) = ESFBROW(I)
        EAIBPAK(IOFF+I) = EAIBROW(I)
        ESTBPAK(IOFF+I) = ESTBROW(I)
        EFIBPAK(IOFF+I) = EFIBROW(I)
        ESFOPAK(IOFF+I) = ESFOROW(I)
        EAIOPAK(IOFF+I) = EAIOROW(I)
        ESTOPAK(IOFF+I) = ESTOROW(I)
        EFIOPAK(IOFF+I) = EFIOROW(I)
        EDSLPAK(IOFF+I) = EDSLROW(I)
        EDSOPAK(IOFF+I) = EDSOROW(I)
        ESVCPAK(IOFF+I) = ESVCROW(I)
        ESVEPAK(IOFF+I) = ESVEROW(I)
        NOXDPAK(IOFF+I) = NOXDROW(I)
        WDD4PAK(IOFF+I) = WDD4ROW(I)
        WDL4PAK(IOFF+I) = WDL4ROW(I)
        WDS4PAK(IOFF+I) = WDS4ROW(I)
      ENDDO
#ifndef pla
      DO I=IL1,IL2
        DDDPAK (IOFF+I) = DDDROW (I)
        DDBPAK (IOFF+I) = DDBROW (I)
        DDOPAK (IOFF+I) = DDOROW (I)
        DDSPAK (IOFF+I) = DDSROW (I)
        WDLDPAK(IOFF+I) = WDLDROW(I)
        WDLBPAK(IOFF+I) = WDLBROW(I)
        WDLOPAK(IOFF+I) = WDLOROW(I)
        WDLSPAK(IOFF+I) = WDLSROW(I)
        WDDDPAK(IOFF+I) = WDDDROW(I)
        WDDBPAK(IOFF+I) = WDDBROW(I)
        WDDOPAK(IOFF+I) = WDDOROW(I)
        WDDSPAK(IOFF+I) = WDDSROW(I)
        WDSDPAK(IOFF+I) = WDSDROW(I)
        WDSBPAK(IOFF+I) = WDSBROW(I)
        WDSOPAK(IOFF+I) = WDSOROW(I)
        WDSSPAK(IOFF+I) = WDSSROW(I)
        DD6PAK (IOFF+I) = DD6ROW (I)
        SDHPPAK(IOFF+I) = SDHPROW(I)
        SDO3PAK(IOFF+I) = SDO3ROW(I)
        SLHPPAK(IOFF+I) = SLHPROW(I)
        SLO3PAK(IOFF+I) = SLO3ROW(I)
        SSHPPAK(IOFF+I) = SSHPROW(I)
        SSO3PAK(IOFF+I) = SSO3ROW(I)
        WDD6PAK(IOFF+I) = WDD6ROW(I)
        WDL6PAK(IOFF+I) = WDL6ROW(I)
        WDS6PAK(IOFF+I) = WDS6ROW(I)
      ENDDO
C
      DO L=1,ILEV
        DO I=IL1,IL2
          PHDPAK (IOFF+I,L) = PHDROW (I,L)
          PHLPAK (IOFF+I,L) = PHLROW (I,L)
          PHSPAK (IOFF+I,L) = PHSROW (I,L)
        ENDDO
      ENDDO
#endif
#endif
#if defined (xtradust)
      DO I=IL1,IL2
        DUWDPAK(IOFF+I) = DUWDROW(I)
        DUSTPAK(IOFF+I) = DUSTROW(I)
        DUTHPAK(IOFF+I) = DUTHROW(I)
        USMKPAK(IOFF+I) = USMKROW(I)
        FALLPAK(IOFF+I) = FALLROW(I)
        FA10PAK(IOFF+I) = FA10ROW(I)
         FA2PAK(IOFF+I) =  FA2ROW(I)
         FA1PAK(IOFF+I) =  FA1ROW(I)
        GUSTPAK(IOFF+I) = GUSTROW(I)
        ZSPDPAK(IOFF+I) = ZSPDROW(I)
        VGFRPAK(IOFF+I) = VGFRROW(I)
        SMFRPAK(IOFF+I) = SMFRROW(I)
      ENDDO
#endif
#if (defined(pla) && defined(pam))
      DO L=1,ILEV
      DO I=IL1,IL2
        SSLDPAK(IOFF+I,L)   = SSLDROW(I,L)
        RESSPAK(IOFF+I,L)   = RESSROW(I,L)
        VESSPAK(IOFF+I,L)   = VESSROW(I,L)
        DSLDPAK(IOFF+I,L)   = DSLDROW(I,L)
        REDSPAK(IOFF+I,L)   = REDSROW(I,L)
        VEDSPAK(IOFF+I,L)   = VEDSROW(I,L)
        BCLDPAK(IOFF+I,L)   = BCLDROW(I,L)
        REBCPAK(IOFF+I,L)   = REBCROW(I,L)
        VEBCPAK(IOFF+I,L)   = VEBCROW(I,L)
        OCLDPAK(IOFF+I,L)   = OCLDROW(I,L)
        REOCPAK(IOFF+I,L)   = REOCROW(I,L)
        VEOCPAK(IOFF+I,L)   = VEOCROW(I,L)
        AMLDPAK(IOFF+I,L)   = AMLDROW(I,L)
        REAMPAK(IOFF+I,L)   = REAMROW(I,L)
        VEAMPAK(IOFF+I,L)   = VEAMROW(I,L)
        FR1PAK (IOFF+I,L)   = FR1ROW (I,L)
        FR2PAK (IOFF+I,L)   = FR2ROW (I,L)
        ZCDNPAK(IOFF+I,L)   = ZCDNROW(I,L)
        BCICPAK(IOFF+I,L)   = BCICROW(I,L)
        OEDNPAK(IOFF+I,L,:) = OEDNROW(I,L,:)
        OERCPAK(IOFF+I,L,:) = OERCROW(I,L,:)
        OIDNPAK(IOFF+I,L)   = OIDNROW(I,L)
        OIRCPAK(IOFF+I,L)   = OIRCROW(I,L)
        SVVBPAK(IOFF+I,L)   = SVVBROW(I,L)
        PSVVPAK(IOFF+I,L)   = PSVVROW(I,L)
        SVMBPAK(IOFF+I,L,:) = SVMBROW(I,L,:)
        SVCBPAK(IOFF+I,L,:) = SVCBROW(I,L,:)
        PNVBPAK(IOFF+I,L,:)  =PNVBROW(I,L,:)
        PNMBPAK(IOFF+I,L,:,:)=PNMBROW(I,L,:,:)
        PNCBPAK(IOFF+I,L,:,:)=PNCBROW(I,L,:,:)
        PSVBPAK(IOFF+I,L,:,:)  =PSVBROW(I,L,:,:)
        PSMBPAK(IOFF+I,L,:,:,:)=PSMBROW(I,L,:,:,:)
        PSCBPAK(IOFF+I,L,:,:,:)=PSCBROW(I,L,:,:,:)
        QNVBPAK(IOFF+I,L,:)  =QNVBROW(I,L,:)
        QNMBPAK(IOFF+I,L,:,:)=QNMBROW(I,L,:,:)
        QNCBPAK(IOFF+I,L,:,:)=QNCBROW(I,L,:,:)
        QSVBPAK(IOFF+I,L,:,:)  =QSVBROW(I,L,:,:)
        QSMBPAK(IOFF+I,L,:,:,:)=QSMBROW(I,L,:,:,:)
        QSCBPAK(IOFF+I,L,:,:,:)=QSCBROW(I,L,:,:,:)
        QGVBPAK(IOFF+I,L)   = QGVBROW(I,L)
        QGMBPAK(IOFF+I,L,:) = QGMBROW(I,L,:)
        QGCBPAK(IOFF+I,L,:) = QGCBROW(I,L,:)
        QDVBPAK(IOFF+I,L)   = QDVBROW(I,L)
        QDMBPAK(IOFF+I,L,:) = QDMBROW(I,L,:)
        QDCBPAK(IOFF+I,L,:) = QDCBROW(I,L,:)
        ONVBPAK(IOFF+I,L,:)  =ONVBROW(I,L,:)
        ONMBPAK(IOFF+I,L,:,:)=ONMBROW(I,L,:,:)
        ONCBPAK(IOFF+I,L,:,:)=ONCBROW(I,L,:,:)
        OSVBPAK(IOFF+I,L,:,:)  =OSVBROW(I,L,:,:)
        OSMBPAK(IOFF+I,L,:,:,:)=OSMBROW(I,L,:,:,:)
        OSCBPAK(IOFF+I,L,:,:,:)=OSCBROW(I,L,:,:,:)
        OGVBPAK(IOFF+I,L)   = OGVBROW(I,L)
        OGMBPAK(IOFF+I,L,:) = OGMBROW(I,L,:)
        OGCBPAK(IOFF+I,L,:) = OGCBROW(I,L,:)
        DEVBPAK(IOFF+I,L,:) = DEVBROW(I,L,:)
        PDEVPAK(IOFF+I,L,:) = PDEVROW(I,L,:)
        DEMBPAK(IOFF+I,L,:,:)=DEMBROW(I,L,:,:)
        DECBPAK(IOFF+I,L,:,:)=DECBROW(I,L,:,:)
        DIVBPAK(IOFF+I,L)   = DIVBROW(I,L)
        PDIVPAK(IOFF+I,L)   = PDIVROW(I,L)
        DIMBPAK(IOFF+I,L,:) = DIMBROW(I,L,:)
        DICBPAK(IOFF+I,L,:) = DICBROW(I,L,:)
        REVBPAK(IOFF+I,L,:) = REVBROW(I,L,:)
        PREVPAK(IOFF+I,L,:) = PREVROW(I,L,:)
        REMBPAK(IOFF+I,L,:,:)=REMBROW(I,L,:,:)
        RECBPAK(IOFF+I,L,:,:)=RECBROW(I,L,:,:)
        RIVBPAK(IOFF+I,L)   = RIVBROW(I,L)
        PRIVPAK(IOFF+I,L)   = PRIVROW(I,L)
        RIMBPAK(IOFF+I,L,:) = RIMBROW(I,L,:)
        RICBPAK(IOFF+I,L,:) = RICBROW(I,L,:)
        SULIPAK(IOFF+I,L)  = SULIROW(I,L)
        RSUIPAK(IOFF+I,L)  = RSUIROW(I,L)
        VSUIPAK(IOFF+I,L)  = VSUIROW(I,L)
        F1SUPAK(IOFF+I,L)  = F1SUROW(I,L)
        F2SUPAK(IOFF+I,L)  = F2SUROW(I,L)
        BCLIPAK(IOFF+I,L)  = BCLIROW(I,L)
        RBCIPAK(IOFF+I,L)  = RBCIROW(I,L)
        VBCIPAK(IOFF+I,L)  = VBCIROW(I,L)
        F1BCPAK(IOFF+I,L)  = F1BCROW(I,L)
        F2BCPAK(IOFF+I,L)  = F2BCROW(I,L)
        OCLIPAK(IOFF+I,L)  = OCLIROW(I,L)
        ROCIPAK(IOFF+I,L)  = ROCIROW(I,L)
        VOCIPAK(IOFF+I,L)  = VOCIROW(I,L)
        F1OCPAK(IOFF+I,L)  = F1OCROW(I,L)
        F2OCPAK(IOFF+I,L)  = F2OCROW(I,L)
      ENDDO
      ENDDO
#if defined (xtrapla1)
      DO L=1,ILEV
      DO I=IL1,IL2
        SNCNPAK(IOFF+I,L) = SNCNROW(I,L)
        SSUNPAK(IOFF+I,L) = SSUNROW(I,L)
        SCNDPAK(IOFF+I,L) = SCNDROW(I,L)
        SGSPPAK(IOFF+I,L) = SGSPROW(I,L)
        CCNPAK (IOFF+I,L) = CCNROW (I,L)
        CC02PAK(IOFF+I,L) = CC02ROW(I,L)
        CC04PAK(IOFF+I,L) = CC04ROW(I,L)
        CC08PAK(IOFF+I,L) = CC08ROW(I,L)
        CC16PAK(IOFF+I,L) = CC16ROW(I,L)
        CCNEPAK(IOFF+I,L) = CCNEROW(I,L)
        ACASPAK(IOFF+I,L) = ACASROW(I,L)
        ACOAPAK(IOFF+I,L) = ACOAROW(I,L)
        ACBCPAK(IOFF+I,L) = ACBCROW(I,L)
        ACSSPAK(IOFF+I,L) = ACSSROW(I,L)
        ACMDPAK(IOFF+I,L) = ACMDROW(I,L)
        NTPAK  (IOFF+I,L) = NTROW(I,L)
        N20PAK (IOFF+I,L) = N20ROW(I,L)
        N50PAK (IOFF+I,L) = N50ROW(I,L)
        N100PAK(IOFF+I,L) = N100ROW(I,L)
        N200PAK(IOFF+I,L) = N200ROW(I,L)
        WTPAK  (IOFF+I,L) = WTROW(I,L)
        W20PAK (IOFF+I,L) = W20ROW(I,L)
        W50PAK (IOFF+I,L) = W50ROW(I,L)
        W100PAK(IOFF+I,L) = W100ROW(I,L)
        W200PAK(IOFF+I,L) = W200ROW(I,L)
        RCRIPAK(IOFF+I,L) = RCRIROW(I,L)
        SUPSPAK(IOFF+I,L) = SUPSROW(I,L)
        HENRPAK(IOFF+I,L) = HENRROW(I,L)
        O3FRPAK(IOFF+I,L) = O3FRROW(I,L)
        H2O2FRPAK(IOFF+I,L) = H2O2FRROW(I,L)
        WPARPAK(IOFF+I,L) = WPARROW(I,L)
        PM25PAK(IOFF+I,L) = PM25ROW(I,L)
        PM10PAK(IOFF+I,L) = PM10ROW(I,L)
        DM25PAK(IOFF+I,L) = DM25ROW(I,L)
        DM10PAK(IOFF+I,L) = DM10ROW(I,L)
      ENDDO
      ENDDO
      DO I=IL1,IL2
        VNCNPAK(IOFF+I) = VNCNROW(I)
        VASNPAK(IOFF+I) = VASNROW(I)
        VSCDPAK(IOFF+I) = VSCDROW(I)
        VGSPPAK(IOFF+I) = VGSPROW(I)
        VOAEPAK(IOFF+I) = VOAEROW(I)
        VBCEPAK(IOFF+I) = VBCEROW(I)
        VASEPAK(IOFF+I) = VASEROW(I)
        VMDEPAK(IOFF+I) = VMDEROW(I)
        VSSEPAK(IOFF+I) = VSSEROW(I)
        VOAWPAK(IOFF+I) = VOAWROW(I)
        VBCWPAK(IOFF+I) = VBCWROW(I)
        VASWPAK(IOFF+I) = VASWROW(I)
        VMDWPAK(IOFF+I) = VMDWROW(I)
        VSSWPAK(IOFF+I) = VSSWROW(I)
        VOADPAK(IOFF+I) = VOADROW(I)
        VBCDPAK(IOFF+I) = VBCDROW(I)
        VASDPAK(IOFF+I) = VASDROW(I)
        VMDDPAK(IOFF+I) = VMDDROW(I)
        VSSDPAK(IOFF+I) = VSSDROW(I)
        VOAGPAK(IOFF+I) = VOAGROW(I)
        VBCGPAK(IOFF+I) = VBCGROW(I)
        VASGPAK(IOFF+I) = VASGROW(I)
        VMDGPAK(IOFF+I) = VMDGROW(I)
        VSSGPAK(IOFF+I) = VSSGROW(I)
        VOACPAK(IOFF+I) = VOACROW(I)
        VBCCPAK(IOFF+I) = VBCCROW(I)
        VASCPAK(IOFF+I) = VASCROW(I)
        VMDCPAK(IOFF+I) = VMDCROW(I)
        VSSCPAK(IOFF+I) = VSSCROW(I)
        VASIPAK(IOFF+I) = VASIROW(I)
        VAS1PAK(IOFF+I) = VAS1ROW(I)
        VAS2PAK(IOFF+I) = VAS2ROW(I)
        VAS3PAK(IOFF+I) = VAS3ROW(I)
        VCCNPAK(IOFF+I) = VCCNROW(I)
        VCNEPAK(IOFF+I) = VCNEROW(I)
      ENDDO
#endif
#if defined (xtrapla2)
      DO L=1,ILEV
      DO I=IL1,IL2
        CORNPAK(IOFF+I,L) = CORNROW(I,L)
        CORMPAK(IOFF+I,L) = CORMROW(I,L)
        RSN1PAK(IOFF+I,L) = RSN1ROW(I,L)
        RSM1PAK(IOFF+I,L) = RSM1ROW(I,L)
        RSN2PAK(IOFF+I,L) = RSN2ROW(I,L)
        RSM2PAK(IOFF+I,L) = RSM2ROW(I,L)
        RSN3PAK(IOFF+I,L) = RSN3ROW(I,L)
        RSM3PAK(IOFF+I,L) = RSM3ROW(I,L)
        DO ISF=1,ISDNUM
          SDNUPAK(IOFF+I,L,ISF) = SDNUROW(I,L,ISF)
          SDMAPAK(IOFF+I,L,ISF) = SDMAROW(I,L,ISF)
          SDACPAK(IOFF+I,L,ISF) = SDACROW(I,L,ISF)
          SDCOPAK(IOFF+I,L,ISF) = SDCOROW(I,L,ISF)
          SSSNPAK(IOFF+I,L,ISF) = SSSNROW(I,L,ISF)
          SMDNPAK(IOFF+I,L,ISF) = SMDNROW(I,L,ISF)
          SIANPAK(IOFF+I,L,ISF) = SIANROW(I,L,ISF)
          SSSMPAK(IOFF+I,L,ISF) = SSSMROW(I,L,ISF)
          SMDMPAK(IOFF+I,L,ISF) = SMDMROW(I,L,ISF)
          SEWMPAK(IOFF+I,L,ISF) = SEWMROW(I,L,ISF)
          SSUMPAK(IOFF+I,L,ISF) = SSUMROW(I,L,ISF)
          SOCMPAK(IOFF+I,L,ISF) = SOCMROW(I,L,ISF)
          SBCMPAK(IOFF+I,L,ISF) = SBCMROW(I,L,ISF)
          SIWMPAK(IOFF+I,L,ISF) = SIWMROW(I,L,ISF)
        ENDDO
      ENDDO
      ENDDO
      DO I=IL1,IL2
        DO ISF=1,ISDIAG
          SDVLPAK(IOFF+I,ISF) = SDVLROW(I,ISF)
        ENDDO
        VRN1PAK(IOFF+I) = VRN1ROW(I)
        VRM1PAK(IOFF+I) = VRM1ROW(I)
        VRN2PAK(IOFF+I) = VRN2ROW(I)
        VRM2PAK(IOFF+I) = VRM2ROW(I)
        VRN3PAK(IOFF+I) = VRN3ROW(I)
        VRM3PAK(IOFF+I) = VRM3ROW(I)
      ENDDO
      DO I=IL1,IL2
        DEFAPAK(IOFF+I) = DEFAROW(I)
        DEFCPAK(IOFF+I) = DEFCROW(I)
        DO ISF=1,ISDUST
          DMACPAK(IOFF+I,ISF) = DMACROW(I,ISF)
          DMCOPAK(IOFF+I,ISF) = DMCOROW(I,ISF)
          DNACPAK(IOFF+I,ISF) = DNACROW(I,ISF)
          DNCOPAK(IOFF+I,ISF) = DNCOROW(I,ISF)
        ENDDO
        DO ISF=1,ISDIAG
          DEFXPAK(IOFF+I,ISF) = DEFXROW(I,ISF)
          DEFNPAK(IOFF+I,ISF) = DEFNROW(I,ISF)
        ENDDO
      ENDDO
      DO I=IL1,IL2
        TNSSPAK(IOFF+I) = TNSSROW(I)
        TNMDPAK(IOFF+I) = TNMDROW(I)
        TNIAPAK(IOFF+I) = TNIAROW(I)
        TMSSPAK(IOFF+I) = TMSSROW(I)
        TMMDPAK(IOFF+I) = TMMDROW(I)
        TMOCPAK(IOFF+I) = TMOCROW(I)
        TMBCPAK(IOFF+I) = TMBCROW(I)
        TMSPPAK(IOFF+I) = TMSPROW(I)
        DO N=1,ISDNUM
          SNSSPAK(IOFF+I,N) = SNSSROW(I,N)
          SNMDPAK(IOFF+I,N) = SNMDROW(I,N)
          SNIAPAK(IOFF+I,N) = SNIAROW(I,N)
          SMSSPAK(IOFF+I,N) = SMSSROW(I,N)
          SMMDPAK(IOFF+I,N) = SMMDROW(I,N)
          SMOCPAK(IOFF+I,N) = SMOCROW(I,N)
          SMBCPAK(IOFF+I,N) = SMBCROW(I,N)
          SMSPPAK(IOFF+I,N) = SMSPROW(I,N)
          SIWHPAK(IOFF+I,N) = SIWHROW(I,N)
          SEWHPAK(IOFF+I,N) = SEWHROW(I,N)
        ENDDO
      ENDDO
#endif
#endif
#if defined xtrals
      DO L=1,ILEV
      DO I=IL1,IL2
        AGGPAK (IOFF+I,L) = AGGROW (I,L)
        AUTPAK (IOFF+I,L) = AUTROW (I,L)
        CNDPAK (IOFF+I,L) = CNDROW (I,L)
        DEPPAK (IOFF+I,L) = DEPROW (I,L)
        EVPPAK (IOFF+I,L) = EVPROW (I,L)
        FRHPAK (IOFF+I,L) = FRHROW (I,L)
        FRKPAK (IOFF+I,L) = FRKROW (I,L)
        FRSPAK (IOFF+I,L) = FRSROW (I,L)
        MLTIPAK(IOFF+I,L) = MLTIROW(I,L)
        MLTSPAK(IOFF+I,L) = MLTSROW(I,L)
        RACLPAK(IOFF+I,L) = RACLROW(I,L)
        RAINPAK(IOFF+I,L) = RAINROW(I,L)
        SACIPAK(IOFF+I,L) = SACIROW(I,L)
        SACLPAK(IOFF+I,L) = SACLROW(I,L)
        SNOWPAK(IOFF+I,L) = SNOWROW(I,L)
        SUBPAK (IOFF+I,L) = SUBROW (I,L)
        SEDIPAK(IOFF+I,L) = SEDIROW(I,L)
        RLIQPAK(IOFF+I,L) = RLIQROW(I,L)
        RICEPAK(IOFF+I,L) = RICEROW(I,L)
        CLIQPAK(IOFF+I,L) = CLIQROW(I,L)
        CICEPAK(IOFF+I,L) = CICEROW(I,L)
        RLNCPAK(IOFF+I,L) = RLNCROW(I,L)

        RLIQPAL(IOFF+I,L) = RLIQROL(I,L)
        RICEPAL(IOFF+I,L) = RICEROL(I,L)
        CLIQPAL(IOFF+I,L) = CLIQROL(I,L)
        CICEPAL(IOFF+I,L) = CICEROL(I,L)
        RLNCPAL(IOFF+I,L) = RLNCROL(I,L)


      ENDDO
      ENDDO
C
      DO I=IL1,IL2
        VAGGPAK(IOFF+I)   = VAGGROW(I)
        VAUTPAK(IOFF+I)   = VAUTROW(I)
        VCNDPAK(IOFF+I)   = VCNDROW(I)
        VDEPPAK(IOFF+I)   = VDEPROW(I)
        VEVPPAK(IOFF+I)   = VEVPROW(I)
        VFRHPAK(IOFF+I)   = VFRHROW(I)
        VFRKPAK(IOFF+I)   = VFRKROW(I)
        VFRSPAK(IOFF+I)   = VFRSROW(I)
        VMLIPAK(IOFF+I)   = VMLIROW(I)
        VMLSPAK(IOFF+I)   = VMLSROW(I)
        VRCLPAK(IOFF+I)   = VRCLROW(I)
        VSCIPAK(IOFF+I)   = VSCIROW(I)
        VSCLPAK(IOFF+I)   = VSCLROW(I)
        VSUBPAK(IOFF+I)   = VSUBROW(I)
        VSEDIPAK(IOFF+I)  = VSEDIROW(I)
        RELIQPAK(IOFF+I)  = RELIQROW(I)
        REICEPAK(IOFF+I)  = REICEROW(I)
        CLDLIQPAK(IOFF+I) = CLDLIQROW(I)
        CLDICEPAK(IOFF+I) = CLDICEROW(I)
        CTLNCPAK(IOFF+I)  = CTLNCROW(I)
        CLLNCPAK(IOFF+I)  = CLLNCROW(I)

        RELIQPAL(IOFF+I)  = RELIQROL(I)
        REICEPAL(IOFF+I)  = REICEROL(I)
        CLDLIQPAL(IOFF+I) = CLDLIQROL(I)
        CLDICEPAL(IOFF+I) = CLDICEROL(I)
        CTLNCPAL(IOFF+I)  = CTLNCROL(I)
        CLLNCPAL(IOFF+I)  = CLLNCROL(I)

      ENDDO
#endif
#if defined (aodpth)
       DO I=IL1,IL2
C
        EXB1PAK(IOFF+I) = EXB1ROW(I)
        EXB2PAK(IOFF+I) = EXB2ROW(I)
        EXB3PAK(IOFF+I) = EXB3ROW(I)
        EXB4PAK(IOFF+I) = EXB4ROW(I)
        EXB5PAK(IOFF+I) = EXB5ROW(I)
        EXBTPAK(IOFF+I) = EXBTROW(I)
        ODB1PAK(IOFF+I) = ODB1ROW(I)
        ODB2PAK(IOFF+I) = ODB2ROW(I)
        ODB3PAK(IOFF+I) = ODB3ROW(I)
        ODB4PAK(IOFF+I) = ODB4ROW(I)
        ODB5PAK(IOFF+I) = ODB5ROW(I)
        ODBTPAK(IOFF+I) = ODBTROW(I)
        ODBVPAK(IOFF+I) = ODBVROW(I)
        OFB1PAK(IOFF+I) = OFB1ROW(I)
        OFB2PAK(IOFF+I) = OFB2ROW(I)
        OFB3PAK(IOFF+I) = OFB3ROW(I)
        OFB4PAK(IOFF+I) = OFB4ROW(I)
        OFB5PAK(IOFF+I) = OFB5ROW(I)
        OFBTPAK(IOFF+I) = OFBTROW(I)
        ABB1PAK(IOFF+I) = ABB1ROW(I)
        ABB2PAK(IOFF+I) = ABB2ROW(I)
        ABB3PAK(IOFF+I) = ABB3ROW(I)
        ABB4PAK(IOFF+I) = ABB4ROW(I)
        ABB5PAK(IOFF+I) = ABB5ROW(I)
        ABBTPAK(IOFF+I) = ABBTROW(I)
C
        EXB1PAL(IOFF+I) = EXB1ROL(I)
        EXB2PAL(IOFF+I) = EXB2ROL(I)
        EXB3PAL(IOFF+I) = EXB3ROL(I)
        EXB4PAL(IOFF+I) = EXB4ROL(I)
        EXB5PAL(IOFF+I) = EXB5ROL(I)
        EXBTPAL(IOFF+I) = EXBTROL(I)
        ODB1PAL(IOFF+I) = ODB1ROL(I)
        ODB2PAL(IOFF+I) = ODB2ROL(I)
        ODB3PAL(IOFF+I) = ODB3ROL(I)
        ODB4PAL(IOFF+I) = ODB4ROL(I)
        ODB5PAL(IOFF+I) = ODB5ROL(I)
        ODBTPAL(IOFF+I) = ODBTROL(I)
        ODBVPAL(IOFF+I) = ODBVROL(I)
        OFB1PAL(IOFF+I) = OFB1ROL(I)
        OFB2PAL(IOFF+I) = OFB2ROL(I)
        OFB3PAL(IOFF+I) = OFB3ROL(I)
        OFB4PAL(IOFF+I) = OFB4ROL(I)
        OFB5PAL(IOFF+I) = OFB5ROL(I)
        OFBTPAL(IOFF+I) = OFBTROL(I)
        ABB1PAL(IOFF+I) = ABB1ROL(I)
        ABB2PAL(IOFF+I) = ABB2ROL(I)
        ABB3PAL(IOFF+I) = ABB3ROL(I)
        ABB4PAL(IOFF+I) = ABB4ROL(I)
        ABB5PAL(IOFF+I) = ABB5ROL(I)
        ABBTPAL(IOFF+I) = ABBTROL(I)
C
        EXS1PAK(IOFF+I) = EXS1ROW(I)
        EXS2PAK(IOFF+I) = EXS2ROW(I)
        EXS3PAK(IOFF+I) = EXS3ROW(I)
        EXS4PAK(IOFF+I) = EXS4ROW(I)
        EXS5PAK(IOFF+I) = EXS5ROW(I)
        EXSTPAK(IOFF+I) = EXSTROW(I)
        ODS1PAK(IOFF+I) = ODS1ROW(I)
        ODS2PAK(IOFF+I) = ODS2ROW(I)
        ODS3PAK(IOFF+I) = ODS3ROW(I)
        ODS4PAK(IOFF+I) = ODS4ROW(I)
        ODS5PAK(IOFF+I) = ODS5ROW(I)
        ODSTPAK(IOFF+I) = ODSTROW(I)
        ODSVPAK(IOFF+I) = ODSVROW(I)
        OFS1PAK(IOFF+I) = OFS1ROW(I)
        OFS2PAK(IOFF+I) = OFS2ROW(I)
        OFS3PAK(IOFF+I) = OFS3ROW(I)
        OFS4PAK(IOFF+I) = OFS4ROW(I)
        OFS5PAK(IOFF+I) = OFS5ROW(I)
        OFSTPAK(IOFF+I) = OFSTROW(I)
        ABS1PAK(IOFF+I) = ABS1ROW(I)
        ABS2PAK(IOFF+I) = ABS2ROW(I)
        ABS3PAK(IOFF+I) = ABS3ROW(I)
        ABS4PAK(IOFF+I) = ABS4ROW(I)
        ABS5PAK(IOFF+I) = ABS5ROW(I)
        ABSTPAK(IOFF+I) = ABSTROW(I)
C
        EXS1PAL(IOFF+I) = EXS1ROL(I)
        EXS2PAL(IOFF+I) = EXS2ROL(I)
        EXS3PAL(IOFF+I) = EXS3ROL(I)
        EXS4PAL(IOFF+I) = EXS4ROL(I)
        EXS5PAL(IOFF+I) = EXS5ROL(I)
        EXSTPAL(IOFF+I) = EXSTROL(I)
        ODS1PAL(IOFF+I) = ODS1ROL(I)
        ODS2PAL(IOFF+I) = ODS2ROL(I)
        ODS3PAL(IOFF+I) = ODS3ROL(I)
        ODS4PAL(IOFF+I) = ODS4ROL(I)
        ODS5PAL(IOFF+I) = ODS5ROL(I)
        ODSTPAL(IOFF+I) = ODSTROL(I)
        ODSVPAL(IOFF+I) = ODSVROL(I)
        OFS1PAL(IOFF+I) = OFS1ROL(I)
        OFS2PAL(IOFF+I) = OFS2ROL(I)
        OFS3PAL(IOFF+I) = OFS3ROL(I)
        OFS4PAL(IOFF+I) = OFS4ROL(I)
        OFS5PAL(IOFF+I) = OFS5ROL(I)
        OFSTPAL(IOFF+I) = OFSTROL(I)
        ABS1PAL(IOFF+I) = ABS1ROL(I)
        ABS2PAL(IOFF+I) = ABS2ROL(I)
        ABS3PAL(IOFF+I) = ABS3ROL(I)
        ABS4PAL(IOFF+I) = ABS4ROL(I)
        ABS5PAL(IOFF+I) = ABS5ROL(I)
        ABSTPAL(IOFF+I) = ABSTROL(I)
C
       ENDDO
#endif
#if defined use_cosp
! COSP INPUT
      DO L=1,ILEV
         DO I=IL1,IL2
            RMIXPAK (IOFF+I,L) = RMIXROW (I,L)
            SMIXPAK (IOFF+I,L) = SMIXROW (I,L)
            RREFPAK (IOFF+I,L) = RREFROW (I,L)
            SREFPAK (IOFF+I,L) = SREFROW (I,L)
         END DO ! I
      END DO ! L

! COSP OUTPUT

! Put the accumulating arrays from COSP into the output arrays

      DO I = IL1, IL2

! ISCCP fields
         IF (Lalbisccp)
     1        o_albisccp(IOFF+I) = albisccp(I)

         IF (Ltauisccp)
     1        o_tauisccp(IOFF+I) = tauisccp(I)

         IF (Lpctisccp)
     1        o_pctisccp(IOFF+I) = pctisccp(I)

         IF (Lcltisccp)
     1        o_cltisccp(IOFF+I) = cltisccp(I)


         IF (Lmeantbisccp)
     1        o_meantbisccp(IOFF+I) = meantbisccp(I)

         IF (Lmeantbclrisccp)
     1        o_meantbclrisccp(IOFF+I) = meantbclrisccp(I)

         IF (Lisccp_sim)
     1        o_sunl(IOFF+I) = sunl(I)

! CALIPSO fields

         IF (Lclhcalipso) THEN
            o_clhcalipso(IOFF+I)    = clhcalipso(I)
            ocnt_clhcalipso(IOFF+I) = cnt_clhcalipso(I)
         END IF

         IF (Lclmcalipso) THEN
            o_clmcalipso(IOFF+I)    = clmcalipso(I)
            ocnt_clmcalipso(IOFF+I) = cnt_clmcalipso(I)
         END IF

         IF (Lcllcalipso) THEN
            o_cllcalipso(IOFF+I)    = cllcalipso(I)
            ocnt_cllcalipso(IOFF+I) = cnt_cllcalipso(I)
         END IF

         IF (Lcltcalipso) THEN
            o_cltcalipso(IOFF+I)    = cltcalipso(I)
            ocnt_cltcalipso(IOFF+I) = cnt_cltcalipso(I)
         END IF

         IF (Lclhcalipsoliq) THEN
            o_clhcalipsoliq(IOFF+I)    = clhcalipsoliq(I)
            ocnt_clhcalipsoliq(IOFF+I) = cnt_clhcalipsoliq(I)
         END IF

         IF (Lclmcalipsoliq) THEN
            o_clmcalipsoliq(IOFF+I)    = clmcalipsoliq(I)
            ocnt_clmcalipsoliq(IOFF+I) = cnt_clmcalipsoliq(I)
         END IF

         IF (Lcllcalipsoliq) THEN
            o_cllcalipsoliq(IOFF+I)    = cllcalipsoliq(I)
            ocnt_cllcalipsoliq(IOFF+I) = cnt_cllcalipsoliq(I)
         END IF

         IF (Lcltcalipsoliq) THEN
            o_cltcalipsoliq(IOFF+I)    = cltcalipsoliq(I)
            ocnt_cltcalipsoliq(IOFF+I) = cnt_cltcalipsoliq(I)
         END IF

         IF (Lclhcalipsoice) THEN
            o_clhcalipsoice(IOFF+I)    = clhcalipsoice(I)
            ocnt_clhcalipsoice(IOFF+I) = cnt_clhcalipsoice(I)
         END IF

         IF (Lclmcalipsoice) THEN
            o_clmcalipsoice(IOFF+I)    = clmcalipsoice(I)
            ocnt_clmcalipsoice(IOFF+I) = cnt_clmcalipsoice(I)
         END IF

         IF (Lcllcalipsoice) THEN
            o_cllcalipsoice(IOFF+I)    = cllcalipsoice(I)
            ocnt_cllcalipsoice(IOFF+I) = cnt_cllcalipsoice(I)
         END IF

         IF (Lcltcalipsoice) THEN
            o_cltcalipsoice(IOFF+I)    = cltcalipsoice(I)
            ocnt_cltcalipsoice(IOFF+I) = cnt_cltcalipsoice(I)
         END IF

         IF (Lclhcalipsoun) THEN
            o_clhcalipsoun(IOFF+I)    = clhcalipsoun(I)
            ocnt_clhcalipsoun(IOFF+I) = cnt_clhcalipsoun(I)
         END IF

         IF (Lclmcalipsoun) THEN
            o_clmcalipsoun(IOFF+I)    = clmcalipsoun(I)
            ocnt_clmcalipsoun(IOFF+I) = cnt_clmcalipsoun(I)
         END IF

         IF (Lcllcalipsoun) THEN
            o_cllcalipsoun(IOFF+I)    = cllcalipsoun(I)
            ocnt_cllcalipsoun(IOFF+I) = cnt_cllcalipsoun(I)
         END IF

         IF (Lcltcalipsoun) THEN
            o_cltcalipsoun(IOFF+I)    = cltcalipsoun(I)
            ocnt_cltcalipsoun(IOFF+I) = cnt_cltcalipsoun(I)
         END IF


! CloudSat+CALIPSO fields
         IF (Lcltlidarradar) THEN
            o_cltlidarradar(IOFF+I)    = cltlidarradar(I)
            ocnt_cltlidarradar(IOFF+I) = cnt_cltlidarradar(I)
         END IF
      END DO


! Several special fields > 1D

! ISCCP
      IF (Lclisccp) THEN
         DO IP = 1, 7           !NPTOP
            DO IT = 1, 7        !NTAUCLD
               DO I = IL1, IL2
                  o_clisccp(IOFF+I,IT,IP) = clisccp(I,IT,IP)

               END DO
            END DO
         END DO
      END IF

! PARASOL
      IF (LparasolRefl) THEN
         DO IP = 1, PARASOL_NREFL
            DO I = IL1, IL2
               o_parasol_refl(IOFF+I,IP)    =  parasol_refl(I,IP)
               ocnt_parasol_refl(IOFF+I,IP) =  cnt_parasol_refl(I,IP)
            END DO
         END DO
      END IF

! 3D fields that might be interpolated to special levels or not
      IF (use_vgrid) THEN       ! Interpolate to specific heights
         DO N = 1, Nlr
            DO I = IL1, IL2
               IF (Lclcalipso) THEN
                  o_clcalipso(IOFF+I,N)    = clcalipso(I,N)
                  ocnt_clcalipso(IOFF+I,N) = cnt_clcalipso(I,N)
               END IF

               IF (Lclcalipsoliq) THEN
                  o_clcalipsoliq(IOFF+I,N) = clcalipsoliq(I,N)
                  ocnt_clcalipsoliq(IOFF+I,N) = cnt_clcalipsoliq(I,N)
               END IF

               IF (Lclcalipsoice) THEN
                  o_clcalipsoice(IOFF+I,N) = clcalipsoice(I,N)
                  ocnt_clcalipsoice(IOFF+I,N) = cnt_clcalipsoice(I,N)
               END IF

               IF (Lclcalipsoun) THEN
                  o_clcalipsoun(IOFF+I,N) = clcalipsoun(I,N)
                  ocnt_clcalipsoun(IOFF+I,N) = cnt_clcalipsoun(I,N)
               END IF

               IF (Lclcalipso2) THEN
                  o_clcalipso2(IOFF+I,N)    = clcalipso2(I,N)
                  ocnt_clcalipso2(IOFF+I,N) = cnt_clcalipso2(I,N)
               END IF

               IF (LcfadDbze94 .OR. LcfadLidarsr532) THEN
                  o_cosp_height_mask(IOFF+I,N) = cosp_height_mask(I,N)
               END IF
            END DO
         END DO
      ELSE
         DO N = 1, ILEV
            DO I = IL1, IL2
               IF (Lclcalipso) THEN
                   o_clcalipso(IOFF+I,N) =  clcalipso(I,N)
                   ocnt_clcalipso(IOFF+I,N) = cnt_clcalipso(I,N)
               END IF

               IF (Lclcalipsoliq) THEN
                  o_clcalipsoliq(IOFF+I,N) = clcalipsoliq(I,N)
                  ocnt_clcalipsoliq(IOFF+I,N) = cnt_clcalipsoliq(I,N)
               END IF

               IF (Lclcalipsoice) THEN
                  o_clcalipsoice(IOFF+I,N) = clcalipsoice(I,N)
                  ocnt_clcalipsoice(IOFF+I,N) = cnt_clcalipsoice(I,N)
               END IF

               IF (Lclcalipsoun) THEN
                  o_clcalipsoun(IOFF+I,N) = clcalipsoun(I,N)
                  ocnt_clcalipsoun(IOFF+I,N) = cnt_clcalipsoun(I,N)
               END IF

               IF (Lclcalipso2) THEN
                   o_clcalipso2(IOFF+I,N) = clcalipso2(I,N)
                   ocnt_clcalipso2(IOFF+I,N) = cnt_clcalipso2(I,N)
               END IF

               IF (LcfadDbze94 .OR. LcfadLidarsr532) THEN
                  o_cosp_height_mask(IOFF+I,N) = cosp_height_mask(I,N)
               END IF
            END DO
         END DO
      END IF

         DO N = 1, LIDAR_NTEMP
            DO I = IL1, IL2
               IF (Lclcalipsotmp) THEN
                  o_clcalipsotmp(IOFF+I,N) = clcalipsotmp(I,N)
                  ocnt_clcalipsotmp(IOFF+I,N) = cnt_clcalipsotmp(I,N)
               END IF

               IF (Lclcalipsotmpliq) THEN
                  o_clcalipsotmpliq(IOFF+I,N) = clcalipsotmpliq(I,N)
                  ocnt_clcalipsotmpliq(IOFF+I,N) =
     1                                       cnt_clcalipsotmpliq(I,N)
               END IF

               IF (Lclcalipsotmpice) THEN
                  o_clcalipsotmpice(IOFF+I,N) = clcalipsotmpice(I,N)
                  ocnt_clcalipsotmpice(IOFF+I,N) =
     1                                       cnt_clcalipsotmpice(I,N)
               END IF

               IF (Lclcalipsotmpun) THEN
                  o_clcalipsotmpun(IOFF+I,N) = clcalipsotmpun(I,N)
                  ocnt_clcalipsotmpun(IOFF+I,N) =
     1                                        cnt_clcalipsotmpun(I,N)
               END IF
            END DO ! I
         END DO ! N

! CFADS (2D histograms)

! CALIPSO

      IF (use_vgrid) THEN       ! Interpolate to specific heights
         IF (LcfadLidarsr532) THEN
            DO isr = 1, SR_BINS
               DO N = 1, Nlr
                  DO I = IL1, IL2
                     o_cfad_lidarsr532(IOFF+I,N,isr) =
     1                    cfad_lidarsr532(I,N,isr)
                  END DO
               END DO
            END DO
         ENDIF
      ELSE
         IF (LcfadLidarsr532) THEN
            DO isr = 1, SR_BINS
               DO N = 1, ILEV
                  DO I = IL1, IL2
                     o_cfad_lidarsr532(IOFF+I,N,isr) =
     1                    cfad_lidarsr532(I,N,isr)
                  END DO
               END DO
            END DO
         ENDIF
      END IF

! CloudSat

      IF (use_vgrid) THEN       ! Interpolate to specific heights
         IF (LcfadDbze94) THEN
            DO ize = 1, DBZE_BINS
               DO N = 1, Nlr
                  DO I = IL1, IL2
                     o_cfad_dbze94(IOFF+I,N,ize)=
     1                    cfad_dbze94(I,N,ize)
                  END DO
               END DO
            END DO
         ENDIF
      ELSE
         IF (LcfadDbze94) THEN
            DO ize = 1, DBZE_BINS
               DO N = 1, ILEV
                  DO I = IL1, IL2
                     o_cfad_dbze94(IOFF+I,N,ize) =
     1                    cfad_dbze94(I,N,ize)
                  END DO
               END DO
            END DO
         ENDIF
      END IF
      IF (Lceres_sim) THEN
         DO IP = 1, NCERES
            DO I = IL1, IL2
               o_ceres_cf(IOFF+I,IP)    = ceres_cf(I,IP)
               o_ceres_cnt(IOFF+I,IP)   = ceres_cnt(I,IP)
               o_ceres_ctp(IOFF+I,IP)   = ceres_ctp(I,IP)
               o_ceres_tau(IOFF+I,IP)   = ceres_tau(I,IP)
               o_ceres_lntau(IOFF+I,IP) = ceres_lntau(I,IP)
               o_ceres_lwp(IOFF+I,IP)   = ceres_lwp(I,IP)
               o_ceres_iwp(IOFF+I,IP)   = ceres_iwp(I,IP)
               o_ceres_cfl(IOFF+I,IP)   = ceres_cfl(I,IP)
               o_ceres_cfi(IOFF+I,IP)   = ceres_cfi(I,IP)
               o_ceres_cntl(IOFF+I,IP)  = ceres_cntl(I,IP)
               o_ceres_cnti(IOFF+I,IP)  = ceres_cnti(I,IP)
               o_ceres_rel(IOFF+I,IP)   = ceres_rel(I,IP)
               o_ceres_cdnc(IOFF+I,IP)  = ceres_cdnc(I,IP)
               o_ceres_clm(IOFF+I,IP)   = ceres_clm(I,IP)
               o_ceres_cntlm(IOFF+I,IP) = ceres_cntlm(I,IP)
            END DO ! I
         END DO ! IP
      END IF
      IF (Lceres_sim .AND. Lswath) THEN
         DO IP = 1, NCERES
            DO I = IL1, IL2
               os_ceres_cf(IOFF+I,IP)    = s_ceres_cf(I,IP)
               os_ceres_cnt(IOFF+I,IP)   = s_ceres_cnt(I,IP)
               os_ceres_ctp(IOFF+I,IP)   = s_ceres_ctp(I,IP)
               os_ceres_tau(IOFF+I,IP)   = s_ceres_tau(I,IP)
               os_ceres_lntau(IOFF+I,IP) = s_ceres_lntau(I,IP)
               os_ceres_lwp(IOFF+I,IP)   = s_ceres_lwp(I,IP)
               os_ceres_iwp(IOFF+I,IP)   = s_ceres_iwp(I,IP)
               os_ceres_cfl(IOFF+I,IP)   = s_ceres_cfl(I,IP)
               os_ceres_cfi(IOFF+I,IP)   = s_ceres_cfi(I,IP)
               os_ceres_cntl(IOFF+I,IP)  = s_ceres_cntl(I,IP)
               os_ceres_cnti(IOFF+I,IP)  = s_ceres_cnti(I,IP)
               os_ceres_rel(IOFF+I,IP)   = s_ceres_rel(I,IP)
               os_ceres_cdnc(IOFF+I,IP)  = s_ceres_cdnc(I,IP)
               os_ceres_clm(IOFF+I,IP)   = s_ceres_clm(I,IP)
               os_ceres_cntlm(IOFF+I,IP) = s_ceres_cntlm(I,IP)
            END DO ! I
         END DO ! IP
      END IF

! MISR
      IF (LclMISR) THEN
         DO I = IL1, IL2
            o_MISR_cldarea(IOFF+I)   = MISR_cldarea(I)
            o_MISR_mean_ztop(IOFF+I) = MISR_mean_ztop(I)
         END DO ! I

         DO IP = 1,MISR_N_CTH
            DO I = IL1, IL2
               o_dist_model_layertops(IOFF+I,IP) =
     1                                    dist_model_layertops(I,IP)
            END DO ! I
         END DO ! IP

         IBOX = 1
         DO IP = 1,MISR_N_CTH
            DO IT = 1, 7
               DO I = IL1, IL2
                  o_fq_MISR_TAU_v_CTH(IOFF+I,IBOX) =
     1                                    fq_MISR_TAU_v_CTH(I,IBOX)
               END DO ! I
               IBOX = IBOX + 1
            END DO ! IT
         END DO ! IP
      ENDIF ! LclMISR

! MODIS
      IF (Lcltmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Fraction_Total_Mean(IOFF+I) =
     1                             MODIS_Cloud_Fraction_Total_Mean(I)
         END DO ! I
      END IF

      IF (Lclwmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Fraction_Water_Mean(IOFF+I) =
     1                             MODIS_Cloud_Fraction_Water_Mean(I)
         END DO ! I
      END IF

      IF (Lclimodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Fraction_Ice_Mean(IOFF+I) =
     1                             MODIS_Cloud_Fraction_Ice_Mean(I)
         END DO ! I
      END IF

      IF (Lclhmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Fraction_High_Mean(IOFF+I) =
     1                             MODIS_Cloud_Fraction_High_Mean(I)
         END DO ! I
      END IF

      IF (Lclmmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Fraction_Mid_Mean(IOFF+I) =
     1                             MODIS_Cloud_Fraction_Mid_Mean(I)
         END DO ! I
      END IF

      IF (Lcllmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Fraction_Low_Mean(IOFF+I) =
     1                             MODIS_Cloud_Fraction_Low_Mean(I)
         END DO ! I
      END IF

      IF (Ltautmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Optical_Thickness_Total_Mean(IOFF+I) =
     1                           MODIS_Optical_Thickness_Total_Mean(I)
         END DO ! I
      END IF

      IF (Ltauwmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Optical_Thickness_Water_Mean(IOFF+I) =
     1                           MODIS_Optical_Thickness_Water_Mean(I)
         END DO ! I
      END IF

      IF (Ltauimodis) THEN
         DO I = IL1, IL2
            o_MODIS_Optical_Thickness_Ice_Mean(IOFF+I) =
     1                           MODIS_Optical_Thickness_Ice_Mean(I)
         END DO ! I
      END IF

      IF (Ltautlogmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Optical_Thickness_Total_LogMean(IOFF+I) =
     1                       MODIS_Optical_Thickness_Total_LogMean(I)
         END DO ! I
      END IF

      IF (Ltauwlogmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Optical_Thickness_Water_LogMean(IOFF+I) =
     1                        MODIS_Optical_Thickness_Water_LogMean(I)
         END DO ! I
      END IF

      IF (Ltauilogmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Optical_Thickness_Ice_LogMean(IOFF+I) =
     1                         MODIS_Optical_Thickness_Ice_LogMean(I)
         END DO ! I
      END IF

      IF (Lreffclwmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Particle_Size_Water_Mean(IOFF+I) =
     1                       MODIS_Cloud_Particle_Size_Water_Mean(I)
         END DO ! I
      END IF

      IF (Lreffclimodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Particle_Size_Ice_Mean(IOFF+I) =
     1                       MODIS_Cloud_Particle_Size_Ice_Mean(I)
         END DO ! I
      END IF

      IF (Lpctmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Cloud_Top_Pressure_Total_Mean(IOFF+I) =
     1                      MODIS_Cloud_Top_Pressure_Total_Mean(I)
         END DO ! I
      END IF

      IF (Llwpmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Liquid_Water_Path_Mean(IOFF+I) =
     1                                MODIS_Liquid_Water_Path_Mean(I)
         END DO ! I
      END IF

      IF (Liwpmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Ice_Water_Path_Mean(IOFF+I) =
     1                                MODIS_Ice_Water_Path_Mean(I)
         END DO ! I
      END IF

      IF (Lclmodis) THEN
         IBOX = 1
         DO IP = 1, a_numModisPressureBins
            DO IT = 1, a_numModisTauBins+1
               DO I = IL1, IL2
          o_MODIS_Optical_Thickness_vs_Cloud_Top_Pressure(IOFF+I,IBOX)=
     1             MODIS_Optical_Thickness_vs_Cloud_Top_Pressure(I,IBOX)
                END DO ! I
                IBOX = IBOX + 1
             END DO ! IT
          END DO ! IP
      END IF

      IF (Lcrimodis) THEN
         IBOX = 1
         DO IP = 1, a_numModisReffIceBins
            DO IT = 1, a_numModisTauBins+1
               DO I = IL1, IL2
                      o_MODIS_Optical_Thickness_vs_ReffICE(IOFF+I,IBOX)=
     1                        MODIS_Optical_Thickness_vs_reffICE(I,IBOX)
                END DO ! I
                IBOX = IBOX + 1
             END DO ! IT
          END DO ! IP
      END IF

      IF (Lcrlmodis) THEN
         IBOX = 1
         DO IP = 1, a_numModisReffLiqBins
            DO IT = 1, a_numModisTauBins+1
               DO I = IL1, IL2
                      o_MODIS_Optical_Thickness_vs_ReffLiq(IOFF+I,IBOX)=
     1                        MODIS_Optical_Thickness_vs_reffLiq(I,IBOX)
                END DO ! I
                IBOX = IBOX + 1
             END DO ! IT
          END DO ! IP
      END IF

      IF (Llcdncmodis) THEN
         DO I = IL1, IL2
            o_MODIS_Liq_CDNC_Mean(IOFF+I) =
     1                                MODIS_Liq_CDNC_Mean(I)
            o_MODIS_Liq_CDNC_GCM_Mean(IOFF+I) =
     1                                MODIS_Liq_CDNC_GCM_Mean(I)
            o_MODIS_Cloud_Fraction_Warm_Mean(IOFF+I) =
     1                     MODIS_Cloud_Fraction_Warm_Mean(I)
         END DO ! I
      END IF

#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
