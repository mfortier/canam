!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * JUL 27/19 - M.LAZARE. Remove {COM,CSP,CSN} parmsub variables.
C     * NOV 28/03 - M.LAZARE/ NEW VERSION FOR GCM13C, WITH GCMPARM
C     *            L.SOLHEIM. VARIABLES REPLACED BY EXPLICIT PARAMETER 
C     *                       STATEMENT VALUES.
C     * MAY 26/03 - M.LAZARE. NEW COMMON DECK (COM13DB) CONTAINING DECLARATIONS
C     *                       PERTAINING TO NON-LINEAR DYNAMICS.
C     *                       LONGITUDE AND LEVEL ARE EXPLICITLY SPLIT
C     *                       TO FACILITATE CONVERSION TO/FROM POINTERS.
C======================================================================
C     * GRID SLICE FIELDS.

C     * THE FOLLOWING FIELDS ARE MAPPED INTO THE "FOUR2" WORK ARRAY
C     * IN MHANL7.
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      COMMON /GRD/ TVTG  (ILG_TD,ILEV)
      COMMON /GRD/ SVTG  (ILG_TD,LEVS)
      COMMON /GRD/ XVTG  (ILG_TD,ILEV,NTRACA)
      COMMON /GRD/ UTMP  (ILG_TD,ILEV)
      COMMON /GRD/ VTMP  (ILG_TD,ILEV)

C     * THE FOLLOWING FIELDS ARE MAPPED INTO THE "FOUR3" WORK ARRAY
C     * IN MHANL7.

      COMMON /GRD/ TUTG  (ILG_TD,ILEV)
      COMMON /GRD/ SUTG  (ILG_TD,LEVS)
      COMMON /GRD/ XUTG  (ILG_TD,ILEV,NTRACA)
      COMMON /GRD/ EG    (ILG_TD,ILEV)

C     * THE FOLLOWING FIELDS ARE MAPPED INTO THE "FOUR1" WORK ARRAY
C     * IN MHANL7.

      COMMON /GRD/ TTGD   (ILG_TD,ILEV)
      COMMON /GRD/ ESTGD  (ILG_TD,LEVS)
      COMMON /GRD/ TRACTGD(ILG_TD,ILEV,NTRACA)
      COMMON /GRD/ VTGD   (ILG_TD,ILEV)
      COMMON /GRD/ UTGD   (ILG_TD,ILEV)
      COMMON /GRD/ PSTGD  (ILG_TD)
C
      COMMON /GRD/ PRESSGD(ILG_TD)
      COMMON /GRD/ PGD    (ILG_TD,ILEV),CGD    (ILG_TD,ILEV)
      COMMON /GRD/ TGD    (ILG_TD,ILEV),ESGD   (ILG_TD,LEVS)
      COMMON /GRD/ TRACGD (ILG_TD,ILEV,NTRACA)
      COMMON /GRD/ PSDPGD (ILG_TD)
      COMMON /GRD/ UGD    (ILG_TD,ILEV),VGD    (ILG_TD,ILEV)
      COMMON /GRD/ PSDLGD (ILG_TD)
!$OMP THREADPRIVATE (/GRD/)
C
C     * THE FIELDS "DSGJ" AND "DSHJ" ARE USED IN BOTH THE PHYSICS
C     * AND NON-LINEAR DYNAMICS.
C
      COMMON /GR1/ DSGJ (IDLM), DSHJ (IDLM), DLNSGJ(IDLM)
      COMMON /GR1/ D1SGJ(IDLM), A1SGJ(IDLM), B1SGJ (IDLM)
      COMMON /GR1/ D2SGJ(IDLM), A2SGJ(IDLM), B2SGJ (IDLM)
!$OMP THREADPRIVATE (/GR1/)
C=====================================================================
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
