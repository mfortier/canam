!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * OCT 01/2018 - S.KHARIN. Add XSFX/XTVI to AJUST.
C     * JUL 27/2019 - M.LAZARE. Remove {COM,CSP,CSN} parmsub variables.
C     * FEB 27/2018 - M.LAZARE. Replace {NUPS,NSUPS} by {BWGK,QFSO} in
C     *                         AJUST common block shared between
C     *                         gcm18.dk and core18p.dk (hence don't have
C     *                         to be passed in the call).
C     * MAY 05/2010 - M.LAZARE. New version for gcm15i:
C     *                         - Add "MW" array to "IXCONT" common
C     *                           block (read-in as namelist).
C     * FEB 23/2009 - M.LAZARE. PREVIOUS VERSION COM15H FOR GCM15H:
C     *                         - REMOVE QCTOFF FROM CONSQ COMMON BLOCK.
C     * DEC 23/2007 - M.LAZARE. PREVIOUS VERSION COM15G FOR GCM15G:
C     *                         - "SL" AND "SLSAVE" CONDITIONAL
C     *                            CODE REMOVED.
C     *                         - UNUSED WORKSPACE POINTERS FOR
C     *                           S/L AND PHYSICS REMOVED.
C     * DEC 23/2006 - M.LAZARE. PREVIOUS VERSION COM15F FOR GCM15F.
C     *                         - ADD IHFSAVE COMMON DECK.
C     * JUL 02/2006 - M.LAZARE. - PARAMSD COMMON BLOCK (NATIVE REAL)
C     *                           INSTEAD OF PARAMS (REAL*8 FOR NOW,
C     *                           PHYSICS).
C     *                         - UNUSED COMMON BLOCK "ECCENT" REMOVED.
C     *                         - COMMON BLOCK "EPSICE" REMOVED SINCE
C     *                           ONLY QMIN WAS USED AND IT IS NOW
C     *                           PASSED DIRECTLY THROUGH ALL THE CALLS
C     *                           TO THE CORE ROUTINES.
C     * APR 01/2004 - J.SCINOCCA/ PREVIOUS VERSION COM13C FOR GCM15C/D/E.
C     *               M.LAZARE. - "CONTRAC" ADDED TO "IXCONT" COMMON BLOCK.
C     *                         - "ITRLVS" AND "ITRLVF" ADDED TO IXCONT.
C     *                         - "FIZ" REMOVED.
C     *                         - LC,LG,LO MOVED TO GCM DRIVER.
C     *                         - "ISTR" ADDED TO "INTVLS" COMMON BLOCK.
C     *                         - "IT" COMPONENT OF "SCPNT" COMMON BLOCK
C     *                           REMOVED (UNUSED). SAME FOR ITS LENGTH.
C     *                         - "CONSQ" COMMON BLOCK MODIFIED TO ADD
C     *                           ADDITIONAL PARAMETERS FOR CONSERVATION.
C     *                         - NTRACA USED INSTEAD OF NTRAC FOR CALCULATION
C     *                           OF SPECTRAL ADVECTION AND SWAPPING ARRAYS.
C     * NOV 28/2003 - M.LAZARE/  NEW VERSION FOR THE IBM (COM13B):
C     *               L.SOLHEIM.
C     *                       - SUBSTITUTE PARAMETER STATEMENT VARIABLES
C     *                         INSTEAD OF GCMPARM VARIABLES.
C     *                       - REVISE "GAUSS" COMMON BLOCK TO NOW CONTAIN
C     *                         "A" FIELDS ("A" FOR "ALL" NODES) AND
C     *                         REMOVE "X" WORKSPACE.
C     *                       - ADD "SPEC1", "SPEC2", "SPEC3" WORKSPACE.
C     *                       - "P2" REMOVED.
C     *                       - CALL TO "COMPAK7A" REMOVED.
C     *                       - ILAT->NLAT IN "SIZES" COMMON BLOCK.
C     *                       - "ILG" -> "ILGSL" FOR S/L.
C     *                       - "MULTI" CONDITIONAL CODE REMOVED.
C     *                       - ALL "THREADPRIVATE" COMMON BLOCKS MOVED TO NEW
C     *                         COM13PB OR COM13DB. CODE REORGANIZED.
C     *                       - "TIMING" CODE REMOVED.
C     *                       - TASKLOCAL'S CONVERTED TO THREADPRIVATE'S.
C     *                       - DECLARATION OF SPTP,SPTP_P,SPTP_D,IPTN
C     *                         MOVED TO GCM13B SINCE DMA MOVED OUTSIDE
C     *                         OF CORE ROUTINES.
C     *                       - OZZX,OZZXJ REMOVED.
C     *                       - "LO" ADDED TO "LSCOM" COMMON BLOCK FOR
C     *                         LEVEL INFORMATION OF INPUT OZONE FIELD.
C     * OCT 02/01 - M.LAZARE. PREVIOUS COMMON DECK COM13A FOR MULTI-TASKED S/L
C     *                       FOR GCM13A.
C====================================================================
C     * COMPLEX SPECTRAL ARRAYS.
C
      COMPLEX P,C,T,ES, PT,CT,TT,EST, PHIS,PRESS, PS,PST
      COMPLEX TRAC,TRACT
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
      COMMON /SP/ PHIS (LA)
      COMMON /SP/ TT   (RL),EST  (RS)
      COMMON /SP/ TRACT(RL,NTRACA)
      COMMON /SP/ PT   (RL),CT   (RL)
      COMMON /SP/ PST  (LA)
      COMMON /SP/ PRESS(LA)
      COMMON /SP/ PS   (LA)
      COMMON /SP/ P    (RL),C    (RL)
      COMMON /SP/ T    (RL),ES   (RS)
      COMMON /SP/ TRAC (RL,NTRACA)
C====================================================================
C     * ARRAYS USED TO HOLD INTERMEDIATE VALUES OF TRANSFORMS.
C     * "SPEC1" IS FILLED PRIOR TO FORWARD LEGENDRE TRANSFORM LOOPS.
C
C     * "SPEC2" IS SIMILAR TO "SPEC1" BUT ONLY APPLIES TO {P,C} AS
C     * PART OF THE TRANSFORM PROCESS.
C
C     * "WRKT" IS WORK SPACE USED IN SWAPPING ROUTINES.
C
      COMPLEX SPEC1(3*ILEV+LEVS+1
     +              +NTRACA*ILEV
     +              ,LA               )
      COMPLEX SPEC2(2*ILEV,LA)
      COMPLEX WRKT( (LA+1) * (3*ILEV+LEVS+2
     +              +NTRACA*ILEV
     +                             )   )
C====================================================================
C     * GAUSSIAN FIELDS (PHYSICS GRID).
C
C     * THESE ARE COMPLETE SETS.
C
      REAL*8 SLA,CLA,WLA,WOCSLA,RADLA
      COMMON /GAUS/ SLA    (NLAT)
      COMMON /GAUS/ CLA    (NLAT)
      COMMON /GAUS/ WLA    (NLAT)
      COMMON /GAUS/ WOCSLA (NLAT)
      COMMON /GAUS/ RADLA  (NLAT)
C
      REAL*8 SLDA,CLDA,WLDA,WOCSLDA,RADLDA
      COMMON /GAUS/ SLDA   (NLATD)
      COMMON /GAUS/ CLDA   (NLATD)
      COMMON /GAUS/ WLDA   (NLATD)
      COMMON /GAUS/ WOCSLDA(NLATD)
      COMMON /GAUS/ RADLDA (NLATD)
C
      REAL*8 SLX,CLX,WLX,WOCSLX,RADLX
      COMMON /GAUS/ SLX    (NLAT)
      COMMON /GAUS/ CLX    (NLAT)
      COMMON /GAUS/ WLX    (NLAT)
      COMMON /GAUS/ WOCSLX (NLAT)
      COMMON /GAUS/ RADLX  (NLAT)
C
      REAL*8 SLDX,CLDX,WLDX,WOCSLDX,RADLDX
      COMMON /GAUS/ SLDX   (NLATD)
      COMMON /GAUS/ CLDX   (NLATD)
      COMMON /GAUS/ WLDX   (NLATD)
      COMMON /GAUS/ WOCSLDX(NLATD)
      COMMON /GAUS/ RADLDX (NLATD)
C
C     * THESE ARE DEFINED LOCALLY ON EACH NODE.
C
      REAL*8 SL,CL,WL,WOCSL,RADL
      COMMON /GAUS/ SL     (NLATJ,NTASK_TP)
      COMMON /GAUS/ CL     (NLATJ,NTASK_TP)
      COMMON /GAUS/ WL     (NLATJ,NTASK_TP)
      COMMON /GAUS/ WOCSL  (NLATJ,NTASK_TP)
      COMMON /GAUS/ RADL   (NLATJ,NTASK_TP)
C
      REAL*8 SLD,CLD,WLD,WOCSLD,RADLD
      COMMON /GAUS/ SLD    (NLATJD,NTASK_TD)
      COMMON /GAUS/ CLD    (NLATJD,NTASK_TD)
      COMMON /GAUS/ WLD    (NLATJD,NTASK_TD)
      COMMON /GAUS/ WOCSLD (NLATJD,NTASK_TD)
      COMMON /GAUS/ RADLD  (NLATJD,NTASK_TD)

      REAL*8 RADBU,RADBD,RADBXU,RADBXD,RADBX
      COMMON /GAUS/ RADBU (NLATJ,NTASK_TP)
      COMMON /GAUS/ RADBD (NLATJ,NTASK_TP)
      COMMON /GAUS/ RADBXU(NLAT)
      COMMON /GAUS/ RADBXD(NLAT)
      COMMON /GAUS/ RADBX (NLAT+1)

C====================================================================
C     * TRANSFORMS AND IMPLICIT ARRAYS.  ALP=(LA+LM), AI=(NSMAX*ILEV)
C
      REAL*8 ALP,DALP,DELALP,EPSI
      REAL*8 ALPD,DALPD,DELALPD
      REAL*8 ALPX,DALPX,DELALPX
      REAL*8 ALPDX,DALPDX,DELALPDX
C
      COMMON /ALPCOM/ ALP    (NLAT,IRAM)
      COMMON /ALPCOM/ DALP   (NLAT,IRAM)
      COMMON /ALPCOM/ DELALP (NLAT,IRAM)
      COMMON /ALPCOM/ EPSI   (IRAM)
C
      COMMON /ALPCOMD/ALPD   (NLATD,IRAM)
      COMMON /ALPCOMD/DALPD  (NLATD,IRAM)
      COMMON /ALPCOMD/DELALPD(NLATD,IRAM)
C
      COMMON /ALPCOMX/ALPX   (NLAT,IRAM), DALPX(NLAT,IRAM)
      COMMON /ALPCOMX/DELALPX(NLAT,IRAM)
C
      COMMON /ALPCOMDX/ALPDX   (NLATD,IRAM), DALPDX(NLATD,IRAM)
      COMMON /ALPCOMDX/DELALPDX(NLATD,IRAM)
C====================================================================
C     * WATER BUDGET CONSERVATION FIELDS AGGREGATED-UP ARE DIMENSIONED BY THE
C     * NUMBER OF PHYSICS TASKS.
C
      COMMON /AJUST/BWGK(NTASK_P),QFSO(NTASK_P),
     1     XSFX(NTRAC,NTASK_P),XTVI(NTRAC,NTASK_P)
C====================================================================
C     * POINTERS TO SCRATCH ARRAYS USED IN VECTORIZATION.
C     * NOW DEFINED IN THE COMMON BLOCK /SCPNT/
C
      COMMON /SCPNT/ID (60), IR (60), IE (60)
      COMMON /SCPNT/IXD(60), IXP(60)
C
      COMMON /LENGTH_SC/ LEN_IR, LEN_IE, LEN_MAXD, LEN_MAXP
C
      COMMON /LSCOM/ LSRTOTAL(2,LMTOTAL+1), MINDEX(LM)
      COMMON /LSCOM/ LSR(2,LM+1), LS(ILEV), LH(ILEV)
C
C     * VARIOUS FIELDS ASSOCIATED TO THE VERTICAL DICRETIZATION.
C
      COMMON /VERTCD/ SGB(ILEV),SHB(ILEV),SG(ILEV)  ,SH(ILEV)
      COMMON /VERTCD/ ACG(ILEV),BCG(ILEV),ACH (ILEV),BCH (ILEV)
      COMMON /VERTCD/ AG (ILEV),AH (ILEV),BG  (ILEV),BH  (ILEV)
      COMMON /VERTCD/ DB (ILEV),D2B(ILEV)
C=====================================================================
C     * COMMON BLOCKS FOR MODEL PARAMETERS AND TRIGO. FUNC. FOR VFFT.
C
      LOGICAL LRCM
      COMMON /FFTRIG/ IFAX(10),TRIGS(ILGSL)
      COMMON/FFTRIGD/ IFAXD(10),TRIGSD(ILGSLD)
      COMMON /PROCES/ IDIV
      COMMON /PARAM2/ FP,  FC,  FT,   FS,  FX, FPS
      COMMON /PARAHD/ DISP,DISC,DIST,DISES,DISX, SCRIT
      COMMON /SIZES/  LONSLX,NLATX,ILEVX,LEVSX,LRLMT,ICOORD,LAY,
     1                PTOIT,MOIST
      COMMON /INTVLS/ ISSP,ISGG,ISRAD,ISBEG,ISEN,ISTR,IEPR,ICSW,ICLW,
     1                JLATPR
      COMMON /IHFSAVE/ISHF,ISMON,ISST
      COMMON/IOPTION/ IOCEAN,ICEMOD,ITRAC
      COMMON /PARAMS/ WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1                RGASV,CPRESV
      COMMON /PARAM1/ PI,     RVORD, TFREZ, HS,   HV,   DAYLNT
      COMMON /TIMES/  DELT,IFDIFF,KSTART,KTOTAL,NEWRUN,
     1                NDAYS,NSECS,IDAY,LDAY,MDAY,MDAYT,INCD
      COMMON /MODEL/  LRCM
C
C     * COMMON BLOCK FOR MOISTURE CONSERVATION (SHARED WITH
C     * PHYSIC_,RSTART_,INGCM_ AND ENERSV_).
C
      COMMON /CONSQ/ QSRCRAT,QSRCRM1,WPP,WP0,WPM,TFICQM,QSRC0,QSRCM,
     1               QSCL0,QSCLM
C
C     * COMMON BLOCK FOR KEEPING TRACK OF "REAL" TIME, TO BE USED
C     * IN AMIP-LIKE RUNS.
C
      COMMON /KEEPTIM/ IYEAR,IMDH,MYRSSTI,ISAVDTS
C
C     * CONTROL ARRAYS USED TO SELECTIVELY ADVECT, OPERATE
C     * PHYSICS, SPECIFY SURFACE FLUXES, NAME AND INITIALIZE EACH TRACER.
C
      INTEGER ITRNAM,ITRADV,ITRPHS,ITRSRF,ITRIC,ITRWET,ITRCCH,ITRDRY
      REAL CONTRAC,MW
      COMMON /IXCONT/ ITRNAM (NTRAC), ITRADV(NTRAC), ITRPHS(NTRAC),
     1                ITRSRF (NTRAC), ITRIC (NTRAC), ITRWET(NTRAC),
     2                ITRCCH (NTRAC), ITRDRY(NTRAC),
     3                ITRLVS (NTRAC), ITRLVF(NTRAC),
     4                CONTRAC(NTRAC), MW    (NTRAC)
C
C     * INDXNA,INDXA - INDEX VALUES OF NONADVECTED/ADVECTED SPECTRAL TRACERS
C
      INTEGER INDXNA,INDXA
      COMMON /INDEX/ INDXNA(NTRACN)
      COMMON /INDEX/ INDXA (NTRACA)
C
C     * TRACNA - TWO TIME-LEVEL STORAGE OF NON-ADVECTED TRACERS ON THE GRID.
C
      COMMON /NAFLDS/TRACNA(IP0J*ILEV*NTRACN+1,2)
C
C     * COMMON BLOCK FOR TRACER CONSERVATION (ITRVAR.NE."   Q").
C
      COMMON /CONSTR/XSRCRAT(NTRAC),XSRCRM1(NTRAC),XPP(NTRAC),
     1               XP0(NTRAC),XPM(NTRAC),TFICXM(NTRAC),
     2               XREF(NTRAC),XPOW(NTRAC),XMIN(NTRAC),
     3               TMIN(NTRAC),XSRC0(NTRAC),XSRCM(NTRAC),
     4               XSCL0(NTRAC),XSCLM(NTRAC)

      INTEGER PHYS_PARM_IU
      COMMON /PHYSPARMIU/PHYS_PARM_IU

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
