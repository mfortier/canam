#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * Mar 16/2019 - J. Cole     - Add  3-hour 2D fields (CF3hr,E3hr)
C     * Nov 29/2018 - J. Cole     - Add/adjust tendencies
C     * Nov 02/2018 - J. Cole     - Add radiative flux profiles
C     * Nov 20/2018 - M.Lazare.   - Ensure ECO2PAK and ECO2PAL are defined for all cases.
C     * Nov 03/2018 - M.Lazare.   - Added GSNO and FNLA.
C     *                           - Added 3HR save fields.
C     * Oct 01/2018 - S.Kharin.   - Add XTVIPAS for sampled burdens.
C     * Aug 23/2018 - Vivek Arora - Remove PFHC and PEXF
C     * Aug 21/2018 - M.Lazare.     Remove {ALSW,ALLW}.
C     * Aug 14/2018 - M.Lazare.     Remove MASKPAK,GTPAL.
C     * Aug 14/2018 - M.Lazare.     Remove {GTM,SICP,SICM,SICNP,SICM}.
C     * Aug 01/2018 - M.Lazare.     Remove {OBEG,OBWG,RES,GC}.
C     * Jul 30/2018 - M.Lazare.   - Unused FMIROW removed.
C     *                           - Removed non-transient aerosol emission cpp blocks.
C     * Mar 09/2018 - M.Lazare.   - BEG  and BWG  changed from PAK/ROW to
C     *                             PAL/ROL.
C     * Feb 27/2018 - M.Lazare.   - QFSL and BEGL changed from PAK/ROW to
C     *                             PAL/ROL.
C     *                           - Added {BEGK,BWGK,BWGL,QFSO}.
C     * Apr 19/2018 - M.Lazare.    - Add ECO2 arrays (current and target).
C     * Feb 06/2018 - M.Lazare.    - Add {FTOX,FTOY} and {TDOX,TDOY}.
C     * Nov 01/2017 - J. Cole.     - Add arrays to support CMIP6 stratospheric
C     *                              aerosols.
C     * Aug 10/2017 - M.Lazare.    - Add FNPAT.
C     *                            - FLAK->{FLKR,FLKU}.
C     *                            - LICN->GICN.
C     *                            - Add lakes arrays.
C     *                            - WSNOPAT dimension changed from
C     *                              to IM to support use over
C     *                              sea/lake ice.
C     *                            - Add local RFLNDPAK to pass to
C     *                              coupler init for non-ocean fraction.
C     * Aug 07/2017 - M.Lazare.    Initial Git verison.
C     * Mar 26/2015 - M.Lazare.    Final version for gcm18:
C     *                            - Unused {CSTPAL,CLTPAL} removed.
C     *                            - Add (for NEMO/CICE support):
C     *                              OBEGPAL,OBWGPAL,BEGOPAL,BWGOPAL,
C     *                              BEGIPAL,HFLIPAL,
C     *                              HSEAPAL,RAINSPAL,SNOWSPAL.
C     *                            - Remove: FTILPAK.
C     *                            - Add (for harmonization of field
C     *                              capacity and wilting point between
C     *                              CTEM and CLASS): THLWPAT.
C     *                            - Add (for soil colour index look-up
C     *                              for land surface albedo):
C     *                              ALGDVPAT,ALGDNPAT,
C     *                              ALGWVPAT,ALGWNPAT,SOCIPAT.
C     *                            - Add (for fractional land/water/ice):
C     *                              SALBPAT,CSALPAT,EMISPAK,EMISPAT,
C     *                              WRKAPAL,WRKBPAL,SNOPAKO.
C     *                            - Add (for PLA):
C     *                              PSVVPAK,PDEVPAK,PDIVPAK,PREVPAK,
C     *                              PRIVPAK.
C     * Feb 04/2014 - M.Lazare.    Interim version for gcm18:
C     *                            - NTLD used instead of IM for land-only
C     *                              mosaic fields.
C     *                            - FLAKPAK,FLNDPAK,GTPAT,LICNPAK added.
C     *                            - {THLQPAK,THICPAK} removed.
C     * Nov 19/2013 - M.Lazare.    Cosmetic: Remove {GFLX,GA,HBL,ILMO,PET,UE,
C     *                                      WTAB,ROFS,ROFB) "PAT" arrays.
C     * Jul 10/2013 - M.Lazare/    Previous version compak11 for gcm17:
C     *               K.Vonsalzen/ - FSF added as prognostic field
C     *               J.Cole/        rather than residual.
C     *                            - Extra diagnostic microphysics
C     *                              fields added:
C     *                              {SEDI,RLIQ,RICE,RLNC,CLIQ,CICE,
C     *                               VSEDI,RELIQ,REICE,CLDLIQ,CLDICE,CTLNC,CLLNC}.
C     *                            - New emission fields:
C     *                              {SBIO,SSHI,OBIO,OSHI,BBIO,BSHI} and
C     *                              required altitude field ALTI.
C     *                            - Many new aerosol diagnostic fields
C     *                              for PAM, including those for cpp options:
C     *                              "pfrc", "xtrapla1" and "xtrapla2".
C     *                            - The following fields were removed
C     *                              from the "aodpth" cpp option:
C     *                              {SAB1,SAB2,SAB3,SAB4,SAB5,SABT} and
C     *                              {SAS1,SAS2,SAS3,SAS4,SAS5,SAST},
C     *                              (both PAK and PAL).
C     *                            - Due to the implementation of the mosaic
C     *                              for CLASS_v3.6, prognostic fields
C     *                              were changed from PAK/ROW to PAT/ROT,
C     *                              with an extra "IM" dimension (for
C     *                              the number of mosaic tiles).
C     *                            - The instantaneous band-mean values for
C     *                              solar fields are now prognostic
C     *                              as well: {FSDB,FSFB,CSDB,CSFB,FSSB,FSSCB}.
C     *                            - Removed "histemi" fields.
C     * NB: The following are intermediate revisions to frozen gcm16 code
C     *     (upwardly compatible) to support the new CLASS version in development:
C     * Oct 19/2011 - M.Lazare.    - Add THR,THM,BI,PSIS,GRKS,THRA,HCPS,TCS,THFC,
C     *                              PSIW,ALGD,ALGW,ZBTW,ISND,IGDR.
C     * Oct 07/2011 - M.Lazare.    - Add GFLX,GA,HBL,PET,ILMO,ROFB,ROFS,UE,WTAB.
C     * Jul 13/2011 - E.Chan.      - For selected variables, add new packed tile
C     *                              versions (PAT) or convert from PAK to PAT.
C     *                            - Add TPND, ZPND, TAV, QAV, WSNO, TSFS.
C     * MAY 07/2012 - M.LAZARE/    PREVIOUS VERSION COMPAK10 FOR GCM16:
C     *               K.VONSALZEN/ - MODIFY FIELDS TO SUPPORT A NEWER
C     *               J.COLE/        VERSION OF COSP WHICH INCLUDES
C     *               Y.PENG.        THE MODIS, MISR AND CERES AND
C     *                              SAVE THE APPROPRIATE OUTPUT.
C     *                            - REMOVE {FSA,FLA,FSTC,FLTC} AND
C     *                              REPLACE BY {FSR,OLR,FSRC,OLRC}.
C     *                            - NEW CONDITIONAL DIAGNOSTIC OUTPUT
C     *                              UNDER CONTROL OF "XTRADUST" AND
C     *                              "AODPTH".
C     *                            - ADDITIONAL MAM RADIATION OUTPUT
C     *                              FIELDS {FSAN,FLAN}.
C     *                            - ADDITIONAL CORRELATED-K OUTPUT
C     *                              FIELDS: "FLG", "FDLC" AND "FLAM".
C     *                            - "FLGROL_R" IS ACTUAL CALCULATED
C     *                              RADIATIVE FORCING TERM INSTEAD
C     *                              OF "FDLROL_R-SIGMA*T**4".
C     *                            - INCLUDE "ISEED" FOR RANDOM NUMBER
C     *                              GENERATOR SEED NOW CALCULATED
C     *                              IN MODEL DRIVER INSTEAD OF PHYSICS.
C     * MAY 02/2010 - M.LAZARE/    PREVIOUS VERSION COMPAK9I FOR GCM15I:
C     *               K.VONSALZEN/ - ADD FSOPAL/FSOROL ("FSOL").
C     *               J.COLE.      - ADD FIELDS {RMIX,SMIX,RREF,SREF}
C     *                              FOR COSP INPUT, MANY FIELDS FOR
C     *                              COSP OUTPUT (WITH DIFFERENT OPTIONS)
C     *                              AND REMOVE PREVIOUS DIRECT ISCCP
C     *                              FIELDS.
C     *                            - ADD NEW DIAGNOSTIC FIELDS:
C     *                              SWA (PAK/ROW AND PAL/ROL),SWX,SWXU,
C     *                              SWXV,SRH,SRHN,SRHX,FSDC,FSSC,FDLC
C     *                              AND REMOVE: SWMX.
C     *                            - FOR CONVECTION, ADD: DMCD,DMCU AND
C     *                              REMOVE: ACMT,DCMT,SCMT,PCPS,CLDS,
C     *                              LHRD,LHRS,SHRD,SHRS.
C     *                            - ADD FIELDS FOR CTEM UNDER CONTROL
C     *                              OF NEW CPP DIRECTIVE: "COUPLER_CTEM".
C     *                            - ADD NEW FIELDS FOR COUPLER: OFSG,
C     *                              PHIS,PMSL,XSRF.
C     *                            - PREVIOUS UPDATE DIRECIVE "HISTEMI"
C     *                              CONVERTED TO CPP:
C     *                              "TRANSIENT_AEROSOL_EMISSIONS" WITH
C     *                              FURTHER CHOICE OF CPP DIRECTIVES
C     *                              "HISTEMI" FOR HISTORICAL OR
C     *                              "EMISTS" FOR FUTURE EMISSIONS.
C     *                              THE "HISTEMI" FIELDS ARE THE
C     *                              SAME AS PREVIOUSLY. FOR "EMISTS",
C     *                              THE FOLLOWING ARE ADDED (BOTH PAK/ROW
C     *                              AND PAL/ROL SINCE INTERPOLATED):
C     *                              SAIR,SSFC,SSTK,SFIR,OAIR,OSFC,OSTK,OFIR,
C     *                              BAIR,BSFC,BSTK,BFIR. THE FIRST LETTER
C     *                              INDICATES THE SPECIES ("B" FOR BLACK
C     *                              CARBON, "O" FOR ORGANIC CARBON AND
C     *                              "S" FOR SULFUR), WHILE FOR EACH OF
C     *                              THESE, THERE ARE "AIR" FOR AIRCRAFT,
C     *                              "SFC" FOR SURFACE, "STK" FOR STACK
C     *                              AND "FIR" FOR FIRE, EACH HAVING
C     *                              DIFFERENT EMISSION HEIGHTS.
C     *                            - FOR THE CHEMISTRY, THE FOLLOWING
C     *                              FIELDS HAVE BEEN ADDED:
C     *                              DDD,DDB,DDO,DDS,WDLD,WDLB,WDLO,WDLS,
C     *                              WDDD,WDDB,WDDO,WDDS,WDSD,WDSB,WDSO,
C     *                              WDSS,ESD,ESFS,EAIS,ESTS,EFIS,ESFB,
C     *                              EAIB,ESTB,EFIB,ESFO,EAIO,ESTO,EFIO
C     *                              AND THE FOLLOWING HAVE BEEN REMOVED:
C     *                              ASFS,ASHP,ASO3,AWDS,DAFX,DCFX,DDA,DDC,
C     *                              ESBT,ESFF,EOFF,EBFF,EOBB,EBBB,ESWF,
C     *                              SFD,SFS,WDDA,WDDC,WDLA,WDLC,WDSA,WDSC,
C     *                              CDPH,CLPH,CSPH.
C     *                            - FAIRPAK/FAIRROW AND FAIRPAL/FAIRROL
C     *                              ADDED FOR AIRCRAFT EMISSIONS
C     *                            - O3CPAK/O3CROW AND O3CPAL/O3CROL
C     *                              ADDED FOR CHEMICAL OZONE INTERPOLATION.
C     *                            - UPDATE DIRECTIVES TURNED INTO
C     *                              CPP DIRECTIVES.
C     * FEB 20/2009 - M.LAZARE.    PREVIOUS VERSION COMPAK9H FOR GCM15H:
C     *                            - ADD NEW FIELDS FOR EMISSIONS: EOFF,
C     *                              EBFF,EOBB,EBBB.
C     *                            - ADD NEW FIELDS FOR DIAGNOSTICS OF
C     *                              CONSERVATION: QTPT,XTPT.
C     *                            - ADD NEW FIELD FOR CHEMISTRY: SFRC.
C     *                            - ADD NEW DIAGNOSTIC CLOUD FIELDS
C     *                              (USING OPTICAL DEPTH CUTOFF):
C     *                              CLDO (BOTH PAK AND PAL).
C     *                            - REORGANIZE EMISSION FORCING FIELDS
C     *                              DEPENDANT WHETHER ARE UNDER
C     *                              HISTORICAL EMISSIONS (%DF HISTEMI)
C     *                              OR NOT.
C     *                            - ADD FIELDS FOR EXPLOSIVE VOLCANOES:
C     *                              VTAU AND TROP UNDER CONTROL OF
C     *                              "%DF EXPLVOL". EACH HAVE BOTH PAK
C     *                              AND PAL.
C     * APR 21/2008 - L.SOLHEIM/   PREVIOUS VERSION COMPAK9G FOR GCM15G:
C     *               M.LAZARE/    -  ADD NEW RADIATIVE FORCING ARRAYS
C     *               K.VONSALZEN/    (UNDER CONTROL OF "%DF RADFORCE").
C     *               X.MA.        - NEW DIAGNOSTIC FIELDS: WDD4,WDS4,EDSL,
C     *                              ESBF,ESFF,ESVC,ESVE,ESWF ALONG WITH
C     *                              TDEM->EDSO (UNDER CONTROL OF
C     *                              "XTRACHEM"), AS WELL AS ALMX,ALMC
C     *                              AND INSTANTANEOUS CLWT,CIDT.
C     *                            - NEW AEROCOM FORCING FIELDS:
C     *                              EBWA,EOWA,ESWA,EOST (EACH WITH ACCUMULATED
C     *                              AND TARGET),ESCV,EHCV,ESEV,EHEV,
C     *                              EBBT,EOBT,EBFT,EOFT,ESDT,ESIT,ESST,
C     *                              ESOT,ESPT,ESRT.
C     *                            - REMOVE UNUSED: QTPN,UTPM,VTPM,TSEM,EBC,
C     *                              EOC,EOCF,ESO2,EVOL,HVOL.
C     * JAN 11/2006 - J.COLE/      PREVIOUS VERSION COMPAK9F FOR GCM15F:
C     *               M.LAZARE.    - ADD ISCCP SIMULATOR FIELDS FROM JASON.
C     * NOV 26/2006 - M.LAZARE.    - TWO EXTRA NEW FIELDS ("DMC" AND "SMC")
C     *                              UNDER CONTROL OF "%IF DEF,XTRACONV".
C=========================================================================
C     * GENERAL PHYSICS ARRAYS.
C
      REAL ILSLPAK,JLPAK
C
      REAL LUINPAK,LUIMPAK,LRINPAK,LRIMPAK
      REAL ILMOPAK,LLAKPAK,LDMXPAK,LZICPAK
#if defined (flake)
      REAL LTAVPAK,LTICPAK,LTSNPAK,LSHPPAK
      REAL LTMXPAK,LTWBPAK,LZSNPAK
#endif
#if defined (cslm)
      REAL NLKLPAK
#endif
C
      REAL LAMNPAT,LAMXPAT,LNZ0PAT,MVPAT
      REAL MVPAK,NO3PAK,NH3PAK,NH4PAK,NO3PAL,NH3PAL,NH4PAL
      REAL RANDOMPAK
      REAL*8 RADJPAK,WJPAK
      REAL LONUPAK,LONDPAK,LATUPAK,LATDPAK
      REAL LW_EXT_SA_PAK, LW_EXT_SA_PAL,
     &     LW_SSA_SA_PAK, LW_SSA_SA_PAL,
     &     LW_G_SA_PAK,   LW_G_SA_PAL
      REAL NTPAK,N20PAK,N50PAK,N100PAK,N200PAK

      COMMON /PAK/ ALMCPAK(IP0J,ILEV)
      COMMON /PAK/ ALMXPAK(IP0J,ILEV)
      COMMON /PAK/ CICPAK (IP0J,ILEV)
      COMMON /PAK/ CLCVPAK(IP0J,ILEV)
      COMMON /PAK/ CLDPAK (IP0J,ILEV)
      COMMON /PAK/ CLWPAK (IP0J,ILEV)
      COMMON /PAK/ CSALPAL(IP0J,NBS)
      COMMON /PAK/ CSALPAT(IP0J,IM,NBS)
      COMMON /PAK/ CVARPAK(IP0J,ILEV)
      COMMON /PAK/ CVMCPAK(IP0J,ILEV)
      COMMON /PAK/ CVSGPAK(IP0J,ILEV)
      COMMON /PAK/ H2O2PAK(IP0J,LEVOX)
      COMMON /PAK/ H2O2PAL(IP0J,LEVOX)
      COMMON /PAK/ HNO3PAK(IP0J,LEVOX)
      COMMON /PAK/ HNO3PAL(IP0J,LEVOX)
      COMMON /PAK/ HRLPAK (IP0J,ILEV)
      COMMON /PAK/ HRSPAK (IP0J,ILEV)
      COMMON /PAK/ HRLCPAK(IP0J,ILEV)
      COMMON /PAK/ HRSCPAK(IP0J,ILEV)
      COMMON /PAK/ NH3PAK (IP0J,LEVOX)
      COMMON /PAK/ NH3PAL (IP0J,LEVOX)
      COMMON /PAK/ NH4PAK (IP0J,LEVOX)
      COMMON /PAK/ NH4PAL (IP0J,LEVOX)
      COMMON /PAK/ NO3PAK (IP0J,LEVOX)
      COMMON /PAK/ NO3PAL (IP0J,LEVOX)
      COMMON /PAK/ O3PAK  (IP0J,LEVOX)
      COMMON /PAK/ O3PAL  (IP0J,LEVOX)
      COMMON /PAK/ O3CPAK (IP0J,LEVOZC)
      COMMON /PAK/ O3CPAL (IP0J,LEVOZC)
      COMMON /PAK/ OHPAK  (IP0J,LEVOX)
      COMMON /PAK/ OHPAL  (IP0J,LEVOX)
      COMMON /PAK/ OMETPAK(IP0J,ILEV)
      COMMON /PAK/ OZPAK  (IP0J,LEVOZ)
      COMMON /PAK/ OZPAL  (IP0J,LEVOZ)
      COMMON /PAK/ QWF0PAL(IP0J,ILEV)
      COMMON /PAK/ QWFMPAL(IP0J,ILEV)
      COMMON /PAK/ RHPAK  (IP0J,ILEV)
      COMMON /PAK/ SALBPAL(IP0J,NBS)
      COMMON /PAK/ SALBPAT(IP0J,IM,NBS)
      COMMON /PAK/ SCDNPAK(IP0J,ILEV)
      COMMON /PAK/ SCLFPAK(IP0J,ILEV)
      COMMON /PAK/ SLWCPAK(IP0J,ILEV)
      COMMON /PAK/ TACNPAK(IP0J,ILEV)
      COMMON /PAK/ TBNDPAK(IP0J,ILEV)
      COMMON /PAK/ TBNDPAL(IP0J,ILEV)
      COMMON /PAK/ EA55PAK(IP0J,ILEV)
      COMMON /PAK/ EA55PAL(IP0J,ILEV)
      COMMON /PAK/ WRKAPAL(IP0J,NBS)
      COMMON /PAK/ WRKBPAL(IP0J,NBS)
      COMMON /PAK/ ZDETPAK(IP0J,ILEV)
      COMMON /PAK/ SFRCPAL(IP0J,ILEV,NTRAC)
      COMMON /PAK/ ALTIPAK (IP0J,ILEV)
      COMMON /PAK/ FTOXPAK(IP0J,ILEV), FTOYPAK(IP0J,ILEV)
      COMMON /PAK/ TDOXPAK(IP0J,ILEV), TDOYPAK(IP0J,ILEV)

      COMMON /PAK/ DLATPAK  (IP0J) ! LATITUDE IN DEGREES
      COMMON /PAK/ LATUPAK  (IP0J)
      COMMON /PAK/ LATDPAK  (IP0J)
      COMMON /PAK/ LONUPAK  (IP0J)
      COMMON /PAK/ LONDPAK  (IP0J)
C
      COMMON /PAK/ ALPHPAK(IP0J)
      COMMON /PAK/ BEGIPAL(IP0J)
      COMMON /PAK/ BEGKPAL(IP0J)
      COMMON /PAK/ BEGLPAL(IP0J)
      COMMON /PAK/ BEGOPAL(IP0J)
      COMMON /PAK/ BEGPAL (IP0J)
      COMMON /PAK/ BWGIPAL(IP0J)
      COMMON /PAK/ BWGKPAL(IP0J)
      COMMON /PAK/ BWGLPAL(IP0J)
      COMMON /PAK/ BWGOPAL(IP0J)
      COMMON /PAK/ BWGPAL (IP0J)
      COMMON /PAK/ CBMFPAL(IP0J)
      COMMON /PAK/ CHFXPAK(IP0J)
      COMMON /PAK/ CICTPAK(IP0J)
      COMMON /PAK/ CLBPAL (IP0J)
      COMMON /PAK/ CLBPAT (IP0J,IM)
      COMMON /PAK/ CLDTPAK(IP0J)
      COMMON /PAK/ CLDOPAK(IP0J)
      COMMON /PAK/ CLDOPAL(IP0J)
      COMMON /PAK/ CLDAPAK(IP0J)
      COMMON /PAK/ CLDAPAL(IP0J)
      COMMON /PAK/ CLWTPAK(IP0J)
      COMMON /PAK/ CQFXPAK(IP0J)
      COMMON /PAK/ CSBPAL (IP0J)
      COMMON /PAK/ CSBPAT (IP0J,IM)
      COMMON /PAK/ CSDPAL (IP0J)
      COMMON /PAK/ CSDPAT (IP0J,IM)
      COMMON /PAK/ CSFPAL (IP0J)
      COMMON /PAK/ CSFPAT (IP0J,IM)
      COMMON /PAK/ CSZPAL (IP0J)
      COMMON /PAK/ DELTPAK(IP0J)
      COMMON /PAK/ DLONPAK(IP0J)
      COMMON /PAK/ DMSOPAK(IP0J)
      COMMON /PAK/ DMSOPAL(IP0J)
      COMMON /PAK/ ECO2PAK(IP0J)
      COMMON /PAK/ ECO2PAL(IP0J)
      COMMON /PAK/ EDMSPAK(IP0J)
      COMMON /PAK/ EDMSPAL(IP0J)
      COMMON /PAK/ EMISPAK(IP0J)
      COMMON /PAK/ EMISPAT(IP0J,IM)
      COMMON /PAK/ ENVPAK (IP0J)
      COMMON /PAK/ FDLPAK (IP0J)
      COMMON /PAK/ FDLPAT (IP0J,IM)
      COMMON /PAK/ FDLCPAK(IP0J)
      COMMON /PAK/ FDLCPAT(IP0J,IM)
      COMMON /PAK/ FLAMPAK(IP0J)
      COMMON /PAK/ FLAMPAL(IP0J)
      COMMON /PAK/ FLANPAK(IP0J)
      COMMON /PAK/ FLANPAL(IP0J)
      COMMON /PAK/ FLGCPAK(IP0J)
      COMMON /PAK/ FLGPAK (IP0J)
      COMMON /PAK/ FLGPAT (IP0J,IM)
      COMMON /PAK/ FLKRPAK(IP0J)
      COMMON /PAK/ FLKUPAK(IP0J)
      COMMON /PAK/ FLNDPAK(IP0J)
      COMMON /PAK/ FSAMPAK(IP0J)
      COMMON /PAK/ FSAMPAL(IP0J)
      COMMON /PAK/ FSANPAK(IP0J)
      COMMON /PAK/ FSANPAL(IP0J)
      COMMON /PAK/ FSDCPAK(IP0J)
      COMMON /PAK/ FSDPAK (IP0J)
      COMMON /PAK/ FSDPAL (IP0J)
      COMMON /PAK/ FSDPAT (IP0J,IM)
      COMMON /PAK/ FSFPAL (IP0J)
      COMMON /PAK/ FSFPAT (IP0J,IM)
      COMMON /PAK/ FSGCPAK(IP0J)
      COMMON /PAK/ FSGPAK (IP0J)
      COMMON /PAK/ FSGIPAL(IP0J)
      COMMON /PAK/ FSGOPAL(IP0J)
      COMMON /PAK/ FSGPAT (IP0J,IM)
      COMMON /PAK/ FSIPAL (IP0J)
      COMMON /PAK/ FSIPAT (IP0J,IM)
      COMMON /PAK/ FSLOPAK(IP0J)
      COMMON /PAK/ FSLOPAL(IP0J)
      COMMON /PAK/ FSOPAK (IP0J)
      COMMON /PAK/ FSRCPAK(IP0J)
      COMMON /PAK/ FSRPAK (IP0J)
      COMMON /PAK/ FSSCPAK(IP0J)
      COMMON /PAK/ FSSPAK (IP0J)
      COMMON /PAK/ FSVPAK (IP0J)
      COMMON /PAK/ FSVPAL (IP0J)
      COMMON /PAK/ FSVPAT (IP0J,IM)
      COMMON /PAK/ FSDBPAL (IP0J,NBS)
      COMMON /PAK/ FSDBPAT (IP0J,IM,NBS)
      COMMON /PAK/ FSFBPAL (IP0J,NBS)
      COMMON /PAK/ FSFBPAT (IP0J,IM,NBS)
      COMMON /PAK/ CSDBPAL (IP0J,NBS)
      COMMON /PAK/ CSDBPAT (IP0J,IM,NBS)
      COMMON /PAK/ CSFBPAL (IP0J,NBS)
      COMMON /PAK/ CSFBPAT (IP0J,IM,NBS)
      COMMON /PAK/ FSSBPAL (IP0J,NBS)
      COMMON /PAK/ FSSBPAT (IP0J,IM,NBS)
      COMMON /PAK/ FSSCBPAL(IP0J,NBS)
      COMMON /PAK/ FSSCBPAT(IP0J,IM,NBS)
      COMMON /PAK/ GAMPAK (IP0J)
      COMMON /PAK/ GICNPAK(IP0J)
      COMMON /PAK/ GTAPAK (IP0J)
      COMMON /PAK/ GTPAK  (IP0J)
      COMMON /PAK/ GTPAT  (IP0J,IM)
      COMMON /PAK/ GTPAX  (IP0J,IM)
      COMMON /PAK/ HFLPAK (IP0J)
      COMMON /PAK/ HFSPAK (IP0J)
      COMMON /PAK/ HSEAPAL(IP0J)
      COMMON /PAK/ ILSLPAK(IP0J)
      COMMON /PAK/ JLPAK  (IP0J)
      COMMON /PAK/ OFSGPAL(IP0J)
      COMMON /PAK/ OLRPAK (IP0J)
      COMMON /PAK/ OLRCPAK(IP0J)
      COMMON /PAK/ PARPAL (IP0J)
      COMMON /PAK/ PARPAT (IP0J,IM)
      COMMON /PAK/ PBLHPAK(IP0J)
      COMMON /PAK/ PBLTPAK(IP0J)
      COMMON /PAK/ PCHFPAK(IP0J)
      COMMON /PAK/ PCPCPAK(IP0J)
      COMMON /PAK/ PCPPAK (IP0J)
      COMMON /PAK/ PCPSPAK(IP0J)
      COMMON /PAK/ PHISPAK(IP0J)
      COMMON /PAK/ PLHFPAK(IP0J)
      COMMON /PAK/ PMSLPAL(IP0J)
      COMMON /PAK/ PSAPAK (IP0J)
      COMMON /PAK/ PSHFPAK(IP0J)
      COMMON /PAK/ PSIPAK (IP0J)
      COMMON /PAK/ PWATPAK(IP0J)
      COMMON /PAK/ PWATPAM(IP0J)
      COMMON /PAK/ QFSPAK (IP0J)
      COMMON /PAK/ QFSLPAL(IP0J)
      COMMON /PAK/ QFSOPAL(IP0J)
      COMMON /PAK/ QFXPAK (IP0J)
      COMMON /PAK/ QTPFPAM(IP0J)
      COMMON /PAK/ QTPHPAM(IP0J)
      COMMON /PAK/ QTPTPAM(IP0J)
      COMMON /PAK/ RADJPAK(IP0J)
      COMMON /PAK/ RAINSPAL(IP0J)
      COMMON /PAK/ SICNPAK(IP0J)
      COMMON /PAK/ SICPAK (IP0J)
      COMMON /PAK/ SIGXPAK(IP0J)
      COMMON /PAK/ SLIMPAL(IP0J)
      COMMON /PAK/ SLIMPlw(IP0J),SLIMPsh(IP0J),SLIMPlh(IP0J)
      COMMON /PAK/ SNOPAK (IP0J)
      COMMON /PAK/ SNOPAT (IP0J,IM)
      COMMON /PAK/ SNOWSPAL(IP0J)
      COMMON /PAK/ SQPAK  (IP0J)
      COMMON /PAK/ SRHPAK (IP0J)
      COMMON /PAK/ SRHNPAK(IP0J)
      COMMON /PAK/ SRHXPAK(IP0J)
      COMMON /PAK/ STMNPAK(IP0J)
      COMMON /PAK/ STMXPAK(IP0J)
      COMMON /PAK/ STPAK  (IP0J)
      COMMON /PAK/ SUPAK  (IP0J)
      COMMON /PAK/ SVPAK  (IP0J)
      COMMON /PAK/ SWAPAK (IP0J)
      COMMON /PAK/ SWAPAL (IP0J)
      COMMON /PAK/ SWXPAK (IP0J)
      COMMON /PAK/ SWXUPAK(IP0J)
      COMMON /PAK/ SWXVPAK(IP0J)
      COMMON /PAK/ TCVPAK (IP0J)
      COMMON /PAK/ TFXPAK (IP0J)
      COMMON /PAK/ UFSPAK (IP0J)
      COMMON /PAK/ UFSPAL (IP0J)
      COMMON /PAK/ UFSIPAL(IP0J)
      COMMON /PAK/ UFSOPAL(IP0J)
      COMMON /PAK/ VFSPAK (IP0J)
      COMMON /PAK/ VFSPAL (IP0J)
      COMMON /PAK/ VFSIPAL(IP0J)
      COMMON /PAK/ VFSOPAL(IP0J)
      COMMON /PAK/ WJPAK  (IP0J)
      COMMON /PAK/ RKMPAK (IP0J,ILEV)  ! Momentum eddy diffusivities
      COMMON /PAK/ RKHPAK (IP0J,ILEV)  ! Temperature eddy diffusivities
      COMMON /PAK/ RKQPAK (IP0J,ILEV)  ! Water vapour eddy diffusivities
C
C     * TKE ADDITIONAL FIELDS.
C
      COMMON /PAK/ TKEMPAK(IP0J,ILEV)
      COMMON /PAK/ XLMPAK (IP0J,ILEV)
      COMMON /PAK/ SVARPAK(IP0J,ILEV)
      COMMON /PAK/ XLMPAT (IP0J,IM,ILEV)
C
C     * 3-HOUR SAVE FIELDS.
C
      COMMON /PAK/ CLDT3HPAK(IP0J)
      COMMON /PAK/ HFL3HPAK (IP0J)
      COMMON /PAK/ HFS3HPAK (IP0J)
      COMMON /PAK/ ROF3HPAL (IP0J)
      COMMON /PAK/ WGL3HPAK (IP0J)
      COMMON /PAK/ WGF3HPAK (IP0J)
      COMMON /PAK/ PCP3HPAK (IP0J)
      COMMON /PAK/ PCPC3HPAK(IP0J)
      COMMON /PAK/ PCPS3HPAK(IP0J)
      COMMON /PAK/ PCPL3HPAK(IP0J)
      COMMON /PAK/ PCPN3HPAK(IP0J)
      COMMON /PAK/ FDL3HPAK (IP0J)
      COMMON /PAK/ FDLC3HPAK(IP0J)
      COMMON /PAK/ FLG3HPAK (IP0J)
      COMMON /PAK/ FSS3HPAK (IP0J)
      COMMON /PAK/ FSSC3HPAK(IP0J)
      COMMON /PAK/ FSD3HPAK (IP0J)
      COMMON /PAK/ FSG3HPAK (IP0J)
      COMMON /PAK/ FSGC3HPAK(IP0J)
      COMMON /PAK/ SQ3HPAK  (IP0J)
      COMMON /PAK/ ST3HPAK  (IP0J)
      COMMON /PAK/ SU3HPAK  (IP0J)
      COMMON /PAK/ SV3HPAK  (IP0J)

      COMMON /PAK/ OLR3HPAK  (IP0J) ! All-sky outgoing thermal TOA
      COMMON /PAK/ FSR3HPAK  (IP0J) ! All-sky outgoing solar TOA
      COMMON /PAK/ OLRC3HPAK (IP0J) ! Clear-sky outgoing thermal TOA
      COMMON /PAK/ FSRC3HPAK (IP0J) ! Clear-sky outgoing solar TOA
      COMMON /PAK/ FSO3HPAK  (IP0J) ! Incident solar at TOA
      COMMON /PAK/ PWAT3HPAK (IP0J) ! Precipitable water
      COMMON /PAK/ CLWT3HPAK (IP0J) ! Vertically integrated non-precipitating liquid water
      COMMON /PAK/ CICT3HPAK (IP0J) ! Vertically integrated non-precipitating ice water
      COMMON /PAK/ PMSL3HPAK (IP0J) ! Sea-level pressure

C
C     * INSTANTANEOUS VARIABLES FOR SAMPLING
C

      COMMON /PAK/ SWPAL(IP0J)       ! Wind speed
      COMMON /PAK/ SRHPAL(IP0J)      ! Near-surface Relative Humidity
      COMMON /PAK/ PCPPAL(IP0J)      ! Total precip
      COMMON /PAK/ PCPNPAL(IP0J)     ! Snow precip
      COMMON /PAK/ PCPCPAL(IP0J)     ! Convective precip
      COMMON /PAK/ QFSIPAL(IP0J)     ! Evaporation including Sublimation and Transpiration
      COMMON /PAK/ QFNPAL(IP0J)      ! Surface Snow and Ice Sublimation Flux
      COMMON /PAK/ QFVFPAL(IP0J)     !
      COMMON /PAK/ UFSINSTPAL(IP0J)  ! Surface Downward Eastward Wind Stress
      COMMON /PAK/ VFSINSTPAL(IP0J)  ! Surface Downward Northward Wind Stress
      COMMON /PAK/ HFLIPAL(IP0J)     ! Surface Upward Latent Heat Flux
      COMMON /PAK/ HFSIPAL(IP0J)     ! Surface Upward Sensible Heat Flux
      COMMON /PAK/ FDLPAL(IP0J)      ! Surface Downwelling Longwave Radiation
      COMMON /PAK/ FLGPAL(IP0J)      ! Net Longwave Radiation (subtract from FDL to get upward flux)
      COMMON /PAK/ FSSPAL(IP0J)      ! Surface Downwelling Shortwave Radiation
      COMMON /PAK/ FSGPAL(IP0J)      ! Net Shortwave Radiation (subtract from FSS to get upward flux)
      COMMON /PAK/ FSSCPAL(IP0J)     ! Surface Downwelling Clear-Sky Shortwave Radiation
      COMMON /PAK/ FSGCPAL(IP0J)     ! Surface Upwelling Clear-Sky Shortwave Radiation (subtract from FSSC to get upward flux)
      COMMON /PAK/ FDLCPAL (IP0J)    ! Surface Downwelling Clear-Sky Longwave Radiation
      COMMON /PAK/ FSOPAL  (IP0J)    ! TOA Incident Shortwave Radiation
      COMMON /PAK/ FSRPAL  (IP0J)    ! Top-of-Atmosphere Outgoing Shortwave Radiation
      COMMON /PAK/ OLRPAL  (IP0J)    ! TOA Outgoing Longwave Radiation
      COMMON /PAK/ OLRCPAL (IP0J)    ! TOA Outgoing Clear-Sky Longwave Radiation
      COMMON /PAK/ FSRCPAL (IP0J)    ! TOA Outgoing Clear-Sky Shortwave Radiation
      COMMON /PAK/ PWATIPAL(IP0J)    ! Water Vapor Path
      COMMON /PAK/ CLDTPAL (IP0J)    ! Total Cloud Cover Percentage
      COMMON /PAK/ CLWTPAL (IP0J)    ! Condensed Water Path (liquid)
      COMMON /PAK/ CICTPAL(IP0J)     ! Condensed Water Path (ice)
      COMMON /PAK/ BALTPAL(IP0J)     ! Net Downward Radiative Flux at Top of Model
      COMMON /PAK/ PMSLIPAL(IP0J)    ! Sea Level Pressure
      COMMON /PAK/ CDCBPAL(IP0J)     ! Fraction of Time Convection Occurs in Cell
      COMMON /PAK/ CSCBPAL(IP0J)     ! Fraction of Time Shallow Convection Occurs
      COMMON /PAK/ GTPAL(IP0J)       ! Surface Temperature

      COMMON /PAK/ TCDPAL(IP0J)     ! Pressure at top of convection
      COMMON /PAK/ BCDPAL(IP0J)     ! Pressure at bottom of convection
      COMMON /PAK/ PSPAL(IP0J)      ! Surface pressure

      COMMON /PAK/ STPAL(IP0J)       ! Near surface temperature
      COMMON /PAK/ SUPAL(IP0J)       ! Near surface east wind
      COMMON /PAK/ SVPAL(IP0J)       ! Near surface north wind

C INSTANTANEOUS 3D FIELDS
      COMMON /PAK/ DMCPAL(IP0J,ILEV) ! Net convective mass flux
      COMMON /PAK/ TAPAL(IP0J,ILEV)  ! Temperature (from physics)
      COMMON /PAK/ UAPAL(IP0J,ILEV)  ! Eastward wind (from physics)
      COMMON /PAK/ VAPAL(IP0J,ILEV)  ! Northward wind (from physics)
      COMMON /PAK/ QAPAL(IP0J,ILEV)  ! Specific humidity (from physics)
      COMMON /PAK/ RHPAL(IP0J,ILEV)  ! Relative humidity (from physics)
      COMMON /PAK/ ZGPAL(IP0J,ILEV)  ! Geopotential height (from physics)
      COMMON /PAK/ RKHPAL(IP0J,ILEV)  ! Eddy coefficient for temperature
      COMMON /PAK/ RKMPAL(IP0J,ILEV)  ! Eddy coefficient for momentum
      COMMON /PAK/ RKQPAL(IP0J,ILEV)  ! Water vapour eddy diffusivities

      COMMON /PAK/ TTPPAL(IP0J,ILEV)  ! Temperature tendency from model physics
      COMMON /PAK/ TTPPPAL(IP0J,ILEV)  !
      COMMON /PAK/ TTPVPAL(IP0J,ILEV)  !
      COMMON /PAK/ TTPLPAL(IP0J,ILEV)  ! Temperature tendency from LW heating
      COMMON /PAK/ TTPSPAL(IP0J,ILEV)  ! Clear-sky temperature tendency from SW heating
      COMMON /PAK/ TTLCPAL(IP0J,ILEV)  ! Clear-sky temperature tendency from LW heating
      COMMON /PAK/ TTSCPAL(IP0J,ILEV)  ! Temperature tendency from SW heating
      COMMON /PAK/ TTPCPAL(IP0J,ILEV)  ! Temperature tendency from convection
      COMMON /PAK/ TTPMPAL(IP0J,ILEV)  ! Temperature tendency from convection

      COMMON /PAK/ QTPCPAL(IP0J,ILEV)  ! Specific humidity tendency from convection
      COMMON /PAK/ QTPMPAL(IP0J,ILEV)  ! Specific humidity tendency from convection
      COMMON /PAK/ QTPPPAL(IP0J,ILEV)  !
      COMMON /PAK/ QTPVPAL(IP0J,ILEV)  !
      COMMON /PAK/ QTPPAL(IP0J,ILEV)  ! Specific humidity tendency from model physics

C
C     * OTHERS.
C
      COMMON /PAK/ FCOOPAK(IP0J)
      COMMON /PAK/ ISEEDPAK(IP0J,4)
      COMMON /PAK/ RFLNDPAK(IP0J)
      COMMON /PAK/ RANDOMPAK(IP0J)
C
C     * LAKES.
C
      COMMON /PAK/ BLAKPAK(IP0J)
      COMMON /PAK/ HLAKPAK(IP0J)
      COMMON /PAK/ LDMXPAK(IP0J)
      COMMON /PAK/ LLAKPAK(IP0J)
      COMMON /PAK/ LRIMPAK(IP0J)
      COMMON /PAK/ LRINPAK(IP0J)
      COMMON /PAK/ LUIMPAK(IP0J)
      COMMON /PAK/ LUINPAK(IP0J)
      COMMON /PAK/ LZICPAK(IP0J)
#if defined (cslm)
      COMMON /PAK/ DELUPAK(IP0J)
      COMMON /PAK/ DTMPPAK(IP0J)
      COMMON /PAK/ EXPWPAK(IP0J)
      COMMON /PAK/ GREDPAK(IP0J)
      COMMON /PAK/ NLKLPAK(IP0J)
      COMMON /PAK/ RHOMPAK(IP0J)
      COMMON /PAK/ T0LKPAK(IP0J)
      COMMON /PAK/ TKELPAK(IP0J)
      COMMON /PAK/ TLAKPAK(IP0J,NLKLM)
C
      COMMON /PAK/ FLGLPAK(IP0J)
      COMMON /PAK/ FNLPAK (IP0J)
      COMMON /PAK/ FSGLPAK(IP0J)
      COMMON /PAK/ HFCLPAK(IP0J)
      COMMON /PAK/ HFLLPAK(IP0J)
      COMMON /PAK/ HFSLPAK(IP0J)
      COMMON /PAK/ HMFLPAK(IP0J)
      COMMON /PAK/ PILPAK (IP0J)
      COMMON /PAK/ QFLPAK (IP0J)
#endif
#if defined (flake)
      COMMON /PAK/ LSHPPAK(IP0J)
      COMMON /PAK/ LTAVPAK(IP0J)
      COMMON /PAK/ LTICPAK(IP0J)
      COMMON /PAK/ LTMXPAK(IP0J)
      COMMON /PAK/ LTSNPAK(IP0J)
      COMMON /PAK/ LTWBPAK(IP0J)
      COMMON /PAK/ LZSNPAK(IP0J)
#endif
C
C     * EMISSIONS.
C
      COMMON /PAK/ EOSTPAK(IP0J)
      COMMON /PAK/ EOSTPAL(IP0J)
C
      COMMON /PAK/ ESCVPAK(IP0J)
      COMMON /PAK/ EHCVPAK(IP0J)
      COMMON /PAK/ ESEVPAK(IP0J)
      COMMON /PAK/ EHEVPAK(IP0J)
C
#if defined transient_aerosol_emissions
      COMMON /PAK/ FBBCPAK(IP0J,LEVWF)
      COMMON /PAK/ FBBCPAL(IP0J,LEVWF)
      COMMON /PAK/ FAIRPAK(IP0J,LEVAIR)
      COMMON /PAK/ FAIRPAL(IP0J,LEVAIR)
#if defined emists
      COMMON /PAK/  SAIRPAK(IP0J)
      COMMON /PAK/  SSFCPAK(IP0J)
      COMMON /PAK/  SBIOPAK(IP0J)
      COMMON /PAK/  SSHIPAK(IP0J)
      COMMON /PAK/  SSTKPAK(IP0J)
      COMMON /PAK/  SFIRPAK(IP0J)
      COMMON /PAK/  SAIRPAL(IP0J)
      COMMON /PAK/  SSFCPAL(IP0J)
      COMMON /PAK/  SBIOPAL(IP0J)
      COMMON /PAK/  SSHIPAL(IP0J)
      COMMON /PAK/  SSTKPAL(IP0J)
      COMMON /PAK/  SFIRPAL(IP0J)
      COMMON /PAK/  OAIRPAK(IP0J)
      COMMON /PAK/  OSFCPAK(IP0J)
      COMMON /PAK/  OBIOPAK(IP0J)
      COMMON /PAK/  OSHIPAK(IP0J)
      COMMON /PAK/  OSTKPAK(IP0J)
      COMMON /PAK/  OFIRPAK(IP0J)
      COMMON /PAK/  OAIRPAL(IP0J)
      COMMON /PAK/  OSFCPAL(IP0J)
      COMMON /PAK/  OBIOPAL(IP0J)
      COMMON /PAK/  OSHIPAL(IP0J)
      COMMON /PAK/  OSTKPAL(IP0J)
      COMMON /PAK/  OFIRPAL(IP0J)
      COMMON /PAK/  BAIRPAK(IP0J)
      COMMON /PAK/  BSFCPAK(IP0J)
      COMMON /PAK/  BBIOPAK(IP0J)
      COMMON /PAK/  BSHIPAK(IP0J)
      COMMON /PAK/  BSTKPAK(IP0J)
      COMMON /PAK/  BFIRPAK(IP0J)
      COMMON /PAK/  BAIRPAL(IP0J)
      COMMON /PAK/  BSFCPAL(IP0J)
      COMMON /PAK/  BBIOPAL(IP0J)
      COMMON /PAK/  BSHIPAL(IP0J)
      COMMON /PAK/  BSTKPAL(IP0J)
      COMMON /PAK/  BFIRPAL(IP0J)
#endif
#endif
#if (defined(pla) && defined(pfrc))
      COMMON /PAK/  AMLDFPAK(IP0J,ILEV)
      COMMON /PAK/  AMLDFPAL(IP0J,ILEV)
      COMMON /PAK/  REAMFPAK(IP0J,ILEV)
      COMMON /PAK/  REAMFPAL(IP0J,ILEV)
      COMMON /PAK/  VEAMFPAK(IP0J,ILEV)
      COMMON /PAK/  VEAMFPAL(IP0J,ILEV)
      COMMON /PAK/  FR1FPAK(IP0J,ILEV)
      COMMON /PAK/  FR1FPAL(IP0J,ILEV)
      COMMON /PAK/  FR2FPAK(IP0J,ILEV)
      COMMON /PAK/  FR2FPAL(IP0J,ILEV)
      COMMON /PAK/  SSLDFPAK(IP0J,ILEV)
      COMMON /PAK/  SSLDFPAL(IP0J,ILEV)
      COMMON /PAK/  RESSFPAK(IP0J,ILEV)
      COMMON /PAK/  RESSFPAL(IP0J,ILEV)
      COMMON /PAK/  VESSFPAK(IP0J,ILEV)
      COMMON /PAK/  VESSFPAL(IP0J,ILEV)
      COMMON /PAK/  DSLDFPAK(IP0J,ILEV)
      COMMON /PAK/  DSLDFPAL(IP0J,ILEV)
      COMMON /PAK/  REDSFPAK(IP0J,ILEV)
      COMMON /PAK/  REDSFPAL(IP0J,ILEV)
      COMMON /PAK/  VEDSFPAK(IP0J,ILEV)
      COMMON /PAK/  VEDSFPAL(IP0J,ILEV)
      COMMON /PAK/  BCLDFPAK(IP0J,ILEV)
      COMMON /PAK/  BCLDFPAL(IP0J,ILEV)
      COMMON /PAK/  REBCFPAK(IP0J,ILEV)
      COMMON /PAK/  REBCFPAL(IP0J,ILEV)
      COMMON /PAK/  VEBCFPAK(IP0J,ILEV)
      COMMON /PAK/  VEBCFPAL(IP0J,ILEV)
      COMMON /PAK/  OCLDFPAK(IP0J,ILEV)
      COMMON /PAK/  OCLDFPAL(IP0J,ILEV)
      COMMON /PAK/  REOCFPAK(IP0J,ILEV)
      COMMON /PAK/  REOCFPAL(IP0J,ILEV)
      COMMON /PAK/  VEOCFPAK(IP0J,ILEV)
      COMMON /PAK/  VEOCFPAL(IP0J,ILEV)
      COMMON /PAK/  ZCDNFPAK(IP0J,ILEV)
      COMMON /PAK/  ZCDNFPAL(IP0J,ILEV)
      COMMON /PAK/  BCICFPAK(IP0J,ILEV)
      COMMON /PAK/  BCICFPAL(IP0J,ILEV)
      COMMON /PAK/  BCDPFPAK(IP0J)
      COMMON /PAK/  BCDPFPAL(IP0J)
#endif
C
C     * GENERAL TRACER ARRAYS.
C
      COMMON /PAK/ XFSPAK (IP0J,NTRAC)
      COMMON /PAK/ XSFXPAK(IP0J,NTRAC)
      COMMON /PAK/ XSFXPAL(IP0J,NTRAC)
      COMMON /PAK/ XSRFPAK(IP0J,NTRAC)
      COMMON /PAK/ XSRFPAL(IP0J,NTRAC)
      COMMON /PAK/ XTPFPAM(IP0J,NTRAC)
      COMMON /PAK/ XTPHPAM(IP0J,NTRAC)
      COMMON /PAK/ XTPTPAM(IP0J,NTRAC)
      COMMON /PAK/ XTVIPAS(IP0J,NTRAC)
      COMMON /PAK/ XTVIPAK(IP0J,NTRAC)
      COMMON /PAK/ XTVIPAM(IP0J,NTRAC)
      COMMON /PAK/ XWF0PAL(IP0J,ILEV,NTRAC)
      COMMON /PAK/ XWFMPAL(IP0J,ILEV,NTRAC)
C
C     * LAND SURFACE SCHEME ARRAYS.
C
      COMMON /PAK/ ALICPAT(IP0J,NTLD,ICAN+1)
      COMMON /PAK/ ALVCPAT(IP0J,NTLD,ICAN+1)
      COMMON /PAK/ CLAYPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ CMASPAT(IP0J,NTLD,ICAN)
      COMMON /PAK/ DZGPAK (IP0J,IGND)
      COMMON /PAK/ FCANPAT(IP0J,NTLD,ICAN+1)
      COMMON /PAK/ GFLXPAK(IP0J,IGND)
      COMMON /PAK/ HFCGPAK(IP0J,IGND)
      COMMON /PAK/ HMFGPAK(IP0J,IGND)
      COMMON /PAK/ LAMNPAT(IP0J,NTLD,ICAN)
      COMMON /PAK/ LAMXPAT(IP0J,NTLD,ICAN)
      COMMON /PAK/ LNZ0PAT(IP0J,NTLD,ICAN+1)
      COMMON /PAK/ ORGMPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ PORGPAK(IP0J,IGND)
      COMMON /PAK/ FCAPPAK(IP0J,IGND)
      COMMON /PAK/ DLZWPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ PORGPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ THFCPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ THLWPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ THRPAT (IP0J,NTLD,IGND)
      COMMON /PAK/ THMPAT (IP0J,NTLD,IGND)
      COMMON /PAK/ BIPAT  (IP0J,NTLD,IGND)
      COMMON /PAK/ PSISPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ GRKSPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ THRAPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ HCPSPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ TCSPAT (IP0J,NTLD,IGND)
      COMMON /PAK/ PSIWPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ ZBTWPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ ISNDPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ QFVGPAK(IP0J,IGND)
      COMMON /PAK/ ROOTPAT(IP0J,NTLD,ICAN)
      COMMON /PAK/ SANDPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ TGPAK  (IP0J,IGND)
      COMMON /PAK/ TGPAT  (IP0J,NTLD,IGND)
      COMMON /PAK/ THICPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ THLQPAT(IP0J,NTLD,IGND)
      COMMON /PAK/ WGFPAK (IP0J,IGND)
      COMMON /PAK/ WGLPAK (IP0J,IGND)
C
      COMMON /PAK/ ANPAK  (IP0J)
      COMMON /PAK/ ANPAT  (IP0J,IM)
      COMMON /PAK/ DPTHPAK(IP0J)
      COMMON /PAK/ DPTHPAT(IP0J,NTLD)
      COMMON /PAK/ ALGDVPAT(IP0J,NTLD)
      COMMON /PAK/ ALGDNPAT(IP0J,NTLD)
      COMMON /PAK/ ALGWVPAT(IP0J,NTLD)
      COMMON /PAK/ ALGWNPAT(IP0J,NTLD)
      COMMON /PAK/ IGDRPAT(IP0J,NTLD)
      COMMON /PAK/ DRNPAT (IP0J,NTLD)
      COMMON /PAK/ DRPAK  (IP0J)
      COMMON /PAK/ FLGGPAK(IP0J)
      COMMON /PAK/ FLGNPAK(IP0J)
      COMMON /PAK/ FLGVPAK(IP0J)
      COMMON /PAK/ FNLAPAK(IP0J)
      COMMON /PAK/ FNPAK  (IP0J)
      COMMON /PAK/ FNPAT  (IP0J,IM)
      COMMON /PAK/ FSGGPAK(IP0J)
      COMMON /PAK/ FSGNPAK(IP0J)
      COMMON /PAK/ FSGVPAK(IP0J)
      COMMON /PAK/ FVGPAK (IP0J)
      COMMON /PAK/ FVNPAK (IP0J)
      COMMON /PAK/ GAPAK  (IP0J)
      COMMON /PAK/ GSNOPAK(IP0J)
      COMMON /PAK/ HBLPAK (IP0J)
      COMMON /PAK/ HFCNPAK(IP0J)
      COMMON /PAK/ HFCVPAK(IP0J)
      COMMON /PAK/ HFLGPAK(IP0J)
      COMMON /PAK/ HFLNPAK(IP0J)
      COMMON /PAK/ HFLVPAK(IP0J)
      COMMON /PAK/ HFSGPAK(IP0J)
      COMMON /PAK/ HFSNPAK(IP0J)
      COMMON /PAK/ HFSVPAK(IP0J)
      COMMON /PAK/ HMFNPAK(IP0J)
      COMMON /PAK/ HMFVPAK(IP0J)
      COMMON /PAK/ MVPAK  (IP0J)
      COMMON /PAK/ ILMOPAK(IP0J)
      COMMON /PAK/ MVPAT  (IP0J,NTLD)
      COMMON /PAK/ PCPNPAK(IP0J)
      COMMON /PAK/ PETPAK (IP0J)
      COMMON /PAK/ PIVFPAK(IP0J)
      COMMON /PAK/ PIVLPAK(IP0J)
      COMMON /PAK/ PIGPAK (IP0J)
      COMMON /PAK/ PINPAK (IP0J)
      COMMON /PAK/ QFGPAK (IP0J)
      COMMON /PAK/ QFNPAK (IP0J)
      COMMON /PAK/ QFVFPAK(IP0J)
      COMMON /PAK/ QFVLPAK(IP0J)
      COMMON /PAK/ RHONPAK(IP0J)
      COMMON /PAK/ RHONPAT(IP0J,IM)
      COMMON /PAK/ ROFBPAK(IP0J)
      COMMON /PAK/ REFPAK(IP0J)
      COMMON /PAK/ REFPAT(IP0J,IM)
      COMMON /PAK/ BCSNPAK(IP0J)
      COMMON /PAK/ BCSNPAT(IP0J,IM)
      COMMON /PAK/ ROFNPAK(IP0J)
      COMMON /PAK/ ROFOPAK(IP0J)
      COMMON /PAK/ ROFOPAL(IP0J)
      COMMON /PAK/ ROFPAK (IP0J)
      COMMON /PAK/ ROFPAL (IP0J)
      COMMON /PAK/ ROFSPAK(IP0J)
      COMMON /PAK/ ROFVPAK(IP0J)
      COMMON /PAK/ ROVGPAK(IP0J)
      COMMON /PAK/ SKYGPAK(IP0J)
      COMMON /PAK/ SKYNPAK(IP0J)
      COMMON /PAK/ SMLTPAK(IP0J)
      COMMON /PAK/ SOCIPAT(IP0J,NTLD)
      COMMON /PAK/ TBASPAK(IP0J)
      COMMON /PAK/ TBASPAT(IP0J,NTLD)
      COMMON /PAK/ TNPAK  (IP0J)
      COMMON /PAK/ TNPAT  (IP0J,IM)
      COMMON /PAK/ TTPAK  (IP0J)
      COMMON /PAK/ TTPAT  (IP0J,NTLD)
      COMMON /PAK/ TVPAK  (IP0J)
      COMMON /PAK/ TVPAT  (IP0J,NTLD)
      COMMON /PAK/ UEPAK  (IP0J)
      COMMON /PAK/ WTABPAK(IP0J)
      COMMON /PAK/ WTRGPAK(IP0J)
      COMMON /PAK/ WTRNPAK(IP0J)
      COMMON /PAK/ WTRVPAK(IP0J)
      COMMON /PAK/ WVFPAK (IP0J)
      COMMON /PAK/ WVFPAT (IP0J,NTLD)
      COMMON /PAK/ WVLPAK (IP0J)
      COMMON /PAK/ WVLPAT (IP0J,NTLD)
      COMMON /PAK/ ZNPAK  (IP0J)
      COMMON /PAK/ DEPBPAK(IP0J)
      COMMON /PAK/ TPNDPAT(IP0J,NTLD)
      COMMON /PAK/ ZPNDPAK(IP0J)
      COMMON /PAK/ ZPNDPAT(IP0J,NTLD)
      COMMON /PAK/ TAVPAT (IP0J,NTLD)
      COMMON /PAK/ QAVPAT (IP0J,NTLD)
      COMMON /PAK/ WSNOPAK(IP0J)
      COMMON /PAK/ WSNOPAT(IP0J,IM)

      COMMON /PAK/ FAREPAT(IP0J,IM)
      COMMON /PAK/ TSFSPAT(IP0J,NTLD,4)
#if defined (agcm_ctem)
C
C     * CTEM RELATED VARIABLES
C
      COMMON /PAK/ CFCANPAT  (IP0J,NTLD,ICANP1)
      COMMON /PAK/ CALVCPAT  (IP0J,NTLD,ICANP1)
      COMMON /PAK/ CALICPAT  (IP0J,NTLD,ICANP1)

      COMMON /PAK/ ZOLNCPAT  (IP0J,NTLD,ICAN)
      COMMON /PAK/ CMASCPAT  (IP0J,NTLD,ICAN)
      COMMON /PAK/ RMATCPAT  (IP0J,NTLD,ICAN,  IGND)
      COMMON /PAK/ RTCTMPAT  (IP0J,NTLD,ICTEM, IGND)

      COMMON /PAK/ AILCPAT   (IP0J,NTLD,ICAN)
      COMMON /PAK/ PAICPAT   (IP0J,NTLD,ICAN)
      COMMON /PAK/ SLAICPAT  (IP0J,NTLD,ICAN)
      COMMON /PAK/ FCANCPAT  (IP0J,NTLD,ICTEM)
      COMMON /PAK/ TODFCPAT  (IP0J,NTLD,ICTEM)
      COMMON /PAK/ AILCGPAT  (IP0J,NTLD,ICTEM)
      COMMON /PAK/ SLAIPAT   (IP0J,NTLD,ICTEM)
      COMMON /PAK/ CO2CG1PAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ CO2CG2PAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ CO2CS1PAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ CO2CS2PAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ ANCGPTL   (IP0J,NTLD,ICTEM)
      COMMON /PAK/ ANCSPTL   (IP0J,NTLD,ICTEM)
      COMMON /PAK/ RMLCGPTL  (IP0J,NTLD,ICTEM)
      COMMON /PAK/ RMLCSPTL  (IP0J,NTLD,ICTEM)

      COMMON /PAK/ FSNOWPTL  (IP0J,NTLD)
      COMMON /PAK/ TCANOPTL  (IP0J,NTLD)
      COMMON /PAK/ TCANSPTL  (IP0J,NTLD)
      COMMON /PAK/ TAPTL     (IP0J,NTLD)
      COMMON /PAK/ CFLUXCSPAT(IP0J,NTLD)
      COMMON /PAK/ CFLUXCGPAT(IP0J,NTLD)

      COMMON /PAK/ TBARPTL   (IP0J,NTLD,IGND)
      COMMON /PAK/ TBARCPTL  (IP0J,NTLD,IGND)
      COMMON /PAK/ TBARCSPTL (IP0J,NTLD,IGND)
      COMMON /PAK/ TBARGPTL  (IP0J,NTLD,IGND)
      COMMON /PAK/ TBARGSPTL (IP0J,NTLD,IGND)
      COMMON /PAK/ THLIQCPTL (IP0J,NTLD,IGND)
      COMMON /PAK/ THLIQGPTL (IP0J,NTLD,IGND)
      COMMON /PAK/ THICECPTL (IP0J,NTLD,IGND)
C
      REAL LGHTPAK,LITRCPAT,LEAFSPAT,LASTRPAT,LASTSPAT,NEWFPAT
C
C     * INVARIANT.
C
      COMMON /PAK/ WETFPAK  (IP0J)
      COMMON /PAK/ WETSPAK  (IP0J)
      COMMON /PAK/ LGHTPAK  (IP0J)
C
C     * TIME VARYING.
C
      COMMON /PAK/ SOILCPAT (IP0J,NTLD,ICTEMP1)
      COMMON /PAK/ LITRCPAT (IP0J,NTLD,ICTEMP1)
      COMMON /PAK/ ROOTCPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ STEMCPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ GLEAFCPAT(IP0J,NTLD,ICTEM)
      COMMON /PAK/ BLEAFCPAT(IP0J,NTLD,ICTEM)
      COMMON /PAK/ FALLHPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ POSPHPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ LEAFSPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ GROWTPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ LASTRPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ LASTSPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ THISYLPAT(IP0J,NTLD,ICTEM)
      COMMON /PAK/ STEMHPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ ROOTHPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ TEMPCPAT (IP0J,NTLD,2)
      COMMON /PAK/ AILCBPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ BMASVPAT (IP0J,NTLD,ICTEM)
      COMMON /PAK/ VEGHPAT  (IP0J,NTLD,ICTEM)
      COMMON /PAK/ ROOTDPAT (IP0J,NTLD,ICTEM)
C
      COMMON /PAK/ PREFPAT  (IP0J,NTLD,ICTEM)
      COMMON /PAK/ NEWFPAT  (IP0J,NTLD,ICTEM)
C
      COMMON /PAK/ CVEGPAT  (IP0J,NTLD)
      COMMON /PAK/ CDEBPAT  (IP0J,NTLD)
      COMMON /PAK/ CHUMPAT  (IP0J,NTLD)
      COMMON /PAK/ FCOLPAT  (IP0J,NTLD)
C
C     * CTEM DIAGNOSTIC OUTPUT FIELDS.
C
      COMMON /PAK/ CVEGPAK(IP0J)
      COMMON /PAK/ CDEBPAK(IP0J)
      COMMON /PAK/ CHUMPAK(IP0J)
      COMMON /PAK/ CLAIPAK(IP0J)
      COMMON /PAK/ CFNPPAK(IP0J)
      COMMON /PAK/ CFNEPAK(IP0J)
      COMMON /PAK/ CFRVPAK(IP0J)
      COMMON /PAK/ CFGPPAK(IP0J)
      COMMON /PAK/ CFNBPAK(IP0J)
      COMMON /PAK/ CFFVPAK(IP0J)
      COMMON /PAK/ CFFDPAK(IP0J)
      COMMON /PAK/ CFLVPAK(IP0J)
      COMMON /PAK/ CFLDPAK(IP0J)
      COMMON /PAK/ CFLHPAK(IP0J)
      COMMON /PAK/ CBRNPAK(IP0J)
      COMMON /PAK/ CFRHPAK(IP0J)
      COMMON /PAK/ CFHTPAK(IP0J)
      COMMON /PAK/ CFLFPAK(IP0J)
      COMMON /PAK/ CFRDPAK(IP0J)
      COMMON /PAK/ CFRGPAK(IP0J)
      COMMON /PAK/ CFRMPAK(IP0J)
      COMMON /PAK/ CVGLPAK(IP0J)
      COMMON /PAK/ CVGSPAK(IP0J)
      COMMON /PAK/ CVGRPAK(IP0J)
      COMMON /PAK/ CFNLPAK(IP0J)
      COMMON /PAK/ CFNSPAK(IP0J)
      COMMON /PAK/ CFNRPAK(IP0J)
      COMMON /PAK/ CH4HPAK(IP0J)
      COMMON /PAK/ CH4NPAK(IP0J)
      COMMON /PAK/ WFRAPAK(IP0J)
      COMMON /PAK/ CW1DPAK(IP0J)
      COMMON /PAK/ CW2DPAK(IP0J)
      COMMON /PAK/ FCOLPAK(IP0J)
      COMMON /PAK/ CURFPAK(IP0J,ICTEM)

C     Additional CTEM related CMIP6 output

      COMMON /PAK/ BRFRPAK(IP0J)   ! baresoilFrac
      COMMON /PAK/ C3CRPAK(IP0J)   ! cropFracC3
      COMMON /PAK/ C4CRPAK(IP0J)   ! cropFracC4
      COMMON /PAK/ CRPFPAK(IP0J)   ! cropFrac
      COMMON /PAK/ C3GRPAK(IP0J)   ! grassFracC3
      COMMON /PAK/ C4GRPAK(IP0J)   ! grassFracC4
      COMMON /PAK/ GRSFPAK(IP0J)   ! grassFrac
      COMMON /PAK/ BDTFPAK(IP0J)   ! treeFracBdlDcd
      COMMON /PAK/ BETFPAK(IP0J)   ! treeFracBdlEvg
      COMMON /PAK/ NDTFPAK(IP0J)   ! treeFracNdlDcd
      COMMON /PAK/ NETFPAK(IP0J)   ! treeFracNdlEvg
      COMMON /PAK/ TREEPAK(IP0J)   ! treeFrac
      COMMON /PAK/ VEGFPAK(IP0J)   ! vegFrac
      COMMON /PAK/ C3PFPAK(IP0J)   ! c3PftFrac
      COMMON /PAK/ C4PFPAK(IP0J)   ! c4PftFrac
      COMMON /PAK/ CLNDPAK(IP0J)   ! cLand

C
C Instantaneous output (used for subdaily output)
C
      COMMON /PAK/ CFGPPAL (IP0J) ! GPP
      COMMON /PAK/ CFRVPAL (IP0J) ! RA
      COMMON /PAK/ CFRHPAL (IP0J) ! RH
      COMMON /PAK/ CFRDPAL (IP0J) ! RH

#endif
#if defined explvol
      COMMON /PAK/ VTAUPAK(IP0J)
      COMMON /PAK/ VTAUPAL(IP0J)
      COMMON /PAK/ SW_EXT_SA_PAK(IP0JZ,LEVSA,NBS)
      COMMON /PAK/ SW_EXT_SA_PAL(IP0JZ,LEVSA,NBS)
      COMMON /PAK/ SW_SSA_SA_PAK(IP0JZ,LEVSA,NBS)
      COMMON /PAK/ SW_SSA_SA_PAL(IP0JZ,LEVSA,NBS)
      COMMON /PAK/ SW_G_SA_PAK(IP0JZ,LEVSA,NBS)
      COMMON /PAK/ SW_G_SA_PAL(IP0JZ,LEVSA,NBS)
      COMMON /PAK/ LW_EXT_SA_PAK(IP0JZ,LEVSA,NBL)
      COMMON /PAK/ LW_EXT_SA_PAL(IP0JZ,LEVSA,NBL)
      COMMON /PAK/ LW_SSA_SA_PAK(IP0JZ,LEVSA,NBL)
      COMMON /PAK/ LW_SSA_SA_PAL(IP0JZ,LEVSA,NBL)
      COMMON /PAK/ LW_G_SA_PAK(IP0JZ,LEVSA,NBL)
      COMMON /PAK/ LW_G_SA_PAL(IP0JZ,LEVSA,NBL)
      COMMON /PAK/ W055_EXT_SA_PAK(IP0JZ,LEVSA)
      COMMON /PAK/ W055_EXT_SA_PAL(IP0JZ,LEVSA)
      COMMON /PAK/ W055_VTAU_SA_PAK(IP0J)
      COMMON /PAK/ W055_VTAU_SA_PAL(IP0J)
      COMMON /PAK/ W055_EXT_GCM_SA_PAK(IP0J,ILEV)
      COMMON /PAK/ W055_EXT_GCM_SA_PAL(IP0J,ILEV)
      COMMON /PAK/ W110_EXT_SA_PAK(IP0JZ,LEVSA)
      COMMON /PAK/ W110_EXT_SA_PAL(IP0JZ,LEVSA)
      COMMON /PAK/ W110_VTAU_SA_PAK(IP0J)
      COMMON /PAK/ W110_VTAU_SA_PAL(IP0J)
      COMMON /PAK/ W110_EXT_GCM_SA_PAK(IP0J,ILEV)
      COMMON /PAK/ W110_EXT_GCM_SA_PAL(IP0J,ILEV)
      COMMON /PAK/ PRESSURE_SA_PAK(IP0JZ,LEVSA)
      COMMON /PAK/ PRESSURE_SA_PAL(IP0JZ,LEVSA)
      COMMON /PAK/ TROPPAK(IP0J)
      COMMON /PAK/ TROPPAL(IP0J)
#else
      COMMON /PAK/ VTAUPAK(1)
      COMMON /PAK/ VTAUPAL(1)
      COMMON /PAK/ SW_EXT_SA_PAK(1,1,1)
      COMMON /PAK/ SW_EXT_SA_PAL(1,1,1)
      COMMON /PAK/ SW_SSA_SA_PAK(1,1,1)
      COMMON /PAK/ SW_SSA_SA_PAL(1,1,1)
      COMMON /PAK/ SW_G_SA_PAK(1,1,1)
      COMMON /PAK/ SW_G_SA_PAL(1,1,1)
      COMMON /PAK/ LW_EXT_SA_PAK(1,1,1)
      COMMON /PAK/ LW_EXT_SA_PAL(1,1,1)
      COMMON /PAK/ LW_SSA_SA_PAK(1,1,1)
      COMMON /PAK/ LW_SSA_SA_PAL(1,1,1)
      COMMON /PAK/ LW_G_SA_PAK(1,1,1)
      COMMON /PAK/ LW_G_SA_PAL(1,1,1)
      COMMON /PAK/ W055_EXT_SA_PAK(1,1)
      COMMON /PAK/ W055_EXT_SA_PAL(1,1)
      COMMON /PAK/ W055_VTAU_SA_PAK(1)
      COMMON /PAK/ W055_VTAU_SA_PAL(1)
      COMMON /PAK/ W055_EXT_GCM_SA_PAK(1,1)
      COMMON /PAK/ W055_EXT_GCM_SA_PAL(1,1)
      COMMON /PAK/ W110_EXT_SA_PAK(1,1)
      COMMON /PAK/ W110_EXT_SA_PAL(1,1)
      COMMON /PAK/ W110_VTAU_SA_PAK(1)
      COMMON /PAK/ W110_VTAU_SA_PAL(1)
      COMMON /PAK/ W110_EXT_GCM_SA_PAK(1,1)
      COMMON /PAK/ W110_EXT_GCM_SA_PAL(1,1)
      COMMON /PAK/ PRESSURE_SA_PAK(1,1)
      COMMON /PAK/ PRESSURE_SA_PAL(1,1)
      COMMON /PAK/ TROPPAK(1)
      COMMON /PAK/ TROPPAL(1)
#endif
      COMMON /PAK/ SPOTPAK(IP0J)
      COMMON /PAK/ ST01PAK(IP0J)
      COMMON /PAK/ ST02PAK(IP0J)
      COMMON /PAK/ ST03PAK(IP0J)
      COMMON /PAK/ ST04PAK(IP0J)
      COMMON /PAK/ ST06PAK(IP0J)
      COMMON /PAK/ ST13PAK(IP0J)
      COMMON /PAK/ ST14PAK(IP0J)
      COMMON /PAK/ ST15PAK(IP0J)
      COMMON /PAK/ ST16PAK(IP0J)
      COMMON /PAK/ ST17PAK(IP0J)
      COMMON /PAK/ SUZ0PAK(IP0J)
      COMMON /PAK/ SUZ0PAL(IP0J)
      COMMON /PAK/ PDSFPAK(IP0J)
      COMMON /PAK/ PDSFPAL(IP0J)
#if defined (xtradust)
      COMMON /PAK/ DUWDPAK(IP0J)
      COMMON /PAK/ DUSTPAK(IP0J)
      COMMON /PAK/ DUTHPAK(IP0J)
      COMMON /PAK/ USMKPAK(IP0J)
      COMMON /PAK/ FALLPAK(IP0J)
      COMMON /PAK/ FA10PAK(IP0J)
      COMMON /PAK/ FA2PAK(IP0J)
      COMMON /PAK/ FA1PAK(IP0J)
      COMMON /PAK/ GUSTPAK(IP0J)
      COMMON /PAK/ ZSPDPAK(IP0J)
      COMMON /PAK/ VGFRPAK(IP0J)
      COMMON /PAK/ SMFRPAK(IP0J)
#endif

#if defined rad_flux_profs
! Note that for vertical profiles
! Level 1 is the top of atmosphere
! Level 2 is the top of the model
! Level ILEV+2 is the surface
      COMMON /PAK/ FSAUPAK (IP0J,ILEV+2) ! Upward all-sky shortwave flux profile
      COMMON /PAK/ FSADPAK (IP0J,ILEV+2) ! Downward all-sky shortwave flux profile
      COMMON /PAK/ FLAUPAK (IP0J,ILEV+2) ! Upward all-sky longwave flux profile
      COMMON /PAK/ FLADPAK (IP0J,ILEV+2) ! Downward all-sky longwave flux profile
      COMMON /PAK/ FSCUPAK (IP0J,ILEV+2) ! Upward clear-sky shortwave flux profile
      COMMON /PAK/ FSCDPAK (IP0J,ILEV+2) ! Downward clear-sky shortwave flux profile
      COMMON /PAK/ FLCUPAK (IP0J,ILEV+2) ! Upward clear-sky longwave flux profile
      COMMON /PAK/ FLCDPAK (IP0J,ILEV+2) ! Downward clear-sky longwave flux profile
#endif
      COMMON /PAK/ FSAUPAL (IP0J,ILEV+2) ! Upward all-sky shortwave flux profile
      COMMON /PAK/ FSADPAL (IP0J,ILEV+2) ! Downward all-sky shortwave flux profile
      COMMON /PAK/ FLAUPAL (IP0J,ILEV+2) ! Upward all-sky longwave flux profile
      COMMON /PAK/ FLADPAL (IP0J,ILEV+2) ! Downward all-sky longwave flux profile
      COMMON /PAK/ FSCUPAL (IP0J,ILEV+2) ! Upward clear-sky shortwave flux profile
      COMMON /PAK/ FSCDPAL (IP0J,ILEV+2) ! Downward clear-sky shortwave flux profile
      COMMON /PAK/ FLCUPAL (IP0J,ILEV+2) ! Upward clear-sky longwave flux profile
      COMMON /PAK/ FLCDPAL (IP0J,ILEV+2) ! Downward clear-sky longwave flux profile

#if defined radforce
      COMMON /PAK/ FSAUPAK_R (IP0J,ILEV+2,NRFP) ! Upward all-sky shortwave flux profile
      COMMON /PAK/ FSADPAK_R (IP0J,ILEV+2,NRFP) ! Downward all-sky shortwave flux profile
      COMMON /PAK/ FLAUPAK_R (IP0J,ILEV+2,NRFP) ! Upward all-sky longwave flux profile
      COMMON /PAK/ FLADPAK_R (IP0J,ILEV+2,NRFP) ! Downward all-sky longwave flux profile
      COMMON /PAK/ FSCUPAK_R (IP0J,ILEV+2,NRFP) ! Upward clear-sky shortwave flux profile
      COMMON /PAK/ FSCDPAK_R (IP0J,ILEV+2,NRFP) ! Downward clear-sky shortwave flux profile
      COMMON /PAK/ FLCUPAK_R (IP0J,ILEV+2,NRFP) ! Upward clear-sky longwave flux profile
      COMMON /PAK/ FLCDPAK_R (IP0J,ILEV+2,NRFP) ! Downward clear-sky longwave flux profile

      COMMON /PAK/ FSAUPAL_R (IP0J,ILEV+2,NRFP) ! Upward all-sky shortwave flux profile
      COMMON /PAK/ FSADPAL_R (IP0J,ILEV+2,NRFP) ! Downward all-sky shortwave flux profile
      COMMON /PAK/ FLAUPAL_R (IP0J,ILEV+2,NRFP) ! Upward all-sky longwave flux profile
      COMMON /PAK/ FLADPAL_R (IP0J,ILEV+2,NRFP) ! Downward all-sky longwave flux profile
      COMMON /PAK/ FSCUPAL_R (IP0J,ILEV+2,NRFP) ! Upward clear-sky shortwave flux profile
      COMMON /PAK/ FSCDPAL_R (IP0J,ILEV+2,NRFP) ! Downward clear-sky shortwave flux profile
      COMMON /PAK/ FLCUPAL_R (IP0J,ILEV+2,NRFP) ! Upward clear-sky longwave flux profile
      COMMON /PAK/ FLCDPAL_R (IP0J,ILEV+2,NRFP) ! Downward clear-sky longwave flux profile

      COMMON /PAK/ RDTPAK_R  (IP0J,ILEV,NRFP)
      COMMON /PAK/ RDTPAL_R  (IP0J,ILEV,NRFP)
      COMMON /PAK/ RDTPAM_R  (IP0J,ILEV,NRFP)
      COMMON /PAK/ HRSPAK_R  (IP0J,ILEV,NRFP)
      COMMON /PAK/ HRLPAK_R  (IP0J,ILEV,NRFP)
      COMMON /PAK/ HRSCPAK_R (IP0J,ILEV,NRFP)
      COMMON /PAK/ HRLCPAK_R (IP0J,ILEV,NRFP)
      COMMON /PAK/ CSBPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FSGPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FSSPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FSSCPAL_R (IP0J,NRFP)
      COMMON /PAK/ CLBPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FLGPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FDLPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FDLCPAL_R (IP0J,NRFP)
      COMMON /PAK/ FSRPAL_R  (IP0J,NRFP)
      COMMON /PAK/ FSRCPAL_R (IP0J,NRFP)
      COMMON /PAK/ FSOPAL_R  (IP0J,NRFP)
      COMMON /PAK/ OLRPAL_R  (IP0J,NRFP)
      COMMON /PAK/ OLRCPAL_R (IP0J,NRFP)
      COMMON /PAK/ FSLOPAL_R (IP0J,NRFP)
      COMMON /PAK/ CSBPAT_R  (IP0J,IM,NRFP)
      COMMON /PAK/ CLBPAT_R  (IP0J,IM,NRFP)
      COMMON /PAK/ FSGPAT_R  (IP0J,IM,NRFP)
      COMMON /PAK/ FLGPAT_R  (IP0J,IM,NRFP)
      COMMON /PAK/ KTHPAL    (IP0J)
#endif
#if defined tprhs
C
C     * TPRHS defined
      COMMON /PAK/ TTPPAK (IP0J,ILEV)
#endif
#if defined qprhs
C
C     * QPRHS defined
      COMMON /PAK/ QTPPAK (IP0J,ILEV)
#endif
#if defined uprhs
C
C     * UPRHS defined
      COMMON /PAK/ UTPPAK (IP0J,ILEV)
#endif
#if defined vprhs
C
C     * VPRHS defined
      COMMON /PAK/ VTPPAK (IP0J,ILEV)
#endif
#if defined qconsav
C
C     * QCONSAV defined
      COMMON /PAK/ QADDPAK(IP0J,2)
      COMMON /PAK/ QTPHPAK(IP0J,2)
      COMMON /PAK/ QTPFPAK(IP0J,ILEV)
#endif
#if defined tprhsc
C
C     * TPRHSC defined
      COMMON /PAK/ TTPCPAK(IP0J,ILEV)
      COMMON /PAK/ TTPKPAK(IP0J,ILEV)
      COMMON /PAK/ TTPMPAK(IP0J,ILEV)
      COMMON /PAK/ TTPNPAK(IP0J,ILEV)
      COMMON /PAK/ TTPPPAK(IP0J,ILEV)
      COMMON /PAK/ TTPVPAK(IP0J,ILEV)
#endif
#if (defined (tprhsc) || defined (radforce))
      COMMON /PAK/ TTPLPAK(IP0J,ILEV)
      COMMON /PAK/ TTPSPAK(IP0J,ILEV)
      COMMON /PAK/ TTSCPAK(IP0J,ILEV)
      COMMON /PAK/ TTLCPAK(IP0J,ILEV)
#endif
#if defined qprhsc
C
C     * QPRHSC defined
      COMMON /PAK/ QTPCPAK(IP0J,ILEV)
      COMMON /PAK/ QTPMPAK(IP0J,ILEV)
      COMMON /PAK/ QTPPPAK(IP0J,ILEV)
      COMMON /PAK/ QTPVPAK(IP0J,ILEV)
#endif
#if defined uprhsc
C
C     * UPRHSC defined
      COMMON /PAK/ UTPCPAK(IP0J,ILEV)
      COMMON /PAK/ UTPGPAK(IP0J,ILEV)
      COMMON /PAK/ UTPNPAK(IP0J,ILEV)
      COMMON /PAK/ UTPSPAK(IP0J,ILEV)
      COMMON /PAK/ UTPVPAK(IP0J,ILEV)
#endif
#if defined vprhsc
C
C     * VPRHSC defined
      COMMON /PAK/ VTPCPAK(IP0J,ILEV)
      COMMON /PAK/ VTPGPAK(IP0J,ILEV)
      COMMON /PAK/ VTPNPAK(IP0J,ILEV)
      COMMON /PAK/ VTPSPAK(IP0J,ILEV)
      COMMON /PAK/ VTPVPAK(IP0J,ILEV)
#endif
#if defined xconsav
C
C     * XCONSAV defined
      COMMON /PAK/ XADDPAK(IP0J,2,NTRAC)
      COMMON /PAK/ XTPHPAK(IP0J,2,NTRAC)
      COMMON /PAK/ XTPFPAK(IP0J,ILEV,NTRAC)
#endif
#if defined xprhs
C
C     * XPRHS defined
      COMMON /PAK/ XTPPAK(IP0J,ILEV,NTRAC)
#endif
#if defined xprhsc
C
C     * XPRHSC defined
      COMMON /PAK/ XTPCPAK(IP0J,ILEV,NTRAC)
      COMMON /PAK/ XTPMPAK(IP0J,ILEV,NTRAC)
      COMMON /PAK/ XTPPPAK(IP0J,ILEV,NTRAC)
      COMMON /PAK/ XTPVPAK(IP0J,ILEV,NTRAC)

#endif
#if defined xtraconv
C
C     * XTRACONV DEFINED
C
      COMMON /PAK/ BCDPAK (IP0J)
      COMMON /PAK/ BCSPAK (IP0J)
      COMMON /PAK/ CAPEPAK(IP0J)
      COMMON /PAK/ CDCBPAK(IP0J)
      COMMON /PAK/ CINHPAK(IP0J)
      COMMON /PAK/ CSCBPAK(IP0J)
      COMMON /PAK/ DMCPAK (IP0J,ILEV)
      COMMON /PAK/ DMCDPAK(IP0J,ILEV)
      COMMON /PAK/ DMCUPAK(IP0J,ILEV)
      COMMON /PAK/ SMCPAK (IP0J,ILEV)
      COMMON /PAK/ TCDPAK (IP0J)
      COMMON /PAK/ TCSPAK (IP0J)
#endif
#if defined xtrachem
      REAL NOXDPAK
C
      COMMON /PAK/ DD4PAK (IP0J)
      COMMON /PAK/ DOX4PAK(IP0J)
      COMMON /PAK/ DOXDPAK(IP0J)
      COMMON /PAK/ ESDPAK (IP0J)
      COMMON /PAK/ ESFSPAK(IP0J)
      COMMON /PAK/ EAISPAK(IP0J)
      COMMON /PAK/ ESTSPAK(IP0J)
      COMMON /PAK/ EFISPAK(IP0J)
      COMMON /PAK/ ESFBPAK(IP0J)
      COMMON /PAK/ EAIBPAK(IP0J)
      COMMON /PAK/ ESTBPAK(IP0J)
      COMMON /PAK/ EFIBPAK(IP0J)
      COMMON /PAK/ ESFOPAK(IP0J)
      COMMON /PAK/ EAIOPAK(IP0J)
      COMMON /PAK/ ESTOPAK(IP0J)
      COMMON /PAK/ EFIOPAK(IP0J)
      COMMON /PAK/ EDSLPAK(IP0J)
      COMMON /PAK/ EDSOPAK(IP0J)
      COMMON /PAK/ ESVCPAK(IP0J)
      COMMON /PAK/ ESVEPAK(IP0J)
      COMMON /PAK/ NOXDPAK(IP0J)
      COMMON /PAK/ WDD4PAK(IP0J)
      COMMON /PAK/ WDL4PAK(IP0J)
      COMMON /PAK/ WDS4PAK(IP0J)
#ifndef pla
      COMMON /PAK/ DDDPAK (IP0J)
      COMMON /PAK/ DDBPAK (IP0J)
      COMMON /PAK/ DDOPAK (IP0J)
      COMMON /PAK/ DDSPAK (IP0J)
      COMMON /PAK/ WDLDPAK(IP0J)
      COMMON /PAK/ WDLBPAK(IP0J)
      COMMON /PAK/ WDLOPAK(IP0J)
      COMMON /PAK/ WDLSPAK(IP0J)
      COMMON /PAK/ WDDDPAK(IP0J)
      COMMON /PAK/ WDDBPAK(IP0J)
      COMMON /PAK/ WDDOPAK(IP0J)
      COMMON /PAK/ WDDSPAK(IP0J)
      COMMON /PAK/ WDSDPAK(IP0J)
      COMMON /PAK/ WDSBPAK(IP0J)
      COMMON /PAK/ WDSOPAK(IP0J)
      COMMON /PAK/ WDSSPAK(IP0J)
      COMMON /PAK/ DD6PAK (IP0J)
      COMMON /PAK/ PHDPAK (IP0J,ILEV)
      COMMON /PAK/ PHLPAK (IP0J,ILEV)
      COMMON /PAK/ PHSPAK (IP0J,ILEV)
      COMMON /PAK/ SDHPPAK(IP0J)
      COMMON /PAK/ SDO3PAK(IP0J)
      COMMON /PAK/ SLHPPAK(IP0J)
      COMMON /PAK/ SLO3PAK(IP0J)
      COMMON /PAK/ SSHPPAK(IP0J)
      COMMON /PAK/ SSO3PAK(IP0J)
      COMMON /PAK/ WDD6PAK(IP0J)
      COMMON /PAK/ WDL6PAK(IP0J)
      COMMON /PAK/ WDS6PAK(IP0J)
#endif
#endif
#if (defined(pla) && defined(pam))
      COMMON /PAK/ SSLDPAK (IP0J,ILEV)
      COMMON /PAK/ RESSPAK (IP0J,ILEV)
      COMMON /PAK/ VESSPAK (IP0J,ILEV)
      COMMON /PAK/ DSLDPAK (IP0J,ILEV)
      COMMON /PAK/ REDSPAK (IP0J,ILEV)
      COMMON /PAK/ VEDSPAK (IP0J,ILEV)
      COMMON /PAK/ BCLDPAK (IP0J,ILEV)
      COMMON /PAK/ REBCPAK (IP0J,ILEV)
      COMMON /PAK/ VEBCPAK (IP0J,ILEV)
      COMMON /PAK/ OCLDPAK (IP0J,ILEV)
      COMMON /PAK/ REOCPAK (IP0J,ILEV)
      COMMON /PAK/ VEOCPAK (IP0J,ILEV)
      COMMON /PAK/ AMLDPAK (IP0J,ILEV)
      COMMON /PAK/ REAMPAK (IP0J,ILEV)
      COMMON /PAK/ VEAMPAK (IP0J,ILEV)
      COMMON /PAK/ FR1PAK  (IP0J,ILEV)
      COMMON /PAK/ FR2PAK  (IP0J,ILEV)
      COMMON /PAK/ ZCDNPAK (IP0J,ILEV)
      COMMON /PAK/ BCICPAK (IP0J,ILEV)
      COMMON /PAK/ OEDNPAK (IP0J,ILEV,KEXTT)
      COMMON /PAK/ OERCPAK (IP0J,ILEV,KEXTT)
      COMMON /PAK/ OIDNPAK (IP0J,ILEV)
      COMMON /PAK/ OIRCPAK (IP0J,ILEV)
      COMMON /PAK/ SVVBPAK (IP0J,ILEV)
      COMMON /PAK/ PSVVPAK (IP0J,ILEV)
      COMMON /PAK/ SVMBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ SVCBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ PNVBPAK (IP0J,ILEV,ISAINTT)
      COMMON /PAK/ PNMBPAK (IP0J,ILEV,ISAINTT,NRMFLD)
      COMMON /PAK/ PNCBPAK (IP0J,ILEV,ISAINTT,NRMFLD)
      COMMON /PAK/ PSVBPAK (IP0J,ILEV,ISAINTT,KINTT)
      COMMON /PAK/ PSMBPAK (IP0J,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /PAK/ PSCBPAK (IP0J,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /PAK/ QNVBPAK (IP0J,ILEV,ISAINTT)
      COMMON /PAK/ QNMBPAK (IP0J,ILEV,ISAINTT,NRMFLD)
      COMMON /PAK/ QNCBPAK (IP0J,ILEV,ISAINTT,NRMFLD)
      COMMON /PAK/ QSVBPAK (IP0J,ILEV,ISAINTT,KINTT)
      COMMON /PAK/ QSMBPAK (IP0J,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /PAK/ QSCBPAK (IP0J,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /PAK/ QGVBPAK (IP0J,ILEV)
      COMMON /PAK/ QGMBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ QGCBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ QDVBPAK (IP0J,ILEV)
      COMMON /PAK/ QDMBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ QDCBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ ONVBPAK (IP0J,ILEV,ISAINTT)
      COMMON /PAK/ ONMBPAK (IP0J,ILEV,ISAINTT,NRMFLD)
      COMMON /PAK/ ONCBPAK (IP0J,ILEV,ISAINTT,NRMFLD)
      COMMON /PAK/ OSVBPAK (IP0J,ILEV,ISAINTT,KINTT)
      COMMON /PAK/ OSMBPAK (IP0J,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /PAK/ OSCBPAK (IP0J,ILEV,ISAINTT,KINTT,NRMFLD)
      COMMON /PAK/ OGVBPAK (IP0J,ILEV)
      COMMON /PAK/ OGMBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ OGCBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ DEVBPAK (IP0J,ILEV,KEXTT)
      COMMON /PAK/ PDEVPAK (IP0J,ILEV,KEXTT)
      COMMON /PAK/ DEMBPAK (IP0J,ILEV,KEXTT,NRMFLD)
      COMMON /PAK/ DECBPAK (IP0J,ILEV,KEXTT,NRMFLD)
      COMMON /PAK/ DIVBPAK (IP0J,ILEV)
      COMMON /PAK/ PDIVPAK (IP0J,ILEV)
      COMMON /PAK/ DIMBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ DICBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ REVBPAK (IP0J,ILEV,KEXTT)
      COMMON /PAK/ PREVPAK (IP0J,ILEV,KEXTT)
      COMMON /PAK/ REMBPAK (IP0J,ILEV,KEXTT,NRMFLD)
      COMMON /PAK/ RECBPAK (IP0J,ILEV,KEXTT,NRMFLD)
      COMMON /PAK/ RIVBPAK (IP0J,ILEV)
      COMMON /PAK/ PRIVPAK (IP0J,ILEV)
      COMMON /PAK/ RIMBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ RICBPAK (IP0J,ILEV,NRMFLD)
      COMMON /PAK/ SULIPAK (IP0J,ILEV)
      COMMON /PAK/ RSUIPAK (IP0J,ILEV)
      COMMON /PAK/ VSUIPAK (IP0J,ILEV)
      COMMON /PAK/ F1SUPAK (IP0J,ILEV)
      COMMON /PAK/ F2SUPAK (IP0J,ILEV)
      COMMON /PAK/ BCLIPAK (IP0J,ILEV)
      COMMON /PAK/ RBCIPAK (IP0J,ILEV)
      COMMON /PAK/ VBCIPAK (IP0J,ILEV)
      COMMON /PAK/ F1BCPAK (IP0J,ILEV)
      COMMON /PAK/ F2BCPAK (IP0J,ILEV)
      COMMON /PAK/ OCLIPAK (IP0J,ILEV)
      COMMON /PAK/ ROCIPAK (IP0J,ILEV)
      COMMON /PAK/ VOCIPAK (IP0J,ILEV)
      COMMON /PAK/ F1OCPAK (IP0J,ILEV)
      COMMON /PAK/ F2OCPAK (IP0J,ILEV)
#if defined (xtrapla1)
      COMMON /PAK/ SNCNPAK (IP0J,ILEV)
      COMMON /PAK/ SSUNPAK (IP0J,ILEV)
      COMMON /PAK/ SCNDPAK (IP0J,ILEV)
      COMMON /PAK/ SGSPPAK (IP0J,ILEV)
      COMMON /PAK/ CCNPAK  (IP0J,ILEV)
      COMMON /PAK/ CC02PAK (IP0J,ILEV)
      COMMON /PAK/ CC04PAK (IP0J,ILEV)
      COMMON /PAK/ CC08PAK (IP0J,ILEV)
      COMMON /PAK/ CC16PAK (IP0J,ILEV)
      COMMON /PAK/ CCNEPAK (IP0J,ILEV)
      COMMON /PAK/ ACASPAK (IP0J,ILEV)
      COMMON /PAK/ ACOAPAK (IP0J,ILEV)
      COMMON /PAK/ ACBCPAK (IP0J,ILEV)
      COMMON /PAK/ ACSSPAK (IP0J,ILEV)
      COMMON /PAK/ ACMDPAK (IP0J,ILEV)
      COMMON /PAK/ NTPAK   (IP0J,ILEV)
      COMMON /PAK/ N20PAK  (IP0J,ILEV)
      COMMON /PAK/ N50PAK  (IP0J,ILEV)
      COMMON /PAK/ N100PAK (IP0J,ILEV)
      COMMON /PAK/ N200PAK (IP0J,ILEV)
      COMMON /PAK/ WTPAK   (IP0J,ILEV)
      COMMON /PAK/ W20PAK  (IP0J,ILEV)
      COMMON /PAK/ W50PAK  (IP0J,ILEV)
      COMMON /PAK/ W100PAK (IP0J,ILEV)
      COMMON /PAK/ W200PAK (IP0J,ILEV)
      COMMON /PAK/ RCRIPAK (IP0J,ILEV)
      COMMON /PAK/ SUPSPAK (IP0J,ILEV)
      COMMON /PAK/ HENRPAK (IP0J,ILEV)
      COMMON /PAK/ O3FRPAK (IP0J,ILEV)
      COMMON /PAK/ H2O2FRPAK(IP0J,ILEV)
      COMMON /PAK/ WPARPAK (IP0J,ILEV)
      COMMON /PAK/ PM25PAK (IP0J,ILEV)
      COMMON /PAK/ PM10PAK (IP0J,ILEV)
      COMMON /PAK/ DM25PAK (IP0J,ILEV)
      COMMON /PAK/ DM10PAK (IP0J,ILEV)
      COMMON /PAK/ VNCNPAK (IP0J)
      COMMON /PAK/ VASNPAK (IP0J)
      COMMON /PAK/ VSCDPAK (IP0J)
      COMMON /PAK/ VGSPPAK (IP0J)
      COMMON /PAK/ VOAEPAK (IP0J)
      COMMON /PAK/ VBCEPAK (IP0J)
      COMMON /PAK/ VASEPAK (IP0J)
      COMMON /PAK/ VMDEPAK (IP0J)
      COMMON /PAK/ VSSEPAK (IP0J)
      COMMON /PAK/ VOAWPAK (IP0J)
      COMMON /PAK/ VBCWPAK (IP0J)
      COMMON /PAK/ VASWPAK (IP0J)
      COMMON /PAK/ VMDWPAK (IP0J)
      COMMON /PAK/ VSSWPAK (IP0J)
      COMMON /PAK/ VOADPAK (IP0J)
      COMMON /PAK/ VBCDPAK (IP0J)
      COMMON /PAK/ VASDPAK (IP0J)
      COMMON /PAK/ VMDDPAK (IP0J)
      COMMON /PAK/ VSSDPAK (IP0J)
      COMMON /PAK/ VOAGPAK (IP0J)
      COMMON /PAK/ VBCGPAK (IP0J)
      COMMON /PAK/ VASGPAK (IP0J)
      COMMON /PAK/ VMDGPAK (IP0J)
      COMMON /PAK/ VSSGPAK (IP0J)
      COMMON /PAK/ VOACPAK (IP0J)
      COMMON /PAK/ VBCCPAK (IP0J)
      COMMON /PAK/ VASCPAK (IP0J)
      COMMON /PAK/ VMDCPAK (IP0J)
      COMMON /PAK/ VSSCPAK (IP0J)
      COMMON /PAK/ VASIPAK (IP0J)
      COMMON /PAK/ VAS1PAK (IP0J)
      COMMON /PAK/ VAS2PAK (IP0J)
      COMMON /PAK/ VAS3PAK (IP0J)
      COMMON /PAK/ VCCNPAK (IP0J)
      COMMON /PAK/ VCNEPAK (IP0J)
#endif
#if defined (xtrapla2)
      COMMON /PAK/ CORNPAK (IP0J,ILEV)
      COMMON /PAK/ CORMPAK (IP0J,ILEV)
      COMMON /PAK/ RSN1PAK (IP0J,ILEV)
      COMMON /PAK/ RSM1PAK (IP0J,ILEV)
      COMMON /PAK/ RSN2PAK (IP0J,ILEV)
      COMMON /PAK/ RSM2PAK (IP0J,ILEV)
      COMMON /PAK/ RSN3PAK (IP0J,ILEV)
      COMMON /PAK/ RSM3PAK (IP0J,ILEV)
      COMMON /PAK/ SDNUPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SDMAPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SDACPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SDCOPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SSSNPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SMDNPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SIANPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SSSMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SMDMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SEWMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SSUMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SOCMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SBCMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SIWMPAK (IP0J,ILEV,ISDNUM)
      COMMON /PAK/ SDVLPAK (IP0J,ISDIAG)
      COMMON /PAK/ VRN1PAK (IP0J)
      COMMON /PAK/ VRM1PAK (IP0J)
      COMMON /PAK/ VRN2PAK (IP0J)
      COMMON /PAK/ VRM2PAK (IP0J)
      COMMON /PAK/ VRN3PAK (IP0J)
      COMMON /PAK/ VRM3PAK (IP0J)
      COMMON /PAK/ DEFAPAK(IP0J)
      COMMON /PAK/ DEFCPAK(IP0J)
      COMMON /PAK/ DMACPAK(IP0J,ISDUST)
      COMMON /PAK/ DMCOPAK(IP0J,ISDUST)
      COMMON /PAK/ DNACPAK(IP0J,ISDUST)
      COMMON /PAK/ DNCOPAK(IP0J,ISDUST)
      COMMON /PAK/ DEFXPAK(IP0J,ISDIAG)
      COMMON /PAK/ DEFNPAK(IP0J,ISDIAG)
      COMMON /PAK/ TNSSPAK(IP0J)
      COMMON /PAK/ TNMDPAK(IP0J)
      COMMON /PAK/ TNIAPAK(IP0J)
      COMMON /PAK/ TMSSPAK(IP0J)
      COMMON /PAK/ TMMDPAK(IP0J)
      COMMON /PAK/ TMOCPAK(IP0J)
      COMMON /PAK/ TMBCPAK(IP0J)
      COMMON /PAK/ TMSPPAK(IP0J)
      COMMON /PAK/ SNSSPAK(IP0J,ISDNUM)
      COMMON /PAK/ SNMDPAK(IP0J,ISDNUM)
      COMMON /PAK/ SNIAPAK(IP0J,ISDNUM)
      COMMON /PAK/ SMSSPAK(IP0J,ISDNUM)
      COMMON /PAK/ SMMDPAK(IP0J,ISDNUM)
      COMMON /PAK/ SMOCPAK(IP0J,ISDNUM)
      COMMON /PAK/ SMBCPAK(IP0J,ISDNUM)
      COMMON /PAK/ SMSPPAK(IP0J,ISDNUM)
      COMMON /PAK/ SIWHPAK(IP0J,ISDNUM)
      COMMON /PAK/ SEWHPAK(IP0J,ISDNUM)
#endif
#endif
#if defined xtrals
      REAL MLTIPAK,MLTSPAK
      COMMON /PAK/ AGGPAK (IP0J,ILEV)
      COMMON /PAK/ AUTPAK (IP0J,ILEV)
      COMMON /PAK/ CNDPAK (IP0J,ILEV)
      COMMON /PAK/ DEPPAK (IP0J,ILEV)
      COMMON /PAK/ EVPPAK (IP0J,ILEV)
      COMMON /PAK/ FRHPAK (IP0J,ILEV)
      COMMON /PAK/ FRKPAK (IP0J,ILEV)
      COMMON /PAK/ FRSPAK (IP0J,ILEV)
      COMMON /PAK/ MLTIPAK(IP0J,ILEV)
      COMMON /PAK/ MLTSPAK(IP0J,ILEV)
      COMMON /PAK/ RACLPAK(IP0J,ILEV)
      COMMON /PAK/ RAINPAK(IP0J,ILEV)
      COMMON /PAK/ SACIPAK(IP0J,ILEV)
      COMMON /PAK/ SACLPAK(IP0J,ILEV)
      COMMON /PAK/ SNOWPAK(IP0J,ILEV)
      COMMON /PAK/ SUBPAK (IP0J,ILEV)
      COMMON /PAK/ SEDIPAK(IP0J,ILEV)
      COMMON /PAK/ RLIQPAK(IP0J,ILEV)
      COMMON /PAK/ RICEPAK(IP0J,ILEV)
      COMMON /PAK/ RLNCPAK(IP0J,ILEV)
      COMMON /PAK/ CLIQPAK(IP0J,ILEV)
      COMMON /PAK/ CICEPAK(IP0J,ILEV)

      COMMON /PAK/ RLIQPAL(IP0J,ILEV)
      COMMON /PAK/ RICEPAL(IP0J,ILEV)
      COMMON /PAK/ RLNCPAL(IP0J,ILEV)
      COMMON /PAK/ CLIQPAL(IP0J,ILEV)
      COMMON /PAK/ CICEPAL(IP0J,ILEV)
C
      COMMON /PAK/ VAGGPAK(IP0J)
      COMMON /PAK/ VAUTPAK(IP0J)
      COMMON /PAK/ VCNDPAK(IP0J)
      COMMON /PAK/ VDEPPAK(IP0J)
      COMMON /PAK/ VEVPPAK(IP0J)
      COMMON /PAK/ VFRHPAK(IP0J)
      COMMON /PAK/ VFRKPAK(IP0J)
      COMMON /PAK/ VFRSPAK(IP0J)
      COMMON /PAK/ VMLIPAK(IP0J)
      COMMON /PAK/ VMLSPAK(IP0J)
      COMMON /PAK/ VRCLPAK(IP0J)
      COMMON /PAK/ VSCIPAK(IP0J)
      COMMON /PAK/ VSCLPAK(IP0J)
      COMMON /PAK/ VSUBPAK(IP0J)
      COMMON /PAK/ VSEDIPAK(IP0J)
      COMMON /PAK/ RELIQPAK(IP0J)
      COMMON /PAK/ REICEPAK(IP0J)
      COMMON /PAK/ CLDLIQPAK(IP0J)
      COMMON /PAK/ CLDICEPAK(IP0J)
      COMMON /PAK/ CTLNCPAK(IP0J)
      COMMON /PAK/ CLLNCPAK(IP0J)

      COMMON /PAK/ RELIQPAL(IP0J)
      COMMON /PAK/ REICEPAL(IP0J)
      COMMON /PAK/ CLDLIQPAL(IP0J)
      COMMON /PAK/ CLDICEPAL(IP0J)
      COMMON /PAK/ CTLNCPAL(IP0J)
      COMMON /PAK/ CLLNCPAL(IP0J)
#endif
#if defined use_cosp
! COSP INPUT
      COMMON /PAK/ RMIXPAK(IP0J,ILEV)
      COMMON /PAK/ SMIXPAK(IP0J,ILEV)
      COMMON /PAK/ RREFPAK(IP0J,ILEV)
      COMMON /PAK/ SREFPAK(IP0J,ILEV)
#endif
#if defined (aodpth)
C
C     EXxx: EXTINCTION
C     ODxx: OPTICAL DEPTH
C     OFxx: FINE MODE OPTICAL DEPTH (BULK), OF06: ANGSTROM COEFFICIENT (PLA)
C     ABxx: ABSORPTION
C
C     xxBx: AT BAND 1
C     xxSx: AT 550 NM
C
C     xxx1: SO4,
C     xxx2: SS,
C     xxx3: DU,
C     xxx4: BC,
C     xxx5: POM
C     xxxT: TOTAL
C
      COMMON /PAK/ EXB1PAK(IP0J)
      COMMON /PAK/ EXB2PAK(IP0J)
      COMMON /PAK/ EXB3PAK(IP0J)
      COMMON /PAK/ EXB4PAK(IP0J)
      COMMON /PAK/ EXB5PAK(IP0J)
      COMMON /PAK/ EXBTPAK(IP0J)
      COMMON /PAK/ ODB1PAK(IP0J)
      COMMON /PAK/ ODB2PAK(IP0J)
      COMMON /PAK/ ODB3PAK(IP0J)
      COMMON /PAK/ ODB4PAK(IP0J)
      COMMON /PAK/ ODB5PAK(IP0J)
      COMMON /PAK/ ODBTPAK(IP0J)
      COMMON /PAK/ ODBVPAK(IP0J)
      COMMON /PAK/ OFB1PAK(IP0J)
      COMMON /PAK/ OFB2PAK(IP0J)
      COMMON /PAK/ OFB3PAK(IP0J)
      COMMON /PAK/ OFB4PAK(IP0J)
      COMMON /PAK/ OFB5PAK(IP0J)
      COMMON /PAK/ OFBTPAK(IP0J)
      COMMON /PAK/ ABB1PAK(IP0J)
      COMMON /PAK/ ABB2PAK(IP0J)
      COMMON /PAK/ ABB3PAK(IP0J)
      COMMON /PAK/ ABB4PAK(IP0J)
      COMMON /PAK/ ABB5PAK(IP0J)
      COMMON /PAK/ ABBTPAK(IP0J)
C
      COMMON /PAK/ EXB1PAL(IP0J)
      COMMON /PAK/ EXB2PAL(IP0J)
      COMMON /PAK/ EXB3PAL(IP0J)
      COMMON /PAK/ EXB4PAL(IP0J)
      COMMON /PAK/ EXB5PAL(IP0J)
      COMMON /PAK/ EXBTPAL(IP0J)
      COMMON /PAK/ ODB1PAL(IP0J)
      COMMON /PAK/ ODB2PAL(IP0J)
      COMMON /PAK/ ODB3PAL(IP0J)
      COMMON /PAK/ ODB4PAL(IP0J)
      COMMON /PAK/ ODB5PAL(IP0J)
      COMMON /PAK/ ODBTPAL(IP0J)
      COMMON /PAK/ ODBVPAL(IP0J)
      COMMON /PAK/ OFB1PAL(IP0J)
      COMMON /PAK/ OFB2PAL(IP0J)
      COMMON /PAK/ OFB3PAL(IP0J)
      COMMON /PAK/ OFB4PAL(IP0J)
      COMMON /PAK/ OFB5PAL(IP0J)
      COMMON /PAK/ OFBTPAL(IP0J)
      COMMON /PAK/ ABB1PAL(IP0J)
      COMMON /PAK/ ABB2PAL(IP0J)
      COMMON /PAK/ ABB3PAL(IP0J)
      COMMON /PAK/ ABB4PAL(IP0J)
      COMMON /PAK/ ABB5PAL(IP0J)
      COMMON /PAK/ ABBTPAL(IP0J)
C
      COMMON /PAK/ EXS1PAK(IP0J)
      COMMON /PAK/ EXS2PAK(IP0J)
      COMMON /PAK/ EXS3PAK(IP0J)
      COMMON /PAK/ EXS4PAK(IP0J)
      COMMON /PAK/ EXS5PAK(IP0J)
      COMMON /PAK/ EXSTPAK(IP0J)
      COMMON /PAK/ ODS1PAK(IP0J)
      COMMON /PAK/ ODS2PAK(IP0J)
      COMMON /PAK/ ODS3PAK(IP0J)
      COMMON /PAK/ ODS4PAK(IP0J)
      COMMON /PAK/ ODS5PAK(IP0J)
      COMMON /PAK/ ODSTPAK(IP0J)
      COMMON /PAK/ ODSVPAK(IP0J)
      COMMON /PAK/ OFS1PAK(IP0J)
      COMMON /PAK/ OFS2PAK(IP0J)
      COMMON /PAK/ OFS3PAK(IP0J)
      COMMON /PAK/ OFS4PAK(IP0J)
      COMMON /PAK/ OFS5PAK(IP0J)
      COMMON /PAK/ OFSTPAK(IP0J)
      COMMON /PAK/ ABS1PAK(IP0J)
      COMMON /PAK/ ABS2PAK(IP0J)
      COMMON /PAK/ ABS3PAK(IP0J)
      COMMON /PAK/ ABS4PAK(IP0J)
      COMMON /PAK/ ABS5PAK(IP0J)
      COMMON /PAK/ ABSTPAK(IP0J)
C
      COMMON /PAK/ EXS1PAL(IP0J)
      COMMON /PAK/ EXS2PAL(IP0J)
      COMMON /PAK/ EXS3PAL(IP0J)
      COMMON /PAK/ EXS4PAL(IP0J)
      COMMON /PAK/ EXS5PAL(IP0J)
      COMMON /PAK/ EXSTPAL(IP0J)
      COMMON /PAK/ ODS1PAL(IP0J)
      COMMON /PAK/ ODS2PAL(IP0J)
      COMMON /PAK/ ODS3PAL(IP0J)
      COMMON /PAK/ ODS4PAL(IP0J)
      COMMON /PAK/ ODS5PAL(IP0J)
      COMMON /PAK/ ODSTPAL(IP0J)
      COMMON /PAK/ ODSVPAL(IP0J)
      COMMON /PAK/ OFS1PAL(IP0J)
      COMMON /PAK/ OFS2PAL(IP0J)
      COMMON /PAK/ OFS3PAL(IP0J)
      COMMON /PAK/ OFS4PAL(IP0J)
      COMMON /PAK/ OFS5PAL(IP0J)
      COMMON /PAK/ OFSTPAL(IP0J)
      COMMON /PAK/ ABS1PAL(IP0J)
      COMMON /PAK/ ABS2PAL(IP0J)
      COMMON /PAK/ ABS3PAL(IP0J)
      COMMON /PAK/ ABS4PAL(IP0J)
      COMMON /PAK/ ABS5PAL(IP0J)
      COMMON /PAK/ ABSTPAL(IP0J)
C
#endif
#if defined x01
      COMMON /PAK/ X01PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x02
      COMMON /PAK/ X02PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x03
      COMMON /PAK/ X03PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x04
      COMMON /PAK/ X04PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x05
      COMMON /PAK/ X05PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x06
      COMMON /PAK/ X06PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x07
      COMMON /PAK/ X07PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x08
      COMMON /PAK/ X08PAK (IP0J,ILEV,NTRAC)
#endif
#if defined x09
      COMMON /PAK/ X09PAK (IP0J,ILEV,NTRAC)
#endif
#if defined agcm_river_routing
C
C     * RIVER ROUTING FIELDS PASSED THROUGH RESTART.
C     * NOTE!!! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
C     *         AT SOME POINT.
C
      COMMON /GRDFULL/ YGWOGRD(LONSL+1,NLAT)
      COMMON /GRDFULL/ YGWSGRD(LONSL+1,NLAT)
      COMMON /GRDFULL/ YSWOGRD(LONSL+1,NLAT)
      COMMON /GRDFULL/ YSWSGRD(LONSL+1,NLAT)
      COMMON /GRDFULL/ YSWIGRD(LONSL+1,NLAT)
      COMMON /GRDFULL/ YDEPGRD(LONSL+1,NLAT)
C
C     * RIVER ROUTING FIELDS SAVED TO HISTORY FILE.
C     * NOTE!!! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
C     *         AT SOME POINT.
C
      COMMON /GRDFULL/ RVELGRD(LONSL+1,NLAT)
      COMMON /GRDFULL/ FOULGRD(LONSL+1,NLAT)
#endif
C
C     * RIVER ROUTING FIELDS SAVED TO HISTORY FILE **AND**
C     * WRITTEN TO COUPLER.
C     * NOTE!!! THESE MAY ULTIMATELY CONVERTED TO "PAK" FIELDS
C     *         AT SOME POINT.
C
      COMMON /GRDFULL/ RIVOGRD(LONSL+1,NLAT)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
