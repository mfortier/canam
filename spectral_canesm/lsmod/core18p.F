#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      SUBROUTINE CORE18P (J, IOFF,  JOFF, IOFFZ, JOFFZ,
     1   FOUR1P, NLEVI,
     2 ENVELOP, GSPONG, GNOGWDS, GNOGWDH, GNOGWDM, NCDIM, NADIM, LMAM,
     3    LCSW,   LCLW,  LSCOSP, ALFMOD,
     4     IL1,    IL2,     IL1Z, IL2Z, LEV, 
     5  ITRVAR,ITRAFLX, NTRSPEC,     INP,  IEXPLVOL, IVTAU, 
     X  NTRACP, IAIND,
     6    IGWD, NAZMTH,    NHAR, 
     7   KOUNT,    GMT,     DAY,     DT,
     8   QREFL,  QREFU,    SREF,    SPOW,   SMIN,   QMIN,
     9 TOTENERJ,  QMOMJ,  TFICQJ, TRACOLJ,  TMOMJ, TFICXJ, TPHSXJ,
     + TSCALJ, SCALJ,
     A  FOURP1,  NLEVO                                                 )
C
C     * Oct 01/2018 - S.Kharin.    Add quadrature for XSFX and XTVI.
C     * Aug 13/2018 - M.Lazare.    Remove IOCEAN and ICEMOD in call to physici.
C     * JUL 27/2018 - M.Lazare.    Remove {COM,CSP,CSN} parmsub variables.
C     * Mar 04/2018 - M.Lazare.    Revise quadrature for tile fractions. This
C     *                            requires adding ISTIND common block.
C     * Feb 27/2018 - M.Lazare.    Add quadrature for {BWGK,QFSO} for water
C     *                            conservation with lakes.
C     * Jan 29/2018 - M.Lazare.    Initialize angle to due E-W for GSD
C     *                            launching (PHIEROW) to zero for AGCM.
C     * Nov 01/2017 - J. Cole.     - Add support for CMIP6 stratospheric aerosols.
C     * Jan 27/2014 - M.Lazare.    New version for gcm18:
C     *                            - Calls new COMPAK12,COMROW12,
C     *                              UNPACK10,PACK10,PHYSICI,PSIZES_18.
C     * JUL 14/2013 - M.LAZARE/    Previous version core17p for gcm17:
C     *               K.VONSALZEN. - Revised calls to new
C     *                              PSIZES_17,COMPAK11,COMROW11,
C     *                              UNPACK9,PACK9,PHYSICH.
C     *                            - NSUPS removed (not used).
C     *                            - Support for PAM added (new
C     *                              IAIND,NTRACP).
C     * MAY 08/2012 - M.LAZARE. PREVIOUS VERSION CORE16P FOR GCM16:
C     *                         - REVISED CALLS TO NEW SUBROUTINES.
C     * APR 30/2010 - M.LAZARE. PREVIOUS VERSION CORE15PI FOR GCM15I:
C     *                         - NEW CALL TO NEW PHYSICFI (ALSO TO
C     *                           PASS MOLECULAR WEIGHT ARRAY) AND
C     *                           TO NEW ENER9 FOR CONSERVATION
C     *                           CHANGES.
C     *                         - LSCOSP PASSED IN AND COSP ENVIRONMENT
C     *                           OBTAINED THROUGH CALL TO COSP DEFS
C     *                           MODULE IF COSP TURNED ON.
C     *                         - ADDED CALCULATION OF NEW DIAGNOSTIC
C     *                           VERTICAL VELOCITY FIELD ("OMET")
C     *                           THROUGH AN ADDED CALL TO THE NEW
C     *                           SUBROUTINE "CALCW".
C     *                         - OPTIMIZATION OF "QHYB" MOISTURE
C     *                           AND TRACER CORRECTION SECTIONS.
C     *                         - MODIFY TRACER CONSERVATION CALCULATION
C     *                           SECTION TO COMBINE "ITRVAR=Q" AND
C     *                           "ITRVAR=QHYB BUT XREF=0" INTO ONE.
C     *                           ALSO, A GENERAL CONSERVATION
C     *                           CALCULATION IS NOW DONE REGARDLESS OF
C     *                           THE CHOICE OF "ITRVAR", AND 
C     *                           CONSERVATION IS SKIPPED FOR SPECIFIED-CO2.
C     *                         - MODIFICATIONO TO TRACER TENDENCY USED
C     *                           FOR CONSERVATION ("TENDFAC") TO BE
C     *                           TRACER DENSITY FOR CO2.
C     *                         - NEW CALL TO NEW COM15I.
C     * FEB 23/2009 - M.LAZARE. PREVIOUS VERSION CORE15PH FOR GCM15H:
C     *                         - PASS THE EXPLOSIVE VOLCANOES SWITCH
C     *                           "IEXPLVOL" THROUGH FROM GCM MAIN TO
C     *                           PHYSICS.
C     *                         - ADD DEFINITIONS FOR NEW FIELDS
C     *                           REPRESENTING VERTICAL INTEGRAL OF
C     *                           ABSOLUTE VALUE OF HYBRID MOISTURE/
C     *                           TRACER TENDENCY (QTPT/XTPT).
C     *                         - REMOVE DEFINITION OF QCTOFF AND ONLY
C     *                           USE TCTOFF DURING SPIN-UP PERIOD 
C     *                           (DEFINED WHEN TRACER VALUE IS LESS
C     *                           THAN A VERY SMALL FRACTION). THESE
C     *                           WERE LOWER BOUNDS APPLIED TO ABSOLUTE
C     *                           VALUES OF THE PHYSICS TENDENCIES FOR
C     *                           MOISTURE AND TRACER, RESPECTIVELY.
C     *                         - MODIFY HOLE-FILLING TENDENCY
C     *                           DIAGNOSTICS TO PROPERLY DO FOR
C     *                           HYBRID VARIABLES AND NOT MIXING
C     *                           RATIOS. THIS SIMPLIFIES THE TRACER
C     *                           CODE AS IT CAN BE DONE ONCE BEFORE
C     *                           THE X->T CONVERSION, RATHER THAN
C     *                           SEPARATELY IN TWO DIFFERENT SECTIONS
C     *                           DEPENDING ON WHETHER TRACER WAS
C     *                           HYBRID OR NOT.
C     *                         - REVISED CALLS FOR NEW VERSIONS OF
C     *                           VARIOUS COMDECKS AND PHYSICS DRIVER. 
C     * DEC 21/2007 - M.LAZARE. PREVIOUS VERSION CORE15PG FOR GCM15G:
C     *                         - CALLS TO REVISED SUBROUTINES AND COMMON
C     *                           DECKS.
C     *                         - HYBRID VARIABLE AND CONSERVATION CODE
C     *                           RELOCATED HERE FROM PHYSICS.
C     *                         - ADVECTIVE TIMESTEP (DTADV) DEFINED HERE;
C     *                           USED FOR SAVING CONSERVATION-RELATED
C     *                           FIELDS AND PASSED TO PHYSICS AS WELL.   
C     *                         - REMOVE CODE RELATED TO S/L (INCLUDING
C     *                           ITZ,ITP.
C     *                         - CALLS TO NEW COM15G AND COM15PG.. 
C     * NOV 26/2006 - M.LAZARE. PREVIOUS VERSION CORE15PF FOR GCM15F:  
C     *                         - CALLS NEW {COMPAK9F,PACK7F,UNPACK7F}
C     *                           WITH TWO EXTRA FIELDS ("DMC" AND "SMC"), 
C     *                           UNDER CONTROL OF "%IF DEF,XTRACONV".
C     * SEP 12/2006 - M.LAZARE. - REMOVE SINJ, WOCSJ FROM GAUSSJ COMMON
C     *                           BLOCK.
C     *                         - CALCULATE AND PASS 32-BIT VERSIONS OF
C     *                           RADJ AND WJ TO PHYSICS.
C     * JUN 27/2006 - M.LAZARE. - Calls COM15PF instead of COM13PC.
C     *                         - COSJ and SINJ no longer passed to physics.
C     *                         - QMIN PASSED IN AS WELL.
C     *                         - AH,BH removed from the call to physics.
C     *                         - Calls COM15F instead of COM13C.
C     *                         - COSD,SINSD no longer passed through
C     *                           this subroutine to physics, in
C     *                           conjunction with other changes to 
C     *                           gcm15f.
C     *                         - unused common block "EPSICE" removed.
C     *                         - unused QREFL,QREFU,XSCL0,XSRC0 not
C     *                           passed.
C     *                         - calling sequence reorganized for 
C     *                           clarity.
C     *                         - IOCEAN,ICEMOD,ITRAC passed in call
C     *                           to physicff so that "IOPTION" common
C     *                           block can be removed.
C     *                         - ICSW,ISGG,ISBEG,ISRAD,ISMON,ISHF,JLATPR
C     *                           passed in call to physicff so that
C     *                           "INTVLS","HFSAVE" common blocks can
C     *                           be removed.
C     *                         - MOIST PASSED THROUGH CALL TO PHYSICFF
C     *                           SO THAT "SIZES" COMMON BLOCK CAN BE
C     *                           REMOVED.
C     *                         - DELT,IDAY,LDAY,MDAY,MDAYT,INCD PASSED
C     *                           THROUGH CALL TO PHYSICFF SO THAT
C     *                           "TIMES" COMMON BLOCK CAN BE REMOVED.
C     *                         - LOCAL WORK ARRAYS IN F90 FORMAT.   
C     *                         - "USES" PSIZES_15F INSTEAD OF PSIZES_15D.
C     *                         - CALLS NEW COMROW9F.
C     *                         - CALLS NEW PHYSICFF, WITHOUT PASSING
C     *                           OBSELETE IVA,IWRKPH (ALSO IN CALL).
C     * MAY 07/2006 - M.LAZARE. PREVIOUS VERSION CORE15PE FOR GCM15E: 
C     *                         - BASED ON CORE15PD EXCEPT CALLS 
C     *                           PHYSICFE,UNPACK7E,PACK7E,COMPAK9E,COMROW9E
C     *                           INSTEAD OF "D" COUNTERPARTS. 
C     * DEC 15/2005 - M.LAZARE.   PREVIOUS VERSION CORE15PD FOR GCM15D:
C
C     * DICTIONARY FOR THE ARGUMENTS OF THIS ROUTINE
C     * --------------------------------------------
C
C            ***  INPUT ARGUMENTS ***
C                 ---------------
C
C         NAME       TYPE    SIGNIFICATION
C     * ---------  -------   ------------- 
C     * J        : INTEGER  : CURRENT LATITUDE SET INDEX.
C     * IOFF     : INTEGER  : OFFSET INTO GRID ARRAYS.
C     * JOFF     : INTEGER  : OFFSET INTO NODE LATITUDE SPACE FOR A
C     *                       GIVEN TASK.
C     * FOUR1P   : REAL     : FOURIER COMPONENTS OF PROGNOSTIC VARIABLES
C     *                       FOR THIS NODE LATITUDE SET (SIZE 
C     *                       (2,NLEVI,ILAT,ILH)
C     *
C     * ENVELOP  : LOGICAL  : OROGRAPHY SWITCH.
C     * GSPONG   : LOGICAL  : MAM SPONGE SWITCH.
C     * GNOGWDH  : LOGICAL  : OROGRAPHY SWITCH.
C     * GNOGWDM  : LOGICAL  : OROGRAPHY SWITCH.
C     * LMAM     : LOGICAL  : MAM SWITCH.
C     * LCSW     : LOGICAL  : SHORTWAVE RADIATION SWITCH.
C     * LSCOSP     : LOGICAL  : COSP SIMULATOR SWITCH. 
C     * ALFMOD   : REAL     : WEIGHT USE IN VERTICAL DISCRETIZATION.
C     * IL1      : INTEGER  : STARTING LONGITUDE INDEX FOR THIS PASS. 
C     * IL2      : INTEGER  : ENDING   LONGITUDE INDEX FOR THIS PASS.
C     * IL1Z     : INTEGER  : STARTING ZONAL LONGITUDE INDEX FOR THIS PASS. 
C     * IL2Z     : INTEGER  : ENDING   ZONAL LONGITUDE INDEX FOR THIS PASS.
C     * LEV      : INTEGER  : NUMBER OF LEVELS + 1
C     * ITRVAR   : INTEGER  : TRACER VARIABLE TYPE.
C     * ITRAFLX  : INTEGER  : SWITCH TO INDICATE SURFACE TRACER FLUX INPUT.
C     * NTRSPEC  : INTEGER  : NUMBER OF SPECTRALLY-ADVECTED TRACERS.
C     * INP      : INTEGER  : T+1 TIME-INDEX OF NON-ADVECTED TRACERS.
C
C     * IGWD     : INTEGER  | CHOICE OF OROGRAPHIC GWD SCHEME.
C     * NAZMTH   : INTEGER  | NUMBER OF AZIMUTHS FOR NON-ORO (HINES)SCHEME.
C     * NHAR     : INTEGER  : NUMBER OF HARMONICS FOR OROGRAPHIC GWD.

C     * KOUNT    : INTEGER  : CURRENT TIMESTEP NUMBER.
C     * GMT      : REAL     : GREEWICH TIME OF THE DAY (SEC)
C     * DAY      : REAL     : DAY OF THE YEAR.
C     * DT       : REAL     : TIMESTEP LENGTH (= DELT OR DELT/2)
C
C     * QREFL    : REAL     : SPECIFIC HUMIDITY LOWER BOUND REFERENCE 
C     *                       VALUE FOR CONSERVATION SCHEME.
C     * QREFU    : REAL     : SPECIFIC HUMIDITY UPPER BOUND REFERENCE 
C     *                       VALUE FOR CONSERVATION SCHEME.
C     * SREF     : REAL     : REFERENCE MOISTURE VALUE FOR QHYB.
C     * SPOW     : REAL     : POWER              VALUE FOR QHYB.
C     * SMIN     : REAL     : MINIMUM MOISTURE   VALUE FOR QHYB.
C
C            ***  OUTPUT  ARGUMENTS ***
C                 -----------------
C     * TOTENERJ : REAL     : ARRAY USED TO HOLD PARTIAL CONTRIBUTION OF
C     *                       THE CURRENT LATITUDE SET (J) TO GLOBAL MEAN
C     *                       ENERGETICS STORE IN TOTENER.
C     * TFICQJ   : REAL     : USE IN A SIMILAR FASHION THAN TOTENERJ BUT FOR
C     *                       MOISTURE CONSERVATION.
C     * TRACOLJ  : REAL     : LIKE TOTENERJ BUT FOR TRACERS.                 
C     * TFICXJ   : REAL     : LIKE TFICQJ BUT FOR TRACERS.
C     * TPHSXJ  : REAL     : LIKE TFICXJ BUT (FOR TRACERS) REPRESENTS
C     *                       PHYSICS SOURCES/SINKS AND USED IN 
C     *                       CORRECTION (QHYB).
C     *
C     * FOURP1   : REAL     : FOURIER COMPONENTS OF VARIABLE SET #1
C     *                       FOR THIS NODE LATITUDE SET (SIZE 
C     *                       (2,NLEVO,ILAT,ILH)
C     -------------------------------------------------------------------
C
      USE PSIZES_19
#if defined use_cosp
      USE cosp_defs
#endif
#if (defined(pla) && defined(pam))
      USE COMPAR
#endif
C
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
C
C     * OUTPUT FOURIER ARRAYS (PASSED AND USED AS REAL).
C
      REAL FOURP1(2,NLEVO,ILAT,ILH)	!<Variable description\f$[units]\f$
C
C     * INPUT FOURIER ARRAY (PASSED AND USED AS REAL).
C
      REAL FOUR1P(2,NLEVI,ILAT,ILH)	!<Variable description\f$[units]\f$
C
C     * DYNAMIC MEMORY POINTER FOR SCRATCH ARRAY "SC".
C
      REAL, ALLOCATABLE, DIMENSION(:) :: SC

#include "com15i.h"
#include "com15pg.h"
#include "compak12.h"
#include "comrow12.h"
C
C     * I/O ACCUMULATION ARRAYS.
C
      REAL QMOMJ(ILEV,NTASK_P)		!<Variable description\f$[units]\f$
      REAL TMOMJ (ILEV,NTRAC,NTASK_P)	!<Variable description\f$[units]\f$
      REAL SCALJ(ILEV,NTASK_P)		!<Variable description\f$[units]\f$
      REAL TSCALJ(ILEV,NTRAC,NTASK_P)	!<Variable description\f$[units]\f$

      REAL TOTENERJ(ILEV,7,NTASK_P)	!<Variable description\f$[units]\f$
      REAL TRACOLJ (ILEV,NTRAC,NTASK_P)	!<Variable description\f$[units]\f$
      REAL TFICQJ (ILEV,NTASK_P)	!<Variable description\f$[units]\f$
      REAL TFICXJ (ILEV,NTRAC,NTASK_P)	!<Variable description\f$[units]\f$
      REAL TPHSXJ(ILEV,NTRAC,NTASK_P)	!<Variable description\f$[units]\f$
C
C     * ADDITIONAL I/O ARRAYS.
C
      INTEGER, DIMENSION(NTRACP) :: IAIND !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
C     * LOCAL INTERNAL WORK ARRAYS:
C
C     * NEW OUTPUT FIELDS USED IN PHYSICS.
C
      REAL, DIMENSION(ILG,ILEV)         ::  WG,OMEG
C
C     * TRACG AND TRACTG USED TO HOLD THE TRACERS AND THEIR TENDENCIES
C     * ON THE GRID FOR BOTH THE SPECTAL AND SL OPTIONS.  NOW SINCE ONLY
C     * THE ADVECTED TRACERS ARE CARRIED INTO AND OUT OF THE SPECTRAL
C     * TRANSFORMS TRACG CANNOT BE THE SAME SIZE FOR BOTH THE SPECTRAL
C     * (NTRSPEC) AND SL (NTRAC) OPTIONS. ADD SOME GRID POINT WORK ARRAYS
C     * TO COMBINE ADVECTED AND NON-ADVECTED TRACERS WHEN SPECTAL 
C     * ADVECTION SELECTED.  THESE WILL NOW BE THE ARRAYS WHICH ARE
C     * USED BY THE SL OPTION SINCE THESE ARE OF DIMENSION NTRAC.
C
      REAL, DIMENSION(ILG,ILEV)         :: QADDROL,QG,QTG,QOROW
      REAL, DIMENSION(ILG,ILEV,NTRAC)   :: XADDROL
      REAL, DIMENSION(ILG,ILEV,NTRAC)   :: WTRACG,WTRACTG
      REAL, DIMENSION(ILG,ILEV,NTRAC)   :: XG,XTG,XOROW
      REAL, DIMENSION(ILG)              :: RADJN,AREAROW
C
C     * LOGICAL SWITCHES.
C
      LOGICAL ENVELOP
      LOGICAL GSPONG, GNOGWDS, GNOGWDH, GNOGWDM, LMAM
      LOGICAL LCSW, LCLW, LSCOSP
C
C     * THERE ARE TWO OPTIONS FOR RUNNING WITH "Q" FOR MOISTURE AND/OR TRACERS.
C     * THE DEFAULT IS TO BORROW AROUND A LATITUDE CIRCLE FIRST IF POSSIBLE
C     * AND THEN FILL HOLES IF NECESSARY. THE RESULTANT HOLE-FILL FOR
C     * LEVEL IS STORED IN QADDROL/XADDROL AND SUBSEQUENTLY CORRECTED FOR
C     * IN SPECTRAL SPACE.
C     * IF NOT RUNNING WITH SPECTRAL CORRECTION FOR HOLE-FILLING, WE
C     * SUBTRACT OFF THE HOLE-FILL QUANTITY BEFORE CALCULATING THE PHYSICS
C     * TENDENCY AND THEN SET QADDROL/XADDROL=0 SO THAT THERE WILL BE NO 
C     * CORRECTION IN SPECTRAL SPACE.
C     * THIS OPTION IS IGNORED FOR "QHYB".
C
      LOGICAL SPECCOR
      DATA SPECCOR/.TRUE./ 
C
C     * THE FOLLOWING EQUIVILENCE IS NECESSARY SINCE DSHJ IS 1-D THROUGH
C     * COMMON BLOCK /GR/ WHILE 2-D REFERENCE IS NEEDED IN VERTICAL 
C     * INTEGRATION LOOPS.
C
C     * COMMON BLOCK NEEDED FOR TILE INDICES FOR AREAL FRACTIONS IN 
C     * QUADRATURE FOR LAKES FRESH WATER CONSERVATION.
C
      COMMON /ISTIND  / IULAK, IRLWT, IRLIC, IOWAT, IOSIC
C
      REAL DEL(ILG,ILEV)

!     NCS adding the below to get around old "equivalence"
!     problem when using openmp (equivalence not allowed here)     
      REAL tmp1d(ILGD*ILEV)
      
C===========================================================================
C     * DEFINE ADVECTED TIMESTEP BASED ON DT PASSED INTO CORE15PF, SINCE DT 
C     * CAN BE EITHER BE DELT OR DELT/2(LATTER FOR TIME=0 ONLY).
C
      DTADV=2.*DT
      RCDT    = 1./DTADV
      FAC     = 1./(GRAV*DTADV)
      MSG     = ILEV-LEVS
      SAVEMON = 1./REAL(ISMON)
      SAVERAD = 1./REAL(ISRAD)
C
C     * INITIALIZE CONSERVATION ARRAYS TO 0..
C
      QADDROL(IL1:IL2,1:ILEV)   =0.0
      XADDROL(IL1:IL2,1:ILEV, :)=0.0
C
C     * UNPACK PAK ARRAYS INTO ROW ARRAYS.
C
#include "unpack10.h"
C
C     * CALCULATE RELEVANT ARRAYS, TO BE PASSED TO PHYSICS..
C
      DO I=IL1,IL2
         JL(I)      = NINT(JLPAK(IOFF+I))
         ILSL(I)    = NINT(ILSLPAK(IOFF+I))
         RADJ(I)    = RADJPAK(IOFF+I)
         COSJ(I)    = COS(RADJ(I))
         WJ(I)      = WJPAK(IOFF+I)
         DLON(I)    = DLONPAK(IOFF+I)
         RADJN(I)   = RADJ(I)
         AREAROW(I) = 4.0*PI*(A**2)*WJ(I)*FLNDROW(I)/(2.0*REAL(LONSL))  !M^2
      ENDDO
C
C     * ZERO OUT PHYSICS TENDENCIES FOR RESULTING ACCUMULATION
C     * WITHIN PHYSICS.
C
          TTG(1:ILG, 1:ILEV)          = 0.0
          UTG(1:ILG, 1:ILEV)          = 0.0
          VTG(1:ILG, 1:ILEV)          = 0.0
         ESTG(1:ILG, 1:LEVS)          = 0.0
      WTRACTG(1:ILG, 1:ILEV, 1:NTRAC) = 0.0
C
C     * GET MEMORY DYNAMICALLY FOR THE SCRATCH ARRAY "SC".
C
      ALLOCATE(SC(LEN_MAXP))
C
C     * COMPUTE GRID POINT VALUES FROM SPECTRAL COEFF.
C     * CONVERT WIND IMAGES TO REAL WINDS FOR PHYSICS AND DYNAMICS.
C
C     * EVEN IF DOING SEMI-LAGRANGIAN TRANSPORT, THE TRANSFORMS WILL CONVERT 
C     * SPECTRAL MOISTURE TO THE GRID. HOWEVER, THIS WILL NOT BE USED,
C     * RATHER THE ROUTINE CBATRA2X WILL OVERWRITE IT WITH THE CORRECT
C     * LATITUDE SLICES EXTRACTED FROM THE 3D MOISTURE FIELD, I.E. "ES3D",
C     * FOR PROPER INPUT INTO THE PHYSICS ROUTINE. HERE WE DECIDED TO LET
C     * FOURF DO ITS NORMAL JOB IN ORDER NOT TO INTERFERE WITH THE
C     * FIELDS BEING ALIGNED IN MEMORY (FOR TRANSFORM PURPOSES), SINCE 
C     * DOING SO WOULD UNDULY INCREASE COMPUTATIONAL TIME. 
C     * FOR TRACERS, THE VARIABLE "NTRSPEC" IS USED TO DEFINE SIZES FOR
C     * TRANSFORMS, SO ONLY THOSE ACTUALLY BEING ADVECTED SPECTRALLY
C     * ARE TRANSFORMED.
C
      CALL FOURF(PRESSG,FOUR1P,NLEVI,
     1           ILH,LONSL,LMTOTAL,
     2           IFAX,TRIGS,NLATJ,ILAT,JOFF,
     3           SC(IXP(1)),SC(IXP(2))          )

      CALL IMVRAI2 (UG,VG,PSDLG,PSDPG,COSJ,ILG,LONP,ILEV,A)
C
      DO 120 I=1,LONP
          PRESSG(I)=EXP(PRESSG(I))
  120 CONTINUE
      CALL SIGCALH (
     1              DSGJ,DSHJ,DLNSGJ,
     2              D1SGJ,A1SGJ,B1SGJ,
     3              D2SGJ,A2SGJ,B2SGJ,
     4              ILG, LONP, ILEV,
     5              PTOIT,
     6              PRESSG,AG,BG,AH,BH,ALFMOD
     7              )
      
      tmp1d(:) = DSHJ(:)
      DEL = reshape(tmp1d(1:ILG*ILEV),(/ILG, ILEV/))
      
C   
C     * OVERWRITE OF ABOVE MOISTURE/TRACER FIELDS IF USING S/L OPTION.
C     * CALL THIS EVEN IF THERE ARE NO NON-ADVECTED TRACERS FOR SPECTRAL
C     * OPTION SINCE TRACG MUST BE COPIED INTO WTRACG
C
      CALL FILLNAF(TRACG,WTRACG,TRACNA(1,INP),IOFF,
     1             NTRAC,NTRACN,NTRSPEC,ITRADV,
     2             ILEV,IP0J,ILG,IL1,IL2)
C   
C     * DEFINE GRID SLICE OF LAYER DEFINITION FOR PHYSICS.
C
      CALL LEVCAL3 (SGBJ,SHTJ, SGJ,SHJ, DSGJ,DSHJ, 
     1              AG,BG, AH,BH, ACG,BCG, ACH,BCH, PRESSG,
     2              ILEV,ILEV+1,ILG,IL1,IL2,J,PTOIT)
C
      CALL CALCW (WG, OMETROW,
     1            CG, UG, VG, TG, PSDLG, PSDPG, PRESSG,
     2            SGJ, DSGJ, DSHJ, DLNSGJ, 
     3            A1SGJ, B1SGJ, A2SGJ, B2SGJ, DB,
     4            ILEV, ILG, IL1, IL2)
C==================================================================
C     * CALCULATE DIAGNOSTIC HOLE-FILLING TENDENCIES ACCUMULATED
C     * OVER MONTH, FOR BOTH MOISTURE AND ADVECTED TRACERS.
C     * NOTE THAT THIS WILL BE TRUE FOR Q OR QHYB, DUE TO USE OF
C     * MOISTURE/TRACER VARIABLE (ESG,WTRACG) INSTEAD OF Q,X.
C  
      DO L=MSG+1,ILEV
      DO I=IL1,IL2
         QTPFROM(I) = QTPFROM(I) + 
     1                PRESSG(I)*DEL(I,L)/GRAV*
     2                MAX(SMIN-ESG(I,L),0.)*SAVEMON*RCDT
      ENDDO
      ENDDO
C
      DO N=1,NTRAC
         IF(ITRADV(N).NE.0)               THEN
            DO L=1,ILEV
            DO I=IL1,IL2
               XTPFROM(I,N) = XTPFROM(I,N) + 
     1                        PRESSG(I)*DEL(I,L)/GRAV*
     2                        MAX(XMIN(N)-WTRACG(I,L,N),0.)*SAVEMON*RCDT
            ENDDO
            ENDDO
         ENDIF
      ENDDO
C==================================================================
#if defined qconsav
C     * FOR HYBRID MOISTURE VARIABLE, KEEP TRACK OF AMOUNT OF
C     * SPECIFIC HUMIDITY USED IN FILLING HOLES OF HYBRID
C     * MOISTURE VARIABLE (INCOMING ESG < SMIN, WHERE SMIN IS
C     * VALUE OF HYBRID MOISTURE VARIABLE AT Q=QMIN) IN ARRAY "QTPF".
C     * NOTE THAT SINCE ONE CANNOT DETERMINE A CORRESPONDING VALUE
C     * OF SPECIFIC HUMIDITY FOR A NEGATIVE VALUE OF ES, THE DERIVATIVE
C     * (DS/DQ) AT Q=QMIN IS USED AS AN APPROXIMATION.
C
      IF(MOIST.EQ.NC4TO8("QHYB") .OR. MOIST.EQ.NC4TO8("SLQB"))    THEN
          DSODQ = (SMIN/QMIN)*((SMIN/SREF)**SPOW)
          DO L=MSG+1,ILEV
          DO IL=IL1,IL2
              DS  = MAX( SMIN-ESG(IL,L-MSG) , 0. )
              DQ = DS/DSODQ
              QTPFROW(IL,L) = QTPFROW(IL,L) + DQ*RCDT*SAVERAD
          ENDDO
          ENDDO
      ENDIF
#endif
#if defined xconsav
C     * FOR HYBRID TRACER VARIABLE, KEEP TRACK OF AMOUNT OF
C     * MIXING RATIO USED IN FILLING HOLES OF HYBRID
C     * TRACER VARIABLE (INCOMING WTRACG < XMIN, WHERE XMIN IS
C     * VALUE OF HYBRID MOISTURE VARIABLE AT TMIN) IN ARRAY "XTPF".
C     * NOTE THAT SINCE ONE CANNOT DETERMINE A CORRESPONDING VALUE
C     * OF TRACER FOR A NEGATIVE VALUE OF WTRACG, THE DERIVATIVE
C     * (DX/DQ) AT TMIN IS USED AS AN APPROXIMATION.
C
      IF(ITRVAR.EQ.NC4TO8("QHYB") .OR. ITRVAR.EQ.NC4TO8("SLQB")) THEN
        DO N=1,NTRAC
          IF(ITRADV(N).NE.0)                              THEN 
            IF(XREF(N).EQ.0.) THEN
              DXODQ = 1.
            ELSE
              DXODQ = (XMIN(N)/TMIN(N))*((XMIN(N)/XREF(N))**XPOW(N))
            ENDIF
            DO L=1,ILEV
            DO IL=IL1,IL2
              DX  = MAX( XMIN(N)-WTRACG(IL,L,N) , 0. )
              DQ = DX/DXODQ
              XTPFROW(IL,L,N) = XTPFROW(IL,L,N) + DQ*RCDT*SAVERAD
            ENDDO
            ENDDO
          ENDIF 
        ENDDO
      ENDIF
#endif
C=================================================================
C     * GET SPECIFIC HUMIDITY ON GRID FROM MOISTURE VARIABLE.
C
      CALL HSCALA5 (QG, ESG, IL1, IL2, ILG, ILEV, LEVS, 
     1              MOIST, .FALSE., SREF, SPOW, QMIN, SMIN)
C
C     * CONVERSION OF TRACER VARIABLE (WTRACG) TO MIXING RATIO.
C
      DO N=1,NTRAC
C
C       * IF SPECIES IS NOT BEING ADVECTED, THEN VALUE REPRESENTS
C       * MIXING RATIO AND THEREFORE NO CONVERSION SHOULD BE
C       * DONE. WE TRICK THIS BY CALLING ROUTINE WITH "   Q".
C
        IF(ITRADV(N).EQ.0 .OR. XREF(N).EQ.0.)                  THEN
          ITRICK=NC4TO8("   Q")
        ELSE
          ITRICK=ITRVAR
        ENDIF
        CALL HSCALA5(XG(1,1,N),WTRACG(1,1,N), IL1, IL2, ILG, ILEV, ILEV, 
     1              ITRICK, .FALSE., XREF(N), XPOW(N), TMIN(N), XMIN(N))
      ENDDO
C==================================================================
C     * MOISTURE CORRECTION SECTION.
C
      IF(MOIST.EQ.NC4TO8("   Q"))                                THEN
C
C         * FILL IN "HOLES" IN MOISTURE FIELD (I.E. VALUES IN CARRIED
C         * MOISTURE LEVELS HAVING QG<QMIN).
C
            DO L=MSG+1,ILEV
              IF(SPECCOR)                  THEN
                CALL HOLEFIL3(QG(1,L),QADDROL(1,L),QMIN,
     1                        ILG,LONSL,IL1,IL2)  
              ELSE
                DO I=IL1,IL2
                  QADDROL(I,L) = MAX(QMIN-QG(I,L),0.)
                  QG(I,L)      = MAX(QG(I,L),QMIN)
                ENDDO
              ENDIF
#if defined qconsav
              DO I=IL1,IL2             
                QTPFROW(I,L)   = QTPFROW(I,L) + 
     1                           QADDROL(I,L) * RCDT * SAVERAD
              ENDDO
#endif
            ENDDO 

      ELSE IF(MOIST.EQ.NC4TO8("QHYB"))                           THEN
C
C         * CONSERVE MOISTURE BASED ON RATIO OF TIME-STEPPED
C         * GLOBAL PRECIPITABLE WATER (TAKING INTO ACCOUNT GLOBAL E-P)
C         * TO ACTUAL GLOBAL PRECIPITABLE WATER DERIVED FROM SPECIFIC
C         * HUMIDITY GAUSSIAN QUADRATURE ON GRID (LAGGED BY 2 TIMESTEPS
C         * AVOID DE-COUPLING).
C
C         * MOISTURE CONSERVATION FOR S/L WILL BE DONE ELSEWHERE 
C         * (I.E., JUST BEFORE THE ACTUAL S/L TIME STEP, IN THE
C         * MAIN GCM DRIVER).

          DO L=MSG+1,ILEV
          DO IL=IL1,IL2
             QOLD  = QG(IL,L)
             AFACT = (1.-QSCLM)*QSRCM*QWFMROL(IL,L)+QSCLM
             QG(IL,L)=QG(IL,L)*(1.+(QSRCRM1-1.)*AFACT)
             DQ    = QG(IL,L)-QOLD
             CONST = DEL(IL,L)*PRESSG(IL)*FAC
             QTPHROM(IL)= QTPHROM(IL) + CONST*DQ*SAVEMON
#if defined qconsav
             DQ2           = DQ*DQ
             QTPHROW(IL,1) = QTPHROW(IL,1) + CONST*DQ *SAVERAD
             QTPHROW(IL,2) = QTPHROW(IL,2) + CONST*DQ2*SAVERAD
#endif
          ENDDO
          ENDDO
      ENDIF

C
C     * SET LOWER BOUND ON QG AT KOUNT=0 IN CASE INITIALIZATION INADEQUATE.
C     * ADD OFFSET AT KOUNT=0.
C
      DO L=1,ILEV
      DO IL=IL1,IL2
        IF(KOUNT.EQ.0) THEN
          QG(IL,L)=MAX(QG(IL,L),2.E-6)
          QG(IL,L)=QG(IL,L)+QMIN
        ENDIF
      ENDDO
      ENDDO
C======================================================================
C     * TRACER CORRECTION SECTION.
C
      DO N=1,NTRAC
        IF(ITRADV(N).NE.0)               THEN
#if defined specified_co2
          IF(ITRNAM(N).EQ.NC4TO8(" CO2"))    THEN
            cycle
          ENDIF
#endif 
          IF(ITRVAR.EQ.NC4TO8("   Q") .OR. XREF(N).EQ.0.)           THEN
C
C           * FILL IN "HOLES" IN TRACER FIELD IF TRACER VARIABLE IS
C           * SPECIFIC CONCENTRATION. THE ARRAY XADDROL REPRESENTS THE
C           * TRACER QUANTITY ADDED TO THE SYSTEM.
C
            DO L=1,ILEV
              IF(SPECCOR)                  THEN
                CALL HOLEFIL3(XG(1,L,N),XADDROL(1,L,N),TMIN(N),
     1                        ILG,LONSL,IL1,IL2)
              ELSE
                DO I=IL1,IL2
                  XADDROL(I,L,N) = MAX(TMIN(N)-XG(I,L,N),0.)
                  XG     (I,L,N) = MAX(XG(I,L,N),TMIN(N))
                ENDDO
              ENDIF
#if defined xconsav
              DO I=IL1,IL2
                XTPFROW(I,L,N)   = XTPFROW(I,L,N) + 
     1                             XADDROL(I,L,N) * RCDT * SAVERAD
              ENDDO
#endif
            ENDDO
          ENDIF 
C
C         * CONSERVE TRACER BASED ON RATIO OF TIME-STEPPED GLOBAL TRACER
C         * TO ACTUAL TRACER AMOUNT DERIVED FROM GAUSSIAN QUADRATURE
C         * ON GRID (LAGGED BY 2 TIMESTEPS TO AVOID DE-COUPLING).
C         * TREAT NON-HYBRID TRACERS "Q" IN THE SAME WAY AS "QHYB"
C         * TRACERS W.R.T. CONSERVATION.
C
          DO L=1,ILEV
          DO IL=IL1,IL2
            XOLD  = XG(IL,L,N)
            AFACT = (1.-XSCLM(N))*XSRCM(N)*XWFMROL(IL,L,N)
     1              +XSCLM(N)
            XG(IL,L,N)=XG(IL,L,N)*(1.+(XSRCRM1(N)-1.)*AFACT)
            DX    = XG(IL,L,N)-XOLD
            CONST = DEL(IL,L)*PRESSG(IL)*FAC
            XTPHROM(IL,N)   = XTPHROM(IL,N)   + CONST*DX*SAVEMON 
#if defined xconsav
            DX2             = DX*DX
            XTPHROW(IL,1,N) = XTPHROW(IL,1,N) + CONST*DX *SAVERAD
            XTPHROW(IL,2,N) = XTPHROW(IL,2,N) + CONST*DX2*SAVERAD
#endif
          ENDDO
          ENDDO
        ENDIF
      ENDDO
C
C     * ADD OFFSET AT KOUNT=0.
C
      DO N=1,NTRAC
      DO L=1,ILEV
      DO IL=IL1,IL2
        IF(KOUNT.EQ.0) THEN
          XG(IL,L,N)=XG(IL,L,N)+TMIN(N)
        ENDIF
      ENDDO
      ENDDO
      ENDDO
C
C     * INITIALIZE ANGLE TO DUE E-W FOR GWD LAUNCHING TO ZERO FOR AGCM.
C     * THIS WILL **NOT** BE TRUE FOR RCM WRAPPER! 
C
      PHIEROW=0.
C
C     * PERFORM PHYSICS CALCULATIONS.
C
      CALL PHYSICI (J,
     1      QG,     TG,     XG,     UG,     VG, PRESSG, WG,
     +   QOROW,  XOROW,
     2     QTG,    TTG,    XTG,    UTG,    VTG,
     3     SGJ,    SHJ,   SGBJ,   SHTJ,   DSGJ,   DSHJ,  
     4  RADJN,    AREAROW,  ILSL,     JL,   DLON,
     5 ENVELOP,   IGWD, GSPONG,GNOGWDS,GNOGWDH,GNOGWDM,  NCDIM,  NADIM,
     6  NAZMTH,   NHAR,
     7    DELT,  DTADV,   IDAY,   LDAY,   MDAY,  MDAYT,   INCD, 
     8    LMAM,   LCSW,   LCLW, LSCOSP,
     9    ICSW,   ISGG,  ISBEG,  ISRAD,  ISMON,   ISHF,   IEXPLVOL,
     +    IVTAU,
     A    KSTART,  KOUNT,    GMT,  MOIST,  
     B     LEV,    IL1,    IL2, IL1Z, IL2Z, JLATPR,
     C   ITRAC,ITRAFLX, 
     D  ITRADV, ITRPHS, ITRLVS, ITRLVF, ITRWET, ITRCCH, ITRDRY, 
     E  IAIND,NTRACP,ITRSRF,CONTRAC,MW,TMIN)
C======================================================================
      IF(.NOT.SPECCOR)                                             THEN
C
C       * IF NOT RUNNING WITH SPECTRAL CORRECTION FOR HOLE-FILLING, WE
C       * SUBTRACT OFF THE HOLE-FILL QUANTITY BEFORE CALCULATING THE
C       * NEW MODEL VARIABLE FOR MOISTURE AND TRACERS, AND THEN SET 
C       * {QADDROL,XADDROL}=0 SO THAT THERE WILL BE NO CORRECTION IN
C       * SPECTRAL SPACE.
C       * THIS OPTION IS IGNORED FOR "QHYB".
C
        IF(MOIST.EQ.NC4TO8("   Q"))                         THEN
          DO L=MSG+1,ILEV
          DO IL=IL1,IL2
             QOROW  (IL,L) = QOROW(IL,L) - QADDROL(IL,L)
             QADDROL(IL,L) = 0.
          ENDDO
          ENDDO
        ENDIF 
C
        DO N=1,NTRAC
          IF(ITRVAR.EQ.NC4TO8("   Q") .OR. 
     1      (ITRVAR.EQ.NC4TO8("QHYB").AND.XREF(N).EQ.0.) )    THEN
            DO L=1,ILEV 
            DO IL=IL1,IL2
              XOROW  (IL,L,N) = XOROW(IL,L,N) - XADDROL(IL,L,N)  
              XADDROL(IL,L,N) = 0.
            ENDDO
            ENDDO
          ENDIF
        ENDDO
      ENDIF
C
C     * MODEL VARIABLE TENDENCIES.
C     * ** NOTE! ** IF NOT RUNNING WITH HYBRID VARIABLE, TYPICALLY
C     *             ESTG WILL BE IDENTICAL TO QTG AND WTRACTG WILL
C     *             BE IDENTICAL TO XTG (ASSUMING ILEV=LEVS FOR
C     *             MOISTURE.
C
      CALL TENDCL8(ESTG,ESG,QOROW,ILG,IL1,IL2,ILEV,LEVS,
     1             DTADV,MOIST,SREF,SPOW)
C
C     * CALCULATE VERTICAL INTEGRAL OF ABSOLUTE VALUE OF HYBRID MOISTURE
C     * TENDENCY.
C
      DO L=MSG+1,ILEV
      DO I=IL1,IL2
         QTPTROM(I) = QTPTROM(I) + 
     1                PRESSG(I)*DEL(I,L)/GRAV*
     2                MAX(ESTG(I,L),0.)*SAVEMON
      ENDDO
      ENDDO
C
      DO N=1,NTRAC
C
C       * IF SPECIES IS NOT BEING ADVECTED, THEN VALUE REPRESENTS
C       * MIXING RATIO AND THEREFORE NO CONVERSION SHOULD BE
C       * DONE. WE TRICK THIS BY CALLING ROUTINE WITH "   Q".
C
        IF(ITRADV(N).EQ.0 .OR. XREF(N).EQ.0.)                  THEN
          ITRICK=NC4TO8("   Q")
        ELSE
          ITRICK=ITRVAR
        ENDIF
        CALL TENDCL8(WTRACTG(1,1,N),WTRACG(1,1,N),XOROW(1,1,N),
     1               ILG,IL1,IL2,ILEV,ILEV,
     2               DTADV,ITRICK,XREF(N),XPOW(N))
C
C       * CALCULATE VERTICAL INTEGRAL OF ABSOLUTE VALUE OF HYBRID TRACER
C       * TENDENCY.
C
        DO L=1,ILEV
        DO IL=IL1,IL2
          XTPTROM(IL,N)= XTPTROM(IL,N) + 
     1                   PRESSG(IL)*DEL(IL,L)/GRAV*
     2                   MAX(WTRACTG(IL,L,N),0.)*SAVEMON
        ENDDO
        ENDDO
      ENDDO
C=======================================================================
C     * MOISTURE CONSERVATION SECTION.
C
      IF(MOIST.NE.NC4TO8("   Q"))                                 THEN
C
C       * CALCULATE INTEGRATED SOURCES/SINKS FOR MOISTURE CONSERVATION AT 
C       * NEXT STEP. 
C
        DO L=1,ILEV
        DO IL=IL1,IL2
          QADDROL(IL,L) = QTG(IL,L)
        ENDDO
        ENDDO
      ENDIF
#if defined qconsav
C
C     * CALCULATE ACCUMULATED MEAN AND VARIANCE OF PHYSICS MOISTURE SOURCE/SINKS.
C
      DO L=1,ILEV 
      DO IL=IL1,IL2
        CONST         = DEL(IL,L)*PRESSG(IL)*FAC
        DQ            = QTG(IL,L)*DTADV
        DQ2           = DQ*DQ
        QADDROW(IL,1) = QADDROW(IL,1) + CONST*DQ *SAVERAD
        QADDROW(IL,2) = QADDROW(IL,2) + CONST*DQ2*SAVERAD
      ENDDO
      ENDDO
#endif
C
#if defined xconsav
C
C     * CALCULATE ACCUMULATED VARIANCE OF PHYSICS TRACER SOURCE/SINKS.
C
      DO N=1,NTRAC  
      DO L=1,ILEV 
      DO IL=IL1,IL2
        CONST           = DEL(IL,L)*PRESSG(IL)*FAC
        DX              = XTG(IL,L,N)*DTADV
        DX2             = DX*DX
        XADDROW(IL,1,N) = XADDROW(IL,1,N) + CONST*DX *SAVERAD
        XADDROW(IL,2,N) = XADDROW(IL,2,N) + CONST*DX2*SAVERAD
      ENDDO
      ENDDO
      ENDDO
#endif
C
C     * CALCULATE GLOBAL INTEGRALS OF MOISTURE/TRACER TO BE USED
C     * IN CONSERVATION. NOTE THAT THIS IS DONE ELSEWHERE (CORE15SI)
C     * FOR S/L VARIABLES.
C
      IQEN=0
      ITEN=0
      IF ( MOIST.NE.NC4TO8("SL3D") .AND.
     1     MOIST.NE.NC4TO8("SLQB"))      IQEN=1
      IF (ITRVAR.NE.NC4TO8("SL3D") .AND.
     1    ITRVAR.NE.NC4TO8("SLQB"))      ITEN=1
C
C     * MAGNITUDES OF LOCAL MASS CORRECTION. THE GOAL IS
C     * TO CALCULATE THE CORRECTION PROPORTIONAL TO THE TIME RATE OF
C     * CHANGE IN MIXING RATIOS OWING TO TRANSPORT AND HORIZONTAL DIFFUSION.
C     * IN THIS CASE, THE TIME RATE OF CHANGE DUE TO PHYSICAL PROCESSES
C     * IS USED AS A PROXY FOR THE CHANGE IN MIXING RATIOS.
C
      DO L=1,ILEV
      DO IL=IL1,IL2
          QWFMROL(IL,L)=QWF0ROL(IL,L)
          QWF0ROL(IL,L)=ABS(QTG(IL,L))
      ENDDO
      ENDDO
C
      ICUTOFF=1
      DO N=1,NTRAC
      DO L=1,ILEV
      DO IL=IL1,IL2
        IF ( ABS(XWF0ROL(IL,L,N)) > 1.E-20 ) ICUTOFF=0
      ENDDO
      ENDDO
      ENDDO
      IF ( ICUTOFF==1 ) THEN
        TCTOFF=1.E-20
      ELSE
        TCTOFF=0.
      ENDIF
      DO N=1,NTRAC
      DO L=1,ILEV
      DO IL=IL1,IL2
          XWFMROL(IL,L,N)=XWF0ROL(IL,L,N)
C 
C         * FOR GENERAL TRACERS, THE TENDENCY USED FOR CONSERVATION IS
C         * BASED ON MMR, BUT FOR CO2 IN THE CONTEXT OF THE CARBON
C         * CYCLE, IT IS BASED ON TRACER DENSITY (KG/M3).
C
          TENDFAC=XTG(IL,L,N)
#if defined carbon
          IF(ITRNAM(N).EQ.NC4TO8(" CO2"))    THEN
            TENDFAC=XTG(IL,L,N)*PRESSG(IL)*SHJ(IL,L)/(RGAS*TG(IL,L))
          ENDIF
#endif
          XWF0ROL(IL,L,N)=MAX(ABS(TENDFAC),TCTOFF)
      ENDDO
      ENDDO
      ENDDO
C
C     * SAVE PHYSICS GRID RESULTS INTO PAK ARRAYS.
C
#include "pack10.h"
C
C     * CALL FILLNAB EVEN IF THERE ARE NO NON-ADVECTED TRACERS FOR SPECTRAL
C     * OPTION SINCE THE ADVECTED TRACERS IN WTRACTG MUST BE COPIED INTO 
C     * TRACTG.
C
      CALL FILLNAB(TRACNA(1,INP),WTRACTG,TRACTG,DTADV,IOFF,
     1             NTRAC,NTRACN,NTRSPEC,ITRADV,
     2             ILEV,IP0J,ILG,IL1,IL2)                  
C
C     * FINALLY, COMPUTE CONTRIBUTIONS TO GLOBAL INTEGRALS FOR THIS PASS.
C
      CALL ENERQX4(TOTENERJ(1,4,J),QMOMJ(1,J),  QWF0ROL,SCALJ(1,J),
     1             TRACOLJ (1,1,J),TMOMJ(1,1,J),XWF0ROL,TSCALJ(1,1,J),
     1             XOROW,ITRAC,NTRAC,ITEN,
     2             QOROW,PRESSG,IQEN,
     3             DSHJ,IL1,IL2,ILG,ILEV,LONSL,
     4             WJ)            
      CALL ENER9  (TOTENERJ(1,1,J),TOTENERJ(1,2,J),TOTENERJ(1,3,J), 
     1             TOTENERJ(1,5,J),TOTENERJ(1,6,J), 
     2             TFICXJ(1,1,J),TPHSXJ(1,1,J),XADDROL,XTG,
     +             XREF,ITRAC,NTRAC,ITRVAR,  
     3             TFICQJ(1,J),  QADDROL,
     4             PG,CG,TG,UG,VG,PRESSG,
     5             MOIST,DSGJ,DSHJ,ILG,LONP,ILEV,LONSL,WJ,SC(IE(1))  )
C
C     * DO QUADRATURE FOR BWGK AND QFSO.
C
      DO IL=1,LONP
        WX      = 0.5*WJ(IL)/REAL(LONSL)
        BWGK(J) = BWGK(J) + WX*BWGKROL(IL)*FAREROT(IL,IULAK)
        QFSO(J) = QFSO(J) + WX*QFSOROL(IL)*FAREROT(IL,IOWAT)
      ENDDO
C
C     * DO QUADRATURE FOR XSFX AND SAMPLED XTVI
C
      DO N=1,NTRAC
        DO IL=1,LONP
          WX      = 0.5*WJ(IL)/REAL(LONSL)
          XSFX(N,J) = XSFX(N,J) + WX*XSFXROW(IL,N)
          XTVI(N,J) = XTVI(N,J) + WX*XTVIROS(IL,N)
        ENDDO
      ENDDO
C
C     * RESCALE TENDENCY TERMS IN OLD TRADITION.
C
      CALL VRAIMAP(VTG,UTG,ILG,LONP,ILEV,COSJ,A)
C
C     * FOURIER TRANSFORM OF PHYSICS GRID TENDENCIES.
C  
      CALL FOURB(FOURP1,TTG,NLEVO,
     1           ILH,NLATJ,ILAT,JOFF,LMTOTAL,
     2           LONSL,IFAX,TRIGS,
     3           SC(IXP(1)),SC(IXP(2))               )

      DEALLOCATE(SC)

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
