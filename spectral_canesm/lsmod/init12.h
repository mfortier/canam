#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
C     * Jul 09/2020 - M.Lazare.   - Generalization of GTPAT to GTPAK instead of 0.
C     *                             This was causing problems when initialization
C     *                             program produced SICN=0 and then coupler had
C     *                             SICN>0. In that case, GTPAT=0. found its way into
C     *                             routine DRCOEF for ice condition, which led to a
C     *                             NaN for WSTAR, etc. (when GTGAT=0.).    
C     * May 06/2020 - M.Lazare.   - Remove IF condition on NTLK>1 since always done.
C     * Aug 14/2018 - M.Lazare.   - Remove MASKPAK and use FLND instead.
C     * Aug 07/2018 - M.Lazare.   - Remove zeroing of FCOO since done in gcm18.dk.
C     *                           - Added zeroing of a number of fields existing in
C     *                             restarting which are defined/used in physics. Most,
C     *                             like the radiation fields, are defined at GMT=0
C     *                             so these will be overwritten but still need to exist
C     *                             in restart read.
C     *                           - Added zeroing of {CLDTPAL,CLDOPAL} since needed in physics.
C     *                           - Added zeroing of other 
C     * Aug 01/2018 - M.Lazare.   - Remove unused GC.
C     * Feb 27/2018 - M.Lazare.   - QFSL and BEGL changed from PAK/ROW to
C     *                             PAL/ROL.
C     *                           - Added {BEGK,BWGK,BWGL,QFSO}.
C     *                           - Moved accumulated fields from INIT12 to ZEROACC4.
C     * May 12/2018 - M.Lazare.    Correct resetting of SAND to -3. so
C     *                            that it is not being done over water.
C     * Aug 09/2017 - M.Lazare.    Major changes in initialization of tiled
C     *                            fields.
C     * Aug 07/2017 - M.Lazare.    New Git verison.
C     * Mar 07/2015 - M.Lazare/    Final version for gcm18:
C     *               K.Vonsalzen. - Define GCPAK for saving at KOUNT=0.
C     *                            - Define fractional area arrays.
C     *                            - Initialize CSALPAT,SALBPAT,EMISPAT.
C     *                            - Initialize BEGIPAL,BEGOPAL,
C     *                              BWGOPAL,HFLIPAL,HSEAPAL,OBEGPAL,
C     *                              OBWGPAL.
C     *                            - Initialize certain PLA arrays. 
C     * Apr 17/2014 - M.Lazare.    Interim version for gcm18:
C     *                            - NTLD used instead of IM for land-only   
C     *                              mosaic fields.
C     *                            - FLAKPAK,FLNDPAK,LICNPAK added.
C     *                            - Remove MASKPAK from setting land tile
C     *                              soil properties (unnecessary because
C     *                              already has to be land).
C     *                            - {XFSPAK,XSRFPAK,XTVIPAK} moved to new
C     *                              ZEROACC4.
C     *                            - "use_cosp", "pla", fields removed 
C     *                              to new ZEROACC4.
C     * Nov 19/2013 - M.Lazare.    Cosmetic: Remove {GFLX,GA,HBL,ILMO,PET,UE,
C     *                                      WTAB,ROFS,ROFB) "PAT" arrays.
C     * Jul 10/2013 - M.Lazare/    Previous version init11 for gcm17:
C     *               K.Vonsalzen/ - Certain PLA fields initialized to zero.
C     *               E.Chan.      - Remove PORG and only use a temporary
C     *                              scalar DZG for soil moisture conversion.
C     *                              The calculation remains for CLASSB
C     *                              which is saved for these two invariant
C     *                              fields (along with newly-saved field
C     *                              capacity FCAP) **after** the physics. 
C     *                            - Modified for use with packed tile
C     *                              variables (PAT)
C     *                            - Added setting of FARE (temporary).
C     *                            - Added setting/computation of PAK
C     *                              variables based on the associated PAT
C     *                              versions, particularly for output at
C     *                              KOUNT=0.
C     *                            - Revised to support arbitrary number of
C     *                              soil layers.
C     *                            - Define soil layers as in CLASSB.
C     *                            - Added TPND,ZPND,TAV,QAV,WSNO,TSFS.
C     *                            - Set SNO values below 1.E-3 to 0.
C     *                            - Initialized required CTEM fields.
C     * MAY 08/2012 - M.LAZARE.    PREVIOU VERSION INIT10 FOR GCM16:
C     *                            - FDLPAL_R REPLACED BY FLGPAL_R FOR
C     *                              RADIATIVE FORCING.
C     *                            - REMOVE {FSA,FLA,FSTC,FLTC}.
C     *                            - ADD {FSAM,FLAM} FIELDS.
C     * MAY 04/2010 - M.LAZARE.    PREVIOUS VERSION INIT9I FOR GCM15I:
C     *                            - ADD/REMOVE FIELDS (SEE OTHER
C     *                              COMMON DECKS FOR DETAILS).
C     *                            - UPDATE DIRECTIVES CHANGED TO
C     *                              CPP DIRECTIVES.
C     * FEB 19/2009 - M.LAZARE.    PREVIOUS VERSION INIT9H FOR GCM15H:
C     *                            - BUGFIX TO INITIALIZE
C     *                              CSAL IN THE SAME WAY WE DO FOR SALB. 
C     *                            - REMOVE THE INITIALIZATION OF ALL THE
C     *                              SOLAR "PAL" ARRAYS WHICH IS 
C     *                              UNNECESSARY SINCE THIS IS ALREADY DONE
C     *                              AT THE BEGINNING OF THE RADIATION.
C     *                            - ADD NEW FIELDS FOR EMISSIONS: EOFF,
C     *                              EBFF,EOBB,EBBB.
C     *                            - ADD NEW FIELDS FOR DIAGNOSTICS OF
C     *                              CONSERVATION: QTPT,XTPT.
C     *                            - ADD NEW FIELD FOR CHEMISTRY: SFRC.
C     *                            - ADD NEW DIAGNOSTIC CLOUD FIELD
C     *                              (USING OPTICAL DEPTH CUTOFF): CLDO. 
C     * APR 21/2008 - L.SOLHEIM/   PREVIOUS VERSION INIT9G FOR GCM15G:
C     *               M.LAZARE/    -  ADD NEW RADIATIVE FORCING ARRAYS
C     *               K.VONSALZEN/    (UNDER CONTROL OF "%DF RADFORCE").
C     *               X.MA.        - NEW DIAGNOSTIC FIELDS: WDD4,WDS4,EDSL,
C     *                              ESBF,ESFF,ESVC,ESVE,ESWF ALONG WITH
C     *                              TDEM->EDSO (UNDER CONTROL OF
C     *                              "XTRACHEM"), AS WELL AS ALMX,ALMC
C     *                              AND INSTANTANEOUS CLWT,CIDT.
C     *                            - REMOVE UNUSED QTPN,TSEM. 
C     * JAN 11/2006 - J.COLE/      PREVIOUS VERSION INIT9F FOR GCM15F:
C     *               M.LAZARE.    ADD ISCCP SIMULATOR FIELDS FROM JASON.  
C     * NOV 26/2006 - M.LAZARE.    TWO EXTRA NEW FIELDS ("DMC" AND "SMC")
C     *                            UNDER CONTROL OF "%IF DEF,XTRACONV".
C     * JUN 20/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - USE VARIABLES INSTEAD OF CONSTANTS
C     *                              IN INTRINSICS SUCH AS "MAX" OR 
C     *                              "MIN", TO ENABLE COMPILING WITH
C     *                              IMPLICIT REAL*8 IN 32-BIT MODE.  
C     * MAY 07/2006 - M.LAZARE.    PREVIOUS VERSION INIT9E FOR GCM15E:
C     *                            - CVDU->CVMC.
C     * DEC 15/2005 - M.LAZARE/    PREVIOUS VERSION INIT9D FOR GCM15D.
C     *               K.VONSALZEN. 
C======================================================================
C
C     * Initialize selected fields.
C
      TZEROC=273.16
C----------------------------------------------------------------------
C     * Reset FAREPAT if running with fractional glaciers
C     * and re-define ground properties to those of glaciers
C     * for this tile.
C     * Note that one just has to redefine the SAND for
C     * the first layer; there is code below to re-define
C     * the other layers, and TG/THLQ/THIC, based on this.
C ****************************************************************
C     * NOTE!!! For now, the code below will only work when
C     *         running with "general" and "land glacier" tiles,
C     *         ie NTLD=2. To generalize requires more thought
C     *         and possibly changes to initialization.
C*****************************************************************
C
C--------------------------------------------------------------
C     * DEFINE FRACTIONAL AREA ARRAYS.
C
C     * NOTE THAT THIS OVERRIDES WHAT IS CURRENTLY IN PROGRAM
C     * INITG14 AND THAT SHOULD BE REMOVED WHEN THIS WORKS
C     * (AND THIS COMMENT!). ALSO TO BE REMOVED ARE THE
C     * READING OF "FARE" AND "MASK" IN GETGG14.
C
C     * IMPORTANT!!!! THE USER MUST HAND-CODE CHANGES HERE
C     *               WHEN TILING CHANGES!!!
C
      IF(NTLD.GT.0) THEN
        DO N=1,NTLD
          FAREPAT(:,N)=FLNDPAK(:)
        ENDDO
      ENDIF
C
      IF(NTLK.GT.0) THEN
        DO NT=1,NTLK
          N=NTLD+NT
          IF(IKTNAM(NT).EQ.NC4TO8(" ULK"))      THEN
            FAREPAT(:,N)=FLKUPAK(:)
          ELSE IF(IKTNAM(NT).EQ.NC4TO8(" RWT"))      THEN
            FAREPAT(:,N)=(1.-LRINPAK(:))*FLKRPAK(:)
          ELSE IF(IKTNAM(NT).EQ.NC4TO8(" RIC")) THEN
            FAREPAT(:,N)=LRINPAK(:)*FLKRPAK(:)
          ELSE
            CALL XIT('INIT12',-4)
          ENDIF
        ENDDO
C
        LDMXPAK = 0.
        LZICPAK = 0.
#if defined (cslm)
        DELUPAK = 0.
        DTMPPAK = 0.
        EXPWPAK = 0.
        GREDPAK = 0.
        NLKLPAK = 0.
        RHOMPAK = 0.
        T0LKPAK = 0.
        TKELPAK = 0.
        DO L=1,NLKLM
          TLAKPAK(:,L) = 0.
        ENDDO
#endif
#if defined (flake)
        LSHPPAK = 0.
        LTAVPAK = 0.
        LTICPAK = 0.
        LTMXPAK = 0.
        LTSNPAK = 0.
        LTWBPAK = 0.
        LZSNPAK = 0.
#endif
        WHERE (FLKUPAK.NE.0.)
          LZICPAK = LUIMPAK/913.
#if defined (cslm)
          BLAKPAK=0.5   ! overrides zero in forcing file for now.
          HLAKPAK=MIN( MAX(HLAKPAK,3.0), 100.)
          LLAKPAK=MAX(LLAKPAK,100.)
          NLKLPAK=REAL(NINT(HLAKPAK/0.50))    ! DELZLK=0.5
          DELUPAK=0.
          T0LKPAK=MAX(GTPAK,277.16)
          TKELPAK=1.0E-12                ! TKEMIN=1.0E-12
          LDMXPAK(:)=0.5*NINT(NLKLPAK(:))-1
          DTMPPAK=0.
C
C         * INITIAL MIXED LAYER TEMP (CELSIUS), DENSITY, EXPANSIVITY,
C         * AND REDUCED GRAVITY
C
          RHOMPAK(:) = 999.975*
     1               (1.-8.2545E-6*((T0LKPAK(:)-TFREZ)-3.9816)*
     2               ((T0LKPAK(:)-TFREZ)-3.9816))
          EXPWPAK(:) = -1.0*(-2.*999.975*8.2545E-6*
     1               ((T0LKPAK(:)-TFREZ)-3.9816))/RHOMPAK(:)
          GREDPAK(:) = GRAV*ABS(RHOW-RHOMPAK(:))/RHOW
#endif
#if defined (flake)
          HLAKPAK = MIN(HLAKPAK,60.)
          LDMXPAK = MIN(0.25*HLAKPAK,10.)
          LSHPPAK = 0.5              ! the value used for C_T_min in Flake.
          LTAVPAK = 283.15
          LTICPAK = 273.15
          LTMXPAK = 283.15
          LTSNPAK = 273.15
          LTWBPAK = 283.15
#endif
        ENDWHERE
#if defined (cslm)
C
C       * ABORT CONDITIONS FOR CSLM.
C
        DO I=1,LONSL*ILAT
          IF(FLKUPAK(I).GT.0.) THEN
            IF(NLKLPAK(I).GT.REAL(NLKLM)) THEN
              PRINT *, '0Bad NLAK value: I,NLKLPAK(I),NLKLM = ',
     1                  I,NLKLPAK(I),NLKLM
              CALL XIT('GCM18',-12)
            ENDIF
C
            IF (GREDPAK(I).LE.0.)  THEN
              CALL XIT('GCM18',-14)
            ENDIF
C
            DO L=1,NLKLPAK(I)
              TLAKPAK(I,L)=MAX(GTPAK(I),277.16)
            ENDDO
          ENDIF
        ENDDO
#endif
      ENDIF ! NTLK.gt.0
C
      DO NT=1,NTWT
        N=NTLD+NTLK+NT
        IF(NTWT.GT.1) THEN
          IF(IWTNAM(NT).EQ.NC4TO8(" WAT"))      THEN
            FAREPAT(:,N)=(1.-SICNPAK(:))*
     1                   (1.-FLNDPAK(:)-FLKUPAK(:)-FLKRPAK(:))
          ELSE IF(IWTNAM(NT).EQ.NC4TO8(" SIC")) THEN
            FAREPAT(:,N)=SICNPAK(:)*
     1                   (1.-FLNDPAK(:)-FLKUPAK(:)-FLKRPAK(:))
          ELSE
            CALL XIT('INIT12',-1)
          ENDIF
        ELSE
          CALL XIT('INIT12',-2)
        ENDIF
      ENDDO
C--------------------------------------------------------------    
C     * THE FOLLOWING CODE CAN BE USED AS A TEMPLATE IN CASE
C     * RUNNING WITH GLACIERS AS A TILE. IT MIGHT REQUIRE
C     * FURTHER MODIFICATIONS.
C
C     IF(NTLD.GT.1) THEN
C       DO N=1,NTLD
C         IF(ILTNAM(N).EQ.NC4TO8(" GIC"))    THEN
C            FAREPAT(:,N)=GICNPAK*FLNDPAK
C            SANDPAT(:,N,:)=-4.
C            FCANPAT(:,N,:)=0.
C            SNOPAT (:,N)=MIN(SNOPAT(:,N),60.)
C         ELSE
C            IF(NTLD.EQ.2)                   THEN
C              FAREPAT(:,N)=(1.-GICNPAK)*FLNDPAK
C            ELSE
C              CALL XIT('INIT12',-1)
C            ENDIF
C         ENDIF
C       ENDDO
C     ENDIF
C-------------------------------------------------------------
C
      WHERE (SANDPAT(:,1,1).EQ.-4. .AND. SNOPAK.GT.60.) SNOPAK=60.
C     
C     * Zero out values of SNO below 1.E-3.
C
      WHERE (SNOPAK.GT.1.E-3)
        RHONPAK = 300.
        ANPAK = 0.84
        FNPAK = 1.
        REFPAK = 5.45256270447e-05  
        BCSNPAK= 0.
      ELSEWHERE
        SNOPAK = 0.
        RHONPAK = 0.
        ANPAK = 0.
        FNPAK = 0.
        REFPAK = 0.   
        BCSNPAK= 0.
      END WHERE
      EMISPAK=1.
C
      TNPAK = MIN(GTPAK,TZEROC)
C
      DO I=1,LONSL*ILAT
C
         IF(FLNDPAK(I).GT.0.) THEN
           IF(GTPAK(I).GT.TZEROC) THEN
             TTPAT(I,:)=1.
           ELSE
             TTPAT(I,:)=0.
           ENDIF
           TVPAT(I,:) = GTPAK(I)
         ELSE
           TTPAT(I,:)=2.
           TVPAT(I,:)=0.
         ENDIF
C
C        * Ensure total vegetation cover is less than/equal to 1.
C
         DO M=1,NTLD
           FVEG = 0.0
           DO L=1,ICANP1
              FVEG = FVEG+FCANPAT(I,M,L)
           ENDDO
           IF(FVEG.GT.1.0) THEN
              DO L=1,ICANP1
                 FCANPAT(I,M,L)=FCANPAT(I,M,L)/FVEG
              ENDDO
           ENDIF
         ENDDO
C
C        * Set liquid/frozen water contents for rock layers
C        * and glaciers and set TG for glaciers.
C
         DO M=1,NTLD
         DO L=1,IGND
           SANDX=SANDPAT(I,M,L)
           DEPTHX=ZBOT(L)-DELZ(L)+RLIM
           IF(SANDPAT(I,M,1).EQ.-4.) THEN
             SANDPAT(I,M,L)=-4.
             THLQPAT(I,M,L) = 0.
             THICPAT(I,M,L) = 1.
             TGPAT(I,M,L) = MIN(TGPAT(I,M,L),TZEROC)
           ELSEIF(SANDX.NE.-3. AND. DPTHPAT(I,M).LT.DEPTHX) THEN
             SANDPAT(I,M,L)=-3.
             THLQPAT(I,M,L)=0.
             THICPAT(I,M,L)=0.
           ENDIF
         ENDDO
         ENDDO
      ENDDO
C
C     * Other tiled fields which exist over ALL tiles.
C
      DO I=1,LONSL*ILAT
C
C       * GENERAL INITIALIZATION FOR ALL TILES TO START.
C
        DO M=1,IM
          EMISPAT(I,M)=0.
          CSALPAT(I,M,1)=0.
          SALBPAT(I,M,1)=0.
          CSALPAT(I,M,2)=0.
          SALBPAT(I,M,2)=0.
          CSALPAT(I,M,3)=0.
          SALBPAT(I,M,3)=0.
          CSALPAT(I,M,4)=0.
          SALBPAT(I,M,4)=0.
C
          GTPAT  (I,M) = GTPAK(I)
          SNOPAT (I,M) = 0.
          ANPAT  (I,M) = 0.
          FNPAT  (I,M) = 0.
          TNPAT  (I,M) = 0.
          RHONPAT(I,M) = 0.
          REFPAT (I,M) = 0.
          BCSNPAT(I,M) = 0.
        ENDDO
C
C       * LAND TILES.
C
        DO M=1,NTLD
          IF(FLNDPAK(I).GT.0.) THEN
C
C           * Set PAT fields from initialized read-in PAK fields (getgg13)
C           * and above.
C
            EMISPAT(I,M)=1.
            CSALPAT(I,M,1)=ALSWPAK(I)
            SALBPAT(I,M,1)=ALSWPAK(I)
            CSALPAT(I,M,2)=ALLWPAK(I)
            SALBPAT(I,M,2)=ALLWPAK(I)
            CSALPAT(I,M,3)=ALLWPAK(I)
            SALBPAT(I,M,3)=ALLWPAK(I)
            CSALPAT(I,M,4)=ALLWPAK(I)
            SALBPAT(I,M,4)=ALLWPAK(I)
C
            IF(SNOPAK(I).GT.0.) THEN
              SNOPAT (I,M) = SNOPAK (I)
              ANPAT  (I,M) = ANPAK  (I)
              FNPAT  (I,M) = FNPAK  (I)
              TNPAT  (I,M) = TNPAK  (I)
              RHONPAT(I,M) = RHONPAK(I)
              REFPAT (I,M) = REFPAK (I)
              BCSNPAT(I,M) = BCSNPAK(I)
            ELSE
              SNOPAT (I,M) = 0.
              ANPAT  (I,M) = 0.
              TNPAT  (I,M) = 0.
              RHONPAT(I,M) = 0.
              REFPAT (I,M) = 0.
              BCSNPAT(I,M) = 0.
            ENDIF
          ENDIF
        ENDDO
C
C       * UNRESOLVED LAKES.
C
        IF(FLKUPAK(I).GT.0.) THEN
          IF(LUIMPAK(I).GT.0.) THEN
            GTPAT  (I,IULAK)  =273.15
            EMISPAT(I,IULAK)  =1.
            CSALPAT(I,IULAK,1)=0.778
            SALBPAT(I,IULAK,1)=0.778
            CSALPAT(I,IULAK,2)=0.443
            SALBPAT(I,IULAK,2)=0.443
            CSALPAT(I,IULAK,3)=0.055
            SALBPAT(I,IULAK,3)=0.055
            CSALPAT(I,IULAK,4)=0.036
            SALBPAT(I,IULAK,4)=0.036
          ELSE
            GTPAT  (I,IULAK)  =GTPAK(I)
            EMISPAT(I,IULAK)  =0.97   ! EMSW
            CSALPAT(I,IULAK,1)=ALSWPAK(I)
            SALBPAT(I,IULAK,1)=ALSWPAK(I)
            CSALPAT(I,IULAK,2)=ALLWPAK(I)
            SALBPAT(I,IULAK,2)=ALLWPAK(I)
            CSALPAT(I,IULAK,3)=ALLWPAK(I)
            SALBPAT(I,IULAK,3)=ALLWPAK(I)
            CSALPAT(I,IULAK,4)=ALLWPAK(I)
            SALBPAT(I,IULAK,4)=ALLWPAK(I)
          ENDIF
        ENDIF
C
C       * RESOLVED LAKES (ASSUMED NO INITIAL SNOW AND ALL ICE-COVERED).
C
        IF(FLKRPAK(I).GT.0.) THEN
          IF(LRINPAK(I).GT.0.) THEN
            GTPAT  (I,IRLIC)  =273.15
            EMISPAT(I,IRLIC)  =1.
            CSALPAT(I,IRLIC,1)=0.778
            SALBPAT(I,IRLIC,1)=0.778
            CSALPAT(I,IRLIC,2)=0.443
            SALBPAT(I,IRLIC,2)=0.443
            CSALPAT(I,IRLIC,3)=0.055
            SALBPAT(I,IRLIC,3)=0.055
            CSALPAT(I,IRLIC,4)=0.036
            SALBPAT(I,IRLIC,4)=0.036
          ENDIF
          IF(LRINPAK(I).LT.1.) THEN
            GTPAT  (I,IRLWT)  =GTPAK(I)
            EMISPAT(I,IRLWT)  =0.97   ! EMSW
            CSALPAT(I,IRLWT,1)=ALSWPAK(I)
            SALBPAT(I,IRLWT,1)=ALSWPAK(I)
            CSALPAT(I,IRLWT,2)=ALLWPAK(I)
            SALBPAT(I,IRLWT,2)=ALLWPAK(I)
            CSALPAT(I,IRLWT,3)=ALLWPAK(I)
            SALBPAT(I,IRLWT,3)=ALLWPAK(I)
            CSALPAT(I,IRLWT,4)=ALLWPAK(I)
            SALBPAT(I,IRLWT,4)=ALLWPAK(I)
          ENDIF
        ENDIF
C
C       * OCEAN TILES (ASSUME NO SNOW AND ICE-COVERED).
C
        FWAT=1.-FLNDPAK(I)-FLKUPAK(I)-FLKRPAK(I)
        IF(FWAT.GT.0.) THEN
          IF(SICNPAK(I).GT.0.) THEN
            GTPAT  (I,IOSIC)  =273.15
            EMISPAT(I,IOSIC)  =1.
            CSALPAT(I,IOSIC,1)=0.778
            SALBPAT(I,IOSIC,1)=0.778
            CSALPAT(I,IOSIC,2)=0.443
            SALBPAT(I,IOSIC,2)=0.443
            CSALPAT(I,IOSIC,3)=0.055
            SALBPAT(I,IOSIC,3)=0.055
            CSALPAT(I,IOSIC,4)=0.036
            SALBPAT(I,IOSIC,4)=0.036
          ENDIF
          IF(SICNPAK(I).LT.1.) THEN
            GTPAT  (I,IOWAT)  =GTPAK(I)
            EMISPAT(I,IOWAT)  =0.97   ! EMSW
            CSALPAT(I,IOWAT,1)=ALSWPAK(I)
            SALBPAT(I,IOWAT,1)=ALSWPAK(I)
            CSALPAT(I,IOWAT,2)=ALLWPAK(I)
            SALBPAT(I,IOWAT,2)=ALLWPAK(I)
            CSALPAT(I,IOWAT,3)=ALLWPAK(I)
            SALBPAT(I,IOWAT,3)=ALLWPAK(I)
            CSALPAT(I,IOWAT,4)=ALLWPAK(I)
            SALBPAT(I,IOWAT,4)=ALLWPAK(I)
          ENDIF
        ENDIF
      ENDDO
C
C     * Set TT for glaciers.
C
      WHERE(SANDPAT(:,:,1).EQ.-4.) TTPAT=-2.
C
C     * Initialize the bedrock temperature, the temperature and humidity
C     * of the air in the canopy space, and the vegetation canopy mass.
C
      TBASPAT = TGPAT(:,:,IGND)
      TAVPAT = TVPAT
      QAVPAT = 0.5E-4
      MVPAT = 0.
C
C     * Initialize the temperature of the ground/snow surface
C     * for each of the 4 subareas.
C
      TSFSPAT(:,:,1:2) = TZEROC
      TSFSPAT(:,:,3) = TGPAT(:,:,1)
      TSFSPAT(:,:,4) = TGPAT(:,:,1)
C
      PBLTPAK = REAL(ILEV)
      TCVPAK  = REAL(ILEV)
      GTAPAK  = GTPAK
      STMXPAK = GTPAK
      STMNPAK = GTPAK
C----------------------------------------------------------------------
C
C     * INITIALIZE REMAINING PACKED ARRAYS TO ZERO.
C
      CALL PKZEROS2(  CLBPAT,IJPAK,  IM)
      CALL PKZEROS2(  CSBPAT,IJPAK,  IM)
      CALL PKZEROS2(  CSDPAT,IJPAK,  IM)
      CALL PKZEROS2(  CSFPAT,IJPAK,  IM)
      CALL PKZEROS2(  FDLPAT,IJPAK,  IM)
      CALL PKZEROS2( FDLCPAT,IJPAK,  IM)
      CALL PKZEROS2(  FLGPAT,IJPAK,  IM)
      CALL PKZEROS2(  FSDPAT,IJPAK,  IM)
      CALL PKZEROS2(  FSFPAT,IJPAK,  IM)
      CALL PKZEROS2(  FSGPAT,IJPAK,  IM)
      CALL PKZEROS2(  FSIPAT,IJPAK,  IM)
      CALL PKZEROS2(  FSVPAT,IJPAK,  IM)
      CALL PKZEROS2(  PARPAT,IJPAK,  IM)
      CALL PKZEROS2( FSDBPAT,IJPAK,NTBS)
      CALL PKZEROS2( FSFBPAT,IJPAK,NTBS)
      CALL PKZEROS2( FSSBPAT,IJPAK,NTBS)
      CALL PKZEROS2( CSDBPAT,IJPAK,NTBS)
      CALL PKZEROS2( CSFBPAT,IJPAK,NTBS)
      CALL PKZEROS2(FSSCBPAT,IJPAK,NTBS)
C
      CALL PKZEROS2(  FSOPAL,IJPAK,   1)
      CALL PKZEROS2( CSALPAL,IJPAK, NBS)
      CALL PKZEROS2( SALBPAL,IJPAK, NBS)
      CALL PKZEROS2(  CSDPAL,IJPAK,   1)
      CALL PKZEROS2(  CSFPAL,IJPAK,   1)
      CALL PKZEROS2( FSDBPAL,IJPAK, NBS)
      CALL PKZEROS2( FSFBPAL,IJPAK, NBS)
      CALL PKZEROS2( FSSBPAL,IJPAK, NBS)
      CALL PKZEROS2( CSDBPAL,IJPAK, NBS)
      CALL PKZEROS2( CSFBPAL,IJPAK, NBS)
      CALL PKZEROS2(FSSCBPAL,IJPAK, NBS)
      CALL PKZEROS2( WRKAPAL,IJPAK, NBS)
      CALL PKZEROS2( WRKBPAL,IJPAK, NBS)
      CALL PKZEROS2(  FSGPAL,IJPAK,   1)
      CALL PKZEROS2(  FSDPAL,IJPAK,   1)
      CALL PKZEROS2(  FSFPAL,IJPAK,   1)
      CALL PKZEROS2(  FSVPAL,IJPAK,   1)
      CALL PKZEROS2(  FSIPAL,IJPAK,   1)
      CALL PKZEROS2(  FSRPAL,IJPAK,   1)
      CALL PKZEROS2( FSRCPAL,IJPAK,   1)
      CALL PKZEROS2(  FDLPAL,IJPAK,   1)
      CALL PKZEROS2( FDLCPAL,IJPAK,   1)
      CALL PKZEROS2(  FLGPAL,IJPAK,   1)
      CALL PKZEROS2( FSLOPAL,IJPAK,   1)
      CALL PKZEROS2(  PARPAL,IJPAK,   1)
      CALL PKZEROS2(  OLRPAL,IJPAK,   1)
      CALL PKZEROS2( OLRCPAL,IJPAK,   1)
      CALL PKZEROS2(  CSBPAL,IJPAK,   1)
      CALL PKZEROS2(  CLBPAL,IJPAK,   1)
      CALL PKZEROS2( FLAMPAL,IJPAK,   1)
      CALL PKZEROS2( FSAMPAL,IJPAK,   1)
      CALL PKZEROS2( FLANPAL,IJPAK,   1)
      CALL PKZEROS2( FSANPAL,IJPAK,   1)
C
      CALL PKZEROS2( PBLHPAK,IJPAK,   1)   
      CALL PKZEROS2( CLWTPAL,IJPAK,   1)
      CALL PKZEROS2( CICTPAL,IJPAK,   1)
      CALL PKZEROS2(  WVLPAK,IJPAK,   1)
      CALL PKZEROS2(  WVFPAK,IJPAK,   1)
C
      CALL PKZEROS2( PWATPAM,IJPAK,   1)
      CALL PKZEROS2( QTPHPAM,IJPAK,   1)
      CALL PKZEROS2( QTPFPAM,IJPAK,   1)
      CALL PKZEROS2( QTPTPAM,IJPAK,   1)
C
      CALL PKZEROS2( CLDTPAL,IJPAK,   1)
      CALL PKZEROS2( CLDOPAL,IJPAK,   1)
C
      CALL PKZEROS2(  TFXPAK,IJPAK,   1)
      CALL PKZEROS2(  QFXPAK,IJPAK,   1)
      CALL PKZEROS2( CHFXPAK,IJPAK,   1)
      CALL PKZEROS2( CQFXPAK,IJPAK,   1)
      CALL PKZEROS2( CBMFPAL,IJPAK,   1)
C
      CALL PKZEROS2(  HRSPAK,IJPAK,ILEV)
      CALL PKZEROS2(  HRLPAK,IJPAK,ILEV)
      CALL PKZEROS2(  CLDPAK,IJPAK,ILEV)
      CALL PKZEROS2( TACNPAK,IJPAK,ILEV)
      CALL PKZEROS2(   RHPAK,IJPAK,ILEV)
      CALL PKZEROS2(  CLWPAK,IJPAK,ILEV)
      CALL PKZEROS2(  CICPAK,IJPAK,ILEV)
      CALL PKZEROS2( CVARPAK,IJPAK,ILEV)
      CALL PKZEROS2( CVMCPAK,IJPAK,ILEV)
      CALL PKZEROS2( CVSGPAK,IJPAK,ILEV)
      CALL PKZEROS2( ALMXPAK,IJPAK,ILEV)
      CALL PKZEROS2( ALMCPAK,IJPAK,ILEV)
C
C     * TKE.
C
      TKEMPAK=0.
      XLMPAK=0.
      SVARPAK=0.
      XLMPAT=0.
C
C     * OTHERS.
C  
      WSNOPAK=0.
      WTABPAK=0.
      ZPNDPAK=0.
      DEPBPAK=0.                     
C
      TPNDPAT=0.
      WSNOPAT=0.
      WVLPAT=0.
      WVFPAT=0.
      ZPNDPAT=0.

      CALL PKZEROS2(SCLFPAK,IJPAK,ILEV)
      CALL PKZEROS2(SCDNPAK,IJPAK,ILEV)
      CALL PKZEROS2(SLWCPAK,IJPAK,ILEV)
      CALL PKZEROS2(OMETPAK,IJPAK,ILEV)
      CALL PKZEROS2(ZDETPAK,IJPAK,ILEV)
      CALL PKZEROS2(CLCVPAK,IJPAK,ILEV)
      CALL PKZEROS2(QWF0PAL,IJPAK,ILEV)
      CALL PKZEROS2(QWFMPAL,IJPAK,ILEV)

#if defined (agcm_ctem)
C
C     * INITIALIZE CTEM RELATED VARIABLES
C
      FSNOWPTL  = 0.
      TCANOPTL  = 0.
      TCANSPTL  = 0.
      TAPTL     = 0.
      TBARPTL   = 0.
      TBARCPTL  = 0.
      TBARCSPTL = 0.
      TBARGPTL  = 0.
      TBARGSPTL = 0.
      THLIQCPTL = 0.
      THLIQGPTL = 0.
      THICECPTL = 0.
      ANCSPTL   = 0.
      ANCGPTL   = 0.
      RMLCSPTL  = 0.
      RMLCGPTL  = 0.
C
C     * TIME VARYING.
C
      SOILCPAT  = 0.
      LITRCPAT  = 0.
      ROOTCPAT  = 0.
      STEMCPAT  = 0.
      GLEAFCPAT = 0.
      BLEAFCPAT = 0.
      FALLHPAT  = 0.
      POSPHPAT  = 0.
      LEAFSPAT  = 0.
      GROWTPAT  = 0.
      LASTRPAT  = 0.
      LASTSPAT  = 0.
      THISYLPAT = 0.
      STEMHPAT  = 0.
      ROOTHPAT  = 0.
      TEMPCPAT  = 0.
      AILCBPAT  = 0.
      BMASVPAT  = 0.
      VEGHPAT   = 0.
      ROOTDPAT  = 0.
C
      PREFPAT   = 0.
      NEWFPAT   = 0.
C
      CVEGPAT   = 0.
      CDEBPAT   = 0.
      CHUMPAT   = 0.
#endif

      SFRCPAL = 1.
      QSRCRAT = 1.
      QSRCRM1 = 1. 
      QSRCM   = 1. 
      QSRC0   = 1. 
      QSCL0   = 1. 
      QSCLM   = 1. 
      DO L=1,ILEV
         QSRCRATL(L) = 0.
      ENDDO
C
      DO N=1,NTRAC
        XSRCRAT(N)=1.
        XSRCRM1(N)=1.
        XSRC0  (N)=1.
        XSRCM  (N)=1.
        XSCL0  (N)=1.
        XSCLM  (N)=1.
        DO L=1,ILEV
           XSRCRATL(L,N) = 0.
        ENDDO
C
        CALL PKZEROS2(XSFXPAK(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XSFXPAL(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XSRFPAL(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XTPFPAM(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XTPHPAM(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XTVIPAM(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XTPTPAM(1,N)  ,IJPAK,   1)
        CALL PKZEROS2(XWF0PAL(1,1,N),IJPAK,ILEV)
        CALL PKZEROS2(XWFMPAL(1,1,N),IJPAK,ILEV)
C
      ENDDO
#if (defined(pla) && defined(pam))
      CALL PKZEROS2(SSLDPAK,IJPAK,ILEV)
      CALL PKZEROS2(RESSPAK,IJPAK,ILEV)
      CALL PKZEROS2(VESSPAK,IJPAK,ILEV)
      CALL PKZEROS2(DSLDPAK,IJPAK,ILEV)
      CALL PKZEROS2(REDSPAK,IJPAK,ILEV)
      CALL PKZEROS2(VEDSPAK,IJPAK,ILEV)
      CALL PKZEROS2(BCLDPAK,IJPAK,ILEV)
      CALL PKZEROS2(REBCPAK,IJPAK,ILEV)
      CALL PKZEROS2(VEBCPAK,IJPAK,ILEV)
      CALL PKZEROS2(OCLDPAK,IJPAK,ILEV)
      CALL PKZEROS2(REOCPAK,IJPAK,ILEV)
      CALL PKZEROS2(VEOCPAK,IJPAK,ILEV)
      CALL PKZEROS2(AMLDPAK,IJPAK,ILEV)
      CALL PKZEROS2(REAMPAK,IJPAK,ILEV)
      CALL PKZEROS2(VEAMPAK,IJPAK,ILEV)
      CALL PKZEROS2(FR1PAK ,IJPAK,ILEV)
      CALL PKZEROS2(FR2PAK ,IJPAK,ILEV)
      CALL PKZEROS2(ZCDNPAK,IJPAK,ILEV)
      CALL PKZEROS2(BCICPAK,IJPAK,ILEV)
      DO ISF=1,KEXTT
        OEDNPAK(:,:,ISF)=YNA
        OERCPAK(:,:,ISF)=YNA
      ENDDO
      OIDNPAK=YNA
      OIRCPAK=YNA
      CALL PKZEROS2(SVVBPAK,IJPAK,ILEV)
      PSVVPAK=YNA 
      CALL PKZEROS2(QGVBPAK,IJPAK,ILEV)
      CALL PKZEROS2(QDVBPAK,IJPAK,ILEV)
      CALL PKZEROS2(OGVBPAK,IJPAK,ILEV)
      DO ISI=1,ISAINTT
        CALL PKZEROS2(PNVBPAK(1,1,ISI),IJPAK,ILEV)
        CALL PKZEROS2(QNVBPAK(1,1,ISI),IJPAK,ILEV)
        CALL PKZEROS2(ONVBPAK(1,1,ISI),IJPAK,ILEV)
        DO K=1,KINTT
          CALL PKZEROS2(PSVBPAK(1,1,ISI,K),IJPAK,ILEV)
          CALL PKZEROS2(QSVBPAK(1,1,ISI,K),IJPAK,ILEV)
          CALL PKZEROS2(OSVBPAK(1,1,ISI,K),IJPAK,ILEV)
        ENDDO
      ENDDO
      DO ISF=1,NRMFLD
        CALL PKZEROS2(SVMBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(QGMBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(QDMBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(OGMBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(SVCBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(QGCBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(QDCBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(OGCBPAK(1,1,ISF),IJPAK,ILEV)
        DO ISI=1,ISAINTT
          CALL PKZEROS2(PNMBPAK(1,1,ISI,ISF),IJPAK,ILEV)
          CALL PKZEROS2(QNMBPAK(1,1,ISI,ISF),IJPAK,ILEV)
          CALL PKZEROS2(ONMBPAK(1,1,ISI,ISF),IJPAK,ILEV)
          CALL PKZEROS2(PNCBPAK(1,1,ISI,ISF),IJPAK,ILEV)
          CALL PKZEROS2(QNCBPAK(1,1,ISI,ISF),IJPAK,ILEV)
          CALL PKZEROS2(ONCBPAK(1,1,ISI,ISF),IJPAK,ILEV)
          DO K=1,KINTT
            CALL PKZEROS2(PSMBPAK(1,1,ISI,K,ISF),IJPAK,ILEV)
            CALL PKZEROS2(QSMBPAK(1,1,ISI,K,ISF),IJPAK,ILEV)
            CALL PKZEROS2(OSMBPAK(1,1,ISI,K,ISF),IJPAK,ILEV)
            CALL PKZEROS2(PSCBPAK(1,1,ISI,K,ISF),IJPAK,ILEV)
            CALL PKZEROS2(QSCBPAK(1,1,ISI,K,ISF),IJPAK,ILEV)
            CALL PKZEROS2(OSCBPAK(1,1,ISI,K,ISF),IJPAK,ILEV)
          ENDDO
        ENDDO
      ENDDO
      DO ISF=1,KEXTT
        CALL PKZEROS2(DEVBPAK(1,1,ISF),IJPAK,ILEV)
        PDEVPAK(:,:,ISF)=YNA
        DO ISG=1,NRMFLD
          CALL PKZEROS2(DEMBPAK(1,1,ISF,ISG),IJPAK,ILEV)
          CALL PKZEROS2(DECBPAK(1,1,ISF,ISG),IJPAK,ILEV)
        ENDDO
      ENDDO
      CALL PKZEROS2(DIVBPAK,IJPAK,ILEV)
      PDIVPAK=YNA
      DO ISF=1,NRMFLD
        CALL PKZEROS2(DIMBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(DICBPAK(1,1,ISF),IJPAK,ILEV)
      ENDDO
      DO ISF=1,KEXTT
        REVBPAK(:,:,ISF)=YNA
        PREVPAK(:,:,ISF)=YNA
        DO ISG=1,NRMFLD
          CALL PKZEROS2(REMBPAK(1,1,ISF,ISG),IJPAK,ILEV)
          CALL PKZEROS2(RECBPAK(1,1,ISF,ISG),IJPAK,ILEV)
        ENDDO
      ENDDO
      RIVBPAK=YNA
      PRIVPAK=YNA
      DO ISF=1,NRMFLD
        CALL PKZEROS2(RIMBPAK(1,1,ISF),IJPAK,ILEV)
        CALL PKZEROS2(RICBPAK(1,1,ISF),IJPAK,ILEV)
      ENDDO
#if defined radforce
      CALL PKZEROS2(SULIPAK,IJPAK,ILEV)
      CALL PKZEROS2(RSUIPAK,IJPAK,ILEV)
      CALL PKZEROS2(VSUIPAK,IJPAK,ILEV)
      CALL PKZEROS2(F1SUPAK,IJPAK,ILEV)
      CALL PKZEROS2(F2SUPAK,IJPAK,ILEV)
      CALL PKZEROS2(BCLIPAK,IJPAK,ILEV)
      CALL PKZEROS2(RBCIPAK,IJPAK,ILEV)
      CALL PKZEROS2(VBCIPAK,IJPAK,ILEV)
      CALL PKZEROS2(F1BCPAK,IJPAK,ILEV)
      CALL PKZEROS2(F2BCPAK,IJPAK,ILEV)
      CALL PKZEROS2(OCLIPAK,IJPAK,ILEV)
      CALL PKZEROS2(ROCIPAK,IJPAK,ILEV)
      CALL PKZEROS2(VOCIPAK,IJPAK,ILEV)
      CALL PKZEROS2(F1OCPAK,IJPAK,ILEV)
      CALL PKZEROS2(F2OCPAK,IJPAK,ILEV)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
