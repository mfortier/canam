#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * Mar 23/2019 - J. Cole     - Save site data for CFMIP/CMIP6
C     * Nov 02/2018 - J. Cole     - Add radiative flux profiles
C     * Nov 20/2018 - M.Lazare.   - Ensure ECO2PAK and ECO2PAL are defined for all cases.
C     * Nov 07/2018 - M.Lazare.   - Added GSNO and FNLA.
C     *                           - Added 3HR save fields.
C     *                           - Now save all tiled fields, SIC and SICN
C     *                             at (LSGG.OR.LSBEG) frequency.
C     * Aug 14/2018 - M.Lazare.   - Remove GTPAL.
C     * Aug 14/2018 - M.Lazare.   - Remove {GTM,SICP,SICM,SICNP,SICM}.
C     * Jul 30/2018 - M.Lazare.   - Unused {FMI,GC} removed.
C     * Mar 09/2018 - M.Lazare.   - BEG changed from PAK to PAL
C     *                             and BWG removed.
C     * Mar 04/2018 - M.Lazare.   Remove unused IOCEAN=0 section.
C     * Feb 27/2018 - M.Lazare.   - QFSL and BEGL changed from PAK/ROW to
C     *                             PAL/ROL.
C     *                           - Added {BEGK,BWGK,BWGL,QFSO}.
C     * Apr 19/2018 - M.Lazare.    - Add FCOO and ECO2.
C     * Feb 06/2018 - M.Lazare.    - Add {FTOX,FTOY} and {TDOX,TDOY}.
C     * Aug 10/2017 - M.Lazare.    Add lakes saving.
C     * Aug 07/2017 - M.Lazare.    Initial Git verison.
C     * Aug 10/2017 - M.Lazare.    - Add saving of {FNPAT,TNPAT,RHONPAT,BCSNPAT,REFPAT}
C     *                              under cpp control of saving tiled
C     *                              fields.
C     *                            - {PHLPAK,PHSPAK,PHDPAK) are not
C     *                              accumulated for now, so zeroing
C     *                              removed.
C     *                            - Add lakes fields.
C     * Aug 07/2017 - M.Lazare.    - Initial Git version.
C     * Mar 14/2015 - M.Lazare/    New version for gcm18:
C     *               K.Vonsalzen/ - Add (for NEMO/CICE support):
C     *               J.Cole.        HFLIPAL,BEGIPAL,BEGOPAL,BWGOPAL,
C     *                              HSEAPAL,OBEGPAL,OBWGPAL,RAINSPAL,SNOWSPAL.
C     *                            - Bugfixes for ISCCP fields and additions
C     *                              for CALIPSO and MODIS.
C     * Jul 10/2013 - M.Lazare/    PREVIOUS VERSION SAVEACC5 FOR GCM17:
C     *               K.Vonsalzen/
C     *               J.Cole.
C     *                            - Extra diagnostic microphysics
C     *                              fields added:
C     *                              {SEDI,RLIQ,RICE,RLNC,CLIQ,CICE,
C     *                               VSEDI,RELIQ,REICE,CLDLIQ,CLDICE,CTLNC,CLLNC}.
C     *                            - Added: GFLX,GA,HBL,PET,ILMO,ROFB,ROFS,UE,WTAB.
C     *                            - Many new aerosol diagnostic fields
C     *                              for PAM, including those for cpp options:
C     *                              "pfrc", "xtrapla1" and "xtrapla2".
C     *                            - The following fields were removed
C     *                              from the "aodpth" cpp option:
C     *                              {SAB1,SAB2,SAB3,SAB4,SAB5,SABT} and
C     *                              {SAS1,SAS2,SAS3,SAS4,SAS5,SAST},
C     *                              (both PAK and PAL).
C     * MAY 07/2012 - M.LAZARE/    PREVIOUS VERSION SAVEACC4 FOR GCM16:
C     *               K.VONSALZEN/ - MODIFY FIELDS TO SUPPORT A NEWER
C     *               J.COLE/        VERSION OF COSP WHICH INCLUDES
C     *               Y.PENG.        THE MODIS, MISR AND CERES AND
C     *                              SAVE THE APPROPRIATE OUTPUT.
C     *                            - REMOVE {FSA,FLA,FSTC,FLTC} AND
C     *                              REPLACE BY {FSR,OLR,FSRC,OLRC}.
C     *                            - NEW CONDITIONAL DIAGNOSTIC OUTPUT
C     *                              UNDER CONTROL OF "XTRADUST" AND
C     *                              "AODPTH".
C     *                            - ADDITIONAL MAM RADIATION OUTPUT
C     *                              FIELDS {FSAN,FLAN}.
C     *                            - ADDITIONAL CORRELATED-K OUTPUT
C     *                              FIELDS: "FLG", "FDLC" AND "FLAM".
C     * MAY 04/2010 - M.LAZARE.    PREVIOUS VERSION SAVEACC3I FOR GCM15I:
C     *                            - ADD/REMOVE FIELDS (SEE OTHER
C     *                              COMMON DECKS FOR DETAILS).
C     *                            - UPDATE DIRECTIVES TURNED INTO
C     *                              CPP DIRECTIVES.
C     * FEB 20/2009 - K.VONSALZEN/ PREVIOUS VERSION SAVEACC3H FOR GCM15H:
C     *               M.LAZARE.    - ADD NEW FIELDS FOR EMISSIONS: EOFF,
C     *                              EBFF,EOBB,EBBB.
C     *                            - ADD NEW FIELDS FOR DIAGNOSTICS OF
C     *                              CONSERVATION: QTPT,XTPT.
C     *                            - ADD NEW FIELD FOR CHEMISTRY: SFRC.
C     *                            - ADD NEW DIAGNOSTIC CLOUD FIELD
C     *                              (USING OPTICAL DEPTH CUTOFF): CLDO.
C     * APR 21/2008 - L.SOLHEIM/   PREVIOUS VERSION SAVEACC3G FOR GCM15G:
C     *               M.LAZARE/    - ADD NEW RADIATIVE FORCING ARRAYS
C     *               X.MA/          (UNDER CONTROL OF "%DF RADFORCE").
C     *               K.VONSALZEN. - NEW DIAGNOSTIC FIELDS: WDD4,WDS4,EDSL,
C     *                              ESBF,ESFF,ESVC,ESVE,ESWF ALONG WITH
C     *                              TDEM->EDSO (UNDER CONTROL OF
C     *                              "XTRACHEM"), AS WELL AS ALMX,ALMC
C     *                              AND INSTANTANEOUS CLWT,CIDT.
C     *                            - REMOVE UNUSED: QTPN,UTPM,VTPM,TSEM.
C     * JAN 11/2006 - J.COLE/      PREVIOUS VERSION SAVEACC3F FOR GCM15F:
C     *               M.LAZARE.    ADD ISCCP SIMULATOR FIELDS FROM JASON.
C     * NOV 26/2006 - M.LAZARE.    TWO EXTRA NEW FIELDS ("DMC" AND "SMC")
C     *                            UNDER CONTROL OF "%IF DEF,XTRACONV".
C     * SEP 11/2006 - M.LAZARE.    - CALLS "NAME2" INSTEAD OF "NAME".
C     *                            - SAVED NAME OF CSCBPAK NOW CONSISTENT
C     *                              (IE "CSCB") INSTEAD OF "SDCB".
C     * DEC 15/2005 - M.LAZARE/    PREVIOUS VERSION FOR GCM15D/E:
C     *               K.VONSALZEN. - ADD MONTHLY ACCUMULATION FIELDS (IE
C     *                              PAM/ROM) FOR PWAT,QTPH,QTPF,XTVI,XTPH,
C     *                              XTPF, FOR NEW MOISTURE/TRACER
C     *                              CONSERVATION DIAGNOSTICS.
C     *                            - BUGFIXES FOR XADDPAK AND QTPHPAK SAVINGS.
C
#if defined isavdts
C     * IF AT A NEW DAY BOUNDARY (NSECS=0), REDEFINE "IMDH" TO BE
C     * IN FORMAT "MMDD24"(WHERE "DD" IS THE PREVIOUS DAY), USING
C     * INFORMATION FROM PREVIOUS STEPS' "IMDH" (STORED IN "IMDHO"
C     * DEFINED JUST BEFORE CALL TO GCMTYM2). THIS WILL BE APPLIED
C     * IN THE ENSUING ROUTINES THROUGH THE "KEEPTIM" COMMON BLOCK
C     * AND "IMDH" WILL BE RESET BACK AT THE END OF THIS COMMON DECK.
C
      IF(NSECS.EQ.0)                                  THEN
         IMDHN=IMDH
         IMDO=IMDHO/100
         IMDH=IMDO*100 + 24
      ENDIF
#endif
#if defined (agcm_ctem)
C
C     * DEFINE TIME LABEL FOR CTEM RESTART AND OUTPUT FILES.
C     * SAVE CTEM OUTPUT.
C
      IYMD = IYEARO*10000+IMDHO/100
      IF (LSDAY)                                                 THEN
#include "save_ctem.h"
      ENDIF
#endif

C     * SAVE SCREEN LEVEL FIELDS ON FILE NUPR EVERY ISDAY STEPS.

      IF (LSDAY)                                                 THEN
          K=KOUNT
          CALL PUTGG(SRHNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SRHN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(SRHXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SRHX")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(STMXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("STMX")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(STMNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("STMN")
     1                        ,1,GLL,WRKS)
          CALL PUTGG( SWXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" SWX")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(SWXUPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SWXU")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(SWXVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SWXV")
     1                       , 1,GLL,WRKS)
      ENDIF

C     * SAVE SELECTED FIELDS ON FILE NU3HR EVERY IS3HR (3-HOUR) STEPS.

      IF (LS3HR) THEN
         K=KOUNT
         CALL PUTGG(CLDT3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CLDT"),1,GLL,WRKS)
         CALL PUTGG( HFL3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" HFL"),1,GLL,WRKS)
         CALL PUTGG( HFS3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" HFS"),1,GLL,WRKS)
         CALL PUTGG( ROF3HPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" ROF"),1,GLL,WRKS)
         CALL PUTGG( WGL3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" WGL"),1,GLL,WRKS)
         CALL PUTGG( WGF3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" WGF"),1,GLL,WRKS)
         CALL PUTGG( PCP3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" PCP"),1,GLL,WRKS)
         CALL PUTGG(PCPC3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCPC"),1,GLL,WRKS)
         CALL PUTGG(PCPL3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCPL"),1,GLL,WRKS)
         CALL PUTGG(PCPS3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCPS"),1,GLL,WRKS)
         CALL PUTGG(PCPN3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCPN"),1,GLL,WRKS)
         CALL PUTGG( FDL3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FDL"),1,GLL,WRKS)
         CALL PUTGG(FDLC3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FDLC"),1,GLL,WRKS)
         CALL PUTGG( FLG3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FLG"),1,GLL,WRKS)
         CALL PUTGG( FSS3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FSS"),1,GLL,WRKS)
         CALL PUTGG(FSSC3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSSC"),1,GLL,WRKS)
         CALL PUTGG( FSD3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FSD"),1,GLL,WRKS)
         CALL PUTGG( FSG3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FSG"),1,GLL,WRKS)
         CALL PUTGG(FSGC3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSGC"),1,GLL,WRKS)
         CALL PUTGG(  SQ3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("  SQ"),1,GLL,WRKS)
         CALL PUTGG(  ST3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" STA"),1,GLL,WRKS)
         CALL PUTGG(  SU3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" SUA"),1,GLL,WRKS)
         CALL PUTGG(  SV3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" SVA"),1,GLL,WRKS)
         CALL PUTGG( OLR3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" OLR"),1,GLL,WRKS)
         CALL PUTGG( FSR3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FSR"),1,GLL,WRKS)
         CALL PUTGG(OLRC3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("OLRC"),1,GLL,WRKS)
         CALL PUTGG(FSRC3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSRC"),1,GLL,WRKS)
         CALL PUTGG(FSO3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" FSO"),1,GLL,WRKS)
         CALL PUTGG(PWAT3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PWAT"),1,GLL,WRKS)
         CALL PUTGG(CLWT3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CLWT"),1,GLL,WRKS)
         CALL PUTGG(CICT3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CICT"),1,GLL,WRKS)
         CALL PUTGG(PMSL3HPAK,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PMSL"),1,GLL,WRKS)

         DO M=1,IM
           CALL PUTGG(GTPAT  (1,M),LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                  NC4TO8(" GTT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(FAREPAT(1,M),LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                  NC4TO8("FARE"),M,GLL,WRKS)
         ENDDO
C
         CALL PKZEROS2(CLDT3HPAK,IJPAK,   1)
         CALL PKZEROS2( HFL3HPAK,IJPAK,   1)
         CALL PKZEROS2( HFS3HPAK,IJPAK,   1)
         CALL PKZEROS2( ROF3HPAL,IJPAK,   1)
         CALL PKZEROS2( PCP3HPAK,IJPAK,   1)
         CALL PKZEROS2(PCPC3HPAK,IJPAK,   1)
         CALL PKZEROS2(PCPL3HPAK,IJPAK,   1)
         CALL PKZEROS2(PCPS3HPAK,IJPAK,   1)
         CALL PKZEROS2(PCPN3HPAK,IJPAK,   1)
         CALL PKZEROS2( FDL3HPAK,IJPAK,   1)
         CALL PKZEROS2(FDLC3HPAK,IJPAK,   1)
         CALL PKZEROS2( FLG3HPAK,IJPAK,   1)
         CALL PKZEROS2( FSS3HPAK,IJPAK,   1)
         CALL PKZEROS2(FSSC3HPAK,IJPAK,   1)
         CALL PKZEROS2( FSD3HPAK,IJPAK,   1)
         CALL PKZEROS2( FSG3HPAK,IJPAK,   1)
         CALL PKZEROS2(FSGC3HPAK,IJPAK,   1)

         CALL PKZEROS2(OLR3HPAK,IJPAK,    1)
         CALL PKZEROS2(FSR3HPAK,IJPAK,    1)
         CALL PKZEROS2(OLRC3HPAK,IJPAK,   1)
         CALL PKZEROS2(FSRC3HPAK,IJPAK,   1)
         CALL PKZEROS2(FSO3HPAK,IJPAK,    1)
         CALL PKZEROS2(PWAT3HPAK,IJPAK,   1)
         CALL PKZEROS2(CLWT3HPAK,IJPAK,   1)
         CALL PKZEROS2(CICT3HPAK,IJPAK,   1)
         CALL PKZEROS2(PMSL3HPAK,IJPAK,   1)
         CALL PKZEROS2(ST3HPAK,IJPAK,     1)
         CALL PKZEROS2(SU3HPAK,IJPAK,     1)
         CALL PKZEROS2(SV3HPAK,IJPAK,     1)

C
C Sampled fields
C
         CALL PUTGG(STPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("  ST"),1,GLL,WRKS)
         CALL PUTGG(SUPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("  SU"),1,GLL,WRKS)
         CALL PUTGG(SVPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("  SV"),1,GLL,WRKS)
         CALL PUTGG(SWPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" SWI"),1,GLL,WRKS)
         CALL PUTGG(SRHPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("SRHI"),1,GLL,WRKS)
         CALL PUTGG(PCPPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCPI"),1,GLL,WRKS)
         CALL PUTGG(PCPNPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCNI"),1,GLL,WRKS)
         CALL PUTGG(PCPCPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PCCI"),1,GLL,WRKS)
         CALL PUTGG(QFSIPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("QFSI"),1,GLL,WRKS)
         CALL PUTGG(QFNPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("QFNI"),1,GLL,WRKS)
         CALL PUTGG(QFVFPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("QFVI"),1,GLL,WRKS)
         CALL PUTGG(UFSINSTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("UFSI"),1,GLL,WRKS)
         CALL PUTGG(VFSINSTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("VFSI"),1,GLL,WRKS)
         CALL PUTGG(HFLIPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("HFLI"),1,GLL,WRKS)
         CALL PUTGG(HFSIPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("HFSI"),1,GLL,WRKS)
         CALL PUTGG(FDLPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FDLI"),1,GLL,WRKS)
         CALL PUTGG(FLGPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FLGI"),1,GLL,WRKS)
         CALL PUTGG(FSSPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSSI"),1,GLL,WRKS)
         CALL PUTGG(FSGPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSGI"),1,GLL,WRKS)
         CALL PUTGG(FSSCPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSCI"),1,GLL,WRKS)
         CALL PUTGG(FSGCPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FGCI"),1,GLL,WRKS)
         CALL PUTGG(FDLCPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FLCI"),1,GLL,WRKS)
         CALL PUTGG(FSOPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSOI"),1,GLL,WRKS)
         CALL PUTGG(FSRPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FSRI"),1,GLL,WRKS)
         CALL PUTGG(OLRPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("OLRI"),1,GLL,WRKS)
         CALL PUTGG(OLRCPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("OLCI"),1,GLL,WRKS)
         CALL PUTGG(FSRCPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("FRCI"),1,GLL,WRKS)
         CALL PUTGG(PWATIPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PWTI"),1,GLL,WRKS)
         CALL PUTGG(CLDTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CLTI"),1,GLL,WRKS)
         CALL PUTGG(CLWTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CLWI"),1,GLL,WRKS)
         CALL PUTGG(CICTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CICI"),1,GLL,WRKS)
         CALL PUTGG(BALTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("BLTI"),1,GLL,WRKS)
         CALL PUTGG(CDCBPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CDCI"),1,GLL,WRKS)
         CALL PUTGG(CSCBPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CSCI"),1,GLL,WRKS)
         CALL PUTGG(PMSLIPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("PSMI"),1,GLL,WRKS)
         CALL PUTGG(GTPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8(" GTI"),1,GLL,WRKS)

#if defined (agcm_ctem)
         CALL PUTGG(CFGPPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CFGP"),1,GLL,WRKS)
         CALL PUTGG(CFRVPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CFRV"),1,GLL,WRKS)
         CALL PUTGG(CFRHPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CFRH"),1,GLL,WRKS)
         CALL PUTGG(CFRDPAL,LON1,ILAT,KHEM,NPGG,K,NU3HR,
     1                NC4TO8("CFRD"),1,GLL,WRKS)
#endif

      ENDIF ! LS3HR
C
      IF (LSBEG) THEN
C
C        * SAVE I/O GRIDS ON FILE NUPR EVERY ISBEG STEPS, IF REQUESTED.
C
         K=KOUNT
         CALL PUTGG(ECO2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ECO2")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(FCOOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FCOO")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(QFSLPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("QFSL")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(QFSOPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("QFSO")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(BEGKPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("BEGK")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(BEGLPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("BEGL")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(BWGKPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("BWGK")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(BWGLPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("BWGL")
     1                       ,1,GLL,WRKS)
         CALL PKZEROS2(QFSLPAL,IJPAK,   1)
C        CALL PKZEROS2(QFSOPAL,IJPAK,   1)    ! zeroed out after normalizing BWGO
         CALL PKZEROS2(BEGKPAL,IJPAK,   1)
         CALL PKZEROS2(BEGLPAL,IJPAK,   1)
C        CALL PKZEROS2(BWGKPAL,IJPAK,   1)    ! zeroed out after normalizing BWGO
C        CALL PKZEROS2(BWGLPAL,IJPAK,   1)    ! zeroed out after normalizing BWGO
C
         DO N=1,NTRAC
           IF(ITRSRF(N).NE.0) THEN
             CALL NAME2(NAM,'XS',N)
             CALL PUTGG(XSFXPAK(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,NAM,
     1                    1,GLL,WRKS)
           ENDIF
         ENDDO
      ENDIF
      IF (LSBEG.OR.LSGG) THEN
C
C        * SAVE SEA-ICE AND TILED FIELDS ON FILE NUPR EVERY (ISBEG.OR.ISGG) STEPS.
C
         K = KOUNT
         CALL PUTGG(SICNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SICN")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( SICPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" SIC")
     1                       ,1,GLL,WRKS)
C
         DO M=1,IM
           CALL PUTGG(FAREPAT(1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8("FARE"),M,GLL,WRKS)
         ENDDO

         DO M=1,IM
           CALL PUTGG(GTPAT  (1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8(" GTT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(SNOPAT (1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8("SNOT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(ANPAT  (1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8(" ANT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(FNPAT  (1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8(" FNT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(TNPAT  (1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8(" TNT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(RHONPAT(1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8("RHNT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(BCSNPAT(1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8("BCNT"),M,GLL,WRKS)
         ENDDO
         DO M=1,IM
           CALL PUTGG(REFPAT (1,M),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8("REFT"),M,GLL,WRKS)
         ENDDO
      ENDIF
      IF (LSGG) THEN
C
C        * SAVE I/O GRIDS ON FILE NUPR EVERY ISGG STEPS, IF REQUESTED.
C
         K=KOUNT
         CALL PUTGG(TBASPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("TBAS")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  GTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  GT")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( GTAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" GTA")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( SNOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" SNO")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(WSNOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WSNO")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(ZPNDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ZPND")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( REFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" REF")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(BCSNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("BCSN")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(DEPBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DEPB")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  ANPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  AN")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(RHONPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("RHON")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  TVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  TV")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  TNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  TN")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  TTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  TT")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( WVLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" WVL")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( WVFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" WVF")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  MVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  MV")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( TFXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" TFX")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( QFXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QFX")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(CHFXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CHFX")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(CQFXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CQFX")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(CBMFPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CBML")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(PBLTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PBLT")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( TCVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" TCV")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(PBLHPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PBLH")
     1                       ,1,GLL,WRKS)
#if (defined(pla) && defined(pam))
#if defined (xtrapla2)
          IF ( ISDIAG > 1 ) THEN
           DO ISF=1,ISDIAG
            CALL NAME2(NAM,'SI',ISF)
            CALL PUTGG(SDVLPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
           ENDDO
          ENDIF
#endif
#endif
#if defined (aodpth)
C         CALL PUTGG(ODSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODST")
C     1                       ,1,GLL,WRKS)
C         CALL PUTGG(ABSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABST")
C     1                       ,1,GLL,WRKS)
C         CALL PUTGG(ODSVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODSV")
C     1                       ,1,GLL,WRKS)
C         CALL PUTGG(OFSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFST")
C     1                       ,1,GLL,WRKS)
#endif
C        DO L=1,NBS
C           CALL PUTGG(SALBPAL(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
C    1                                NC4TO8("SALB"),L,GLL,WRKS)
C        ENDDO

         DO L=1,IGND
            CALL PUTGG(  TGPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("  TG"),LG(L),GLL,WRKS)
            CALL PUTGG( WGLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" WGL"),LG(L),GLL,WRKS)
            CALL PUTGG( WGFPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" WGF"),LG(L),GLL,WRKS)
         ENDDO
C
#if defined xtrals
         DO L=1,ILEV
            CALL PUTGG(RAINPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("RAIN"),LH(L),GLL,WRKS)
            CALL PUTGG(SNOWPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("SNOW"),LH(L),GLL,WRKS)
         ENDDO
#endif
C
#if (defined(pla) && defined(pam))
#if defined (xtraplafrc)
          DO L=1,ILEV
             CALL PUTGG(AMLDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("AMLD"),LH(L),GLL,WRKS)
             CALL PUTGG(REAMPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("REAM"),LH(L),GLL,WRKS)
             CALL PUTGG(VEAMPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("VEAM"),LH(L),GLL,WRKS)
             CALL PUTGG(FR1PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8(" FR1"),LH(L),GLL,WRKS)
             CALL PUTGG(FR2PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8(" FR2"),LH(L),GLL,WRKS)
             CALL PUTGG(SSLDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("SSLD"),LH(L),GLL,WRKS)
             CALL PUTGG(RESSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("RESS"),LH(L),GLL,WRKS)
             CALL PUTGG(VESSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("VESS"),LH(L),GLL,WRKS)
             CALL PUTGG(DSLDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("DSLD"),LH(L),GLL,WRKS)
             CALL PUTGG(REDSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("REDS"),LH(L),GLL,WRKS)
             CALL PUTGG(VEDSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("VEDS"),LH(L),GLL,WRKS)
             CALL PUTGG(BCLDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("BCLD"),LH(L),GLL,WRKS)
             CALL PUTGG(REBCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("REBC"),LH(L),GLL,WRKS)
             CALL PUTGG(VEBCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("VEBC"),LH(L),GLL,WRKS)
             CALL PUTGG(OCLDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("OCLD"),LH(L),GLL,WRKS)
             CALL PUTGG(REOCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("REOC"),LH(L),GLL,WRKS)
             CALL PUTGG(VEOCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("VEOC"),LH(L),GLL,WRKS)
             CALL PUTGG(BCICPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("BCIC"),LH(L),GLL,WRKS)
          ENDDO
#endif
#if (defined(xtraplafrc) || defined(xtrapla1))
          DO L=1,ILEV
             CALL PUTGG(ZCDNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ZCDN"),LH(L),GLL,WRKS)
          ENDDO
#endif
#endif
C
C     * SAVE SAMPLED BURDENS
C
          DO N=1,NTRAC
            CALL NAME2(NAM,'VS',N)
            CALL PUTGG(XTVIPAS(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NAM,1,GLL,WRKS)
          ENDDO
      ENDIF

C     * SAVE HIGH-FREQUENCY FIELDS ON FILE NUPR EVERY ISHF STEPS.

      IF (LSHF)                                                  THEN
          K=KOUNT
          CALL PUTGG(PCHFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PCHF")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(PLHFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PLHF")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(PSHFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PSHF")
     1                        ,1,GLL,WRKS)
          CALL PKZEROS2(PCHFPAK,IJPAK,   1)
          CALL PKZEROS2(PLHFPAK,IJPAK,   1)
          CALL PKZEROS2(PSHFPAK,IJPAK,   1)
      ENDIF

C     * SAVE MONTHLY-SAVED FIELDS ON FILE NUPR EVERY ISMON STEPS.

      IF (IDAY.EQ.LDAY .AND. NSECS.EQ.0)                            THEN
          K=KOUNT
          CALL PUTGG(PWATPAM,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PWM")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(QTPHPAM,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QHM")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(QTPFPAM,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QFM")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(QTPTPAM,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QTM")
     1                       ,1,GLL,WRKS)
          CALL PKZEROS2(PWATPAM,IJPAK,   1)
          CALL PKZEROS2(QTPHPAM,IJPAK,   1)
          CALL PKZEROS2(QTPFPAM,IJPAK,   1)
          CALL PKZEROS2(QTPTPAM,IJPAK,   1)
C
          DO N=1,NTRAC
            CALL NAME2(NAM,'VM',N)
            CALL PUTGG(XTVIPAM(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'VH',N)
            CALL PUTGG(XTPHPAM(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'VF',N)
            CALL PUTGG(XTPFPAM(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'VT',N)
            CALL PUTGG(XTPTPAM(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NAM,1,GLL,WRKS)
            CALL PKZEROS2(XTVIPAM(1,N),IJPAK,   1)
            CALL PKZEROS2(XTPHPAM(1,N),IJPAK,   1)
            CALL PKZEROS2(XTPFPAM(1,N),IJPAK,   1)
            CALL PKZEROS2(XTPTPAM(1,N),IJPAK,   1)
          ENDDO
      ENDIF
C
C     * SAVE ACCUMULATED FIELDS ON FILE NUPR EVERY ISRAD STEPS, IF
C     * REQUESTED.
C
      IF (LSRAD)                                                 THEN
          K = KOUNT
          CALL PUTGG(  DRPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  DR")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FSOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FSO")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FSGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FSG")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FSDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FSD")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FSVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FSV")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FSSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FSS")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSDCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSDC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSSCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSSC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FDLCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FDLC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( OLRPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" OLR")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(OLRCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OLRC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FLAMPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLAM")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FLANPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLAN")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FDLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FDL")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FLGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FLG")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSLOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSLO")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(CLDTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CLDT")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(CLDOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CLDO")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(CLDAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CLDA")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( FSRPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FSR")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSRCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSRC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSAMPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSAM")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSANPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSAN")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FSGCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSGC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FLGCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLGC")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(PWATPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PWAT")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(  FNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  FN")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(FNLAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FNLA")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(GSNOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("GSNO")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(SMLTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SMLT")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(CLWTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CLWT")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(CICTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CICT")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( HFLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" HFL")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( HFSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" HFS")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( PCPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PCP")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(PCPCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PCPC")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(PCPSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PCPS")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(PIVFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PIVF")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(PCPNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PCPN")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(PIVLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("PIVL")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( PINPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PIN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( PIGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PIG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( QFSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QFS")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( BEGPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" BEG")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( UFSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" UFS")
     1                       ,1,GLL,WRKS)
          CALL PUTGG( VFSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" VFS")
     1                       ,1,GLL,WRKS)
          CALL PUTGG(  STPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  ST")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(  SQPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  SQ")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(  SUPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  SU")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(  SVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  SV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( SRHPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" SRH")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( SWAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" SWA")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(FSGVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSGV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(FSGGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSGG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(FLGVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLGV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(FLGGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLGG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFSVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFSV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFSGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFSG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFSNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFSN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HMFVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HMFV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HMFNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HMFN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFCVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFCV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFCNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFCN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( QFGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QFG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( QFNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QFN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(QFVLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("QFVL")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(QFVFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("QFVF")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(FSGNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSGN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(FLGNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLGN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( ROFPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" ROF")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(ROFVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ROFV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(ROFNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ROFN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(ROFOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ROFO")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(ROVGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ROVG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(WTRVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WTRV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(WTRNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WTRN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(WTRGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WTRG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFLVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFLV")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFLNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFLN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(HFLGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFLG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(  ZNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  ZN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( PSAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PSA")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( FVNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FVN")
     1                       , 1,GLL,WRKS)
          CALL PUTGG( FVGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FVG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(SKYGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SKYG")
     1                       , 1,GLL,WRKS)
          CALL PUTGG(SKYNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SKYN")
     1                       , 1,GLL,WRKS)
         CALL PUTGG(  GAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  GA")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( HBLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" HBL")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(ILMOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ILMO")
     1                       ,1,GLL,WRKS)
         CALL PUTGG( PETPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PET")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(ROFBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ROFB")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(ROFSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ROFS")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(  UEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("  UE")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(WTABPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WTAB")
     1                       ,1,GLL,WRKS)

          DO 530 L=1,IGND
              CALL PUTGG(HFCGPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HFCG"),LG(L),GLL,WRKS)
              CALL PUTGG(GFLXPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("GFLX"),LG(L),GLL,WRKS)
              CALL PUTGG(QFVGPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("QFVG"),LG(L),GLL,WRKS)
              CALL PUTGG(HMFGPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HMFG"),LG(L),GLL,WRKS)
  530     CONTINUE
C
          CALL PKZEROS2(  DRPAK,IJPAK,   1)
          CALL PKZEROS2( FSOPAK,IJPAK,   1)
          CALL PKZEROS2( FSGPAK,IJPAK,   1)
          CALL PKZEROS2( FSDPAK,IJPAK,   1)
          CALL PKZEROS2( FSVPAK,IJPAK,   1)
          CALL PKZEROS2( FSSPAK,IJPAK,   1)
          CALL PKZEROS2(FSDCPAK,IJPAK,   1)
          CALL PKZEROS2(FSSCPAK,IJPAK,   1)
          CALL PKZEROS2(FDLCPAK,IJPAK,   1)
          CALL PKZEROS2( OLRPAK,IJPAK,   1)
          CALL PKZEROS2(OLRCPAK,IJPAK,   1)
          CALL PKZEROS2(FLAMPAK,IJPAK,   1)
          CALL PKZEROS2(FLANPAK,IJPAK,   1)
          CALL PKZEROS2( FDLPAK,IJPAK,   1)
          CALL PKZEROS2( FLGPAK,IJPAK,   1)
          CALL PKZEROS2(FSLOPAK,IJPAK,   1)
          CALL PKZEROS2(CLDTPAK,IJPAK,   1)
          CALL PKZEROS2(CLDOPAK,IJPAK,   1)
          CALL PKZEROS2(CLDAPAK,IJPAK,   1)
          CALL PKZEROS2( FSRPAK,IJPAK,   1)
          CALL PKZEROS2(FSRCPAK,IJPAK,   1)
          CALL PKZEROS2(FSAMPAK,IJPAK,   1)
          CALL PKZEROS2(FSANPAK,IJPAK,   1)
          CALL PKZEROS2(FSGCPAK,IJPAK,   1)
          CALL PKZEROS2(FLGCPAK,IJPAK,   1)
          CALL PKZEROS2(PWATPAK,IJPAK,   1)
          CALL PKZEROS2(SMLTPAK,IJPAK,   1)
          CALL PKZEROS2(  FNPAK,IJPAK,   1)
          CALL PKZEROS2(FNLAPAK,IJPAK,   1)
          CALL PKZEROS2(GSNOPAK,IJPAK,   1)
          CALL PKZEROS2(CLWTPAK,IJPAK,   1)
          CALL PKZEROS2(CICTPAK,IJPAK,   1)
          CALL PKZEROS2( HFLPAK,IJPAK,   1)
          CALL PKZEROS2( HFSPAK,IJPAK,   1)
          CALL PKZEROS2( QFSPAK,IJPAK,   1)
          CALL PKZEROS2( PCPPAK,IJPAK,   1)
          CALL PKZEROS2(PCPCPAK,IJPAK,   1)
          CALL PKZEROS2(PCPNPAK,IJPAK,   1)
          CALL PKZEROS2(PCPSPAK,IJPAK,   1)
          CALL PKZEROS2(PIVFPAK,IJPAK,   1)
          CALL PKZEROS2(PIVLPAK,IJPAK,   1)
          CALL PKZEROS2( PINPAK,IJPAK,   1)
          CALL PKZEROS2( PIGPAK,IJPAK,   1)
          CALL PKZEROS2( UFSPAK,IJPAK,   1)
          CALL PKZEROS2( VFSPAK,IJPAK,   1)
          CALL PKZEROS2( BEGPAL,IJPAK,   1)
          CALL PKZEROS2(  STPAK,IJPAK,   1)
          CALL PKZEROS2(  SQPAK,IJPAK,   1)
          CALL PKZEROS2(  SUPAK,IJPAK,   1)
          CALL PKZEROS2(  SVPAK,IJPAK,   1)
          CALL PKZEROS2( SRHPAK,IJPAK,   1)
          CALL PKZEROS2( SWAPAK,IJPAK,   1)
          CALL PKZEROS2(FSGVPAK,IJPAK,   1)
          CALL PKZEROS2(FSGGPAK,IJPAK,   1)
          CALL PKZEROS2(FLGVPAK,IJPAK,   1)
          CALL PKZEROS2(FLGGPAK,IJPAK,   1)
          CALL PKZEROS2(HFSVPAK,IJPAK,   1)
          CALL PKZEROS2(HFSGPAK,IJPAK,   1)
          CALL PKZEROS2(HFSNPAK,IJPAK,   1)
          CALL PKZEROS2(HMFVPAK,IJPAK,   1)
          CALL PKZEROS2(HMFNPAK,IJPAK,   1)
          CALL PKZEROS2(HFCGPAK,IJPAK,IGND)
          CALL PKZEROS2(HFCVPAK,IJPAK,   1)
          CALL PKZEROS2(HFCNPAK,IJPAK,   1)
          CALL PKZEROS2( QFGPAK,IJPAK,   1)
          CALL PKZEROS2( QFNPAK,IJPAK,   1)
          CALL PKZEROS2(QFVGPAK,IJPAK,IGND)
          CALL PKZEROS2(QFVLPAK,IJPAK,   1)
          CALL PKZEROS2(QFVFPAK,IJPAK,   1)
          CALL PKZEROS2(FSGNPAK,IJPAK,   1)
          CALL PKZEROS2(FLGNPAK,IJPAK,   1)
          CALL PKZEROS2( ROFPAK,IJPAK,   1)
          CALL PKZEROS2(ROFVPAK,IJPAK,   1)
          CALL PKZEROS2(ROFNPAK,IJPAK,   1)
          CALL PKZEROS2(ROFOPAK,IJPAK,   1)
          CALL PKZEROS2(ROVGPAK,IJPAK,   1)
          CALL PKZEROS2(WTRVPAK,IJPAK,   1)
          CALL PKZEROS2(WTRNPAK,IJPAK,   1)
          CALL PKZEROS2(WTRGPAK,IJPAK,   1)
          CALL PKZEROS2(HFLVPAK,IJPAK,   1)
          CALL PKZEROS2(HFLNPAK,IJPAK,   1)
          CALL PKZEROS2(HFLGPAK,IJPAK,   1)
          CALL PKZEROS2(  ZNPAK,IJPAK,   1)
          CALL PKZEROS2( PSAPAK,IJPAK,   1)
          CALL PKZEROS2( FVNPAK,IJPAK,   1)
          CALL PKZEROS2( FVGPAK,IJPAK,   1)
          CALL PKZEROS2(SKYGPAK,IJPAK,   1)
          CALL PKZEROS2(SKYNPAK,IJPAK,   1)
          CALL PKZEROS2(ROFBPAK,IJPAK,   1)
          CALL PKZEROS2(ROFSPAK,IJPAK,   1)
          CALL PKZEROS2(HMFGPAK,IJPAK,IGND)
          CALL PKZEROS2(  GAPAK,IJPAK,   1)
          CALL PKZEROS2(GFLXPAK,IJPAK,   1)
          CALL PKZEROS2( PETPAK,IJPAK,   1)
          CALL PKZEROS2(  UEPAK,IJPAK,   1)
C
          DO 534 N=1,NTRAC
              CALL NAME2(NAM,'XF',N)
              CALL PUTGG(XFSPAK (1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NAM,1,GLL,WRKS)
              CALL NAME2(NAM,'XK',N)
              CALL PUTGG(XSRFPAK(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NAM,1,GLL,WRKS)
              CALL NAME2(NAM,'VI',N)
              CALL PUTGG(XTVIPAK(1,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NAM,1,GLL,WRKS)
C
              CALL PKZEROS2(XFSPAK (1,N),IJPAK,   1)
              CALL PKZEROS2(XSRFPAK(1,N),IJPAK,   1)
              CALL PKZEROS2(XTVIPAK(1,N),IJPAK,   1)
  534     CONTINUE
          DO L=1,ILEV
             CALL PUTGG(ALTIPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ALTI"),LH(L),GLL,WRKS)
             CALL PUTGG(TBNDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("TBND"),LH(L),GLL,WRKS)
             CALL PUTGG(EA55PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("EA55"),LH(L),GLL,WRKS)
             CALL PUTGG(RKMPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8(" RKM"),LH(L),GLL,WRKS)
             CALL PUTGG(RKHPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8(" RKH"),LH(L),GLL,WRKS)
             CALL PUTGG(RKQPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8(" RKQ"),LH(L),GLL,WRKS)
          ENDDO

          CALL PKZEROS2(ALTIPAK,IJPAK,ILEV)
          CALL PKZEROS2(TBNDPAK,IJPAK,ILEV)
          CALL PKZEROS2(EA55PAK,IJPAK,ILEV)
          CALL PKZEROS2( RKMPAK,IJPAK,ILEV)
          CALL PKZEROS2( RKHPAK,IJPAK,ILEV)
          CALL PKZEROS2( RKQPAK,IJPAK,ILEV)
C
C         * GENERAL LAKE FIELDS.
C
          CALL PUTGG(LDMXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LDMX"),1,GLL,WRKS)
          CALL PUTGG(LRIMPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LRIM"),1,GLL,WRKS)
          CALL PUTGG(LRINPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LRIN"),1,GLL,WRKS)
          CALL PUTGG(LUIMPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LUIM"),1,GLL,WRKS)
          CALL PUTGG(LUINPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LUIN"),1,GLL,WRKS)
          CALL PUTGG(LZICPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LZIC"),1,GLL,WRKS)
#if defined (cslm)
C
C         * SAMPLED FIELDS.
C
          CALL PUTGG(DELUPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("DELU"),1,GLL,WRKS)
          CALL PUTGG(DTMPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("DTMP"),1,GLL,WRKS)
          CALL PUTGG(EXPWPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("EXPW"),1,GLL,WRKS)
          CALL PUTGG(GREDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("GRED"),1,GLL,WRKS)
          CALL PUTGG(RHOMPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("RHOM"),1,GLL,WRKS)
          CALL PUTGG(T0LKPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("T0LK"),1,GLL,WRKS)
          CALL PUTGG(TKELPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("TKEL"),1,GLL,WRKS)
          DO L=1,100
           CALL PUTGG(TLAKPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                  NC4TO8("TLAK"),L,GLL,WRKS)
          ENDDO
C
C         * ACCUMULATED FIELDS.
C
          CALL PUTGG(FLGLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FLGL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(FNLPAK, LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FNL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(FSGLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FSGL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(HFCLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFCL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(HFLLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFLL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(HFSLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HFSL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(HMFLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("HMFL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(PILPAK, LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" PIL")
     1                        ,1,GLL,WRKS)
          CALL PUTGG(QFLPAK, LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" QFL")
     1                        ,1,GLL,WRKS)
C
          FLGLPAK=0.
          FNLPAK =0.
          FSGLPAK=0.
          HFCLPAK=0.
          HFLLPAK=0.
          HFSLPAK=0.
          HMFLPAK=0.
          PILPAK =0.
          QFLPAK =0.
#endif
#if defined (flake)
C
C        * SAMPLED FIELDS.
C
          CALL PUTGG(LSHPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LSHP"),1,GLL,WRKS)
          CALL PUTGG(LTAVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LTAV"),1,GLL,WRKS)
          CALL PUTGG(LTICPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LTIC"),1,GLL,WRKS)
          CALL PUTGG(LTMXPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LTMX"),1,GLL,WRKS)
          CALL PUTGG(LTSNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LTSN"),1,GLL,WRKS)
          CALL PUTGG(LTWBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LTWB"),1,GLL,WRKS)
          CALL PUTGG(LZSNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("LZSN"),1,GLL,WRKS)
#endif
#if defined xtraconv
C
          CALL PUTGG(CAPEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CAPE")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(CINHPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CINH")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( BCDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" BCD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( BCSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" BCS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( TCDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" TCD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( TCSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" TCS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(CDCBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CDCB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(CSCBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("CSCB")
     1                         ,1,GLL,WRKS)
C
          DO 535 L=1,ILEV
              CALL PUTGG( DMCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" DMC"),LH(L),GLL,WRKS)
              CALL PUTGG( SMCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" SMC"),LH(L),GLL,WRKS)
              CALL PUTGG(DMCDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("DMCD"),LH(L),GLL,WRKS)
              CALL PUTGG(DMCUPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("DMCU"),LH(L),GLL,WRKS)
  535     CONTINUE
C
          CALL PKZEROS2(CAPEPAK,IJPAK,   1)
          CALL PKZEROS2(CINHPAK,IJPAK,   1)
          CALL PKZEROS2( BCDPAK,IJPAK,   1)
          CALL PKZEROS2( BCSPAK,IJPAK,   1)
          CALL PKZEROS2( TCDPAK,IJPAK,   1)
          CALL PKZEROS2( TCSPAK,IJPAK,   1)
          CALL PKZEROS2(CDCBPAK,IJPAK,   1)
          CALL PKZEROS2(CSCBPAK,IJPAK,   1)
          CALL PKZEROS2( DMCPAK,IJPAK,ILEV)
          CALL PKZEROS2( SMCPAK,IJPAK,ILEV)
          CALL PKZEROS2(DMCDPAK,IJPAK,ILEV)
          CALL PKZEROS2(DMCUPAK,IJPAK,ILEV)
#endif
#if defined xtrachem
          CALL PUTGG( DD4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" DD4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(DOX4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DOX4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(DOXDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DOXD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESDPAK ,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" ESD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESFSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESFS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EAISPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EAIS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESTSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESTS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EFISPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EFIS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESFBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESFB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EAIBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EAIB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESTBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESTB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EFIBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EFIB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESFOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESFO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EAIOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EAIO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESTOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESTO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EFIOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EFIO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EDSLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EDSL")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EDSOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EDSO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESVCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESVC")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ESVEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ESVE")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(NOXDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("NOXD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDD4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDD4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDL4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDL4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDS4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDS4")
     1                         ,1,GLL,WRKS)
C
          CALL PKZEROS2( DD4PAK,IJPAK,   1)
          CALL PKZEROS2(DOX4PAK,IJPAK,   1)
          CALL PKZEROS2(DOXDPAK,IJPAK,   1)
          CALL PKZEROS2(ESDPAK, IJPAK,   1)
          CALL PKZEROS2(ESFSPAK,IJPAK,   1)
          CALL PKZEROS2(EAISPAK,IJPAK,   1)
          CALL PKZEROS2(ESTSPAK,IJPAK,   1)
          CALL PKZEROS2(EFISPAK,IJPAK,   1)
          CALL PKZEROS2(ESFBPAK,IJPAK,   1)
          CALL PKZEROS2(EAIBPAK,IJPAK,   1)
          CALL PKZEROS2(ESTBPAK,IJPAK,   1)
          CALL PKZEROS2(EFIBPAK,IJPAK,   1)
          CALL PKZEROS2(ESFOPAK,IJPAK,   1)
          CALL PKZEROS2(EAIOPAK,IJPAK,   1)
          CALL PKZEROS2(ESTOPAK,IJPAK,   1)
          CALL PKZEROS2(EFIOPAK,IJPAK,   1)
          CALL PKZEROS2(EDSLPAK,IJPAK,   1)
          CALL PKZEROS2(EDSOPAK,IJPAK,   1)
          CALL PKZEROS2(ESVCPAK,IJPAK,   1)
          CALL PKZEROS2(ESVEPAK,IJPAK,   1)
          CALL PKZEROS2(NOXDPAK,IJPAK,   1)
          CALL PKZEROS2(WDD4PAK,IJPAK,   1)
          CALL PKZEROS2(WDL4PAK,IJPAK,   1)
          CALL PKZEROS2(WDS4PAK,IJPAK,   1)
#ifndef pla
          CALL PUTGG(SLO3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SLO3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(SLHPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SLHP")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(SSO3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SSO3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(SSHPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SSHP")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDS6PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDS6")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(SDO3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SDO3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(SDHPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SDHP")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( DD6PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" DD6")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDL6PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDL6")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDD6PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDD6")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( DDDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" DDD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( DDBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" DDB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( DDOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" DDO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( DDSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" DDS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDLDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDLD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDLBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDLB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDLOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDLO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDLSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDLS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDDDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDDD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDDBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDDB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDDOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDDO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDDSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDDS")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDSDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDSD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDSBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDSB")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDSOPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDSO")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(WDSSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("WDSS")
     1                         ,1,GLL,WRKS)
C
          DO 540 L=1,ILEV
              CALL PUTGG( PHLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                  NC4TO8(" PHL"),LH(L),GLL,WRKS)
              CALL PUTGG( PHSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                  NC4TO8(" PHS"),LH(L),GLL,WRKS)
              CALL PUTGG( PHDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                  NC4TO8(" PHD"),LH(L),GLL,WRKS)
  540     CONTINUE
C
          CALL PKZEROS2(SLO3PAK,IJPAK,   1)
          CALL PKZEROS2(SLHPPAK,IJPAK,   1)
          CALL PKZEROS2(SSO3PAK,IJPAK,   1)
          CALL PKZEROS2(SSHPPAK,IJPAK,   1)
          CALL PKZEROS2(WDS6PAK,IJPAK,   1)
          CALL PKZEROS2(SDO3PAK,IJPAK,   1)
          CALL PKZEROS2(SDHPPAK,IJPAK,   1)
          CALL PKZEROS2( DD6PAK,IJPAK,   1)
          CALL PKZEROS2(WDL6PAK,IJPAK,   1)
          CALL PKZEROS2(WDD6PAK,IJPAK,   1)
          CALL PKZEROS2(DDDPAK ,IJPAK,   1)
          CALL PKZEROS2(DDBPAK ,IJPAK,   1)
          CALL PKZEROS2(DDOPAK ,IJPAK,   1)
          CALL PKZEROS2(DDSPAK ,IJPAK,   1)
          CALL PKZEROS2(WDLDPAK,IJPAK,   1)
          CALL PKZEROS2(WDLBPAK,IJPAK,   1)
          CALL PKZEROS2(WDLOPAK,IJPAK,   1)
          CALL PKZEROS2(WDLSPAK,IJPAK,   1)
          CALL PKZEROS2(WDDDPAK,IJPAK,   1)
          CALL PKZEROS2(WDDBPAK,IJPAK,   1)
          CALL PKZEROS2(WDDOPAK,IJPAK,   1)
          CALL PKZEROS2(WDDSPAK,IJPAK,   1)
          CALL PKZEROS2(WDSDPAK,IJPAK,   1)
          CALL PKZEROS2(WDSBPAK,IJPAK,   1)
          CALL PKZEROS2(WDSOPAK,IJPAK,   1)
          CALL PKZEROS2(WDSSPAK,IJPAK,   1)
#endif
#endif
#if defined (xtradust)
          CALL PUTGG(DUWDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DUWD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(DUSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DUST")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(DUTHPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DUTH")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(USMKPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("USMK")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(FALLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FALL")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(FA10PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("FA10")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( FA2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FA2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG( FA1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8(" FA1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(GUSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("GUST")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ZSPDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ZSPD")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(VGFRPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("VGFR")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(SMFRPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("SMFR")
     1                         ,1,GLL,WRKS)
C
          CALL PKZEROS2(DUWDPAK,IJPAK,   1)
          CALL PKZEROS2(DUSTPAK,IJPAK,   1)
          CALL PKZEROS2(DUTHPAK,IJPAK,   1)
          CALL PKZEROS2(USMKPAK,IJPAK,   1)
          CALL PKZEROS2(FALLPAK,IJPAK,   1)
          CALL PKZEROS2(FA10PAK,IJPAK,   1)
          CALL PKZEROS2( FA2PAK,IJPAK,   1)
          CALL PKZEROS2( FA1PAK,IJPAK,   1)
          CALL PKZEROS2(GUSTPAK,IJPAK,   1)
          CALL PKZEROS2(ZSPDPAK,IJPAK,   1)
          CALL PKZEROS2(VGFRPAK,IJPAK,   1)
          CALL PKZEROS2(SMFRPAK,IJPAK,   1)
#endif
#if (defined(pla) && defined(pam))
#if defined (xtrapla1)
          DO L=1,ILEV
              CALL PUTGG(SNCNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SNCN"),LH(L),GLL,WRKS)
              CALL PUTGG(SSUNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SSUN"),LH(L),GLL,WRKS)
              CALL PUTGG(SCNDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SCND"),LH(L),GLL,WRKS)
              CALL PUTGG(SGSPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SGSP"),LH(L),GLL,WRKS)
              CALL PUTGG(CCNPAK(1,L) ,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" CCN"),LH(L),GLL,WRKS)
              CALL PUTGG(CC02PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CC02"),LH(L),GLL,WRKS)
              CALL PUTGG(CC04PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CC04"),LH(L),GLL,WRKS)
              CALL PUTGG(CC08PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CC08"),LH(L),GLL,WRKS)
              CALL PUTGG(CC16PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CC16"),LH(L),GLL,WRKS)
              CALL PUTGG(CCNEPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CCNE"),LH(L),GLL,WRKS)
              CALL PUTGG(ACASPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("ACAS"),LH(L),GLL,WRKS)
              CALL PUTGG(ACOAPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("ACOA"),LH(L),GLL,WRKS)
              CALL PUTGG(ACBCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("ACBC"),LH(L),GLL,WRKS)
              CALL PUTGG(ACSSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("ACSS"),LH(L),GLL,WRKS)
              CALL PUTGG(ACMDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("ACMD"),LH(L),GLL,WRKS)
              CALL PUTGG(NTPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" N00"),LH(L),GLL,WRKS)
              CALL PUTGG(N20PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" N20"),LH(L),GLL,WRKS)
              CALL PUTGG(N50PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" N50"),LH(L),GLL,WRKS)
              CALL PUTGG(N100PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("N100"),LH(L),GLL,WRKS)
              CALL PUTGG(N200PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("N200"),LH(L),GLL,WRKS)
              CALL PUTGG(WTPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" W00"),LH(L),GLL,WRKS)
              CALL PUTGG(W20PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" W20"),LH(L),GLL,WRKS)
              CALL PUTGG(W50PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" W50"),LH(L),GLL,WRKS)
              CALL PUTGG(W100PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("W100"),LH(L),GLL,WRKS)
              CALL PUTGG(W200PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("W200"),LH(L),GLL,WRKS)
              CALL PUTGG(RCRIPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RCRI"),LH(L),GLL,WRKS)
              CALL PUTGG(SUPSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SUPS"),LH(L),GLL,WRKS)
              CALL PUTGG(HENRPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("HENR"),LH(L),GLL,WRKS)
              CALL PUTGG(O3FRPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("O3FR"),LH(L),GLL,WRKS)
              CALL PUTGG(H2O2FRPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("HPFR"),LH(L),GLL,WRKS)
              CALL PUTGG(WPARPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("WPAR"),LH(L),GLL,WRKS)
              CALL PUTGG(PM25PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("PM25"),LH(L),GLL,WRKS)
              CALL PUTGG(PM10PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("PM10"),LH(L),GLL,WRKS)
              CALL PUTGG(DM25PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("DM25"),LH(L),GLL,WRKS)
              CALL PUTGG(DM10PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("DM10"),LH(L),GLL,WRKS)
          ENDDO
          CALL PUTGG(VNCNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VNCN"),1,GLL,WRKS)
          CALL PUTGG(VASNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASN"),1,GLL,WRKS)
          CALL PUTGG(VSCDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSCD"),1,GLL,WRKS)
          CALL PUTGG(VGSPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VGSP"),1,GLL,WRKS)
          CALL PUTGG(VOAEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VOAE"),1,GLL,WRKS)
          CALL PUTGG(VBCEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VBCE"),1,GLL,WRKS)
          CALL PUTGG(VASEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASE"),1,GLL,WRKS)
          CALL PUTGG(VMDEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMDE"),1,GLL,WRKS)
          CALL PUTGG(VSSEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSSE"),1,GLL,WRKS)
          CALL PUTGG(VOAWPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VOAW"),1,GLL,WRKS)
          CALL PUTGG(VBCWPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VBCW"),1,GLL,WRKS)
          CALL PUTGG(VASWPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASW"),1,GLL,WRKS)
          CALL PUTGG(VMDWPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMDW"),1,GLL,WRKS)
          CALL PUTGG(VSSWPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSSW"),1,GLL,WRKS)
          CALL PUTGG(VOADPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VOAD"),1,GLL,WRKS)
          CALL PUTGG(VBCDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VBCD"),1,GLL,WRKS)
          CALL PUTGG(VASDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASD"),1,GLL,WRKS)
          CALL PUTGG(VMDDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMDD"),1,GLL,WRKS)
          CALL PUTGG(VSSDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSSD"),1,GLL,WRKS)
          CALL PUTGG(VOAGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VOAG"),1,GLL,WRKS)
          CALL PUTGG(VBCGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VBCG"),1,GLL,WRKS)
          CALL PUTGG(VASGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASG"),1,GLL,WRKS)
          CALL PUTGG(VMDGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMDG"),1,GLL,WRKS)
          CALL PUTGG(VSSGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSSG"),1,GLL,WRKS)
          CALL PUTGG(VOACPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VOAC"),1,GLL,WRKS)
          CALL PUTGG(VBCCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VBCC"),1,GLL,WRKS)
          CALL PUTGG(VASCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASC"),1,GLL,WRKS)
          CALL PUTGG(VMDCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMDC"),1,GLL,WRKS)
          CALL PUTGG(VSSCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSSC"),1,GLL,WRKS)
          CALL PUTGG(VASIPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VASI"),1,GLL,WRKS)
          CALL PUTGG(VAS1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VAS1"),1,GLL,WRKS)
          CALL PUTGG(VAS2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VAS2"),1,GLL,WRKS)
          CALL PUTGG(VAS3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VAS3"),1,GLL,WRKS)
          CALL PUTGG(VCCNPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VCCN"),1,GLL,WRKS)
          CALL PUTGG(VCNEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VCNE"),1,GLL,WRKS)
C
C          CALL PKZEROS2(SNCNPAK,IJPAK,ILEV)
C          CALL PKZEROS2(SSUNPAK,IJPAK,ILEV)
C          CALL PKZEROS2(SCNDPAK,IJPAK,ILEV)
C          CALL PKZEROS2(SGSPPAK,IJPAK,ILEV)
          CALL PKZEROS2(CC02PAK,IJPAK,ILEV)
          CALL PKZEROS2(CC04PAK,IJPAK,ILEV)
          CALL PKZEROS2(CC08PAK,IJPAK,ILEV)
          CALL PKZEROS2(CC16PAK,IJPAK,ILEV)
          CALL PKZEROS2(ACASPAK,IJPAK,ILEV)
          CALL PKZEROS2(ACOAPAK,IJPAK,ILEV)
          CALL PKZEROS2(ACBCPAK,IJPAK,ILEV)
          CALL PKZEROS2(ACSSPAK,IJPAK,ILEV)
          CALL PKZEROS2(ACMDPAK,IJPAK,ILEV)
          CALL PKZEROS2(NTPAK,IJPAK,ILEV  )
          CALL PKZEROS2(N20PAK,IJPAK,ILEV )
          CALL PKZEROS2(N50PAK,IJPAK,ILEV )
          CALL PKZEROS2(N100PAK,IJPAK,ILEV)
          CALL PKZEROS2(N200PAK,IJPAK,ILEV)
          CALL PKZEROS2(WTPAK,IJPAK,ILEV  )
          CALL PKZEROS2(W20PAK,IJPAK,ILEV )
          CALL PKZEROS2(W50PAK,IJPAK,ILEV )
          CALL PKZEROS2(W100PAK,IJPAK,ILEV)
          CALL PKZEROS2(W200PAK,IJPAK,ILEV)
          CALL PKZEROS2(PM25PAK,IJPAK,ILEV)
          CALL PKZEROS2(PM10PAK,IJPAK,ILEV)
          CALL PKZEROS2(DM25PAK,IJPAK,ILEV)
          CALL PKZEROS2(DM10PAK,IJPAK,ILEV)
          CALL PKZEROS2(VNCNPAK,IJPAK,   1)
          CALL PKZEROS2(VASNPAK,IJPAK,   1)
          CALL PKZEROS2(VSCDPAK,IJPAK,   1)
          CALL PKZEROS2(VGSPPAK,IJPAK,   1)
          CALL PKZEROS2(VOAEPAK,IJPAK,   1)
          CALL PKZEROS2(VBCEPAK,IJPAK,   1)
          CALL PKZEROS2(VASEPAK,IJPAK,   1)
          CALL PKZEROS2(VMDEPAK,IJPAK,   1)
          CALL PKZEROS2(VSSEPAK,IJPAK,   1)
          CALL PKZEROS2(VOAWPAK,IJPAK,   1)
          CALL PKZEROS2(VBCWPAK,IJPAK,   1)
          CALL PKZEROS2(VASWPAK,IJPAK,   1)
          CALL PKZEROS2(VMDWPAK,IJPAK,   1)
          CALL PKZEROS2(VSSWPAK,IJPAK,   1)
          CALL PKZEROS2(VOADPAK,IJPAK,   1)
          CALL PKZEROS2(VBCDPAK,IJPAK,   1)
          CALL PKZEROS2(VASDPAK,IJPAK,   1)
          CALL PKZEROS2(VMDDPAK,IJPAK,   1)
          CALL PKZEROS2(VSSDPAK,IJPAK,   1)
          CALL PKZEROS2(VOAGPAK,IJPAK,   1)
          CALL PKZEROS2(VBCGPAK,IJPAK,   1)
          CALL PKZEROS2(VASGPAK,IJPAK,   1)
          CALL PKZEROS2(VMDGPAK,IJPAK,   1)
          CALL PKZEROS2(VSSGPAK,IJPAK,   1)
          CALL PKZEROS2(VOACPAK,IJPAK,   1)
          CALL PKZEROS2(VBCCPAK,IJPAK,   1)
          CALL PKZEROS2(VASCPAK,IJPAK,   1)
          CALL PKZEROS2(VMDCPAK,IJPAK,   1)
          CALL PKZEROS2(VSSCPAK,IJPAK,   1)
          CALL PKZEROS2(VASIPAK,IJPAK,   1)
          CALL PKZEROS2(VAS1PAK,IJPAK,   1)
          CALL PKZEROS2(VAS2PAK,IJPAK,   1)
          CALL PKZEROS2(VAS3PAK,IJPAK,   1)
C          CALL PKZEROS2(VCCNPAK,IJPAK,   1)
C          CALL PKZEROS2(VCNEPAK,IJPAK,   1)
#endif
#if defined (xtrapla2)
          DO L=1,ILEV
              CALL PUTGG(CORNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CORN"),LH(L),GLL,WRKS)
              CALL PUTGG(CORMPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CORM"),LH(L),GLL,WRKS)
              CALL PUTGG(RSN1PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RSN1"),LH(L),GLL,WRKS)
              CALL PUTGG(RSM1PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RSM1"),LH(L),GLL,WRKS)
              CALL PUTGG(RSN2PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RSN2"),LH(L),GLL,WRKS)
              CALL PUTGG(RSM2PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RSM2"),LH(L),GLL,WRKS)
              CALL PUTGG(RSN3PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RSN3"),LH(L),GLL,WRKS)
              CALL PUTGG(RSM3PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RSM3"),LH(L),GLL,WRKS)
              IF ( ISDNUM > 1 ) THEN
               DO ISF=1,ISDNUM
                CALL NAME2(NAM,'SN',ISF)
                CALL PUTGG(SDNUPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'SM',ISF)
                CALL PUTGG(SDMAPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'SA',ISF)
                CALL PUTGG(SDACPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'SC',ISF)
                CALL PUTGG(SDCOPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'N1',ISF)
                CALL PUTGG(SSSNPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'N2',ISF)
                CALL PUTGG(SMDNPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'N3',ISF)
                CALL PUTGG(SIANPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M1',ISF)
                CALL PUTGG(SSSMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M2',ISF)
                CALL PUTGG(SMDMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M3',ISF)
                CALL PUTGG(SEWMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M4',ISF)
                CALL PUTGG(SSUMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M5',ISF)
                CALL PUTGG(SOCMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M6',ISF)
                CALL PUTGG(SBCMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
                CALL NAME2(NAM,'M7',ISF)
                CALL PUTGG(SIWMPAK(1,L,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,LH(L),GLL,WRKS)
               ENDDO
              ENDIF
          ENDDO
          CALL PUTGG(VRN1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRN1"),1,GLL,WRKS)
          CALL PUTGG(VRM1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRM1"),1,GLL,WRKS)
          CALL PUTGG(VRN2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRN2"),1,GLL,WRKS)
          CALL PUTGG(VRM2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRM2"),1,GLL,WRKS)
          CALL PUTGG(VRN3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRN3"),1,GLL,WRKS)
          CALL PUTGG(VRM3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRM3"),1,GLL,WRKS)
          CALL PUTGG(DEFAPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DEFA")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(DEFCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("DEFC")
     1                         ,1,GLL,WRKS)
          DO ISF=1,ISDUST
            CALL NAME2(NAM,'MA',ISF)
            CALL PUTGG(DMACPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'MC',ISF)
            CALL PUTGG(DMCOPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'NA',ISF)
            CALL PUTGG(DNACPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'NC',ISF)
            CALL PUTGG(DNCOPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
          ENDDO
          IF ( ISDIAG > 1 ) THEN
           DO ISF=1,ISDIAG
            CALL NAME2(NAM,'EF',ISF)
            CALL PUTGG(DEFXPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
            CALL NAME2(NAM,'EN',ISF)
            CALL PUTGG(DEFNPAK(1,ISF),LON1,ILAT,KHEM,NPGG,K,
     1                                         NUPR,NAM,1,GLL,WRKS)
           ENDDO
          ENDIF
C
          CALL PKZEROS2(CORNPAK,IJPAK,ILEV)
          CALL PKZEROS2(CORMPAK,IJPAK,ILEV)
          CALL PKZEROS2(RSN1PAK,IJPAK,ILEV)
          CALL PKZEROS2(RSM1PAK,IJPAK,ILEV)
          CALL PKZEROS2(RSN2PAK,IJPAK,ILEV)
          CALL PKZEROS2(RSM2PAK,IJPAK,ILEV)
          CALL PKZEROS2(RSN3PAK,IJPAK,ILEV)
          CALL PKZEROS2(RSM3PAK,IJPAK,ILEV)
          DO ISF=1,ISDNUM
            CALL PKZEROS2(SDNUPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SDMAPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SDACPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SDCOPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SSSNPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SMDNPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SIANPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SSSMPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SMDMPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SEWMPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SSUMPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SOCMPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SBCMPAK(1,1,ISF),IJPAK,ILEV)
            CALL PKZEROS2(SIWMPAK(1,1,ISF),IJPAK,ILEV)
          ENDDO
!          DO ISF=1,ISDIAG
!            CALL PKZEROS2(SDVLPAK(1,ISF),IJPAK,   1)
!          ENDDO
          CALL PKZEROS2(VRN1PAK,IJPAK,   1)
          CALL PKZEROS2(VRM1PAK,IJPAK,   1)
          CALL PKZEROS2(VRN2PAK,IJPAK,   1)
          CALL PKZEROS2(VRM2PAK,IJPAK,   1)
          CALL PKZEROS2(VRN3PAK,IJPAK,   1)
          CALL PKZEROS2(VRM3PAK,IJPAK,   1)
          CALL PKZEROS2(DEFAPAK,IJPAK,   1)
          CALL PKZEROS2(DEFCPAK,IJPAK,   1)
          DO ISF=1,ISDUST
            CALL PKZEROS2(DMACPAK(1,ISF),IJPAK,   1)
            CALL PKZEROS2(DMCOPAK(1,ISF),IJPAK,   1)
            CALL PKZEROS2(DNACPAK(1,ISF),IJPAK,   1)
            CALL PKZEROS2(DNCOPAK(1,ISF),IJPAK,   1)
          ENDDO
          DO ISF=1,ISDIAG
            CALL PKZEROS2(DEFXPAK(1,ISF),IJPAK,   1)
            CALL PKZEROS2(DEFNPAK(1,ISF),IJPAK,   1)
          ENDDO
#endif
#endif
#if defined xtrals
C
          DO L=1,ILEV
              CALL PUTGG(AGGPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" AGG"),LH(L),GLL,WRKS)
              CALL PUTGG(AUTPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" AUT"),LH(L),GLL,WRKS)
              CALL PUTGG(CNDPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" CND"),LH(L),GLL,WRKS)
              CALL PUTGG(DEPPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" DEP"),LH(L),GLL,WRKS)
              CALL PUTGG(EVPPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" EVP"),LH(L),GLL,WRKS)
              CALL PUTGG(FRHPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" FRH"),LH(L),GLL,WRKS)
              CALL PUTGG(FRKPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" FRK"),LH(L),GLL,WRKS)
              CALL PUTGG(FRSPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" FRS"),LH(L),GLL,WRKS)
              CALL PUTGG(MLTIPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("MLTI"),LH(L),GLL,WRKS)
              CALL PUTGG(MLTSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("MLTS"),LH(L),GLL,WRKS)
              CALL PUTGG(RACLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RACL"),LH(L),GLL,WRKS)
              CALL PUTGG(SACIPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SACI"),LH(L),GLL,WRKS)
              CALL PUTGG(SACLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SACL"),LH(L),GLL,WRKS)
              CALL PUTGG(SUBPAK (1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" SUB"),LH(L),GLL,WRKS)
              CALL PUTGG(SEDIPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("SEDI"),LH(L),GLL,WRKS)
              CALL PUTGG(RLIQPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RLIQ"),LH(L),GLL,WRKS)
              CALL PUTGG(RICEPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("RICE"),LH(L),GLL,WRKS)
              CALL PUTGG(CLIQPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CLIQ"),LH(L),GLL,WRKS)
              CALL PUTGG(CICEPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8("CICE"),LH(L),GLL,WRKS)
              CALL PUTGG(RLNCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                     NC4TO8(" LNC"),LH(L),GLL,WRKS)
          ENDDO
C
          CALL PUTGG(VAGGPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VAGG"),1,GLL,WRKS)
          CALL PUTGG(VAUTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VAUT"),1,GLL,WRKS)
          CALL PUTGG(VCNDPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VCND"),1,GLL,WRKS)
          CALL PUTGG(VDEPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VDEP"),1,GLL,WRKS)
          CALL PUTGG(VEVPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VEVP"),1,GLL,WRKS)
          CALL PUTGG(VFRHPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VFRH"),1,GLL,WRKS)
          CALL PUTGG(VFRKPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VFRK"),1,GLL,WRKS)
          CALL PUTGG(VFRSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VFRS"),1,GLL,WRKS)
          CALL PUTGG(VMLIPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMLI"),1,GLL,WRKS)
          CALL PUTGG(VMLSPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VMLS"),1,GLL,WRKS)
          CALL PUTGG(VRCLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VRCL"),1,GLL,WRKS)
          CALL PUTGG(VSCIPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSCI"),1,GLL,WRKS)
          CALL PUTGG(VSCLPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSCL"),1,GLL,WRKS)
          CALL PUTGG(VSUBPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSUB"),1,GLL,WRKS)
          CALL PUTGG(VSEDIPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("VSED"),1,GLL,WRKS)
          CALL PUTGG(RELIQPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("RELL"),1,GLL,WRKS)
          CALL PUTGG(REICEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("RELI"),1,GLL,WRKS)
          CALL PUTGG(CLDLIQPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("CLDL"),1,GLL,WRKS)
          CALL PUTGG(CLDICEPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("CLDI"),1,GLL,WRKS)
          CALL PUTGG(CTLNCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("CTLN"),1,GLL,WRKS)
          CALL PUTGG(CLLNCPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("CLLN"),1,GLL,WRKS)
C
          CALL PKZEROS2(AGGPAK, IJPAK,ILEV)
          CALL PKZEROS2(AUTPAK, IJPAK,ILEV)
          CALL PKZEROS2(CNDPAK, IJPAK,ILEV)
          CALL PKZEROS2(DEPPAK, IJPAK,ILEV)
          CALL PKZEROS2(EVPPAK, IJPAK,ILEV)
          CALL PKZEROS2(FRHPAK, IJPAK,ILEV)
          CALL PKZEROS2(FRKPAK, IJPAK,ILEV)
          CALL PKZEROS2(FRSPAK, IJPAK,ILEV)
          CALL PKZEROS2(MLTIPAK,IJPAK,ILEV)
          CALL PKZEROS2(MLTSPAK,IJPAK,ILEV)
          CALL PKZEROS2(RACLPAK,IJPAK,ILEV)
          CALL PKZEROS2(SACIPAK,IJPAK,ILEV)
          CALL PKZEROS2(SACLPAK,IJPAK,ILEV)
          CALL PKZEROS2(SUBPAK, IJPAK,ILEV)
          CALL PKZEROS2(SEDIPAK,IJPAK,ILEV)
          CALL PKZEROS2(RLIQPAK,IJPAK,ILEV)
          CALL PKZEROS2(RICEPAK,IJPAK,ILEV)
          CALL PKZEROS2(CLIQPAK,IJPAK,ILEV)
          CALL PKZEROS2(CICEPAK,IJPAK,ILEV)
          CALL PKZEROS2(RLNCPAK,IJPAK,ILEV)
C
          CALL PKZEROS2(VAGGPAK,IJPAK,   1)
          CALL PKZEROS2(VAUTPAK,IJPAK,   1)
          CALL PKZEROS2(VCNDPAK,IJPAK,   1)
          CALL PKZEROS2(VDEPPAK,IJPAK,   1)
          CALL PKZEROS2(VEVPPAK,IJPAK,   1)
          CALL PKZEROS2(VFRHPAK,IJPAK,   1)
          CALL PKZEROS2(VFRKPAK,IJPAK,   1)
          CALL PKZEROS2(VFRSPAK,IJPAK,   1)
          CALL PKZEROS2(VMLIPAK,IJPAK,   1)
          CALL PKZEROS2(VMLSPAK,IJPAK,   1)
          CALL PKZEROS2(VRCLPAK,IJPAK,   1)
          CALL PKZEROS2(VSCIPAK,IJPAK,   1)
          CALL PKZEROS2(VSCLPAK,IJPAK,   1)
          CALL PKZEROS2(VSUBPAK,IJPAK,   1)
          CALL PKZEROS2(VSEDIPAK,IJPAK,  1)
          CALL PKZEROS2(RELIQPAK,IJPAK,  1)
          CALL PKZEROS2(REICEPAK,IJPAK,  1)
          CALL PKZEROS2(CLDLIQPAK,IJPAK, 1)
          CALL PKZEROS2(CLDICEPAK,IJPAK, 1)
          CALL PKZEROS2(CTLNCPAK,IJPAK,  1)
          CALL PKZEROS2(CLLNCPAK,IJPAK,  1)
#endif
C
#if defined use_cosp
! COSP INPUT FOR CLOUDSAT SIMULATOR
          IF (Lradar_sim) THEN
           DO L=1,ILEV
             CALL PUTGG(RMIXPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("RMIX"),LH(L),GLL,WRKS)
             CALL PUTGG(SMIXPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("SMIX"),LH(L),GLL,WRKS)
             CALL PUTGG(RREFPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("RREF"),LH(L),GLL,WRKS)
             CALL PUTGG(SREFPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("SREF"),LH(L),GLL,WRKS)
           END DO
          ENDIF
          CALL PKZEROS2(RMIXPAK,IJPAK,ILEV)
          CALL PKZEROS2(SMIXPAK,IJPAK,ILEV)
          CALL PKZEROS2(RREFPAK,IJPAK,ILEV)
          CALL PKZEROS2(SREFPAK,IJPAK,ILEV)

! COSP OUTPUT
! Save fields from the module cosp_defs and zero as needed

! ISCCP

! Total cloud from ISCCP
          IF (Lalbisccp) THEN
             CALL PUTGG(o_albisccp,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ICCA"),1,GLL,WRKS)
             CALL PKZEROS2(o_albisccp,IJPAK,1)
          END IF

          IF (Lcltisccp) THEN
             CALL PUTGG(o_cltisccp,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ICCF"),1,GLL,WRKS)
             CALL PKZEROS2(o_cltisccp,IJPAK,1)
          END IF

          IF (Ltauisccp) THEN
             CALL PUTGG(o_tauisccp,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ICCT"),1,GLL,WRKS)
             CALL PKZEROS2(o_tauisccp,IJPAK,1)
          END IF

          IF (Lpctisccp) THEN
             CALL PUTGG(o_pctisccp,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ICCP"),1,GLL,WRKS)
             CALL PKZEROS2(o_pctisccp,IJPAK,1)
          END IF

          IF (Lmeantbisccp) THEN
             CALL PUTGG(o_meantbisccp,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8(" ITB"),1,GLL,WRKS)
             CALL PKZEROS2(o_meantbisccp,IJPAK,1)
          END IF

          IF (Lmeantbclrisccp) THEN
             CALL PUTGG(o_meantbclrisccp,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("ITBC"),1,GLL,WRKS)
             CALL PKZEROS2(o_meantbclrisccp,IJPAK,1)
          END IF

          IF (Lisccp_sim) THEN
             CALL PUTGG(o_sunl,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("SUNL"),1,GLL,WRKS)
             CALL PKZEROS2(o_sunl,IJPAK,1)
          END IF

          IF (Lclisccp) THEN
             N = 1
             DO IP = 1,  7      !NPTOP
                DO IT = 1, 7    !NTAUCLD
                   CALL PUTGG(o_clisccp(1,IT,IP),LON1,ILAT,KHEM,NPGG,
     1                          K,NUPR,NC4TO8("ICTP"),N,GLL,WRKS)
                   CALL PKZEROS2(o_clisccp(1,IT,IP),IJPAK,1)
                   N = N + 1
                END DO ! IT
             END DO ! IP
          END IF

          IF (Lfracout) THEN
! Array too large to be saved globally
             o_frac_out = 0.0
          END IF

! CALIPSO fields
          IF (Lclhcalipso) THEN
             CALL PUTGG(o_clhcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLHC"),1,GLL,WRKS)
             CALL PKZEROS2(o_clhcalipso,IJPAK,1)

             CALL PUTGG(ocnt_clhcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLHM"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clhcalipso,IJPAK,1)
          END IF

          IF (Lclmcalipso) THEN
             CALL PUTGG(o_clmcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLMC"),1,GLL,WRKS)
             CALL PKZEROS2(o_clmcalipso,IJPAK,1)

             CALL PUTGG(ocnt_clmcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLMM"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clmcalipso,IJPAK,1)
          END IF

          IF (Lcllcalipso) THEN
             CALL PUTGG(o_cllcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLLC"),1,GLL,WRKS)
             CALL PKZEROS2(o_cllcalipso,IJPAK,1)

             CALL PUTGG(ocnt_cllcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLLM"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cllcalipso,IJPAK,1)
          END IF

          IF (Lcltcalipso) THEN
             CALL PUTGG(o_cltcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLTC"),1,GLL,WRKS)
             CALL PKZEROS2(o_cltcalipso,IJPAK,1)

             CALL PUTGG(ocnt_cltcalipso,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CLTM"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cltcalipso,IJPAK,1)
          END IF

          IF (Lclhcalipsoliq) THEN
             CALL PUTGG(o_clhcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL1C"),1,GLL,WRKS)
             CALL PKZEROS2(o_clhcalipsoliq,IJPAK,1)

             CALL PUTGG(ocnt_clhcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL1M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clhcalipsoliq,IJPAK,1)
          END IF

          IF (Lclmcalipsoliq) THEN
             CALL PUTGG(o_clmcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL2C"),1,GLL,WRKS)
             CALL PKZEROS2(o_clmcalipsoliq,IJPAK,1)

             CALL PUTGG(ocnt_clmcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL2M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clmcalipsoliq,IJPAK,1)
          END IF

          IF (Lcllcalipsoliq) THEN
             CALL PUTGG(o_cllcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL3C"),1,GLL,WRKS)
             CALL PKZEROS2(o_cllcalipsoliq,IJPAK,1)

             CALL PUTGG(ocnt_cllcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL3M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cllcalipsoliq,IJPAK,1)
          END IF

          IF (Lcltcalipsoliq) THEN
             CALL PUTGG(o_cltcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL4C"),1,GLL,WRKS)
             CALL PKZEROS2(o_cltcalipsoliq,IJPAK,1)

             CALL PUTGG(ocnt_cltcalipsoliq,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CL4M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cltcalipsoliq,IJPAK,1)
          END IF

          IF (Lclhcalipsoice) THEN
             CALL PUTGG(o_clhcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI1C"),1,GLL,WRKS)
             CALL PKZEROS2(o_clhcalipsoice,IJPAK,1)

             CALL PUTGG(ocnt_clhcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI1M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clhcalipsoice,IJPAK,1)
          END IF

          IF (Lclmcalipsoice) THEN
             CALL PUTGG(o_clmcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI2C"),1,GLL,WRKS)
             CALL PKZEROS2(o_clmcalipsoice,IJPAK,1)

             CALL PUTGG(ocnt_clmcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI2M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clmcalipsoice,IJPAK,1)
          END IF

          IF (Lcllcalipsoice) THEN
             CALL PUTGG(o_cllcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI3C"),1,GLL,WRKS)
             CALL PKZEROS2(o_cllcalipsoice,IJPAK,1)

             CALL PUTGG(ocnt_cllcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI3M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cllcalipsoice,IJPAK,1)
          END IF

          IF (Lcltcalipsoice) THEN
             CALL PUTGG(o_cltcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI4C"),1,GLL,WRKS)
             CALL PKZEROS2(o_cltcalipsoice,IJPAK,1)

             CALL PUTGG(ocnt_cltcalipsoice,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CI4M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cltcalipsoice,IJPAK,1)
          END IF


          IF (Lclhcalipsoun) THEN
             CALL PUTGG(o_clhcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU1C"),1,GLL,WRKS)
             CALL PKZEROS2(o_clhcalipsoun,IJPAK,1)

             CALL PUTGG(ocnt_clhcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU1M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clhcalipsoun,IJPAK,1)
          END IF

          IF (Lclmcalipsoun) THEN
             CALL PUTGG(o_clmcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU2C"),1,GLL,WRKS)
             CALL PKZEROS2(o_clmcalipsoun,IJPAK,1)

             CALL PUTGG(ocnt_clmcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU2M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_clmcalipsoun,IJPAK,1)
          END IF

          IF (Lcllcalipsoun) THEN
             CALL PUTGG(o_cllcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU3C"),1,GLL,WRKS)
             CALL PKZEROS2(o_cllcalipsoun,IJPAK,1)

             CALL PUTGG(ocnt_cllcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU3M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cllcalipsoun,IJPAK,1)
          END IF

          IF (Lcltcalipsoun) THEN
             CALL PUTGG(o_cltcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU4C"),1,GLL,WRKS)
             CALL PKZEROS2(o_cltcalipsoun,IJPAK,1)

             CALL PUTGG(ocnt_cltcalipsoun,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CU4M"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cltcalipsoun,IJPAK,1)
          END IF

! Need to handle potential of different vertical methods
          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (Lclcalipso) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipso(1,L),LON1,ILAT,KHEM,NPGG,K,
     1                          NUPR,NC4TO8("CLCP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipso(1,L),LON1,ILAT,KHEM,NPGG
     1                          ,K,NUPR,NC4TO8("CLCM"),LLIDAR(L),GLL,
     2                          WRKS)
                END DO
                CALL PKZEROS2(o_clcalipso,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipso,IJPAK,NLR)
             END IF

             IF (Lclcalipsoliq) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipsoliq(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CLPP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsoliq(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CLPM"),LLIDAR(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsoliq,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipsoliq,IJPAK,NLR)
             END IF

             IF (Lclcalipsoice) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipsoice(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CIPP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsoice(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CIPM"),LLIDAR(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsoice,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipsoice,IJPAK,NLR)
             END IF

             IF (Lclcalipsoun) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipsoun(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CUPP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsoun(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CUPM"),LLIDAR(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsoun,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipsoun,IJPAK,NLR)
             END IF

          ELSE                  ! Output on GCM layers
             IF (Lclcalipso) THEN
                DO L=1,ILEV
                   CALL PUTGG(o_clcalipso(1,L),LON1,ILAT,KHEM,NPGG,K,
     1                           NUPR,NC4TO8("CLCP"),LLIDAR(L),GLL,WRKS)
                   CALL PUTGG(ocnt_clcalipso(1,L),LON1,ILAT,KHEM,NPGG
     1                          ,K,NUPR,NC4TO8("CLCM"),LLIDAR(L),GLL,
     2                          WRKS)
                END DO
                CALL PKZEROS2(o_clcalipso,IJPAK,ILEV)
                CALL PKZEROS2(ocnt_clcalipso,IJPAK,ILEV)
             END IF

             IF (Lclcalipsoliq) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipsoliq(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CLPP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsoliq(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CLPM"),LLIDAR(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsoliq,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipsoliq,IJPAK,NLR)
             END IF

             IF (Lclcalipsoice) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipsoice(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CIPP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsoice(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CIPM"),LLIDAR(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsoice,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipsoice,IJPAK,NLR)
             END IF

             IF (Lclcalipsoun) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipsoun(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CUPP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsoun(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CUPM"),LLIDAR(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsoun,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipsoun,IJPAK,NLR)
             END IF

          END IF

             IF (Lclcalipsotmp) THEN
                DO L=1,LIDAR_NTEMP
                   CALL PUTGG(o_clcalipsotmp(1,L),LON1,ILAT,KHEM,NPGG,
     1                          K,NUPR,NC4TO8("CT1P"),LLIDARTEMP(L),GLL,
     2                          WRKS)

                   CALL PUTGG(ocnt_clcalipsotmp(1,L),LON1,ILAT,KHEM,
     1                         NPGG,K,NUPR,NC4TO8("CT1M"),LLIDARTEMP(L),
     2                          GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsotmp,IJPAK,LIDAR_NTEMP)
                CALL PKZEROS2(ocnt_clcalipsotmp,IJPAK,LIDAR_NTEMP)
             END IF

             IF (Lclcalipsotmpliq) THEN
                DO L=1,LIDAR_NTEMP
                   CALL PUTGG(o_clcalipsotmpliq(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CT2P"),
     2                          LLIDARTEMP(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsotmpliq(1,L),LON1,ILAT,
     1                          KHEM,NPGG,K,NUPR,NC4TO8("CT2M"),
     2                          LLIDARTEMP(L),GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsotmpliq,IJPAK,LIDAR_NTEMP)
                CALL PKZEROS2(ocnt_clcalipsotmpliq,IJPAK,LIDAR_NTEMP)
             END IF

             IF (Lclcalipsotmpice) THEN
                DO L=1,LIDAR_NTEMP
                   CALL PUTGG(o_clcalipsotmpice(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CT3P"),
     2                          LLIDARTEMP(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsotmpice(1,L),LON1,ILAT,
     1                          KHEM,NPGG,K,NUPR,NC4TO8("CT3M"),
     2                          LLIDARTEMP(L),GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsotmpice,IJPAK,LIDAR_NTEMP)
                CALL PKZEROS2(ocnt_clcalipsotmpice,IJPAK,LIDAR_NTEMP)
             END IF

             IF (Lclcalipsotmpun) THEN
                DO L=1,LIDAR_NTEMP
                   CALL PUTGG(o_clcalipsotmpun(1,L),LON1,ILAT,KHEM,
     1                          NPGG,K,NUPR,NC4TO8("CT4P"),
     2                          LLIDARTEMP(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipsotmpun(1,L),LON1,ILAT,
     1                          KHEM,NPGG,K,NUPR,NC4TO8("CT4M"),
     2                          LLIDARTEMP(L),GLL,WRKS)
                END DO
                CALL PKZEROS2(o_clcalipsotmpun,IJPAK,LIDAR_NTEMP)
                CALL PKZEROS2(ocnt_clcalipsotmpun,IJPAK,LIDAR_NTEMP)
             END IF
          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (LcfadLidarsr532) THEN
                DO N = 1, SR_BINS
                   CALL NAME2(NAM,'SR',N)
                   DO L=1,NLR
                      CALL PUTGG(o_cfad_lidarsr532(1,L,N),LON1,ILAT,
     1                          KHEM,NPGG,K,NUPR,NAM,LLIDAR(L),GLL,WRKS)
                   END DO
                   CALL PKZEROS2(o_cfad_lidarsr532(1,1,N),IJPAK,NLR)
                END DO
             END IF
          ELSE                  ! Output on GCM layers
             IF (LcfadLidarsr532) THEN
                DO N = 1, SR_BINS
                   CALL NAME2(NAM,'SR',N)
                   DO L=1,ILEV
                      CALL PUTGG(o_cfad_lidarsr532(1,L,N),LON1,ILAT,
     1                          KHEM,NPGG,K,NUPR,NAM,LLIDAR(L),GLL,WRKS)
                   END DO
                   CALL PKZEROS2(o_cfad_lidarsr532(1,1,N),IJPAK,ILEV)
                END DO
             END IF
          END IF

          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (LlidarBetaMol532) THEN
                DO L=1,NLR
                   CALL PUTGG(o_beta_mol532(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("LDBS"),LLIDAR(L),GLL,WRKS)
                END DO
                CALL PKZEROS2(o_beta_mol532,IJPAK,NLR)
             END IF
          ELSE                  ! Output on GCM layers
             IF (LlidarBetaMol532) THEN
                DO L=1,ILEV
                   CALL PUTGG(o_beta_mol532(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("LDBS"),LLIDAR(L),GLL,WRKS)
                END DO
                CALL PKZEROS2(o_beta_mol532,IJPAK,ILEV)
             END IF
          END IF

          IF (Latb532) THEN
! Array too large to be saved globally
             o_atb532 = 0.0
          END IF

! CloudSat fields
          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (LcfadDbze94) THEN
                DO N = 1, DBZE_BINS
                   CALL NAME2(NAM,'ZE',N)
                   DO L=1,NLR
                      CALL PUTGG(o_cfad_dbze94(1,L,N),LON1,ILAT,KHEM,
     1                              NPGG,K,NUPR,NAM,LRADAR(L),GLL,WRKS)
                   END DO
                   CALL PKZEROS2(o_cfad_dbze94(1,1,N),IJPAK,NLR)
                END DO
             END IF
          ELSE                  ! Output on GCM layers
             IF (LcfadDbze94) THEN
                DO N = 1, DBZE_BINS
                   CALL NAME2(NAM,'ZE',N)
                   DO L=1,ILEV
                      CALL PUTGG(o_cfad_dbze94(1,L,N),LON1,ILAT,KHEM,
     1                             NPGG,K,NUPR,NAM,LRADAR(L),GLL,WRKS)
                   END DO
                   CALL PKZEROS2(o_cfad_dbze94(1,1,N),IJPAK,ILEV)
                END DO
             END IF
          END IF

          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (Ldbze94) THEN
! Array too large to save globally
                o_dbze94 = 0.0
             END IF
          ELSE                  ! Output on GCM layers
             IF (Ldbze94) THEN
! Array too large to save globally
                o_dbze94 = 0.0
             END IF
          END IF

! CloudSat+CALIPSO fields
          IF (Lcltlidarradar) THEN
             CALL PUTGG(o_cltlidarradar,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CCTC"),1,GLL,WRKS)
             CALL PKZEROS2(o_cltlidarradar,IJPAK,1)

             CALL PUTGG(ocnt_cltlidarradar,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                    NC4TO8("CCTM"),1,GLL,WRKS)
             CALL PKZEROS2(ocnt_cltlidarradar,IJPAK,1)
          END IF

          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (Lclcalipso2) THEN
                DO L=1,NLR
                   CALL PUTGG(o_clcalipso2(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CCCP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipso2(1,L),LON1,ILAT,KHEM,
     1                         NPGG,K,NUPR,NC4TO8("CCCM"),LLIDAR(L),GLL,
     2                         WRKS)
                END DO
                CALL PKZEROS2(o_clcalipso2,IJPAK,NLR)
                CALL PKZEROS2(ocnt_clcalipso2,IJPAK,NLR)
             END IF
          ELSE                  ! Output on GCM layers
             IF (Lclcalipso2) THEN
                DO L=1,ILEV
                   CALL PUTGG(o_clcalipso2(1,L),LON1,ILAT,KHEM,NPGG,
     1                         K,NUPR,NC4TO8("CCCP"),LLIDAR(L),GLL,WRKS)

                   CALL PUTGG(ocnt_clcalipso2(1,L),LON1,ILAT,KHEM,
     1                         NPGG,K,NUPR,NC4TO8("CCCM"),LLIDAR(L),GLL,
     2                         WRKS)
                END DO
                CALL PKZEROS2(o_clcalipso2,IJPAK,ILEV)
                CALL PKZEROS2(ocnt_clcalipso2,IJPAK,ILEV)
             END IF
          END IF
! COSP height mask
          IF (use_vgrid) THEN   ! Interpolate to specific heights
             IF (LcfadDbze94 .OR. LcfadLidarsr532) THEN
                DO L=1,NLR
                   CALL PUTGG(o_cosp_height_mask(1,L),LON1,ILAT,KHEM,
     1                         NPGG,K,NUPR,NC4TO8("CHMK"),LLIDAR(L),GLL,
     1                         WRKS)
                END DO
                CALL PKZEROS2(o_cosp_height_mask,IJPAK,NLR)
             END IF
          ELSE ! Output on GCM layers
             IF (LcfadDbze94 .OR. LcfadLidarsr532) THEN
                DO L=1,ILEV
                   CALL PUTGG(o_cosp_height_mask(1,L),LON1,ILAT,KHEM,
     1                         NPGG,K,NUPR,NC4TO8("CHMK"),LLIDAR(L),GLL,
     1                         WRKS)
                END DO
                CALL PKZEROS2(o_cosp_height_mask,IJPAK,ILEV)
             END IF
          ENDIF
! PARASOL
          IF (LparasolRefl) THEN
             DO N = 1, PARASOL_NREFL
                CALL NAME2(NAM,'PL',N)
                CALL PUTGG(o_parasol_refl(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NAM,1,GLL,WRKS)

                CALL NAME2(NAM,'ML',N)
                CALL PUTGG(ocnt_parasol_refl(1,N),LON1,ILAT,KHEM,NPGG
     1                       ,K,NUPR,NAM,1,GLL,WRKS)
             END DO
             CALL PKZEROS2(o_parasol_refl,IJPAK,PARASOL_NREFL)
             CALL PKZEROS2(ocnt_parasol_refl,IJPAK,PARASOL_NREFL)
          END IF

          IF (Lceres_sim) THEN
             DO N = 1, NCERES
                CALL PUTGG(o_ceres_cf(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCCF"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cnt(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCNT"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_ctp(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCTP"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_tau(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCCT"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_lntau(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCLT"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cfl(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCFL"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cfi(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCFI"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_lwp(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CLWP"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_iwp(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CIWP"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cntl(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CNLC"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cnti(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CNIC"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_rel(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CREL"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cdnc(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCDD"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_clm(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CCFM"),N,GLL,WRKS)

                CALL PUTGG(o_ceres_cntlm(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CNLM"),N,GLL,WRKS)
             END DO

             CALL PKZEROS2(o_ceres_cf,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cnt,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_ctp,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_tau,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_lntau,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_lwp,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_iwp,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cfl,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cfi,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cntl,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cnti,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_rel,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cdnc,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_clm,IJPAK,NCERES)
             CALL PKZEROS2(o_ceres_cntlm,IJPAK,NCERES)

          END IF

          IF (Lceres_sim .AND. Lswath) THEN
             DO N = 1, NCERES
                CALL PUTGG(os_ceres_cf(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS01"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cnt(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS02"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_ctp(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS03"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_tau(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS04"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_lntau(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS05"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cfl(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS06"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cfi(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS07"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_lwp(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS08"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_iwp(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS09"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cntl(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS10"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cnti(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS11"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_rel(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS12"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cdnc(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS13"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_clm(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS14"),N,GLL,WRKS)

                CALL PUTGG(os_ceres_cntlm(1,N),LON1,ILAT,KHEM,NPGG,K,
     1                       NUPR,NC4TO8("CS15"),N,GLL,WRKS)

             END DO

             CALL PKZEROS2(os_ceres_cf,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cnt,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_ctp,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_tau,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_lntau,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_lwp,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_iwp,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cfl,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cfi,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cntl,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cnti,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_rel,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cdnc,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_clm,IJPAK,NCERES)
             CALL PKZEROS2(os_ceres_cntlm,IJPAK,NCERES)

          END IF

! MISR
          IF (LclMISR) THEN
             CALL PUTGG(o_MISR_cldarea,LON1,ILAT,KHEM,NPGG,K,
     1                    NUPR,NC4TO8("MSTC"),1,GLL,WRKS)
             CALL PUTGG(o_MISR_mean_ztop,LON1,ILAT,KHEM,NPGG,K,
     1                   NUPR,NC4TO8("MSMZ"),1,GLL,WRKS)

             CALL PKZEROS2(o_MISR_cldarea,IJPAK,1)
             CALL PKZEROS2(o_MISR_mean_ztop,IJPAK,1)

             NBOX = 16
             DO IBOX = 1, NBOX
             CALL PUTGG(o_dist_model_layertops(1,IBOX),LON1,ILAT,KHEM,
     1                    NPGG,K,NUPR,NC4TO8("MSDT"),IBOX,GLL,WRKS)
             END DO ! IBOX
             CALL PKZEROS2(o_dist_model_layertops,IJPAK,NBOX)

             NBOX = 7*16
             DO IBOX = 1, NBOX
                CALL PUTGG(o_fq_MISR_TAU_v_CTH(1,IBOX),LON1,ILAT,KHEM,
     1                       NPGG,K,NUPR,NC4TO8("MSTZ"),IBOX,GLL,WRKS)
             END DO ! IBOX
             CALL PKZEROS2(o_fq_MISR_TAU_v_CTH,IJPAK,NBOX)

          END IF ! LclMISR

! MODIS
          IF (Lcltmodis) THEN
          CALL PUTGG(o_MODIS_Cloud_Fraction_Total_Mean,LON1,ILAT,KHEM,
     1                 NPGG,K,NUPR,NC4TO8("MDTC"),1,GLL,WRKS)
          CALL PKZEROS2(o_MODIS_Cloud_Fraction_Total_Mean,IJPAK,1)
          END IF

          IF (Lclwmodis) THEN
          CALL PUTGG(o_MODIS_Cloud_Fraction_Water_Mean,LON1,ILAT,KHEM,
     1                 NPGG,K,NUPR,NC4TO8("MDWC"),1,GLL,WRKS)
          CALL PKZEROS2(o_MODIS_Cloud_Fraction_Water_Mean,IJPAK,1)
          END IF

          IF (Lclimodis) THEN
            CALL PUTGG(o_MODIS_Cloud_Fraction_Ice_Mean,LON1,ILAT,KHEM,
     1                  NPGG,K,NUPR,NC4TO8("MDIC"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Cloud_Fraction_Ice_Mean,IJPAK,1)
          END IF

          IF (Lclhmodis) THEN
           CALL PUTGG(o_MODIS_Cloud_Fraction_High_Mean,LON1,ILAT,KHEM,
     1                 NPGG,K,NUPR,NC4TO8("MDHC"),1,GLL,WRKS)
           CALL PKZEROS2(o_MODIS_Cloud_Fraction_High_Mean,IJPAK,1)
          END IF

          IF (Lclmmodis) THEN
            CALL PUTGG(o_MODIS_Cloud_Fraction_Mid_Mean,LON1,ILAT,KHEM,
     1                   NPGG,K,NUPR,NC4TO8("MDMC"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Cloud_Fraction_Mid_Mean,IJPAK,1)
          END IF

          IF (Lcllmodis) THEN
            CALL PUTGG(o_MODIS_Cloud_Fraction_Low_Mean,LON1,ILAT,KHEM,
     1                   NPGG,K,NUPR,NC4TO8("MDLC"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Cloud_Fraction_Low_Mean,IJPAK,1)
          END IF

          IF (Ltautmodis) THEN
            CALL PUTGG(o_MODIS_Optical_Thickness_Total_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MDTT"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Optical_Thickness_Total_Mean,IJPAK,1)
          END IF

          IF (Ltauwmodis) THEN
            CALL PUTGG(o_MODIS_Optical_Thickness_Water_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MDWT"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Optical_Thickness_Water_Mean,IJPAK,1)
          END IF

          IF (Ltauimodis) THEN
             CALL PUTGG(o_MODIS_Optical_Thickness_Ice_Mean,LON1,ILAT,
     1                    KHEM,NPGG,K,NUPR,NC4TO8("MDIT"),1,GLL,WRKS)
             CALL PKZEROS2(o_MODIS_Optical_Thickness_Ice_Mean,IJPAK,1)
          END IF

          IF (Ltautlogmodis) THEN
          CALL PUTGG(o_MODIS_Optical_Thickness_Total_LogMean,LON1,
     1                 ILAT,KHEM,NPGG,K,NUPR,NC4TO8("MDGT"),1,GLL,WRKS)
          CALL PKZEROS2(o_MODIS_Optical_Thickness_Total_LogMean,IJPAK,1)
          END IF

          IF (Ltauwlogmodis) THEN
          CALL PUTGG(o_MODIS_Optical_Thickness_Water_LogMean,LON1,
     1                ILAT,KHEM,NPGG,K,NUPR,NC4TO8("MDGW"),1,GLL,WRKS)
          CALL PKZEROS2(o_MODIS_Optical_Thickness_Water_LogMean,IJPAK,1)
          END IF

          IF (Ltauilogmodis) THEN
           CALL PUTGG(o_MODIS_Optical_Thickness_Ice_LogMean,LON1,ILAT,
     1                  KHEM,NPGG,K,NUPR,NC4TO8("MDGI"),1,GLL,WRKS)
           CALL PKZEROS2(o_MODIS_Optical_Thickness_Ice_LogMean,IJPAK,1)
          END IF

          IF (Lreffclwmodis) THEN
          CALL PUTGG(o_MODIS_Cloud_Particle_Size_Water_Mean,LON1,ILAT,
     1                 KHEM,NPGG,K,NUPR,NC4TO8("MDRE"),1,GLL,WRKS)
          CALL PKZEROS2(o_MODIS_Cloud_Particle_Size_Water_Mean,IJPAK,1)
          END IF

          IF (Lreffclimodis) THEN
            CALL PUTGG(o_MODIS_Cloud_Particle_Size_Ice_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MDRI"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Cloud_Particle_Size_Ice_Mean,IJPAK,1)
          END IF

          IF (Lpctmodis) THEN
           CALL PUTGG(o_MODIS_Cloud_Top_Pressure_Total_Mean,LON1,ILAT,
     1                  KHEM,NPGG,K,NUPR,NC4TO8("MDPT"),1,GLL,WRKS)
           CALL PKZEROS2(o_MODIS_Cloud_Top_Pressure_Total_Mean,IJPAK,1)
          END IF

          IF (Llwpmodis) THEN
            CALL PUTGG(o_MODIS_Liquid_Water_Path_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MDWP"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Liquid_Water_Path_Mean,IJPAK,1)
          END IF

          IF (Liwpmodis) THEN
            CALL PUTGG(o_MODIS_Ice_Water_Path_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MDIP"),1,GLL,WRKS)
            CALL PKZEROS2(o_MODIS_Ice_Water_Path_Mean,IJPAK,1)
          END IF

          IF (Llcdncmodis) THEN
            CALL PUTGG(o_MODIS_Liq_CDNC_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MLN1"),1,GLL,WRKS)
            CALL PUTGG(o_MODIS_Liq_CDNC_GCM_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MLN2"),1,GLL,WRKS)
            CALL PUTGG(o_MODIS_Cloud_Fraction_Warm_Mean,LON1,ILAT,
     1                   KHEM,NPGG,K,NUPR,NC4TO8("MDLQ"),1,GLL,WRKS)

            CALL PKZEROS2(o_MODIS_Liq_CDNC_Mean,IJPAK,1)
            CALL PKZEROS2(o_MODIS_Liq_CDNC_GCM_Mean,IJPAK,1)
            CALL PKZEROS2(o_MODIS_Cloud_Fraction_Warm_Mean,IJPAK,1)
          END IF

          IF (Lclmodis) THEN
           NBOX = (a_numModisTauBins+1)*a_numModisPressureBins
           DO IBOX = 1, NBOX
           CALL PUTGG(o_MODIS_Optical_Thickness_vs_Cloud_Top_Pressure(
     1                                                           1,IBOX)
     2         ,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("MDTP"),IBOX,GLL,WRKS)
           END DO ! IBOX
           CALL PKZEROS2(o_MODIS_Optical_Thickness_vs_Cloud_Top_Pressure
     1                   ,IJPAK,NBOX)
          END IF

          IF (Lcrimodis) THEN
           NBOX = (a_numModisTauBins+1)*a_numModisReffIceBins
           DO IBOX = 1, NBOX
           CALL PUTGG(o_MODIS_Optical_Thickness_vs_reffICE(1,IBOX),
     2          LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("MDTI"),IBOX,GLL,WRKS)
           END DO ! IBOX
           CALL PKZEROS2(o_MODIS_Optical_Thickness_vs_reffICE,
     1                   IJPAK,NBOX)
          END IF

          IF (Lcrlmodis) THEN
           NBOX = (a_numModisTauBins+1)*a_numModisReffLiqBins
           DO IBOX = 1, NBOX
           CALL PUTGG(o_MODIS_Optical_Thickness_vs_reffLiq(1,IBOX),
     2          LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("MDTL"),IBOX,GLL,WRKS)
           END DO ! IBOX
           CALL PKZEROS2(o_MODIS_Optical_Thickness_vs_reffLiq,
     1                   IJPAK,NBOX)
          END IF

#endif
#if defined (aodpth)
C
          CALL PUTGG(EXB1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXB1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXB2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXB2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXB3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXB3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXB4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXB4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXB5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXB5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXBTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXBT")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODB1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODB1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODB2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODB2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODB3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODB3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODB4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODB4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODB5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODB5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODBTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODBT")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODBVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODBV")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFB1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFB1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFB2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFB2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFB3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFB3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFB4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFB4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFB5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFB5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFBTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFBT")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABB1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABB1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABB2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABB2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABB3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABB3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABB4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABB4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABB5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABB5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABBTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABBT")
     1                         ,1,GLL,WRKS)
C
          CALL PUTGG(EXS1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXS1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXS2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXS2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXS3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXS3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXS4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXS4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXS5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXS5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(EXSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("EXST")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODS1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODS1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODS2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODS2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODS3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODS3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODS4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODS4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODS5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODS5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODST")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ODSVPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ODSV")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFS1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFS1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFS2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFS2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFS3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFS3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFS4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFS4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFS5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFS5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(OFSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("OFST")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABS1PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABS1")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABS2PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABS2")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABS3PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABS3")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABS4PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABS4")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABS5PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABS5")
     1                         ,1,GLL,WRKS)
          CALL PUTGG(ABSTPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("ABST")
     1                         ,1,GLL,WRKS)
C
          CALL PKZEROS2(EXB1PAK,IJPAK,   1)
          CALL PKZEROS2(EXB2PAK,IJPAK,   1)
          CALL PKZEROS2(EXB3PAK,IJPAK,   1)
          CALL PKZEROS2(EXB4PAK,IJPAK,   1)
          CALL PKZEROS2(EXB5PAK,IJPAK,   1)
          CALL PKZEROS2(EXBTPAK,IJPAK,   1)
          CALL PKZEROS2(ODB1PAK,IJPAK,   1)
          CALL PKZEROS2(ODB2PAK,IJPAK,   1)
          CALL PKZEROS2(ODB3PAK,IJPAK,   1)
          CALL PKZEROS2(ODB4PAK,IJPAK,   1)
          CALL PKZEROS2(ODB5PAK,IJPAK,   1)
          CALL PKZEROS2(ODBTPAK,IJPAK,   1)
          CALL PKZEROS2(ODBVPAK,IJPAK,   1)
          CALL PKZEROS2(OFB1PAK,IJPAK,   1)
          CALL PKZEROS2(OFB2PAK,IJPAK,   1)
          CALL PKZEROS2(OFB3PAK,IJPAK,   1)
          CALL PKZEROS2(OFB4PAK,IJPAK,   1)
          CALL PKZEROS2(OFB5PAK,IJPAK,   1)
          CALL PKZEROS2(OFBTPAK,IJPAK,   1)
          CALL PKZEROS2(ABB1PAK,IJPAK,   1)
          CALL PKZEROS2(ABB2PAK,IJPAK,   1)
          CALL PKZEROS2(ABB3PAK,IJPAK,   1)
          CALL PKZEROS2(ABB4PAK,IJPAK,   1)
          CALL PKZEROS2(ABB5PAK,IJPAK,   1)
          CALL PKZEROS2(ABBTPAK,IJPAK,   1)
C
          CALL PKZEROS2(EXS1PAK,IJPAK,   1)
          CALL PKZEROS2(EXS2PAK,IJPAK,   1)
          CALL PKZEROS2(EXS3PAK,IJPAK,   1)
          CALL PKZEROS2(EXS4PAK,IJPAK,   1)
          CALL PKZEROS2(EXS5PAK,IJPAK,   1)
          CALL PKZEROS2(EXSTPAK,IJPAK,   1)
          CALL PKZEROS2(ODS1PAK,IJPAK,   1)
          CALL PKZEROS2(ODS2PAK,IJPAK,   1)
          CALL PKZEROS2(ODS3PAK,IJPAK,   1)
          CALL PKZEROS2(ODS4PAK,IJPAK,   1)
          CALL PKZEROS2(ODS5PAK,IJPAK,   1)
          CALL PKZEROS2(ODSTPAK,IJPAK,   1)
          CALL PKZEROS2(ODSVPAK,IJPAK,   1)
          CALL PKZEROS2(OFS1PAK,IJPAK,   1)
          CALL PKZEROS2(OFS2PAK,IJPAK,   1)
          CALL PKZEROS2(OFS3PAK,IJPAK,   1)
          CALL PKZEROS2(OFS4PAK,IJPAK,   1)
          CALL PKZEROS2(OFS5PAK,IJPAK,   1)
          CALL PKZEROS2(OFSTPAK,IJPAK,   1)
          CALL PKZEROS2(ABS1PAK,IJPAK,   1)
          CALL PKZEROS2(ABS2PAK,IJPAK,   1)
          CALL PKZEROS2(ABS3PAK,IJPAK,   1)
          CALL PKZEROS2(ABS4PAK,IJPAK,   1)
          CALL PKZEROS2(ABS5PAK,IJPAK,   1)
          CALL PKZEROS2(ABSTPAK,IJPAK,   1)
#endif
#if defined (explvol)
      IF (IVTAU .EQ. 2) THEN
         CALL PUTGG( W055_VTAU_SA_PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("W055"),1,GLL,WRKS)
         CALL PUTGG( W110_VTAU_SA_PAK,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("W110"),1,GLL,WRKS)

         CALL PKZEROS2( W055_VTAU_SA_PAK,IJPAK,   1)
         CALL PKZEROS2( W110_VTAU_SA_PAK,IJPAK,   1)

          DO L=1,ILEV
             CALL PUTGG(W055_EXT_GCM_SA_PAK(1,L),LON1,ILAT,KHEM,NPGG,
     1                    K,NUPR,NC4TO8("EV55"),LH(L),GLL,WRKS)
             CALL PUTGG(W110_EXT_GCM_SA_PAK(1,L),LON1,ILAT,KHEM,NPGG,
     1                    K,NUPR,NC4TO8("EV11"),LH(L),GLL,WRKS)
          ENDDO
          CALL PKZEROS2(W055_EXT_GCM_SA_PAK,IJPAK,ILEV)
	  CALL PKZEROS2(W110_EXT_GCM_SA_PAK,IJPAK,ILEV)
      END IF ! IVTAU
#endif
      ENDIF
C
C     * SAVE GWD DIAGNOSTIC FIELDS UNDER CONTROL OF SWITCH.
C
      IF (LSINST_EXTRAGWD) THEN
        KK = KOUNT_INST_EXTRAGWD
        DO L=1,ILEV
          CALL PUTGG( FTOXPAK(1,L),LON1,ILAT,KHEM,NPGG,KK,NUPR,
     1                                 NC4TO8("FTOX"),LHB(L),GLL,WRKS)
        ENDDO
        DO L=1,ILEV
          CALL PUTGG( FTOYPAK(1,L),LON1,ILAT,KHEM,NPGG,KK,NUPR,
     1                                 NC4TO8("FTOY"),LHB(L),GLL,WRKS)
        ENDDO
        DO L=1,ILEV
          CALL PUTGG( TDOXPAK(1,L),LON1,ILAT,KHEM,NPGG,KK,NUPR,
     1                                 NC4TO8("TDOX"),LH(L),GLL,WRKS)
        ENDDO
        DO L=1,ILEV
          CALL PUTGG( TDOYPAK(1,L),LON1,ILAT,KHEM,NPGG,KK,NUPR,
     1                                 NC4TO8("TDOY"),LH(L),GLL,WRKS)
        ENDDO
      ENDIF

#if defined rad_flux_profs
C     * SAVE LW/SW ALL-SKY/CLEAR-SKY UP/DOWN RADIATIVE FLUXES AT EVERY MODEL
C     * LEVEL, EVERY ISRAD STEPS THEN RESET ACCUMULATED FIELD TO ZERO
      IF (LSRAD) THEN
       K = KOUNT

        DO L=1,ILEV+2

          !--- Upward all-sky shortwave flux
          CALL PUTGG(FSAUPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FSAU"),LR(L),GLL,WRKS)
          !--- Downward all-sky shortwave flux
          CALL PUTGG(FSADPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FSAD"),LR(L),GLL,WRKS)
          !--- Upward all-sky longwave flux
          CALL PUTGG(FLAUPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FLAU"),LR(L),GLL,WRKS)
          !--- Downward all-sky longwave flux
          CALL PUTGG(FLADPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FLAD"),LR(L),GLL,WRKS)


          !--- Upward clear-sky shortwave flux
          CALL PUTGG(FSCUPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FSCU"),LR(L),GLL,WRKS)
          !--- Downward clear-sky shortwave flux
          CALL PUTGG(FSCDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FSCD"),LR(L),GLL,WRKS)
          !--- Upward clear-sky longwave flux
          CALL PUTGG(FLCUPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FLCU"),LR(L),GLL,WRKS)
          !--- Downward clear-sky longwave flux
          CALL PUTGG(FLCDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                          NC4TO8("FLCD"),LR(L),GLL,WRKS)

       END DO ! L

       CALL PKZEROS2(FSAUPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FSADPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FLAUPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FLADPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FSCUPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FSCDPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FLCUPAK,IJPAK,ILEV+2)
       CALL PKZEROS2(FLCDPAK,IJPAK,ILEV+2)

      ENDIF ! LSRAD
#endif

#if defined radforce
C
C     * SAVE LW/SW ALL-SKY/CLEAR-SKY RADIATIVE FLUXES AT EVERY MODEL
C     * LEVEL, EVERY ISRAD STEPS THEN RESET ACCUMULATED FIELD TO ZERO

      IF (LSRAD) THEN
        K = KOUNT

C
C.......Save perturbation temperature and fluxes from the diagnostics radiation calls
C
        DO NRF=1,NRFP

          WRITE(RFP_ID,'(I2.2)') NRF

c.........T_* - T_o at all model levels
          DO L=1,ILEV
            !--- T_* - T_o
            RFNAME="DT"//RFP_ID(1:2)
            CALL PUTGG(RDTPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                   K,NUPR,NC4TO8(RFNAME(1:4)),LH(L),GLL,WRKS)
          ENDDO

          WRITE(RFP_ID,'(I1.1)') NRF

          DO L=1,ILEV+2

            !--- Upward all-sky shortwave flux
            RFNAME="SAU"//RFP_ID(1:1)
            CALL PUTGG(FSAUPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)

            !--- Downward all-sky shortwave flux
            RFNAME="SAD"//RFP_ID(1:1)
            CALL PUTGG(FSADPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)

            !--- Upward all-sky longwave flux
            RFNAME="LAU"//RFP_ID(1:1)
            CALL PUTGG(FLAUPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)

            !--- Downward all-sky longwave flux
            RFNAME="LAD"//RFP_ID(1:1)
            CALL PUTGG(FLADPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)


            !--- Upward clear-sky shortwave flux
            RFNAME="SCU"//RFP_ID(1:1)
            CALL PUTGG(FSCUPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)

            !--- Downward clear-sky shortwave flux
            RFNAME="SCD"//RFP_ID(1:1)
            CALL PUTGG(FSCDPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)

            !--- Upward clear-sky longwave flux
            RFNAME="LCU"//RFP_ID(1:1)
            CALL PUTGG(FLCUPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)

            !--- Downward clear-sky longwave flux
            RFNAME="LCD"//RFP_ID(1:1)
            CALL PUTGG(FLCDPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,
     1                 K,NUPR,NC4TO8(RFNAME(1:4)),LR(L),GLL,WRKS)
         END DO ! L

         RDTPAM_R(:,:,NRF) = RDTPAK_R(:,:,NRF)
         CALL PKZEROS2(RDTPAK_R(1,1,NRF),IJPAK,ILEV)
         CALL PKZEROS2(FSAUPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FSADPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FLAUPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FLADPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FSCUPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FSCDPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FLCUPAK_R(1,1,NRF),IJPAK,ILEV+2)
         CALL PKZEROS2(FLCDPAK_R(1,1,NRF),IJPAK,ILEV+2)
       END DO ! NRF

      ENDIF
#endif
#if defined tprhs

C     * SAVE PHYSICS TEMPERATURE TENDENCY EVERY ISRAD STEPS AND
C     * RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 565 L=1,ILEV
              CALL PUTGG(TTPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8(" TTP"),LH(L),GLL,WRKS)
  565     CONTINUE
          CALL PKZEROS2( TTPPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined qprhs

C     * SAVE PHYSICS MOISTURE TENDENCY EVERY ISRAD STEPS AND
C     * RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 566 L=1,ILEV
              CALL PUTGG(QTPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8(" QTP"),LH(L),GLL,WRKS)
  566     CONTINUE
          CALL PKZEROS2( QTPPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined uprhs

C     * SAVE PHYSICS U-WIND TENDENCY EVERY ISRAD STEPS AND
C     * RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 567 L=1,ILEV
              CALL PUTGG(UTPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8(" UTP"),LS(L),GLL,WRKS)
  567     CONTINUE
          CALL PKZEROS2( UTPPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined vprhs

C     * SAVE PHYSICS V-WIND TENDENCY EVERY ISRAD STEPS AND
C     * RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 568 L=1,ILEV
              CALL PUTGG(VTPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8(" VTP"),LS(L),GLL,WRKS)
  568     CONTINUE
          CALL PKZEROS2( VTPPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined qconsav

C     * SAVE PHYSICS MOISTURE CORRECTION TENDENCY EVERY ISRAD STEPS AND
C     * RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          CALL PUTGG(QADDPAK(1,1),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                        NC4TO8("QADD"),1,GLL,WRKS)
          CALL PUTGG(QADDPAK(1,2),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                        NC4TO8("QADD"),2,GLL,WRKS)
          CALL PUTGG(QTPHPAK(1,1),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                        NC4TO8("QTPH"),1,GLL,WRKS)
          CALL PUTGG(QTPHPAK(1,2),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                        NC4TO8("QTPH"),2,GLL,WRKS)
          DO 569 L=1,ILEV
              CALL PUTGG(QTPFPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("QTPF"),LH(L),GLL,WRKS)
  569     CONTINUE
          CALL PKZEROS2(QADDPAK,IJPAK,   2)
          CALL PKZEROS2(QTPHPAK,IJPAK,   2)
          CALL PKZEROS2(QTPFPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined tprhsc

C     * SAVE PHYSICS COMPONENT TEMPERATURE TENDENCIES EVERY ISRAD STEPS
C     * AND RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 570 L=1,ILEV
              CALL PUTGG(TTPVPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPV"),LH(L),GLL,WRKS)
              CALL PUTGG(TTPMPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPM"),LH(L),GLL,WRKS)
              CALL PUTGG(TTPCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPC"),LH(L),GLL,WRKS)
              CALL PUTGG(TTPPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPP"),LH(L),GLL,WRKS)
              CALL PUTGG(TTPKPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPK"),LH(L),GLL,WRKS)
              CALL PUTGG(TTPNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPN"),LH(L),GLL,WRKS)
  570     CONTINUE

          CALL PKZEROS2(TTPVPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTPMPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTPCPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTPPPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTPKPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTPNPAK,IJPAK,ILEV)
       END IF
#endif
#if (defined (tprhsc) || defined (radforce))
      IF (LSRAD)                                                 THEN
          K = KOUNT

          DO L=1,ILEV
              CALL PUTGG(TTPSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPS"),LH(L),GLL,WRKS)
              CALL PUTGG(TTPLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTPL"),LH(L),GLL,WRKS)
              CALL PUTGG(TTSCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTSC"),LH(L),GLL,WRKS)
              CALL PUTGG(TTLCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("TTLC"),LH(L),GLL,WRKS)
          END DO ! L

          CALL PKZEROS2(TTPSPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTPLPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTSCPAK,IJPAK,ILEV)
          CALL PKZEROS2(TTSCPAK,IJPAK,ILEV)
       END IF
#endif

#if defined (radforce)
C     * SAVE THE HEATING RATE DIAGNOSTIC CALCULATIONS WHICH WILL ALLOW ATTRIBUTION
C     * OF HEATING RATES TO DIFFERENT COMPONENTS.
C     * SINCE IT IS REQUESTED FOR VOLMIP ALSO SAVE THE FLUXES AT THE SURFACE AND
C     * TOA.
       IF (LSGG)                                                 THEN
          K = KOUNT

C     * THE UNPERTURBED HEATING RATES AND FLUX CALCULATIONS ARE SAVED HERE.
C     * THIS IS A BIT WASTEFUL SINCE THE HEATING RATE IS SAVED TO THE FILE AT
C     * LSGG FREQUENCY.
C     * BUT THIS IS REQUIRED SINCE THEY NEED TO BE SAMPLED AT EXACTLY THE SAME
C     * FREQUENCY FOR THE INSTANTANEOUS CALCULATIONS.

           DO L=1,ILEV

            CALL PUTGG( HRLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HRLR"),LH(L),GLL,WRKS)
            CALL PUTGG( HRSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HRSR"),LH(L),GLL,WRKS)
            CALL PUTGG(HRLCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HLCR"),LH(L),GLL,WRKS)
            CALL PUTGG(HRSCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HSCR"),LH(L),GLL,WRKS)

          END DO ! L

          CALL PUTGG(FSRPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FSRR"),1,GLL,WRKS)
          CALL PUTGG(FSRCPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FRCR"),1,GLL,WRKS)
          CALL PUTGG(FSOPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FSOR"),1,GLL,WRKS)
          CALL PUTGG(OLRPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("OLRR"),1,GLL,WRKS)
          CALL PUTGG(OLRCPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("OLCR"),1,GLL,WRKS)
          CALL PUTGG(FSLOPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FSLR"),1,GLL,WRKS)
          CALL PUTGG(FSGPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FSGR"),1,GLL,WRKS)
          CALL PUTGG(CSBPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FCSR"),1,GLL,WRKS)
          CALL PUTGG(FLGPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FLGR"),1,GLL,WRKS)
          CALL PUTGG(CLBPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FCLR"),1,GLL,WRKS)
          CALL PUTGG(FSSPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FSSR"),1,GLL,WRKS)
          CALL PUTGG(FDLPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FDLR"),1,GLL,WRKS)
          CALL PUTGG(FSSCPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FSCR"),1,GLL,WRKS)
          CALL PUTGG(FDLCPAL,LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                 NC4TO8("FLCR"),1,GLL,WRKS)

          DO NRF=1,NRFP

           WRITE(RFP_ID,'(I1.1)') NRF

           DO L=1,ILEV

            RFNAME="HRS"//RFP_ID(1:1)
            CALL PUTGG(HRSPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NC4TO8(RFNAME(1:4)),LH(L),GLL,WRKS)

            RFNAME="HRL"//RFP_ID(1:1)
            CALL PUTGG(HRLPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NC4TO8(RFNAME(1:4)),LH(L),GLL,WRKS)

            RFNAME="HSC"//RFP_ID(1:1)
            CALL PUTGG(HRSCPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NC4TO8(RFNAME(1:4)),LH(L),GLL,WRKS)

            RFNAME="HLC"//RFP_ID(1:1)
            CALL PUTGG(HRLCPAK_R(1,L,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                   NC4TO8(RFNAME(1:4)),LH(L),GLL,WRKS)

         ENDDO ! L

         RFNAME="FSR"//RFP_ID(1:1)
         CALL PUTGG(FSRPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FRC"//RFP_ID(1:1)
         CALL PUTGG(FSRCPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FSO"//RFP_ID(1:1)
         CALL PUTGG(FSOPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="OLR"//RFP_ID(1:1)
         CALL PUTGG(OLRPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="OLC"//RFP_ID(1:1)
         CALL PUTGG(OLRCPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FSL"//RFP_ID(1:1)
         CALL PUTGG(FSLOPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)

         RFNAME="FSG"//RFP_ID(1:1)
         CALL PUTGG(FSGPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FCS"//RFP_ID(1:1)
         CALL PUTGG(CSBPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FLG"//RFP_ID(1:1)
         CALL PUTGG(FLGPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FCL"//RFP_ID(1:1)
         CALL PUTGG(CLBPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)

         RFNAME="FSS"//RFP_ID(1:1)
         CALL PUTGG(FSSPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FDL"//RFP_ID(1:1)
         CALL PUTGG(FDLPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FSC"//RFP_ID(1:1)
         CALL PUTGG(FSSCPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
         RFNAME="FLC"//RFP_ID(1:1)
         CALL PUTGG(FDLCPAL_R(1,NRF),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                NC4TO8(RFNAME(1:4)),1,GLL,WRKS)
       ENDDO ! NRFP

       HRSPAK_R(:,:,:)  = 0.0
       HRLPAK_R(:,:,:)  = 0.0
       HRSCPAK_R(:,:,:) = 0.0
       HRLCPAK_R(:,:,:) = 0.0

      ENDIF
#endif

#if defined qprhsc

C     * SAVE PHYSICS COMPONENT SPEC. HUM. TENDENCIES EVERY ISRAD STEPS
C     * AND RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 571 L=1,ILEV
              CALL PUTGG(QTPVPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("QTPV"),LH(L),GLL,WRKS)
              CALL PUTGG(QTPMPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("QTPM"),LH(L),GLL,WRKS)
              CALL PUTGG(QTPCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("QTPC"),LH(L),GLL,WRKS)
              CALL PUTGG(QTPPPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("QTPP"),LH(L),GLL,WRKS)
  571     CONTINUE
          CALL PKZEROS2(QTPVPAK,IJPAK,ILEV)
          CALL PKZEROS2(QTPMPAK,IJPAK,ILEV)
          CALL PKZEROS2(QTPCPAK,IJPAK,ILEV)
          CALL PKZEROS2(QTPPPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined uprhsc

C     * SAVE PHYSICS COMPONENT U-WIND TENDENCIES EVERY ISRAD STEPS
C     * AND RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 572 L=1,ILEV
              CALL PUTGG(UTPVPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("UTPV"),LH(L),GLL,WRKS)
              CALL PUTGG(UTPGPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("UTPG"),LH(L),GLL,WRKS)
              CALL PUTGG(UTPCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("UTPC"),LH(L),GLL,WRKS)
              CALL PUTGG(UTPSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("UTPS"),LH(L),GLL,WRKS)
              CALL PUTGG(UTPNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("UTPN"),LH(L),GLL,WRKS)
  572     CONTINUE
          CALL PKZEROS2(UTPVPAK,IJPAK,ILEV)
          CALL PKZEROS2(UTPGPAK,IJPAK,ILEV)
          CALL PKZEROS2(UTPCPAK,IJPAK,ILEV)
          CALL PKZEROS2(UTPSPAK,IJPAK,ILEV)
          CALL PKZEROS2(UTPNPAK,IJPAK,ILEV)
      ENDIF
#endif
#if defined vprhsc

C     * SAVE PHYSICS COMPONENT V-WIND TENDENCIES EVERY ISRAD STEPS
C     * AND RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

      IF (LSRAD)                                                 THEN
          K = KOUNT
          DO 573 L=1,ILEV
              CALL PUTGG(VTPVPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("VTPV"),LH(L),GLL,WRKS)
              CALL PUTGG(VTPGPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("VTPG"),LH(L),GLL,WRKS)
              CALL PUTGG(VTPCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("VTPC"),LH(L),GLL,WRKS)
              CALL PUTGG(VTPSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("VTPS"),LH(L),GLL,WRKS)
              CALL PUTGG(VTPNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                                NC4TO8("VTPN"),LH(L),GLL,WRKS)
  573     CONTINUE
          CALL PKZEROS2(VTPVPAK,IJPAK,ILEV)
          CALL PKZEROS2(VTPGPAK,IJPAK,ILEV)
          CALL PKZEROS2(VTPCPAK,IJPAK,ILEV)
          CALL PKZEROS2(VTPSPAK,IJPAK,ILEV)
          CALL PKZEROS2(VTPNPAK,IJPAK,ILEV)
      ENDIF
#endif

C       * SAVE PHYSICS COMPONENT TRACER TENDENCIES EVERY ISRAD STEPS
C       * AND RESET ACCUMULATED FIELD TO ZERO AFTER SAVING.

        IF (LSRAD)                                                THEN
          K = KOUNT
          DO 575 N=1,NTRAC
#if defined xconsav

              CALL NAME2(NAM,'PA',N)
              CALL PUTGG(XADDPAK(1,1,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,1,GLL,WRKS)
              CALL PUTGG(XADDPAK(1,2,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,2,GLL,WRKS)

              CALL NAME2(NAM,'PH',N)
              CALL PUTGG(XTPHPAK(1,1,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,1,GLL,WRKS)
              CALL PUTGG(XTPHPAK(1,2,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,2,GLL,WRKS)
#endif
            DO 574 L=1,ILEV
#if defined xprhs

              CALL NAME2(NAM,'PT',N)
              CALL PUTGG(XTPPAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined xconsav

              CALL NAME2(NAM,'PF',N)
              CALL PUTGG(XTPFPAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined xprhsc

              CALL NAME2(NAM,'PM',N)
              CALL PUTGG(XTPMPAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)

              CALL NAME2(NAM,'PC',N)
              CALL PUTGG(XTPCPAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)

              CALL NAME2(NAM,'PP',N)
              CALL PUTGG(XTPPPAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)

              CALL NAME2(NAM,'PV',N)
              CALL PUTGG(XTPVPAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x01

              CALL NAME2(NAM,'X1',N)
              CALL PUTGG(X01PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x02

              CALL NAME2(NAM,'X2',N)
              CALL PUTGG(X02PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x03

              CALL NAME2(NAM,'X3',N)
              CALL PUTGG(X03PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x04

              CALL NAME2(NAM,'X4',N)
              CALL PUTGG(X04PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x05

              CALL NAME2(NAM,'X5',N)
              CALL PUTGG(X05PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x06

              CALL NAME2(NAM,'X6',N)
              CALL PUTGG(X06PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x07

              CALL NAME2(NAM,'X7',N)
              CALL PUTGG(X07PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x08

              CALL NAME2(NAM,'X8',N)
              CALL PUTGG(X08PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
#if defined x09

              CALL NAME2(NAM,'X9',N)
              CALL PUTGG(X09PAK(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUTD,
     1                     NAM,LH(L),GLL,WRKS)
#endif
  574       CONTINUE

#if defined xprhs
            CALL PKZEROS2(XTPPAK(1,1,N),IJPAK,ILEV)
#endif
#if defined xconsav
            CALL PKZEROS2(XADDPAK(1,1,N),IJPAK,   2)
            CALL PKZEROS2(XTPHPAK(1,1,N),IJPAK,   2)
            CALL PKZEROS2(XTPFPAK(1,1,N),IJPAK,ILEV)
#endif
#if defined xprhsc
            CALL PKZEROS2(XTPVPAK(1,1,N),IJPAK,ILEV)
            CALL PKZEROS2(XTPMPAK(1,1,N),IJPAK,ILEV)
            CALL PKZEROS2(XTPCPAK(1,1,N),IJPAK,ILEV)
            CALL PKZEROS2(XTPPPAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x01
            CALL PKZEROS2( X01PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x02
            CALL PKZEROS2( X02PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x03
            CALL PKZEROS2( X03PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x04
            CALL PKZEROS2( X04PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x05
            CALL PKZEROS2( X05PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x06
            CALL PKZEROS2( X06PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x07
            CALL PKZEROS2( X07PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x08
            CALL PKZEROS2( X08PAK(1,1,N),IJPAK,ILEV)
#endif
#if defined x09
            CALL PKZEROS2( X09PAK(1,1,N),IJPAK,ILEV)
#endif
  575     CONTINUE
        ENDIF
C
#if defined isavdts
      IF(NSECS.EQ.0)                                  THEN
C
C        * RE-SET "IMDH" BACK TO CURRENT VALUE.
C
         IMDH=IMDHN
      ENDIF
#endif
#ifdef cf_sites
      IF (LSST) THEN
! SAVE THE DATA FROM A SELECT NUMBER OF POINTS TO FILE NUSITE
         K=KOUNT

! WRITE FIELDS TO A FILE

! APPROACH IS TO GATHER THE FIELDS FOR EACH LEVEL AND THEN EXTRACT
! THE DATA FROM THE FIELDS.  THE VARIABLE STNPAK CONTAINS AN INTEGER
! WHICH ASSOCIATES A POINT WITH A STATION NUMBER.

! GO THROUGH AND WRITE OUT THE FIELDS TO NUSITE

! GCM LONGITUDES
         VAR_NAME = "GLON"
         NNAME = NC4TO8(VAR_NAME)
         L = 1
         CALL WRITE_STATION_INFO(DLONPAK,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! GCM LONGITUDES
         VAR_NAME = "GLAT"
         NNAME = NC4TO8(VAR_NAME)
         L = 1
         CALL WRITE_STATION_INFO(DLATPAK,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! 2D FIELDS
         L = 1

! 2m TEMPERATURE
        VAR_NAME ="  ST"
        NNAME = NC4TO8(VAR_NAME)
        CALL WRITE_STATION_INFO(STPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                          LON1,ILAT,L,NUM_SITES,NUSITE)

! SKIN TEMPERATURE
         VAR_NAME ="  GT"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(GTPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! MEAN SEA LEVEL PRESSURE
         VAR_NAME ="PMSL"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(PMSLIPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SURFACE PRESSURE (TO ADD)
          VAR_NAME ="  PS"
          NNAME = NC4TO8(VAR_NAME)
          CALL WRITE_STATION_INFO(PSPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                            LON1,ILAT,L,NUM_SITES,NUSITE)

! 2M EAST WIND SPEED
         VAR_NAME ="  SU"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(SUPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! 2M NORTH WIND SPEED
         VAR_NAME ="  SV"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(SVPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! 2M WIND SPEED
         VAR_NAME ="  WS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(SWPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! 2M RELATIVE HUMIDITY
         VAR_NAME =" SRH"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(SRHPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! 2M SPECIFIC HUMIDITY
         VAR_NAME ="  SQ"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(SQPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! TOTAL PRECIPITATION
         VAR_NAME =" PCP"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(PCPPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SNOW
         VAR_NAME ="PCPN"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(PCPNPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! CONVECTIVE PRECIPITATION
         VAR_NAME ="PCPC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(PCPCPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SURFACE EVAPORATION
         VAR_NAME =" QFS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QFSIPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SURFACE SUBLIMATION
         VAR_NAME =" QFN"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QFNPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

         VAR_NAME ="QFVF"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QFVFPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! DOWNWARD EASTWARD WINDSTRESS
         VAR_NAME =" UFS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(UFSPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! DOWNWARD NORTHWARD WINDSTRESS
         VAR_NAME =" VFS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(VFSPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! LATENT HEAT FLUX
         VAR_NAME =" HFL"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(HFLPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SENSIBLE HEAT FLUX
         VAR_NAME =" HFS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(HFSPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! LW DOWNWARD AT SURFACE
         VAR_NAME =" FDL"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FDLPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! NET LW DOWNWARD AT SURFACE
         VAR_NAME =" FLG"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FLGPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SW DOWNWARD AT SURFACE
         VAR_NAME =" FSS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSSPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! NET SW DOWNWARD AT SURFACE
         VAR_NAME =" FSG"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSGPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! CLEAR-SKY SW DOWNWARD AT SURFACE
         VAR_NAME ="FSSC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSSCPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! CLEAR-SKY NET SW DOWNWARD AT SURFACE
         VAR_NAME ="FSGC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSGCPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! CLEAR-SKY LW DOWNWARD AT SURFACE
         VAR_NAME ="FDLC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FDLCPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SW DOWNWARD AT TOA
         VAR_NAME =" FSO"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSOPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! SW UPWARD AT TOA
         VAR_NAME =" FSR"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSRPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! LW UPWARD AT TOA
         VAR_NAME =" OLR"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(OLRPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! CLEAR-SKY LW UPWARD AT TOA
         VAR_NAME ="OLRC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(OLRCPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! CLEAR-SKY SW UPWARD AT TOA
         VAR_NAME ="FSRC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(SRCPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! PRECIPITABLE WATER
         VAR_NAME ="PWAT"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(PWATPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! TOTAL CLOUD FRACTION
         VAR_NAME ="CLDT"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CLDTPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! LIQUID CLOUD WATER PATH (GRID-MEAN)
         VAR_NAME ="CLWT"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CLWTPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! ICE CLOUD WATER PATH (GRID-MEAN)
         VAR_NAME ="CICT"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CICTPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! NET RADIATIVE FLUX (SW+LW) AT TOA
         VAR_NAME ="BALT"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(BALTPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! PRESSURE AT CONVECTION BASE
         VAR_NAME =" BCD"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(BCDPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! PRESSURE AT CONVECTION TOP
         VAR_NAME =" TCD"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TCDPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! FRACTION OF TIME CONVECTION OCCURS
         VAR_NAME ="CDCB"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CDCBPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! FRACTION OF TIME SHALLOW CONVECTION OCCURS
         VAR_NAME ="CSCB"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CSCBPAL,CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,L,NUM_SITES,NUSITE)

! 3D FIELDS
      DO L = 1, ILEV

! CLOUD AMOUNT
         VAR_NAME =" CLD"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CLDPAK(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! LIQUID CLOUD WATER (GRID MEAN)
         VAR_NAME =" CLW"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CLWPAK(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! ICE CLOUD WATER (GRID MEAN)
         VAR_NAME =" CIC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CICPAK(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! NET CONVECTIVE MASS FLUX
         VAR_NAME =" DMC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(DMCPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! TEMPERATURE
         VAR_NAME ="   T"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TAPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! EASTWARD WIND
         VAR_NAME ="   U"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(UAPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! NORTHWARD WIND
         VAR_NAME ="   V"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(VAPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! SPECIFIC HUMIDITY
         VAR_NAME ="   Q"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QAPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! RELATIVE HUMIDITY
         VAR_NAME ="  RH"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(RHPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! OMEGA
         VAR_NAME ="OMET"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(OMETPAK(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! GEOPOTENTIAL HEIGHT
         VAR_NAME ="  ZG"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(ZGPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! EDDY VISCOSITY COEFFICIENT FOR MOMENTUM
         VAR_NAME =" RKM"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(RKMPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! EDDY VISCOSITY COEFFICIENT FOR TEMPERATURE
         VAR_NAME =" RKH"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(RKHPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! CLEAR-SKY LW HEATING
         VAR_NAME ="TTLC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTLCPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! CLEAR-SKY SW HEATING
         VAR_NAME ="TTSC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTSCPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! STRATIFORM LIQUID CLOUD EFFECTIVE RADIUS
         VAR_NAME ="RLIQ"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(RLIQPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

         VAR_NAME ="CLIQ"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CLIQPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! STRATIFORM ICE CLOUD EFFECTIVE RADIUS
         VAR_NAME ="RICE"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(RICEPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

         VAR_NAME ="CICE"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(CICEPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! TEMPERATURE TENDENCY DUE TO PHYSICS
         VAR_NAME =" TTP"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! TEMPERATURE TENDENCY DUE TO STRAT. CLOUD AND PBL MIXING
         VAR_NAME ="TTPP"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPPPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

         VAR_NAME ="TTPV"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPVPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! TEMPERATURE TENDENCY DUE TO LW HEATING
         VAR_NAME ="TTPL"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPLPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! TEMPERATURE TENDENCY DUE TO SW HEATING
         VAR_NAME ="TTPS"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPSPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! TEMPERATURE TENDENCY DUE TO CONVECTION
         VAR_NAME ="TTPC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPCPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

         VAR_NAME ="TTPM"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(TTPMPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! SPECIFIC HUMIDITY TENDENCY DUE TO CONVECTION
         VAR_NAME ="QTPC"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QTPCPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

         VAR_NAME ="QTPM"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QTPMPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! SPECIFIC HUMIDITY TENDENCY DUE TO STRAT. CLOUD AND PBL MIXING
         VAR_NAME ="QTPP"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QTPPPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

         VAR_NAME ="QTPV"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QTPVPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)

! SPECIFIC HUMIDITY TENDENCY DUE TO PHYSICS
         VAR_NAME =" QTP"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(QTPPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LH(L),NUM_SITES,NUSITE)
       END DO  ! L

! 3D RADIATIVE FLUX FIELDS
      DO L = 1, ILEV+2
! LW UPWARD
         VAR_NAME ="FLAU"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FLAUPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LR(L),NUM_SITES,NUSITE)

! SW UPWARD
         VAR_NAME ="FSAU"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSAUPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LR(L),NUM_SITES,NUSITE)

! CLEAR-SKY LW UPWARD
         VAR_NAME ="FLCU"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FLCUPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LR(L),NUM_SITES,NUSITE)

! CLEAR-SKY SW UPWARD
         VAR_NAME ="FSCU"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSCUPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LR(L),NUM_SITES,NUSITE)

! CLEAR-SKY LW DOWNWARD
         VAR_NAME ="FLCD"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FLCDPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LR(L),NUM_SITES,NUSITE)

! CLEAR-SKY SW DOWNWARD
         VAR_NAME ="FSCD"
         NNAME = NC4TO8(VAR_NAME)
         CALL WRITE_STATION_INFO(FSCDPAL(1,L),CF_SITE_IND,NNAME,KOUNT, ! INPUT
     1                           LON1,ILAT,LR(L),NUM_SITES,NUSITE)

       END DO ! L

      ENDIF !LSST

#endif ! cf_sites
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
