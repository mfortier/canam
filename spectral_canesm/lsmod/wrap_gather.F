#include "cppdef_config.h"
!!--- NEW: #define with_MPI_

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      SUBROUTINE WRAP_GATHER(A_IN,   SZ_IN,  TYPE_IN, 
     1                       A_OUT,  SZ_OUT, TYPE_OUT, 
     2                       MASTER, COMM,   IERR)
C
C     * OCT 05/2003 - R.MCLAY.
C
C     * COMMON FORTRAN CODE FOR MPI CALLS IN CRB OUTPUT ROUTINES.
C
      INTEGER*4 :: SZ_IN 		!<Variable description\f$[units]\f$
      INTEGER*4 :: TYPE_IN 		!<Variable description\f$[units]\f$
      INTEGER*4 :: SZ_OUT		!<Variable description\f$[units]\f$
      INTEGER*4 :: TYPE_OUT		!<Variable description\f$[units]\f$
      INTEGER*4 :: COMM			!<Variable description\f$[units]\f$
      INTEGER*4 :: IERR			!<Variable description\f$[units]\f$
      INTEGER*4 :: MASTER		!<Variable description\f$[units]\f$
      REAL      :: A_IN(SZ_IN)		!<Variable description\f$[units]\f$
      REAL      :: A_OUT(SZ_OUT)	!<Variable description\f$[units]\f$

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

#ifdef with_MPI_
      CALL MPI_GATHER( A_IN,   SZ_IN,  TYPE_IN, 
     1                 A_OUT,  SZ_OUT, TYPE_OUT, 
     2                 MASTER, COMM,   IERR)
#endif
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
