!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C     * Aug 23/2018 - V.Arora.   Additional CMIP6 output variables
C                                added to output to tm file
C
C     * Jul 17/2018 - V.Arora.   No need to output CH4N
C                                and CH4H from specified wetlands. 
C                                Also don't output CW2D because
C                                we don't use it. So the only wetland
C                                CH4 emissions are now based on dynamic
C                                wetlands and heterotrophic respiration 
C                                and called CW1D.
C
C     * Feb 16/2016 - M.Lazare.  New routine (common deck) for gcm19.
C     *                          Save fields for CTEM, analagous to
C     *                          how save normal fields in AGCM.
C=========================================================================
C     * SAVE DAILY CTEM OUTPUT TO FILE NUTM (OUTCM).

      K=IYMD

C          1. VEGETATION BIOMASS
           CALL PUTGG(CVEGPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CVEG"),1,GLL,WRKS)

C          2. LITTER MASS
           CALL PUTGG(CDEBPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CDEB"),1,GLL,WRKS)

C          3. SOIL C MASS
           CALL PUTGG(CHUMPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CHUM"),1,GLL,WRKS)

C          4. LEAF AREA INDEX
           CALL PUTGG(CLAIPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CLAI"),1,GLL,WRKS)

C          5. NET PRIMARY PRODUCTIVITY
           CALL PUTGG(CFNPPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFNP"),1,GLL,WRKS)

C          6. NET ECOSYSTEM PRODUCTIVITY
           CALL PUTGG(CFNEPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFNE"),1,GLL,WRKS)

C          7. AUTOTROPHIC RESPIRATION
           CALL PUTGG(CFRVPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFRV"),1,GLL,WRKS)

C          8. GROSS PRIMARY PRODUCTIVITY
           CALL PUTGG(CFGPPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFGP"),1,GLL,WRKS)

C          9. NET BIOME PRODUCTIVITY
           CALL PUTGG(CFNBPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFNB"),1,GLL,WRKS)

C          10. FIRE LOSSES FROM VEGETATION
           CALL PUTGG(CFFVPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFFV"),1,GLL,WRKS)

C          11. FIRE LOSSES FROM LITTER
           CALL PUTGG(CFFDPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFFD"),1,GLL,WRKS)

C          12. LAND USE CHANGE COMBUSTION LOSSES FROM VEGETATION
           CALL PUTGG(CFLVPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFLV"),1,GLL,WRKS)

C          13. LAND USE CHANGE LITTER INPUTS
           CALL PUTGG(CFLDPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFLD"),1,GLL,WRKS)

C          14. LAND USE CHANGE SOIL CARBON INPUTS
           CALL PUTGG(CFLHPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFLH"),1,GLL,WRKS)

C          15. AREA BURNED
           CALL PUTGG(CBRNPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CBRN"),1,GLL,WRKS)

C          16. RESPIRATION FROM SOIL C POOL
           CALL PUTGG(CFRHPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFRH"),1,GLL,WRKS)

C          17. HUMIFICATION TRANSFER FROM LITTER TO SOIL C POOL
           CALL PUTGG(CFHTPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFHT"),1,GLL,WRKS)

C          18. LITTER FALL
           CALL PUTGG(CFLFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFLF"),1,GLL,WRKS)

C          19. LITTER RESPIRATION
           CALL PUTGG(CFRDPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFRD"),1,GLL,WRKS)
C
C          ADDITIONAL CMIP5 OUTPUT
C
C          20. GROWTH RESPIRATION
           CALL PUTGG(CFRGPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFRG"),1,GLL,WRKS)

C          21. MAINTENANCE RESPIRATION
           CALL PUTGG(CFRMPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFRM"),1,GLL,WRKS)

C          22. BIOMASS IN LEAVES 
           CALL PUTGG(CVGLPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CVGL"),1,GLL,WRKS)

C          23. BIOMASS IN STEM
           CALL PUTGG(CVGSPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CVGS"),1,GLL,WRKS)

C          24. BIOMASS IN ROOTS
           CALL PUTGG(CVGRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CVGR"),1,GLL,WRKS)

C          25. NPP OF LEAVES
           CALL PUTGG(CFNLPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFNL"),1,GLL,WRKS)

C          26. NPP OF STEM
           CALL PUTGG(CFNSPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFNS"),1,GLL,WRKS)

C          27. NPP OF ROOTS
           CALL PUTGG(CFNRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CFNR"),1,GLL,WRKS)

C          28. CH4 WETLAND EMISSIONS, HETEROTROPHIC RESP. BASED
C          CALL PUTGG(CH4HPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
C    &                  NC4TO8("CH4H"),1,GLL,WRKS)

C          29. CH4 WETLAND EMISSIONS, NPP BASED
C          CALL PUTGG(CH4NPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
C    &                  NC4TO8("CH4N"),1,GLL,WRKS)

C          30. DYNAMIC WETLAND FRACTION
           CALL PUTGG(WFRAPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("WFRA"),1,GLL,WRKS)

C          31. DYNAMIC CH4 WETLAND EMISSIONS, HETEROTROPHIC 
           CALL PUTGG(CW1DPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CW1D"),1,GLL,WRKS)

C          32. DYNAMIC CH4 WETLAND EMISSIONS, NPP 
C          CALL PUTGG(CW2DPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
C    &                  NC4TO8("CW2D"),1,GLL,WRKS)

C          33. CARBON LAND FLUX
           CALL PUTGG(FCOLPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("FCOL"),1,GLL,WRKS)

C          34. CURRENT CTEM VEGETATION FRACTION (ICTEM LEVELS)
           DO L=1,ICTEM
             CALL PUTGG(CURFPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                    NC4TO8("CURF"),LCTEM(L),GLL,WRKS)
           ENDDO

C          Additional CTEM related CMIP6 output

C          35. BRFRPAK   ! baresoilFrac
           CALL PUTGG(BRFRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("BRFR"),1,GLL,WRKS)

C          36. C3CRPAK   ! cropFracC3
           CALL PUTGG(C3CRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("C3CR"),1,GLL,WRKS)

C          37. C4CRPAK   ! cropFracC4
           CALL PUTGG(C4CRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("C4CR"),1,GLL,WRKS)

C          38. CRPFPAK   ! cropFrac
           CALL PUTGG(CRPFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CRPF"),1,GLL,WRKS)

C          39. C3GRPAK   ! grassFracC3
           CALL PUTGG(C3GRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("C3GR"),1,GLL,WRKS)

C          40. C4GRPAK   ! grassFracC4
           CALL PUTGG(C4GRPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("C4GR"),1,GLL,WRKS)

C          41. GRSFPAK   ! grassFrac
           CALL PUTGG(GRSFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("GRSF"),1,GLL,WRKS)

C          42. BDTFPAK   ! treeFracBdlDcd
           CALL PUTGG(BDTFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("BDTF"),1,GLL,WRKS)

C          43. BETFPAK   ! treeFracBdlEvg
           CALL PUTGG(BETFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("BETF"),1,GLL,WRKS)

C          44. NDTFPAK   ! treeFracNdlDcd
           CALL PUTGG(NDTFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("NDTF"),1,GLL,WRKS)

C          45. NETFPAK   ! treeFracNdlEvg
           CALL PUTGG(NETFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("NETF"),1,GLL,WRKS)

C          46. TREEPAK   ! treeFrac
           CALL PUTGG(TREEPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("TREE"),1,GLL,WRKS)

C          47. VEGFPAK   ! vegFrac
           CALL PUTGG(VEGFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("VEGF"),1,GLL,WRKS)

C          48. C3PFPAK   ! c3PftFrac
           CALL PUTGG(C3PFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("C3PF"),1,GLL,WRKS)

C          49. C4PFPAK   ! c4PftFrac
           CALL PUTGG(C4PFPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("C4PF"),1,GLL,WRKS)

C          50. CLNDPAK   ! cLand
           CALL PUTGG(CLNDPAK,LON1,ILAT,KHEM,NPGG,K,NUTM,
     &                  NC4TO8("CLND"),1,GLL,WRKS)

C
C     *    ZERO OUT ACCUMULATED FIELDS.
C     *    NOTE THAT {CVEG,CDEB,CHUM,CLAI,CVGL,CVGS,CVGR,CURF}
C     *    ARE ONLY SAMPLED!
C
C     *    THE ADDITIONAL CMIP6 LAND COVER RELATED FRACTIONS
C          AND OTHER VARIABLES (CLND)
C     *    ARE ZEROED AS ROW VARIABLES IN SFCPROC2.DK SO LIKELY
C     *    NOT NEEDED TO BE ZEROED HERE.
C
           CALL PKZEROS2(CFNPPAK,IJPAK,   1)
           CALL PKZEROS2(CFNEPAK,IJPAK,   1)
           CALL PKZEROS2(CFRVPAK,IJPAK,   1)
           CALL PKZEROS2(CFGPPAK,IJPAK,   1)
           CALL PKZEROS2(CFNBPAK,IJPAK,   1)
           CALL PKZEROS2(CFFVPAK,IJPAK,   1)
           CALL PKZEROS2(CFFDPAK,IJPAK,   1)
           CALL PKZEROS2(CFLVPAK,IJPAK,   1)
           CALL PKZEROS2(CFLDPAK,IJPAK,   1)
           CALL PKZEROS2(CFLHPAK,IJPAK,   1)
           CALL PKZEROS2(CBRNPAK,IJPAK,   1)
           CALL PKZEROS2(CFRHPAK,IJPAK,   1)
           CALL PKZEROS2(CFHTPAK,IJPAK,   1)
           CALL PKZEROS2(CFLFPAK,IJPAK,   1)
           CALL PKZEROS2(CFRDPAK,IJPAK,   1)
           CALL PKZEROS2(CFRGPAK,IJPAK,   1)
           CALL PKZEROS2(CFRMPAK,IJPAK,   1)
           CALL PKZEROS2(CFNLPAK,IJPAK,   1)
           CALL PKZEROS2(CFNSPAK,IJPAK,   1)
           CALL PKZEROS2(CFNRPAK,IJPAK,   1)
           CALL PKZEROS2(CH4HPAK,IJPAK,   1)
           CALL PKZEROS2(CH4NPAK,IJPAK,   1)
           CALL PKZEROS2(WFRAPAK,IJPAK,   1)
           CALL PKZEROS2(CW1DPAK,IJPAK,   1)
           CALL PKZEROS2(CW2DPAK,IJPAK,   1)
           CALL PKZEROS2(FCOLPAK,IJPAK,   1)
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
