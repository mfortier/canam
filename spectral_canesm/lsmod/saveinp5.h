#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * Nov 22/2018 - M.Lazare.    Put back in ICO2 redefinition for specifiec CO2.
C     * Nov 05/2018 - M.Lazare.    Added 3HR save fields.
C     * Aug 19/2018 - M.Lazare.    Remove ICO2 redefinition for specified CO2
C     *                            since now done in revised RSTARTH.
C     * Aug 14/2018 - M.Lazare.    Remove MASKPAK.
C     * JUL 27/2019 - M.LAZARE.    Remove {COM,CSP,CSN} parmsub variables.
C     * Nov 08/2017 - M.Lazare.    Add CMPLX intinsic specification for "TRAC"
C     *                            ICO2 redefinition for specified CO2.
C     * Aug 10/2017 - M.Lazare.    - FLAK->{FLKR,FLKU}.
C     *                            - LICN->GICN.     
C     *                            - Add lakes arrays.              
C     * Aug 07/2017 - M.Lazare.    Initial Git verison. 
C     * Feb 04/2014 - M.Lazare.    New version for gcm18:
C     *                            - Added FLND,FLAK,LICN to invariant section.
C     * Jul 10/2013 - M.Lazare/    PREVIOUS VERSION SAVEINP4 FOR GCM17:
C     *               K.Vonsalzen. - "xtraplafrc" fields added.
C     *                            - Removed DZG,PORG and added WSNO/ZPND
C     *                              for new CLASS version.
C     *                            - ODST and ABST are calculated as
C     *                              instantaneous results in the radiation
C     *                              so removed here and put in SAVEINP4
C     *                              instead.
C     * MAY 07/2012 - M.LAZARE.    PREVIOUS VERSION SAVEINP3 FOR GCM16:
C     *                            - SAVE HRS AND HRL.
C     * MAY 04/2010 - M.LAZARE.    PREVIOUS VERSION SAVEINP2I FOR GCM15I:
C     *                            - "OMET" NOW SAVED SINCE IS TRUE
C     *                              VERTICAL VELOCITY.
C     *                            - "CLDS" REMOVED AND "ZDET","CLCV",
C     *                              "SFRC" SAVING COMMENTED OUT TO
C     *                              SAVE SPACE.
C     *                            - IF RUNNING "SPECIFIED_CO2" EXPERIMENT
C     *                              FOR CARBON CYCYLE, STORE THE MMR
C     *                              VALUE OF CO2 INTO THE (0,0) COEFFICIENT
C     *                              OF THE TRACER ARRAY (CARBON PORTION) AND
C     *                              ZERO OUT THE OTHER COEFFICIENTS. THIS
C     *                              WILL ALLOW ALL THE DIAGNOSTICS TO WORK
C     *                              IN THE SAME MANNER WITH/WITHOUT THE
C     *                              "SPECIFIED_CO2" SWITCH. 
C     * MAR 24/2009 - M.LAZARE/    PREVIOUS VERSION SAVEINP2H FOR GCM15H:
C     *               K.VONSALZEN. - NEW SAVED FIELDS "VTAU" AND "TROP",
C     *                              UNDER CONTROL OF "%IF DEF,EXPLVOL".
C     *                            - NEW SAVED FIELD "SFRCPAL" UNDER
C     *                              CONTROL OF "%IF DEF,XTRACONV". 
C     *                            - SAVE MASKPAK IN INVARIANT SECTION.
C     *                            - SAVE DPTHPAK IN INVARIANT SECTION.
C     * JAN 17/2008 - M.LAZARE/    PREVIOUS VERSION SAVEINP2G FOR GCM15G:
C     *               K.VONSALZEN. - SAVES NEW ALMX,ALMC FIELDS.
C     *                            - CODE RELATED TO S/L REMOVED.
C     *                            - REMOVES SAVING OF: SCLF,SCDN,SLWC,
C     *                              EBC,EOC,EOCF,EDMS,DMSO.
C     * SEP 11/2006 - M.LAZARE.    PREVIOUS VERSION SAVEINP2F FOR GCM15F:
C     *                            - CALLS "NAME2" INSTEAD OF "NAME".
C     * DEC 15/2005 - M.LAZARE/    PREVIOUS VERSION SAVEINP2D FOR GCM15D/E:
C     *               K.VONSALZEN. - ADD CVAR FOR NEW STATISTICAL
C     *                              CLOUD SCHEME.
C     *                            - ADD SCDN,SCLF,SLWC FOR SHALLOW-CLOUD
C     *                              EFFECTS ON RADIATION.
C     * FEB 15,2005 - M.LAZARE.    PREVIOUS VERSION FOR GCM15C:
C     *                           - ADD MULTI-LEVEL DIAGNOSTIC TERMS
C     *                             FOR LARGE-SCALE UNDER CONTROL
C     *                             OF %IF DEF,XTRALS.
C     *                            - REWIND NUINV AT KOUNT=0 SO
C     *                              INVARIANT FIELDS SAVED ONLY ONCE.
C     *                            - SAVE NEW INPUT FIELDS CHFX,CQFX.      
C     * APR 01,2004 - J.SCINOCCA.  FINAL VERSION OF PREVIOUS SAVEINP2B:  
C     *                            CALLS NEW PUTNATR3 IN SUPPORT OF
C     *                            {ITRLVS,ITRLVF} SUBSET OF VERTICAL
C     *                            DOMAIN FOR NON-ADVECTED TRACERS.
C     * FEB 16,2004 - M.LAZARE.    "SALB" AND ""OMET" SAVING COMMENTED
C     *                            OUT AND "ZDET","CLCV" SAVING NOW
C     *                            OPTIONAL UNDER CONTROL OF "%IF DEF,
C     *                            XTRACONV" (ALL THESE GENERALLY NOT
C     *                            NEEDED).  
C     * DEC 20,2003 - M.LAZARE/    NEW VERSION FOR GCM15B (SAVEINP2B):
C     *               K.VONSALZEN. - SALBPAL ADDED AND ALSWPAL,ALLWPAL
C     *                              REMOVED.
C     *                            - ADD QWF0,QWFM,XWF0,XWFM FIELDS
C     *                              FOR NEW CONSERVATION.
C     *                            - ADD NEW FIELDS: TFX,QFX,CBMF,ZDET,
C     *                              CLCV,OMET,CLDS,ETC. 
C     *                            - NEW INPUT ARRAYS SAVED UNDER CONTROL
C     *                              OF UPDATE DIRECTIVE "XTRASULF".
C     * JAN 05/2004 - M.LAZARE.    PREVIOUS VERSION SAVEINPD FOR GCM13D.
C===================================================================
C     * SAVE INVARIANT FIELDS AT FIRST TIMESTEP.
C
      IF(KOUNT.EQ.KSTART)                                        THEN
          K=0
          REWIND NUINV
          CALL PUTGG(DPTHPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("DPTH"),1,GLL,WRKS)
          CALL PUTGG(FLKRPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("FLKR"),1,GLL,WRKS)
          CALL PUTGG(FLKUPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("FLKU"),1,GLL,WRKS)
          CALL PUTGG(BLAKPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("BLAK"),1,GLL,WRKS)
          CALL PUTGG(HLAKPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("HLAK"),1,GLL,WRKS)
          CALL PUTGG(LLAKPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("LLAK"),1,GLL,WRKS)
#if defined (cslm)
          CALL PUTGG(NLKLPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("NLAK"),1,GLL,WRKS)
#endif
          CALL PUTGG(FLNDPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("FLND"),1,GLL,WRKS)
          CALL PUTGG(GICNPAK,LON1,ILAT,KHEM,NPGG,K,NUINV,
     1                 NC4TO8("GICN"),1,GLL,WRKS)
      ENDIF
C
C     * SAVE SPECTRAL FIELDS EVERY ISSP TIMESTEPS.
C
      IF (LSSP)                                                  THEN
        CALL PUTSTG9(NUPR,PS,P,C,T,ES,PHIS,
     1               KOUNT,LA,LRLMT,ILEV,LEVSPEC,LS,LH,
     2               LSRTOTAL,LATOTAL,LMTOTAL,GLL)
      ENDIF
C
C     * SAVE TRACER FIELDS EVERY ISTR TIMESTEPS.
C
      IF(NTRACA.GT.0) THEN
#if defined carbon
#if defined specified_co2
C
C       * IF RUNNING INVERSE EXPERIMENT FOR CARBON CYCLE, WE WANT TO STORE
C       * THE MMR VALUE OF CO2 INTO THE (0,0) COEFFICIENT OF THE TRACER
C       * ARRAY (FOR THE CARBON PORTION) AND ZERO OUT THE OTHER COEFFICIENTS.
C       * THIS WILL ENABLE ALL ASSOCIATED DIAGNOSTICS TO WORK THE SAME WITH/
C       * WITHOUT THE INVERSE CALCULATION.
C
        DO NA=1,NTRSPEC
          N=INDXA(NA)
          IF(N.EQ.ICO2)                                           THEN
            IF( ITRVAR.EQ.NC4TO8("   Q") .OR.
     1         (ITRVAR.EQ.NC4TO8("QHYB") .AND. XREF(N).EQ.0.) ) THEN
              REAL_ZERO = SQRT(2.)*RMCO2
              REAL_REST = 0.
            ELSE
              PINV  = 1./XPOW(N)
              XXREF = XREF(N)/((1.+XPOW(N)*LOG(XREF(N)/RMCO2))**PINV)
              REAL_ZERO = SQRT(2.)*XXREF
              REAL_REST = XMIN(N)
            ENDIF
            DO L=1,ILEV
              DO I=1,LA
                MNK = (L-1)*LA+I
                TRAC(MNK,NA) = CMPLX(REAL_REST,0.)
              ENDDO
              IF(MYNODE.EQ.0)                                    THEN
                MNK = (L-1)*LA+1
                TRAC(MNK,NA) = CMPLX(REAL_ZERO,0.)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
#endif
#endif
        CALL PUTRAC4X(NUPR,TRAC,LA,LRLMT,ILEV,LH,KOUNT,LSTR,
     1                LSRTOTAL,LATOTAL,LMTOTAL,GLL, 
     2                ITRSPEC,NTRSPEC,NTRAC,ITRNAM,INDXA)
      ENDIF
C
C     * GENERALLY, WE NOW SAVE NON-ADVECTED FIELDS. CHANGE ISAVNA
C     * VIA UPDATES IF THIS IS NOT DESIRED.
C
      ISAVNA=1
      IF(NTRACN.GT.0 .AND. ISAVNA.NE.0) THEN
         CALL PUTNATR3(NUPR,TRACNA(1,INP),LON1,ILAT,IP0J,ILEV,
     1                 LH,KOUNT,LSTR,KHEM,NTRAC,
     2                 ITRNAM,ITRLVS,ITRLVF,INDXNA,NTRACN,GLL,WRKS)
      ENDIF
C
      IF (LS3HR) THEN
C
C        * SAVE SELECTED FIELDS ON FILE NU3HR EVERY IS3HR (3-HOUR) STEPS.
C
         IF(ISAVDTS.NE.0)                   THEN
            IBUF2=IYMDH
         ELSE
            IBUF2=KOUNT
         ENDIF
C
         CALL SETLAB(IBUF,NC4TO8("SPEC"),IBUF2,-1,1,-1,1,LRLMT,0)
         IBUF(3)=NC4TO8("LNSP")
         CALL PUTSPN(NU3HR,PS,LA,
     1               LSRTOTAL,LATOTAL,LMTOTAL,GLL,
     2               IBUF,MAXX,IPIO,OK)
        
      ENDIF
C
      IF (LSGG) THEN
C
C        * SAVE I/O GRIDS ON FILE NUPR EVERY ISGG STEPS, IF REQUESTED.
C
         K=KOUNT
#if defined explvol
C
         CALL PUTGG(VTAUPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("VTAU")
     1                       ,1,GLL,WRKS)
         CALL PUTGG(TROPPAK,LON1,ILAT,KHEM,NPGG,K,NUPR,NC4TO8("TROP")
     1                       ,1,GLL,WRKS)
#endif
C
         DO L=1,ILEV
            CALL PUTGG( CLDPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" CLD"),LH(L),GLL,WRKS)
            CALL PUTGG(TACNPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("TACN"),LH(L),GLL,WRKS)
            CALL PUTGG(  RHPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("  RH"),LH(L),GLL,WRKS)
            CALL PUTGG( CLWPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" CLW"),LH(L),GLL,WRKS)
            CALL PUTGG( CICPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" CIC"),LH(L),GLL,WRKS)
            CALL PUTGG(CVARPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("CVAR"),LH(L),GLL,WRKS)
            CALL PUTGG(ALMXPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("ALMX"),LH(L),GLL,WRKS)
            CALL PUTGG(ALMCPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("ALMC"),LH(L),GLL,WRKS)
            CALL PUTGG(OMETPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("OMET"),LH(L),GLL,WRKS)
            CALL PUTGG( HRLPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" HRL"),LH(L),GLL,WRKS)
            CALL PUTGG( HRSPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" HRS"),LH(L),GLL,WRKS) 
#if defined qconsav
            CALL PUTGG(QWF0PAL(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("QWF0"),LH(L),GLL,WRKS)
            CALL PUTGG(QWFMPAL(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("QWFM"),LH(L),GLL,WRKS)
#endif
         ENDDO
C
C        * TKE FIELDS.
C
         IF(ITKE.EQ.1) THEN
           DO L=1,ILEV
             CALL PUTGG(XLMPAK(1,L), LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                               NC4TO8(" XLM"),LH(L),GLL,WRKS)
             CALL PUTGG(SVARPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                               NC4TO8("SVAR"),LH(L),GLL,WRKS)
           ENDDO
         ENDIF
C
#if defined xconsav
         DO N=1,NTRAC
            CALL NAME2(NAM,'W0',N)
            DO L=1,ILEV
               CALL PUTGG(XWF0PAL(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                     NAM,LH(L),GLL,WRKS)
            ENDDO
C
            CALL NAME2(NAM,'WM',N)
            DO L=1,ILEV
               CALL PUTGG(XWFMPAL(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                     NAM,LH(L),GLL,WRKS)
            ENDDO
         ENDDO 
#endif
#if defined xtraconv

C        DO L=1,ILEV
C           CALL PUTGG(ZDETPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
C    1                                NC4TO8(" DET"),LH(L),GLL,WRKS)
C           CALL PUTGG(CLCVPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
C    1                                NC4TO8(" CCV"),LH(L),GLL,WRKS)
C        ENDDO
C        DO N=1,NTRAC
C           CALL NAME2(NAM,'SF',N)
C           DO L=1,ILEV
C              CALL PUTGG(SFRCPAL(1,L,N),LON1,ILAT,KHEM,NPGG,K,NUPR,
C    1                                     NAM,LH(L),GLL,WRKS)
C           ENDDO
C        ENDDO
#endif
#if defined xtrasulf

         DO L=1,ILEV

            CALL PUTGG(  OHPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("  OH"),LH(L),GLL,WRKS)

            CALL PUTGG(H2O2PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("H2O2"),LH(L),GLL,WRKS)

            CALL PUTGG(  O3PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("  O3"),LH(L),GLL,WRKS)

            CALL PUTGG( NO3PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" NO3"),LH(L),GLL,WRKS)

            CALL PUTGG(HNO3PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("HNO3"),LH(L),GLL,WRKS)

            CALL PUTGG( NH3PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" NH3"),LH(L),GLL,WRKS)

            CALL PUTGG( NH4PAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8(" NH4"),LH(L),GLL,WRKS)

            CALL PUTGG(SO4CPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                                NC4TO8("SO4C"),LH(L),GLL,WRKS)
         ENDDO
#endif
      ENDIF
C
      IF (LSHF)                                             THEN
C
C       * SAVE HIGH-FREQUENCY FIELDS ON FILE NUPR EVERY ISGG STEPS, IF REQUESTED.                                                                                          C            
        IF(ITKE.EQ.1) THEN
          K=KOUNT
          DO L=1,ILEV
            CALL PUTGG(TKEMPAK(1,L),LON1,ILAT,KHEM,NPGG,K,NUPR,
     1                            NC4TO8("TKEM"),LH(L),GLL,WRKS)
          ENDDO
        ENDIF
      ENDIF
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
