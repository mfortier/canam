#include "cppdef_config.h"
!!--- NEW: #define with_COUPLED_
!!--- NEW: #define with_MPI_

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * MAR 22/2019 - J. COLE      Add new unit numer 18 for site output (NUSITE).
C     * NOV 03/2018 - M.LAZARE.    Add new unit number 17 for 3-hr saving (NU3HR).
C     * JUL 27/2018 - M.LAZARE.    Remove {COM,CSP,CSN} parmsub variables.
C     * JUN 12/2018 - M.LAZARE.    - Add new unit number 25 (NUCH) for CHEM
C     *                              and new unit number 26 (NUOX) for OXI.
C     * JUL 13/2013 - K.VONSALZEN. New version for gcm17:
C     *                            - Additional unit numbers for PAM.
C     * MAY 08/2012 - J.SCINOCCA.  Previous version COMOPEN9 for gcm16:
C     *                            - Add IRELAX unit number and open for
C     *                              relaxation cpp option.
C     * MAY 05/2010 - K.VONSALZEN/ Previous version COMOPEN8 for gcm15i:
C     *               M.LAZARE.    - "HISTEMI" update directive
c     *                              replaced by
C     *                              "transient_aerosol_emissions" cpp
C     *                              directive.
C     *                            - Add new "transient_ozone_emissions"
C     *                              cpp directive option.
C     *                            - other update directives replaced
C     *                              by cpp directives.
C     * FEB 23/2007 - K.VONSALZEN. PREVIOUS VERSION COMOPEN7 FOR GCM15H:
C     *                            - DEFINE AND OPEN NEW UNIT FOR
C     *                              TRANSIENT AEROSOL EMISSIONS
C     *                              UNDER CONTROL OF "%DF HISTEMI".
C     * JAN 10/2004 - M.LAZARE. PREVIOUS VERSION COMOPEN6 FOR
C     *                         GCM13C/GCM15B TO GCM15G:
C     *                         - OPENING OF OUTGCM,OUTINV,OUTEND LOGICAL
C     *                           UNITS MOVED TO GCM DRIVER AND DONE
C     *                           DIFFERENTLY DEPENDING ON WHETHER DOING
C     *                           PARALLEL I/O OR NOT.
C     *                         - "FIZ" REMOVED.
C     *                         - DEFINE "IPARIO" COMMON BLOCK AND
C     *                           INITIALIZE ITS VARIABLE "IPIO" BASED
C     *                           ON UPDATE DIRECTIVE "PARALLEL_IO".
C     *                         - VALUE FOR NPGG NOW HARDCOATED TO ZERO.
C     * MAY 30/2003 - M.LAZARE. NEW VERSION FOR IBM (COMOPEN5):
C     *                         - "FIZTRAC" REMOVED.
C     *                         - NPPS,NPSF,NPVG REMOVED.
C     * DEC 12/2001 - M.LAZARE. PREVIOUS VERSION COMOPEN4:
C     *                         ADD NUINV/LUINV/OUTINV UNIT FOR
C     *                         INVARIANT FIELDS. BASED ON COMOPEN3.
C
C     * MACHINE CONSTANTS FOR PACKING ROUTINES.
C     * "MACHINE" HAS A VALUE OF 1 FOR 64-BIT MACHINES (INTEGER SIZE)
C     * AND A VALUE OF 2 FOR 32-BIT MACHINES (INTEGER SIZE).
C     * "INTSIZE" IS THE RATIO OF THE FLOATING-POINT WORD SIZE TO
C     * INTEGER SIZE (I.E. IS UNITY UNLESS RUNNING REAL*8 ON A 32-BIT
C     * MACHINE).
C
      COMMON /MACHTYP/ MACHINE,INTSIZE
C
C     * DEFINE "IPARIO" COMMON BLOCK AND INITIALIZE ITS VARIABLE "IPIO" BASED
C     * ON UPDATE DIRECTIVE "PARALLEL_IO".
C
      COMMON /IPARIO/ IPIO
C
C     * STRING ARRAYS FOR DEFINING OUTGCM,OUTEND,OUTINV,OUTTM UNIT NAMES.
C
      CHARACTER*128 ONAME
      CHARACTER*128 TNAME
      CHARACTER*128 INAME
      CHARACTER*128 CNAME
      CHARACTER*128 HNAME
      CHARACTER*128 SNAME
C
#if defined parallel_io
      DATA IPIO   /1 /
#else
      DATA IPIO   /0 /
#endif
C
C     * LOGICAL UNITS.
C     * NOTE THAT LUIA MUST NOT CHANGE BECAUSE IT'S VALUE IS USED IN
C     * PUTGGB3 TO USE KOUNT IN IBUF(2) EVEN WHEN RUNNING WITH
C     * MULTI-YEAR DATE-TIME STAMP (IE WHEN RUNNING WITH INTERACTIVE
C     * OCEAN).
C
      DATA NPGG   /0 /
      DATA NUSP   /11/
      DATA NUGG   /12/
      DATA NUPR   /13/
      DATA NURS   /14/
      DATA NUIA   /15/
      DATA NU3HR  /17/
      DATA NUSITE /18/
      DATA LUIA   /19/
      DATA NUTD   /20/
      DATA NUINV  /21/
#if defined transient_aerosol_emissions
      DATA NUTR   /22/
#endif
#if defined transient_ozone_concentrations
      DATA NUOZ_CHEM /23/
      DATA NUOZ_RAD  /24/
#endif
      DATA NUCH      /25/
      DATA NUOX      /26/
#if defined (agcm_ctem)
      DATA NUTM      /27/
#endif
#if defined relax
      DATA IORELAX_SPC/35/      ! spectral P/C/T/ES/PS reference
      DATA IORELAX_TSPC/36/     ! spectral bias-corrections
      DATA IORELAX_TG1/37/ 	! land TG1 reference
      DATA IORELAX_WG1/38/ 	! land WG1 reference
      DATA IORELAX_TGRD/39/     ! gridded bias-corrections
      DATA IORELAX_SST/40/      ! SST reference
      DATA IORELAX_SICN/41/     ! SICN reference
      DATA IORELAX_SIC/42/      ! SIC reference
      DATA IORELAX_SSS/43/      ! SSS reference
#endif
#if defined (pla)
#if defined (pfrc)
      INTEGER :: NUPF = 81
#endif
#endif
#if defined with_MPI_ || defined with_COUPLED_
      CALL MPI_INIT(IERR)
#endif
C
      OPEN(NUSP,FILE='OLDRS',FORM='UNFORMATTED')
      OPEN(NUGG,FILE='AN',FORM='UNFORMATTED')
      OPEN(NUCH,FILE='CHEM',FORM='UNFORMATTED')
      OPEN(NUOX,FILE='OXI',FORM='UNFORMATTED')

#if defined transient_aerosol_emissions
      OPEN(NUTR,FILE='TRANS',FORM='UNFORMATTED')
#endif
#if defined transient_ozone_concentrations
      OPEN(NUOZ_CHEM,FILE='HISTOZ_CHEM',FORM='UNFORMATTED')
      OPEN(NUOZ_RAD,FILE='HISTOZ_RAD',FORM='UNFORMATTED')
#endif
#if defined relax
      OPEN(IORELAX_SPC,FILE='SPCFILE',FORM='UNFORMATTED')
      OPEN(IORELAX_TSPC,FILE='TSPCCLIM',FORM='UNFORMATTED')
      OPEN(IORELAX_TGRD,FILE='TGRDCLIM',FORM='UNFORMATTED')

      OPEN(IORELAX_SST,FILE='SSTFILE',FORM='UNFORMATTED')
      OPEN(IORELAX_SICN,FILE='SICNFILE',FORM='UNFORMATTED')
      OPEN(IORELAX_SIC,FILE='SICFILE',FORM='UNFORMATTED')
      OPEN(IORELAX_SSS,FILE='SSSFILE',FORM='UNFORMATTED')

      OPEN(IORELAX_TG1,FILE='TG1FILE',FORM='UNFORMATTED')
      OPEN(IORELAX_WG1,FILE='WG1FILE',FORM='UNFORMATTED')
#endif
C
C     * DEFINE MACHINE CONSTANTS (PASSED TO I/O ROUTINES VIA COMMON
C     * BLOCK MACHTYP.
C
      MACHINE=ME32O64(IDUMMY)
      INTSIZE=INTEFLT(IDUMMY)
C
C     * DEFINE UNITS FOR MULTI-YEAR SST/ICE INPUT FILES AND SET
C     * MYRSSTI SWITCH AND "AN" ACCESS UNITS ACCORDINGLY.
C
#if defined myrssti
      MYRSSTI=1
C
      DATA LUGC   /50/
      DATA LUGT   /51/
      DATA LUSIC  /52/
C
      OPEN(LUGC,FILE='NUGC',FORM='UNFORMATTED')
      OPEN(LUGT,FILE='NUGT',FORM='UNFORMATTED')
      OPEN(LUSIC,FILE='NUSIC',FORM='UNFORMATTED')
C
      NUGC=LUGC
      NUGT=LUGT
      NUSIC=LUSIC
#else
      MYRSSTI=0
      NUGC=NUGG
      NUGT=NUGG
      NUSIC=NUGG
#endif
C
      DATA LUXSFX  /53/
      OPEN(LUXSFX,FILE='NUXSFX',FORM='UNFORMATTED')
      NUXSFX=LUXSFX
C
#if (defined(pla) && defined(pam))
      DATA NUCOA /77/
      DATA NUACT /78/
      DATA NURAD1/79/
      DATA NURAD2/80/
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

