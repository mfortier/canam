!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      subroutine INIT_OZONE_SCENARIO(irefyro,mynode)
c***********************************************************************
c Read ozone scenario value from a file named OZONE_SCENARIO
c
c Input
c   integer*4 mynode  ...mpi node (used to restrict I/O to node 0)
c
c Output
c   integer   irefyro ...Reference year to be used in ozone concentration
c                        scenario (if zero, climatology used).
c
c Mike Lazare ...Sep 16,2009
c***********************************************************************
      use ozone_defs

      implicit none

      !--- Input
      integer*4 :: mynode	!<Variable description\f$[units]\f$

      !--- Output
      integer :: irefyro	!<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      !--- Local
      integer :: iu, newunit
      logical :: ok
      integer, parameter :: verbose=2
c-----------------------------------------------------------------------
      !--- Read ozone control parmsub parameters from a namelist
      inquire(file='OZONE_CONFIG',exist=ok)
      if (ok) then
        iu=newunit(0)
        open(iu,file='OZONE_CONFIG',form='formatted')
        read(iu,nml=ozone_config)
        close(iu)
        if (mynode.eq.0) then
          write(6,*)' '
          write(6,ozone_config)
          write(6,*)' '
          call flush(6)
        endif
      else
        write(6,*)'Namelist file OZONE_CONFIG is missing.'
        call xit("INIT_OZONE_SCENARIO",-1)
      endif

      !--- determine irefyro from value in namelist.
      irefyro=0
      if(ozone_scn_mode.lt.0)  then
        irefyro=(abs(ozone_scn_mode))/100 
      endif

      if (mynode.eq.0 .and. verbose.gt.1) then
        write(6,*)' '
        write(6,*)' ozone_scn_mode,irefyro=',
     &              ozone_scn_mode,irefyro
      endif

      end subroutine init_ozone_scenario
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
