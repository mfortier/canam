!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE LAYER_RH_DIAG(RH,T,Q,PRESSG,FMASK,ILG,LAY,IL1,IL2)
C
C     * MAR 31, 2010 - J. COLE
C
C     * CALCULATES RELATIVE HUMIDITY BASED ON INPUT
C     * TEMPERATURE, SPECIFIC HUMIDITY AND SURFACE PRESSURE.
C     * THE FORMULAE USED HERE ARE CONSISTENT WITH THAT USED ELSEWHERE
C     * IN THE GCM PHYSICS.
C     * THIS SUBROUTINE IS EFFECTIVELY IDENTICAL TO SCREENRH EXCEPT
C     * IT COMPUTE THE RELATIVE HUMIDITY FOR EACH MODEL LAYER
C
      IMPLICIT NONE
C
C     * OUTPUT FIELD:
C
      REAL,   DIMENSION(ILG,LAY)  :: RH         !<Variable description\f$[units]\f$
C
C     * INPUT FIELDS.
C
      REAL,   DIMENSION(ILG)      :: PRESSG     !<Variable description\f$[units]\f$
      REAL,   DIMENSION(ILG,LAY)  :: T          !<Variable description\f$[units]\f$
      REAL,   DIMENSION(ILG,LAY)  :: Q          !<Variable description\f$[units]\f$
      REAL,   DIMENSION(ILG,LAY)  :: FMASK      !<Variable description\f$[units]\f$

      REAL FACTE,EPSLIM,FRACW,ETMP,ESTREF,ESAT,QSW
      REAL A,B,EPS1,EPS2,T1S,T2S,AI,BI,AW,BW,SLP
      REAL RW1,RW2,RW3,RI1,RI2,RI3
      REAL ESW,ESI,ESTEFF,TTT,UUU
C
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER LAY       !<Variable description\f$[units]\f$
      INTEGER IL,L
      INTEGER IL1       !<Variable description\f$[units]\f$
      INTEGER IL2       !<Variable description\f$[units]\f$

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C     * COMMON BLOCKS FOR THERMODYNAMIC CONSTANTS.
C
      COMMON /EPS /  A,B,EPS1,EPS2
      COMMON /HTCP/  T1S,T2S,AI,BI,AW,BW,SLP
C
C     * PARAMETERS USED IN NEW SATURATION VAPOUR PRESSURE FORMULATION.
C
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
C     * COMPUTES THE SATURATION VAPOUR PRESSURE OVER WATER OR ICE.
C
      ESW(TTT)        = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)        = EXP(RI1+RI2/TTT)*TTT**RI3
      ESTEFF(TTT,UUU) = UUU*ESW(TTT) + (1.-UUU)*ESI(TTT)
C========================================================================
      EPSLIM=0.001
      FACTE=1./EPS1-1.
      DO L = 1, LAY
         DO IL=IL1,IL2
            IF(FMASK(IL,L).GT.0.)                                   THEN
C
C       * COMPUTE THE FRACTIONAL PROBABILITY OF WATER PHASE
C       * EXISTING AS A FUNCTION OF TEMPERATURE (FROM ROCKEL,
C       * RASCHKE AND WEYRES, BEITR. PHYS. ATMOSPH., 1991.)
C
               FRACW = MERGE( 1.,
     1                    0.0059+0.9941*EXP(-0.003102*(T1S-T(IL,L))**2),
     2                    T(IL,L).GE.T1S )
C
               ETMP=ESTEFF(T(IL,L),FRACW)
               ESTREF=0.01*PRESSG(IL)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
               IF ( ETMP.LT.ESTREF ) THEN
                  ESAT=ETMP
               ELSE
                  ESAT=ESTREF
               ENDIF
C
               QSW=EPS1*ESAT/(0.01*PRESSG(IL)-EPS2*ESAT)
               RH(IL,L)=MIN(MAX((Q(IL,L)*(1.+QSW*FACTE))
     1                 /(QSW*(1.+Q(IL,L)*FACTE)),0.),1.)
            ENDIF
         ENDDO ! IL
      ENDDO ! L
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
