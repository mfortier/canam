!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

C
C     * NOV 19, 2012 - K.VONSALZEN.  NEW VERSION FOR GCM17+:
C     *                              CLEAN-UP AND MERGING WITH PLA CODE.
C     * APR 27, 2010 - K.VONSALZEN.  PREVIOUS VERSION INTEMI UP TO GCM16.
C     * INTERPOLATE TRACER EMISSION.
C---------------------------------------------------------------------
C     * INTERPOLATE BASIC TRACER EMISSION ARRAYS.
C
      CALL INTCAC(EOSTROW,EOSTROL,
     1            IL1,IL2,ILG,ILEV,DELT,GMT,IDAY,MDAYT)
C
#if defined transient_aerosol_emissions
      CALL INTPRF(FBBCROW,FBBCROL,FAIRROW,FAIRROL,
     1            ILG,IL1,IL2,LEVWF,LEVAIR,DELT,GMT,IDAY,MDAYT)
#endif
C---------------------------------------------------------------------
#if defined emists
#ifndef pla
C     * INITIALIZE 2D TRACER EMISSION ARRAYS.
C
      X2DEMISA(IL1:IL2,:)=0.
      X2DEMISS(IL1:IL2,:)=0.
      X2DEMISK(IL1:IL2,:)=0.
      X2DEMISF(IL1:IL2,:)=0.
C
#endif
#endif
#if defined emists
C     * INTERPOLATE EMISSIONS.
C
      CALL INTTEMI2(SAIRROW,SAIRROL,SSFCROW,SSFCROL,SBIOROW,
     1              SBIOROL,SSHIROW,SSHIROL,SSTKROW,SSTKROL,
     2              SFIRROW,SFIRROL,OAIRROW,OAIRROL,OSFCROW,
     3              OSFCROL,OBIOROW,OBIOROL,OSHIROW,OSHIROL,
     4              OSTKROW,OSTKROL,OFIRROW,OFIRROL,BAIRROW,
     5              BAIRROL,BSFCROW,BSFCROL,BBIOROW,BBIOROL,
     6              BSHIROW,BSHIROL,BSTKROW,BSTKROL,BFIRROW,
     7              BFIRROL,ILG,IL1,IL2,DELT,GMT,IDAY,MDAYT)
#endif
#if defined emists
#ifndef pla
C
C---------------------------------------------------------------------
C     * FILL 2D TRACER EMISSSION ARRAYS WITH INTERPOLATED RESULTS.
C
      X2DEMISA(IL1:IL2,ISO2)=SAIRROW(IL1:IL2)
      X2DEMISS(IL1:IL2,ISO2)=SSFCROW(IL1:IL2)
      X2DEMISK(IL1:IL2,ISO2)=SSTKROW(IL1:IL2)
      X2DEMISF(IL1:IL2,ISO2)=SFIRROW(IL1:IL2)
C
      X2DEMISA(IL1:IL2,IOCO)=.5*OAIRROW(IL1:IL2)
      X2DEMISS(IL1:IL2,IOCO)=.5*OSFCROW(IL1:IL2)
      X2DEMISK(IL1:IL2,IOCO)=.5*OSTKROW(IL1:IL2)
      X2DEMISF(IL1:IL2,IOCO)=.5*OFIRROW(IL1:IL2)
      X2DEMISA(IL1:IL2,IOCY)=.5*OAIRROW(IL1:IL2)
      X2DEMISS(IL1:IL2,IOCY)=.5*OSFCROW(IL1:IL2)
      X2DEMISK(IL1:IL2,IOCY)=.5*OSTKROW(IL1:IL2)
      X2DEMISF(IL1:IL2,IOCY)=.5*OFIRROW(IL1:IL2)
C
      X2DEMISA(IL1:IL2,IBCO)=.8*BAIRROW(IL1:IL2)
      X2DEMISS(IL1:IL2,IBCO)=.8*BSFCROW(IL1:IL2)
      X2DEMISK(IL1:IL2,IBCO)=.8*BSTKROW(IL1:IL2)
      X2DEMISF(IL1:IL2,IBCO)=.8*BFIRROW(IL1:IL2)
      X2DEMISA(IL1:IL2,IBCY)=.2*BAIRROW(IL1:IL2)
      X2DEMISS(IL1:IL2,IBCY)=.2*BSFCROW(IL1:IL2)
      X2DEMISK(IL1:IL2,IBCY)=.2*BSTKROW(IL1:IL2)
      X2DEMISF(IL1:IL2,IBCY)=.2*BFIRROW(IL1:IL2)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
