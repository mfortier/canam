!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE OCEANG (SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT, 
     1                   REFGAT,  BCSNGAT, GCGAT,
     2                   SNOWGAT, TAGAT,
     3                   ULGAT,   VLGAT,   CSZGAT,  PRESGAT, 
     4                   FSGGAT,  FLGGAT,  QAGAT,   THLGAT,
     5                   VMODGAT, GUSTGAT,
     6                   ZRFMGAT, ZRFHGAT, ZDMGAT,  ZDHGAT,
     +                   DEPBGAT,  SPCPGAT,  RHSIGAT,  PREGAT,
     7                   FSDGAT,  FSFGAT,  CSDGAT,  CSFGAT, 
     8                   WRKAGAT, WRKBGAT, CLDTGAT,
     9                   IWMOS,   JWMOS,
     A                   NMW,NL,NM,ILG,NBS,
     B                   SNOROT,  GTROT,   ANROT,   RHONROT, TNROT, 
     C                   REFROT,  BCSNROT, GCROT,
     D                   SNOWROW, TAROW,
     E                   ULROW,   VLROW,   CSZROW,  PRESROW, 
     F                   FSGROT,  FLGROT,  QAROW,   THLROW,
     G                   VMODL,   GUSTROL,
     H                   ZRFMROW, ZRFHROW, ZDMROW,  ZDHROW,
     +                   DEPBROW,  SPCPROW,  RHSIROW,  PREROW,
     I                   FSDROT,  FSFROT,  CSDROT,  CSFROT,
     J                   WRKAROL, WRKBROL, CLDTROL)
C
C     * Aug 07, 2017 - M.Lazare. New Git version.
C     * Mar 03, 2015 - M.Lazare. New version called by sfcproc2
C     *                          in new model version gcm18. This
C     *                          is modelled on "classg" since
C     *                          ocean is now tiled as well.
C     *                          - ZRFM,ZRFH,ZDM,ZDH added.
C     *                          - SGL,SHL removed as no longer needed
C     *                            in OIFLUX11.
C     *                          - TISL,MASK removed and
C     *                            GT,GUST added.
C     *                          - WRKA,WRKB,CLDT added for ocean albedo 
C     *                            calculation.
C     *                          - DSIC,GTA,GTP,SIC,SICN removed.
C     *                          - RAIN,RES,ILSL,JL removed.
C     * Nov 15, 2013 - M.Lazare. Previous version "oceang" called by
C     *                          "sfcproc" in previous model version gcm17.
C     *                          This differs from the pre-Nov 15/2013
C     *                          version in that ROFN has been removed.
C     * NOTE: This contains the following changes compared to the
C     *       working temporary version used in conjunction with
C     *       updates to gcm16 (ie not official):
C     *         1) {TN,RHON,REF,BCSN,TISL} added for Maryam's
C     *            contribution to the new snow albedo formulation.
C     *         2) No more SNOROT,ANROT (SNOROW,ANROW still there)
C     *            since these are for land only.
C     *         3) {FSO,FSF,CSD,CSF| now 2-D arrays since defined
C     *            over 4 bands (hence NBS passed in).
C--------------------------------------------------------------------
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER  NMW      !<Variable description\f$[units]\f$	
      INTEGER  NL       !<Variable description\f$[units]\f$	
      INTEGER  NM       !<Variable description\f$[units]\f$	
      INTEGER  ILG      !<Variable description\f$[units]\f$	
      INTEGER  K        !<Variable description\f$[units]\f$	
      INTEGER  L        !<Variable description\f$[units]\f$	
      INTEGER  M        !<Variable description\f$[units]\f$	
      INTEGER  NBS      !<Variable description\f$[units]\f$	
      INTEGER  IOCEAN   !<Variable description\f$[units]\f$	
      INTEGER  IB       !<Variable description\f$[units]\f$	
C
C     * OCEAN PROGNOSTIC VARIABLES.
C
      REAL, DIMENSION(NL,NM) :: SNOROT  !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: GTROT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: ANROT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: RHONROT !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: TNROT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: REFROT  !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: BCSNROT !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: GCROT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: FSGROT  !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM) :: FLGROT  !<Variable description\f$[units]\f$	

      REAL, DIMENSION(ILG)   :: SNOGAT  !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: GTGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: ANGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: RHONGAT !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: TNGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: REFGAT  !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: BCSNGAT !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG)   :: GCGAT   !<Variable description\f$[units]\f$	
C
C     * ATMOSPHERIC AND GRID-CONSTANT INPUT VARIABLES.
C
      REAL, DIMENSION(NL,NM,NBS) :: FSDROT      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM,NBS) :: FSFROT      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM,NBS) :: CSDROT      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NM,NBS) :: CSFROT      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NBS)    :: WRKAROL     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL,NBS)    :: WRKBROL     !<Variable description\f$[units]\f$	

      REAL, DIMENSION(NL) :: SNOWROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: TAROW      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: ULROW      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: VLROW      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: CSZROW     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: CLDTROL    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: PRESROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: QAROW      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: THLROW     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: VMODL      !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: GUSTROL    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: ZRFMROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: ZRFHROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: ZDMROW     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: ZDHROW     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: DEPBROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: SPCPROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: RHSIROW    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(NL) :: PREROW     !<Variable description\f$[units]\f$	

      REAL, DIMENSION(ILG,NBS) :: FSDGAT        !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG,NBS) :: FSFGAT        !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG,NBS) :: CSDGAT        !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG,NBS) :: CSFGAT        !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG,NBS) :: WRKAGAT       !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG,NBS) :: WRKBGAT       !<Variable description\f$[units]\f$	
 
      REAL, DIMENSION(ILG) :: SNOWGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: TAGAT     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: ULGAT     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: VLGAT     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: CSZGAT    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: CLDTGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: PRESGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: FSGGAT    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: FLGGAT    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: QAGAT     !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: THLGAT    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: VMODGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: GUSTGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: ZRFMGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: ZRFHGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: ZDMGAT    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: ZDHGAT    !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: DEPBGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: SPCPGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: RHSIGAT   !<Variable description\f$[units]\f$	
      REAL, DIMENSION(ILG) :: PREGAT    !<Variable description\f$[units]\f$	
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  IWMOS (ILG)      !<Variable description\f$[units]\f$	
      INTEGER  JWMOS  (ILG)     !<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C--------------------------------------------------------------------
      DO 100 K=1,NMW
          SNOGAT (K)=SNOROT (IWMOS(K),JWMOS(K))  
          GTGAT  (K)=GTROT  (IWMOS(K),JWMOS(K))  
          ANGAT  (K)=ANROT  (IWMOS(K),JWMOS(K))  
          RHONGAT(K)=RHONROT(IWMOS(K),JWMOS(K))  
          TNGAT  (K)=TNROT  (IWMOS(K),JWMOS(K))  
          REFGAT (K)=REFROT (IWMOS(K),JWMOS(K))  
          BCSNGAT(K)=BCSNROT(IWMOS(K),JWMOS(K))  
          GCGAT  (K)=GCROT  (IWMOS(K),JWMOS(K))
          FSGGAT (K)=FSGROT (IWMOS(K),JWMOS(K))
          FLGGAT (K)=FLGROT (IWMOS(K),JWMOS(K))
  100 CONTINUE
C
      DO 200 K=1,NMW
          SNOWGAT  (K)=SNOWROW  (IWMOS(K))
          TAGAT    (K)=TAROW    (IWMOS(K))
          ULGAT    (K)=ULROW    (IWMOS(K))
          VLGAT    (K)=VLROW    (IWMOS(K))
          CSZGAT   (K)=CSZROW   (IWMOS(K))
          CLDTGAT  (K)=CLDTROL  (IWMOS(K))
          PRESGAT  (K)=PRESROW  (IWMOS(K))
          QAGAT    (K)=QAROW    (IWMOS(K))
          THLGAT   (K)=THLROW   (IWMOS(K))
          VMODGAT  (K)=VMODL    (IWMOS(K))    
          GUSTGAT  (K)=GUSTROL  (IWMOS(K))
          ZRFMGAT  (K)=ZRFMROW  (IWMOS(K))
          ZRFHGAT  (K)=ZRFHROW  (IWMOS(K))
          ZDMGAT   (K)=ZDMROW   (IWMOS(K))
          ZDHGAT   (K)=ZDHROW   (IWMOS(K))
          DEPBGAT  (K)=DEPBROW  (IWMOS(K))
          SPCPGAT  (K)=SPCPROW  (IWMOS(K))
          RHSIGAT  (K)=RHSIROW  (IWMOS(K))
          PREGAT   (K)=PREROW   (IWMOS(K))
  200 CONTINUE
C
      DO 300 IB=1,NBS
      DO 300 K=1,NMW
            FSDGAT (K,IB) = FSDROT (IWMOS(K),JWMOS(K),IB)
            FSFGAT (K,IB) = FSFROT (IWMOS(K),JWMOS(K),IB)
            CSDGAT (K,IB) = CSDROT (IWMOS(K),JWMOS(K),IB)
            CSFGAT (K,IB) = CSFROT (IWMOS(K),JWMOS(K),IB)
            WRKAGAT(K,IB) = WRKAROL(IWMOS(K),IB)
            WRKBGAT(K,IB) = WRKBROL(IWMOS(K),IB)
  300 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
