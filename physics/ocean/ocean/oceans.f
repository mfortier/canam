!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE OCEANS (SNOROT,  GTROT,   ANROT,   TNROT,
     1                   RHONROT, BCSNROT, REFROT, EMISROT,
     2                   CDMROT,  QGROT,   HFSROT,  QFSROT, FNROT, 
     +                   SALBROT, CSALROT,
     3                   IWMOS,   JWMOS,
     4                   NMW,NL,NM,ILG,NBS,
     5                   SNOGAT,  GTGAT,   ANGAT,   TNGAT,
     6                   RHONGAT, BCSNGAT, REFGAT, EMISGAT,
     7                   CDMGAT,  QGGAT,   HFSGAT,  QFSGAT, FNGAT,
     +                   SALBGAT, CSALGAT)
C
C     * Jun 20, 2014 - M.Lazare. New version called by "sfcproc3"
C     *                          in new model version gcm18. This
C     *                          is modelled on "classs" since
C     *                          ocean is now tiled as well.
C     *                          - Adds SALBGAT/SALBROT, CSALGAT/CSALROT 
C     *                            and  EMISGAT/EMISROT.
C     *                          - {SNO,GT,AN,TN,RHON,BCSN,REF} now tiled.
C     *                          - Most output diagnostic fields removed
C     *                            since now accumulated over ocean tile(s)
C     *                            after call to this routine.
C     *                          - GC removed.
C     * Nov 15, 2013 - M.Lazare. Previous version "oceans" called by
C     *                          "sfcproc" in previous model version gcm17.
C     * Nov 15, 2013 - M.Lazare. ROFN now accumulated while
C                                scattering, not instantaneous.
C     * Jun 11, 2013 - M.Lazare. Ocean scatter routine called by 
C     *                          "sfcproc" in new version gcm17.
C     * NOTE: This contains the following changes compared to the
C     *       working temporary version used in conjunction with
C     *       updates to gcm16 (ie not official):
C     *         1) {TN,RHON,FN,TISL,ROFN} added for Maryam's
C     *            contribution to the new snow albedo formulation.
C     *         2) No more SNOROT,ANROT (SNOROW,ANROW still there)
C     *            since these are for land only.
C     *         3) Instantaneous fields ZNROL,SMLTROL scattered
C     *            instead of accumulated ZNROW,SMLTROW, since
C     *            these are now done in the physics driver. 
C--------------------------------------------------------------------
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER NMW       !<Variable description\f$[units]\f$
      INTEGER NL        !<Variable description\f$[units]\f$
      INTEGER NM        !<Variable description\f$[units]\f$
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER K         !<Variable description\f$[units]\f$
      INTEGER L         !<Variable description\f$[units]\f$
      INTEGER M         !<Variable description\f$[units]\f$
      INTEGER NBS       
C
C     * OCEAN PROGNOSTIC VARIABLES.
C
      REAL, DIMENSION(NL,NM,NBS) :: SALBROT     
      REAL, DIMENSION(NL,NM,NBS) :: CSALROT     !<Variable description\f$[units]\f$

      REAL, DIMENSION(ILG,NBS) :: SALBGAT       !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,NBS) :: CSALGAT       !<Variable description\f$[units]\f$

      REAL, DIMENSION(NL,NM) :: SNOROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: GTROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: ANROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: RHONROT !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: TNROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: BCSNROT !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: REFROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: EMISROT !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: CDMROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: QGROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: HFSROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: QFSROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: FNROT   !<Variable description\f$[units]\f$

      REAL, DIMENSION(ILG)   :: SNOGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: GTGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: ANGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: RHONGAT !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: TNGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: BCSNGAT !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: REFGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: EMISGAT !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: CDMGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: QGGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: HFSGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: QFSGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: FNGAT   !<Variable description\f$[units]\f$
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  IWMOS  (ILG)     !<Variable description\f$[units]\f$
      INTEGER  JWMOS (ILG)      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C----------------------------------------------------------------------
      DO 100 K=1,NMW
          SNOROT (IWMOS(K),JWMOS(K))=SNOGAT (K)  
          GTROT  (IWMOS(K),JWMOS(K))=GTGAT  (K)  
          ANROT  (IWMOS(K),JWMOS(K))=ANGAT  (K)  
          TNROT  (IWMOS(K),JWMOS(K))=TNGAT  (K)  
          RHONROT(IWMOS(K),JWMOS(K))=RHONGAT(K)  
          BCSNROT(IWMOS(K),JWMOS(K))=BCSNGAT(K) 
          REFROT (IWMOS(K),JWMOS(K))=REFGAT (K) 
          EMISROT(IWMOS(K),JWMOS(K))=EMISGAT(K) 
          CDMROT (IWMOS(K),JWMOS(K))=CDMGAT (K)
          QGROT  (IWMOS(K),JWMOS(K))=QGGAT  (K)
          HFSROT (IWMOS(K),JWMOS(K))=HFSGAT (K)
          QFSROT (IWMOS(K),JWMOS(K))=QFSGAT (K)
          FNROT  (IWMOS(K),JWMOS(K))=FNGAT  (K)
  100 CONTINUE
C
      DO 200 L=1,NBS
      DO 200 K=1,NMW
          SALBROT(IWMOS(K),JWMOS(K),L)=SALBGAT(K,L)
          CSALROT(IWMOS(K),JWMOS(K),L)=CSALGAT(K,L)
  200 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
