!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INTFLD(ROW,ROL,IL1,IL2,ILG,DELT,GMT,IDAY,MDAY)
C
C     * MAY 03/14 - M.LAZARE. NEW ROUTINE FOR GCM18+:
C     *                       - BASED ON INTGTIO2 BUT NO
C     *                         DEPENDANCE ON GCROW
C     *                         (HENCE "IMASK" REMOVED ALSO).
C     *                         MADE GENERAL FOR ANY FIELD BY
C     *                         REMOVING REFERENCES TO "GT".
C     * APR 29/03 - M.LAZARE. PREVIOUS VERSION INTGTIO2:
C     *                       1,LON -> IL1,IL2.
C     * OCT 16/97 - M.LAZARE. PREVIOUS VERSION INTGTIO.
C
C     * INPUT/OUTPUT:
C     * ROW  = CURRENT VALUE ARRAY.
C     * INPUT:
C     * ROL  = TARGET VALUE ARRAY (VALID AT MDAY).
C
C     * IL1    = START LONGITUDE INDEX.
C     * IL2    = END   LONGITUDE INDEX.
C     * ILG    = DIMENSION OF LONGITUDE ARRAYS.
C     * DELT   = MODEL TIMESTEP IN SECONDS. 
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.  
C     * MDAY   = DATE OF TARGET FIELD.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)                                         
 
      REAL ROW(ILG)     !<Variable description\f$[units]\f$
      REAL ROL(ILG)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY. 
C
      DAY=REAL(IDAY)+GMT/86400. 
      FMDAY=REAL(MDAY) 
      IF(FMDAY.LT.DAY) FMDAY=FMDAY+365. 
      DAYSM=FMDAY-DAY 
      STEPSM=DAYSM*86400./DELT
C 
C     * GENERAL INTERPLATION.
C 
      DO 210 I=IL1,IL2
        ROW  (I)  = ((STEPSM-1.) * ROW  (I) + ROL  (I)) / STEPSM      
  210 CONTINUE
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
