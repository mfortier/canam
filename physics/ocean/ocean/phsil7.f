!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PHSIL7 (  GTROW,  GTROL,  GTROM,
     1                    SICROW, SICROL, SICROM,
     2                   SICNROW,SICNROL,SICNROM,
     3                   MASKROW,  GCROW,DSICROW,
     4                   ILG,IL1,IL2,DELT,GMT,IDAY,MDAY,SICN_CRT)
C
C     * MAR 03/17 - S.KHARIN. NEW VERSION FOR GCM19 ONWARD:
C     *                       - SIMPLIFIED TO REMOVE PREVIOUS TRACKING
C     *                         OF SICN INTERSECTING SICN_CRT TO 
C     *                         ENSURE CONSISTENCIES IN FIELDS WHEN
C     *                         GROUND COVER CHANGES (NO SUCH
C     *                         CRITERIA IN NEW MODEL VERSION).
C     *                       - RESTRICT SIC FROM ABOVE WHEN SICN<=0.15.
C     * APR 23/10 - M.LAZARE. PREVIOUS VERSION PHSIL7 FOR GCM15I->GCM18:
C     *                       - SET GTROW=GTROL WHEN END INTERPOLATION
C     *                         TARGET DATE IS REACHED, FOR TRANSITION
C     *                         FROM SEAICE TO WATER, TO AVOID ROUNDOFF
C     *                         ERROR.
C     * APR 02/08 - M.LAZARE. REVISED (COSMETIC) VERSION PHSIL5 
C     *                       FOR GCM15F/G/H:
C     *                       - INITIALIZE DSICROW TO ZERO!
C     * JUN 15/06 - M.LAZARE. PREVIOUS VERSION PHSIL5 FOR GCM15F:
C     *                       - "ZERO","ONE" DATA CONSTANTS ADDED AND
C     *                         USED IN CALLS TO INTRINSICS.
C     * NOV 01/04 - M.LAZARE. PEVIOUS VERSION PHSIL4 UP TO AND INCL GCM15E.
C     *                       -  COSMETIC CHANGE: FORCE SICN=0. OVER LAND.
C     * JUN 10/03 - MLAZARE.  CHANGE 1,LON TO IL1,IL2 (PASSED IN).  
C     * MAR 17/99 - M.LAZARE. PREVIOUS VERSION PHSIL3.
C
C     * GTROW  = GROUND TEMPERATURE    FOR PREVIOUS TIMESTEP. 
C     * GTROL  = GROUND TEMPERATURE    AT NEXT PHYSICS TIME.
C     * GTROM  = GROUND TEMPERATURE    AT PREVIOUS PHYSICS TIME.
C     * SICROW = SEA-ICE MASS          FOR PREVIOUS TIMESTEP.
C     * SICROL = SEA-ICE MASS          AT NEXT PHYSICS TIME.
C     * SICROM = SEA-ICE MASS          AT PREVIOUS PHYSICS TIME.
C     * SICNROW= SEA-ICE CONCENTRATION FOR PREVIOUS TIMESTEP.
C     * SICNROL= SEA-ICE CONCENTRATION AT NEXT PHYSICS TIME.
C     * SICNROM= SEA-ICE CONCENTRATION AT PREVIOUS PHYSICS TIME.
C     * MASKROW= LAND MASK (INVARIANT; -1=LAND, 0 OTHERWISE). 
C     * GCROW  = GROUND COVER (-1.,0,+1.).
C     * DSICROW= CHANGE IN SEA-ICE MASS OVER TIMESTEP, TO BE
C     *          USED IN CALCULATION OF ENERGY BALANCE AT
C     *          BOTTOM OF SEA-ICE IN ROUTINE OIFPST3.
C
C     * ILG    = SIZE OF LONGITUDE SLICE.
C     * IL1    = START OF LONGITUDE SLICE.
C     * IL2    = END OF LONGITUDE SLICE.
C     * DELT   = MODEL TIMESTEP IN SECONDS. 
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.  
C     * DAY    = CURRENT TIME (IDAY PLUS TIME OF DAY).  
C     * MDAY   = DATE OF NEXT      MID-MONTH PHYSICS. 
C     * MDAYM  = DATE OF PREVIOUS  MID-MONTH PHYSICS. 
C     * SICN_CRT=THRESHOLD OF SEA-ICE FRACTION ABOVE WHICH POINT IS 
C     *          CONSIDERED TO BE ICE-COVERED. 
C 
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   GTROW(ILG)!<Variable description\f$[units]\f$	
      REAL   GTROL(ILG)!<Variable description\f$[units]\f$	
      REAL   GTROM(ILG)!<Variable description\f$[units]\f$	
      REAL  SICROW(ILG)!<Variable description\f$[units]\f$	
      REAL  SICROL(ILG)!<Variable description\f$[units]\f$	
      REAL  SICROM(ILG)!<Variable description\f$[units]\f$	
      REAL SICNROW(ILG)!<Variable description\f$[units]\f$	
      REAL SICNROL(ILG)!<Variable description\f$[units]\f$	
      REAL SICNROM(ILG)!<Variable description\f$[units]\f$	
      REAL MASKROW(ILG)!<Variable description\f$[units]\f$	
      REAL   GCROW(ILG)!<Variable description\f$[units]\f$	
      REAL DSICROW(ILG)!<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      INTEGER MMD(12) 
C
      COMMON /PARAM3/ CSNO,   CPACK, GTFSW,  RKHI, SBC,  SNOMAX
      COMMON /PARAM5/ CONI,   DENI,  SIC_MIN,DFSIC,CONF
C
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
      DATA ZERO,ONE / 0., 1. /
C-------------------------------------------------------------------- 
      M=0
      DO 150 N=1,12
       IF(MDAY.EQ.MMD(N))M=N
  150 CONTINUE
      IF(M.EQ.0)CALL XIT('PHSIL7',-1)
      MM1=M-1
      IF(MM1.EQ.0)MM1=12
      MDAYM=MMD(MM1)
C
C     * COMPUTE THE TIME DIFFERENCES.
C
      DAY=REAL(IDAY)+(GMT+DELT)/86400.0
      DT1=DAY-REAL(MDAYM)
      DT2=REAL(MDAY)-DAY
      DT3=REAL(MDAY-MDAYM)
      IF(DT1.LT.0.0)DT1=DT1+365.0
      IF(DT2.LT.0.0)DT2=DT2+365.0
      IF(DT3.LT.0.0)DT3=DT3+365.0
C
      DO 210 I=IL1,IL2
C                         
C     * CASE 1: OVER LAND (NOTE: FORCE SEA-ICE CONCENTRATIONS TO ZERO)
C                         
       IF(MASKROW(I).LT.-0.5)THEN
        GCROW(I)=-1.0 
        SICNROW(I)=0.          
        DSICROW(I)=0.
       ELSE      
        IF(SICNROW(I).GT.SICN_CRT)THEN
         GCROW(I)=1.0
        ELSE
         GCROW(I)=0.0
        ENDIF
C
C       * INTERPOLATED SST
C
        GTROW(I)=GTROM(I)+(GTROL(I)-GTROM(I))*DT1/DT3
C
C       * CLIP AT FREEZING POINT
C
        GTROW(I)=MAX(GTROW(I),GTFSW)
C
C       * INTERPOLATED SICN
C
        SICNROW(I)=SICNROM(I)+(SICNROL(I)-SICNROM(I))*DT1/DT3
C
C       * CLIP AT (0...1)
C
        SICNROW(I)=MAX(ZERO,MIN(SICNROW(I),ONE)) 
C
C       * INTERPOLATED SIC
C
        SICOLD=SICROW(I)
        SICROW(I)=SICROM(I)+(SICROL(I)-SICROM(I))*DT1/DT3
        IF(SICNROW(I).GT.0.15)THEN
C
C       * RESTRICT FROM BELOW WHEN SICN>0.15
C
         SICROW(I)=MAX(SICROW(I),SIC_MIN)
        ELSE
C
C       * RESTRICT FROM ABOVE WHEN SICN<=0.15
C
         SICROW(I)=(SIC_MIN/0.15)*SICNROW(I)
        ENDIF
        DSICROW(I)=SICROW(I)-SICOLD
       ENDIF
  210 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
