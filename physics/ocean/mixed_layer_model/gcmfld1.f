      SUBROUTINE GCMFLD1(BEG,GC,GT,SIC,IBUF,
     1                   ICEMOD,IDAY,KSTEPS,MAXX,NFB)
C 
C     * JAN 30/89 - M.LAZARE. - REVISE 4HNAME OF BEG FIELD IN GETFLD2 
C     *                         CALL TO PROPER 4HOBEG.
C     * OCT 11,1984 T.CARRIERES 
C     * GETS THE FOLLOWING AGCM PRODUCED FIELDS FROM THE INTERACTIVE
C     * FILE: AVERAGED SURFACE ENERGY FLUX , GROUND COVER , SURFACE 
C     * TEMPERATURE AND SEA ICE AMOUNT FIELDS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL BEG(LON,LY),GC(LON,LY),GT(LON,LY)
      REAL SIC(LON,LY)
      INTEGER IBUF(8) 
      LOGICAL OK
      COMMON/DIM/ LON,LX,LY 
      DATA NPPS,NPGG/2,6/ 
C---------------------------------------------------------------------
C 
C     * GET SURFACE ENERGY FLUX FIELD.
C 
      CALL GETFLD2(-NFB,BEG,NC4TO8("GRID"),-1,NC4TO8("OBEG"),1,IBUF,
     1                                                      MAXX,OK)
      WRITE(6,6000) IBUF
      IF(.NOT.OK) CALL XIT('GCMFLD1',-1)
C 
C     * GET GROUND COVER FIELD. 
C 
      CALL GETFLD2(-NFB,GC,NC4TO8("GRID"),-1,NC4TO8("  GC"),1,IBUF,
     1                                                     MAXX,OK)
      WRITE(6,6000) IBUF
      IF(.NOT.OK) CALL XIT('GCMFLD1',-2)
C 
C     * GET SURFACE TEMPERATURE FIELD.
C 
      CALL GETFLD2(-NFB,GT,NC4TO8("GRID"),-1,NC4TO8("  GT"),1,IBUF,
     1                                                     MAXX,OK)
      WRITE(6,6000) IBUF
      IF(.NOT.OK) CALL XIT('GCMFLD1',-3)
C 
C     * GET SEA ICE AMOUNT FIELD IF ICE MODEL IS BEING USED.
C 
      IF(ICEMOD.GT.0) THEN
C 
      CALL GETFLD2(-NFB,SIC,NC4TO8("GRID"),-1,NC4TO8(" SIC"),1,IBUF,
     1                                                      MAXX,OK)
      WRITE(6,6000) IBUF
      IF(.NOT.OK) CALL XIT('GCMFLD1',-4)
C 
      ENDIF 
C---------------------------------------------------------------------- 
 6000 FORMAT('0',A4,I10,2X,A4,5I6)
      RETURN
      END 
