!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE FLAKEG (SNOGAT,  GTGAT,   ANGAT,   RHONGAT, TNGAT, 
     1                   REFGAT,  BCSNGAT, GCGAT,
     2                   FSVGAT,  FSIGAT,  FDLGAT,  FSGGAT,  FLGGAT,
     3                   HLAKGAT, LLAKGAT, BLAKGAT, 
     4                   RADJGAT, LTICGAT, LTSNGAT, LDMXGAT, LTAVGAT,
     5                   LTMXGAT, LTWBGAT, LSHPGAT, LZICGAT, LZSNGAT,
     6                   SNOWGAT, TAGAT,   ULGAT,   VLGAT,   PRESGAT, 
     7                   QAGAT,   THLGAT,
     8                   VMODGAT, GUSTGAT, RHOAGAT, GTAGAT,  CSZGAT,
     9                   ZRFMGAT, ZRFHGAT, ZDMGAT,  ZDHGAT,
     A                   DEPBGAT, RPREGAT, SPCPGAT, RHSIGAT, PREGAT,
     B                   FSDBGAT, FSFBGAT, CSDBGAT, CSFBGAT, 
     C                   IKMOS,   JKMOS,
     D                   NMK,NL,NM,ILG,NBS,
     E                   SNOROT,  GTROT,   ANROT,   RHONROT, TNROT, 
     F                   REFROT,  BCSNROT, GCROT,
     G                   FSVROT,  FSIROT,  FDLROT,  FSGROT,  FLGROT,
     H                   HLAKROW, LLAKROW, BLAKROW, 
     I                   RADJROW, LTICROW, LTSNROW, LDMXROW, LTAVROW,
     J                   LTMXROW, LTWBROW, LSHPROW, LZICROW, LZSNROW,
     K                   SNOWROW, TAROW,   ULROW,   VLROW,   PRESROW, 
     L                   QAROW,   THLROW,
     M                   VMODL,   GUSTROL, RHOAROW, GTAROW,  CSZROW,
     N                   ZRFMROW, ZRFHROW, ZDMROW,  ZDHROW,
     O                   DEPBROW, RPREROW, SPCPROW, RHSIROW, PREROW,
     P                   FSDBROT, FSFBROT, CSDBROT, CSFBROT         )
C
C     * Dec 10, 2016 - M.Lazare. New version called by sfcproc2
C     *                          in new model version gcm19. This
C     *                          is modelled on "oceang", with:
C     *                          - NBS and tiled radiation removed.
C     *                          - WRKA,WRKB,CLDT,CSZ removed.
C     *                          - FSV,FSI,FDL,HLAK,LLAK,BLAK added.
C     *                          - RADJ,LTIC,LTSN,LDMX,LTAV,
C     *                            LTMX,LTWB,LSHP,LZIC,LZSN added.
C     *                          - RHOA,RPRE added.
C--------------------------------------------------------------------
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER  NMK                      !<Variable description\f$[units]\f$
      INTEGER  NL                       !<Variable description\f$[units]\f$
      INTEGER  NM                       !<Variable description\f$[units]\f$
      INTEGER  ILG                      !<Variable description\f$[units]\f$
      INTEGER  NBS                      !<Variable description\f$[units]\f$
      INTEGER  K                        !<Variable description\f$[units]\f$
      INTEGER  IB                       !<Variable description\f$[units]\f$
C
C     * OCEAN PROGNOSTIC VARIABLES.
C
      REAL, DIMENSION(NL,NM) :: SNOROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: GTROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: ANROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: RHONROT !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: TNROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: REFROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: BCSNROT !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: GCROT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: FSVROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: FSIROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: FDLROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: FSGROT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM) :: FLGROT  !<Variable description\f$[units]\f$

      REAL, DIMENSION(ILG)   :: SNOGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: GTGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: ANGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: RHONGAT !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: TNGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: REFGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: BCSNGAT !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: GCGAT   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: FSVGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: FSIGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: FDLGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: FSGGAT  !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG)   :: FLGGAT  !<Variable description\f$[units]\f$
        
      REAL, DIMENSION(NL) :: HLAKROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LLAKROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: BLAKROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: RADJROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LTICROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LTSNROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LDMXROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LTAVROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LTMXROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LTWBROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LSHPROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LZICROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: LZSNROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: SNOWROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: TAROW      !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: ULROW      !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: VLROW      !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: PRESROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: QAROW      !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: THLROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: VMODL      !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: GUSTROL    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: RHOAROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: GTAROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: CSZROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: ZRFMROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: ZRFHROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: ZDMROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: ZDHROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: DEPBROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: RPREROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: SPCPROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: RHSIROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL) :: PREROW     !<Variable description\f$[units]\f$
        
      REAL, DIMENSION(ILG):: HLAKGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LLAKGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: BLAKGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: RADJGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LTICGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LTSNGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LDMXGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LTAVGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LTMXGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LTWBGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LSHPGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LZICGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: LZSNGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: SNOWGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: TAGAT      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: ULGAT      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: VLGAT      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: PRESGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: QAGAT      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: THLGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: VMODGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: GUSTGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: RHOAGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: GTAGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: CSZGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: ZRFMGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: ZRFHGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: ZDMGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: ZDHGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: DEPBGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: RPREGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: SPCPGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: RHSIGAT    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG):: PREGAT     !<Variable description\f$[units]\f$

      REAL, DIMENSION(NL,NM,NBS) :: FSDBROT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM,NBS) :: FSFBROT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM,NBS) :: CSDBROT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(NL,NM,NBS) :: CSFBROT     !<Variable description\f$[units]\f$
        
      REAL, DIMENSION(ILG,NBS)   :: FSDBGAT     !<Variable description\f$[units]\f$     
      REAL, DIMENSION(ILG,NBS)   :: FSFBGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,NBS)   :: CSDBGAT     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,NBS)   :: CSFBGAT     !<Variable description\f$[units]\f$
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  IKMOS (ILG)                      !<Variable description\f$[units]\f$
      INTEGER  JKMOS  (ILG)                     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C--------------------------------------------------------------------
      DO 100 K=1,NMK
          SNOGAT (K)=SNOROT (IKMOS(K),JKMOS(K))  
          GTGAT  (K)=GTROT  (IKMOS(K),JKMOS(K))  
          ANGAT  (K)=ANROT  (IKMOS(K),JKMOS(K))  
          RHONGAT(K)=RHONROT(IKMOS(K),JKMOS(K))  
          TNGAT  (K)=TNROT  (IKMOS(K),JKMOS(K))  
          REFGAT (K)=REFROT (IKMOS(K),JKMOS(K))  
          BCSNGAT(K)=BCSNROT(IKMOS(K),JKMOS(K))  
          GCGAT  (K)=GCROT  (IKMOS(K),JKMOS(K))
          FSVGAT (K)=FSVROT (IKMOS(K),JKMOS(K))  
          FSIGAT (K)=FSIROT (IKMOS(K),JKMOS(K))  
          FDLGAT (K)=FDLROT (IKMOS(K),JKMOS(K))  
          FSGGAT (K)=FSGROT (IKMOS(K),JKMOS(K))  
          FLGGAT (K)=FLGROT (IKMOS(K),JKMOS(K))  
  100 CONTINUE
C
      DO 200 K=1,NMK
          HLAKGAT(K)=HLAKROW(IKMOS(K))
          LLAKGAT(K)=LLAKROW(IKMOS(K))
          BLAKGAT(K)=BLAKROW(IKMOS(K))
          RADJGAT(K)=RADJROW(IKMOS(K))
          LTICGAT(K)=LTICROW(IKMOS(K))
          LTSNGAT(K)=LTSNROW(IKMOS(K))
          LDMXGAT(K)=LDMXROW(IKMOS(K))
          LTAVGAT(K)=LTAVROW(IKMOS(K))
          LTMXGAT(K)=LTMXROW(IKMOS(K))
          LTWBGAT(K)=LTWBROW(IKMOS(K))
          LSHPGAT(K)=LSHPROW(IKMOS(K))
          LZICGAT(K)=LZICROW(IKMOS(K))
          LZSNGAT(K)=LZSNROW(IKMOS(K))
          SNOWGAT(K)=SNOWROW(IKMOS(K))
          TAGAT  (K)=TAROW  (IKMOS(K))
          ULGAT  (K)=ULROW  (IKMOS(K))
          VLGAT  (K)=VLROW  (IKMOS(K))
          PRESGAT(K)=PRESROW(IKMOS(K))
          QAGAT  (K)=QAROW  (IKMOS(K))
          THLGAT (K)=THLROW (IKMOS(K))
          VMODGAT(K)=VMODL  (IKMOS(K))    
          GUSTGAT(K)=GUSTROL(IKMOS(K))
          RHOAGAT(K)=RHOAROW(IKMOS(K))
          GTAGAT (K)=GTAROW (IKMOS(K))
          CSZGAT (K)=CSZROW (IKMOS(K))
          ZRFMGAT(K)=ZRFMROW(IKMOS(K))
          ZRFHGAT(K)=ZRFHROW(IKMOS(K))
          ZDMGAT (K)=ZDMROW (IKMOS(K))
          ZDHGAT (K)=ZDHROW (IKMOS(K))
          DEPBGAT(K)=DEPBROW(IKMOS(K))
          RPREGAT(K)=RPREROW(IKMOS(K))
          SPCPGAT(K)=SPCPROW(IKMOS(K))
          RHSIGAT(K)=RHSIROW(IKMOS(K))
          PREGAT (K)=PREROW (IKMOS(K))
  200 CONTINUE
C
      DO 300 IB=1,NBS
      DO 300 K=1,NMK
          FSDBGAT (K,IB) = FSDBROT (IKMOS(K),JKMOS(K),IB)
          FSFBGAT (K,IB) = FSFBROT (IKMOS(K),JKMOS(K),IB)
          CSDBGAT (K,IB) = CSDBROT (IKMOS(K),JKMOS(K),IB)
          CSFBGAT (K,IB) = CSFBROT (IKMOS(K),JKMOS(K),IB)
  300 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
