!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CSLMG (HLAKGAT,LLAKGAT,BLAKGAT,NLAKGAT,
     +                  TLAKGAT,T0LAKGAT,HICELGAT,HDPTHGAT,
     +                  EXPWGAT,DTMPGAT,TKELGAT,DELUGAT,GREDGAT,RHOMGAT,
     +                  SNOGAT,RHONGAT,TNGAT,ANGAT,WSNOGAT,
     +                  FSVGAT,FSIGAT,FDLGAT,FSGGAT,FLGGAT,CSZGAT,
     +                  ULGAT,VLGAT,TAGAT,QAGAT,RHOAGAT,PADRGAT,PRESGAT,
     +                  ZRFMGAT,ZRFHGAT,ZDMGAT,ZDHGAT,RPCPGAT,
     +                  TRPCGAT,SPCPGAT,TSPCGAT,RHSIGAT,RADJGAT,
     +                  ASVDGAT,ASIDGAT,FSDBGAT,FSFBGAT,FSSBGAT,
     +                  REFGAT,BCSNGAT,DEPBGAT,
     +                  IKMOS,JKMOS,NLAKMAX,NMK,NL,NM,ILG,NBS,
     +                  HLAKROW,LLAKROW,BLAKROW,NLAKROW,
     +                  TLAKROW,T0LAKROW,HICELROW,HDPTHROW,
     +                  EXPWROW,DTMPROW,TKELROW,DELUROW,GREDROW,RHOMROW,
     +                  SNOROT,RHONROT,TNROT,ANROT,WSNOROT,
     +                  FSVROT,FSIROT,FDLROT,FSGROT,FLGROT,CSZROW,
     +                  ULROW,VLROW,TAROW,QAROW,RHOAROW,PADRROW,PRESROW,
     +                  ZRFMROW,ZRFHROW,ZDMROW,ZDHROW,RPCPROW,
     +                  TRPCROW,SPCPROW,TSPCROW,RHSIROW,RADJROW,
     +                  ASVDROT,ASIDROT,FSDBROT,FSFBROT,FSSBROT,
     +                  REFROT,BCSNROT,DEPBROW  )
C
C     * DEC 04/16 - M.LAZARE. - REMOVE TILE DIMENSION FOR SEVERAL LAKE FIELDS.
C     *                       - CHANGE "ROT" -> "ROW" FOR NON-TILED FIELDS.
C     *                       - RENAMED LAKEG-> CSLMG.
C     *                       - ADD "DEPB".
C     * APR 13/15 - D.VERSEGHY. LOCAL VERSION FOR OFFLINE TESTING.
C     * JAN 18/12 - M.MACKAY.  GATHER OPERATION FOR LAKE TILE VARIABLES.
C
      IMPLICIT NONE
C
      INTEGER NMK
      INTEGER NL
      INTEGER NM
      INTEGER ILG
      INTEGER K
      INTEGER L
      INTEGER NLAKMAX
      INTEGER NBS
C
      INTEGER  IKMOS (ILG)
      INTEGER  JKMOS  (ILG)                           
C
C     * LAKE AND SNOW FIELDS.
C
      REAL HLAKROW (NL)         !<Variable description\f$[units]\f$
      REAL LLAKROW (NL)         !<Variable description\f$[units]\f$
      REAL BLAKROW (NL)         !<Variable description\f$[units]\f$
      REAL T0LAKROW(NL)         !<Variable description\f$[units]\f$
      REAL HICELROW(NL)         !<Variable description\f$[units]\f$
      REAL HDPTHROW(NL)         !<Variable description\f$[units]\f$
      REAL EXPWROW (NL)         !<Variable description\f$[units]\f$
      REAL DTMPROW (NL)         !<Variable description\f$[units]\f$
      REAL TKELROW (NL)         !<Variable description\f$[units]\f$
      REAL DELUROW(NL)          !<Variable description\f$[units]\f$
      REAL GREDROW (NL)         !<Variable description\f$[units]\f$
      REAL RHOMROW (NL)         !<Variable description\f$[units]\f$
      REAL DEPBROW (NL)         !<Variable description\f$[units]\f$
      REAL NLAKROW(NL)          !<Variable description\f$[units]\f$
      REAL TLAKROW(NL,NLAKMAX)  !<Variable description\f$[units]\f$

      REAL SNOROT  (NL,NM)      !<Variable description\f$[units]\f$
      REAL RHONROT (NL,NM)      !<Variable description\f$[units]\f$
      REAL TNROT  (NL,NM)       !<Variable description\f$[units]\f$
      REAL ANROT   (NL,NM)      !<Variable description\f$[units]\f$
      REAL WSNOROT(NL,NM)       !<Variable description\f$[units]\f$
      REAL FSVROT (NL,NM)       !<Variable description\f$[units]\f$
      REAL FSIROT  (NL,NM)      !<Variable description\f$[units]\f$
      REAL FDLROT (NL,NM)       !<Variable description\f$[units]\f$
      REAL FSGROT (NL,NM)       !<Variable description\f$[units]\f$
      REAL FLGROT  (NL,NM)      !<Variable description\f$[units]\f$
      REAL ASVDROT(NL,NM)       !<Variable description\f$[units]\f$
      REAL ASIDROT(NL,NM)       !<Variable description\f$[units]\f$
      REAL REFROT  (NL,NM)      !<Variable description\f$[units]\f$
      REAL BCSNROT(NL,NM)       !<Variable description\f$[units]\f$
C
      REAL HLAKGAT (ILG)        !<Variable description\f$[units]\f$
      REAL LLAKGAT (ILG)        !<Variable description\f$[units]\f$
      REAL BLAKGAT (ILG)        !<Variable description\f$[units]\f$
      REAL T0LAKGAT(ILG)        !<Variable description\f$[units]\f$
      REAL HICELGAT(ILG)        !<Variable description\f$[units]\f$
      REAL HDPTHGAT(ILG)        !<Variable description\f$[units]\f$
      REAL EXPWGAT (ILG)        !<Variable description\f$[units]\f$
      REAL DTMPGAT (ILG)        !<Variable description\f$[units]\f$
      REAL TKELGAT (ILG)        !<Variable description\f$[units]\f$
      REAL DELUGAT(ILG)         !<Variable description\f$[units]\f$
      REAL GREDGAT (ILG)        !<Variable description\f$[units]\f$
      REAL RHOMGAT (ILG)        !<Variable description\f$[units]\f$
      REAL DEPBGAT (ILG)        !<Variable description\f$[units]\f$
      INTEGER NLAKGAT(ILG)      !<Variable description\f$[units]\f$
      REAL TLAKGAT(ILG,NLAKMAX) !<Variable description\f$[units]\f$

      REAL SNOGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL RHONGAT (ILG)        !<Variable description\f$[units]\f$
      REAL TNGAT   (ILG)        !<Variable description\f$[units]\f$
      REAL ANGAT   (ILG)        !<Variable description\f$[units]\f$
      REAL WSNOGAT (ILG)        !<Variable description\f$[units]\f$
      REAL FSVGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL FSIGAT  (ILG)        !<Variable description\f$[units]\f$     
      REAL FDLGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL FSGGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL FLGGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL ASVDGAT (ILG)        !<Variable description\f$[units]\f$
      REAL ASIDGAT (ILG)        !<Variable description\f$[units]\f$
      REAL REFGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL BCSNGAT (ILG)        !<Variable description\f$[units]\f$
C
C     * ATMOSPHERIC AND GRID-CONSTANT INPUT VARIABLES.
C
      REAL  CSZROW ( NL)        !<Variable description\f$[units]\f$
      REAL  ULROW  ( NL)        !<Variable description\f$[units]\f$
      REAL  VLROW  ( NL)        !<Variable description\f$[units]\f$
      REAL  TAROW  ( NL)        !<Variable description\f$[units]\f$
      REAL  QAROW  ( NL)        !<Variable description\f$[units]\f$
      REAL  PRESROW( NL)        !<Variable description\f$[units]\f$
      REAL  RHOAROW( NL)        !<Variable description\f$[units]\f$
      REAL  PADRROW( NL)        !<Variable description\f$[units]\f$
      REAL  ZRFMROW( NL)        !<Variable description\f$[units]\f$
      REAL  ZRFHROW( NL)        !<Variable description\f$[units]\f$
      REAL  ZDMROW ( NL)        !<Variable description\f$[units]\f$
      REAL  ZDHROW ( NL)        !<Variable description\f$[units]\f$
      REAL  RPCPROW( NL)        !<Variable description\f$[units]\f$
      REAL  TRPCROW( NL)        !<Variable description\f$[units]\f$
      REAL  SPCPROW( NL)        !<Variable description\f$[units]\f$
      REAL  TSPCROW( NL)        !<Variable description\f$[units]\f$
      REAL  RHSIROW( NL)        !<Variable description\f$[units]\f$
      REAL  RADJROW( NL)        !<Variable description\f$[units]\f$
C
      REAL  FSDBROT(NL,NM,NBS)  !<Variable description\f$[units]\f$
      REAL  FSFBROT(NL,NM,NBS)  !<Variable description\f$[units]\f$
      REAL  FSSBROT(NL,NM,NBS)  !<Variable description\f$[units]\f$
C       
      REAL  CSZGAT (ILG)        !<Variable description\f$[units]\f$
      REAL  ULGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL  VLGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL  TAGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL  QAGAT  (ILG)        !<Variable description\f$[units]\f$
      REAL  PRESGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  RHOAGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  PADRGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  ZRFMGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  ZRFHGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  ZDMGAT (ILG)        !<Variable description\f$[units]\f$
      REAL  ZDHGAT (ILG)        !<Variable description\f$[units]\f$
      REAL  RPCPGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  TRPCGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  SPCPGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  TSPCGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  RHSIGAT(ILG)        !<Variable description\f$[units]\f$
      REAL  RADJGAT(ILG)        !<Variable description\f$[units]\f$
C
      REAL  FSDBGAT(ILG,NBS)    !<Variable description\f$[units]\f$
      REAL  FSFBGAT(ILG,NBS)    !<Variable description\f$[units]\f$
      REAL  FSSBGAT(ILG,NBS)    !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C----------------------------------------------------------------------
      IF(NMK.GT.0) THEN
C
C       * GATHER LAKE RELEVANT VARIABLES INTO                    
C       * LAKE-TILE GAT ARRAYS                                     
C                                                                
        DO 100 K=1,NMK                                             
          SNOGAT (K)=SNOROT (IKMOS(K),JKMOS(K))
          RHONGAT(K)=RHONROT(IKMOS(K),JKMOS(K))
          TNGAT  (K)=TNROT  (IKMOS(K),JKMOS(K))
          ANGAT  (K)=ANROT  (IKMOS(K),JKMOS(K))
          ASVDGAT(K)=ASVDROT(IKMOS(K),JKMOS(K))
          ASIDGAT(K)=ASIDROT(IKMOS(K),JKMOS(K))
          REFGAT (K)=REFROT (IKMOS(K),JKMOS(K))
          BCSNGAT(K)=BCSNROT(IKMOS(K),JKMOS(K))
          WSNOGAT(K)=WSNOROT(IKMOS(K),JKMOS(K))
          FSVGAT (K)=FSVROT (IKMOS(K),JKMOS(K))
          FSIGAT (K)=FSIROT (IKMOS(K),JKMOS(K))
          FDLGAT (K)=FDLROT (IKMOS(K),JKMOS(K))
          FSGGAT (K)=FSGROT (IKMOS(K),JKMOS(K))
          FSIGAT (K)=FSIROT (IKMOS(K),JKMOS(K))
  100   CONTINUE
C
        DO 200 K=1,NMK                                             
          HLAKGAT (K)=HLAKROW (IKMOS(K))              
          LLAKGAT (K)=LLAKROW (IKMOS(K))              
          BLAKGAT (K)=BLAKROW (IKMOS(K))              
          NLAKGAT (K)=NINT(NLAKROW (IKMOS(K)))              
          T0LAKGAT(K)=T0LAKROW(IKMOS(K))
          HICELGAT(K)=HICELROW(IKMOS(K))
          HDPTHGAT(K)=HDPTHROW(IKMOS(K))
          EXPWGAT (K)=EXPWROW (IKMOS(K))
          DTMPGAT (K)=DTMPROW (IKMOS(K))
          TKELGAT (K)=TKELROW (IKMOS(K))
          DELUGAT (K)=DELUROW (IKMOS(K))
          GREDGAT (K)=GREDROW (IKMOS(K))
          RHOMGAT (K)=RHOMROW (IKMOS(K))
          DEPBGAT (K)=DEPBROW (IKMOS(K))
          DO L=1,NLAKGAT(K)                              
            TLAKGAT(K,L)=TLAKROW(IKMOS(K),L)        
          ENDDO
  200   CONTINUE                                                   

C       * ATMOSPHERIC FORCING VARIABLES NEEDED FOR LAKE TILES     
C
        DO 300 K=1,NMK                                           
          CSZGAT (K)=CSZROW (IKMOS(K))                    
          ULGAT  (K)=ULROW  (IKMOS(K))                    
          VLGAT  (K)=VLROW  (IKMOS(K))                    
          TAGAT  (K)=TAROW  (IKMOS(K))                
          QAGAT  (K)=QAROW  (IKMOS(K))                
          RHOAGAT(K)=RHOAROW(IKMOS(K))                
          PADRGAT(K)=PADRROW(IKMOS(K))                
          PRESGAT(K)=PRESROW(IKMOS(K))                
          ZRFMGAT(K)=ZRFMROW(IKMOS(K))
          ZRFHGAT(K)=ZRFHROW(IKMOS(K))
          ZDMGAT (K)=ZDMROW (IKMOS(K))
          ZDHGAT (K)=ZDHROW (IKMOS(K))
          RPCPGAT(K)=RPCPROW(IKMOS(K))
          TRPCGAT(K)=TRPCROW(IKMOS(K))
          SPCPGAT(K)=SPCPROW(IKMOS(K))
          TSPCGAT(K)=TSPCROW(IKMOS(K))
          RHSIGAT(K)=RHSIROW(IKMOS(K))
          RADJGAT(K)=RADJROW(IKMOS(K))
          DO L=1,NBS
            FSDBGAT(K,L)=FSDBROT(IKMOS(K),JKMOS(K),L)        
            FSFBGAT(K,L)=FSFBROT(IKMOS(K),JKMOS(K),L)        
            FSSBGAT(K,L)=FSSBROT(IKMOS(K),JKMOS(K),L)        
          ENDDO
  300   CONTINUE                                           
      ENDIF

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
