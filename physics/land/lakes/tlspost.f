!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TLSPOST(GSNOW,TSNOW,WSNOW,RHOSNO,QMELTS,GZERO,TSNBOT,
     1                  HTCS,HMFN,QFN,EVAPS,RPCN,TRPCN,SPCN,TSPCN,
     2                  GCONSTS,GCOEFFS,T0,ZSNOW,TCSNOW,HCPSNO,QTRANS,
     3                  RPCP,TRPCP,SPCP,TSPCP,TZEROS,RHOSNI,
     4                  FLS,DELSKIN,ILG,IL1,IL2,JL          )
C
C     * JAN 30/18 - M.LAZARE.   LAST ELEMENT IN CALL, "N", REMOVED
C     *                         BECAUSE NOT IN CALL STATEMENT FROM 
C     *                         ROUTINE CLASSL, AND IS NOT USED.
C     * SEP 01/15 - D.VERSEGHY. LAKE SNOW TEMPERATURE AND HEAT FLUX
C     *                         CALCULATIONS (BASED ON CLASS SUBROUTINE
C     *                         TSPOST); NET SURFACE WATER FLUX TERMS
C     *                         (BASED ON CLASS SUBROUTINE WPREP).
C
      IMPLICIT NONE
C                                                                                 
C     * INTEGER CONSTANTS.
C
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER IL1       !<Variable description\f$[units]\f$
      INTEGER IL2       !<Variable description\f$[units]\f$
      INTEGER JL        !<Variable description\f$[units]\f$
      INTEGER I         !<Variable description\f$[units]\f$
      INTEGER J         !<Variable description\f$[units]\f$
C
C     * OUTPUT ARRAYS.
C
      REAL GZERO (ILG)  !<Variable description\f$[units]\f$
      REAL TSNBOT(ILG)  !<Variable description\f$[units]\f$
      REAL RPCN  (ILG)  !<Variable description\f$[units]\f$
      REAL TRPCN (ILG)  !<Variable description\f$[units]\f$
      REAL SPCN  (ILG)  !<Variable description\f$[units]\f$
      REAL TSPCN (ILG)  !<Variable description\f$[units]\f$
C
C     * INPUT/OUTPUT ARRAYS.
C
      REAL GSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL TSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL WSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL RHOSNO(ILG)  !<Variable description\f$[units]\f$
      REAL QMELTS(ILG)  !<Variable description\f$[units]\f$
      REAL HTCS  (ILG)  !<Variable description\f$[units]\f$
      REAL HMFN  (ILG)  !<Variable description\f$[units]\f$
      REAL QFN   (ILG)  !<Variable description\f$[units]\f$
      REAL EVAPS (ILG)  !<Variable description\f$[units]\f$
C
C     * INPUT ARRAYS.
C
      REAL T0    (ILG)  !<Variable description\f$[units]\f$
      REAL ZSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL TCSNOW(ILG)  !<Variable description\f$[units]\f$
      REAL HCPSNO(ILG)  !<Variable description\f$[units]\f$
      REAL QTRANS(ILG)  !<Variable description\f$[units]\f$
      REAL GCONSTS(ILG) !<Variable description\f$[units]\f$
      REAL GCOEFFS(ILG) !<Variable description\f$[units]\f$
      REAL RPCP  (ILG)  !<Variable description\f$[units]\f$
      REAL TRPCP  (ILG) !<Variable description\f$[units]\f$
      REAL SPCP   (ILG) !<Variable description\f$[units]\f$
      REAL TSPCP (ILG)  !<Variable description\f$[units]\f$
      REAL TZEROS(ILG)  !<Variable description\f$[units]\f$
      REAL RHOSNI (ILG) !<Variable description\f$[units]\f$
      REAL FLS    (ILG) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      REAL DELSKIN
C
C     * TEMPORARY VARIABLES.
C
      REAL RADD  (ILG),    SADD  (ILG)
C
      REAL HADD,HCONV,WFREZ
C
C     * COMMON BLOCK PARAMETERS.
C
      REAL DELT,TFREZ,TCW,TCICE,TCSAND,TCCLAY,TCOM,TCDRYS,
     1     RHOSOL,RHOOM,HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,
     2     SPHW,SPHICE,SPHVEG,SPHAIR,RHOW,RHOICE,TCGLAC,CLHMLT,CLHVAP
C
      COMMON /CLASS1/ DELT,TFREZ                                       
      COMMON /CLASS3/ TCW,TCICE,TCSAND,TCCLAY,TCOM,TCDRYS,
     1                RHOSOL,RHOOM
      COMMON /CLASS4/ HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,
     1                SPHW,SPHICE,SPHVEG,SPHAIR,RHOW,RHOICE,
     2                TCGLAC,CLHMLT,CLHVAP
C-----------------------------------------------------------------------
C
      DO 100 I=IL1,IL2
          IF(FLS(I).GT.0.)                                          THEN
              TSNBOT(I)=(ZSNOW(I)*TSNOW(I)+DELSKIN*T0(I))/
     1             (ZSNOW(I)+DELSKIN)
              GZERO(I)=-2.0*TCSNOW(I)*(TSNBOT(I)-TSNOW(I))/ZSNOW(I)
C     1                 +TCICE*(T0(I)-TSNBOT(I))/DELSKIN)
              IF(QMELTS(I).LT.0.)                               THEN
                  GSNOW(I)=GSNOW(I)+QMELTS(I)                           
                  QMELTS(I)=0.                                          
              ENDIF                                                     
              TSNOW(I)=TSNOW(I)+(GSNOW(I)-GZERO(I))*DELT/
     1                          (HCPSNO(I)*ZSNOW(I))-TFREZ             
              IF(TSNOW(I).GT.0.)                                THEN
                  QMELTS(I)=QMELTS(I)+TSNOW(I)*HCPSNO(I)*ZSNOW(I)/DELT
                  GSNOW(I)=GSNOW(I)-TSNOW(I)*HCPSNO(I)*ZSNOW(I)/DELT
                  TSNOW(I)=0.                                         
              ENDIF                                                  
C              GZERO(I)=GZERO(I)+QMELTS(I)
C              QMELTS(I)=0.0
          ENDIF
  100 CONTINUE
C 
      DO 200 I=IL1,IL2
           IF(FLS(I).GT.0. .AND. TSNOW(I).LT.0. .AND. WSNOW(I).GT.0.)
     1                                                              THEN
             HTCS(I)=HTCS(I)-FLS(I)*HCPSNO(I)*(TSNOW(I)+TFREZ)*ZSNOW(I)/
     1               DELT
             HADD=-TSNOW(I)*HCPSNO(I)*ZSNOW(I)
             HCONV=CLHMLT*WSNOW(I)
             IF(HADD.LE.HCONV)                           THEN      
                 WFREZ=HADD/CLHMLT
                 HADD=0.0
                 WSNOW(I)=MAX(0.0,WSNOW(I)-WFREZ)
                 TSNOW(I)=0.0
                 RHOSNO(I)=RHOSNO(I)+WFREZ/ZSNOW(I)
                 HCPSNO(I)=HCPICE*RHOSNO(I)/RHOICE+HCPW*WSNOW(I)/
     1               (RHOW*ZSNOW(I))
             ELSE                 
                 HADD=HADD-HCONV 
                 WFREZ=WSNOW(I)
                 WSNOW(I)=0.0
                 RHOSNO(I)=RHOSNO(I)+WFREZ/ZSNOW(I)
                 HCPSNO(I)=HCPICE*RHOSNO(I)/RHOICE
                 TSNOW(I)=-HADD/(HCPSNO(I)*ZSNOW(I))
             ENDIF
             HMFN(I)=HMFN(I)-FLS(I)*CLHMLT*WFREZ/DELT
             HTCS(I)=HTCS(I)-FLS(I)*CLHMLT*WFREZ/DELT
             HTCS(I)=HTCS(I)+FLS(I)*HCPSNO(I)*(TSNOW(I)+TFREZ)*ZSNOW(I)/
     1               DELT
          ENDIF
  200 CONTINUE
C
      DO 300 I=IL1,IL2
        QFN(I)=FLS(I)*EVAPS(I)*RHOW
        IF(SPCP(I).GT.0. .OR. EVAPS(I).LT.0.) THEN           
            SADD(I)=SPCP(I)-EVAPS(I)*RHOW/RHOSNI(I)
            IF(ABS(SADD(I)).LT.1.0E-12) SADD(I)=0.0
            IF(SADD(I).GT.0.0) THEN                          
                SPCN (I)=SADD(I)                            
                IF(SPCP(I).GT.0.0) THEN
                    TSPCN(I)=TSPCP(I)
                ELSE
                    TSPCN(I)=MIN((TZEROS(I)-TFREZ),0.0)
                ENDIF
                EVAPS(I)=0.0                               
            ELSE                                            
                EVAPS(I)=-SADD(I)*RHOSNI(I)/RHOW           
                SPCN (I)=0.0                               
                TSPCN(I)=0.0                               
            ENDIF                                           
        ELSE                                                
            SPCN (I)=0.0                                   
            TSPCN(I)=0.0                                   
        ENDIF

        IF(RPCP(I).GT.0.)                         THEN     
            RADD(I)=RPCP(I)-EVAPS(I)                      
            IF(ABS(RADD(I)).LT.1.0E-12) RADD(I)=0.0
            IF(RADD(I).GT.0.)   THEN                      
                RPCN (I)=RADD(I)                         
                TRPCN(I)=TRPCP(I)
                EVAPS(I)=0.0                            
            ELSE                                         
                EVAPS(I)=-RADD(I)                       
                RPCN (I)=0.0                            
                TRPCN(I)=0.0                           
            ENDIF                                       
        ELSE                                            
            RPCN (I)=0.0                               
            TRPCN(I)=0.0                               
        ENDIF                                             
  300 CONTINUE
C
      RETURN                    
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
