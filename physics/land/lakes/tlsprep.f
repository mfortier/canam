!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TLSPREP(GCOEFFS,GCONSTS,CPHCHS,TCSNOW,HCPSNO,IWATER,
     1                  ZRSLDM,ZRSLDH,ZRSLFM,ZRSLFH,ZDSLM,ZDSLH,
     2                  ZOSCLM,ZOSCLH,ZOMLNS,ZOELNS,ZOM,ZOH,
     3                  TVIRTA,TPOTA,CRIB,DRAGS,CEVAP,IEVAP,ISAND,
     4                  FLS,ZSNOW,TSNOW,RHOSNO,WSNOW,ZREFM,ZREFH,
     5                  ZDIAGM,ZDIAGH,TA,QA,VA,IZREF,ILG,IL1,IL2,IG,JL)
C
C     * MAY 13/15 - D.VERSEGHY. PREPARATION FOR LAKE SNOW TEMPERATURE
C     *                         CALCULATIONS (BASED ON CLASS 
C     *                         SUBROUTINES CLASST, TPREP AND TSPREP).
C
      IMPLICIT NONE
C                                                                                 
C     * INTEGER CONSTANTS.
C
      INTEGER IZREF     !<Variable description\f$[units]\f$
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER IL1       !<Variable description\f$[units]\f$
      INTEGER IL2       !<Variable description\f$[units]\f$
      INTEGER IG        !<Variable description\f$[units]\f$
      INTEGER JL        !<Variable description\f$[units]\f$
      INTEGER I         !<Variable description\f$[units]\f$
      INTEGER J         !<Variable description\f$[units]\f$
C
C     * OUTPUT ARRAYS.
C
      REAL GCOEFFS(ILG) !<Variable description\f$[units]\f$
      REAL GCONSTS(ILG) !<Variable description\f$[units]\f$
      REAL CPHCHS (ILG) !<Variable description\f$[units]\f$
      REAL TCSNOW (ILG) !<Variable description\f$[units]\f$
      REAL ZRSLDM (ILG) !<Variable description\f$[units]\f$
      REAL ZRSLDH (ILG) !<Variable description\f$[units]\f$
      REAL ZRSLFM (ILG) !<Variable description\f$[units]\f$
      REAL ZRSLFH (ILG) !<Variable description\f$[units]\f$
      REAL ZDSLM  (ILG) !<Variable description\f$[units]\f$
      REAL ZDSLH  (ILG) !<Variable description\f$[units]\f$
      REAL ZOSCLM (ILG) !<Variable description\f$[units]\f$
      REAL ZOSCLH (ILG) !<Variable description\f$[units]\f$
      REAL ZOMLNS (ILG) !<Variable description\f$[units]\f$
      REAL ZOELNS (ILG) !<Variable description\f$[units]\f$
      REAL ZOM    (ILG) !<Variable description\f$[units]\f$
      REAL ZOH    (ILG) !<Variable description\f$[units]\f$
      REAL TVIRTA (ILG) !<Variable description\f$[units]\f$
      REAL TPOTA  (ILG) !<Variable description\f$[units]\f$
      REAL CRIB   (ILG) !<Variable description\f$[units]\f$
      REAL DRAGS  (ILG) !<Variable description\f$[units]\f$
      REAL CEVAP  (ILG) !<Variable description\f$[units]\f$
      REAL HCPSNO (ILG) !<Variable description\f$[units]\f$
C
      INTEGER IWATER(ILG)       !<Variable description\f$[units]\f$
      INTEGER IEVAP  (ILG)      !<Variable description\f$[units]\f$
      INTEGER ISAND (ILG,IG)    !<Variable description\f$[units]\f$
C
C     * INPUT ARRAYS.
C
      REAL FLS   (ILG)  !<Variable description\f$[units]\f$
      REAL ZSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL TSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL RHOSNO(ILG)  !<Variable description\f$[units]\f$
      REAL WSNOW (ILG)  !<Variable description\f$[units]\f$
      REAL ZREFM (ILG)  !<Variable description\f$[units]\f$
      REAL ZREFH (ILG)  !<Variable description\f$[units]\f$
      REAL ZDIAGM(ILG)  !<Variable description\f$[units]\f$
      REAL ZDIAGH(ILG)  !<Variable description\f$[units]\f$
      REAL TA    (ILG)  !<Variable description\f$[units]\f$
      REAL QA    (ILG)  !<Variable description\f$[units]\f$
      REAL VA    (ILG)  !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C     * COMMON BLOCK PARAMETERS.
C
      REAL TCW,TCICE,TCSAND,TCCLAY,TCOM,TCDRYS,RHOSOL,RHOOM,
     1     HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,SPHW,SPHICE,
     2     SPHVEG,SPHAIR,RHOW,RHOICE,TCGLAC,CLHMLT,CLHVAP,
     3     DELTA,CGRAV,CKARM,CPD 
C
      COMMON /CLASS3/ TCW,TCICE,TCSAND,TCCLAY,TCOM,TCDRYS,
     1                RHOSOL,RHOOM
      COMMON /CLASS4/ HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,
     1                SPHW,SPHICE,SPHVEG,SPHAIR,RHOW,RHOICE,
     2                TCGLAC,CLHMLT,CLHVAP
      COMMON /PHYCON/ DELTA,CGRAV,CKARM,CPD
C-----------------------------------------------------------------------
C     * CALCULATIONS FOR SNOW-COVERED GROUND.                           
C                                                                       
      DO 100 I=IL1,IL2                                              
          IF(FLS(I).GT.0.)                                      THEN
              ZOM(I)=0.001
              ZOMLNS(I)=LOG(ZOM(I))
              ZOH(I)=0.0003
              ZOELNS(I)=LOG(ZOH(I))
              IF(IZREF.EQ.1) THEN                                   
                  ZRSLDM(I)=ZREFM(I)                                
                  ZRSLDH(I)=ZREFH(I)                                
                  ZRSLFM(I)=ZREFM(I)-ZOM(I)                         
                  ZRSLFH(I)=ZREFH(I)-ZOM(I)                         
                  ZDSLM(I)=ZDIAGM(I)-ZOM(I)                         
                  ZDSLH(I)=ZDIAGH(I)-ZOM(I)                         
                  TPOTA(I)=TA(I)+ZRSLFH(I)*CGRAV/CPD                 
              ELSE                                                  
                  ZRSLDM(I)=ZREFM(I)+ZOM(I)                         
                  ZRSLDH(I)=ZREFH(I)+ZOM(I)                         
                  ZRSLFM(I)=ZREFM(I)                                
                  ZRSLFH(I)=ZREFH(I)                                
                  ZDSLM(I)=ZDIAGM(I)                                
                  ZDSLH(I)=ZDIAGH(I)                                
                  TPOTA(I)=TA(I)                                    
              ENDIF                                                 
              ZOSCLM(I)=ZOM(I)/ZRSLDM(I)                            
              ZOSCLH(I)=ZOH(I)/ZRSLDH(I)                            
              TVIRTA(I)=TPOTA(I)*(1.0+0.61*QA(I))                   
              CRIB(I)=-CGRAV*ZRSLDM(I)/(TVIRTA(I)*VA(I)**2)          
              DRAGS(I)=(CKARM/(LOG(ZRSLDM(I))-ZOMLNS(I)))**2  
          ENDIF                                                     
  100     CONTINUE                                                      
C                                                                       
C     * THERMAL PROPERTIES OF SNOW.                                     
C                                                                       
      DO 200 I=IL1,IL2                                                  
          IF(ZSNOW(I).GT.0.)                                        THEN
              HCPSNO(I)=HCPICE*RHOSNO(I)/RHOICE+HCPW*WSNOW(I)/          
     1            (RHOW*ZSNOW(I))                                       
C             TCSNOW(I)=2.576E-6*RHOSNO(I)*RHOSNO(I)+0.074              
              IF(RHOSNO(I).LT.156.0) THEN                               
                  TCSNOW(I)=0.234E-3*RHOSNO(I)+0.023                    
              ELSE                                                      
                  TCSNOW(I)=3.233E-6*RHOSNO(I)*RHOSNO(I)-1.01E-3*       
     1                RHOSNO(I)+0.138                                   
              ENDIF                                                     
          ENDIF
  200 CONTINUE
C                                                                       
C     * CALCULATE COEFFICIENTS.
C
      DO 300 I=IL1,IL2
          IF(FLS(I).GT.0.)                                          THEN
              GCOEFFS(I)=3.0*TCSNOW(I)/ZSNOW(I)
              GCONSTS(I)=-3.0*TCSNOW(I)*TSNOW(I)/ZSNOW(I)
              CPHCHS(I)=CLHVAP+CLHMLT
              IWATER(I)=2             
          ELSE
              IWATER(I)=1
          ENDIF
          CEVAP(I)=1.0
          IEVAP(I)=1
          DO 250 J=1,IG
              ISAND(I,J)=-4
  250     CONTINUE
  300 CONTINUE
C
      RETURN                                                                      
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

