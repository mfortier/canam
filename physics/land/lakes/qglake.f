!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE QGLAKE(QG,                        !output
     1                  GT,PS,                     !input
     2                  DELT,ILG,IS,IF)
C
C     * Nov 24/16 - M.Lazare. New version for gcm19+:
C     *                       Based on oifprp10, but only calculates QG.
C
C     * CALCULATES GROUND SPECIFIC HUMIDITY OVER WATER AND ICE.
C
      IMPLICIT NONE
C 
C     * OUTPUT OR I/O FIELDS:
C
      REAL   QG     (ILG)       !<Variable description\f$[units]\f$
C
C     * INPUT FIELDS:
C
      REAL   GT     (ILG)       !<Variable description\f$[units]\f$
      REAL   PS     (ILG)       !<Variable description\f$[units]\f$
C
      REAL DELT                 !<Variable description\f$[units]\f$
      REAL FRACW                !<Variable description\f$[units]\f$
      INTEGER ILG               !<Variable description\f$[units]\f$
      INTEGER IS                !<Variable description\f$[units]\f$
      INTEGER IF                !<Variable description\f$[units]\f$
      INTEGER I                 !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      REAL A,B,EPS1,EPS2,T1S,T2S,AI,BI,AW,BW,SLPE
      COMMON /EPS/ A,B,EPS1,EPS2               
      COMMON /HTCP  / T1S,T2S,AI,BI,AW,BW,SLPE               
C
C     * PARAMETERS USED IN NEW SATURATION VAPOUR PRESSURE FORMULATION.
C
      REAL RW1,RW2,RW3,RI1,RI2,RI3
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
C     * IN-LINE FUNCTIONS:
C
C     * COMPUTES THE SATURATION VAPOUR PRESSURE OVER WATER OR ICE.             
C
      REAL TTT,UUU,ESW,ESI,ESTEFF,Q,EEE,P,EST
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)    = EXP(RI1+RI2/TTT)*TTT**RI3
      ESTEFF(TTT,UUU) = UUU*ESW(TTT) + (1.-UUU)*ESI(TTT)
      Q(EEE,P,EPS1,EPS2)=EPS1*EEE/(P-EPS2*EEE)               
C-----------------------------------------------------------------------
      DO 100 I=IS,IF
C
C       * SATURATION SPECIFIC HUMIDITY AT GROUND TEMPERATURE AND PRESSURE.
C       * COMPUTE THE FRACTIONAL PROBABILITY OF WATER PHASE EXISTING
C       * AS A FUNCTION OF TEMPERATURE (FROM ROCKEL,                  
C       * RASCHKE AND WEYRES, BEITR. PHYS. ATMOSPH., 1991.)                    
C
        FRACW = 1.
        IF(GT(I).LT.T1S)          THEN
          FRACW = 0.0059+0.9941*EXP(-0.003102*(T1S-GT(I))**2)
        ENDIF      
        EST=ESTEFF(GT(I),FRACW)
        QG(I)=Q(EST, .01*PS(I), EPS1, EPS2)                                
  100 CONTINUE
C--------------------------------------------------------------------
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

