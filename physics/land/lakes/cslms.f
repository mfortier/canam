!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CSLMS (TLAKROW,T0LAKROW,HICELROW,HDPTHROW,EXPWROW,
     1                  DTMPROW,TKELROW,DELUROW,GREDROW,RHOMROW,
     2                  LUINROW,LUIMROW,WSNOLROT,
     3                  IKMOS,JKMOS,NLAKMAX,NMK,NL,NM,ILG,
     4                  TLAKGAT,T0LAKGAT,HICELGAT,HDPTHGAT,EXPWGAT,
     5                  DTMPGAT,TKELGAT,DELUGAT,GREDGAT,RHOMGAT,
     6                  LUINGAT,LUIMGAT,WSNOLGAT,NLAKGAT)
C
C     * DEC 01/16 - M.LAZARE. - REMOVE {SNOL,RHOSL,TSNOL,ALBSL} ALREADY DONE
C     *                         IN "OCEANS" ROUTINE.
C     *                       - REMOVE {HLAK,BLAK,LLAK,NLAK} WHICH ARE INVARIANT
C     *                         AND THUS DON'T NEED TO BE SCATTERED.
C     *                       - REMOVE TILE DIMENSION FOR ALL FIELDS
C     *                         EXCEPT WSNO.
C     *                       - {IKMOS,JKMOS}->{IKMOS,JKMOS} AND NMW->NMK.
C     *                       - "ROT"->"ROW" FOR NAMES EXCEPT WSNO.
C     *                       - RENAMED LAKEG-> CSLMG.
C     *                       - ADD "LUIN" AND "LUIM", IE LAKE ICE
C     *                         FRACTION AND MASS..
C     * JAN 19/12 - M.MACKAY. SCATTER OPERATION FOR LAKE TILE VARIABLES.
C
      IMPLICIT NONE
C
      INTEGER NMK,NL,NM,ILG,K,L,NLAKMAX         
C
C     * LAKE TILE PARAMETERS                                    
C
      REAL T0LAKROW(NL)         !<Variable description\f$[units]\f$
      REAL HICELROW(NL)         !<Variable description\f$[units]\f$
      REAL HDPTHROW(NL)         !<Variable description\f$[units]\f$
      REAL EXPWROW(NL)          !<Variable description\f$[units]\f$
      REAL DTMPROW(NL)          !<Variable description\f$[units]\f$
      REAL TKELROW(NL)          !<Variable description\f$[units]\f$
      REAL DELUROW(NL)          !<Variable description\f$[units]\f$
      REAL GREDROW(NL)          !<Variable description\f$[units]\f$
      REAL RHOMROW(NL)          !<Variable description\f$[units]\f$
      REAL LUINROW(NL)          !<Variable description\f$[units]\f$
      REAL LUIMROW(NL)          !<Variable description\f$[units]\f$
      REAL WSNOLROT(NL,NM)      !<Variable description\f$[units]\f$
      REAL TLAKROW(NL,NLAKMAX)  !<Variable description\f$[units]\f$
C
      REAL T0LAKGAT(ILG)        !<Variable description\f$[units]\f$
      REAL HICELGAT(ILG)        !<Variable description\f$[units]\f$
      REAL HDPTHGAT(ILG)        !<Variable description\f$[units]\f$
      REAL EXPWGAT(ILG)         !<Variable description\f$[units]\f$
      REAL DTMPGAT(ILG)         !<Variable description\f$[units]\f$
      REAL TKELGAT(ILG)         !<Variable description\f$[units]\f$     
      REAL DELUGAT(ILG)         !<Variable description\f$[units]\f$
      REAL GREDGAT(ILG)         !<Variable description\f$[units]\f$
      REAL RHOMGAT(ILG)         !<Variable description\f$[units]\f$
      REAL LUINGAT(ILG)         !<Variable description\f$[units]\f$
      REAL LUIMGAT(ILG)         !<Variable description\f$[units]\f$
      REAL WSNOLGAT(ILG)        !<Variable description\f$[units]\f$
      REAL TLAKGAT(ILG,NLAKMAX) !<Variable description\f$[units]\f$
      INTEGER NLAKGAT(ILG)      !<Variable description\f$[units]\f$
C
      INTEGER  IKMOS(ILG)       !<Variable description\f$[units]\f$
      INTEGER  JKMOS(ILG)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C----------------------------------------------------------------------
C     * SCATTER LAKE RELEVANT VARIABLES OUT OF
C     * WATER-TILE GAT ARRAYS                               
C                                                           
      DO 200 K=1,NMK                                        
        T0LAKROW(IKMOS(K))=T0LAKGAT(K) 
        HICELROW(IKMOS(K))=HICELGAT(K) 
        HDPTHROW(IKMOS(K))=HDPTHGAT(K) 
        EXPWROW (IKMOS(K))=EXPWGAT (K) 
        DTMPROW (IKMOS(K))=DTMPGAT (K) 
        TKELROW (IKMOS(K))=TKELGAT (K) 
        DELUROW (IKMOS(K))=DELUGAT (K) 
        GREDROW (IKMOS(K))=GREDGAT (K) 
        RHOMROW (IKMOS(K))=RHOMGAT (K) 
        LUINROW (IKMOS(K))=LUINGAT (K)
        LUIMROW (IKMOS(K))=LUIMGAT (K)
        DO L=1,NLAKGAT(K)                           
          TLAKROW(IKMOS(K),L)=TLAKGAT(K,L)     
        ENDDO
  200 CONTINUE                         
C
      DO 300 K=1,NMK
        WSNOLROT(IKMOS(K),JKMOS(K))=WSNOLGAT(K)
  300 CONTINUE                     
C
      RETURN
      END  
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
