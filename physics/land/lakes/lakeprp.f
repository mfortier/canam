!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE LAKEPRP (SNO,SIC,DENS,
     1                    GC,SNOW,HLAT,
     2                    DELT,ILG,IS,IF)
C.......................................................................
C     * SEP 06/16 - M.LAZARE.    NEW VERSION FOR GCM19+:
C     *                          - BASED ON OIFPST10 BUT MUCH SIMPLIFIED
C     *                            FOR LAKE STUFF NOT DONE IN "FLAKE".
C     *                            SNOWFALL CONTRIBUTION MOVED FROM 
C     *                            OIFPRP10.
C---------------------------------------------------------------------- 
C     * DICTIONARY OF VARIABLES
C
C  DELT  - model time step (s)
C  DENS  - snow density (kg m-3)
C  EVAPR - ground evaporation rate (kg m-2 s-1)
C  GC    - ground cover type (1.=pack ice, 0.=open water, -1.=land)
C  HLAT  - latent heat flux (W m-2)
C  HS    - latent heat of sublimation (J kg-1)
C  SIC   - sea ice amount (kg m-2)
C  SNO   - snow amount (kg m-2)
C  SNOW  - snowfall rate (kg/m2-s)
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
C     * OUTPUT FIELDS:
C
      REAL  , DIMENSION(ILG)  :: SNO    !<Variable description\f$[units]\f$
      REAL  , DIMENSION(ILG)  :: SIC    !<Variable description\f$[units]\f$
      REAL  , DIMENSION(ILG)  :: DENS   !<Variable description\f$[units]\f$
C       
C     * INPUT FIELDS:
C
      REAL  , DIMENSION(ILG)  :: GC     !<Variable description\f$[units]\f$
      REAL  , DIMENSION(ILG)  :: SNOW   !<Variable description\f$[units]\f$
      REAL  , DIMENSION(ILG)  :: HLAT   !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      COMMON /PARAMS/WW,TW,A,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,
     1               RGASV
      COMMON /PARAM1/PI,RVORD,TFREZ,HS,HV,DAYLNT
      COMMON /PARAM3/CSNO,CPACK,GTFSW,RKHI,SBC,SNOMAX 
      COMMON /PARAM5/CONI,DENI,XXX,DFSIC,CONF 
C-------------------------------------------------------------------
      DO 100 I=IS,IF
        IF(GC(I).GT.0.5)                        THEN
C
C         * SNOW ACCUMULATION OVER ICE.
C 
          IF(SNOW(I).GT.0.)                     THEN
            SNO(I)=SNO(I)+SNOW(I)*DELT
          ENDIF
C     
C         * SUBLIMATION/DEPOSITION. 
C 
          EVAPR=HLAT(I)/HS
          DELE=EVAPR*DELT 
          IF(SNO(I).GT.DELE)         THEN
            SNO(I)=SNO(I)-DELE 
          ELSE
            SIC(I)=SIC(I)-(DELE-SNO(I)) 
            SNO(I)=0. 
          ENDIF 
          SIC(I)=MAX(SIC(I),0.) 
C
C         * SNOW DENSITY.
C
          IF(SNO(I).GT.SNOMAX) THEN 
            DENS(I)=0.54*SNO(I)/LOG(1.+0.54*SNO(I)/275.)
          ELSE 
            DENS(I)=275. 
          ENDIF
        ENDIF
  100 CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

