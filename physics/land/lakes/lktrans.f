!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE LKTRANS (CQ1A,CQ1B,CQ2A,CQ2B,CQ3A,CQ3B,
     1                    BLAK,IL1,IL2,ILG,CQ1BI,CQ2BI,CQ3BI)
C======================================================================
C     * DEC  7/07 - M.MACKAY.  	COMPUTES LIGHT EXTINCTION COEFFICIENTS
C     *                         THIS ROUTINE KEPT FOR FUTURE DYNAMIC
C				CHANGES TO EXTINCTION
C
      IMPLICIT NONE
C
C ----* INPUT FIELDS *------------------------------------------------
C
      REAL,DIMENSION(ILG) :: BLAK       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ1A       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ1B       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ2A       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ2B       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ3A       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ3B       !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ1BI      !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ2BI      !<Variable description\f$[units]\f$
      REAL,DIMENSION(ILG) :: CQ3BI      !<Variable description\f$[units]\f$
      INTEGER IL1                       !<Variable description\f$[units]\f$
      INTEGER IL2                       !<Variable description\f$[units]\f$
      INTEGER ILG                       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C ----* LOCAL VARIABLES *------------------------------------------------
C
      INTEGER I

C======================================================================
C                CQ1A,    CQ1B,    CQ2A,    CQ2B,    CQ3A,    CQ3B 
C Rayner,1980    0.54,    0.561,   0.30,    6.89,    0.16,    69.0 
C======================================================================
      DO 100 I=IL1,IL2
C FIXED WATER VALUES
      CQ1A(I)=0.5817
      CQ2A(I)=0.4183
      CQ2B(I)=6.89
      CQ3A(I)=0.0
      CQ3B(I)=69.0
C FIXED ICE VALUES (from Patterson and Hamblin, 1988, L&O)
      CQ1BI(I)=1.5
      CQ2BI(I)=20.0
      CQ3BI(I)=69.0

C======================================================================
C CQ1B NOW READ IN .INI FILE
C----------------------------------------------------------------------
      CQ1B(I)=BLAK(I)

100   CONTINUE

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
