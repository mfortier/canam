!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE FLAKES(LTICROW,LTSNROW,LDMXROW,LTAVROW,LTMXROW,
     1                  LTWBROW,LSHPROW,LZICROW,LZSNROW,
     2                  IKMOS,JKMOS,NMK,NL,ILG,
     3                  LTICGAT,LTSNGAT,LDMXGAT,LTAVGAT,LTMXGAT,
     4                  LTWBGAT,LSHPGAT,LZICGAT,LZSNGAT        )
C
C     * DEC 07/2016 - M.LAZARE. SCATTER OPERATION FOR FLAKE TILE VARIABLES.
C
      IMPLICIT NONE
C
      INTEGER NMK       !<Variable description\f$[units]\f$
      INTEGER NL        !<Variable description\f$[units]\f$
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER K         !<Variable description\f$[units]\f$
C
C     * LAKE TILE PARAMETERS.                                    
C
      REAL LTICROW(NL)  !<Variable description\f$[units]\f$
      REAL LTSNROW(NL)  !<Variable description\f$[units]\f$
      REAL LDMXROW(NL)  !<Variable description\f$[units]\f$
      REAL LTAVROW(NL)  !<Variable description\f$[units]\f$
      REAL LTMXROW(NL)  !<Variable description\f$[units]\f$
      REAL LTWBROW(NL)  !<Variable description\f$[units]\f$
      REAL LSHPROW(NL)  !<Variable description\f$[units]\f$
      REAL LZICROW(NL)  !<Variable description\f$[units]\f$
      REAL LZSNROW(NL)  !<Variable description\f$[units]\f$
C
      REAL LTICGAT(ILG) !<Variable description\f$[units]\f$
      REAL LTSNGAT(ILG) !<Variable description\f$[units]\f$
      REAL LDMXGAT(ILG) !<Variable description\f$[units]\f$
      REAL LTAVGAT(ILG) !<Variable description\f$[units]\f$
      REAL LTMXGAT(ILG) !<Variable description\f$[units]\f$
      REAL LTWBGAT(ILG) !<Variable description\f$[units]\f$
      REAL LSHPGAT(ILG) !<Variable description\f$[units]\f$
      REAL LZICGAT(ILG) !<Variable description\f$[units]\f$
      REAL LZSNGAT(ILG) !<Variable description\f$[units]\f$
C
      INTEGER  IKMOS(ILG)!<Variable description\f$[units]\f$
      INTEGER  JKMOS(ILG)!<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C----------------------------------------------------------------------
C     * SCATTER LAKE RELEVANT VARIABLES OUT OF
C     * WATER-TILE GAT ARRAYS                               
C                                                           
      DO K=1,NMK                                        
        LTICROW (IKMOS(K))=LTICGAT (K) 
        LTSNROW (IKMOS(K))=LTSNGAT (K) 
        LDMXROW (IKMOS(K))=LDMXGAT (K) 
        LTAVROW (IKMOS(K))=LTAVGAT (K) 
        LTMXROW (IKMOS(K))=LTMXGAT (K) 
        LTWBROW (IKMOS(K))=LTWBGAT (K) 
        LSHPROW (IKMOS(K))=LSHPGAT (K)
        LZICROW (IKMOS(K))=LZICGAT (K)
        LZSNROW (IKMOS(K))=LZSNGAT (K)     
      ENDDO
C
      RETURN
      END  
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
