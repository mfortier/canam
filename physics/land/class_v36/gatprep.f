      SUBROUTINE GATPREP (IMOS,JMOS,FAREA,
     1                    NM,NTSTART,NTEND,IM,NL,ILG,IL1,IL2)
C
C     * JAN 25/14 - M.LAZARE.  New version for gcm18+:
C     *             E.CHAN.    - Generalized version to be called
C     *                          for EACH of {land,lake,water}
C     *                          based on input number of tiles (NTILE)
C     *                          and the fractional area of each tile
C     *                          FAREA.
C     *                        - Order of loops are reversed.
C     *                        - MIDROT and GCROW are therefore no longer
C     *                          needed and are removed.
C     * DEC 28/11 - D.VERSEGHY. PREVIOUS VERSION GATPREP. 
C     *                         CHANGE ILGM BACK TO ILG AND
C     *                         ILG TO NL FOR CONSISTENCY WITH
C     *                         BOTH STAND-ALONE AND GCM
C     *                         CONVENTIONS.
C     * OCT 22/11 - M.LAZARE. REMOVE OCEAN/ICE CODE (NOW DONE
C     *                       IN COISS).
C     * OCT 21/11 - M.LAZARE. COSMETIC: ILG->ILGM AND NLAT->ILG,
C     *                       TO BE CONSISTENT WITH MODEL
C     *                       CONVENTION. ALSO GCGRD->GCROW.
C     * JUN 12/06 - E.CHAN.  DIMENSION IWAT AND IICE BY ILG.
C     * NOV 03/04 - D.VERSEGHY. ADD "IMPLICIT NONE" COMMAND.
C     * AUG 09/02 - D.VERSEGHY/M.LAZARE. DETERMINE INDICES FOR
C     *                        GATHER-SCATTER OPERATIONS ON
C     *                        CURRENT LATITUDE LOOP.
C     
C     * THIS ROUTINE CALCULATES THE CHAINED TILE LENGTH INDICES
C     * FOR EACH SET OF TILES, CALLED SEPARATELY FOR: LAND 
C     * (WITH ARGUMENTS {ILMOS,JLMOS}), LAKES (WITH ARGUMENTS
C     * ((ILKMOS,JLKMOS}) AND WATER/ICE (WITH ARGUMENTS
C     * {IWMOS,JWMOS}).
C
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER NM,NTSTART,NTEND,IM,NL,ILG,IL1,IL2
      INTEGER I,J
C
C     * OUTPUT FIELDS.
C
      INTEGER  IMOS  (ILG),  JMOS  (ILG)
C
C     * INPUT FIELDS.
C 
      REAL FAREA (NL,IM)
C---------------------------------------------------------------------
      NM=0
      DO 100 J=NTSTART,NTEND
      DO 100 I=IL1,IL2
         IF(FAREA(I,J).GT.0.0)                          THEN
            NM=NM+1
            IMOS(NM)=I
            JMOS(NM)=J
         ENDIF
  100 CONTINUE
C
      RETURN
      END
