      SUBROUTINE HYDLABT(LC,LG,LCT,LGT,LCTEM,LCTG,LCTEMG,
     1                   ICAN,ICANP1,IGND,ICTEM,ICTEMP1,NTLD)

C     * Feb 15/16 - M.Lazare. New version for gcm19:
C     *                       - ICTEMP1 passed in and used to dimension LCTEM
C     *                         to handle extra "level" for SOILCMAS and LITRCMAS
C     *                         for CTEM now in CLASS.
C     *                       - ICAN also passed in to help define new "LCTG"
C     *                         array, along with IGND and NTLD..
C     *                       - ICTEM,NTLD,IGND used to define new "LCTEMG" array
C     *                       - Also, LCTEM is defined the usual non-tiled
C     *                         way if only one land tile.
C     * Jan 26/14 - M.Lazare. Previous version for gcm18:
C     *                       - ICTEM passed in and used to dimension LCTEM.
C     *                       - IM->NTLD for land-only tiles. 
C     *                       - Now IMPLICT NONE. 
C     * NOV 08/11 - M.LAZARE. PREVIOUS VERSION HYDLABT FOR GCM15J+ USING
C     *                       CLASS_V3.5 AND TILES.  
C     * AUG 13/91 - M.LAZARE. PREVIOUS VERSION HYDLAB FOR GCM7 IHYD=2 (ICAN=4). 
C 
C     * DEFINES LEVEL INDEX VALUES FOR CANOPY AND SUB-SURFACE LAND- 
C     * SURFACE SCHEME (IHYD=2) ARRAYS. 
C     * CANOPY ARRAY IS EXTENDED TO INCLUDE "URBAN" CLASS.
C 
      IMPLICIT NONE

      INTEGER ICAN,ICANP1,IGND,ICTEM,ICTEMP1,NTLD,K,L,M,
     1        MLC,MLG,MLCT,MLCG

      INTEGER LC(ICANP1),LG(IGND)
      INTEGER LCT(NTLD*ICANP1),LGT(NTLD*IGND),LCTEM(NTLD*ICTEMP1)
      INTEGER LCTG(NTLD*IGND*ICAN), LCTEMG(NTLD*IGND*ICTEM)
C-----------------------------------------------------------------------
C     * NON-TILED USUAL INPUT/OUTPUT.
C
      DO 10 L=1,ICANP1 
        LC(L)=L 
  10  CONTINUE
  
      DO 20 L=1,IGND 
        LG(L)=L 
  20  CONTINUE
C
C     * TILED INPUT/OUTPUT.
C
      MLC=0
      DO 100 L=1,ICANP1 
      DO 100 M=1,NTLD
        MLC      = MLC+1
        LCT(MLC) = 1000*L + M 
  100 CONTINUE
C
      MLG=0   
      DO 200 L=1,IGND 
      DO 200 M=1,NTLD
        MLG      = MLG+1
        LGT(MLG) = 1000*L + M 
  200 CONTINUE
C
      MLCG=0   
      DO L=1,ICAN
      DO K=1,IGND
      DO M=1,NTLD
        MLCG     = MLCG+1
        LCTG(MLCG) = 1000*L + K*M
      ENDDO
      ENDDO
      ENDDO
C
      IF(NTLD.EQ.1) THEN
        DO L=1,ICTEMP1 
          LCTEM(L)=L 
        ENDDO
C
        MLCG=0   
        DO L=1,ICAN
        DO K=1,IGND
          MLCG         = MLCG+1
          LCTEMG(MLCG) = 1000*L + K
        ENDDO
        ENDDO
      ELSE
        MLCT=0   
        DO L=1,ICTEMP1
        DO M=1,NTLD
          MLCT        = MLCT+1
          LCTEM(MLCT) = 1000*L + M 
        ENDDO
        ENDDO
C
        MLCG=0   
        DO L=1,ICTEM
        DO K=1,IGND
        DO M=1,NTLD
          MLCG         = MLCG+1
          LCTEMG(MLCG) = 1000*L + K*M
        ENDDO
        ENDDO
        ENDDO
      ENDIF
  
      RETURN
      END
