      SUBROUTINE CLASSG (TBARGAT,THLQGAT,THICGAT,TPNDGAT,ZPNDGAT,
     1                   TBASGAT,ALBSGAT,TSNOGAT,RHOSGAT,SNOGAT, 
     2                   TCANGAT,RCANGAT,SCANGAT,GROGAT, CMAIGAT,
     3                   FCANGAT,LNZ0GAT,ALVCGAT,ALICGAT,PAMXGAT,
     4                   PAMNGAT,CMASGAT,ROOTGAT,RSMNGAT,QA50GAT,
     5                   VPDAGAT,VPDBGAT,PSGAGAT,PSGBGAT,PAIDGAT,
     6                   HGTDGAT,ACVDGAT,ACIDGAT,TSFSGAT,WSNOGAT,
     7                   THPGAT, THRGAT, THMGAT, BIGAT,  PSISGAT,
     8                   GRKSGAT,THRAGAT,HCPSGAT,TCSGAT, IGDRGAT,
     9                   THFCGAT,THLWGAT,PSIWGAT,DLZWGAT,ZBTWGAT,
     A                   VMODGAT,ZSNLGAT,ZPLGGAT,ZPLSGAT,TACGAT, 
     B                   QACGAT, DRNGAT, XSLPGAT,GRKFGAT,WFSFGAT,
     C                   WFCIGAT,ALGWVGAT,ALGWNGAT,ALGDVGAT,
     D                   ALGDNGAT,ASVDGAT,ASIDGAT,AGVDGAT,
     E                   AGIDGAT,ISNDGAT,RADJGAT,ZBLDGAT,Z0ORGAT,
     F                   ZRFMGAT,ZRFHGAT,ZDMGAT, ZDHGAT, FSVGAT,
     G                   FSIGAT, FSDBGAT,FSFBGAT,FSSBGAT,CSZGAT,
     H                   FSGGAT, FLGGAT, FDLGAT, ULGAT,  VLGAT,
     I                   TAGAT,  QAGAT,  PRESGAT,PREGAT, PADRGAT,
     J                   VPDGAT, TADPGAT,RHOAGAT,RPCPGAT,TRPCGAT,
     K                   SPCPGAT,TSPCGAT,RHSIGAT,FCLOGAT,DLONGAT,
     L                   GGEOGAT,GUSTGAT,REFGAT, BCSNGAT,DEPBGAT,   
     M                   ILMOS,JLMOS,
     N                   NML,NL,NT,NM,ILG,IG,IC,ICP1,NBS,
     O                   TBARROT,THLQROT,THICROT,TPNDROT,ZPNDROT,
     P                   TBASROT,ALBSROT,TSNOROT,RHOSROT,SNOROT, 
     Q                   TCANROT,RCANROT,SCANROT,GROROT, CMAIROT,
     R                   FCANROT,LNZ0ROT,ALVCROT,ALICROT,PAMXROT,
     S                   PAMNROT,CMASROT,ROOTROT,RSMNROT,QA50ROT,
     T                   VPDAROT,VPDBROT,PSGAROT,PSGBROT,PAIDROT,
     U                   HGTDROT,ACVDROT,ACIDROT,TSFSROT,WSNOROT,
     V                   THPROT, THRROT, THMROT, BIROT,  PSISROT,
     W                   GRKSROT,THRAROT,HCPSROT,TCSROT, IGDRROT,
     X                   THFCROT,THLWROT,PSIWROT,DLZWROT,ZBTWROT,
     Y                   VMODL,  ZSNLROT,ZPLGROT,ZPLSROT,TACROT,
     Z                   QACROT, DRNROT, XSLPROT,GRKFROT,WFSFROT,
     +                   WFCIROT,ALGWVROT,ALGWNROT,ALGDVROT,
     +                   ALGDNROT,ASVDROT,ASIDROT,AGVDROT,
     +                   AGIDROT,ISNDROT,RADJ   ,ZBLDROW,Z0ORROW,
     +                   ZRFMROW,ZRFHROW,ZDMROW, ZDHROW, FSVROT,
     +                   FSIROT, FSDBROT,FSFBROT,FSSBROT,CSZROW,
     +                   FSGROT, FLGROT, FDLROT, ULROW,  VLROW,
     +                   TAROW,  QAROW,  PRESROW,PREROW, PADRROW,
     +                   VPDROW, TADPROW,RHOAROW,RPCPROW,TRPCROW,
     +                   SPCPROW,TSPCROW,RHSIROW,FCLOROW,DLONROW,
     +                   GGEOROW,GUSTROL,REFROT, BCSNROT,DEPBROW )  
C
C     * Jan 16, 2015 - M.Lazare. New version called by "sfcproc3":
C     *                          - Add THLW.
C     *                          - {ALGWV,ALGWN,ALGDV,ALGDN} replace
C     *                            {ALGW,ALGD}.
C     *                          - FSG,FLG,GUST added.
C     *                          - FDLROW changed to FDLROL (cosmetic).
C     *                          - Adds GTGAT/GTROT.
C     *                          - Adds NT (NTLD in sfcproc2
C     *                            call) to dimension land-only
C     *                            ROT fields, consistent with
C     *                            new comrow12.
C     *                          - Unused IWMOS,JWMOS removed.
C     * Jun 13, 2013 - M.Lazare. CLASS gather routine called by 
C     *                          "sfcproc" in new version gcm17.
C     * NOTE: This contains the following changes compared to the
C     *       working temporary version used in conjunction with
C     *       updates to gcm16 (ie not official):
C     *         1) {DEPB,REF,BCSN} added for Maryam's new code.
C     *         2) {FSDB,FSFB,FSSB} added for Jason's new code.
C     * OCT 18/11 - M.LAZARE.  ADD IGDR.
C     * OCT 07/11 - M.LAZARE.  ADD VMODL->VMODGAT.
C     * OCT 05/11 - M.LAZARE.  PUT BACK IN PRESGROW->PRESGAT
C     *                        REQUIRED FOR ADDED SURFACE RH 
C     *                        CALCULATION.
C     * OCT 03/11 - M.LAZARE.  REMOVE ALL INITIALIZATION TO
C     *                        ZERO OF GAT ARRAYS (NOW DONE
C     *                        IN CLASS DRIVER).
C     * SEP 16/11 - M.LAZARE.  - ROW->ROT AND GRD->ROW.
C     *                        - REMOVE INITIALIZATION OF
C     *                          {ALVS,ALIR} TO ZERO.
C     *                        - REMOVE PRESGROW->PRESGAT 
C     *                          (OCEAN-ONLY NOW).
C     *                        - RADJROW (64-BIT) NOW RADJ
C     *                          (32-BIT).
C     * MAR 23/06 - D.VERSEGHY. ADD WSNO,FSNO,GGEO.
C     * MAR 18/05 - D.VERSEGHY. ADDITIONAL VARIABLES.
C     * FEB 18/05 - D.VERSEGHY. ADD "TSFS" VARIABLES.
C     * NOV 03/04 - D.VERSEGHY. ADD "IMPLICIT NONE" COMMAND.
C     * AUG 15/02 - D.VERSEGHY. GATHER OPERATION ON CLASS 
C     *                         VARIABLES.
C 
      IMPLICIT NONE
C
C     * INTEGER CONSTANTS.
C
      INTEGER  NML,NL,NM,NT,ILG,IG,IC,ICP1,K,L,M,NBS
C
C     * LAND SURFACE PROGNOSTIC VARIABLES.
C
      REAL    TBARROT(NL,NT,IG), THLQROT(NL,NT,IG), THICROT(NL,NT,IG), 
     1        TPNDROT(NL,NT),    ZPNDROT(NL,NT),    TBASROT(NL,NT),   
     2        ALBSROT(NL,NM),    TSNOROT(NL,NM),    RHOSROT(NL,NM),   
     3        SNOROT (NL,NM),    TCANROT(NL,NT),    RCANROT(NL,NT),   
     4        SCANROT(NL,NT),    GROROT (NL,NT),    CMAIROT(NL,NT),
     5        TSFSROT(NL,NT,4),  TACROT (NL,NT),    QACROT (NL,NT),
     6        WSNOROT(NL,NM),    REFROT (NL,NM),    BCSNROT(NL,NM)
C
      REAL    TBARGAT(ILG,IG),   THLQGAT(ILG,IG),   THICGAT(ILG,IG), 
     1        TPNDGAT(ILG),      ZPNDGAT(ILG),      TBASGAT(ILG),   
     2        ALBSGAT(ILG),      TSNOGAT(ILG),      RHOSGAT(ILG),   
     3        SNOGAT (ILG),      TCANGAT(ILG),      RCANGAT(ILG),   
     4        SCANGAT(ILG),      GROGAT (ILG),      CMAIGAT(ILG),
     5        TSFSGAT(ILG,4),    TACGAT (ILG),      QACGAT (ILG),
     6        WSNOGAT(ILG),      REFGAT (ILG),      BCSNGAT(ILG)
C
C     * GATHER-SCATTER INDEX ARRAYS.
C
      INTEGER  ILMOS (ILG),  JLMOS  (ILG)
C
C     * CANOPY AND SOIL INFORMATION ARRAYS.
C     * (THE LENGTH OF THESE ARRAYS IS DETERMINED BY THE NUMBER
C     * OF SOIL LAYERS (3) AND THE NUMBER OF BROAD VEGETATION
C     * CATEGORIES (4, OR 5 INCLUDING URBAN AREAS).)
C
      REAL          FCANROT(NL,NT,ICP1), LNZ0ROT(NL,NT,ICP1),
     1              ALVCROT(NL,NT,ICP1), ALICROT(NL,NT,ICP1),
     2              PAMXROT(NL,NT,IC),   PAMNROT(NL,NT,IC),
     3              CMASROT(NL,NT,IC),   ROOTROT(NL,NT,IC),
     4              RSMNROT(NL,NT,IC),   QA50ROT(NL,NT,IC),
     5              VPDAROT(NL,NT,IC),   VPDBROT(NL,NT,IC),
     6              PSGAROT(NL,NT,IC),   PSGBROT(NL,NT,IC),
     7              PAIDROT(NL,NT,IC),   HGTDROT(NL,NT,IC),
     8              ACVDROT(NL,NT,IC),   ACIDROT(NL,NT,IC)
C
      REAL          FCANGAT(ILG,ICP1),   LNZ0GAT(ILG,ICP1),
     1              ALVCGAT(ILG,ICP1),   ALICGAT(ILG,ICP1),
     2              PAMXGAT(ILG,IC),     PAMNGAT(ILG,IC),
     3              CMASGAT(ILG,IC),     ROOTGAT(ILG,IC),
     4              RSMNGAT(ILG,IC),     QA50GAT(ILG,IC),
     5              VPDAGAT(ILG,IC),     VPDBGAT(ILG,IC),
     6              PSGAGAT(ILG,IC),     PSGBGAT(ILG,IC),
     7              PAIDGAT(ILG,IC),     HGTDGAT(ILG,IC),
     8              ACVDGAT(ILG,IC),     ACIDGAT(ILG,IC)
C
      REAL    THPROT (NL,NT,IG), THRROT (NL,NT,IG), THMROT (NL,NT,IG),
     1        BIROT  (NL,NT,IG), PSISROT(NL,NT,IG), GRKSROT(NL,NT,IG),   
     2        THRAROT(NL,NT,IG), HCPSROT(NL,NT,IG), 
     3        TCSROT (NL,NT,IG), THFCROT(NL,NT,IG), THLWROT(NL,NT,IG),
     4        PSIWROT(NL,NT,IG), DLZWROT(NL,NT,IG), ZBTWROT(NL,NT,IG), 
     5        DRNROT (NL,NT),    XSLPROT(NL,NT),    GRKFROT(NL,NT),
     6        WFSFROT(NL,NT),    WFCIROT(NL,NT),    ALGWVROT(NL,NT),   
     7        ALGWNROT(NL,NT),   ALGDVROT(NL,NT),   ALGDNROT(NL,NT),
     8        ASVDROT(NL,NM),    ASIDROT(NL,NM),   
     9        AGVDROT(NL,NT),    AGIDROT(NL,NT),    ZSNLROT(NL,NT),
     A        ZPLGROT(NL,NT),    ZPLSROT(NL,NT)
C

      REAL    THPGAT (ILG,IG),   THRGAT (ILG,IG),   THMGAT (ILG,IG),
     1        BIGAT  (ILG,IG),   PSISGAT(ILG,IG),   GRKSGAT(ILG,IG),   
     2        THRAGAT(ILG,IG),   HCPSGAT(ILG,IG), 
     3        TCSGAT (ILG,IG),   THFCGAT(ILG,IG),   THLWGAT(ILG,IG),
     4        PSIWGAT(ILG,IG),   DLZWGAT(ILG,IG),   ZBTWGAT(ILG,IG),   
     5        DRNGAT (ILG),      XSLPGAT(ILG),      GRKFGAT(ILG),
     6        WFSFGAT(ILG),      WFCIGAT(ILG),      ALGWVGAT(ILG),     
     7        ALGWNGAT(ILG),     ALGDVGAT(ILG),     ALGDNGAT(ILG),
     8        ASVDGAT(ILG),      ASIDGAT(ILG),     
     9        AGVDGAT(ILG),      AGIDGAT(ILG),      ZSNLGAT(ILG),
     A        ZPLGGAT(ILG),      ZPLSGAT(ILG)
C
      INTEGER ISNDROT(NL,NT,IG), ISNDGAT(ILG,IG)
      INTEGER IGDRROT(NL,NT),    IGDRGAT(ILG)

C     * ATMOSPHERIC AND GRID-CONSTANT INPUT VARIABLES.
C
      REAL  ZRFMROW( NL), ZRFHROW( NL), ZDMROW ( NL), ZDHROW ( NL),
     1      CSZROW ( NL),
     3      ULROW  ( NL), VLROW  ( NL), TAROW  ( NL), QAROW  ( NL), 
     4      PRESROW( NL), PREROW ( NL), PADRROW( NL), VPDROW ( NL), 
     5      TADPROW( NL), RHOAROW( NL), ZBLDROW( NL), Z0ORROW( NL),
     6      RPCPROW( NL), TRPCROW( NL), SPCPROW( NL), TSPCROW( NL),
     7      RHSIROW( NL), FCLOROW( NL), DLONROW( NL), GGEOROW( NL),
     8      GUSTROL (NL), RADJ   ( NL), VMODL  ( NL), DEPBROW( NL)

      REAL, DIMENSION(NL,NM,NBS) ::  FSDBROT, FSFBROT, FSSBROT
      REAL, DIMENSION(NL,NM)     ::  FSVROT,  FSIROT,  FSGROT,  FLGROT,
     1                               FDLROT
C
      REAL  ZRFMGAT(ILG), ZRFHGAT(ILG), ZDMGAT (ILG), ZDHGAT (ILG),
     1      FSVGAT (ILG), FSIGAT (ILG), CSZGAT (ILG),
     2      FSGGAT (ILG), FLGGAT (ILG), FDLGAT (ILG), 
     3      ULGAT  (ILG), VLGAT  (ILG), TAGAT  (ILG), QAGAT  (ILG), 
     4      PRESGAT(ILG), PREGAT (ILG), PADRGAT(ILG), VPDGAT (ILG), 
     5      TADPGAT(ILG), RHOAGAT(ILG), ZBLDGAT(ILG), Z0ORGAT(ILG),
     6      RPCPGAT(ILG), TRPCGAT(ILG), SPCPGAT(ILG), TSPCGAT(ILG),
     7      RHSIGAT(ILG), FCLOGAT(ILG), DLONGAT(ILG), GGEOGAT(ILG),
     8      GUSTGAT(ILG), RADJGAT(ILG), VMODGAT(ILG), DEPBGAT(ILG)

      REAL, DIMENSION(ILG,NBS) :: FSDBGAT, FSFBGAT, FSSBGAT
C----------------------------------------------------------------------
      DO 100 K=1,NML
          TPNDGAT(K)=TPNDROT(ILMOS(K),JLMOS(K))  
          ZPNDGAT(K)=ZPNDROT(ILMOS(K),JLMOS(K))  
          TBASGAT(K)=TBASROT(ILMOS(K),JLMOS(K))  
          ALBSGAT(K)=ALBSROT(ILMOS(K),JLMOS(K))  
          TSNOGAT(K)=TSNOROT(ILMOS(K),JLMOS(K))  
          RHOSGAT(K)=RHOSROT(ILMOS(K),JLMOS(K))  
          SNOGAT (K)=SNOROT (ILMOS(K),JLMOS(K))  
          REFGAT (K)=REFROT (ILMOS(K),JLMOS(K)) 
          BCSNGAT(K)=BCSNROT(ILMOS(K),JLMOS(K)) 
          WSNOGAT(K)=WSNOROT(ILMOS(K),JLMOS(K))  
          TCANGAT(K)=TCANROT(ILMOS(K),JLMOS(K))  
          RCANGAT(K)=RCANROT(ILMOS(K),JLMOS(K))  
          SCANGAT(K)=SCANROT(ILMOS(K),JLMOS(K))  
          GROGAT (K)=GROROT (ILMOS(K),JLMOS(K))  
          CMAIGAT(K)=CMAIROT(ILMOS(K),JLMOS(K))  
          DRNGAT (K)=DRNROT (ILMOS(K),JLMOS(K))  
          XSLPGAT(K)=XSLPROT(ILMOS(K),JLMOS(K))  
          GRKFGAT(K)=GRKFROT(ILMOS(K),JLMOS(K))  
          WFSFGAT(K)=WFSFROT(ILMOS(K),JLMOS(K))  
          WFCIGAT(K)=WFCIROT(ILMOS(K),JLMOS(K))  
          ALGWVGAT(K)=ALGWVROT(ILMOS(K),JLMOS(K))  
          ALGWNGAT(K)=ALGWNROT(ILMOS(K),JLMOS(K))  
          ALGDVGAT(K)=ALGDVROT(ILMOS(K),JLMOS(K))  
          ALGDNGAT(K)=ALGDNROT(ILMOS(K),JLMOS(K))  
          ASVDGAT(K)=ASVDROT(ILMOS(K),JLMOS(K))  
          ASIDGAT(K)=ASIDROT(ILMOS(K),JLMOS(K))  
          AGVDGAT(K)=AGVDROT(ILMOS(K),JLMOS(K))  
          AGIDGAT(K)=AGIDROT(ILMOS(K),JLMOS(K))  
          ZSNLGAT(K)=ZSNLROT(ILMOS(K),JLMOS(K))  
          ZPLGGAT(K)=ZPLGROT(ILMOS(K),JLMOS(K))  
          ZPLSGAT(K)=ZPLSROT(ILMOS(K),JLMOS(K))  
          TACGAT (K)=TACROT (ILMOS(K),JLMOS(K))  
          QACGAT (K)=QACROT (ILMOS(K),JLMOS(K))  
          IGDRGAT(K)=IGDRROT(ILMOS(K),JLMOS(K))
          ZBLDGAT(K)=ZBLDROW(ILMOS(K))
          Z0ORGAT(K)=Z0ORROW(ILMOS(K))
          ZRFMGAT(K)=ZRFMROW(ILMOS(K))
          ZRFHGAT(K)=ZRFHROW(ILMOS(K))
          ZDMGAT (K)=ZDMROW(ILMOS(K))
          ZDHGAT (K)=ZDHROW(ILMOS(K))
          FSVGAT (K)=FSVROT (ILMOS(K),JLMOS(K))
          FSIGAT (K)=FSIROT (ILMOS(K),JLMOS(K))
          CSZGAT (K)=CSZROW (ILMOS(K))
          FSGGAT (K)=FSGROT (ILMOS(K),JLMOS(K))
          FLGGAT (K)=FLGROT (ILMOS(K),JLMOS(K))
          FDLGAT (K)=FDLROT (ILMOS(K),JLMOS(K))
          ULGAT  (K)=ULROW  (ILMOS(K))
          VLGAT  (K)=VLROW  (ILMOS(K))
          TAGAT  (K)=TAROW  (ILMOS(K))
          QAGAT  (K)=QAROW  (ILMOS(K))
          PRESGAT(K)=PRESROW(ILMOS(K))
          PREGAT (K)=PREROW (ILMOS(K))
          PADRGAT(K)=PADRROW(ILMOS(K))
          VPDGAT (K)=VPDROW (ILMOS(K))
          TADPGAT(K)=TADPROW(ILMOS(K))
          RHOAGAT(K)=RHOAROW(ILMOS(K))
          RPCPGAT(K)=RPCPROW(ILMOS(K))
          TRPCGAT(K)=TRPCROW(ILMOS(K))
          SPCPGAT(K)=SPCPROW(ILMOS(K))
          TSPCGAT(K)=TSPCROW(ILMOS(K))
          RHSIGAT(K)=RHSIROW(ILMOS(K))
          FCLOGAT(K)=FCLOROW(ILMOS(K))
          DLONGAT(K)=DLONROW(ILMOS(K))
          GGEOGAT(K)=GGEOROW(ILMOS(K))
          GUSTGAT(K)=GUSTROL(ILMOS(K))
          RADJGAT(K)=RADJ   (ILMOS(K))
          VMODGAT(K)=VMODL  (ILMOS(K))
          DEPBGAT(K)=DEPBROW(ILMOS(K))
  100 CONTINUE
C
      DO 250 L=1,IG
      DO 200 K=1,NML
          TBARGAT(K,L)=TBARROT(ILMOS(K),JLMOS(K),L)
          THLQGAT(K,L)=THLQROT(ILMOS(K),JLMOS(K),L)
          THICGAT(K,L)=THICROT(ILMOS(K),JLMOS(K),L)
          THPGAT (K,L)=THPROT (ILMOS(K),JLMOS(K),L)
          THRGAT (K,L)=THRROT (ILMOS(K),JLMOS(K),L)
          THMGAT (K,L)=THMROT (ILMOS(K),JLMOS(K),L)
          BIGAT  (K,L)=BIROT  (ILMOS(K),JLMOS(K),L)
          PSISGAT(K,L)=PSISROT(ILMOS(K),JLMOS(K),L)
          GRKSGAT(K,L)=GRKSROT(ILMOS(K),JLMOS(K),L)
          THRAGAT(K,L)=THRAROT(ILMOS(K),JLMOS(K),L)
          HCPSGAT(K,L)=HCPSROT(ILMOS(K),JLMOS(K),L)
          TCSGAT (K,L)=TCSROT (ILMOS(K),JLMOS(K),L)
          THFCGAT(K,L)=THFCROT(ILMOS(K),JLMOS(K),L)
          THLWGAT(K,L)=THLWROT(ILMOS(K),JLMOS(K),L)
          PSIWGAT(K,L)=PSIWROT(ILMOS(K),JLMOS(K),L)
          DLZWGAT(K,L)=DLZWROT(ILMOS(K),JLMOS(K),L)
          ZBTWGAT(K,L)=ZBTWROT(ILMOS(K),JLMOS(K),L)
          ISNDGAT(K,L)=ISNDROT(ILMOS(K),JLMOS(K),L)
  200 CONTINUE
  250 CONTINUE
C
      DO 300 L=1,ICP1
      DO 300 K=1,NML
          FCANGAT(K,L)=FCANROT(ILMOS(K),JLMOS(K),L)
          LNZ0GAT(K,L)=LNZ0ROT(ILMOS(K),JLMOS(K),L)
          ALVCGAT(K,L)=ALVCROT(ILMOS(K),JLMOS(K),L)
          ALICGAT(K,L)=ALICROT(ILMOS(K),JLMOS(K),L)
  300 CONTINUE
C
      DO 400 L=1,IC
      DO 400 K=1,NML
          PAMXGAT(K,L)=PAMXROT(ILMOS(K),JLMOS(K),L)
          PAMNGAT(K,L)=PAMNROT(ILMOS(K),JLMOS(K),L)
          CMASGAT(K,L)=CMASROT(ILMOS(K),JLMOS(K),L)
          ROOTGAT(K,L)=ROOTROT(ILMOS(K),JLMOS(K),L)
          RSMNGAT(K,L)=RSMNROT(ILMOS(K),JLMOS(K),L)
          QA50GAT(K,L)=QA50ROT(ILMOS(K),JLMOS(K),L)
          VPDAGAT(K,L)=VPDAROT(ILMOS(K),JLMOS(K),L)
          VPDBGAT(K,L)=VPDBROT(ILMOS(K),JLMOS(K),L)
          PSGAGAT(K,L)=PSGAROT(ILMOS(K),JLMOS(K),L)
          PSGBGAT(K,L)=PSGBROT(ILMOS(K),JLMOS(K),L)
          PAIDGAT(K,L)=PAIDROT(ILMOS(K),JLMOS(K),L)
          HGTDGAT(K,L)=HGTDROT(ILMOS(K),JLMOS(K),L)
          ACVDGAT(K,L)=ACVDROT(ILMOS(K),JLMOS(K),L)
          ACIDGAT(K,L)=ACIDROT(ILMOS(K),JLMOS(K),L)
          TSFSGAT(K,L)=TSFSROT(ILMOS(K),JLMOS(K),L)
400   CONTINUE
C
      DO L = 1, NBS
         DO K = 1, NML
            FSDBGAT(K,L) = FSDBROT(ILMOS(K),JLMOS(K),L)
            FSFBGAT(K,L) = FSFBROT(ILMOS(K),JLMOS(K),L)
            FSSBGAT(K,L) = FSSBROT(ILMOS(K),JLMOS(K),L)
         END DO ! K
      END DO ! L

      RETURN
      END
