      SUBROUTINE GRALB(ALVSG,ALIRG,ALVSGC,ALIRGC,
     1                 ALGWV,ALGWN,ALGDV,ALGDN,
     2                 THLIQ,FSNOW,ALVSU,ALIRU,FCMXU,
     3                 AGVDAT,AGIDAT,FG,ISAND,
     4                 ILG,IG,IL1,IL2,JL,IALG)
C
C     * FEB 09/15 - D.VERSEGHY. New version for gcm18 and class 3.6:
C     *                         - Wet and dry albedoes for EACH of
C     *                           visible and near-ir are passed in
C     *                           instead of ALGWET and ALGDRY. These
C     *                           are used to calculate ALISG and ALIRG.
C     * NOV 16/13 - M.LAZARE.   FINAL VERSION FOR GCM17:
C     *                         - REMOVE UNNECESSARY LOWER BOUND
C     *                           OF 1.E-5 ON "FURB".
C     * APR 13/06 - D.VERSEGHY. SEPARATE ALBEDOS FOR OPEN AND
C     *                         CANOPY-COVERED GROUND.
C     * NOV 03/04 - D.VERSEGHY. ADD "IMPLICIT NONE" COMMAND.
C     * SEP 04/03 - D.VERSEGHY. RATIONALIZE CALCULATION OF URBAN
C     *                         ALBEDO.
C     * MAR 18/02 - D.VERSEGHY. UPDATES TO ALLOW ASSIGNMENT OF USER-
C     *                         SPECIFIED VALUES TO GROUND ALBEDO.
C     *                         PASS IN ICE AND ORGANIC ALBEDOS
C     *                         VIA NEW COMMON BLOCK "CLASS8".
C     * FEB 07/02 - D.VERSEGHY. REVISIONS TO BARE SOIL ALBEDO
C     *                         CALCULATIONS; REMOVAL OF SOIL
C     *                         MOISTURE EXTRAPOLATION TO SURFACE.
C     * JUN 05/97 - D.VERSEGHY. CLASS - VERSION 2.7.
C     *                         CALCULATE SOIL ALBEDO FROM PERCENT
C     *                         SAND CONTENT RATHER THAN FROM COLOUR 
C     *                         INDEX.
C     * SEP 27/96 - D.VERSEGHY. CLASS - VERSION 2.5.
C     *                         FIX BUG TO CALCULATE GROUND ALBEDO
C     *                         UNDER CANOPIES AS WELL AS OVER BARE
C     *                         SOIL.
C     * NOV 29/94 - M.LAZARE.   CLASS - VERSION 2.3.
C     *                         "CALL ABORT" CHANGED TO "CALL XIT",
C     *                         TO ENABLE RUNNING RUN ON PC'S.
C     * FEB 12/93 - D.VERSEGHY/M.LAZARE. INCREASE DRY SOIL ALBEDO TO 
C     *                                  0.45 FROM 0.35. 
C     * MAR 13/92 - D.VERSEGHY/M.LAZARE. REVISED AND VECTORIZED CODE
C     *                                  FOR MODEL VERSION GCM7.
C     * AUG 12/91 - D.VERSEGHY. CLASS - VERSION 2.0.
C     *                         CODE FOR MODEL VERSION GCM7U (WITH
C     *                         CANOPY). 
C     * APR 11/89 - D.VERSEGHY. CALCULATE VISIBLE AND NEAR-IR SOIL
C     *                         ALBEDOS BASED ON TEXTURE AND SURFACE
C     *                         WETNESS. (SET TO ICE ALBEDOS OVER
C     *                         CONTINENTAL ICE SHEETS.)
C
      IMPLICIT NONE
C                
C     * INTEGER CONSTANTS.
C
      INTEGER  ILG,IG,IL1,IL2,JL,IALG,IPTBAD,I
C
C     * OUTPUT ARRAYS.
C
      REAL ALVSG (ILG),   ALIRG (ILG),  ALVSGC (ILG),  ALIRGC (ILG)
C
C     * INPUT ARRAYS.
C
      REAL THLIQ (ILG,IG)
      REAL ALGWV (ILG),   ALGWN(ILG),  ALGDV(ILG),  ALGDN(ILG),
     2     FSNOW (ILG),   ALVSU (ILG),   ALIRU (ILG),   FCMXU (ILG),
     3     AGVDAT(ILG),   AGIDAT(ILG),   FG    (ILG)
C
      INTEGER    ISAND  (ILG,IG)
C
C     * TEMPORARY VARIABLES.
C
      REAL FURB,ALBSOL
C
C     * COMMON BLOCK PARAMETERS.
C
      REAL ALVSI,ALIRI,ALVSO,ALIRO,ALBRCK
C
      COMMON /CLASS8/ ALVSI,ALIRI,ALVSO,ALIRO,ALBRCK
C---------------------------------------------------------------------
      IPTBAD=0
      DO 100 I=IL1,IL2
         IF(IALG.EQ.0)                                          THEN
            IF(ISAND(I,1).GE.0)                          THEN
                FURB=FCMXU(I)*(1.0-FSNOW(I))
                IF(THLIQ(I,1).GE.0.26) THEN
                   ALVSG(I)=ALGWV(I)
                   ALIRG(I)=ALGWN(I)
                ELSEIF(THLIQ(I,1).LE.0.22) THEN
                   ALVSG(I)=ALGDV(I)
                   ALIRG(I)=ALGDN(I)
                ELSE
                   ALVSG(I)=THLIQ(I,1)*(ALGWV(I)-ALGDV(I))/0.04+
     1                    ALGDV(I)-5.50*(ALGWV(I)-ALGDV(I))
                   ALIRG(I)=THLIQ(I,1)*(ALGWN(I)-ALGDN(I))/0.04+
     1                    ALGDN(I)-5.50*(ALGWN(I)-ALGDN(I))
                ENDIF
C
                IF(FG(I).GT.0.001)                          THEN
                  ALVSG(I)=((FG(I)-FURB)*ALVSG(I)+FURB*ALVSU(I))/FG(I)
                  ALIRG(I)=((FG(I)-FURB)*ALIRG(I)+FURB*ALIRU(I))/FG(I)
                ENDIF
                IF(ALVSG(I).GT.1.0.OR.ALVSG(I).LT.0.0) IPTBAD=I
                IF(ALIRG(I).GT.1.0.OR.ALIRG(I).LT.0.0) IPTBAD=I
            ELSE IF(ISAND(I,1).EQ.-4)                    THEN
                ALVSG(I)=ALVSI
                ALIRG(I)=ALIRI
            ELSE IF(ISAND(I,1).EQ.-3)                    THEN
                ALVSG(I)=ALGDV(I)
                ALIRG(I)=ALGDN(I)
            ELSE IF(ISAND(I,1).EQ.-2)                    THEN
                ALVSG(I)=ALVSO
                ALIRG(I)=ALIRO
            ENDIF
         ELSEIF(IALG.EQ.1)                                     THEN
            ALVSG(I)=AGVDAT(I)
            ALIRG(I)=AGIDAT(I)
         ENDIF     
         ALVSGC(I)=ALVSG(I)
         ALIRGC(I)=ALIRG(I)
  100 CONTINUE
C
      IF(IPTBAD.NE.0) THEN
         WRITE(6,6100) IPTBAD,JL,ALVSG(IPTBAD),ALIRG(IPTBAD)
 6100    FORMAT('0AT (I,J)= (',I3,',',I3,'), ALVSG,ALIRG = ',2F10.5)
         CALL XIT('GRALB',-1)
      ENDIF

      RETURN                                                                      
      END
