      SUBROUTINE HYDCON5(DELT)
C     * Feb 09, 2015 - D.Verseghy. New version for gcm18 and CLASS 3.6:
C     *                            - Modify value for TCSAND.
C     * Jan 06, 2010 - E. Chan     Previous version HYDCON4 for CLASS 3.5. 
C     *                            - Changed HCPICE from 1.96E6 to 1.9257E6.
C     *                            - Added new constants and common blocks. 
C     *                            - Shortened CLASS4 common block consistent
C     *                              with new CLASS version.
C     * JUN 12, 2005 - M.LAZARE.   Previous version HYDCON3 for gcm15f/g/h/i:
C     *                            - "DELT" passed through call instead
C     *                              of using "TIMES" common block (only
C     *                              used to read DELT).
C     * APRIL 11, 2001 - M.LAZARE. PREVIOUS VERSION HYDCON.
C     *                            DEFINES CONSTANTS IN COMMON BLOCKS
C     *                            AT START OF MODEL, USED IN CLASS.
C     *                            THIS IS CALLED AT THE BEGINNING OF
C     *                            A MODEL JOB, IN SECTION 0, AND
C     *                            APPLIES FOR MODEL VERSIONS GCM12
C     *                            THROUGH GCM15.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
C     * INPUT IS NATIVE REAL.
C
      REAL DELT

C     * ORIGINAL COMMON BLOCKS (NATIVE REAL).

      COMMON /PARAMS/ WW,     TW,    A,     ASQ,  GRAV, RGAS,  RGOCP,
     1                RGOASQ, CPRES,  RGASV,  CPRESV
      COMMON /PARAM1/ PI,     RVORD, TFREZ, HS,   HV,   DAYLNT
      COMMON /PARAM3/ CSNO,   CPACK, GTFSW, RKHI, SBC,  SNOMAX

C     * CLASS COMMON BLOCKS.

      COMMON /CLASS1/ DELTIM,CELZRO
      COMMON /CLASS2/ GAS,GASV,G,SIGMA,VKC,CT,VMIN
      COMMON /CLASS3/ TCW,TCICE,TCSAND,TCCLAY,TCOM,TCDRYS,
     1                RHOSOL,RHOOM
      COMMON /CLASS4/ HCPW,HCPICE,HCPSOL,HCPOM,HCPSND,HCPCLY,
     1                SPHW,SPHICE,SPHVEG,SPHAIR,RHOW,RHOICE,
     2                TCGLAC,CLHMLT,CLHVAP
      COMMON /CLASS5/ THPORG(3),THRORG(3),THMORG(3),BORG(3),
     1                PSISORG(3),GRKSORG(3)      
      COMMON /CLASS6/ CPI,GROWYR(18,4,2),ZOLNG,ZOLNS,ZOLNI,ZORAT(4),
     1                ZORATG
      COMMON /CLASS7/ CANEXT(4),XLEAF(4)
      COMMON /CLASS8/ ALVSI,ALIRI,ALVSO,ALIRO,ALBRCK
      COMMON /PHYCON/ DELTA,CGRAV,CKARM,CPD
      COMMON /CLASSD2/ AS,ASX,CI,BS,BETA,FACTN,HMIN,ANGMAX

C     * DATA FOR CLASS:

      DATA      VKC,        CT,         VMIN
     1       /  0.40,       1.00E-3,    0.1  /

      DATA      TCW,        TCICE,      TCSAND,     TCCLAY,     TCOM
     1       /  0.57,       2.24,       2.5,        2.5,        0.25   /

      DATA      TCDRYS,     RHOSOL,     RHOOM 
     1       /  0.275,      2.65E3,     1.30E3  /

      DATA      HCPW,       HCPICE,     HCPSOL,     HCPOM
     1       /  4.187E6,    1.9257E6,   2.25E6,     2.50E6 /
      
      DATA      HCPSND,     HCPCLY,     SPHW,       SPHICE,     SPHVEG
     1       /  2.13E6,     2.38E6,     4.186E3,    2.10E3,     2.70E3 /
         
      DATA      RHOW,       RHOICE,     TCGLAC,     CLHMLT,     CLHVAP
     1       /  1.0E3,      0.917E3,    2.24,       0.334E6,    2.501E6/

      DATA      ZOLNG,      ZOLNS,      ZOLNI,      ZORATG
     1       /  -4.605,     -6.908,     -6.215,     3.0         /
      
      DATA      ALVSI,      ALIRI,      ALVSO,      ALIRO,      ALBRCK
     1       /  0.95,       0.73,       0.05,       0.30,       0.16  /

      DATA      DELTA,      AS,         ASX,        ANGMAX
     1       /  0.608,      12.0,       4.7,        0.85   /

      DATA      CI,         BS,         BETA,       FACTN,      HMIN 
     1       /  40.0,       1.0,        1.0,        1.2,        40.   /
      
      DATA  GROWYR  /213.,213.,213.,213.,213.,213.,0.,0.,0.,
     1               0.,0.,0., 75.,106.,136.,167.,167.,167.,
     2               273.,273.,273.,273.,273.,273.,0.,0.,0.,
     3               0.,0.,0.,135.,166.,196.,196.,196.,196.,
     4               121.,121.,121.,121.,121.,121.,0.,0.,0.,
     5               0.,0.,0.,275.,244.,214.,214.,214.,214.,
     6               151.,151.,151.,151.,151.,151.,0.,0.,0.,
     7               0.,0.,0.,305.,274.,244.,244.,244.,244.,
     8               213.,213.,213.,213.,213.,213.,0.,0.,0.,
     9               0.,0., 75.,106.,136.,167.,167.,167.,167.,
     A               273.,273.,273.,273.,273.,273.,0.,0.,0.,
     B               0.,0.,135.,166.,196.,196.,196.,196.,196.,
     C               121.,121.,121.,121.,121.,121.,0.,0.,0.,
     D               0.,0.,275.,244.,214.,214.,214.,214.,214.,
     E               151.,151.,151.,151.,151.,151.,0.,0.,0.,
     F               0.,0.,305.,274.,244.,244.,244.,244.,244. /
C
      DATA ZORAT    /1.0,1.0,1.0,1.0/
      DATA CANEXT   /-0.5,-1.5,-0.8,-0.8/
      DATA XLEAF    /0.0247,0.0204,0.0456,0.0456/
      DATA THPORG   /0.93,0.88,0.83/
      DATA THRORG   /0.275,0.620,0.705/
      DATA THMORG   /0.04,0.15,0.22/
      DATA BORG     /2.7,6.1,12.0/
      DATA PSISORG  /0.0103,0.0102,0.0101/
      DATA GRKSORG  /2.8E-4,2.0E-6,1.0E-7/
C==================================================================
C     * RE-DEFINE CONSTANTS FOR STORAGE IN CLASS COMMON BLOCKS.
C
      DELTIM=DELT
      CELZRO=TFREZ
      GAS=RGAS
      GASV=RGASV
      G=GRAV
      CGRAV=GRAV
      SIGMA=SBC
      CKARM=VKC
      SPHAIR=CPRES
      CPD=CPRES
      CPI=PI
C==================================================================
      RETURN
      END
