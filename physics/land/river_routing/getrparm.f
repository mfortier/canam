!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!        
      SUBROUTINE GETRPARM(NFL,SLOPE,WIDTH,LENGTH,DIRECTION,GW_DELAY,
     1                    LAT1,LAT2,LAT3,LAT4,LAT5,LAT6,LAT7,
     2                    LON1,LON2,LON3,LON4,LON5,LON6,LON7,
     3                    LANDMASK,NLON,NLAT)
C
C=======================================================================
C     * Aug 20/2016 - M.Lazare. Add in new LANDMASK ("GC") field and
C     *                         re-order to fit order within Vivek's
C     *                         new file.
C     * NOV 10/2015 - M.Lazare. Adapted from coupler routine for AGCM:
C     *                         - no CPP directives.
C     *                         - LON,LAT,MAXX passed in instead of
C     *                           PARAMETER statement.
C     *                         
C     * GET THE RIVER ROUTING PARAMETERS FROM THE PARAMETER FILE
C     *
C=======================================================================

      IMPLICIT none

      INTEGER NLON      !<Variable description\f$[units]\f$
      INTEGER NLAT      !<Variable description\f$[units]\f$
      INTEGER NFL       !<Variable description\f$[units]\f$

      REAL        SLOPE     (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        WIDTH     (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LENGTH    (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        DIRECTION (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        GW_DELAY  (NLON,NLAT)          !<Variable description\f$[units]\f$

      REAL        LAT1      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LAT2      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LAT3      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LAT4      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LAT5      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LAT6      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LAT7      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON1      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON2      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON3      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON4      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON5      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON6      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LON7      (NLON,NLAT)          !<Variable description\f$[units]\f$
      REAL        LANDMASK  (NLON,NLAT)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER IBUF,IDAT,MACHINE,INTSIZE,MAXX
      INTEGER :: nc4to8 
      LOGICAL OK
C
      COMMON /ICOM/ IBUF(8),IDAT(1)
      COMMON /MACHTYP/ MACHINE, INTSIZE
C-----------------------------------------------------------------------
      MAXX=( NLON*NLAT + 8 )*MACHINE
      REWIND NFL      

      CALL GETFLD2(NFL,DIRECTION,-1,-1,nc4to8("DIRE"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-1)
      ENDIF

      CALL GETFLD2(NFL,LENGTH,   -1,-1,nc4to8("DIST"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-2)
      ENDIF

      CALL GETFLD2(NFL,LANDMASK, -1,-1,nc4to8("  GC"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-3)
      ENDIF

      CALL GETFLD2(NFL,GW_DELAY, -1,-1,nc4to8("  GW"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-4)
      ENDIF

      CALL GETFLD2(NFL,LAT1,     -1,-1,nc4to8("LAT1"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-5)
      ENDIF

      CALL GETFLD2(NFL,LAT2,     -1,-1,nc4to8("LAT2"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-6)
      ENDIF

      CALL GETFLD2(NFL,LAT3,     -1,-1,nc4to8("LAT3"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
      CALL                                   XIT('GETRPARM',-7)
      ENDIF

      CALL GETFLD2(NFL,LAT4,     -1,-1,nc4to8("LAT4"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-8)
      ENDIF

      CALL GETFLD2(NFL,LAT5,     -1,-1,nc4to8("LAT5"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-9)
      ENDIF

      CALL GETFLD2(NFL,LAT6,     -1,-1,nc4to8("LAT6"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-10)
      ENDIF

      CALL GETFLD2(NFL,LAT7,     -1,-1,nc4to8("LAT7"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-11)
      ENDIF

      CALL GETFLD2(NFL,LON1,     -1,-1,nc4to8("LNG1"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-12)
      ENDIF

      CALL GETFLD2(NFL,LON2,     -1,-1,nc4to8("LNG2"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-13)
      ENDIF

      CALL GETFLD2(NFL,LON3,     -1,-1,nc4to8("LNG3"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-14)
      ENDIF

      CALL GETFLD2(NFL,LON4,     -1,-1,nc4to8("LNG4"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-15)
      ENDIF

      CALL GETFLD2(NFL,LON5,     -1,-1,nc4to8("LNG5"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-16)
      ENDIF

      CALL GETFLD2(NFL,LON6,     -1,-1,nc4to8("LNG6"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-17)
      ENDIF

      CALL GETFLD2(NFL,LON7,     -1,-1,nc4to8("LNG7"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-18)
      ENDIF

      CALL GETFLD2(NFL,SLOPE,    -1,-1,nc4to8(" SLP"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-19)
      ENDIF

      CALL GETFLD2(NFL,WIDTH,    -1,-1,nc4to8("WDTH"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                 XIT('GETRPARM',-20)
      ENDIF

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
