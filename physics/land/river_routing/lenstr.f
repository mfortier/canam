!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      FUNCTION LENSTR( INPUT_STRING )

C     * PURPOSE: RETURNS THE NUMBER OF CHARACTERS IN THE INPUT STRING.
C            
C     * MODIFICATION:
C
C     * 10/15/98 BY SCOTT TINIS
C     * FILE CREATED.

      IMPLICIT NONE

      INTEGER I
      CHARACTER INPUT_STRING*(*)   !<INPUT STRING FOR PARSING\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER   LENSTR             ! FUNCTION


      DO I = LEN( INPUT_STRING ), 1, -1
         IF (INPUT_STRING(I:I) .NE. ' ') THEN
            LENSTR = I
            GO TO 999
         END IF
      END DO

      LENSTR = 0

  999 RETURN

      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
