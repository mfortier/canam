!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE GETSTRTOK( INPUT_STRING, NTOK, TOK )

C     * PURPOSE: EVALUATES THE NUMBER OF VALID TOKENS IN A CHARACTER STRING
C     *          AND RETURNS THEIR VALUES.  THE STRUCTURE OF A COMMAND STRING
C     *          IS:  TOKEN_ID  TOK1 ... TOKN # COMMENT.  TOKENS ARE PRECEDED 
C     *          BY A TOKEN IDENTIFIER (HARDCODED AND UPPER CASE) AND SEPARATED
C     *          BY SPACES.  ARBITRARY COMMENTS ARE ALLOWED FOLLOWING THE # 
C     *          CHARACTER.
C            
C     * NOTES:   THE MAXIMUM NUMBER OF ALLOWABLE TOKENS IS 32
C 
C     * MODIFICATION:
C
C     * Nov 07/2015 - M.Lazare. Coupler code modified for use in AGCM:
C     *                         - no CPP diretives.
C     *
C     * 10/15/98 BY SCOTT TINIS
C     *          FILE CREATED.

      IMPLICIT NONE

      INTEGER      I

      CHARACTER    INPUT_STRING*(*)   !<INPUT STRING FOR PARSING\f$[units]\f$
      CHARACTER*32 TOK(32)            !<ARRAY OF 32 CHAR TOKENS\f$[units]\f$
      INTEGER      NTOK               !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER      ISTART(32)         ! POINTER TO START OF ITH TOKEN
      INTEGER      IEND  (32)         ! POINTER TO END OF ITH TOKEN
      INTEGER      IP                 ! RUNNING POINTER

      INTEGER      LENSTR             ! FUNCTION


      IF (LENSTR( INPUT_STRING ) .EQ. 0) THEN
         ! BLANK LINE; NOTHING TO PARSE
         NTOK = 0
         GO TO 999
      END IF

      IP = 1
      NTOK = 0

      DO I = 1, 32
         ! STRIP OFF THE FIRST 32 TOKENS

         ISTART(I) = 0
         IEND(I) = 0

   10    IF (IP .EQ. LEN( INPUT_STRING )) THEN
            IF (INPUT_STRING(IP:IP) .NE. ' ') THEN
               ! REACHED END OF LAST TOKEN BY DEFAULT
               IF (INPUT_STRING(IP:IP) .EQ. '#') THEN
                  ! COMMENT IS LAST CHARACTER; BACK UP 1 CHAR
                  IEND(I) = IP - 1
               ELSE
                  IEND(I) = IP
               END IF
               NTOK = NTOK + 1
            END IF
            GO TO 100
         END IF

         IF (INPUT_STRING(IP:IP) .EQ. '#') THEN
            ! REACHED THE COMMENT
            IF (ISTART(I) .NE. 0 .AND. IEND(I) .EQ. 0) THEN
               ! CHECK TO SEE IF LAST TOKEN CLOSED OFF BEFORE EXIT
               IEND(I) = IP - 1
               NTOK = NTOK + 1
            END IF
            GO TO 100
         END IF

         IF (INPUT_STRING(IP:IP) .EQ. ' ') THEN
            IF (ISTART(I) .GT. 0) THEN
               ! WE HAVE FOUND THE END OF THIS TOKEN
               IEND(I) = IP - 1
               NTOK = NTOK + 1
               IP = IP + 1
               GO TO 50
            ELSE 
               ! WE ARE STILL LOOKING FOR THE BEGINNING
               IP = IP + 1
               GO TO 10
            END IF
         ELSE 
            IF (ISTART(I) .EQ. 0) THEN
               ! WE HAVE FOUND THE BEGINNING OF THIS TOKEN
               ISTART(I) = IP
            END IF
               ! ELSE WE ARE IN THE MIDDLE OF THE CURRENT TOKEN
            IP = IP + 1 
            GO TO 10
         END IF

   50 END DO


  100 DO I = 1, NTOK
         ! SET THE TOKEN STRINGS
         TOK(I) = INPUT_STRING(ISTART(I):IEND(I))
      END DO


  999 RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
