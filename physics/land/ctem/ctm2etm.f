      subroutine ctm2etm(cyear,cday,csec,
     1                   eyear,emon,eday,ehour,emin,esec)

c     * August 19, 2002  - W.G. Lee
c
c     * ctm2etm converts compressed time (year,day,sec) to
c     * expanded time (year,month,day,hour,minute,second)
c
c     * If input values are non-integer, subroutine aborts.
c
c     * Input data are re-arrange so that:
c     *
c     * day is in the range:  1,2,3 ...   365
c     * sec is in the range:  1,2,3 ... 86400
c     * NOTE: This means that the inputs are modified for 
c     *       the routine that calls ctm2etm!

c     ------------------------------------------------------------------

      implicit none 
  
      integer  m

      real     cyear,  cday,  csec
      real     eyear,  emon,  eday, ehour, emin, esec
      real     resid,  day(12)
     
      data     day  /31.,28.,31.,30.,31.,30.,31.,31.,30.,31.,30.,31./

c     ------------------------------------------------------------------

c     First clean up input data.

      call timeck (cyear, cday, csec, 0) 

c     From input seconds, determine output hours/minutes/seconds.

      ehour = int (csec/3600.0)
      resid = csec - ehour * 3600.0
      emin  = int (resid/60.0)
      esec  = resid - emin * 60.0

c     From input days, determine output month, day

      emon = 1.0
      eday = cday

      do 20 m = 1,11
         if (eday.gt.day(m)) then
            eday = eday - day(m)
            emon = emon + 1.0
         else
            goto 90
         endif
  20  continue

  90  eyear = cyear

      return
      end  
