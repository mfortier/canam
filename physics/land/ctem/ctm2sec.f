      subroutine ctm2sec(cyear,cday,csec,sec)

c     * Sept 27, 2002  - W.G. Lee
c
c     * ctm2etm converts compact time (year,day,sec) to
c     * total number of seconds.                            
c
c     * If input values are non-integer, subroutine aborts.
c
c     ------------------------------------------------------------------

      implicit none 
  
      integer  m

      real     cyear, cday, csec
      real     sec
 
      call timeck(cyear,cday,csec,1)

      sec = csec + (cday-1.0)*86400 + (cyear-1.0)*365.0*86400.0

      return
      end  
