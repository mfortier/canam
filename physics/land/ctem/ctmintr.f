      subroutine ctmintr(datain, ctmin,  ntin,
     1                   datout, ctmout, ntout, imcjmc, noption) 

c     * November   6, 2002     W.G. Lee
c                              Bug fix. DR discovered a problem if there was an interpolation
c                              time that exactly matches the input data. His correction was 
c                              implemented.
c
c     * September 30, 2002     W.G. Lee
c
c     * ctmintr reads input data arrays and their associated 
c     * compact timestamp. It then uses linear interpolation to 
c     * determine an output array, given a set of output compact    
c     * timestamps.
c
c
c     * Assumptions:
c     * 
c     * - There must be at least 2 time levels of input data.
c     * - Input timestamps/data must be order from lowest timestamp to
c     *   largest timestamp. 
c      
c
c     * Output options:
c     *
c     *    noption = 0    linearly interpolate, but if requested 
c     *                   data has a timestamp outside of the range
c     *                   of input data, output the closest values.
c     *    noption = 1    linearly interpolate, but if requested 
c     *                   data has a timestamp outside of the
c     *                   range of input data, abort.

c     ------------------------------------------------------------------

      implicit none 
 
      integer  ij,     nin,   n      
      integer  imcjmc, ntin,  ntout, noption

      real     datain(imcjmc,ntin),  ctmin(3,ntin)
      real     datout(imcjmc,ntout), ctmout(3,ntout)

      real     tin(1000), tout(1000)
      real     deltat,    facta,      factb


      do 10 n = 1,ntin
         call ctm2sec(ctmin(1,n),ctmin(2,n),ctmin(3,n),tin(n))
   10 continue

      do 20 n = 2,ntin
         if (tin(n).le.tin(n-1)) call    xit('ctmintr',-20)
   20 continue

      do 900 n = 1,ntout
 
         call ctm2sec(ctmout(1,n),ctmout(2,n),ctmout(3,n),tout(n))
 
         if (tout(n).lt.tin(1)) then
 
            if (noption.eq.1)  then
               call     xit('ctmintr',-1)
            else
               do 110 ij = 1,imcjmc
                  datout(ij,n) = datain(ij,1)
  110          continue
            endif
 
         else
       
            if (tout(n).gt.tin(ntin)) then
 
               if (noption.eq.1)  then
                  call     xit('ctmintr',-2)
               else
                  do 210 ij = 1,imcjmc
                     datout(ij,n) = datain(ij,ntin)
  210             continue
               endif
      
            else
 
               do 380 nin = 2, ntin
                  if ((tout(n).ge.tin(nin-1)) .and. 
     &                (tout(n).le.tin(nin)))  then
                     deltat =  tin(nin) - tin(nin-1)
                     facta  = (tin(nin) - tout(n))    / deltat
                     factb  = (tout(n)  - tin(nin-1)) / deltat
                     do 310 ij = 1,imcjmc
                        datout(ij,n) = facta * datain(ij,nin-1)
     1                               + factb * datain(ij,nin)     
  310                continue
                     goto 900
                  endif
  380          continue

            endif
         endif

  900 continue 

c     ------------------------------------------------------------------

      return
      end  
