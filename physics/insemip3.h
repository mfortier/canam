#include "cppdef_config.h"

!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

#if ((defined(pla) && defined(pam)) || !defined(pla))
C
C     * JUL 30, 2018 - M.LAZARE.     Remove non-transient cpp if blocks.
C     * Aug 10, 2017 - M.Lazare.     No MASKROW passed in call to XTEMISS10.
C     * Aug 07, 2017 - M.Lazare.     Initial GIT version.
C     * FEB 12, 2015 - K.VONSALZEN/  New version for gcm18:
C     *                M.LAZARE.     - Modified call to new subroutine
C     *                                XTEMISS10.
C     * JUN 03, 2013 - K.VONSALZEN/  PREVIOUS VERSION INSEMIP2 FOR GCM17:
C     *                M.LAZARE.     - CLEAN-UP AND MERGING WITH PLA CODE.
C     *                              - REVISED CALLS TO NEW XTEMISS9,
C     *                                XTEVOLC4,XTEOB4,XTEANT4,XTEAERO4,
C     *                                XTEWF5,XTEOB4,XTESOA3.
C     *                              - ADD DIAGNOSTIC CALCULATION OF "ALTI".
C     *                              - REMOVED "HISTEMI" FIELDS.
C     * APR 28, 2012 - K.VONSALZEN/  PREVIOUS VERSION INSEMIP FOR GCM16.
C     *                Y.PENG/       - REVISED CALLS TO NEW XTEMISS8,
C     *                M.LAZARE.       XTEVOLC3,XTEOB3,XTEANT3,XTEAERO3,
C     *                                XTTEMI2,XTEWF4.
C     * APR 27, 2010 - K.VONSALZEN.  PREVIOUS VERSION INSEMI FOR GCM15I.
C     * APPLY TRACER EMISSIONS.
C---------------------------------------------------------------------
C     * INITIALIZATION.
C
      XEMIS(IL1:IL2,:,:)=0.
C
C---------------------------------------------------------------------
C     * CALCULATE HEIGHT AND PRESSURE.
C
      CALL PAHGT2(PF,PH,ZF,ZH,THROW,TFROW,SHJ,SHTJ,PRESSG,RGAS,
     1            GRAV,ILG,ILEV,LEV,IL1,IL2,ILEVP1)
      DO L=1,ILEV
        DP(IL1:IL2,L)=PRESSG(IL1:IL2)*DSHJ(IL1:IL2,L)
      ENDDO
      DO L=1,ILEV
        ZFS(IL1:IL2,L)=ZF(IL1:IL2,L)+PHISROW(IL1:IL2)/GRAV
      ENDDO
      ZFS(IL1:IL2,ILEV+1)=0.
C
C     * SAVE ALTITUDE.
C
      DO L=2,ILEV
        ALTIROW(IL1:IL2,L)=ALTIROW(IL1:IL2,L) + 
     1                     0.5*(ZFS(IL1:IL2,L)+ZFS(IL1:IL2,L-1))*SAVERAD
      ENDDO
      L=1
      ALTIROW(IL1:IL2,L)=ALTIROW(IL1:IL2,L) + (ZFS(IL1:IL2,L) +
     1                   0.5*(ZFS(IL1:IL2,L)-ZFS(IL1:IL2,L+1)))*SAVERAD
C
C---------------------------------------------------------------------
C     * NATURAL EMISSIONS.
C
      CALL XTEVOLC4(XEMIS,PRESSG,DSHJ,ZF,
     1              ESCVROW,EHCVROW,ESEVROW,EHEVROW,ESVCROW,
     2              ESVEROW,DSUEM2,PSUEF2,ISO2,SAVERAD,ISVCHEM,IPAM,
     3              ILG,IL1,IL2,ILEV,LEV,NTRAC,DTADV)
      CALL XTESOA3(XEMIS,PRESSG,DSHJ,EOSTROW,IPAM,OGPP,
     1             IOCO,IOCY,ILG,IL1,IL2,ILEV,LEV,NTRAC,DTADV)
      CALL XTEMISS10(XEMIS,XROW,ILG,IL1,IL2,ILEV,LEV,NTRAC,DTADV,KOUNT,
     1               JL,LNZ0ROW,FCANROW,ICANP1,THROW(1,2),GTROWBS,
     2               USTARBS,GTROT(1,IOWAT),PRESSG,DSHJ,DMSOROW,EDMSROW,
     3               SHJ,SICNROW,EDSOROW,EDSLROW,ZSPDSO,ZSPDSBS,
     4               FLNDROW,FWATROW,FNROL,
     5               SPOTROW,ST01ROW,
     6               ST02ROW,ST03ROW,ST04ROW,ST06ROW, 
     7               ST13ROW,ST14ROW,ST15ROW,ST16ROW,
     8               ST17ROW,SUZ0ROW,PDSFROW,ISVDUST,
     9               FALLROW,FA10ROW,FA2ROW,FA1ROW,
     A               DUWDROW,DUSTROW,DUTHROW,USMKROW, 
     B               CLAYROW,SANDROW,ORGMROW,BSFRAC,SMFRAC,
     C               ESDROW,IGND,IPAM,SICN_CRT,
     D               ISSA,ISSC,IDUA,IDUC,IDMS,ISO2,SAVERAD,ISVCHEM)
C
C     * ANTHROPOGENIC AND ANTHROPOGENICALLY INFLUENCED EMISSIONS.
C
#if defined emists
#ifndef pla
      CALL XTTEMI2(XEMIS,X2DEMISA,X2DEMISS,X2DEMISK,X2DEMISF,
     1             FBBCROW,FAIRROW,PRESSG,DSHJ,ZF,ZFS,DTADV,LEVWF,
     2             LEVAIR,IL1,IL2,LEV,ILG,ILEV,NTRAC)
#else
      CALL XTTEMIP(XEMIS,DOCEM1,DOCEM2,DOCEM3,DOCEM4,DBCEM1,
     1             DBCEM2,DBCEM3,DBCEM4,DSUEM1,DSUEM2,DSUEM3,
     2             DSUEM4,PSUEF1,PSUEF2,PSUEF3,PSUEF4,
     3             SAIRROW,SSFCROW,SBIOROW,SSHIROW,
     4             SSTKROW,SFIRROW,BAIRROW,BSFCROW,BBIOROW,
     5             BSHIROW,BSTKROW,BFIRROW,OAIRROW,OSFCROW,
     6             OBIOROW,OSHIROW,OSTKROW,OFIRROW,FBBCROW,
     7             FAIRROW,PRESSG,DSHJ,ZF,ZFS,DTADV,LEVWF,
     8             LEVAIR,ISO2,IL1,IL2,LEV,ILG,ILEV,NTRAC)
#endif
#endif
C
C     * ADJUST TRACER MIXING RATIOS TO ACCOUNT FOR EMISSIONS.
C
      XROW(IL1:IL2,2:LEV,:)=XROW(IL1:IL2,2:LEV,:)
     1                                         +XEMIS(IL1:IL2,1:ILEV,:)
C
C     * DIAGNOSE EMISSIONS.
C
#if defined xtrachem
#if defined emists
        EAISROW(IL1:IL2)=EAISROW(IL1:IL2)+SAIRROW(IL1:IL2)*SAVERAD
        ESFSROW(IL1:IL2)=ESFSROW(IL1:IL2)+SSFCROW(IL1:IL2)*SAVERAD
        ESTSROW(IL1:IL2)=ESTSROW(IL1:IL2)+SSTKROW(IL1:IL2)*SAVERAD
        EFISROW(IL1:IL2)=EFISROW(IL1:IL2)+SFIRROW(IL1:IL2)*SAVERAD
        EAIBROW(IL1:IL2)=EAIBROW(IL1:IL2)+BAIRROW(IL1:IL2)*SAVERAD
        ESFBROW(IL1:IL2)=ESFBROW(IL1:IL2)+BSFCROW(IL1:IL2)*SAVERAD
        ESTBROW(IL1:IL2)=ESTBROW(IL1:IL2)+BSTKROW(IL1:IL2)*SAVERAD
        EFIBROW(IL1:IL2)=EFIBROW(IL1:IL2)+BFIRROW(IL1:IL2)*SAVERAD
        EAIOROW(IL1:IL2)=EAIOROW(IL1:IL2)+OAIRROW(IL1:IL2)*SAVERAD
        ESFOROW(IL1:IL2)=ESFOROW(IL1:IL2)+OSFCROW(IL1:IL2)*SAVERAD
        ESTOROW(IL1:IL2)=ESTOROW(IL1:IL2)+OSTKROW(IL1:IL2)*SAVERAD
        EFIOROW(IL1:IL2)=EFIOROW(IL1:IL2)+OFIRROW(IL1:IL2)*SAVERAD
#endif
#endif
#endif

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
