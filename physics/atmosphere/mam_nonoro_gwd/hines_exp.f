!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE HINES_EXP (DATA,DATA_ZMAX,ALT,ALT_EXP,IORDER,
     1                      IL1,IL2,LEV1,LEV2,NLONS,NLEVS)
C
C  This routine exponentially damps a longitude by altitude array 
C  of data above a specified altitude.
C
C  Aug. 13/95 - C. McLandress
C
C  Output arguement:
C  -----------------
C
C     * DATA = modified data array.
C
C  Input arguements:
C  -----------------
C
C     * DATA    = original data array.
C     * ALT     = altitudes.
C     * ALT_EXP = altitude above which exponential decay applied.
C     * IORDER  = 1 means vertical levels are indexed from top down 
C     *           (i.e., highest level indexed 1 and lowest level NLEVS);
C     *           .NE. 1 highest level is index NLEVS.
C     * IL1     = first longitudinal index to use (IL1 >= 1).
C     * IL2     = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * LEV1    = first altitude level to use (LEV1 >=1). 
C     * LEV2    = last altitude level to use (LEV1 < LEV2 <= NLEVS).
C     * NLONS   = number of longitudes.
C     * NLEVS   = number of vertical
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  Input work arrays:
C  ------------------
C
C     * DATA_ZMAX = data values just above altitude ALT_EXP.
C
      INTEGER IORDER            !<Variable description\f$[units]\f$	
      INTEGER IL1               !<Variable description\f$[units]\f$	
      INTEGER IL2               !<Variable description\f$[units]\f$	
      INTEGER LEV1              !<Variable description\f$[units]\f$	
      INTEGER LEV2              !<Variable description\f$[units]\f$	
      INTEGER NLONS             !<Variable description\f$[units]\f$	
      INTEGER NLEVS             !<Variable description\f$[units]\f$	
      REAL    ALT_EXP           !<Variable description\f$[units]\f$	
      REAL    DATA(NLONS,NLEVS) !<Variable description\f$[units]\f$	
      REAL    DATA_ZMAX(NLONS)  !<Variable description\f$[units]\f$	
      REAL    ALT(NLONS,NLEVS)  !<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C  Internal variables.
C  -------------------
C
      INTEGER  LEVBOT, LEVTOP, LINCR, I, L
      REAL    HSCALE
      DATA  HSCALE / 5.E3 /
C-----------------------------------------------------------------------     
C
C  Index of lowest altitude level (bottom of drag calculation).
C
      LEVBOT = LEV2
      LEVTOP = LEV1
      LINCR  = 1
      IF (IORDER.NE.1)  THEN
        LEVBOT = LEV1
        LEVTOP = LEV2
        LINCR  = -1
      END IF
C
C  Data values at first level above ALT_EXP.
C
      DO 20 I = IL1,IL2
        DO 10 L = LEVTOP,LEVBOT,LINCR
          IF (ALT(I,L) .GE. ALT_EXP)  THEN
            DATA_ZMAX(I) = DATA(I,L) 
          END IF   
 10     CONTINUE
 20   CONTINUE
C
C  Exponentially damp field above ALT_EXP to model top at L=1.
C
      DO 40 L = 1,LEV2 
        DO 30 I = IL1,IL2
          IF (ALT(I,L) .GE. ALT_EXP)  THEN
            DATA(I,L) = DATA_ZMAX(I) * EXP( (ALT_EXP-ALT(I,L))/HSCALE )
          END IF
 30     CONTINUE
 40   CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
