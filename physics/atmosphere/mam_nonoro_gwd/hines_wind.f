!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE HINES_WIND (V_ALPHA,VEL_U,VEL_V,UBOT,VBOT,DRAG,
     1                       NAZ,IL1,IL2,NLONS,NAZMTH)
C
C  This routine calculates the azimuthal horizontal background wind components 
C  on a longitude at a single altitude for the case of 4, 8, 12 or 16 equally
C  spaced azimuths needed for the Hines' Doppler spread GWD parameterization 
C  scheme.
C
C  Aug. 7/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Feb. 2/96 - C. McLandress (added: 12 and 16 azimuths; logical flags;
C                             only single level calculation; removed UMIN)  

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C
C  Output arguement:
C  ----------------
C
C     * V_ALPHA = background wind component at each azimuth (m/s). 
C     *           (note: first azimuth is in eastward direction
C     *            and rotate in counterclockwise direction.)
C
C  Input arguements:
C  ----------------
C
C     * VEL_U  = background zonal wind component (m/s).
C     * VEL_V  = background meridional wind component (m/s).
C     * UBOT   = background zonal wind component at bottom level.
C     * VBOT   = background meridional wind component at bottom level.
C     * DRAG   = logical flag indicating longitudes where calculations
C     *          to be performed.
C     * NAZ    = number of horizontal azimuths used (4, 8, 12 or 16).
C     * IL1    = first longitudinal index to use (IL1 >= 1).
C     * IL2    = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * NLONS  = number of longitudes.
C     * NAZMTH = azimuthal array dimension (NAZMTH >= NAZ).
C
C  Constants in DATA statements.
C  ----------------------------
C
C     * COS45 = cosine of 45 degrees. 
C     * COS30 = cosine of 30 degrees. 
C     * SIN30 = sine of 30 degrees. 
C     * COS22 = cosine of 22.5 degrees. 
C     * SIN22 = sine of 22.5 degrees. 
C
C  Subroutine arguements.
C  ---------------------
C
      INTEGER  NAZ              !<Variable description\f$[units]\f$
      INTEGER  IL1              !<Variable description\f$[units]\f$
      INTEGER  IL2              !<Variable description\f$[units]\f$
      INTEGER  NLONS            !<Variable description\f$[units]\f$
      INTEGER  NAZMTH           !<Variable description\f$[units]\f$
      INTEGER  DRAG(NLONS)      !<Variable description\f$[units]\f$
      REAL    V_ALPHA(NLONS,NAZMTH)!<Variable description\f$[units]\f$
      REAL    VEL_U(NLONS)      !<Variable description\f$[units]\f$
      REAL    VEL_V(NLONS)      !<Variable description\f$[units]\f$
      REAL    UBOT(NLONS)       !<Variable description\f$[units]\f$
      REAL    VBOT(NLONS)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C  Internal variables.
C  -------------------
C
      INTEGER  I
      REAL   U, V, COS45, COS30, SIN30, COS22, SIN22
C
      DATA  COS45 / 0.7071068 /
      DATA  COS30 / 0.8660254 /, SIN30 / 0.5       /
      DATA  COS22 / 0.9238795 /, SIN22 / 0.3826834 / 
C-----------------------------------------------------------------------     
C
C  Case with 4 azimuths.
C
      IF (NAZ.EQ.4)  THEN
        DO 20 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            U = VEL_U(I) - UBOT(I)
            V = VEL_V(I) - VBOT(I)
            V_ALPHA(I,1) = U 
            V_ALPHA(I,2) = V
            V_ALPHA(I,3) = - U
            V_ALPHA(I,4) = - V
          END IF
 20     CONTINUE
      END IF
C
C  Case with 8 azimuths.
C
      IF (NAZ.EQ.8)  THEN
        DO 30 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            U = VEL_U(I) - UBOT(I)
            V = VEL_V(I) - VBOT(I)
            V_ALPHA(I,1) = U 
            V_ALPHA(I,2) = COS45 * ( V + U )
            V_ALPHA(I,3) = V
            V_ALPHA(I,4) = COS45 * ( V - U )
            V_ALPHA(I,5) = - U
            V_ALPHA(I,6) = - V_ALPHA(I,2)
            V_ALPHA(I,7) = - V
            V_ALPHA(I,8) = - V_ALPHA(I,4)
          END IF
 30     CONTINUE
      END IF
C
C  Case with 12 azimuths.
C
      IF (NAZ.EQ.12)  THEN
        DO 40 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            U = VEL_U(I) - UBOT(I)
            V = VEL_V(I) - VBOT(I)
            V_ALPHA(I,1)  = U 
            V_ALPHA(I,2)  = COS30 * U + SIN30 * V
            V_ALPHA(I,3)  = SIN30 * U + COS30 * V
            V_ALPHA(I,4)  = V
            V_ALPHA(I,5)  = - SIN30 * U + COS30 * V
            V_ALPHA(I,6)  = - COS30 * U + SIN30 * V
            V_ALPHA(I,7)  = - U
            V_ALPHA(I,8)  = - V_ALPHA(I,2)
            V_ALPHA(I,9)  = - V_ALPHA(I,3)
            V_ALPHA(I,10) = - V
            V_ALPHA(I,11) = - V_ALPHA(I,5)
            V_ALPHA(I,12) = - V_ALPHA(I,6)
          END IF
 40     CONTINUE
      END IF
C
C  Case with 16 azimuths.
C
      IF (NAZ.EQ.16)  THEN
        DO 50 I = IL1,IL2
          IF (DRAG(I).eq.1)  THEN
            U = VEL_U(I) - UBOT(I)
            V = VEL_V(I) - VBOT(I)
            V_ALPHA(I,1)  = U 
            V_ALPHA(I,2)  = COS22 * U + SIN22 * V
            V_ALPHA(I,3)  = COS45 * ( U + V )
            V_ALPHA(I,4)  = COS22 * V + SIN22 * U
            V_ALPHA(I,5)  = V
            V_ALPHA(I,6)  = COS22 * V - SIN22 * U
            V_ALPHA(I,7)  = COS45 * ( V - U )
            V_ALPHA(I,8)  = - COS22 * U + SIN22 * V
            V_ALPHA(I,9)  = - U
            V_ALPHA(I,10) = - V_ALPHA(I,2)
            V_ALPHA(I,11) = - V_ALPHA(I,3)
            V_ALPHA(I,12) = - V_ALPHA(I,4)
            V_ALPHA(I,13) = - V
            V_ALPHA(I,14) = - V_ALPHA(I,6)
            V_ALPHA(I,15) = - V_ALPHA(I,7)
            V_ALPHA(I,16) = - V_ALPHA(I,8)
          END IF
 50     CONTINUE
      END IF
C
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
