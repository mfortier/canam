!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE HINES_SMOOTH (DATA,WORK,DRAGIL,COEFF,NSMOOTH,
     1                         IL1,IL2,LEV1,LEV2,NLONS,NLEVS)
C
C  Smooth a longitude by altitude array in the vertical over a
C  specified number of levels using a three point smoother. 
C
C  NOTE: input array DATA is modified on output!
C
C  Aug. 3/95 - C. McLandress
C
C  Modifications:
C  --------------
C  Feb. 2/96 - C. McLandress (added logical flag)
C

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  Output arguement:
C  ----------------
C
C     * DATA    = smoothed array (on output).
C
C  Input arguements:
C  -----------------
C
C     * DATA    = unsmoothed array of data (on input).
C     * WORK    = work array of same dimension as DATA.
C     * DRAGIL  = logical flag indicating longitudes and levels where 
C     *           calculations to be performed.
C     * COEFF   = smoothing coefficient for a 1:COEFF:1 stencil.
C     *           (e.g., COEFF = 2 will result in a smoother which
C     *           weights the level L gridpoint by two and the two 
C     *           adjecent levels (L+1 and L-1) by one).
C     * NSMOOTH = number of times to smooth in vertical.
C     *           (e.g., NSMOOTH=1 means smoothed only once, 
C     *           NSMOOTH=2 means smoothing repeated twice, etc.)
C     * IL1     = first longitudinal index to use (IL1 >= 1).
C     * IL2     = last longitudinal index to use (IL1 <= IL2 <= NLONS).
C     * LEV1    = first altitude level to use (LEV1 >=1). 
C     * LEV2    = last altitude level to use (LEV1 < LEV2 <= NLEVS).
C     * NLONS   = number of longitudes.
C     * NLEVS   = number of vertical levels.
C
C  Subroutine arguements.
C  ----------------------
C
      INTEGER  NSMOOTH  !<Variable description\f$[units]\f$
      INTEGER  IL1      !<Variable description\f$[units]\f$
      INTEGER  IL2      !<Variable description\f$[units]\f$
      INTEGER  LEV1     !<Variable description\f$[units]\f$
      INTEGER  LEV2     !<Variable description\f$[units]\f$
      INTEGER  NLONS    !<Variable description\f$[units]\f$
      INTEGER NLEVS     !<Variable description\f$[units]\f$
cccc      LOGICAL DRAGIL(NLONS,NLEVS)
      INTEGER DRAGIL(NLONS,NLEVS)!<Variable description\f$[units]\f$
      REAL    COEFF             !<Variable description\f$[units]\f$
      REAL    DATA(NLONS,NLEVS) !<Variable description\f$[units]\f$
      REAL    WORK(NLONS,NLEVS) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
C  Internal variables.
C  -------------------
C
      INTEGER  I, L, NS, LEV1P, LEV2M
      REAL    SUM_WTS
C-----------------------------------------------------------------------     
C
C  Calculate sum of weights.
C
      SUM_WTS = COEFF + 2.
C
      LEV1P = LEV1 + 1
      LEV2M = LEV2 - 1
C
C  Smooth NSMOOTH times
C
      DO 50 NS = 1,NSMOOTH
C
C  Copy data into work array.
C
        DO 20 L = LEV1,LEV2
        DO 20 I = IL1,IL2
          WORK(I,L) = DATA(I,L)
 20     CONTINUE
C
C  Smooth array WORK in vertical direction and put into DATA.
C
        DO 30 L = LEV1P,LEV2M
        DO 30 I = IL1,IL2
          IF (DRAGIL(I,L).eq.1)  THEN
            DATA(I,L) = ( WORK(I,L+1) + COEFF*WORK(I,L) + WORK(I,L-1) ) 
     &                    / SUM_WTS 
          END IF
 30     CONTINUE
C
 50   CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
