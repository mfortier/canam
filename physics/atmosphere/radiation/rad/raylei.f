!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RAYLEI (TAUR, IB, DP, IL1, IL2, ILG, LAY)
C
C     * APR 25,2003 - J.LI.
C----------------------------------------------------------------------C
C     RAYLEIGH SCATTERING FOR BANDS2-BANDS4, NEAR INFRARED REGION      C
C                                                                      C
C     TAUR: RAYLEIGH OPTICAL DEPTH                                     C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   RI(3)
      REAL   TAUR(ILG,LAY)      !<Variable description\f$[units]\f$
      REAL   DP(ILG,LAY)        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
      DATA RI / .16305E-04, .17997E-05, .13586E-06 /
C=======================================================================
      IBM1 = IB - 1
      DO 100 K = 1, LAY
      DO 100 I = IL1, IL2
        TAUR(I,K) =  RI(IBM1) * DP(I,K)
  100 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
