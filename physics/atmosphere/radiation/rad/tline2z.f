!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TLINE2Z(TAUG, COEF1, COEF2, S1, S2, DP, DIP, DT,
     1                   INPT, LEV1, GH, LC, IL1, IL2, ILG, LAY)
C
C     * FEB 09,2009 - J.LI.     NEW VERSION FOR GCM15H:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     * APR 18,2008 - M.LAZARE. PREVIOUS VERSION TLINE2Y FOR GCM15G:
C     *                         - COSMETIC CHANGE OF N->LC PASSED
C     *                           IN AND USED IN DIMENSION OF COEF
C     *                           ARRAY(S), SO THAT IT IS NOT REDEFINED
C     *                           AND PASSED BACK OUT TO CALLING
C     *                           ROUTINE, CHANGING VALUE IN DATA
C     *                           STATEMENT!
C     *                         - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                           FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                           OF "RADFORCE" MODEL OPTION. 
C     *                         - WORK ARRAY "S2" NOW LOCAL INSTEAD OF
C     *                           PASSED-IN WORKSPACE. 
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION TLINE2X FOR GCM15E/F:
C     *                         - IMPLEMENT RPN FIX FOR INPT.
C     * APR 25,2003 - J.LI.     PREVIOUS VERSION TLINE2 FOR GCM15D.
C----------------------------------------------------------------------C
C     THE SAME AS TLINE1, BUT WITH TWO MIXED GASES, ONE MUST BE H2O    C
C                                                                      C
C     TAUG: GASEOUS OPTICAL DEPTH                                      C
C     S1:   INPUT H2O MIXING RATIO FOR EACH LAYER                      C
C     S:    INPUT ANOTHER GAS MIXING RATIO FOR EACH LAYER              C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C     DIP:  INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO NEIGHBORING C
C           STANDARD INPUT DATA PRESSURE LEVELS                        C
C     DT:   LAYER TEMPERATURE - 250 K                                  C
C     INPT: NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES  C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   TAUG(ILG,LAY)      !<Variable description\f$[units]\f$
      REAL   COEF1(5,LC)        !<Variable description\f$[units]\f$
      REAL   COEF2(5,LC)        !<Variable description\f$[units]\f$
      REAL   S1(ILG,LAY)        !<Variable description\f$[units]\f$
      REAL   S2(ILG,LAY)        !<Variable description\f$[units]\f$
      REAL   DP(ILG,LAY)        !<Variable description\f$[units]\f$
      REAL   DIP(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL   DT(ILG,LAY)        !<Variable description\f$[units]\f$
      INTEGER INPT(ILG,LAY)     !<Variable description\f$[units]\f$
C
      LOGICAL GH                !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
C=======================================================================
      IF (GH)                                                       THEN
        LAY1 =  1
      ELSE
        LAY1 =  LEV1
      ENDIF
      LAY2   =  LAY
C
      DO 2000 K = LAY1, LAY2
        IF (INPT(1,K) .LT. 950)                                     THEN
          DO 1000 I = IL1, IL2
            M  =  INPT(I,K)
            N  =  M + 1
            X2        =  COEF1(1,N) + DT(I,K) * (COEF1(2,N) + DT(I,K) * 
     1                  (COEF1(3,N) + DT(I,K) * (COEF1(4,N) +
     2                   DT(I,K) * COEF1(5,N))))
C
            Y2        =  COEF2(1,N) + DT(I,K) * (COEF2(2,N) + DT(I,K) * 
     1                  (COEF2(3,N) + DT(I,K) * (COEF2(4,N) +
     2                   DT(I,K) * COEF2(5,N))))
            IF (M .GT. 0)                                           THEN
              X1      =  COEF1(1,M) + DT(I,K) * (COEF1(2,M) + DT(I,K) *
     1                  (COEF1(3,M) + DT(I,K) * (COEF1(4,M) +
     2                   DT(I,K) * COEF1(5,M))))
C
              Y1      =  COEF2(1,M) + DT(I,K) * (COEF2(2,M) + DT(I,K) * 
     1                  (COEF2(3,M) + DT(I,K) * (COEF2(4,M) +
     2                   DT(I,K) * COEF2(5,M))))
            ELSE
              X1      =  0.0
              Y1      =  0.0
            ENDIF
C
            TAUG(I,K) = ( (X1 + (X2 - X1) * DIP(I,K)) * S1(I,K) +
     1                    (Y1 + (Y2 - Y1) * DIP(I,K)) * S2(I,K) ) * 
     1                     DP(I,K)
 1000     CONTINUE        
        ELSE
          M  =  INPT(1,K) - 1000
          N  =  M + 1
          DO 1500 I = IL1, IL2
            X2        =  COEF1(1,N) + DT(I,K) * (COEF1(2,N) + DT(I,K) *
     1                  (COEF1(3,N) + DT(I,K) * (COEF1(4,N) +
     2                   DT(I,K) * COEF1(5,N))))
C
            Y2        =  COEF2(1,N) + DT(I,K) * (COEF2(2,N) + DT(I,K) *
     1                  (COEF2(3,N) + DT(I,K) * (COEF2(4,N) +
     2                   DT(I,K) * COEF2(5,N))))
            IF (M .GT. 0)                                           THEN
              X1      =  COEF1(1,M) + DT(I,K) * (COEF1(2,M) + DT(I,K) *
     1                  (COEF1(3,M) + DT(I,K) * (COEF1(4,M) +
     2                   DT(I,K) * COEF1(5,M))))
C
              Y1      =  COEF2(1,M) + DT(I,K) * (COEF2(2,M) + DT(I,K) *
     1                  (COEF2(3,M) + DT(I,K) * (COEF2(4,M) +
     2                   DT(I,K) * COEF2(5,M))))
            ELSE
              X1      =  0.0
              Y1      =  0.0
            ENDIF
C
            TAUG(I,K) = ( (X1 + (X2 - X1) * DIP(I,K)) * S1(I,K) +
     1                    (Y1 + (Y2 - Y1) * DIP(I,K)) * S2(I,K) ) *
     1                     DP(I,K)
 1500     CONTINUE
        ENDIF 
 2000 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
