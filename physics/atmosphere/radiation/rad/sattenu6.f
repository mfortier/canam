!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SATTENU6 (ATTEN, IB, IG, RMU, O3, CO2, CH4, O2, DP, 
     1                     DIP, DT, DT0, INPT, GH, IL1, IL2, ILG)
C
C     * JUN 22,2013 - J.COLE.   SET THE RADIATIVE EFFECT OF THE MOON
C     *                         LAYER TO ZERO AT END (ATTEN=1.),
C     *                         SO THAT BALT=BALX (FULL TRANSMISSION).
C     * MAY 01,2012 - M.LAZARE. PREVIOUS VERSION SATTENU5 FOR GCM16:
C     *                         - CALLS ATTENUE5 INSTEAD OF ATTENUE4.
C     * FEB 09,2009 - J.LI.     PREVIOUS VERSION SATTENU4 FOR GCM15H/I:
C     *                         - 3D GHG IMPLEMENTED, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK OR
C     *                           TEMPORARY WORK ARRAYS TO HOLD
C     *                           MIXING RATIOS OF GHG DEPENDING ON
C     *                           A PASSED, SPECIFIED OPTION. 
C     *                         - CALLS ATTENUE4 INSTEAD OF ATTENUE3.
C     * APR 21,2008 - L.SOLHEIM/ PREVIOUS VERSION SATTENU3 FOR GCM15G: 
c     *               M.LAZARE/  - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *               J.LI.        FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                            OF "RADFORCE" MODEL OPTION. 
C     *                          - CALLS ATTENUE3 INSTEAD OF ATTENUE2.
C     *                          - UPDATE O3 AND ADD CH4 EFFECT.
C     * MAY 05,2006 - M.LAZARE. PREVIOUS VERSION SATTENU2 FOR GCM15E/F:
C     *                         - PASS INTEGER VARIABLES "INIT" AND
C     *                           "NIT" INSTEAD OF ACTUAL INTEGER
C     *                           VALUES, TO "ATTENUE" ROUTINES.
C     * ORIGINAL VERSION SATTENU BY JIANGNAN LI.
C----------------------------------------------------------------------C
C     CALCULATION OF SOLAR ATTENUATION ABOVE THE MODEL TOP LEVEL. FOR  C
C     BAND1 ONLY O3 AND O2 ARE CONSIDERED, THE CONTRIBUTION OF OTHER   C
C     GASES IS SMALL. FOR BAND 3 AND 4, CO2 IS CONSIDERED FOR GH       C
C                                                                      C
C     ATTEN: ATTENUATION FACTOR FOR DOWNWARD FLUX FROM TOA TO THE      C
C            MODEL TOP LEVEL                                           C
C     O3:    AVERGED O3 MASS MIXING RATIO ABOVE THE MODEL TOP LEVEL    C
C     CO2:   CO2 MASS MIXING RATIO AT MODEL TOP LAYER                  C
C     O2:    O2 MASS MIXING RATIO AT MODEL TOP LAYER                   C
C     DP:    HERE DP IS ONLY THE PRESSURE DIFFERENCE, DIFFERENT FROM   C
C            THAT DEFINED IN RADDRIV. SO THERE IS A FACTOR 1.02        C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE                        C
C     DT:    LAYER TEMPERATURE - 250 K                                 C
C     DT0:   TEMPERATURE IN MOON LAYER - 250 K                         C
C----------------------------------------------------------------------C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL ATTEN(ILG)   !<Variable description\f$[units]\f$
      REAL RMU(ILG)     !<Variable description\f$[units]\f$
      REAL O3(ILG)      !<Variable description\f$[units]\f$
      REAL CO2(ILG)     !<Variable description\f$[units]\f$
      REAL CH4(ILG)     !<Variable description\f$[units]\f$
      REAL O2(ILG)      !<Variable description\f$[units]\f$
      REAL DP(ILG)      !<Variable description\f$[units]\f$
      REAL DIP(ILG)     !<Variable description\f$[units]\f$
      REAL DT(ILG)      !<Variable description\f$[units]\f$
      REAL DT0(ILG)     !<Variable description\f$[units]\f$
      INTEGER INPT(ILG) !<Variable description\f$[units]\f$
      LOGICAL GH        !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      COMMON /BANDS1/ GWS1(6), CS1O3(3,6), CS1O21
C
      COMMON /BANDS1GH/ GWS1GH(3), CS1O3GH(3,3),     CS1O2GH3
      COMMON /BANDS2GH/ GWS2GH(4), CS2H2OGH(5,28),   CS2O2GH(5,28,3)
      COMMON /BANDS3GH/ GWS3GH(4), CS3H2OGH(5,28,2), CS3CO2GH(5,28,4)
      COMMON /BANDS4GH/ GWS4GH(9), CS4H2OGH(5,28,6), CS4CO2GH(5,28,6),
     1                             CS4CH4GH(5,28)
C
C     * NUMBER OF VERTICAL LEVELS IN ABSORBER PRESSURE-BASED COEFFICIENT
C     * ARRAY.
C
      DATA NTL /28/
C=======================================================================
      IF (IB .EQ. 1)                                                THEN
        IF (GH)                                                     THEN
          IF (IG .EQ. 3)                                            THEN
            DO 100 I = IL1, IL2
              DTO3     =  DT(I) + 23.13
              TAU      =  1.02 * ((CS1O3GH(1,IG) + DTO3 *
     1                   (CS1O3GH(2,IG) + DTO3 * CS1O3GH(3,IG))) *
     2                    O3(I) +  CS1O2GH3 * O2(I)) * DP(I)
              ATTEN(I) =  EXP( - TAU / RMU(I))
  100       CONTINUE
          ELSE
            DO 110 I = IL1, IL2
              DTO3     =  DT(I) + 23.13
              TAU      =  1.02 * (CS1O3GH(1,IG) + DTO3 *
     1                   (CS1O3GH(2,IG) + DTO3 * CS1O3GH(3,IG))) *
     2                    O3(I) * DP(I)
              ATTEN(I) =  EXP( - TAU / RMU(I))
  110       CONTINUE
          ENDIF
        ELSE
C
          IF (IG .EQ. 1)                                            THEN
            DO 120 I = IL1, IL2
              DTO3     =  DT(I) + 23.13
              TAU      =  1.02 * ((CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                    DTO3 * CS1O3(3,IG))) * O3(I) +
     2                    CS1O21 * O2(I)) * DP(I)
              ATTEN(I) =  EXP( - TAU / RMU(I))
  120       CONTINUE
          ELSE
            DO 130 I = IL1, IL2
              DTO3     =  DT(I) + 23.13
              TAU      =  1.02 * (CS1O3(1,IG) + DTO3 * (CS1O3(2,IG) +
     1                    DTO3 * CS1O3(3,IG))) * O3(I) * DP(I)
              ATTEN(I) =  EXP( - TAU / RMU(I))
  130       CONTINUE
          ENDIF
        ENDIF
C
      ELSE IF (IB .EQ. 2)                                           THEN
        IF (IG .EQ. 1)                                              THEN
          DO 200 I = IL1, IL2
            ATTEN(I)   =  1.0
  200     CONTINUE
        ELSE
          IM = IG - 1
          ISL = 1
          CALL ATTENUE5 (ATTEN, CS2O2GH(1,1,IM), O2, DP, DIP, DT,
     1                   DT0, RMU, INPT, NTL, ISL, IL1, IL2, ILG)
        ENDIF
C
      ELSE IF (IB .EQ. 3)                                           THEN
        ISL = 1
        CALL ATTENUE5 (ATTEN, CS3CO2GH(1,1,IG), CO2, DP, DIP, DT, DT0, 
     1                 RMU, INPT, NTL, ISL, IL1, IL2, ILG)
C
      ELSE IF (IB .EQ. 4)                                           THEN
        ISL = 1
        IF (IG .NE. 4 .AND. IG .NE. 6 .AND. IG .NE. 8)              THEN
          IF (IG .LE. 3)  IM =  IG 
          IF (IG .EQ. 5)  IM =  IG - 1
          IF (IG .EQ. 7)  IM =  IG - 2
          IF (IG .EQ. 9)  IM =  IG - 3
          CALL ATTENUE5 (ATTEN, CS4CO2GH(1,1,IM), CO2, DP, DIP, DT, 
     1                   DT0, RMU, INPT, NTL, ISL, IL1, IL2, ILG)
        ELSE IF (IG .EQ. 4)                                         THEN
          CALL ATTENUE5 (ATTEN, CS4CH4GH, CH4, DP, DIP, DT, DT0, RMU,
     1                   INPT, NTL, ISL, IL1, IL2, ILG)
        ELSE
          DO 400 I = IL1, IL2
            ATTEN(I)   =  1.0
  400     CONTINUE
        ENDIF
      ENDIF
C
C     * SET THE RADIATIVE EFFECT OF THE MOON LAYER TO ONE (FULL
C     * TRANSMISSION).
C
      DO I = IL1, IL2
        ATTEN(I) =  1.0
      END DO
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
