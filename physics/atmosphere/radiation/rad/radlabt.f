!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RADLABT(LSW,LLW,LSWT,LLWT,NBS,NBL,IM)

C     * Jan 26/14 - M.Lazare. New routine for gcm18.
C 
C     * DEFINES LEVEL INDEX VALUES FOR SOLAR AND LONGWAVE RADIATION.
C 
      IMPLICIT NONE

      INTEGER NBS       !<Variable description\f$[units]\f$
      INTEGER NBL       !<Variable description\f$[units]\f$
      INTEGER IM        !<Variable description\f$[units]\f$
      INTEGER L,M,MSW,MLW

      INTEGER LSW(NBS)          !<Variable description\f$[units]\f$
      INTEGER LLW(NBL)          !<Variable description\f$[units]\f$
      INTEGER LSWT(IM*NBS)      !<Variable description\f$[units]\f$
      INTEGER LLWT(IM*NBL)      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
C     * NON-TILED USUAL INPUT/OUTPUT.
C
      DO 10 L=1,NBS 
        LSW(L)=L 
  10  CONTINUE
  
      DO 20 L=1,NBL
        LLW(L)=L 
  20  CONTINUE
C
C     * TILED INPUT/OUTPUT.
C
      IF(IM.EQ.1)                                                THEN
        DO 100 L=1,NBS 
          LSWT(L)=L 
  100   CONTINUE
  
        DO 200 L=1,NBL
          LLWT(L)=L 
  200   CONTINUE

      ELSE

        MSW=0
        DO 300 L=1,NBS
        DO 300 M=1,IM
          MSW      = MSW+1
          LSWT(MSW) = 1000*L + M 
  300   CONTINUE
C
        MLW=0   
        DO 400 L=1,NBL
        DO 400 M=1,IM
          MLW      = MLW+1
          LLWT(MLW) = 1000*L + M 
  400   CONTINUE

      ENDIF
  
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
