!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RADCONS4
C
C     * APR 30/2012 - M.LAZARE. NEW VERSION FOR GCM16:
C     *                         SOLAR_C REDUCED FROM 1365 TO 1361 IN
C     *                         LINE WITH NEWER SATTELLITE OBS.
C     * FEB 13/2009 - M.LAZARE. PREVIOUS VERSION RADCONS3 FOR GCM15H/I:
C     *                         REMOVE CFC-13 AND CFC-14 EFFECT SINCE
C     *                         UNCERTAIN.
C     * APR 18/2007 - L.SOLHEIM. PREVIOUS VERSION RADCONS2 FOR GCM15G:
C     *                          - MMR OF TRACE GASES NOW DEFINED
C     *                            IN SEPARATE ROUTINE SET_MMR (ALSO
C     *                            CALLED ELSEWHERE IN CODE). THUS
C     *                            "TRACE" COMMON BLOCK EXPLICIT
C     *                            IN THIS ROUTINE REMOVED (NOW IN
C     *                            SET_MMR).
C     * APR  3/2003 - M.LAZARE/ PREVIOUS VERSION RADCONS FOR GCM15F
C     *               J.LI.     AND EARLIER.
C
C     *                        DEFINES CONSTANTS IN COMMON BLOCKS
C     *                        AT START OF MODEL, USED IN LI RADIATION.
C
C     * ORIGINAL COMMON BLOCK CONTAININ PPM CONSTANTS.
C
      USE PHYS_PARM_DEFS, ONLY : pp_solar_const

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   N2O_PPM
      COMMON /RADCON/ SOLAR_C, CO2_PPM, CH4_PPM, N2O_PPM, F11_PPM,
     1                F12_PPM, F113_PPM, F114_PPM
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C============================================================================
C     * DEFINE PPM VALUES (ACTUALLY, THESE ARE CONCENTRATIONS
C     * EXPRESSED AS A PPM VALUE MULTIPLIED BY 1.E-6).
C
C     * NOTE THAT FOR TIME-VARYING CONSTANTS, ONE CAN USE
C     * THESE AS VALUES AT KOUNT=0, THEN MODIFY IN TIME
C     * AT START OF SECTION 2, BASED ON VALUES OF KOUNT,DELT.
C     * IN THIS CASE, THE MIXING RATIO DERIVATIONS BELOW WOULD
C     * ** ALSO ** HAVE TO BE RELOCATED TO SECTION 2!
C
      SOLAR_C  = pp_solar_const
      CO2_PPM  = 348.0   * 1.E-6                     
      CH4_PPM  = 1.650   * 1.E-6      
      N2O_PPM  = 0.306   * 1.E-6 
      F11_PPM  = 0.18E-3 * 1.E-6  
      F12_PPM  = 0.28E-3 * 1.E-6
c     F113_PPM = 0.05E-3 * 1.E-6
c     F114_PPM = 0.03E-3 * 1.E-6
      F113_PPM = 0.
      F114_PPM = 0.
C
C     * Set corresponding mass mixing ratios in common/TRACE/.
C
      CALL SET_MMR(CO2_PPM, CH4_PPM, N2O_PPM, F11_PPM,F12_PPM,
     1             F113_PPM, F114_PPM)
C-------------------------------------------------------------
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
