!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PREINTP2(INPT, INPTM, DIP, DIP0, P, IL1, IL2, ILG, LAY)
C
C     * MAY 05,2006 - M.LAZARE. NEW VERSION FOR GCM15E:
C     *                         - IMPLEMENT RPN FIX FOR INPT/INPTM AND
C     *                           COSMETIC REORGANIZATION.
C     * ORIGINAL VERSION PREINTP BY JIANGNAN LI.
C----------------------------------------------------------------------C
C     THIS SUBROUTINE DETERMINES THE PRESSURE INTERPRETATION POINTS    C
C                                                                      C
C     INPT:  NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C            (FOR 28 INTERPRETATION LEVELS)                            C
C     INPTM: NUMBER OF THE LEVEL FOR THE STANDARD INPUT DATA PRESSURES C
C            (FOR 18 INTERPRETATION LEVELS BELOW 1 MB)                 C
C     P:     PRESSURE AT MIDDLE OF EACH LAYER                          C
C     DIP:   INTERPRETATION FACTOR FOR PRESSURE BETWEEN TWO            C
C            NEIGHBORING STANDARD INPUT DATA PRESSURE LEVELS           C
C     DIP0:  INTERPRETATION FACTOR FOR PRESSURE ABOVE MODEL TOP LEVEL  C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   DIP(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL   DIP0(ILG)          !<Variable description\f$[units]\f$
      REAL   P(ILG,LAY)         !<Variable description\f$[units]\f$
      REAL   STANDP(28)         
      INTEGER INPT(ILG,LAY)     !<Variable description\f$[units]\f$
      INTEGER INPTM(ILG,LAY)    !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      DATA STANDP / 5.0000E-04, 1.4604E-03, 2.9621E-03, 6.0080E-03,
     1              1.2186E-02, 2.4717E-02, 5.0134E-02, 1.0169E-01,
     2              2.0625E-01, 4.1834E-01,
     3              1.2180, 1.8075, 2.6824, 3.9806, 5.9072, 8.7662,
     4              13.0091, 19.3054, 28.6491, 42.5151, 63.0922,
     5              93.6284, 138.9440, 206.1920, 305.9876, 454.0837,
     6              673.8573, 1000.0000 /
C
      JENDS = 27
      DO 600 K = 1, LAY
        DO 100 I = IL1, IL2
          INPT(I,K)   =  0
  100   CONTINUE
C
        DO 200 J = 1, JENDS
        DO 200 I = IL1, IL2
          IF (P(I,K) .GT. STANDP(J))                                THEN
            INPT(I,K) =  INPT(I,K) + 1
          ENDIF
  200   CONTINUE
C
C----------------------------------------------------------------------C
C     CALCULATE ARRAYS DIP AND DIT REQUIRED LATER FOR GASOPT ROUTINES. C
C     ALSO, SET VALUES OF INPT FOR A GIVEN LEVEL TO BE NEGATIVE IF ALL C
C     LONGITUDE VALUES ARE THE SAME. THIS IS ALSO USED IN THE GASOPT   C
C     ROUTINES TO IMPROVE PERFORMANCE BY ELIMINATING THE UNNECESSARY   C
C     INDIRECT-ADDRESSING IF INPL IS NEGATIVE FOR A GIVEN LEVEL.       C
C     NOTE THAT FOR INPT=0, IT IS ASSUMED THAT LEVELS ARE MORE OR      C
C     LESS HORIZONTAL IN PRESSURE, SO SCALING BY -1 STILL PRESERVES    C
C     THE VALUE OF ZERO AND NO INDIRECT-ADDRESSING IS DONE IN THE      C
C     GASOPT ROUTINES.                                                 C
C----------------------------------------------------------------------C
C
        INPDIF =  0
        INP1   =  INPT(1,K)
        DO 300 I = IL1, IL2
          IF(INPT(I,K) .NE. INP1)  INPDIF = 1
          M  =  INPT(I,K)
          N  =  M + 1
          IF (M .GT. 0)                                             THEN
            DIP(I,K)  = (P(I,K) - STANDP(M)) / (STANDP(N) - STANDP(M))
          ELSE
            DIP(I,K)  =  P(I,K) / STANDP(1)
          ENDIF
  300   CONTINUE
C
        IF(INPDIF .EQ. 0)                                           THEN
          DO 400 I = IL1, IL2
            INPT(I,K) =  INPT(I,K) + 1000
  400     CONTINUE
        ENDIF
C
        DO 500 I = IL1, IL2
          INPTM(I,K)  =  INPT(I,K) - 10
  500   CONTINUE
  600 CONTINUE
C
C----------------------------------------------------------------------C
C     INTERPRETATION FACTOR FOR LATTENU AND SATTENU (ATTENUATION ABOVE C
C     MODEL TOP                                                        C
C----------------------------------------------------------------------C
C
      PM =  P(1,1)  
      DO 700 I = IL1, IL2
        PM          =  MIN (PM, P(I,1))
  700 CONTINUE
C
      IF (PM .LE. 0.0005)                                         THEN
        DO 800 I = IL1, IL2
          DIP0(I)   =  0.0
  800   CONTINUE
      ELSE
        DO 900 I = IL1, IL2
          P0        =  P(I,1) * P(I,1) / P(I,2)
          X         =  SQRT (P0 * P(I,1))
          DIP0(I)   = (X - P(I,1)) / (P0 - P(I,1))
  900   CONTINUE
      ENDIF
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
