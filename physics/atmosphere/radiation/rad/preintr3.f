!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
      SUBROUTINE PREINTR3 (INPR, DIR, Q, CO2, RHC, IL1, IL2, ILG, LAY)
C
C     * FEB 09,2009 - J.LI.     NEW VERSION FOR GCM15H:
C     *                         - CO2 PASSED DIRECTLY, THUS NO NEED
C     *                           FOR "TRACE" COMMON BLOCK.
C     * APR 21,2008 - L.SOLHEIM. PREVIOUS VERSION PRINTR2 FOR GCM15G:
C     *                          - COSMETIC CHANGE TO ADD THREADPRIVATE
C     *                            FOR COMMON BLOCK "TRACE", IN SUPPORT
C     *                            OF "RADFORCE" MODEL OPTION. 
C     * APR 25,2003 - J.LI.  PREVIOUS VERSION PREINTR FOR GCM15E/GCM15F.
C----------------------------------------------------------------------C
C     THIS SUBROUTINE DETERMINES THE INTERPRETION POINTS FOR THE RATIO C
C     OF H2O AND CO2.                                                  C
C                                                                      C
C     INPR:  NUMBER OF THE RATIO LEVEL FOR THE STANDARD 5 RATIOS       C
C     DIR:   INTERPRETATION FACTOR FOR MASS RATIO OF H2O / CO2         C
C            BETWEEN TWO NEIGHBORING STANDARD INPUT RATIOS             C
C     Q:     WATER VAPOR MASS MIXING RATIO                             C
C     CO2:   CO2 MASS MIXING RATIO                                     C
C     RHC:   THE RATIO OF THE H2O MASS MIXING TO CO2 MASS MIXING       C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   DIR(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL     Q(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL   CO2(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL   RHC(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL      STANDR(5)       
      INTEGER INPR(ILG,LAY)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      DATA STANDR / .06,  .24,  1., 4., 16. /
C=======================================================================
      JENDR  =  5
      DO 400 K = 1, LAY
C
        DO 100 I = IL1, IL2
          INPR(I,K)   =  0
          RHC(I,K)    =  Q(I,K) / MAX(CO2(I,K), 1.0E-10)
  100   CONTINUE
C
        DO 200 J = 1, JENDR
        DO 200 I = IL1, IL2
          IF (RHC(I,K) .GT. STANDR(J))                              THEN
            INPR(I,K) =  INPR(I,K) + 1
          ENDIF
  200   CONTINUE
C
        DO 300 I = IL1, IL2
          L   =  INPR(I,K)
          LP1 =  L + 1
          IF (L .GE. 1 .AND. L .LT. 5)                              THEN
            DIR(I,K)  = (RHC(I,K) - STANDR(L)) / 
     1                  (STANDR(LP1) - STANDR(L))
          ELSE
C
C----------------------------------------------------------------------C
C     DIR IS NOT USED WITH VALUES OF {0,5} IN TLINEHC, BUT WE          C
C     INITIALIZE HERE TO AVOID PROBLEMS WITH NaN WHEN USED             C
C     IN MULTITASKING MODE.                                            C
C----------------------------------------------------------------------C
C
            DIR(I,K)  =  0.0
          ENDIF
  300   CONTINUE
C
  400 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
