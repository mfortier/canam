!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RAYLEV2 (TAUR, IG, DP, RMU3, IL1, IL2, ILG, LAY)
C
C     * DEC 05,2007 - J.LI.  NEW VERSION FOR GCM15G:
C     *                      - REVISED DATA FOR RI0,RI2.
C     * APR 25,2003 - J.LI.  PREVIOUS VERSION RAYLEV UP THROUGH GCM15F.
C----------------------------------------------------------------------C
C     RAYLEIGH SCATTERING FOR EACH SUB-BAND IN BANDS1, VISIBLE REGION  C
C     TAUR IS THE OPTICAL DEPTH RAYLEIGH SCATTERING FOR A GIVEN LAYER  C
C     FOR UVC (35700 - 50000 CM^-1), SINCE THE OPTICAL DEPTH OF O3 AND C
C     O2 ARE VERY LARGE, RAYLEIGH SCATTERING EFFECT IS NEGLECTED, IT   C
C     IS SHOWN EVEN FOR 10% O3 AMOUNT OF THE STANDARD ATMO, THE        C
C     RAYLEIGH SCATTERING FOR UVC STILL CAN BE NEGLECTED.              C
C     FOR PAR AND UVA, SINCE THEIR SPECTRAL RANGES ARE VERY WIDE, SMALLC
C     ERRORS COULD OCCUR FOR LARGE ZENITH ANGLE, SLIGHTLY ADJUSTMENT   C
C     IS NEEDED, THIS DOES MEAN THE RAYLEIGH OPTICAL DEPTH IS RELATED  C
C     SOLAR ZENITH ANGLE FOR MULTIPLE SCATTERING PROCESS IN SWTRAN.    C
C                                                                      C
C     TAUR: RAYLEIGH OPTICAL DEPTH                                     C
C     DP:   AIR MASS PATH FOR A MODEL LAYER (EXLAINED IN RADDRIV).     C
C     RMU3:  A FACTOR OF SOLAR ZENITH ANGLE, GIVEN IN RADDRIV          C
C----------------------------------------------------------------------C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL   RI0(6), RI2(3)
      REAL   TAUR(ILG,LAY)      !<Variable description\f$[units]\f$
      REAL   DP(ILG,LAY)        !<Variable description\f$[units]\f$
      REAL    RMU3(ILG)         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      DATA RI0 / .67758E-04, .19425E-03, .52145E-03, .88082E-03,
     1           .10541E-02, .12835E-02 /
      DATA RI2 / .28000E-05, .21300E-04, .36000E-04 /
C=======================================================================
      IF (IG .LE. 3)                                                THEN
        DO 100 K = 1, LAY
        DO 100 I = IL1, IL2
          TAUR(I,K) = (RI0(IG) - RI2(IG) * RMU3(I)) * DP(I,K)
  100   CONTINUE
      ELSE
        DO 200 K = 1, LAY
        DO 200 I = IL1, IL2
          TAUR(I,K) =  RI0(IG) * DP(I,K)
  200   CONTINUE
      ENDIF
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
