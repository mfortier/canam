!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      FUNCTION A18LIN(X,XN,YN,M,N)

C****************************************************************************
C*                                                                          *
C*                            FUNCTION A18LIN                               *
C*                                                                          *
C****************************************************************************
C
C linear interpolation
C
C called by CCO2GR
C calls nothing
C
C input:
C  X - argument for which a value of function should be found
C  XN(N),YN(N) - values of function YN(N) at XN(N) grid. X(N) should be
C                ordered so that X(I-1) < X(I).
C output:
C  A18LIN - value of function for X


      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
      real   XN(N)      !<Variable description\f$[units]\f$
      REAL   YN(N)      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      k=m-1
      do 1 i=m,n
      k=k+1
      if(x-xn(i)) 2,2,1
    1 continue
    2 if(k.eq.1) k=2

c k has been found so that xn(k).le.x.lt.xn(k+1)

      A18LIN=(yn(k)-yn(k-1))/(xn(k)-xn(k-1))*(x-xn(k))+yn(k)
      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
