!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE A18INT(X1,Y1,X2,Y2,N1,N2)
C****************************************************************************
C*                                                                          *
C*                    SUBROUTINE A18INT                                     *
C*                                                                          *
C****************************************************************************
C
C         third order spline interpolation
C input argument and function:  X1(1:N1),Y1(1:N1)
C output argument and function: X2(1:N2)X2(1:N2),Y2(1:N2)
C the necessary conditionts are: X1(I) < X1(I+1), and the same for X2 array.
C
C called by CCO2GR
C calls nothing


      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
      REAL   X1(N1)     !<Variable description\f$[units]\f$
      REAL   X2(N2)     !<Variable description\f$[units]\f$
      REAL   Y1(N1)     !<Variable description\f$[units]\f$
      REAL   Y2(N2)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

     *,A(150),E(150),F(150),H(150)
      H2=X1(1)
      NVS=N1-1
      DO 1 K=1,NVS
      H1=H2
      H2=X1(K+1)
      H(K)=H2-H1
    1 CONTINUE
      A(1)=0.
      A(N1)=0.
      E(N1)=0.
      F(N1)=0.
      H1=H(N1-1)
      F1=Y1(N1-1)
      F2=Y1(N1)
      DO 2 KR=2,NVS
      K=NVS+2-KR
      H2=H1
      H1=H(K-1)
      F3=F2
      F2=F1
      F1=Y1(K-1)
      G=1/(H2*E(K+1)+2.*(H1+H2))
      E(K)=-H1*G
      F(K)=(3.*((F3-F2)/H2-(F2-F1)/H1)-H2*F(K+1))*G
    2 CONTINUE
      G=0.
      DO 3 K=2,NVS
      G=E(K)*G+F(K)
      A(K)=G
    3 CONTINUE
      L=1
      DO 4 I=1,N2
      G=X2(I)
      DO 6 K=L,NVS
      IF(G.GT.X1(K+1))GOTO6
      L=K
      GOTO 5
    6 CONTINUE
      L=NVS
    5 G=G-X1(L)
      H2=H(L)
      F2=Y1(L)
      F1=H2**2
      F3=G**2
      Y2(I)=F2+G/H2*(Y1(L+1)-F2-(A(L+1)*(F1-F3)+
     *               A(L)*(2.*F1-3.*G*H2+F3))/3.)
    4 CONTINUE
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
