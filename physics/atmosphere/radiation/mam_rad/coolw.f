!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE COOLW(CW,CmH2O,T,AMUVF,PVF,KMX,LMX,IL1,IL2)

C****************************************************************************
C*                                                                          *
C*                       SUBROUTINE  COOLW                                  *
C*                                                                          *
C****************************************************************************
C
C to calculate cooling rate due to rotational H2O band (cooling-to-space
C approximation, LTE).
C Method used: Gordiets & Markov (1982), desribed by Fomichev et al. (JATP,
C 1986, vol. 48, No 6, pp 529-544).
C                                          V Fomichev, October, 1999
C Called by MAMRAD
C Calls nothing

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C
C OUTPUT:
C CW - cooling rate in K/sec*(specific heat)

      real   CW(LMX,KMX)        !<Variable description\f$[units]\f$

C INPUT (all input data are specified for a level in question):
C CmH2O - H2O mass mixing ratio
C T - temperature (K)

      real   CmH2O(LMX,KMX)     !<Variable description\f$[units]\f$
      REAL   T(LMX,KMX)         !<Variable description\f$[units]\f$
      REAL   AMUVF(KMX)         !<Variable description\f$[units]\f$
      REAL   PVF(KMX)           !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C----------------------------------------------------------------------
C     * INITIALIZE.
C
      DO 5 K=1,KMX
      DO 5 IL=IL1,IL2
        CW(IL,K)=0.
    5 CONTINUE
C
      do 10 k=1,KMX
      do 20 il = IL1,IL2

      T2 = T(il,k)*T(il,k)

c to define the optical depth:
      ta = 1.096E12*AMuvf(k)*pvf(k)*CmH2O(il,k)/T2

c escape function:
      alta = log(ta)
      if (alta.le.1) then
        FL=1.           ! to avoid dividing by zero... L>1 in this case 
      else
        alta = log(ta*alta)
        f1 = 373.1*sqrt(pvf(k)/(ta*T(il,k)*sqrt(T(il,k))))
        f2 = 1.2/ta*(1.2+.5*alta+1.2/alta)
        FL = f1+f2
        if(FL.gt.1) FL = 1.
      end if

c cooling rate:
      CW(il,k) = 2007.*CmH2O(il,k)*T2*FL

   20 continue
   10 continue

      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
