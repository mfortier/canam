!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PRECL2(pdr,pcd,idr,icd,
     1                  xvf,N)

C*********************************************************************
C*                                                                   *
C*            SUBROUTINE PRECL2                                       *
C*                                                                   *
C*********************************************************************
C
C       calculate the interpolation coefficients which link
C  temperature and cooling rate grids. Second order interpolation is used.
C                  V.Fomichev, May 1996.
C                 - modified for CKD: V. Fomichev, November, 2004.
C                 - modified by M. Lazare for gcm15j, April, 2011.
C
C Called by driver (after THERMDAT2, before any model loops)
C Calls DETINT
C

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

C  input: N     - number of model levels
C         xvf   - model levels (mid-layer, defined in THERMDAT2)

      real xvf(N)       !<Variable description\f$[units]\f$
      real pcd(3,N)     !<Variable description\f$[units]\f$
      real pdr(3,67)    !<Variable description\f$[units]\f$
      integer icd(N)    !<Variable description\f$[units]\f$
      integer idr(67)   !<Variable description\f$[units]\f$
      integer N         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C output: (eventually stored in common block /VFCLPR/):
C    PDR(3,67)- coefficients for interpolation from X(N) to XR(67) grid
C    IDR(67)- the indexes of levels (for X(N)) for second order
C             interpolation from X(N) to XR(67) grid
C    PCD(3,N),ICD(N) - coefficients and indexes of levels for
C                      interpolation from XC(59) to X(N)
C
      COMMON /PIRGRD/ XR(67), XC(59)
C-------------------------------------------------------------
C     to form PDR(3,67),IDR(67) arrays

      DO 3 I=1,67
      IF(XR(I).LE.XVF(1)) THEN
        PDR(1,I) = 1.
        PDR(2,I) = 0.
        PDR(3,I) = 0.
        IDR(I) = 1
       ELSE
         CALL DETINT(XR(I),XVF,N,Z,Z1,Z2,K)
         PDR(1,I)=Z
         PDR(2,I)=Z1
         PDR(3,I)=Z2
         IDR(I)=K
      END IF
    3 CONTINUE

C     to form the PCD(3,N),ICD(N) arrays

      DO 4 I=1,N
      CALL DETINT(XVF(I),XC,59,Z,Z1,Z2,K)
      PCD(1,I)=Z
      PCD(2,I)=Z1
      PCD(3,I)=Z2
      ICD(I)=K
    4 CONTINUE

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
