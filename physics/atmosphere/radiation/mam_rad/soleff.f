!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SOLEFF(HEAT,QOF,S1,XVF,PVF,EFFH,KMX,KMXP,LMX,IL1,IL2)

C***********************************************************************
C*                                                                     *
C*              SUBROUTINE SOLEFF                                      *
C*                                                                     *
C***********************************************************************

C to take into account efficiency of the solar heating in the O3 Hartley band
C by method of Mlynczack&Solomon (JGR, vol 98, p 10517, 1993)
C to evaluate the eficiency, it's supposed that 74% of the total solar
C heating (without the O2 heating) in the mesosphere is due to the "net"
C (i.e. without chemical potential energy) heating in the Hartley band.
C The efficiency is calculated in THERMDAT subroutine and passed through
C common block THERSH (array: effH).
C As well, the O3 heating rate above x=10 is made to be proportional to O3 vmr
C
C                                                 V. Fomichev, November, 1997
C Called by MAMRAD
C Calls nothing


      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   HEAT(LMX,KMX)      !<Variable description\f$[units]\f$
      REAL   S1(LMX,KMXP)       !<Variable description\f$[units]\f$
      REAL   QOF(LMX,KMX)       !<Variable description\f$[units]\f$
      REAL   XVF(KMX)           !<Variable description\f$[units]\f$
      REAL   PVF(KMX)           !<Variable description\f$[units]\f$
      REAL   EFFH(KMX)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


C========================================================================
C     * To make heating rate above x=10 to be proportional to O3
C     * mixing ratio:

      do 1 k=1,KMX
        if(xvf(k).lt.10) then
          ko3 = k
          kko3 = KMX+1-k
        endif
    1 continue

      do 3 k=1,KMX
        if(xvf(k).gt.10) then
          kk = KMX+1-k
          do 2 il = IL1,IL2
            HEAT(il,k) = HEAT(il,ko3)/QOF(il,ko3)*QOF(il,k)*
     1                   (S1(1,kko3+1)-S1(1,kko3))/(S1(1,kk+1)-S1(1,kk))
    2     continue
        endif
    3 continue

c     * to account for the efficiency in O3 Ha band:

      do 20 k=1,KMX
        p = pvf(k)
        if(p.gt.1.) then
          go to 20
        else
          do 10 il = IL1,IL2
            HEAT(il,k) = HEAT(il,k)*effH(k)
   10     continue
        endif
   20 continue

      return
      end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
