!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE MAMRAD_INIT3(xvf, pvf, o2vf, cn2vf, ovf, amuvf, 
     1                        cpvf, gvf, co2vf,
     2                        effh, effsrc, to2, effo2nir,
     3                        x1chem, x2chem, dxchem, wchem, k1chem,
     4                        wwvs2, wwvs3, wco2s, wco2st,
     5                        wwvl3, wch4l, wo3l, k1co2s, k2co2s,
     6                        pdr, pcd, idr, icd,
     7                        sg,ptoit,co2_ppm,ilev)
C
C     * APR 30/2011 - M.LAZARE. NEW VERSION FOR GCM15J, BASED
C     *                         ON MAMRAD_INIT:
C     *                         - MODIFIED TO INTERFACE WITH
C     *                           CK SCHEME AND VICTOR'S MODIFIED
C     *                           MIDDLE ATMOSPHERE CODE.
C 
C     * THIS ROUTINE IS CALLED IN SECTION 0 OF THE MODEL AND 
C     * CALCULATES THE 1-D, INVARIANT DATA FOR RADIATIVE COOLING/
C     * HEATING IN THE MIDDLE ATMOSPHERE.
C
C     * INPUT DATA FOR MAMRAD2 (DETERMINED in THERMDAT2,VMRCO2 AND PRECL2).
C
      IMPLICIT NONE
C
      REAL PTOIT        !<Variable description\f$[units]\f$
      REAL CO2_PPM      !<Variable description\f$[units]\f$
      INTEGER ILEV      !<Variable description\f$[units]\f$
      INTEGER L         !<Variable description\f$[units]\f$
C
c     * output fields:
c
      real  , dimension(3,ilev):: pcd           !<Variable description\f$[units]\f$
      real  , dimension(3,67)  :: pdr           !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: xvf           !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: pvf           !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: o2vf          !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: cn2vf         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: ovf           !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: amuvf         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: cpvf          !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: gvf           !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: co2vf         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: effh          !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: effsrc        !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: to2           !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: effo2nir      !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wwvs2         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wwvs3         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wco2s         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wco2st        !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wwvl3         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wch4l         !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wo3l          !<Variable description\f$[units]\f$
      real  , dimension(ilev)  :: wchem         !<Variable description\f$[units]\f$
      integer,dimension(ilev)  :: icd           !<Variable description\f$[units]\f$
      integer,dimension(67)    :: idr           !<Variable description\f$[units]\f$
      real x1chem       !<Variable description\f$[units]\f$
      real x2chem       !<Variable description\f$[units]\f$
      real dxchem       !<Variable description\f$[units]\f$
      integer k1chem    !<Variable description\f$[units]\f$
      integer k1co2s    !<Variable description\f$[units]\f$
      integer  k2co2s   !<Variable description\f$[units]\f$
C
c     * input fields.
c
      real sg(ilev)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C========================================================================
C     * DETERMINE ATMOSPHERIC COMPOSITION USED TO CALCULATE THE RADIATIVE
C     * HEATING/COOLING IN THE MIDDLE ATMOSPHERE. NOTE THAT 1-D FIELDS
C     * ARE USED BECAUSE AT HIGH ENOUGH ALTITUDES, MODEL SURFACES ARE
C     * ESSENTIALLY CONSTANT-PRESSURE INDEPENDANT OF LOCATION. 
C
      CALL THERMDAT2(xvf, pvf, O2vf, CN2vf, Ovf, AMuvf, 
     1               Cpvf, Gvf, 
     2               effH, effSRC, tO2, effO2nir,
     3               X1chem, X2chem, DXchem, WCHEM, K1CHEM,
     4               WWVS2, WWVS3, WCO2S, WCO2ST,
     5               WWVL3, WCH4L, WO3L, K1CO2S, K2CO2S,
     6               SG, ILEV)
      CALL DPMCO2
      CALL PCO2NIR
      CALL PRECL2(pdr, pcd, idr, icd, xvf, ilev)
      CALL VMRCO2(co2vf,xvf,co2_ppm,ilev)
      CALL CCO2GR(co2_ppm)
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
