!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RADFLAB(LR,LEVRF,PTOIT,LH,ILEV)

C     * MAR 3/2008 - L.SOLHEIM,M.LAZARE.
C
C     * DEFINES LEVEL INDEX VALUES FOR RADIATIVE FORCING DIAGNOSTIC FIELDS.
C
      IMPLICIT NONE
C
      INTEGER LR(LEVRF) !<Variable description\f$[units]\f$
      INTEGER LH(ILEV)  !<Variable description\f$[units]\f$
C
      REAL PTOIT        !<Variable description\f$[units]\f$
      INTEGER LEVRF     !<Variable description\f$[units]\f$
      INTEGER ILEV      !<Variable description\f$[units]\f$
      INTEGER LHTOP,L
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================      
C-----------------------------------------------------------------------
      CALL LVCODE(LHTOP,1e-5*PTOIT,1)
      DO L=1,LEVRF
        if (L.eq.1) then
          !--- TOA values are stored in first level.
          LR(L)=LHTOP
        else if (L.eq.LEVRF) then
          !--- Surface values are stored in last level. 
          LR(L)=1111
        else
          !--- model levels are stored in between.
          LR(L)=LH(L-1)
        endif
      ENDDO
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
