!>\file
!>\brief The purpose of this subroutine is to evaluate the vertical profiles
!! of the mass flux, entrainment, detrainment, and associated updraft
!! and downdraft properties for the Zhang-McFarlane (ZM) deep convection
!! parameterization as implemented in CanAM. The output profiles of
!! mass flux, entrainment, and detrainment are all per unit cloud-base
!! mass flux. 
!!
!! @author Norm McFarlane, John Scinocca
!
      SUBROUTINE CLDPRPC(Q,T,P,Z,S,MU,EU,DU,MD,ED,SD,QD,MC,QU,SU,ZF,QST,   
     1                   HMN,HSAT,ALPHA,SHAT,QL, 
     2                   EUO,EDO,DZO,DUO,MDO,EPS0O,                    
     3                   JB,LEL,JT,MX,J0,JD,WRK,ITRPHS,
     4                   ILEV,ILG,IL1G,IL2G,C0FAC,RD,GRAV,CP,MSG)       
C
C     * FEB 02/2017 - M.LAZARE.    LOOP 378 MODIFIED TO SUPPORT BOUNDS
C     *                            ARRAY CHECKING EXPLICITLY.
C     * FEB 16/2009 - M.LAZARE.    NEW VERSION FOR GCM15H:
C     *                            - ADD ACCURACY RESTRICTION ON EPS0. 
C     * MAR 27/2007 - M.LAZARE.    PREVIOUS VERSION CLDPRPB FOR GCM15G:
C     *                            - WEIGHT INCREASED FROM 0.25 TO 0.75.
C     *                            - SELECTIVE PROMOTION OF VARIABLES
C     *                              IN REAL*8 MODE TO WORK IN 32-BIT.  
C     *                            - C0FAC NOW PASSED IN FROM CONVECTION
C     *                              DRIVER, TO ENSURE CONSISTENT USEAGE
C     *                              IN CLDPRP AND CONTRA.
C     *                            - WEIGHT CHANGED FROM 0.50 TO 0.25
C     *                              FOR TUNING PURPOSES. 
C     * JUN 15/2006 - M.LAZARE.    PREVIOUS VERSION CLDPRPA FOR GCM15F:
C     *                            - NOW USES INTERNAL WORK ARRAYS
C     *                              INSTEAD OF PASSED WORKSPACE.    
C     *                            - "ZERO","ONE","FMAX" DATA CONSTANTS
C     *                              ADDED AND USED IN CALL TO 
C     *                              INTRINSICS. 
C     * MAY 08/2006 - M.LAZARE/    PREVIOUS VERSION CLDPRPA FOR GCM15E:
C     *               K.VONSALZEN. - CALCULATION OF SATURATION VAPOUR
C     *                              PRESSURE MODIFIED TO WORK OVER
C     *                              COMPLETE VERTICAL DOMAIN, BY
C     *                              SPECIFYING LIMIT AS ES->P.
C     *                            - C0 INCREASED FROM 4.E-3 TO 6.E-3
C     *                              FOR REDUCED CLOUD FORCING IN TROPICS.
C     * DEC 12/2005 - M.LAZARE/    PREVIOUS VERSION CLDPRP9 FOR GCM15D:
C     *               K.VONSALZEN. 

      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
C     * I/O FIELDS:
C


      REAL  , DIMENSION(ILG,ILEV) ::Q !< Large-scale mean water vapor mixing ratio at layer mid-levels \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) ::T !< Large-scale meant temperature at layer mid-levels \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::P !< Large-scale pressure at layer mid-levels \f$[hPa]\f$
      REAL  , DIMENSION(ILG,ILEV) ::Z !< Height above the local column surface of the layer mid-levels \f$[m]\f$
      REAL  , DIMENSION(ILG,ILEV) ::S !< Ratio of the large-scale dry static energy to the specific heat at constant pressure \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::MU !< Updraft mass flux per unit base mass flux \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) ::EU !< Updraft entrainment rate per unit base mass flux \f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG,ILEV) ::DU !< Updraft detrainment rate per unit base mass flux \f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG,ILEV) ::MD !< Downdraft mass flux per unit base mass flux \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) ::ED !< Downdraft entrainment rate per unit base mass flux\f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG,ILEV) ::SD !< Ratio of the downdraft dry static energy to the specific heat at constant pressure \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::QD !< Downdraft water vapor mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) ::MC !< Sum of the updraft and downdraft mass flux per unit base mass flux \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) ::QU !< Updraft water vapor mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) ::SU !< Ratio of the updraft dry static energy to the specific heat at constant pressure \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::ZF !< Height of model interface levels above the local surface \f$[m]\f$
      REAL  , DIMENSION(ILG,ILEV) ::QST !< Saturation value of the water vapor mixing ratio corresponding to the large-scale temperature \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) ::HMN !< Large-scale moist static energy \f$[J/kg]\f$
      REAL  , DIMENSION(ILG,ILEV) ::HSAT !< Large-scale moist static energy at saturation \f$[J/kg]\f$
      REAL  , DIMENSION(ILG,ILEV) ::ALPHA !< Work array not currently used in subroutine \f$[units]\f$
      REAL  , DIMENSION(ILG,ILEV) ::SHAT !< S interpolated to model interface levels \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::QL !< Updraft liquid water mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) ::EUO !< Updraft entrainment rate used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG,ILEV) ::EDO !< Downdraft entrainment rate used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG,ILEV) ::DZO !< Layer depths used in scalar chemical tracer convective effects calculations \f$[m]\f$
      REAL  , DIMENSION(ILG,ILEV) ::DUO !< Updraft detrainment rate used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG,ILEV) ::MDO !< Downdraft mass flux used in scalar chemical tracer convective effects calculations \f$[m^{-1}]\f$

      REAL  , DIMENSION(ILG):: EPS0O !< Maximum updraft entrainment rate for the column \f$[m^{-1}]\f$
      REAL  , DIMENSION(ILG):: WRK !< Downdraft weighting function \f${[}\mu/(1+\mu PCP/EVP)]\f$ (note OUT) \f$[units]\f$

      INTEGER,DIMENSION(ILG):: JB !< Index for the base level of the convective region for the column \f$[1]\f$
      INTEGER,DIMENSION(ILG):: LEL !< Level of neutral buoyancy for an undiluted parcel ascending from the base level\f$[1]\f$
      INTEGER,DIMENSION(ILG):: JT !< Top level of deep convection (initially set to LEL and subsequently re-evaluated (note INOUT) \f$[1]\f$
      INTEGER,DIMENSION(ILG):: MX !< Lowest level for initiation of moist convection \f$[1]\f$
      INTEGER,DIMENSION(ILG):: J0 !< Level of minimum saturated moist static energy (note OUT) \f$[1]\f$
      INTEGER,DIMENSION(ILG):: JD !< Level of the base of the updraft detrainment layer (note OUT) \f$[1]\f$

C                                                                             
C     * INTERNAL WORK FIELDS:                                                         
C
C
C     *** THE FOLLOWING MUST BE PROMOTED FOR ACCURACY!!! *********
      REAL*8, DIMENSION(ILG,ILEV) :: IPRM,I1,I2,IHAT,I3,IDAG,I4,F,EPS
      REAL*8, DIMENSION(ILG)      :: HMIN,EXPDIF,EXPNUM,FTEMP,HMAX,ZUEF
      REAL*8  FMAX,ZERO

C     ************************************************************
C
      REAL, DIMENSION(ILG,ILEV) :: GAMMA, DZ,HU,HD,
     1                             QSTHAT,HSTHAT,GAMHAT,CU,QDS
      REAL, DIMENSION(ILG)      :: EPS0,RMUE,ZDEF,FACT,TU,RL,EST,
     2                             TOTPCP,TOTEVP,ALFA,ZFD,ZFB,
     3                             DENOM,HUB,IRESET,JLCL


C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      COMMON /EPS   / A,B,EPS1,EPS2                                          
      COMMON /HTCP  / TFREEZ,T2S,AI,BI,AW,BW,SLP
C
C     * PARAMETERS USED IN NEW SATURATION VAPOUR PRESSURE FORMULATION.
C
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
C     * STATEMENT FUNCTION TO CALCULATE SATURATION VAPOUR PRESSURE
C     * OVER WATER.
C
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
C
      DATA RRL     / 2.501E6/
      DATA ZERO    / 0.     /
      DATA ONE     / 1.     /
      DATA FMAX    / 0.0002 /
C----------------------------------------------------------------------
      ZEPS=1.E-33                      
      EPSLIM=0.001                                   
      DO 10 K=MSG+1,ILEV-1                                          
      DO 10 IL=IL1G,IL2G                                                      
        DZ(IL,K)=ZF(IL,K)-ZF(IL,K+1)                                           
   10 CONTINUE                                                                 
C
      DO 50 K=MSG+1,ILEV                                                      
      DO 50 IL=IL1G,IL2G                                                      
        MU(IL,K)=0.                                                           
        F(IL,K)=0.                                                            
        EPS(IL,K)=0.                                                          
        EU(IL,K)=0.                                                           
        DU(IL,K)=0.                                                           
        QL(IL,K)=0.                                                           
        CU(IL,K)=0.                                                           
        QDS(IL,K)=Q(IL,K)                                                     
        MD(IL,K)=0.                                                           
        ED(IL,K)=0.                                                           
        SD(IL,K)=S(IL,K)                                                      
        QD(IL,K)=Q(IL,K)                                                      
        MC(IL,K)=0.                                                           
        HD(IL,K)=0.
        I1(IL,K)=0.
        I2(IL,K)=0.
        I3(IL,K)=0.
        I4(IL,K)=0.
        IHAT(IL,K)=0.
        IDAG(IL,K)=0.
        IPRM(IL,K)=0.
        QU(IL,K)=Q(IL,K)                                                      
        SU(IL,K)=S(IL,K)                                                      
        ETMP=ESW(T(IL,K))
        ESTREF=P(IL,K)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
        IF ( ETMP.LT.ESTREF ) THEN
          EST(IL)=ETMP
        ELSE
          EST(IL)=ESTREF
        ENDIF
        IF(K.GE.LEL(IL))                  THEN
          QST(IL,K)=EPS1*EST(IL)/(P(IL,K)-EST(IL))                             
        ELSE
          QST(IL,K) = Q(IL,K)
        ENDIF
        GAMMA(IL,K)=QST(IL,K)*(1.+QST(IL,K)/EPS1)*EPS1*RRL                     
     1              /(RD*T(IL,K)**2)*RRL/CP                                    
        HMN (IL,K)=CP*T(IL,K)+GRAV*Z(IL,K)+RRL*Q  (IL,K)                      
        HSAT(IL,K)=CP*T(IL,K)+GRAV*Z(IL,K)+RRL*QST(IL,K)                       
        HU(IL,K)=HMN(IL,K)                                                    
   50 CONTINUE
C                                                                       
      DO 75 IL=IL1G,IL2G                                                      
        HSTHAT(IL,MSG+1)=HSAT(IL,MSG+1)                                       
        QSTHAT(IL,MSG+1)=QST(IL,MSG+1)                                        
        GAMHAT(IL,MSG+1)=GAMMA(IL,MSG+1)                                      
        TOTPCP(IL)  =0.                                                       
        TOTEVP(IL)  =0.                                                       
        DZ(IL,ILEV) =ZF(IL,ILEV)
        RL(IL)=RRL
        JT(IL)=LEL(IL)                                                        
        EPS0(IL)=0.
        ALFA(IL)=0.
        EXPDIF(IL)=0.
        HUB(IL)=HU(IL,ILEV)
        J0(IL)=ILEV
        JD(IL)=ILEV
        JLCL(IL)=0.
        HMIN(IL)=1.E6
        HMAX(IL)=HMN(IL,MX(IL))
        ZFB(IL)=ZF(IL,JB(IL)) 
        IRESET(IL)=0.                             
   75 CONTINUE
C
      DO 100 K=MSG+2,ILEV                                                      
      DO 100 IL=IL1G,IL2G                                                      
       QSTHAT(IL,K)=QST(IL,K)
       GAMHAT(IL,K)=GAMMA(IL,K)
       IF(K.GE.LEL(IL))                                             THEN
        IF(QST(IL,K-1).NE.QST(IL,K))                            THEN          
          QSTHAT(IL,K)=LOG(QST(IL,K-1)/QST(IL,K))*QST(IL,K-1)*QST(IL,K)      
     1                 /(QST(IL,K-1)-QST(IL,K))                                
        ENDIF
C                                                                              
        IF(GAMMA(IL,K-1).NE.GAMMA(IL,K))                        THEN          
          GAMHAT(IL,K)=LOG(GAMMA(IL,K-1)/GAMMA(IL,K))                          
     1            *GAMMA(IL,K-1)*GAMMA(IL,K)/(GAMMA(IL,K-1)-GAMMA(IL,K))       
        ENDIF     
       ENDIF         
       HSTHAT(IL,K)=CP*SHAT(IL,K)+RRL*QSTHAT(IL,K)
  100 CONTINUE
C                                                                             
CCC   **********************************************************              
CCC   FIND THE LEVEL OF MINIMUM HSAT, WHERE DETRAINMENT STARTS.
CCC   INITIALIZE CERTAIN ARRAYS INSIDE THE CLOUD PLUME.                    
CCC   **********************************************************              
C                                                                             
      DO 150 J=MSG+1,ILEV                                                     
      DO 150 IL=IL1G,IL2G                                                     
        IF(HSAT(IL,J).LE.HMIN(IL).AND.(J.GE.JT(IL).AND.J.LE.JB(IL)))THEN      
           HMIN(IL)=HSAT(IL,J)                                                 
           J0(IL)  =J                                                         
        ENDIF
        IF(J.GE.JT(IL) .AND. J.LE.JB(IL))                           THEN      
           F(IL,J)= 0.                                                       
          HU(IL,J)= HMAX(IL)                                                
        ENDIF                                                                 
  150 CONTINUE
C
C     * DEFINE BOUNDS FOR DETRAINMENT LEVEL.
C     * RE-INITIALIZE "HMIN" FOR ENSUING CALCULATIONS.
C                                                   
      DO 175 IL=IL1G,IL2G  
        J0(IL)=MIN(J0(IL),JB(IL)-2)
        J0(IL)=MAX(J0(IL),JT(IL)+2) 
        HMIN(IL)=1.E6                                                         
  175 CONTINUE      
C                                                                             
CCCC  *********************************************************               
CCCC  COMPUTE TAYLOR SERIES FOR APPROXIMATE EPS(Z) BELOW                      
CCCC  *********************************************************               
C                                                                             
      DO 275 J=ILEV-1,MSG+1,-1                                                
      DO 275 IL=IL1G,IL2G                                                     
        IF(J.LT.JB(IL) .AND. J.GE.JT(IL))                           THEN      
          I1  (IL,J) = I1(IL,J+1)+(HMAX(IL)-HMN(IL,J))*DZ(IL,J)             
          IHAT(IL,J) = 0.5*(I1(IL,J+1)+I1(IL,J))                              
          I2  (IL,J) = I2(IL,J+1)+IHAT(IL,J)*DZ(IL,J)                         
          IDAG(IL,J) = 0.5*(I2(IL,J+1)+I2(IL,J))                              
          I3  (IL,J) = I3(IL,J+1)+IDAG(IL,J)*DZ(IL,J)                         
          IPRM(IL,J) = 0.5*(I3(IL,J+1)+I3(IL,J))                              
          I4  (IL,J) = I4(IL,J+1)+IPRM(IL,J)*DZ(IL,J)                         
        ENDIF                                                                 
  275 CONTINUE       
C
      DO 350 J=MSG+1,ILEV                                                     
      DO 350 IL=IL1G,IL2G                                                     
        IF( (J.GE.J0(IL) .AND. J.LE.JB(IL)) .AND.                             
     1      HMN(IL,J).LE.HMIN(IL)                )                  THEN      
          HMIN(IL)=HMN(IL,J)
          EXPDIF(IL)=HMAX(IL)-HMIN(IL)                                      
        ENDIF                                                                 
  350 CONTINUE       
C                                                                             
CCCC  *********************************************************               
CCCC  COMPUTE APPROXIMATE EPS(Z) USING ABOVE TAYLOR SERIES                    
CCCC  *********************************************************               
C                    
      DO 375 J=MSG+2,ILEV                                                     
      DO 375 IL=IL1G,IL2G
        IF(J.LT.JT(IL) .OR. J.GT.JB(IL))                            THEN      
          I1(IL,J)=0.                                                         
          EXPNUM(IL)=0.                                                       
        ELSE                                                                  
          EXPNUM(IL)=HMAX(IL)- ( HSAT(IL,J-1)*(ZF(IL,J)-Z(IL,J))            
     1          +HSAT(IL,J)*(Z(IL,J-1)-ZF(IL,J)) )/(Z(IL,J-1)-Z(IL,J))        
        ENDIF                                                                 
        IF( (EXPDIF(IL).GT.100. .AND. EXPNUM(IL).GT.0.) .AND.                 
     1   I1(IL,J).GT.EXPNUM(IL)*DZ(IL,J))                        THEN         
          FTEMP(IL)=EXPNUM(IL)/I1(IL,J)                                       
          F(IL,J)=FTEMP(IL)                                                   
     1            +I2(IL,J)/I1(IL,J)*FTEMP(IL)**2                             
     2            +(2.*I2(IL,J)**2-I1(IL,J)*I3(IL,J))/I1(IL,J)**2             
     3            *FTEMP(IL)**3                                               
     4            +(-5.*I1(IL,J)*I2(IL,J)*I3(IL,J)+5.*I2(IL,J)**3             
     5            +I1(IL,J)**2*I4(IL,J))/I1(IL,J)**3*FTEMP(IL)**4             
          F(IL,J)=MAX(F(IL,J),ZERO)                                             
          F(IL,J)=MIN(F(IL,J),FMAX)                  
        ENDIF                                                                 
  375 CONTINUE      
C
        DO 378 IL=IL1G,IL2G                                                   
          IF(F(IL,J0(IL)).LT.1.E-6 .AND. J0(IL).LT.JB(IL) .AND.               
     1       F(IL,MIN(J0(IL)+1,ILEV)) .GT. F(IL,J0(IL)) )          THEN
             J0(IL)=J0(IL)+1         
          ENDIF
  378   CONTINUE     
C
       DO 380 J=MSG+2,ILEV                                                    
       DO 380 IL=IL1G,IL2G                                                    
         IF(J.GE.JT(IL) .AND. J.LE.J0(IL))                         THEN       
            F(IL,J)=MAX(F(IL,J),F(IL,J-1))                                    
         ENDIF                                                                
  380 CONTINUE       
C                                                                           
      DO 425 J=ILEV,MSG+1,-1                                                  
      DO 425 IL=IL1G,IL2G
        IF(J.GE.JT(IL) .AND. J.LT.J0(IL))                          THEN
          EPS(IL,J)=F(IL,J)               
        ELSE IF(J.GE.J0(IL) .AND. J.LE.JB(IL))                     THEN      
          EPS(IL,J)=F(IL,J0(IL))                                             
        ENDIF
  425 CONTINUE                                                                
C                                                                             
CCC   ****************************************************************        
CCC   SPECIFY THE UPDRAFT MASS FLUX MU, ENTRAINMENT EU, DETRAINMENT DU        
CCC   AND MOIST STATIC ENERGY HU.                                             
CCC   HERE AND BELOW MU, EU,DU, MD AND ED ARE ALL NORMALIZED BY MB            
CCC   ****************************************************************        
C                                                                             
      DO 475 IL=IL1G,IL2G
        EPS0(IL)=F(IL,J0(IL))    
        IF(EPS0(IL).LT.1.E-6) EPS0(IL)=0.                                
        IF(EPS0(IL).GT.0.)                                          THEN  
          MU(IL,JB(IL))=1.
          HUB(IL)      =HU(IL,JB(IL))
          EU(IL,JB(IL))=EPS0(IL)/2.                                        
        ENDIF                                                              
  475 CONTINUE
C                                                                          
      DO 480 J=ILEV-1,MSG+1,-1
      DO 480 IL=IL1G,IL2G                                                      
        IF( EPS0(IL).GT.0. .AND. (J.GE.JT(IL).AND.J.LT.JB(IL)) )    THEN       
          ZUEF(IL)=ZF(IL,J)-ZFB(IL)                                         
          RMUE(IL)=(1./EPS0(IL))*(EXP(EPS(IL,J+1)*ZUEF(IL))-1.)/ZUEF(IL)       
          MU(IL,J)=(1./EPS0(IL))*(EXP(EPS(IL,J)*ZUEF(IL))-1.)/ZUEF(IL)         
          EU(IL,J)=(RMUE(IL)-MU(IL,J+1))/DZ(IL,J)                              
          DU(IL,J)=(RMUE(IL)-MU(IL,J))/DZ(IL,J)                                
        ENDIF                                                                  
  480 CONTINUE       
C                                    
      DO 500 J=ILEV-1,MSG+1,-1                                             
      DO 500 IL=IL1G,IL2G                                                     
        IF( (J.GE.LEL(IL) .AND. J.LT.JB(IL)) .AND.
     1    IRESET(IL).EQ.0. .AND. EPS0(IL).GT.0.)                    THEN       
          IF(MU(IL,J).LT.0.01 .OR. HU(IL,J+1).GT.HUB(IL))        THEN          
            HU(IL,J)=HUB(IL)                                                
            MU(IL,J)=0.             
            JT(IL)=J+1
            IRESET(IL)=1.                                               
C           PRINT *,'JT DUE TO TOO MUCH DETRAINMENT  ===',JT(IL),          
C    1              'IL= ',IL        
          ELSE                                                                
            HU(IL,J)=MU(IL,J+1)/MU(IL,J)*HU(IL,J+1)+DZ(IL,J)/MU(IL,J)*        
     1               (EU(IL,J)*HMN(IL,J)-DU(IL,J)*HSAT(IL,J))
            IF (HU(IL,J).LE.HSTHAT(IL,J)   .AND.                               
     1          HU(IL,J+1).GT.HSTHAT(IL,J+1))         THEN
              JT(IL)=J
              IRESET(IL)=2.                                                    
C             PRINT *,'JT DUE TO OVERSHOOTING ===',JT(IL),' IL= ',IL          
            ENDIF
          ENDIF
        ENDIF                                                                 
  500 CONTINUE       
C
      DO 600 J=ILEV-1,MSG+1,-1
      DO 600 IL=IL1G,IL2G                                                     
        IF( J.GE.LEL(IL) .AND. J.LE.JT(IL)                                    
     1                     .AND. EPS0(IL).GT.0. )                   THEN      
          MU(IL,J)=0.                                                         
          EU(IL,J)=0.                                                         
          DU(IL,J)=0.                                                         
          HU(IL,J)=HUB(IL)                                              
        ENDIF                                                                 
        IF(J.EQ.JT(IL) .AND. EPS0(IL).GT.0.)                        THEN      
          DU(IL,J)=MU(IL,J+1)/DZ(IL,J)                                        
        ENDIF                                                                 
  600 CONTINUE       
C                                                                              
CCC   ******************************************************************      
CCC   SPECIFY DOWNDRAFT PROPERTIES (NO DOWNDRAFTS IF JD.GE.JB).               
C     SCALE DOWN DOWNWARD MASS FLUX PROFILE SO THAT NET FLUX                  
C     (UP-DOWN) AT CLOUD BASE IN NOT NEGATIVE.                                 
CCC   ************************************************************            
C                    
      DO 625 IL=IL1G,IL2G
        IF(EPS0(IL).GT.0.)                                          THEN
          JD(IL)=MAX(J0(IL),JT(IL)+1)
          ZFD(IL)=ZF(IL,JD(IL)) 
          HD(IL,JD(IL))=HMN(IL,JD(IL)-1)
          FACT(IL)=2.*EPS0(IL)*(ZFD(IL)-ZFB(IL))  
          IF(FACT(IL).GT.0.)                                      THEN      
            ALFA(IL)=FACT(IL)/(EXP(FACT(IL))-1.)            
          ELSE
            ALFA(IL)=0.
          ENDIF                                                            
        ENDIF
  625 CONTINUE       
C
      DO 630 J=MSG+1,ILEV                                           
      DO 630 IL=IL1G,IL2G
        IF(EPS0(IL).GT.0.)                                          THEN   
          ZDEF(IL)=ZFD(IL)-ZF(IL,J)
          IF((J.EQ.JD(IL)) .AND. FACT(IL).GT.0.)                  THEN
            MD(IL,J)=-1.
          ENDIF
          IF((J.EQ.JB(IL)) .AND. FACT(IL).GT.0.)                  THEN
            MD(IL,J)=-1.
          ENDIF
          IF((J.LT.JB(IL)) .AND. ZDEF(IL).GT.0.)                  THEN
            MD(IL,J)=-1.
          ENDIF
        ENDIF
  630 CONTINUE

C
      DO 650 J=MSG+2,ILEV    
      DO 650 IL=IL1G,IL2G                                                     
        IF((J.GT.JD(IL) .AND. J.LE.JB(IL)) .AND. EPS0(IL).GT.0.)    THEN      
          ED(IL,J-1)=(MD(IL,J-1)-MD(IL,J))/DZ(IL,J-1)                         
          HD(IL,J)=MD(IL,J-1)/MD(IL,J)*HD(IL,J-1)                             
     1             -DZ(IL,J-1)/MD(IL,J)*ED(IL,J-1)*HMN(IL,J-1)                
        ENDIF                                                                 
  650 CONTINUE       
C                                                                             
CCC   *********************************************************               
CCC   CALCULATE UPDRAFT AND DOWNDRAFT EFFECTS ON Q AND S.                     
CCC   *********************************************************               
C                    
      DO 670 K=MSG+1,ILEV             
      DO 670 IL=IL1G,IL2G                                                     
        IF((K.GE.JD(IL) .AND. K.LE.JB(IL)) .AND. EPS0(IL).GT.0.               
     1                                     .AND. JD(IL).LT.JB(IL))  THEN    
C         SD(IL,K) = SHAT(IL,K)                                               
C    1             +              (HD(IL,K)-HSTHAT(IL,K))/                    
C    2               (CP    *(1.+GAMHAT(IL,K)))                               
          QDS(IL,K) = QSTHAT(IL,K)                                            
     1             + GAMHAT(IL,K)*(HD(IL,K)-HSTHAT(IL,K))/                    
     2               (RRL*(1.+GAMHAT(IL,K)))                                   
        ENDIF                                                                 
  670 CONTINUE
C
      DO 690 J=ILEV-1,MSG+2,-1
       DO 680 IL=IL1G,IL2G
        IF(EPS0(IL).GT.0. .AND. JLCL(IL).EQ.0.)                     THEN       
          IF(J.GT.JT(IL) .AND. J.LT.JB(IL))     THEN          
            SU(IL,J)=MU(IL,J+1)/MU(IL,J)*SU(IL,J+1)                           
     1              +DZ(IL,J)/MU(IL,J)*(EU(IL,J)-DU(IL,J))*S(IL,J)            
            QU(IL,J)=MU(IL,J+1)/MU(IL,J)*QU(IL,J+1)                           
     1        +DZ(IL,J)/MU(IL,J)*(EU(IL,J)*Q(IL,J)-DU(IL,J)*QST(IL,J))
            TU(IL)=SU(IL,J)-GRAV/CP*ZF(IL,J)                                   
            ETMP=ESW(TU(IL))
            ESTREF=P(IL,J)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
            IF ( ETMP.LT.ESTREF ) THEN
              ESTU=ETMP
            ELSE
              ESTU=ESTREF
            ENDIF
            QSTU=EPS1*ESTU/((P(IL,J)+P(IL,J-1))/2.-ESTU)                      
            IF(QU(IL,J) .GE. QSTU)                     THEN            
              JLCL(IL)=REAL(J)                                                 
            ENDIF
          ENDIF
        ENDIF                                                            
 680   CONTINUE                                                                
 690  CONTINUE      
C
      DO 700 J=MSG+1,ILEV      
      DO 700 IL=IL1G,IL2G
        IF(EPS0(IL).GT.0.)                                          THEN      
          IF(J.EQ.JB(IL))                                     THEN        
            QU(IL,J)=Q(IL,MX(IL))                                             
            SU(IL,J)=(HU(IL,J)-RRL*QU(IL,J))/CP                               
          ENDIF                                                              
          IF(J.GT.JT(IL) .AND. J.LE.NINT(JLCL(IL)))           THEN          
            SU(IL,J) = SHAT(IL,J)                                             
     1               +              (HU(IL,J)-HSTHAT(IL,J))/                   
     2               (CP    *(1.+GAMHAT(IL,J)))                               
            QU(IL,J) = QSTHAT(IL,J)                                            
     1               + GAMHAT(IL,J)*(HU(IL,J)-HSTHAT(IL,J))/                   
     2               (RRL*(1.+GAMHAT(IL,J)))
          ENDIF
        ENDIF                                                                 
  700 CONTINUE       
C
      DO 750 J=ILEV-1,MSG+1,-1                                    
      DO 750 IL=IL1G,IL2G                                                     
        IF(EPS0(IL) .GT. 0.)                                      THEN        
          IF(J.GT.JT(IL) .AND. J.LT.JB(IL))       THEN        
            CU(IL,J)=((MU(IL,J)*SU(IL,J)-MU(IL,J+1)*SU(IL,J+1))/DZ(IL,J)
     1              -(EU(IL,J)-DU(IL,J))*S(IL,J))/(RRL/CP)
          ENDIF
        ENDIF  
  750 CONTINUE       
C                                                                              
      BETA=1.                                                                
      DO 800 J=ILEV,MSG+2,-1                                                 
      DO 800 IL=IL1G,IL2G
        IF( J.GE.JT(IL) .AND. J.LT.JB(IL) .AND. EPS0(IL).GT.0.               
     1                  .AND. MU(IL,J).GE.0.0 )                     THEN     
          FAC=MAX(T(IL,JB(IL))-T(IL,J),0.)/
     1        MAX(T(IL,JB(IL))-TFREEZ, ONE)
          C0=C0FAC*MIN(FAC**2,ONE)
          DENOM(IL)=(MU(IL,J)+DZ(IL,J)*BETA*DU(IL,J)                         
     1          +DZ(IL,J)*C0*(BETA*MU(IL,J)+(1.-BETA)*MU(IL,J+1))*BETA)      
          IF(DENOM(IL).GT.0.)                                 THEN          
             QL(IL,J)=1./DENOM(IL)                                            
     1        *( MU(IL,J+1)*QL(IL,J+1)-DZ(IL,J)*DU(IL,J)*(1.-BETA)            
     2        *QL(IL,J+1)+DZ(IL,J)*CU(IL,J)-DZ(IL,J)*C0*(BETA*MU(IL,J)        
     3        +(1.-BETA)*MU(IL,J+1))*(1.-BETA)*QL(IL,J+1))                    
          ELSE                                                                
             QL(IL,J)=0.                                                      
          ENDIF                                                               
          TOTPCP(IL)=TOTPCP(IL)+DZ(IL,J)*(CU(IL,J)                            
     1               -DU(IL,J)*(BETA*QL(IL,J)+(1.-BETA)*QL(IL,J+1)))          
        ENDIF                                                                 
  800 CONTINUE       
C                    
      DDRAT=0.
      DO 850 J=MSG+1,ILEV
      DO 850 IL=IL1G,IL2G
        IF(EPS0(IL).GT.0. .AND. J.EQ.JD(IL))                        THEN
          QD(IL,J)=QDS(IL,J) - DDRAT*(QDS(IL,J) -Q(IL,J-1))
          SD(IL,J)=(HD(IL,J)-RRL*QD(IL,J) )/CP
        ENDIF
  850 CONTINUE       
C
      DO 900 J=MSG+1,ILEV-1                                                  
      DO 900 IL=IL1G,IL2G                                                      
        IF((J.GE.JD(IL) .AND. J.LT.JB(IL)) .AND. EPS0(IL).GT.0.               
     1                                     .AND. JD(IL).LT.JB(IL))  THEN      
          QD(IL,J+1)=QDS(IL,J+1) - DDRAT*(QDS(IL,JD(IL))-QD(IL,JD(IL)))
          SD(IL,J+1)=(HD(IL,J+1)-RRL*QD(IL,J+1))/CP                            
          TOTEVP(IL)=TOTEVP(IL)-DZ(IL,J)*ED(IL,J)*Q(IL,J)                     
        ENDIF                                                                
  900 CONTINUE       
C
C     * SAVE CERTAIN FIELDS TO BE USED IN SUBROUTINE CONTRA3 FOR
C     * SULFUR CYCLE, RATHER THAN HAVING TO RECOMPUTE THEM.
C
      DO 923 J=MSG+1,ILEV                                                 
      DO 923 IL=IL1G,IL2G                                                 
         DZO(IL,J) = DZ(IL,J)                                             
         EUO(IL,J) = EU(IL,J)                
         EDO(IL,J) = ED(IL,J)                
         DUO(IL,J) = DU(IL,J)
         MDO(IL,J) = MD(IL,J)
  923 CONTINUE                                                            
C
      DO 925 IL=IL1G,IL2G     
        EPS0O(IL) = EPS0(IL)                                               
        IF(EPS0(IL).GT.0.)                                          THEN
          TOTEVP(IL)=TOTEVP(IL)+MD(IL,JD(IL))*Q(IL,JD(IL)-1)                
     1                         -MD(IL,JB(IL))*QD(IL,JB(IL))                 
        ELSE
C
C         * RESET INDICES TO ILEV TO THAT MB WILL BE ZERO FOR REST OF CODE.
C       
          J0(IL)=ILEV
          JD(IL)=ILEV
          JT(IL)=ILEV
        ENDIF
  925 CONTINUE       
C
      WEIGHT=0.75   
      DO 950 J=MSG+2,ILEV
      DO 950 IL=IL1G,IL2G
        IF(EPS0(IL).GT.0. .AND. TOTEVP(IL).GT.0.)  THEN                   
          RATIO = TOTPCP(IL)/TOTEVP(IL)  
          MD(IL,J)=MD(IL,J)*WEIGHT*RATIO/(1.+WEIGHT*RATIO)                   
          ED(IL,J)=ED(IL,J)*WEIGHT*RATIO/(1.+WEIGHT*RATIO)
        ELSE
          MD(IL,J) = 0.
          ED(IL,J) = 0.                    
        ENDIF            
  950 CONTINUE
C
C     * SAVE FRACTION OF RAIN WHICH EVAPORATES IN THE DOWNDRAFTS.
C
      DO 960 IL=IL1G,IL2G                                               
         WRK(IL)=0.
         IF (EPS0(IL).GT.0. .AND. TOTEVP(IL).NE.0.) THEN
            WRK(IL)=MIN(ONE, WEIGHT*TOTEVP(IL)                    
     1                   /(TOTEVP(IL)+WEIGHT*TOTPCP(IL)))
         ENDIF
 960  CONTINUE
C
      DO 975 J=MSG+1,ILEV                                                     
      DO 975 IL=IL1G,IL2G                                                     
        IF(EPS0(IL).GT.0. .AND. (J.GE.JT(IL) .AND. J.LE.JB(IL)))    THEN
          MC(IL,J)=MU(IL,J)+MD(IL,J)                                          
        ENDIF                                                                 
  975 CONTINUE       
C
      RETURN                                                            
      END       

!> \file
!! \section sec_theory Theoretical basis
!! \subsection ssec_updrafts Updrafts
!! This routine is based on the deep convection parameterization formulation
!! developed by \cite Zhang1995, henceforth referred to as ZM. The formulation is based on a bulk updraft
!! profile that is comprised of an ensemble of deep convective entraining
!! plumes, all of which have the same cloud-base mass flux. Individual
!! plumes within the ensemble have fixed unique fractional entrainment
!! rates. For a given representative sub-ensemble plume the entrainment
!! rate is determined by specifying the height above cloud-base where
!! the plume buoyancy becomes negative while being positive between this
!! level and the level of of free convection (LFC) for the plume. This
!! level of neutral buoyancy (LNB) is approximated as the level at which
!! the plume temperature is equal to that of the large-scale environment,
!! assumed to be identical to the mean state defined by the model prognostic
!! variables. The detrainment rate of the representative plume is assumed
!! to be zero below the LNB but all of the plume mass is detrained at
!! the LNB.
!!\n 
!!\n 
!! The bulk mass flux profile consistent with this conceptual framework
!! is given by
!!\n 
!!\n
!! \f{equation}{
!! \frac{M_{u}(\lambda_{D}(z),z)}{M_{b}}=\frac{1}{\lambda_{0}(z-z_{b})}\left[exp\lambda_{D}(z)(z-z_{b})-1\right]\tag{1}
!! \f}
!! where \f$z_{b}\f$ is the specified height above the surface of the base
!! of the plume ensemble, \f$\lambda_{D}(z)\f$ is the fractional entrainment
!! profile and \f$M_{b}\f$ is the (independently evaluated) updraft mass
!! flux at the base level. The base updraft mass flux ( \f$M_{b}\f$ ) is
!! evaluated through the closure formulation that is implemented in the
!! subroutine closur10.f.
!!\n
!!\n
!! The fractional entrainment profile function is determined from the
!! following equations
!!\n
!! \f{equation}{
!! h_{b}-\overline{h}_{*}(z)=\lambda_{D}(z)\int_{z_{b}}^{z}[h_{b}-\bar{h}(z')]exp[\lambda_{D}(z)(z'-z))]dz'\tag{2}
!! \f}
!! This follows from application of the following entraining plume moist
!! static energy equation:
!!\n
!! \f{equation}{
!! \frac{\partial h_{p}}{\partial z'}=-\lambda_{D}(z)\left[h_{p}(z')-\bar{h}(z')\right]dz';z_{b}\leq z'\leq z\tag{3}
!! \f}
!! where the left-hand side of equation (2) is the difference between
!! the boundary values of the plume moist static energy at the base and
!! detrainment levels.
!!\n
!!\n 
!! The general definition of the moist static energy for a given temperature
!! (\f$T\f$) and water vapor mixing ratio (\f$r\f$) is \f$h=c_{p}T+L_{v}r+gz\f$
!! where \f$L_{v}\f$ is the latent heat of vaporization and \f$g\f$ is the
!! acceleration due to gravity. Here \f$h_{b}=\bar{h}(z_{b})\f$ where \f$\bar{h}\f$
!! is the large-scale environmental value of the moist static energy,
!! with a corresponding saturation value given by \f$\bar{h}_{*}=c_{p}\bar{T}+L_{v}r_{*}(\bar{T},p)+gz\f$
!! where \f$r_{*}\f$ is the saturation value at the given temperature and
!! pressure. Since it is within the cloudy updraft, the plume is assumed
!! to be saturated with respect to water vapor at the detrainment level.
!!\n
!!\n 
!! Equation (2) is implicitly nonlinear in \f$\lambda_{D}\f$ because of
!! the dependence of \f$h_{p}\f$ on that quantity through equation (3).
!! An extended Newton-Raphson scheme, using a three-term Taylor expansion
!! about an initial guess value is used to evaluate this quantity. The
!! method of <em> reversion of series </em> employed to extract an accurate
!! approximation in a single iteration using an initial guess of zero
!! (corresponding to an undiluted plume).The formulation of this procedure
!! is discussed in the appendix of ZM, Limitations on the maximum
!! value of \f$\lambda_{D}\f$ are imposed it two ways: (1) an absolute limit
!! is set through specification of the FMAX variable (units \f$m^{-1})\f$
!! ; (2) the lowest level of detrainment is required to be at or above
!! the level of minimum \f$\bar{h}_{*}\f$ .The above procedure for determining
!! the \f$\lambda_{D}\f$ profile is carried out from this level upward until
!! the LNB for a non-entraining plume is reached.
!!\n
!!\n 
!! A further constraint is that \f$\lambda_{D}\f$ must not increase with
!! height. This is consistent with the underlying plume ensemble framework
!! proposed by \cite Arakawa1974, and adapted in the ZM bulk
!! formulation, wherein individual plumes are distinguished by their
!! fractional entrainment rates with large entrainment rates being associated
!! with shallower plumes. However, local non-monotonic variations in
!! the \f$\bar{h}_{*}\f$ profile can in some cases cause the procedure outlined
!! above to produce a value of \f$\lambda_{D}\f$ at a given level that is
!! larger than the value at the level below it. This implies the existence
!! of a narrow layer with locally reduced buoyancy for the plume that
!! would otherwise detrain at the upper level. It is reasonable to assume
!! that this plume would have sufficient upward momentum to penetrate
!! the small region of negative buoyancy without undergoing substantial
!! detrainment. Therefore in these circumstances the value at the lower
!! level is reset to the larger of the two.
!!\n
!!\n 
!! Once the \f$\lambda_{D}\f$ profile is evaluated the mass flux profile
!! and corresponding entrainment and detrainment profiles mare evaluated.
!! These are formally defined as stated in equations (5) of ZM. In practice,
!! for the discretized formulation used in the model, the mass flux is
!! defined at layer interfaces while entrainment and detrainment are
!! evaluated at the center points of the layers. This is done, for the
!! layer (\f$j\f$) between interface levels (\f$j,j+1\f$) (indexed from the
!! top downward) by evaluating the mass flux at the upper interface as
!! \f$M_{u}(\lambda_{D}(z_{j}),z_{j})\f$ and also a ``non-detraining''
!! mass flux \f$M_{u}^{ND}=M_{u}(\lambda_{D}(z_{j+1}),z_{j})\f$. The the
!! layer updraft entrainment and detrainment rates \f$\left(E_{j,j+1},D_{j,j+1}\right)\f$
!! are defined as
!!\n
!! \f{equation}{
!! E_{j,j+1}=\frac{M_{u}^{ND}-M_{u}(\lambda_{D}(z_{j+1}),z_{j+1})}{z_{j}-z_{j+1}}\tag{4}
!! \f}
!! \f{equation}{
!! D_{j,j+1}=\frac{M_{u}^{ND}-M_{u}(\lambda_{D}(z_{j}),z_{j})}{z_{j}-z_{j+1}}\tag{5}
!! \f}
!! Since, by construction, \f$\lambda_{D}\f$ does not increase with height,
!! these quantities will be non-negative. The expression for the detrainment
!! rate is a discrete approximation to equation (5a) of ZM.
!!\n
!!\n 
!! Using the above determined mass flux, entrainment and detrainment
!! profiles, discretized versions of equations (6) in ZM are solved for
!! the bulk updraft properties. These equations are first combined into
!! a budget equation for the bulk updraft moist static energy (\f$h_{u})\f$
!! which is discretized and used to evaluate this quantity starting from
!! from the specified base level. The top of the convective layer is
!! determined as the highest level (\f$z_{t})\f$ such that \f$h_{u}\geq\bar{h}_{*}\f$
!! for \f$z_{b}\leq z\leq z_{t}\f$ . The bulk mass flux, entrainment and
!! detrainment are set to zero above this level.
!!\n
!!\n 
!! The base level is typically below the lifting condensation level (LCL)
!! of the bulk updraft. The bulk updraft is assumed to be unsaturated
!! with respect to water vapor below the LCL. Accordingly the budget
!! equations for the dry static energy (\f$S_{u}=c_{p}T_{u}+gz)\f$ and the
!! water vapor mixing ratio (\f$q_{u})\f$ are solved separately starting
!! from the base level assuming that there is no condensation. Using
!! the corresponding bulk updraft temperature and ambient pressure the
!! saturation value of the water vapor pressure and the corresponding
!! water vapor mixing ratio are evaluated. The LCL is set as the lowest
!! level where this saturation mixing ratio is less that the bulk updraft
!! mixing ratio calculated assuming no condensation. Above this level
!! the bulk updraft is assumed to be saturated with respect to water
!! vapor, i.e. \f$q_{u}=q_{*}(T_{u},p)\f$. In this case, given the bulk
!! updraft moist static energy, pressure and height above the surface,
!! the corresponding temperature is a the solution of the equation
!!\n
!! \f{equation}{
!! c_{p}T_{u}+L_{v}q_{*}(T_{u},p)+gz=h_{u}\tag{6}
!! \f}
!! The left-hand side of this expression can be approximated with a
!! two-term Taylor expansion about \f$\bar{h}_{*}\f$ to give the following
!! approximate expressions for the updraft dry static energy and water
!! vapor mixing ratio:
!!\n
!! \f{equation}{
!! q_{u}\sim q_{*}(\bar{\bar{T},p)+\frac{\gamma}{L_{v}(1+\gamma)}\left(h_{u}-\bar{h}_{*}\right)}\tag{7}
!! \f}
!! \f{equation}{
!! S_{u}=\bar{S}+\frac{(h_{u}-\bar{h}_{*})}{1+\gamma}\tag{8}
!! \f}
!! where \f$\gamma=1+(L_{v}/c_{p})(\partial q_{*}/\partial\bar{T)}\f$ .
!! These approximations are used to evaluate the effective condensation
!! rate for layer (\f$j,j+1\f$) in accord with a discretized representation
!! of equation (6a) of ZM where the updraft dry static energy, water
!! vapor and liquid water mixing ratios are evaluated at layer interfaces
!! while the dry static energy and water vapor mixing ratio for the environment
!! are evaluated at the mid-points of the layers in terms of the large-scale
!! mean (prognostic) variables and interpolated to layer interfaces. 
!!\n
!!\n
!! <em> Note that mixed phase and ice-phase effects are ignored in the 
!! operational version of the ZM scheme. </em>
!!\n
!!\n 
!! The liquid water mixing ratio of the bulk updraft is evaluated at
!! layer interfaces using a discretized version of equation (6c) in ZM.
!! The layer-mean liquid water detrainment is assumed to be a weighted
!! average of the values at the upper and lower interfaces with a specified
!! weight parameter (\f$BETA\leq1\f$) for the upper interface contribution.
!! The form of the precipitation flux term shown in equation 7 of ZM
!! has been retained but the constant conversion factor (\f$C_{0})\f$ has
!! been replaced by a specified vertical profile function that allows
!! for the efficiency of precipitation formation to be reduced below
!! the environmental freezing level and enhanced above it.
!!\n 
!! \subsection ssec_downdrafts Downdrafts
!! A saturated, evaporatively driven bulk downdraft is included. The
!! basic formulation for this bulk downdraft is similar to that outlined
!! in ZM but simplified in CanAM implementation by setting the downdraft
!! entrainment to zero. The downdraft is initiated at the base
!! of the lowest detrainment level with the downdraft moist static energy
!! set equal to the large-scale environmental value. Between this level
!! and the base level of the convective layer the mass flux and moist
!! static energy of the downdraft are assumed to be independent of height.
!! The downdraft detrains in the layer immediately below the base level.
!! Since it is assumed to be saturated with respect to water vapor, the
!! downdraft dry static energy and water vapor mixing ratio are evaluated
!! in that layer using similar approximations to those used to evaluate
!! updraft properties where the updraft is saturated. The magnitude of
!! the downdraft mass flux, relative the that of the updraft at the base
!! of the convective layer is determined in terms of the precipitation
!! production in the updraft and the evaporation in the downdraft as
!!\n
!! \f{equation}{
!! \frac{M_{d}}{M_{b}}=-\left(\frac{\mu PCP}{\mu PCP+EVP}\right)\tag{9}
!! \f}
!! where \f$PCP\f$,\f$EVP\f$ are, respectively the precipitation production per
!! unit updraft base mass flux and the evaporation of rainwater in the
!! downdraft per unit downdraft mass flux. The weighting parameter \f$\mu\f$ is
!! specified to be 0.75 in the CanAM implementation. This parameter
!! sets the maximum fraction of the precipitation produced in the updraft
!! that can be evaporated in the downdraft if the downdraft maximum evaporation
!! rate is large relative to it.
!!\n
!! \subsection ssec_down_entrainment Downdraft entrainment
!! 
!! Downdraft entrainment effects can be included as in ZM by replacing
!! this code
!> \code
!!      DO 630 J=MSG+1,ILEV                                           
!!      DO 630 IL=IL1G,IL2G
!!        IF(EPS0(IL).GT.0.)                                        THEN   
!!          ZDEF(IL)=ZFD(IL)-ZF(IL,J)
!!          IF((J.EQ.JD(IL)) .AND. FACT(IL).GT.0.)                  THEN
!!            MD(IL,J)=-1.
!!          ENDIF
!!          IF((J.EQ.JB(IL)) .AND. FACT(IL).GT.0.)                  THEN
!!            MD(IL,J)=-1.
!!          ENDIF
!!          IF((J.LT.JB(IL)) .AND. ZDEF(IL).GT.0.)                  THEN
!!            MD(IL,J)=-1.
!!          ENDIF
!!        ENDIF
!!  630 CONTINUE
!! \endcode
!! with this code
!> \code
!!      DO 630 J=MSG+1,ILEV                                           
!!      DO 630 IL=IL1G,IL2G
!!          ZDEF(IL)=ZFD(IL)-ZF(IL,J)
!!          IF((J.EQ.JD(IL)) .AND. FACT(IL).GT.0.) THEN
!!             MD(IL,J)=-ALFA(IL)
!!          ENDIF
!!
!!          IF((J.EQ.JB(IL)) .AND. FACT(IL).GT.0.) THEN
!!              MD(IL,J)=-1.
!!          ENDIF
!!
!!          IF((J.LT.JB(IL)) .AND. ZDEF(IL).GT.0. .AND. EPS0(IL).GT.0.) THEN
!!            MD(IL,J)=-ALFA(IL)/(2.*EPS0(IL))*(EXP(2.*EPS0(IL)*ZDEF(IL))-1.)/ZDEF(IL)
!!          ENDIF
!!      630 CONTINUE
!! \endcode
