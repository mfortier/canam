!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      SUBROUTINE BUDCOR(XSRC1,XSRC2,XSNK,XNET,IL1,IL2,ILG,NTRAC)
C-----------------------------------------------------------------------
C     * SEP 18/2009 - K.VONSALZEN. NEW SUBROUTINE
C
C     * UPDATES CHEMICAL SOURCE (XSRC) AND SINK (XSNK) TERMS DIAGNOSED 
C     * IN THE CONVECTION TO CORRECT NUMERICAL ERRORS IN THE DIAGNOSED 
C     * TERMS. THE IDEA IS TO CORRECT THE TERMS SO THAT THE DIAGNOSED
C     * SOURCES AND SINKS ARE CONSISTENT WITH THE EXPECTED NET 
C     * SOURCE/SINK (XNET). THE CORRECTION IS ACHIEVED BY SOLVING 
C     * XNET=FSRC*XSRC-FSNK*XSNK WHERE THE FACTORS FSRC AND FSNK ARE 
C     * DETERMINED BY MINIMIZING F=(1.-FSRC)**2+(1.-FSNK)**2. THE 
C     * UPDATED SINK/SOURCE TERMS ARE THEREFORE FSRC*XSRC RESP. 
C     * FSNK*XSNK.
C-----------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, INTENT(INOUT), DIMENSION(ILG,NTRAC) :: XSRC1 !<Variable description\f$[units]\f$
      REAL, INTENT(INOUT), DIMENSION(ILG,NTRAC) :: XSRC2 !<Variable description\f$[units]\f$
      REAL, INTENT(INOUT), DIMENSION(ILG,NTRAC) :: XSNK !<Variable description\f$[units]\f$
      REAL, INTENT(INOUT), DIMENSION(ILG,NTRAC) :: XNET !<Variable description\f$[units]\f$

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      REAL, DIMENSION(ILG,NTRAC) :: XSRC,FSRC,FSNK,ATMP
      REAL, PARAMETER :: YSMALL=1.E-30
C-----------------------------------------------------------------------
C
      XSRC(IL1:IL2,:)=XSRC1(IL1:IL2,:)+XSRC2(IL1:IL2,:)
      WHERE ( XSRC(IL1:IL2,:) > YSMALL )
        ATMP(IL1:IL2,:)=-XSNK(IL1:IL2,:)/XSRC(IL1:IL2,:)
        FSNK(IL1:IL2,:)=(1.+XNET(IL1:IL2,:)*ATMP(IL1:IL2,:)
     1                /XSRC(IL1:IL2,:)-ATMP(IL1:IL2,:))
     2                /(1.+ATMP(IL1:IL2,:)**2)
      ELSEWHERE
        WHERE ( XSNK(IL1:IL2,:) > YSMALL )
          FSNK(IL1:IL2,:)=-XNET(IL1:IL2,:)/XSNK(IL1:IL2,:)
        ELSEWHERE
          FSNK(IL1:IL2,:)=0.
        ENDWHERE
      ENDWHERE
      WHERE ( XSRC(IL1:IL2,:) > YSMALL )
        FSRC(IL1:IL2,:)=(XNET(IL1:IL2,:)
     1                +FSNK(IL1:IL2,:)*XSNK(IL1:IL2,:))/XSRC(IL1:IL2,:)
      ELSEWHERE
        FSRC(IL1:IL2,:)=0.
      ENDWHERE
      XSRC1(IL1:IL2,:)=XSRC1(IL1:IL2,:)*FSRC(IL1:IL2,:)
      XSRC2(IL1:IL2,:)=XSRC2(IL1:IL2,:)*FSRC(IL1:IL2,:)
      XSNK (IL1:IL2,:)=XSNK (IL1:IL2,:)*FSNK(IL1:IL2,:)
C
      RETURN
      END 

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
