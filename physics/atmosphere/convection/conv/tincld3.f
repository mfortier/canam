!>\file
!>\brief The purpose of this subroutine is to evaluate the temperature and
!! water vapor mixing ratio of an air parcel give its moist static energy
!! and total water (vapor plus condensate) mixing ratio.
!!
!! @author Norm McFarlane, John Scinocca
!
      SUBROUTINE TINCLD3(TC,ILG,ILEV,MSG,IL1,IL2,LAL,WRK,HMNC,QC,QSTC,
     1                   ZF,PF,RRL,GRAV,CPRES,EPS1,EPS2,A,B,ISKIP)
C-----------------------------------------------------------------------
C     * OCT 24/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - IMPLICIT NONE WITH NECESSARY 
C     *                              PROMOTION TO WORK IN 32-BIT.
C     * MAY 04/2006 - M.LAZARE/    PREVIOUS VERSION FOR GCM15E:
C     *               K.VONSALZEN. - CALCULATION OF SATURATION VAPOUR
C     *                              PRESSURE MODIFIED TO WORK OVER
C     *                              COMPLETE VERTICAL DOMAIN, BY
C     *                              SPECIFYING LIMIT AS ES->P.
C     *                              THIS REQUIRES PASSING IN EPS2.
C     * FEB 06/2004 - M.LAZARE/    PREVIOUS VERSION TINCLD FOR GCM15D:
C     *               K.VONSALZEN/
C     *               N.MCFARLANE. 
C                                                   
C     * CALCULATES IN-CLOUD TEMPERATURE AND SATURATION WATER VAPOUR 
C     * MIXING RATIO FROM MOIST STATIC ENERGY AND TOTAL WATER MIXING 
C     * RATIO. IT IS REQUIRED THAT THE WATER VAPOUR MIXING RATIO
C     * FROM THE INPUT IS LARGER THAN 0.
C-----------------------------------------------------------------------
      IMPLICIT NONE
C     
C     * MULTI-LEVEL WORK FIELDS. 
C

      REAL*8,  DIMENSION(ILG,ILEV)         ::TC !< Parcel temperature \f$[K]\f$
      REAL*8,  DIMENSION(ILG,ILEV)         ::WRK !<Variable description\f$[units]\f$
      REAL*8,  DIMENSION(ILG,ILEV)         ::ZF !< Local height above ground of model levels \f$[m]\f$
      REAL*8,  DIMENSION(ILG,ILEV)         ::QSTC !< Saturation water vapor mixing ratio \f$[kg/kg]\f$
      REAL*8,  DIMENSION(ILG,ILEV)         ::PF !< Local ambient atmospheric pressure on model levels \f$[mb]\f$
      REAL*8,  DIMENSION(ILG,ILEV)         ::HMNC !< Moist static energy for ambient air \f$[J/g]\f$
      REAL*8,  DIMENSION(ILG,ILEV)         ::QC !< Ambient total water mixing ratio \f$[kg/kg]\f$

C     
C     * SINGLE-LEVEL WORK FIELDS. 
C      
      INTEGER, DIMENSION(ILG)              :: LAL !< Lowest model level for saturation conditions to be evaluated \f$[1]\f$
      INTEGER, DIMENSION(ILG)              :: ISKIP !< Switch to control execution of calculations in subroutine \f$[1]\f$

C
C     * SCALAR QUANTITIES PASSED IN.
C
      REAL RRL !< Latent heat of vaporization \f$[J/kg]\f$
      REAL GRAV !< Acceleration due to gravity \f$[m/s^{2}]\f$
      REAL CPRES !< Specific heat at constant pressure \f$[J/kg/K]\f$
      REAL EPS1 !< Ratio of molecular weight of water vapor to the mean molecular weight of dry air \f$[1]\f$
      REAL EPS2 !< 1-EPS1 \f$[1]\f$
      REAL A !< Constant used in saturation vapour pressure calculation \f$e_{s}=exp(A-B/T)\f$ \f$[units]\f$
      REAL B !< Constant used in saturation vapour pressure calculation \f$e_{s}=exp(A-B/T)\f$\f$[units]\f$
      
      INTEGER ILG !< Index limit of the latitude/longitude column array \f$[1]\f$
      INTEGER ILEV !< Index limit of the vertical levels (top to bottom) \f$[1]\f$
      INTEGER MSG !< Index of the starting level for prognostic moisture in the model \f$[1]\f$
      INTEGER IL1 !< Start index of the longitude section array \f$[1]\f$
      INTEGER IL2 !< End index of the longitude section array \f$[1]\f$

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


C
C     * INTERNAL SCALAR QUANTITIES.
C
      REAL*8  AWRK,EST,ESTREF,QSTTMP,QTEMP,TTEMP,HMNPR,DHMNPR,ETMP
      REAL*8  TMIN,TMAX
      REAL    EPSLIM
      INTEGER L,IL
C
C     * LOCAL DATA CONSTANTS.
C
      DATA TMIN / 150. /
      DATA TMAX / 400. /
C-----------------------------------------------------------------------
C     * INITIALIZE CLOUD BASE VALUES BASED ON UNSATURATED 
C     * CONDITIONS.
C
      DO 60 L=MSG+2,ILEV 
      DO 60 IL=IL1,IL2 
         IF ( ISKIP(IL).EQ.0 ) THEN
            QSTC(IL,L)=QC(IL,L)
            WRK(IL,L)=LOG(QSTC(IL,L))
            TC(IL,L)=B/( A-LOG(PF(IL,L)/( EPS1/QSTC(IL,L)+1. )) )
         ENDIF
  60  CONTINUE
C
C     * CALCULATE TEMPERATURE AND SATURATION MIXING RATIO USING NEWTON-
C     * RAPHSON'S ITERATIVE METHOD UNDER SATURATED CONDITIONS. THE 
C     * ITERATIONS ARE PERFORMED FOR LOG(Q*) AS ITERATION VARIABLE IN 
C     * ORDER TO ENSURE POSITIVE DEFINITENESS AND CONVERGENCE OF THE 
C     * METHOD. 
C
      EPSLIM=0.001
      DO 80 L=ILEV,MSG+2,-1 
      DO 80 IL=IL1,IL2 
         IF ( L.LE.LAL(IL) .AND. ISKIP(IL).EQ.0 ) THEN
            AWRK=( HMNC(IL,L)-RRL*QC(IL,L)-GRAV*ZF(IL,L) )/CPRES
            AWRK=MIN(MAX(AWRK,TMIN),TMAX)
            EST=EXP(A-B/AWRK)
            ESTREF=PF(IL,L)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
            IF ( EST .GE. ESTREF ) THEN
C
C              *** NO SATURATION.
C
               TC(IL,L)=AWRK
               QSTC(IL,L)=1.-EPSLIM
            ELSE
               QSTTMP=EPS1*EST/(PF(IL,L)-EST)
               IF ( QSTTMP.GE.QC(IL,L) ) THEN
C
C                 *** NO SATURATION.
C
                  TC(IL,L)=AWRK
                  QSTC(IL,L)=QSTTMP
               ELSE
C
C                 *** FIRST ITERATION.
C
                  QTEMP=QSTC(IL,L)
                  TTEMP=TC(IL,L)
                  HMNPR=CPRES*TTEMP+GRAV*ZF(IL,L)+RRL*QTEMP
     1                 -HMNC(IL,L)
                  DHMNPR=CPRES*EPS1*TTEMP*TTEMP/( B*(EPS1+QTEMP) )
     1                  +RRL*QTEMP
                  WRK(IL,L)=WRK(IL,L)-HMNPR/DHMNPR 
C
C                 *** SECOND ITERATION.
C
                  QSTTMP=EPS1*EST/(PF(IL,L)-EST)
                  QTEMP=MAX(EXP(WRK(IL,L)),QSTTMP)
                  TTEMP=B/( A-LOG(PF(IL,L)/(EPS1/QTEMP+1.)) )
                  HMNPR=CPRES*TTEMP+GRAV*ZF(IL,L)+RRL*QTEMP
     1                 -HMNC(IL,L)
                  DHMNPR=CPRES*EPS1*TTEMP*TTEMP/( B*(EPS1+QTEMP) )
     1                  +RRL*QTEMP
                  WRK(IL,L)=WRK(IL,L)-HMNPR/DHMNPR 
C
C                 *** THIRD ITERATION.
C
                  QTEMP=MAX(EXP(WRK(IL,L)),QSTTMP)
                  TTEMP=B/( A-LOG(PF(IL,L)/(EPS1/QTEMP+1.)) )
                  HMNPR=CPRES*TTEMP+GRAV*ZF(IL,L)+RRL*QTEMP
     1                 -HMNC(IL,L)
                  DHMNPR=CPRES*EPS1*TTEMP*TTEMP/( B*(EPS1+QTEMP) )
     1                  +RRL*QTEMP
                  WRK(IL,L)=WRK(IL,L)-HMNPR/DHMNPR 
C
C                 *** RETRIEVE SATURATION WATER MIXING RATIO AND 
C                 *** TEMPERATURE.
C
                  QSTC(IL,L)=MAX(EXP(WRK(IL,L)),QSTTMP)
                  IF ( QSTC(IL,L).GT.0. ) THEN
                     TC(IL,L)=B/(A-LOG(PF(IL,L)/(EPS1/QSTC(IL,L)+1.)))     
                  ELSE
                     TC(IL,L)=0.
                  ENDIF
               ENDIF
            ENDIF
C
C           *** ALLOW ONLY TEMPERATURES BETWEEN TMIN AND TMAX.
C
            TC(IL,L)=MIN(MAX(TC(IL,L),TMIN),TMAX)
            ETMP=EXP(A-B/TC(IL,L))
            IF ( ETMP.LT.ESTREF ) THEN
              EST=ETMP
            ELSE
              EST=ESTREF
            ENDIF
            QSTC(IL,L)=EPS1*EST/(PF(IL,L)-EST)
         ENDIF
  80  CONTINUE
C
      RETURN
      END
!> \file
!! \section sec_theory Theoretical formulation
!!\n 
!! The subroutine makes use of the following accurate empirical formula
!! for the saturation vapor pressure of water (\f$e_{*})\f$ as a function
!! of its temperature (\f$T\f$):
!!\n
!! \f{equation}{
!! e_{*}=exp[-(A-B/T)]\label{eq_1}\tag{1}
!! \f}
!!\n
!! The corresponding formula for the saturation water vapor mixing ratio is
!!\n
!! \f{equation}{
!! r_{*}(T,p)=\epsilon e_{*}/(p-e_{*})\label{eq_2}\tag{2}
!! \f}
!!\n
!! This equation can, alternatively, be used to express the parcel temperature
!! as a function of its water vapor mixing ratio in saturated conditions as
!!\n
!! \f{equation}{
!! T(r_{*},p)=\frac{B}{A-ln(\frac{P}{\epsilon/r_{*}+1})}\label{eq_3}\tag{3}
!! \f}
!!\n
!! Given the input values of the parcel moist static energy (\f$h^{(p)}\f$)
!! at a given height (\f$z\f$) above the surface, and the corresponding
!! total water mixing ratio (\f$r_{c}^{(p)}=r_{v}^{(p)}+r_{l}^{(p)}+r_{i}^{(p)}\f$)
!! the parcel temperature and water vapor mixing ration can be determined
!! from the following relationships:
!!\n 
!! \subsection ssrc_unsat_conditions Unsaturated conditions
!!\n
!! \f{equation}{
!! r_{v}^{(p)}=r_{c}^{(p)}\label{eq_4}\tag{4}
!! \f}
!!\n
!! \f{equation}{
!! T^{(p)}=(h^{(p)}-L_{v}r_{v}^{(p)}-gz)/c_{p}\label{eq_5}\tag{5}
!! \f}
!!\n
!! where \f$L_{v}\f$ is the latent heat of vaporization and \f$c_{p}\f$ is
!! the specific heat at constant pressure.}}
!!\n 
!! \subsection ssec_sat_conditions Saturated conditions
!!\n 
!! In the subroutine these estimates of the parcel water vapor mixing
!! ratio and temperature are evaluated initially. Using this estimated
!! temperature and the ambient atmospheric pressure, a reference saturation
!! value of the mixing ratio is evaluated using equations (1) and (2).
!! If this reference saturation mixing ratio is less that the estimated
!! parcel mixing ration then it is assumed that the parcel is saturated
!! with respect to water vapor. In this case the moist static energy
!! is held at the input value (\f$h^{(p)})\f$ but, since saturation conditions
!! apply,
!!\n
!! \f{equation}{
!! h(T^{(p)},r_{*}^{(p)},p,z)=c_{p}T^{(p)}+L_{v}r_{*}(T^{(p)},p)+gz=h^{(p)}\label{eq_6}\tag{6}
!! \f}
!!\n
!! For given ambient values of the pressure and height above ground,
!! this equation is a non-linear (transcendental) function of the temperature
!! and/or of the water vapor mixing ratio. In these circumstances a classical
!! Newton-Raphson iterative procedure can be used to obtain accurate
!! estimates of the parcel temperature and water vapor mixing ratio that
!! satisfy equations (1), (2) and (6). This involves choosing either
!! the temperature or the water vapor mixing ratio as the iteration variable
!! as they are related through equations (1) and (2) above, from which
!! it follows that , in saturated conditions,
!!\n
!! \f{equation}{
!! ln(p)+ln(r_{*})-ln(\epsilon+r_{*})=A-B/T^{(p)} \label{eq_7}\tag{7}
!! \f}
!!\n
!! the subroutine uses \f$ln(r_{*})\f$ as the iteration variable with the
!! iteration procedure being defined as
!!\n
!! \f{equation}{
!! [ln(r_{*})]^{(i)}=[ln(r_{*})]^{(i-1)}-\frac{[h-h^{(p)}]^{(i-1)}}{[\partial h/\partial ln(r_{*})]^{(i-1)}}\label{eq_8}\tag{8}
!! \f}
!!\n
!! where \f$i\f$is the iteration number. Note that the ambient atmospheric
!! pressure and height are locally fixed variables for iteration purposes.
!! Given the value of \f$r_{*}^{(i)}\f$, the value of the corresponding
!! estimated parcel temperature is obtained using equation (3) above.
!! It is easily shown that
!!\n
!! \f{equation}{
!! \frac{\partial T^{(p)}}{\partial ln(r_{*})}=\frac{(T^{(p)})^{2}}{B}\frac{\epsilon}{\epsilon+r_{*}}\label{eq_9}\tag{9}
!! \f}
!!\n
!! \f{equation}{
!! \frac{\partial h}{\partial ln(r_{*})}=c_{p}\frac{(T^{(p)})^{2}}{B}\left(\frac{\epsilon}{\epsilon+r_{*}}\right)+L_{v}r_{*}\label{eq_10}\tag{10}
!! \f}
!!\n
!! Typically \f$\epsilon>>r_{*}.\f$ and therefore all of the quantities
!! in these equations are strongly positive and well-behaved. The iteration
!! procedure converges sufficiently rapidly that only three iterations
!! are needed to achieve a high degree of accuracy. At the third iteration
!! stage, the final water vapour mixing ratio and temperature are evaluated
!! as
!!\n
!! \f{equation}{
!! r_{v}^{(p)}=r_{*}^{(3)};T^{(p)}=T(r_{*}^{(3)},p)\label{eq_11}\tag{11}
!! \f}
