!>\file
!>\brief  The purpose of this subroutine is to evaluate the base updraft mass
!! flux for the Zhang-McFarlane convection scheme.
!!
!! @author Norman McFarlane, John Scinocca
!       

      SUBROUTINE CLOSUR10(Q,T,P,Z,S,TP,QS,QU,SU,MC,DU,MU,MD,QD,SD,ALPHA,        
     1                   QHAT,SHAT,DP,QSTP,ZF,QL,DSUBCLD,MB,QDB,SDB,DSR,
     2                   UG,VG,BETAU,BETAD,CAPE,TL,LCL,LEL,JT,MX,J0,
     3                   DELT,ILEV,ILG,IL1G,IL2G,RD,GRAV,CP,MSG,CAPELMT)
C
C     * APR 30/2012 - M.LAZARE.    NEW VERSION FOR GCM16:
C     *                            ALF INCREASED FROM 1.E8 TO 5.E8. 
C     * DEC 18/2007 - M.LAZARE     PREVIOUS VERSION CLOSUR9 FOR GCM15G/H/I:
C     *                            ALF DECREASED FROM 1.E9 TO 1.E8.
C     * JUN 15/2006 - M.LAZARE.    PREVIOUS VERSION CLOSUR8 FOR GCM15F:
C     *                            - NOW USES INTERNAL WORK ARRAYS
C     *                              INSTEAD OF PASSED WORKSPACE.    
C     *                            - "ZERO" DATA CONSTANT ADDED
C     *                               AND USED IN CALL TO INTRINSICS. 
C     * DEC 07/2004 - M.LAZARE.    PREVIOUS VERSION CLOSUR7 FOR GCM15C/D/E:
C     *                            PROGNOSTIC CLOSURE NOW DEFAULT (ICLOS=1).
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
C     * I/O FIELDS:
C       

      REAL  , DIMENSION(ILG,ILEV) :: Q !< Ambient water vapor mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: T !< Ambient temperature \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) :: Z !< Height above the surface of mid-layer levels \f$[m]\f$
      REAL  , DIMENSION(ILG,ILEV) :: S !< Ambient dry static energy /specific heat \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) :: P !< Ambient pressure \f$[hPa]\f$
      REAL  , DIMENSION(ILG,ILEV) :: TP !< Parcel temperature \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) :: QS !< Ambient saturation mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: QU !< Updraft mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: SU !< Updraft dry static energy/specific heat \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) :: MC !< Total mass flux/unit base mass flux \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) :: DU !< Updraft detrainment rate/unit base mass flux \f$[m^{-1})]\f$
      REAL  , DIMENSION(ILG,ILEV) :: MU !< Updraft mass flux/unit base mass flux \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) :: MD !< Downdraft mass flux per unit base mass flux \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) :: QD !< Downdraft water vapor mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: SD !< Downdraft dray static energy/specific heat \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) :: ALPHA !< Vertical differencing parameter (=1 for upstream) \f$[1]\f$
      REAL  , DIMENSION(ILG,ILEV) :: QHAT !< Water vapor mixing ratio at layer interface levels \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: SHAT !< Dry static energy/(specific heat) at layer interface levels \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) :: DP !< Layer pressure thickness \f$[hPa]\f$
      REAL  , DIMENSION(ILG,ILEV) :: QSTP !< Parcel saturation mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: ZF !< Interface level heights \f$[m]\f$
      REAL  , DIMENSION(ILG,ILEV) :: QL !< Updraft liquid water mixing ratio \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) :: UG !< Eastward component of the ambient air velocity \f$[m/s]\f$
      REAL  , DIMENSION(ILG,ILEV) :: VG !< Northward component of the ambient air velocity \f$[m/s]\f$
      REAL  , DIMENSION(ILG,ILEV) :: DSR !< Density of ambient air to the density of dry air \f$[1]\f$

      REAL  , DIMENSION(ILG) :: MB !< Base level mass flux \f$[kg/m^{2}/s]\f$
      REAL  , DIMENSION(ILG) :: QDB !< Downdraft water vapor mixing ratio at cloud base  \f$[g/g]\f$
      REAL  , DIMENSION(ILG) :: SDB !< Downdraft dry static energy/(specific heat) at cloud base \f$[K]\f$
      REAL  , DIMENSION(ILG) :: BETAU !< Updraft mass fluxes at cloud base \f$[kg/m^{2}/s]\f$
      REAL  , DIMENSION(ILG) :: BETAD !< Downdraft mass fluxes at cloud base \f$[kg/m^{2}/s]\f$
      REAL  , DIMENSION(ILG) :: CAPE !< Convective available potential energy \f$[J/kg]\f$
      REAL  , DIMENSION(ILG) :: TL !< parcel saturation temperature (i.e. temperature at the parcel LCL) \f$[K]\f$
      REAL  , DIMENSION(ILG) :: DSUBCLD !< pressure thickness of the layer between the base level and the parcel LCL \f$[hPa]\f$

      INTEGER,DIMENSION(ILG) :: LCL !< Index of the level closest to the lifting condensation level of the parcel \f$[1]\f$
      INTEGER,DIMENSION(ILG) :: LEL !< Index of the highest level that can be reached by a buoyant parcel \f$[1]\f$
      INTEGER,DIMENSION(ILG) :: JT !< Index of the top of the convective layer \f$[1]\f$
      INTEGER,DIMENSION(ILG) :: MX !< Base level index \f$[1]\f$
      INTEGER,DIMENSION(ILG) :: J0 !< Index of the level of the base of the detrainment layer \f$[1]\f$

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C   REAL TAU !< CAPE depletion time scale \f$[s]\f%
C            !! Its minimum/default/maximum is [1800/2400/7200]
C
C   REAL ALF !< Proportionality parameter \f$[m^{4}kg^{-1}]\f$
C            !! Its minimum/default/maximum is [?/\f$5\vartimes10^{8}\f$/?]
C
C   REAL TAUS1 !< Damping time scale \f$[s]\f$
C              !! Its minimum/default/maximum is [?/21600/?]
C
C   REAL CAPELMT !< Threshold value of convective available potential energy \f$[J/kg]\f$
C                !! Its minimum/default/maximum is [0/0/?]
C==================================================================


C                                                                               
C     * INTERNAL WORK FIELDS:                                                          
C                                                                               
      REAL  , DIMENSION(ILG,ILEV) :: DTPDT,DQSDTP,DTMDT,DQMDT,
     1                               DBOYDT,THETAVP,THETAVM
      REAL  , DIMENSION(ILG)      :: DTBDT,DQBDT,DTLDT,RL,PMAX,TMAX,
     1                               QMAX,QHATMAX,SHATMAX,SUMAX,
     2                               SDMAX,MUMAX,MDMAX,QUMAX,QDMAX,
     3                               DADT,DZT,VSHEAR,UGB,VGB,MBTEST
C
      COMMON/EPS   / A,B,EPS1,EPS2                                            
      COMMON/HTCP  / TFREEZ,T2S,AI,BI,AW,BW,SLP                               
C
      DATA ICLOS/1/                                                               
      DATA TAU /2400./                                    
      DATA ZERO    / 0.     /
C---------------------------------------------------------------------------  
CCC   *************************************************************           
CCC   CHANGE OF SUBCLOUD LAYER PROPERTIES DUE TO CONVECTION IS                
CCC   RELATED TO CUMULUS UPDRAFTS AND DOWNDRAFTS.                             
CCC   MC(Z)=F(Z)*MB, MUB=BETAU*MB, MDB=BETAD*MB ARE USED                      
CCC   TO DEFINE BETAU, BETAD AND F(Z).                                         
CCC   NOTE THAT THIS IMPLIES ALL TIME DERIVATIVES ARE IN EFFECT                
CCC   TIME DERIVATIVES PER UNIT CLOUD-BASE MASS FLUX, I.E. THEY                
CCC   HAVE UNITS OF 1/MB INSTEAD OF 1/SEC.                                     
CCC   *************************************************************
C
C     * CONSTANTS FOR PROGNOSTIC CLOSURE SCHEME.
C
      ALF=5.E8
      TAUS1=6.*3600.
      REFM=1./2.4E-4
      TAUSMIN=180.
C
C     * OBTAIN GATHERED FIELDS AT "MX" FOR ENSUING USE.
C     * INITIALIZE "DADT" ARRAY.
C
      DO 50 K=MSG+1,ILEV 
      DO 50 IL=IL1G,IL2G
        IF(K.EQ.MX(IL))                                          THEN
          PMAX   (IL)=P   (IL,K)
          TMAX   (IL)=T   (IL,K)
          QMAX   (IL)=Q   (IL,K)
          QHATMAX(IL)=QHAT(IL,K)
          QUMAX  (IL)=QU  (IL,K)
          QDMAX  (IL)=QD  (IL,K)
          SHATMAX(IL)=SHAT(IL,K)
          SUMAX  (IL)=SU  (IL,K)
          SDMAX  (IL)=SD  (IL,K)
          MUMAX  (IL)=MU  (IL,K)
          MDMAX  (IL)=MD  (IL,K) 
        ENDIF 
   50 CONTINUE      
C
      DO 100 IL=IL1G,IL2G                                                    
         DADT   (IL) = 0.
         DZT    (IL) = 0.
         VSHEAR (IL) = 0.
         UGB    (IL) = UG(IL,LCL(IL))
         VGB    (IL) = VG(IL,LCL(IL))                                        
         EB          = PMAX(IL)*QMAX(IL)/(EPS1+QMAX(IL))                       
         DTBDT(IL)   = (1./DSUBCLD(IL))                                       
     1                 *( MUMAX(IL)*( SHATMAX(IL)-SUMAX(IL) )               
     2                 +MDMAX(IL)*( SHATMAX(IL)-SDMAX(IL) ) )                 
         DQBDT(IL)   = (1./DSUBCLD(IL))                                    
     1                 *( MUMAX(IL)*( QHATMAX(IL)-QUMAX(IL) )                
     2                 +MDMAX(IL)*( QHATMAX(IL)-QDMAX(IL) ) )                 
         DEBDT       = EPS1*PMAX(IL)/(EPS1+QMAX(IL))**2*DQBDT(IL)           
         DTLDT(IL)   =-2840.*(3.5/TMAX(IL)*DTBDT(IL)-DEBDT/EB)/                
     1                 (3.5*LOG(TMAX(IL))-LOG(EB)-4.805)**2                 
  100 CONTINUE
C                                                                            
CCC   ******************************************************************    
CCC     DTMDT AND DQMDT ARE CUMULUS HEATING AND DRYING.                       
CCC   ******************************************************************       
C
      DO 150 K=MSG+1,ILEV                                                      
      DO 150 IL=IL1G,IL2G                                                      
        DTMDT(IL,K)=0.                                                         
        DQMDT(IL,K)=0.                                                         
  150 CONTINUE
C
      DO 175 K=MSG+1,ILEV-1                                                    
      DO 175 IL=IL1G,IL2G                                                     
        IF(K.EQ.JT(IL))                                             THEN       
C         RL(IL)=(2.501-.00237*(T(IL,K+1)-TFREEZ))*1.E6                        
          RL(IL)=2.501E6                                                       
          DTMDT(IL,K)=(DSR(IL,K)/DP(IL,K))                                            
     1               *(MU(IL,K+1)*(SU(IL,K+1)-SHAT(IL,K+1)                     
     2                             -RL(IL)/CP*QL(IL,K+1) ) +                   
     3                 MD(IL,K+1)*(SD(IL,K+1)-SHAT(IL,K+1)) )                  
          DQMDT(IL,K)=(DSR(IL,K)/DP(IL,K))                                            
     1               *(MU(IL,K+1)*(QU(IL,K+1)-QHAT(IL,K+1)+QL(IL,K+1)) +       
     2                 MD(IL,K+1)*(QD(IL,K+1)-QHAT(IL,K+1)) )                  
        ENDIF                                                                  
  175 CONTINUE                                                                 
C                   
      BETA=1.                                                                  
      DO 180 K=MSG+1,ILEV-1                                                    
      DO 180 IL=IL1G,IL2G                                                      
        IF(K.GT.JT(IL) .AND. K.LT.MX(IL))                           THEN       
C         RL(IL)=(2.501-.00237*(T(IL,K)-TFREEZ))*1.E6                         
          RL(IL)=2.501E6                                                      
          DTMDT(IL,K)=DSR(IL,K)*(MC(IL,K)*(SHAT(IL,K)-S(IL,K))                          
     1                +MC(IL,K+1)*(S(IL,K)-SHAT(IL,K+1)))/DP(IL,K)            
     2      -RL(IL)/CP*DU(IL,K)*(BETA*QL(IL,K)+(1-BETA)*QL(IL,K+1))            
          DQMDT(IL,K)=DSR(IL,K)*(MC(IL,K)*(QHAT(IL,K)-Q(IL,K))                           
     1                +MC(IL,K+1)*(Q(IL,K)-QHAT(IL,K+1)))/DP(IL,K)             
     2                +DU(IL,K)*(QS(IL,K)-Q(IL,K))                             
     3                +DU(IL,K)*(BETA*QL(IL,K)+(1-BETA)*QL(IL,K+1))            
        ENDIF                                                                  
  180 CONTINUE                                                                 
C                   
      DO 200 K=MSG+1,ILEV                                                      
      DO 200 IL=IL1G,IL2G                                                      
        IF(K.GE.LEL(IL) .AND. K.LE.LCL(IL))                         THEN       
C         RL(IL)=(2.501-.00237*(TP(IL,K)-TFREEZ))*1.E6                        
          RL(IL)=2.501E6                                                      
          THETAVP(IL,K)=TP(IL,K)*(1000./P(IL,K))**(RD/CP)                     
     1                  *(1.+1.608*QSTP(IL,K)-QMAX(IL))                    
          THETAVM(IL,K)=T(IL,K)*(1000./P(IL,K))                                
     1                  **(RD/CP)*(1.+0.608*Q(IL,K))                           
          DQSDTP(IL,K)=QSTP(IL,K)*(1.+QSTP(IL,K)/EPS1)                        
     1                 *EPS1*RL(IL)/(RD*TP(IL,K)**2)                          
CCC     ******************************************************************     
CCC       DTPDT IS THE PARCEL TEMPERATURE CHANGE DUE TO CHANGE OF              
CCC       SUBCLOUD LAYER PROPERTIES DURING CONVECTION.                         
CCC     ******************************************************************     
          DTPDT(IL,K)=TP(IL,K)                                                 
     1               /(1.+RL(IL)/CP*(DQSDTP(IL,K)-QSTP(IL,K)/TP(IL,K)))*       
     2               (   DTBDT(IL)/TMAX(IL)+RL(IL)/CP*                       
     3               (DQBDT(IL)/TL(IL)-QMAX(IL)/TL(IL)**2*DTLDT(IL)          
     4                                                                ))       
CCC     ******************************************************************     
CCC       DBOYDT IS THE INTEGRAND OF CAPE CHANGE.                              
CCC     ******************************************************************     
          DBOYDT(IL,K)=((DTPDT(IL,K)/TP(IL,K)                                  
     1                 +1./(1.+1.608*QSTP(IL,K)-QMAX(IL))                    
     2                 *(1.608*DQSDTP(IL,K)*DTPDT(IL,K)-DQBDT(IL)))            
     3                 -(DTMDT(IL,K)/T(IL,K)+0.608/(1.+0.608*Q(IL,K))          
     4                 *DQMDT(IL,K)))*GRAV*THETAVP(IL,K)/THETAVM(IL,K)         
CCC     ******************************************************************     
        ENDIF                                                                  
 200  CONTINUE
C                                                                              
      DO 250 K=MSG+1,ILEV                                                      
      DO 250 IL=IL1G,IL2G                                                      
        IF(K.GT.LCL(IL) .AND. K.LT.MX(IL))                          THEN       
C         RL(IL)=(2.501-.00237*(TP(IL,K)-TFREEZ))*1.E6                         
          RL(IL)=2.501E6                                                       
          THETAVP(IL,K)=TP(IL,K)*(1000./P(IL,K))**(RD/CP)                      
     1                  *(1.+0.608*QMAX(IL))                                 
          THETAVM(IL,K)=T(IL,K)*(1000./P(IL,K))                                
     1                  **(RD/CP)*(1.+0.608*Q(IL,K))                           
CCC     ******************************************************************     
CCC       DBOYDT IS THE INTEGRAND OF CAPE CHANGE.                              
CCC     ******************************************************************     
          DBOYDT(IL,K)=(DTBDT(IL)/TMAX(IL)                                   
     1                 +0.608/(1.+0.608*QMAX(IL))*DQBDT(IL)                  
     2                 -DTMDT(IL,K)/T(IL,K)                                    
     3                 -0.608/(1.+0.608*Q(IL,K))*DQMDT(IL,K))                  
     4                 *GRAV*THETAVP(IL,K)/THETAVM(IL,K)                       
CCC     ******************************************************************     
        ENDIF                                                                  
 250  CONTINUE
C                                                                              
      IF(ICLOS.EQ.0) THEN    
C
       DO 300 K=MSG+1,ILEV
       DO 300 IL=IL1G,IL2G
        IF(K.GE.LEL(IL) .AND. K.LT.MX(IL))                        THEN      
          DADT(IL)=DADT(IL)+DBOYDT(IL,K)*(ZF(IL,K)-ZF(IL,K+1))   
        ENDIF                         
  300  CONTINUE                                                            
C
       DO 350 IL=IL1G,IL2G                                                     
        MB(IL)      = 0. 
        DLTAA=-1.*(CAPE(IL)-CAPELMT)
        IF(DADT(IL).NE.0.)     MB(IL)=MAX(DLTAA/TAU/DADT(IL),ZERO)   
  350  CONTINUE    
C
      ELSE
C
       DO 400 K=MSG+1,ILEV
       DO 400 IL=IL1G,IL2G
        IF(K.GE.LEL(IL) .AND. K.LT.MX(IL))                        THEN  
          ADZ=ZF(IL,K)-ZF(IL,K+1)   
          DADT(IL)=DADT(IL)+DBOYDT(IL,K)*ADZ
        ENDIF                         
  400  CONTINUE                                                         
C
       DO 450 IL=IL1G,IL2G                                              
        DLTAA=(CAPE(IL)-CAPELMT)
        IF(DADT(IL).LT.0..AND.DLTAA.GT.0.) THEN
           FR=-DADT(IL)
           MB(IL)=(DLTAA/ALF*DELT+MB(IL))
     1           /(1.+DELT/TAUS1)     
        ELSE
           MB(IL)=0.
        ENDIF   
  450  CONTINUE      
      ENDIF
C                                                                             
      RETURN                                                                  
      END        

!> \file
!! \section sec_theory Theoretical formulation
!!
!! There are two closure options available, controlled by the ICLOS variable. The original
!! diagnostic CAPE tendency closure of \cite Zhang1995 (hereinafter
!! ZM) is activated with the setting ICLOS=0. The prognostic closure
!! option of \cite Scinocca2004 is accessed by setting ICLOS=1.
!! The prognostic closure is the operational default for CanAM4.
!! \n
!! \n
!! NOTE: The vertical coordinate in CanAM is pressure based. All vertical
!! derivatives are expressed in terms of a local pressure coordinate
!! using the hydrostatic relationship, and approximated by centered finite
!! differences with level indexing being from the top downward. Model
!! prognostic variables (hereinafter denoted as ambient variables) are
!! defined at the mid-levels of layers. Cumulus updraft and downdraft
!! mass fluxes, dry and moist static energy, water vapor and liquid water
!! mixing ratios are defined at layer interface levels.
!!\n
!! \subsection ssec_cape_closure_iclos_0 CAPE tendency closure (ICLOS=0)
!!
!! The formulation of this closure condition is as described in ZM. It
!! assumes that the effect of the deep convection as represented by the
!! ZM scheme is to exponentially deplete convective available potential
!! energy (CAPE) at rate that is specified in terms of a prescribed damping
!! time scale \f$\tau\f$ such that
!!\n
!! \f{equation}{
!! \left(\frac{\partial CAPE}{\partial t}\right)_{conv}=-\frac{CAPE}{\tau}\tag{1}
!! \f}
!!\n
!! The convective available potential energy is evaluated in the buoyana.f
!! routine in terms of the buoyancy of an undiluted parcel of air lifted
!! adiabatically and reversibly (i.e. all condensate is carried with
!! the parcel) from the base level to the level of neutral buoyance (LNB) (see the documentation
!! for buoyana.f). Mixed-phase and ice-phase processes are ignored in the
!! lifting process so that condensation involves production of liquid
!! water only. In addition to conservation of total water (vapor + condensate),
!! it is assumed that the equivalent potential temperature the parcel
!! is conserved during the lifting process. It is assumed that the gas
!! constants, specific heat and latent heat of vaporization are all strictly
!! constants. Mixed-phase and ice phase effects are ignored. Therefore
!! the parcel equivalent potential temperature is defined approximately
!! as
!!\n
!! \f{equation}{
!! \theta_{e}^{(p)}=\theta^{(p)}exp\left(\frac{L_{v}r^{(p)}}{c_{p}T^{(p)}}\right)\tag{2}
!! \f}
!!\n
!! As noted above, this approximation ignores temperature and humidity
!! dependance of gas constants and specific heat and also sets the relative
!! humidity factor to unity (cf. \cite Emanuel1994, sec. 4.5).
!!\n 
!! For the purposes of evaluating its tendency due to deep convection,
!! the convective available potential energy is defined in terms of the
!! density potential temperatures of the parcel and environment (cf. \cite Emanuel1994, ch. 6).
!!\n
!! \f{equation}{
!! CAPE=\intop_{z_{b}}^{z_{t}}g\left(\frac{\theta_{\rho}^{(p)}-\overline{\theta}_{\rho}}{\overline{\theta}_{\rho}}\right)\tag{3}
!! \f}
!!\n
!! where \f$\theta_{\rho}=T_{\rho}\left(p_{0}/p\right)^{Rd/c_{p}}\f$ and
!! \f$p_{0}=1000hPa\f$. Density temperature and virtual temperature are
!! equivalent in the absence of condensation:
!!\n
!! \f{equation}{
!! T_{\rho}=T\frac{1+r_{v}/\epsilon}{1+r_{t}}\sim T(1+.608r_{v}-r_{l})=T(1+1.608r_{v}-r_{t})\tag{4}
!! \f}
!!\n
!! where \f$\epsilon(=.622)\f$ is the ratio of the gas constant for dry
!! air to that for water vapor. The middle quantity on the right-hand
!! side is the extended definition of the virtual temperature that is
!! frequently used in meteorological literature.
!!\n 
!!\n
!! From the definition of the convective available potential energy the
!! tendency due to convective effects is formally given by
!!\n
!! \f{equation}{
!! \left(\frac{\partial CAPE}{\partial t}\right)_{c}=\intop_{z_{b}}^{z_{t}}\frac{\partial}{\partial t}\left(\frac{g\left(\theta_{\rho}^{(p)}-\overline{\theta}_{\rho}\right)}{\overline{\theta}_{\rho}}\right)_{c}dz\tag{5}
!! \f}
!!\n
!! In the sub-cloud layer, i.e. the region between the base level and
!! the lifting condensation level (LCL) of the parcel, its potential
!! temperature and water vapor mixing ratio are independent of height
!! and equal to that at the base level. Above the parcel LCL is assumed
!! the the pressure level of the parcel is equal to that of the ambient
!! environment and the ambient pressure is not directly affected by convection
!! so that, for coding purposes, the integrand is written as
!! \f{equation}{
!! \left(g\frac{\theta_{\rho}^{(p)}}{\overline{\theta}_{\rho}}\right)\left(\frac{\partial}{\partial t}ln\left(\frac{T_{\rho}^{(p)}}{\overline{T}_{\rho}}\right)\right)_{c}
!! \f}
!! \n
!! The time derivatives of are then formally approximated as
!!\n
!! \f{equation}{
!! \frac{\partial}{\partial t}ln\left(T_{\rho}\right)\sim\frac{1}{T}\frac{\partial T}{\partial t}+\frac{1}{1+1.608r_{v}-r_{t}}\frac{\partial}{\partial t}\left(1.608r_{v}-r_{t}\right)\tag{6}
!! \f}
!!\n
!! Contributions to individual terms for parcel and environmental tendencies
!! are then evaluated separately for the sub-cloud layer and the cloud
!! layer. Note that, by assumption total water is conserved for the parcel
!! at all levels.
!!\n 
!! \subsubsection sssec_sub_cloud_t_wv Sub-cloud temperature and water vapor tendencies and their contribution to changes in the parcel density temperature
!!
!! The parcel is unsaturated in the sub-cloud layer. Therefore its water
!! vapor mixing ratio and potential temperature are separately invariant
!! with height in that region and equal to those at the base level. The
!! tendencies for the base level temperature and specific humidity (\f$q\f$)
!! water associated with convective updrafts and downdrafts are given
!! by equations 2a,b of ZM. These tendencies are approximated using centered
!! finite differences in the subroutine (Loop 100). These tendencies
!! also contribute to evaluating the tendencies of the parcel density
!! temperature in the cloud layer. Since the model vertical coordinate
!! is locally pressure based, these tendencies are expressed in terms
!! of this coordinate through the hydrostatic relationship.
!!\n 
!! \subsubsection sssec_parcel_dens Parcel density temperature tendency in the cloud layer
!!
!! The parcel is saturated with respect to water vapor in the cloud layer.
!! In this circumstance the water vapor mixing ratio is a function of
!! the temperature and pressure so that
!! \n
!! \f{equation}{
!! \left(\frac{\partial r_{p}}{\partial t}\right)_{c}=\left(\frac{\partial r_{*}}{\partial T}\frac{\partial T}{\partial t}\right)_{c}^{(p)}
!! \f}
!!\n
!! where
!!\n
!! \f{equation}{
!! r_{*}(T,p)=\frac{\epsilon e_{s}(T)}{p-e_{s}(T)}\tag{7}
!! \f}
!!\n
!! where \f$e_{s}\f$ is the saturation vapor pressure and \f$\epsilon=R_{d}/R_{v}\f$
!! is the ratio of the gas constant for dry air to that for water vapor.
!! Making use of the gas law, noting that in all circumstances of interest
!! the saturation vapor pressure is much smaller in magnitude than the
!! ambient air pressure, gives rise to the following acceptably accurate
!! approximation
!!\n
!! \f{equation}{
!! \partial r_{*}/\partial T=\left(1+r_{*}/\epsilon\right)\left(\frac{r_{*}}{e_{s}}\frac{\partial e_{s}}{\partial T}\right)-r_{*}/T\tag{8}
!! \f}
!!\n
!! The usual approximation to the Clausius-Clapeyron equation (\cite Emanuel1994, Sec. 4.4, 
!! AMS glossary, http://glossary.ametsoc.org/wiki/Clausius-clapeyron\_equation)
!! gives
!!\n
!! \f{equation}{
!! \frac{\partial e_{s}}{\partial T}=\frac{\epsilon L_{v}e_{s}}{R_{d}T^{2}}\tag{9}
!! \f}
!!\n
!!\n 
!! The formulation for evaluating the relevant derivatives of the parcel
!! equivalent potential temperature used in the subroutine makes use
!! of the definition of the \textit{saturation temperature} (i.e. the
!! temperature of the parcel at the LCL) of the base-level air developed
!! by of \cite Bolton1980 and the following approximation for the parcel
!! equivalent potential temperature
!!\n
!! \f{equation}{
!! \theta_{e}^{(p)}\sim\theta_{b}^{(p)}exp\left(\frac{r_{*}(T_{L})L_{v}}{c_{p}T_{L}}\right)\tag{10}
!! \f}
!!\n
!! where the temperature at the LCL is approximated by
!!\n
!! \f{equation}{
!! T_{L}\sim55+\frac{2840}{3.5ln(T_{b})-ln(e_{b})-4.805}\tag{11}
!! \f}
!!\n
!! Here \f$\left(T_{b},e_{b}\right)\f$ are, respectively the temperature
!! and water vapor pressure at the base level. Substituting the definition
!! given in equation (2) and taking the logarithmic derivative of both
!! sides of equation (10) and making use of equations (8), (9), and (11)
!! gives an expression of the tendency of the parcel temperature. This
!! is evaluated in Loop 200 of the subroutine.
!!\n 
!! \subsubsection ssec_t_wv_tend Contributions of ambient temperature and water vapor tendencies to the tendency of the the ambient density temperature}
!!
!! The contributions of deep convection to the ambient temperature and
!! water vapor mixing ratio tendencies, as represented by the ZM scheme,
!! can be derived by replacing specific humidity by water vapor (as a
!! is appropriate for the current operational formulation) mixing ratio
!! and combining equations (1), (5) and (6) in ZM to give the following
!! expressions
!!\n
!! \f{equation}{
!! c_{p}\left(\frac{\partial T}{\partial t}\right)_{cu}=\frac{1}{\rho_{d}}\left[M_{c}\frac{\partial S}{\partial z}+D_{u}L_{v}r_{l}^{(u)})\right]\tag{12}
!! \f}
!!\n
!! \f{equation}{
!! \left(\frac{\partial r}{\partial t}\right)_{cu}=\frac{1}{\rho_{d}}\left[M_{c}\frac{\partial r}{\partial z}+D_{u}\left(r_{*}(T,p)-r\right)\right]\tag{13}
!! \f}
!!\n
!! where \f$\rho_{d}\f$ is the dry air density, \f$M_{c}=M_{u}+M_{d}\f$ is
!! the total mass flux per unit base mass flux, \f$D_{u}\f$ is the updraft
!! detrainment rate per unite base mass flux. These expressions are used
!! to evaluate the contributions of the ambient temperature and mixing
!! ratio to the density potential temperature. The form of the detrainment
!! terms arises because it is assumed that detrainment occurs at levels
!! where the detraining updraft air is at the same temperature as the
!! environment.
!!\n 
!! \section sec_prog_closure_iclos_1 Prognostic closure (ICLOS=1)
!!\n 
!! The prognostic closure is based on the work of \cite Scinocca2004.
!! It is much simpler in its implementation. The essential difference
!! between the diagnostic and prognostic closure is that the base mass
!! flux is a prognostic variable in the latter and is represented by
!! the following equation
!!\n
!! \f{equation}{
!! \frac{\partial M_{b}}{\partial t}=\frac{CAPE}{\alpha}-\frac{M_{b}}{\tau}\tag{14}
!! \f}
!!\n
!! where the proportionality parameter, \f$\alpha\f$, and the damping time-scale,
!! \f$\tau\f$, are specified fixed parameters. This equation is solved numerically
!! for the base mass flux using a partially implicit Euler backward time
!! stepping scheme with a time step length \f$\delta t\f$ to give
!!\n
!! \f{equation}{
!! M_{b}^{(t+\delta t)}=\frac{\delta tCAPE^{(t)}/\alpha+M_{b}^{(t)}}{1+\delta t/\tau}\tag{15}
!! \f}
!! \section sec_adj_parameters Adjustable parameters
!!
!! **CAPE depletion time scale (TAU)**
!!\n
!! This time scale sets the exponential rate of depletion of convective available potential
!! energy for the diagnostic closure option (ICLOS=0). Typical values that have been used in 
!! practical implementations range from 30 minutes to 2 hours.
!! \n
!! \n
!! **Threshold value of convective available potential energy (CAPELMT)**
!! \n
!! The sets the onset value of CAPE for activation of deep convection
!! for both the diagnostic and prognostic closure options (current default
!! value is zero).
!! \n
!! \n
!! **Proportionality parameter (ALF)**
!! \n
!! This corresponds to the parameter \f$\alpha\f$ for the prognostic closure option (default
!! value \f$5\times10^{8}m^{4}kg^{-1}\f$). Sensitivity to this parameter
!! has been documented \cite Scinocca2004.
!! \n
!! \n
!! **Damping time scale (TAUS1)**
!! \n
!! This corresponds to the time scale \f$\tau\f$ for the prognostic closure option 
!! (default value 6 hr) . Sensitivity to the parameter has been documented \cite Scinocca2004. 
!! Note that the ratio TAUS1/ALF determines a limiting equilibrium value of the base mass flux
!! for a given value of CAPE.
