!>\file
!>\brief The main purpose of this subroutine is to evaluate convective available potential energy (CAPE)
!! and define the location of the top of the layer that may be occupied by moist convection in a given 
!! model column. This quantitiy is used in determining triggering conditions for moist convection and in 
!! the closure conditions for deep (precipitating) convection.
!!
!! @author Norm McFarlane, John Scinocca
!
      SUBROUTINE BUOYANA(Z,P,PF,T,Q,PRESSG,ILEV,ILEVM,ILG,IL1,IL2,
     1                   MSG,RRL,GRAV,CPRES,EPS1,EPS2,A,B,RGAS,RGASV,
     2                   TVFA,RGOCP,MAXI,LCL,LNB,LPBL,TL,CAPE,CAPEP,
     3                   TC,QSTC,HMN,PBLT)
C
C     * NOV 24/2006 - M.LAZARE.    - CALLS NEW TINCLD3 INSTEAD OF TINCLD2.
C     *                            - SELECTIVE PROMOTION OF VARIABLES TO
C     *                              REAL*8 TO SUPPORT 32-BIT MODE.`
C     * JUN 15/2006 - M.LAZARE.    NEW VERSION FOR GCM15F:
C     *                            - NOW USES INTERNAL WORK ARRAYS
C     *                              INSTEAD OF PASSED WORKSPACE.    
C     *                            - "ZERO" DATA CONSTANT ADDED
C     *                              AND USED IN CALL TO INTRINSICS. 
C     * MAY 04/2006 - M.LAZARE/    PREVIOUS VERSION BUOYAN9 FOR GCM15E:
C     *               K.VONSALZEN. - CALLS NEW TINCLD2.
C     *                              THIS REQUIRES PASSING IN EPS2.
C     * MAY 05/2001 - K.VONSALZEN. PREVIOUS VERSION BUOYAN8 FOR GCM15D.
C                                                   
C     * CALCULATES PROPERTIES OF UNDILUTED AIR PARCEL ASCENDING
C     * FROM THE PBL TO ITS LEVEL OF NEUTRAL BUOYANCY.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      PARAMETER (ITRIGG=0)
C
C     * I/O FIELDS:
C
 
      REAL  , DIMENSION(ILG,ILEV) ::Q !< Grid slice of model water vapour mixing ratio on model levels \f$[g/g]\f$
      REAL  , DIMENSION(ILG,ILEV) ::T !< Grid slice of model prognostic temperature on model levels \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::TC !<Parcel temperature \f$[K]\f$
      REAL  , DIMENSION(ILG,ILEV) ::QC !<Variable description\f$[units]\f$
      REAL  , DIMENSION(ILG,ILEV) ::QSTC !<Saturation mixing ratio for the parcel \f$[units]\f$
      REAL  , DIMENSION(ILG,ILEV) ::Z !< Height above the local column surface of model half levels (centers of layers) \f$[m]\f$
      REAL  , DIMENSION(ILG,ILEV) ::PF !< Grid slice of pressure on model layer interface levels \f$[mb]\f$
      REAL  , DIMENSION(ILG,ILEV) ::P !< Grid slice of pressure on model levels \f$[mb]\f$
      REAL  , DIMENSION(ILG,ILEV) ::HMN !<Moist static energy of the mean background air for the column \f$[units]\f$

      REAL  , DIMENSION(ILG) ::TL    !<Parcel temperature at the LCL \f$[K]\f$
      REAL  , DIMENSION(ILG) ::CAPE !< Convective available potential energy defined to include the entire region between
                                    !! the MAXI level and the LNBVariable description \f$[J/kg]\f$
      REAL  , DIMENSION(ILG) ::CAPEP !<The contribution to the value of CAPE associated with the region above the LCL \f$[J/kg]\f$
      REAL  , DIMENSION(ILG) ::PRESSG !< Grid slice of surface pressure \f$[Pa]\f$
      REAL  , DIMENSION(ILG) ::PBLT !<Index of the level closest to the top of the boundary layer \f$[1]\f$

      INTEGER, DIMENSION(ILG) ::LCL !< The lifting condensation level for an ascending, undiluted parcel of air initiated 
                                    !! at the MAXI level \f$[1]\f$
      INTEGER, DIMENSION(ILG) ::LNB !< The level of neutral buoyancy \f$[1]\f$
      INTEGER, DIMENSION(ILG) ::MAXI !< The model level at which convection is initiated \f$[1]\f$
      INTEGER, DIMENSION(ILG) ::LPBL !< The level corresponding to the top of the planetary boundary layer \f$[1]\f$

C
C     * INTERNAL WORK FIELDS (THE FIRST SET ARE PROMOTED TO
C     * INTEGRATE WITH THE NEW TINCLD3, IE FROM SHALLOW):
C
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


      REAL*8, DIMENSION(ILG,ILEV) :: TC8,TDRY,HMNC,QC8,QSTC8,Z8,P8
      REAL, DIMENSION(ILG,ILEV)   :: WC,BUOY,FACT
      REAL, DIMENSION(ILG)        :: QM,HMNM,MAXGR
C
      INTEGER,DIMENSION(ILG)      :: ISKIP,IDOC,LTM
C
      DATA ZERO    / 0.     /

C-----------------------------------------------------------------------
      DO 20 IL=IL1,IL2                      
         LPBL (IL)=NINT(PBLT(IL))                                
         MAXI (IL)=ILEV
         IDOC (IL)=1
         ISKIP(IL)=0
         MAXGR(IL)=9.E+20
         CAPE (IL)=0.
         CAPEP(IL)=0.
  20  CONTINUE
C
      DO 100 L=1,ILEV
      DO 100 IL=IL1,IL2  
         HMN(IL,L)=CPRES*T(IL,L)+GRAV*Z(IL,L)+RRL*Q(IL,L)
 100  CONTINUE    
C
      DO 120 IL=IL1,IL2                                                     
         QM  (IL)=Q   (IL,MAXI(IL))
         HMNM(IL)=HMN (IL,MAXI(IL))
 120  CONTINUE
C
C    * CALCULATE TEMPERATURE OF UNDILUTED AIR PARCEL.
C
      DO 140 L=MSG+1,ILEV 
      DO 140 IL=IL1,IL2 
         QC  (IL,L)=QM  (IL)
         HMNC(IL,L)=HMNM(IL)
 140  CONTINUE
C
      DO L=1,ILEV
      DO IL=IL1,IL2
        Z8   (IL,L)=Z   (IL,L)
        P8   (IL,L)=P   (IL,L)
        QC8  (IL,L)=QC  (IL,L)
      ENDDO
      ENDDO
C 
      CALL TINCLD3(TC8,ILG,ILEV,MSG-1,IL1,IL2,MAXI,TDRY,HMNC,QC8,QSTC8,
     1             Z8,P8,RRL,GRAV,CPRES,EPS1,EPS2,A,B,ISKIP)
C
      DO L=1,ILEV
      DO IL=IL1,IL2
        TC  (IL,L)=TC8  (IL,L)
        QSTC(IL,L)=QSTC8(IL,L)
      ENDDO
      ENDDO
C
C    * BUOYANCY OF UNDIULTED AIR PARCEL.
C
      DO 200 L=MSG+1,ILEV
      DO 200 IL=IL1,IL2 
         BUOY(IL,L)=-1.E-10
 200  CONTINUE
C
      L=ILEV
      DO 210 IL=IL1,IL2 
         IF ( L.LE.MAXI(IL) ) THEN
             TV =T(IL,L)*(1.+TVFA*Q(IL,L))
             TPV= TC(IL,L)*(1.+TVFA*QC(IL,L)
     1           -(1.+TVFA)*MAX(QC(IL,L)-QSTC(IL,L),ZERO))
             BUOY(IL,L)=(TPV-TV)/TV
             FACT(IL,L)=RGAS*BUOY(IL,L)*T(IL,L)
     1                 *LOG(0.01*PRESSG(IL)/PF(IL,L))
             BUOY(IL,L)=GRAV*BUOY(IL,L)
         ENDIF
 210  CONTINUE
      DO 220 L=MSG+1,ILEVM
      DO 220 IL=IL1,IL2 
         IF ( L.LE.MAXI(IL) ) THEN
             TV =T(IL,L)*(1.+TVFA*Q(IL,L))
             TPV= TC(IL,L)*(1.+TVFA*QC(IL,L)
     1           -(1.+TVFA)*MAX(QC(IL,L)-QSTC(IL,L),ZERO))
             BUOY(IL,L)=(TPV-TV)/TV
             FACT(IL,L)=RGAS*BUOY(IL,L)*T(IL,L)
     1                 *LOG(PF(IL,L+1)/PF(IL,L))
             BUOY(IL,L)=BUOY(IL,L)*GRAV
         ENDIF
 220  CONTINUE
C
C    * DETERMINE CLOUD BASE AS LIFTING CONDENSATION LEVEL (LCL).
C
      DO 240 IL=IL1,IL2
         LCL(IL)=MSG+1
         LTM(IL)=0
 240  CONTINUE
      DO 250 L=ILEVM,MSG+1,-1
      DO 250 IL=IL1,IL2
         IF (     L.LT.MAXI(IL) 
     1      .AND. QC(IL,L  ).GE.QSTC(IL,L  )   
     3      .AND. LTM(IL).EQ.0      ) THEN
            LTM(IL)=1
            LCL(IL)=L
         ENDIF
 250  CONTINUE
C
C     * DETERMINE CLOUD TOP AS LEVEL OF NEUTRAL BUOYANCY (LNB).
C
C
C     * VERTICAL VELOCITY OF PARCEL.  
C
      ADEF=-9.
      DO 295 L=MSG+1,ILEV                                                       
      DO 295 IL=IL1,IL2
         WC(IL,L)=ADEF
 295  CONTINUE
      DO 300 IL=IL1,IL2
         L=LCL(IL)
         TL(IL)=TC(IL,L)
         WC(IL,L)=0.4
 300  CONTINUE
      DO 310 L=ILEVM,MSG+1,-1 
      DO 310 IL=IL1,IL2 
         IF ( L.LT.LCL(IL) .AND. WC(IL,L+1).GE. 0.            ) THEN
            DZ=Z(IL,L)-Z(IL,L+1)
            DXDG=( BUOY(IL,L)-BUOY(IL,L+1) )/DZ
            ATMP=(BUOY(IL,L+1)-DXDG*Z(IL,L+1))*DZ
     1          +.5*DXDG*(Z(IL,L)**2-Z(IL,L+1)**2)
            ATMP=WC(IL,L+1)**2+2.*ATMP
            WC(IL,L)=SIGN(SQRT(ABS(ATMP)),ATMP)
         ENDIF
 310  CONTINUE
C
C     * ASSIGN TOP OF CLOUD LAYER TO LEVEL OF NEUTRAL BUOYANCY (LNB).
C     * THE LNB IS DEFINED AS THE FIRST LEVEL AT WHICH THE CLOUDY AIR
C     * IS NEUTRALLY BUOYANT BELOW THAT LEVEL AT WHICH THE MEAN VERTICAL
C     * VELOCITY IN THE CLOUD DROPS TO ZERO. 
C
      LDEF=ILEV
      DO 500 IL=IL1,IL2
         LNB(IL)=LDEF
         LTM(IL)=LDEF
 500  CONTINUE
      DO 520 L=ILEVM,MSG+1,-1
      DO 520 IL=IL1,IL2 
         IF ( L.LT.LCL(IL) .AND. LNB(IL).EQ.LDEF ) THEN
            IF ( WC(IL,L).LE.0. .AND.WC(IL,L+1).GT.0. ) THEN
               IF ( LTM(IL).NE.LDEF ) THEN
                  LNB(IL)=LTM(IL)
               ELSE
                  LNB(IL)=L
               ENDIF
            ELSE IF ( BUOY(IL,L).LE.0. .AND. BUOY(IL,L+1).GT.0. ) THEN
               LTM(IL)=L
            ENDIF
         ENDIF
 520  CONTINUE
      DO 540 IL=IL1,IL2 
         IF ( LNB(IL).EQ.LDEF ) THEN
            IF ( LTM(IL).NE.LDEF ) THEN 
               LNB(IL)=LTM(IL)
            ELSE
               LNB(IL)=ILEV+1
            ENDIF
         ENDIF
 540  CONTINUE
C                                                                            
C     * CALCULATE CONVECTIVE AVAILABLE POTENTIAL ENERGY (CAPE).             
C                    
      DO 600 L=MSG+1,ILEV                                                   
      DO 600 IL=IL1,IL2                                                       
        IF(IDOC(IL).NE.0 .AND. L.LT.MAXI(IL) .AND. L.GE.LNB(IL)) THEN         
           CAPE(IL)=CAPE(IL)+FACT(IL,L)
           IF ( L.LE.LCL(IL) ) CAPEP(IL)=CAPEP(IL)+FACT(IL,L)
        ENDIF                                                                
  600 CONTINUE
C
      RETURN
      END

!> \file
!! \section sec_theory Theoretical formulation
!!\n 
!! The basic definition of CAPE is as follows:
!!\n
!! \f{equation}{
!! CAPE=\intop_{zb}^{zt}\frac{g(T_{v}^{(p)}-\bar{T_{v})}}{\bar{T_{v}}}dz \tag{1}
!! \f}
!!\n
!! The integrand in this formula is the buoyancy (\f$B^{(p)})\f$of a parcel
!! of air which undergoes undiluted adiabatic ascent between the base
!! level (\f$z_{b})\f$ and uppermost level (\f$z_{t})\f$ may be reached by
!! moists onvection. This level is typically the level of nuetral buoyancy
!! (LNB) for conditionally unstable atmospheric column.
!!\n 
!! The virtual temperature (\f$T_{v})\f$ is defined using a standard approximation
!! as \f$T_{v}\simeq T[1+(\frac{1}{\epsilon}-1)r_{v}-r_{c}]\simeq T(1+.61r_{v}-r_{c})\f$,
!! where \f$\epsilon\simeq.622\f$ is the ratio of the gas constants for
!! dry air (\f$R_{d})\f$ and water vapour (\f$R_{v})\f$. (ref: \textbf{AMS
!! Glossary of Meteorology; http://glossary.ametsoc.org/wiki/Virtual\_temperature}).
!! The remaining quantities in the above formulae are defined as follows:
!!\n 
!! \f$r_{v},r_{c}=r_{l}+r_{i}\f$: respectively mixing ratios of water vapour
!! and condensed [liquid (\f$r_{l})\f$ plus solid (\f$r_{i})\f$] water.
!!\n 
!! \f$g(=9.8m/s^{2}):\f$ acceleration due to gravity.
!!\n 
!! \f$z_{b},z_{c}:\f$ respectively the base and top levels of the convective
!! layer.
!!\n 
!! \f$T_{v}^{(p)}\f$: the virtual temperature associated with a parcel of
!! air lifted adiabatically from the base level (\f$z_{b})\f$ taking into
!! accout the effects of condensation and latent heat release. The ascent
!! is assumed to be reversible (i.e. all of the condensate generated
!! in the ascent is carried upward with the parcel).
!!\n 
!! \f$\bar{T_{v}}\f$: the virtual temperature of the background (mean state)
!! air (evuated from input values of the background temperature and water
!! vapour mixing ratio).
!!\n 
!! \subsection ssec_conservation Conservation assumptions and determination of the parcel properties
!!\n 
!! In the operational formulation it is assumed that the moist static
!! energy (\f$h^{(p)}=c_{p}T^{(p)}+L_{v}r_{v}^{(p)}+gz)\f$ and total water
!! (vapour plus condensate: \f$r_{t}=r_{v}+r_{c}\f$), of the parcel are
!! conserved during the lifting process, where \f$c_{p}\f$ is the specifice
!! heat at constant pressure, \f$L_{v}\f$ is the latent heat of vapourization.
!! Conservation of moist static energy is a \textit{simplifying assumption}
!! (see \textbf{Romps, 2015, DOI: 10.1175/JAS-D-15-0054.1} ). Note that
!! the moist static energy can be decomposed as the sum of the dry static
!! energy (\f$s^{(p)}=c_{p}T^{(p)}+gz)\f$ and the latent heat term (\f$L_{v}r_{v}^{(p)})\f$.
!! It is assumed that these quantities are separately conserved for adiabatic
!! ascent of unsaturated air.
!!\n 
!! In the operational formulation these quantities are initialized to
!! the background values at the base level, i.e.
!!\n
!! \f{equation}{
!! h^{(p)}=\bar{h}(z_{b}) \tag{2}
!! \f}
!!\n
!! \f{equation}{
!! r_{t}^{(p)}=\bar{r}(z_{b}) \tag{3}
!! \f}
!!\n
!! where the overbar denotes environmental values. These quatitied are
!! independant of height under the conservationnassumption. However the
!! parcel properties (temperature, water vapour mixing ratio, and condensed
!! water mixing ratio) do vary in the vertical. The parcel is typically
!! unsaturated at the base level but becomes saturated at the lifting
!! condensation level (LCL) and posotovely buoyant (\f$T_{v}^{(p)}>\bar{T}_{v}\f$
!! ) at the level of free convetion (LFC). At any level the temperature,
!! specific humidity, and water content of the parcel are evaluated in tincld3.f.
!!\n 
!! \subsection ssec_lnb Level of nuetral buoyancy, cloud base, cloud top, parcel vertical velocity}
!!\n 
!! Once the parcel properties have been determined via the subroutine
!! TINCLD3, the parcel buoyancy is readily determined. This quantitly
!! will typically be close to zero within a dry daytime planetery boundary
!! layer (PBL) where the background temperature is structure is close
!! to dry-adiabatic and the water vapour mixing ratio nearly homogeneous
!! (i.e. characteristic of the well mixed turbulent part of the PBL),
!! and negative within the stable stratified region between the well-mixed
!! part and the LFC. In general, however the backgound atmosphere is
!! stable stratified in the region above the LFC in suvh a way that the
!! parcel buoyancy eventually becomes negative again and remains so on
!! further lifting. The level at which the parcel buoyancy becomes negative
!! and remains so is usually referred to as the level of nuetral buoynancy
!! (LNB).
!!\n
!!\n
!! However pecause the background virtual temperature varies with heigh
!! there may be layers of limited vertical extent where the parcel buoyancy
!! is negative but flanked by layers where it is positive. In principle
!! a positively buoyant ascending parcel that enters sugh a layer from
!! below may have sufficient vertical momentum to ascend through the
!! negatively buoyant region. To account for this the following simple
!! equation for the vertical velocity is used:
!!\n
!! \f{equation}{
!! \frac{\partial}{\partial z}\left(\frac{w_{(p)}^{2}}{2}\right)=B^{(p)} \tag{4}
!! \f}
!!\n
!! This equation is intergrated upward level by level numerically between
!! the LCL and higher levels. The parcel vertical velocity (\f$w_{(p)})\f$
!! is set to an arbitrary positive value ( default value = 0.4 m/s )
!! at the LCL. All levels above this level where the vertical velocity
!! remins positive are regarded as potentially part of the region within
!! the atmospheric column where moist convection may be active. The operational
!! LNB (\f$z_{t})\f$is assumed to lie below the level where the \f$w_{(p)}\f$
!! becomes negative. It is defined to be located at the top of the highest
!! layer that is nuetrally or positivly buoyant below this level.

