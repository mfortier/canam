      SUBROUTINE CNCDNCB(CECDNC,CICDNC,CEWETRB,CIWETRB,PEN0,PEPHI0, &
                         PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0,PIPSI, &
                         PIPHIS0,PIDPHI0,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DIAGNOSIS OF CLOUD DROPLET CONCENTRATION (CDNC) BASED ON WET SIZE
!     DISTRIBUTION AND THRESHOLD DROPLET SIZE. THE CDNC IS CALCULATED
!     AS THE CUMULATIVE DROPLET CONCENTRATION FOR ALL DROPLETS BIGGER
!     THAN THE THRESHOLD SIZE.
!
!     HISTORY:
!     --------
!     * NOV 13/2008 - K.VONSALZEN   INTERPOLATION OF SIZE DISTRIBUTION
!                                   NEAR ACTIVATION SIZE BASED ON
!                                   QUADRATIC RELATIONSHIP.
!     * JUN 20/2008 - K.VONSALZEN   BUG FIX FOR KAPPA MODEL FOR
!                                   EXTERNALLY MIXED SPECIES.
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
      USE SCPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, PARAMETER :: RCLDD=1.0E-06        ! DROPLET SIZE THRESHOLD
      REAL, PARAMETER :: ALPHA=2.             ! SHAPE PARAMETER FOR
                                              ! WET SIZE DISTRIBUTION
                                              ! NEAR ACTIVATION SIZE
!
      INTEGER, INTENT(IN) :: ILGA,LEVA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXTB) :: CEWETRB
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINTB) :: CIWETRB
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                               PEN0,PEPHI0,PEPSI, &
                                               PEPHIS0,PEDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                               PIN0,PIPHI0,PIPSI, &
                                               PIPHIS0,PIDPHI0
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: CICDNC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,KEXT) :: CECDNC
      LOGICAL, ALLOCATABLE, DIMENSION(:,:) :: KFRST
      LOGICAL, ALLOCATABLE, DIMENSION(:,:,:) :: TIACT,TEACT
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: CIISI
      INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: CEISI
      REAL, ALLOCATABLE, DIMENSION(:,:) :: CIRCI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TIDPHIS,TIPHISS, &
                                             TISDF,TINUM,TIPN0,TIPHI0, &
                                             TIPSI, &
                                             CERCI,TEDPHIS,TEPHISS, &
                                             TESDF,TENUM,TEPN0,TEPHI0, &
                                             TEPSI
!
!-----------------------------------------------------------------------
!     * ALLOCATE WORK ARRAYS.
!
      IF ( KEXT > 0 ) THEN
        ALLOCATE(KFRST(ILGA,LEVA))
        ALLOCATE(CERCI(ILGA,LEVA,KEXT))
        ALLOCATE(CEISI(ILGA,LEVA,KEXT))
      ENDIF
      IF ( ISWEXT > 0 ) THEN
        ALLOCATE(TEPN0  (ILGA,LEVA,ISWEXT))
        ALLOCATE(TEPHI0 (ILGA,LEVA,ISWEXT))
        ALLOCATE(TEPSI  (ILGA,LEVA,ISWEXT))
        ALLOCATE(TEDPHIS(ILGA,LEVA,ISWEXT))
        ALLOCATE(TEPHISS(ILGA,LEVA,ISWEXT))
        ALLOCATE(TESDF  (ILGA,LEVA,ISWEXT))
        ALLOCATE(TENUM  (ILGA,LEVA,ISWEXT))
        ALLOCATE(TEACT  (ILGA,LEVA,ISWEXT))
      ENDIF
      IF ( KINT > 0 ) THEN
        ALLOCATE(CIRCI (ILGA,LEVA))
        ALLOCATE(CIISI (ILGA,LEVA))
      ENDIF
      IF ( ISWINT > 0 ) THEN
        ALLOCATE(TIPN0  (ILGA,LEVA,ISWINT))
        ALLOCATE(TIPHI0 (ILGA,LEVA,ISWINT))
        ALLOCATE(TIPSI  (ILGA,LEVA,ISWINT))
        ALLOCATE(TIDPHIS(ILGA,LEVA,ISWINT))
        ALLOCATE(TIPHISS(ILGA,LEVA,ISWINT))
        ALLOCATE(TISDF  (ILGA,LEVA,ISWINT))
        ALLOCATE(TINUM  (ILGA,LEVA,ISWINT))
        ALLOCATE(TIACT  (ILGA,LEVA,ISWINT))
      ENDIF
!
!-----------------------------------------------------------------------
!     * MATCH PARTICLE SIZE WITH SPECIFIED SIZE CUTOFF TO DETERMINE LOWER
!     * BOUND OF CLOUD DROPLET SIZE SPECTRUM FOR EACH EXTERNALLY MIXED
!     * AEROSOL TYPE.
!
      IF ( ISEXTB > 0 ) THEN
        CERCI=YLARGE
        CEISI=INA
        KFRST=.TRUE.
        TEACT=.FALSE.
        DO IS=1,ISEXTB-1
          IF ( CEKX(IS) == CEKX(IS+1) ) THEN
            DO L=1,LEVA
            DO IL=1,ILGA
              IF ( KFRST(IL,L) ) THEN
                IF ( CEWETRB(IL,L,IS) > RCLDD  ) THEN
                  CEISI(IL,L,CEKX(IS))=CEIS(IS)
                  CERCI(IL,L,CEKX(IS))=CEDRYRB(IS)
                ENDIF
                KFRST(IL,L)=.FALSE.
              ENDIF
              IF (       RCLDD>=CEWETRB(IL,L,IS  ) &
                   .AND. RCLDD<=CEWETRB(IL,L,IS+1) &
                   .AND. CEISI(IL,L,CEKX(IS))==INA               ) THEN
                CEISI(IL,L,CEKX(IS))=CEIS(IS)
                APX=MIN(MAX((RCLDD-CEWETRB(IL,L,IS)) &
                   /(CEWETRB(IL,L,IS+1)-CEWETRB(IL,L,IS)),0.),1. &
                                                          )**(1./ALPHA)
                CERCI(IL,L,CEKX(IS))=APX*CEDRYRB(IS+1) &
                                                  +(1.-APX)*CEDRYRB(IS)
              ENDIF
!
!             * CHECK IF CONDENSATE PRESENT.
!
              IF (      CEWETRB(IL,L,IS  )>CEDRYRB(IS  ) &
                   .OR. CEWETRB(IL,L,IS+1)>CEDRYRB(IS+1)         ) THEN
                TEACT(IL,L,CEIS(IS))=.TRUE.
              ENDIF
            ENDDO
            ENDDO
          ELSE
            KFRST(:,:)=.TRUE.
          ENDIF
        ENDDO
      ENDIF
!-----------------------------------------------------------------------
!     * MATCH PARTICLE SIZE WITH SPECIFIED SIZE CUTOFF TO DETERMINE LOWER
!     * BOUND OF CLOUD DROPLET SIZE SPECTRUM FOR EACH INTERNALLY MIXED
!     * AEROSOL TYPE.
!
      IF ( ISINTB > 0 ) THEN
        CIRCI=YLARGE
        CIISI=INA
!
!       * SET ACTIVATION SIZE TO SIZE OF SMALLEST PARTICLE
!       * IF SUPERSATURATION LARGE ENOUGH TO ACTIVATE THESE
!       * PARTICLES.
!
        IS=1
        DO L=1,LEVA
        DO IL=1,ILGA
          IF ( CIWETRB(IL,L,IS) > RCLDD .AND. CIISI(IL,L)==INA   ) THEN
            CIISI(IL,L)=CIIS(IS)
            CIRCI(IL,L)=CIDRYRB(IS)
          ENDIF
        ENDDO
        ENDDO
        TIACT=.FALSE.
        DO IS=1,ISINTB-1
        DO L=1,LEVA
        DO IL=1,ILGA
          IF (       RCLDD>=CIWETRB(IL,L,IS  ) &
               .AND. RCLDD<=CIWETRB(IL,L,IS+1) &
               .AND. CIISI(IL,L)==INA                            ) THEN
            CIISI(IL,L)=CIIS(IS)
            APX=MIN(MAX((RCLDD-CIWETRB(IL,L,IS)) &
               /(CIWETRB(IL,L,IS+1)-CIWETRB(IL,L,IS)),0.),1. &
                                                          )**(1./ALPHA)
            CIRCI(IL,L)=APX*CIDRYRB(IS+1)+(1.-APX)*CIDRYRB(IS)
          ENDIF
!
!         * CHECK IF CONDENSATE PRESENT.
!
          IF (      CIWETRB(IL,L,IS  )>CIDRYRB(IS  ) &
               .OR. CIWETRB(IL,L,IS+1)>CIDRYRB(IS+1)             ) THEN
            TIACT(IL,L,CIIS(IS))=.TRUE.
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * PLA PARAMETERS FOR EXTERNALLY MIXED AEROSOL TYPES.
!
      DO IS=1,ISWEXT
        ISM=SEXTF%ISWET(IS)%ISM
        TEPN0  (:,:,IS)=PEN0   (:,:,ISM)
        TEPHI0 (:,:,IS)=PEPHI0 (:,:,ISM)
        TEPSI  (:,:,IS)=PEPSI  (:,:,ISM)
        TEDPHIS(:,:,IS)=PEDPHI0(:,:,ISM)
        TEPHISS(:,:,IS)=PEPHIS0(:,:,ISM)
      ENDDO
!
!     * PLA PARAMETERS FOR INTERNALLY MIXED AEROSOL TYPES.
!
      DO IS=1,ISWINT
        ISM=SINTF%ISWET(IS)%ISI
        TIPN0  (:,:,IS)=PIN0   (:,:,ISM)
        TIPHI0 (:,:,IS)=PIPHI0 (:,:,ISM)
        TIPSI  (:,:,IS)=PIPSI  (:,:,ISM)
        TIDPHIS(:,:,IS)=PIDPHI0(:,:,ISM)
        TIPHISS(:,:,IS)=PIPHIS0(:,:,ISM)
      ENDDO
!
!     * COPY SIZE INFORMATION FOR ACTIVATION SIZE INTO SIZE INFORMATION
!     * ARRAYS FOR EXTERNALLY MIXED AEROSOL.
!
      IF ( ISWEXT > 0 ) THEN
        DO KX=1,KEXT
        DO L=1,LEVA
        DO IL=1,ILGA
          IS=CEISI(IL,L,KX)
          IF ( IS /= INA ) THEN
            PHISRT=TEPHISS(IL,L,IS)+TEDPHIS(IL,L,IS)
            PHISST=MIN(MAX(LOG(CERCI(IL,L,KX)/R0),TEPHISS(IL,L,IS)), &
                                                                PHISRT)
            DPHIST=PHISRT-PHISST
            TEPHISS(IL,L,IS)=PHISST
            TEDPHIS(IL,L,IS)=DPHIST
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!     * COPY SIZE INFORMATION FOR ACTIVATION SIZE INTO SIZE INFORMATION
!     * ARRAYS FOR INTERNALLY MIXED AEROSOL.
!
      IF ( ISWINT > 0 ) THEN
        DO L=1,LEVA
        DO IL=1,ILGA
          IS=CIISI(IL,L)
          IF ( IS /= INA ) THEN
            PHISRT=TIPHISS(IL,L,IS)+TIDPHIS(IL,L,IS)
            PHISST=MIN(MAX(LOG(CIRCI(IL,L)/R0),TIPHISS(IL,L,IS)), &
                                                                PHISRT)
            DPHIST=PHISRT-PHISST
            TIPHISS(IL,L,IS)=PHISST
            TIDPHIS(IL,L,IS)=DPHIST
          ENDIF
        ENDDO
        ENDDO
      ENDIF
!
!     * OBTAIN NUMBER CONCENTRATIONS FROM INTEGRATION OF SIZE DISRIBUTION
!     * FOR EXTERNALLY MIXED AEROSOLS.
!
      IF ( ISWEXT > 0 ) THEN
        TESDF=SDINT0(TEPHI0,TEPSI,TEPHISS,TEDPHIS,ILGA,LEVA,ISWEXT)
        WHERE ( ABS(TEPSI-YNA) > YTINY )
          TENUM=TEPN0*TESDF
        ELSEWHERE
          TENUM=0.
        ENDWHERE
      ENDIF
!
!     * OBTAIN NUMBER CONCENTRATIONS FROM INTEGRATION OF SIZE DISRIBUTION
!     * FOR INTERNALLY MIXED AEROSOLS.
!
      IF ( ISWINT > 0 ) THEN
        TISDF=SDINT0(TIPHI0,TIPSI,TIPHISS,TIDPHIS,ILGA,LEVA,ISWINT)
        WHERE ( ABS(TIPSI-YNA) > YTINY )
          TINUM=TIPN0*TISDF
        ELSEWHERE
          TINUM=0.
        ENDWHERE
      ENDIF
!
!-----------------------------------------------------------------------
!     * CLOUD DROPLET NUMBER CONCENTRATION FROM INTEGRATION OF NUMBER
!     * SIZE DISTRIBUTION FROM CRITICAL SIZE TO SIZE OF LARGEST PARTICLE
!     * IN THE SIZE DISTRIBUTION. FOR EXTERNALLY MIXED AEROSOLS.
!
      IF ( KEXT > 0 ) THEN
        CECDNC=0.
      ENDIF
      ISI=0
      DO KX=1,KEXT
        IF (     (ABS(AEXTF%TP(KX)%NUIO -YNA) > YTINY &
                                      .AND. AEXTF%TP(KX)%NUIO  > YTINY) &
            .OR. (ABS(AEXTF%TP(KX)%KAPPA-YNA) > YTINY &
                               .AND. AEXTF%TP(KX)%KAPPA > YTINY) ) THEN
          DO IS=1,AEXTF%TP(KX)%ISEC
            ISI=ISI+1
            DO L=1,LEVA
            DO IL=1,ILGA
              ISX=CEISI(IL,L,KX)
              IF ( ISX /= INA ) THEN
                ISC=SEXTF%ISWET(ISX)%ISI
                IF ( IS >= ISC .AND. TEACT(IL,L,ISI) ) THEN
                  CECDNC(IL,L,KX)=CECDNC(IL,L,KX)+TENUM(IL,L,ISI)
                ENDIF
              ENDIF
            ENDDO
            ENDDO
          ENDDO
        ENDIF
      ENDDO
!
!     * CLOUD DROPLET NUMBER CONCENTRATION FROM INTEGRATION OF NUMBER
!     * SIZE DISTRIBUTION. FOR INTERNALLY MIXED AEROSOLS.
!
      IF ( KINT > 0 ) THEN
        CICDNC=0.
      ENDIF
      DO IS=1,ISWINT
      DO L=1,LEVA
      DO IL=1,ILGA
        ISX=CIISI(IL,L)
        IF ( ISX /= INA ) THEN
          IF ( IS >= ISX .AND. TIACT(IL,L,IS) ) THEN
            CICDNC(IL,L)=CICDNC(IL,L)+TINUM(IL,L,IS)
          ENDIF
        ENDIF
      ENDDO
      ENDDO
      ENDDO
!-----------------------------------------------------------------------
!     * DEALLOCATE WORK ARRAYS.
!
      IF ( KEXT > 0 ) THEN
        DEALLOCATE(KFRST)
        DEALLOCATE(CERCI)
        DEALLOCATE(CEISI)
      ENDIF
      IF ( ISWEXT > 0 ) THEN
        DEALLOCATE(TEPN0)
        DEALLOCATE(TEPHI0)
        DEALLOCATE(TEPSI)
        DEALLOCATE(TEDPHIS)
        DEALLOCATE(TEPHISS)
        DEALLOCATE(TESDF)
        DEALLOCATE(TENUM)
        DEALLOCATE(TEACT)
      ENDIF
      IF ( KINT > 0 ) THEN
        DEALLOCATE(CIRCI)
        DEALLOCATE(CIISI)
      ENDIF
      IF ( ISWINT > 0 ) THEN
        DEALLOCATE(TIPN0)
        DEALLOCATE(TIPHI0)
        DEALLOCATE(TIPSI)
        DEALLOCATE(TIDPHIS)
        DEALLOCATE(TIPHISS)
        DEALLOCATE(TISDF)
        DEALLOCATE(TINUM)
        DEALLOCATE(TIACT)
      ENDIF
!
      END SUBROUTINE CNCDNCB
