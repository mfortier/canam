      SUBROUTINE AVGTND(SCF,PIDNDT,PIDMDT,PIDFDT,PNVB,PSVB,PNCB, &
                        PSCB,PNMB,PSMB,PINUM,PIMAS,PIFRC,DT, &
                        KOUNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     TIME-AVERAGED TENDENCIES FOR INTERNALLY MIXED AEROSOL SPECIES.
!     INSTANTANEOUSLY CALCULATED TENDENCIES ARE AVERAGED AND SAVED.
!     RESULTS ARE SUBSEQUENTLY SCALED TO ALLOW UPDATES TO PAM TRACERS
!     ON A PER-TIME-STEP BASIS.
!
!     HISTORY:
!     --------
!     * DEC 29/2010 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: SCF
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIDNDT,PIDMDT
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: &
                                                   PIDFDT
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT) :: PNVB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD) :: &
                                                          PNMB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PSVB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD) :: &
                                                          PSMB
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD) :: &
                                                          PNCB
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD):: &
                                                          PSCB
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: PINUM,PIMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: &
                                                   PIFRC
!
      REAL, DIMENSION(ILGA,LEVA,ISAINT) :: PNVM
      REAL, DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PSVM
      REAL, DIMENSION(ILGA,LEVA,ISAINT,NRMFLD) :: PNMM
      REAL, DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD) :: PSMM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PIMAST
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PIFRCT,PIDSDT,PISAST
      INTEGER, DIMENSION(ILGA,LEVA,ISAINT,NRMFLD) :: PNCM
      INTEGER, DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD) :: PSCM
!
!-----------------------------------------------------------------------
!
      IF ( ISAINT > 0 ) THEN
        ALLOCATE(PIMAST (ILGA,LEVA,ISAINT))
        ALLOCATE(PIFRCT (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIDSDT (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PISAST (ILGA,LEVA,ISAINT,KINT))
      ENDIF
!
      IF ( MOD(KOUNT,ICFRQ) == 0 ) THEN
!
!       * AEROSOL MASS TENDENCIES FOR INDIVIDUAL SPECIES.
!
        PIMAST=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRCT=MAX(PIFRC+DT*PIDFDT,0.)
        DO K=1,KINT
          WHERE ( ABS(PIMAS(:,:,:)-YNA) > YTINY )
            PIDSDT(:,:,:,K)=(PIFRCT(:,:,:,K)*PIMAST(:,:,:) &
                            -PIFRC (:,:,:,K)*PIMAS (:,:,:))/DT
          ELSEWHERE
            PIDSDT(:,:,:,K)=YNA
          ENDWHERE
        ENDDO
      ELSE
        PIDNDT=YNA
        PIDSDT=YNA
      ENDIF
!
!     * RETRIEVE MEAN AND ACCUMULATED RESULT AND COUNTER FROM
!     * PREVIOUS TIME STEP.
!
      PNVM=PNVB
      PSVM=PSVB
!
      PNCM=PNCB
      PSCM=PSCB
!
      PNMM=PNMB
      PSMM=PSMB
!
!     * LOOP OVER ALL FIELDS INVOLVED IN CALCULATION OF RUNNING
!     * MEAN RESULTS.
!
      NRMFLDL=CEILING(REAL(IAVGPRD)/REAL(IUPDATP))
      DO IND=1,NRMFLDL
        IOFF=(KOUNT+IAVGPRD-1)-(IND-1)*IUPDATP
        IF ( IOFF >= 1 .AND. MOD(IOFF,IAVGPRD)==0 ) THEN
!
!         * RUNNING MEAN TENDENCIES.
!
          WHERE ( PNCM(:,:,:,IND) > 0 )
            PNVM(:,:,:)=PNMM(:,:,:,IND)/REAL(PNCM(:,:,:,IND))
          ELSEWHERE
            PNVM(:,:,:)=YNA
          ENDWHERE
!
          WHERE ( PSCM(:,:,:,:,IND) > 0 )
            PSVM(:,:,:,:)=PSMM(:,:,:,:,IND)/REAL(PSCM(:,:,:,:,IND))
          ELSEWHERE
            PSVM(:,:,:,:)=YNA
          ENDWHERE
!
!         * RESET ACCUMULATED RESULT AND COUNTER.
!
          PNMM(:,:,:,  IND)=0.
          PSMM(:,:,:,:,IND)=0.
!
          PNCM(:,:,:,  IND)=0
          PSCM(:,:,:,:,IND)=0
        ENDIF
!
!       * ACCUMULATE INSTANTANEOUS RESULT AND ADVANCE COUNTER.
!
        WHERE ( ABS(PIDNDT(:,:,:)-YNA) > YTINY )
          PNMM(:,:,:,IND)=PNMM(:,:,:,IND)+PIDNDT(:,:,:)
          PNCM(:,:,:,IND)=PNCM(:,:,:,IND)+1
        ENDWHERE
!
        WHERE ( ABS(PIDSDT(:,:,:,:)-YNA) > YTINY )
          PSMM(:,:,:,:,IND)=PSMM(:,:,:,:,IND)+PIDSDT(:,:,:,:)
          PSCM(:,:,:,:,IND)=PSCM(:,:,:,:,IND)+1
        ENDWHERE
      ENDDO
!
!     * SAVE MEAN AND ACCUMULATED RESULTS AND COUNTER.
!
      PNVB=PNVM
      PSVB=PSVM
!
      PNCB=PNCM
      PSCB=PSCM
!
      PNMB=PNMM
      PSMB=PSMM
!
!     * RESTORE TENDENCIES TO MEAN VALUES.
!
      PIDNDT=PNVM
      PIDSDT=PSVM
!
!     * SCALE TENDENCIES TO AVOID NEGATIVE CONCENTRATIONS,
!     * IF NECESSARY
!
      DO IS=1,ISAINT
        DO K=1,KINT
          WHERE ( ABS(PIMAS(:,:,IS)-YNA) > YTINY &
                                     .AND. PIDSDT(:,:,IS,K) < -YSMALL )
            SCF(:,:)=MIN(SCF(:,:),MAX(0., &
                 -PIFRC(:,:,IS,K)*PIMAS(:,:,IS)/(DT*PIDSDT(:,:,IS,K))))
          ENDWHERE
        ENDDO
        WHERE ( ABS(PINUM(:,:,IS)-YNA) > YTINY &
                                       .AND. PIDNDT(:,:,IS) < -YSMALL )
          SCF(:,:)=MIN(SCF(:,:), &
                            MAX(0.,-PINUM(:,:,IS)/(DT*PIDNDT(:,:,IS))))
        ENDWHERE
      ENDDO
      DO IS=1,ISAINT
        DO K=1,KINT
          WHERE ( ABS(PIMAS(:,:,IS)-YNA) > YTINY )
            PIDSDT(:,:,IS,K)=SCF(:,:)*PIDSDT(:,:,IS,K)
            PISAST(:,:,IS,K)=MAX(PIFRC(:,:,IS,K)*PIMAS(:,:,IS) &
                                               +PIDSDT(:,:,IS,K)*DT,0.)
          ENDWHERE
        ENDDO
        WHERE ( ABS(PINUM(:,:,IS)-YNA) > YTINY )
          PIDNDT(:,:,IS)=SCF(:,:)*PIDNDT(:,:,IS)
        ELSEWHERE
          PIDNDT(:,:,IS)=0.
        ENDWHERE
      ENDDO
      WHERE ( ABS(PIMAS(:,:,:)-YNA) > YTINY  )
        PIMAST(:,:,:)=SUM(PISAST(:,:,:,:),DIM=4)
        PIDMDT(:,:,:)=(PIMAST(:,:,:)-PIMAS(:,:,:))/DT
      ELSEWHERE
        PIDMDT(:,:,:)=0.
      ENDWHERE
      DO K=1,KINT
        WHERE ( ABS(PIMAS(:,:,:)-YNA) > YTINY &
                                    .AND. ABS(PIMAST(:,:,:)) > YSMALL )
          PIDFDT(:,:,:,K)=(PISAST(:,:,:,K)/PIMAST(:,:,:) &
                                                    -PIFRC(:,:,:,K))/DT
        ELSEWHERE
          PIDFDT(:,:,:,K)=0.
        ENDWHERE
      ENDDO
!
      IF ( ISAINT > 0 ) THEN
        DEALLOCATE(PIMAST)
        DEALLOCATE(PIFRCT)
        DEALLOCATE(PIDSDT)
        DEALLOCATE(PISAST)
      ENDIF
!
      END SUBROUTINE AVGTND
