      SUBROUTINE PGRWTHC (TMAS,RESM,FRC,I0L,I0R, &
                          DP3L,DP4L,DP5L,DP6L,DP3R,DP4R,DP5R, &
                          DP6R,DP3S,DP4S,DP5S,DP6S, &
                          CGR1,CGR2,PN0,DRYDN,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PARTICLE GROWTH FROM IN-SITU CHEMICAL PRODUCTION OF AEROSOL MASS
!     WITH AN ASSOCIATED CHANGE IN PARTICLE RADIUS R -> C*R.
!
!     IT IS ASSUMED THAT SECTIONS INITIALLY HAVE THE SAME SIZE AND THAT
!     GROWTH LEADS TO A AN EQUAL OR SMALLER WIDTH OF EACH SECTION.
!     THE SUBROUTINE TREATS GROWTH IN PARTICLE SIZE SPACE BASED ON
!     A LAGRANGIAN APPROACH.
!
!     HISTORY:
!     --------
!     * SEP 27/2007 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL(R8), INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: TMAS
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: RESM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                    CGR1,CGR2,PN0,DRYDN,FRC
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                    DP3L,DP4L,DP5L,DP6L, &
                                    DP3R,DP4R,DP5R,DP6R, &
                                    DP3S,DP4S,DP5S,DP6S
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: I0L,I0R
      REAL(R8) :: C0,C1,C2,TP3L,TP4L,TP5L,TP6L,TP3R,TP4R,TP5R,TP6R, &
                  TP3S,TP4S,TP5S,TP6S
!
!-----------------------------------------------------------------------
!     * INITIALIZATION.
!
      TMAS=0.
!
!-----------------------------------------------------------------------
!     * CALCULATE NUMBER AND MASS IN EACH SECTION.
!
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
!
!        * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE LEFT-HAND
!        * SIDE PART OF THE MODIFIED SECTION.
!
         IS=I0L(IL,L,ISI)
         IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                                                                )  THEN
!
!          * MASS SIZE DISTRIBUTION FROM
!          * M(PHI(T))=F*(C*R(T=0))**3*N(PHI(T=0))
!
           CT=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*YCNST
           C0=CT*FRC(IL,L,ISI)
           C1=CGR1(IL,L,ISI)
           C2=CGR2(IL,L,ISI)
           TP3L=DP3L(IL,L,ISI)
           TP4L=DP4L(IL,L,ISI)
           TP5L=DP5L(IL,L,ISI)
           TP6L=DP6L(IL,L,ISI)
           IF ( ABS(C2) > YTINY ) THEN
             ATMP=3._R8*TP4L*C2*C1**2+3._R8*TP5L*C1*C2**2+TP6L*C2**3
           ELSE
             ATMP=0.
           ENDIF
           TMAS(IL,L,IS)=TMAS(IL,L,IS)+C0*TP3L+CT*(TP3L*(C1**3-1.) &
                                                                +ATMP)
         ENDIF
!
!        * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE RIGHT-HAND
!        * SIDE PART OF THE MODIFIED SECTION.
!
         IS=I0R(IL,L,ISI)
         IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
             .AND. I0L(IL,L,ISI) /= IS                          )  THEN
!
!          * MASS SIZE DISTRIBUTION FROM
!          * M(PHI(T))=F*(C*R(T=0))**3*N(PHI(T=0))
!
           CT=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*YCNST
           C0=CT*FRC(IL,L,ISI)
           C1=CGR1(IL,L,ISI)
           C2=CGR2(IL,L,ISI)
           TP3R=DP3R(IL,L,ISI)
           TP4R=DP4R(IL,L,ISI)
           TP5R=DP5R(IL,L,ISI)
           TP6R=DP6R(IL,L,ISI)
           IF ( ABS(C2) > YTINY ) THEN
             ATMP=3._R8*TP4R*C2*C1**2+3._R8*TP5R*C1*C2**2+TP6R*C2**3
           ELSE
             ATMP=0.
           ENDIF
           TMAS(IL,L,IS)=TMAS(IL,L,IS)+C0*TP3R+CT*(TP3R*(C1**3-1.) &
                                                                +ATMP)
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * RESIDUUM DUE TO PARTICLES GROWING TO SIZES LARGER THAN
!     * THE SPECTRUM CUTOFF.
!
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         IF ( (I0R(IL,L,ISI) == (ISEC+1) .OR. I0L(IL,L,ISI) == 0) &
            .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!          * MASS SIZE DISTRIBUTION FROM
!          * M(PHI(T))=F*(C*R(T=0))**3*N(PHI(T=0))
!
           CT=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*YCNST
           C0=CT*FRC(IL,L,ISI)
           C1=CGR1(IL,L,ISI)
           C2=CGR2(IL,L,ISI)
           TP3S=DP3S(IL,L,ISI)
           TP4S=DP4S(IL,L,ISI)
           TP5S=DP5S(IL,L,ISI)
           TP6S=DP6S(IL,L,ISI)
           IF ( ABS(C2) > YTINY ) THEN
             ATMP=3._R8*TP4S*C2*C1**2+3._R8*TP5S*C1*C2**2+TP6S*C2**3
           ELSE
             ATMP=0.
           ENDIF
           RESM(IL,L)=RESM(IL,L)+C0*TP3S+CT*(TP3S*(C1**3-1.)+ATMP)
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
      END SUBROUTINE PGRWTHC
