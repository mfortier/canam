      SUBROUTINE CNRCAL (XPN,XP,DU,XPS,TPS,KOP,KGR,IRS,IRSMAX,ILGA, &
                         LEVA,ISEC,IRSTMAX)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     SCALED PARTICLE SIZE FROM NON-DIMENSIONALIZED PARTICLE GROWTH
!     CURVES AFTER SCALED TIME INTERVAL.
!
!     HISTORY:
!     --------
!     * APR 21/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE CNPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC,IRSTMAX) :: XPS,TPS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: XP,DU
      LOGICAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: KOP,KGR
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: IRS
      INTEGER, INTENT(IN), DIMENSION(LEVA,ISEC) :: IRSMAX
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: XPN
      LOGICAL, ALLOCATABLE, DIMENSION(:) :: KGRL
      INTEGER, ALLOCATABLE, DIMENSION(:) :: IRXL,IRXU,IRSL
      REAL, ALLOCATABLE, DIMENSION(:) :: DUT,UPN,XPL,DUL
!
!-----------------------------------------------------------------------
!
!     * ALLOCATE TEMPORARY ARRAYS.
!
      ALLOCATE(IRXU(ILGA))
      ALLOCATE(IRXL(ILGA))
      ALLOCATE(DUT (ILGA))
      ALLOCATE(UPN (ILGA))
      ALLOCATE(XPL (ILGA))
      ALLOCATE(KGRL(ILGA))
      ALLOCATE(IRSL(ILGA))
      ALLOCATE(DUL (ILGA))
!
      DO IS=1,ISEC
      DO L=1,LEVA
!
!-----------------------------------------------------------------------
!       * FIND DISCRETE SCALED PARTICLE SIZE ON GROWTH CURVE THAT IS NEXT
!       * TO THE INITIAL SCALED PARTICLE SIZE.
!
        XPL(:)=XP(:,L,IS)
        IRXL=IDEF
        DO IR=1,IRSMAX(L,IS)-1
        DO IL=1,ILGA
          IF ( IR <= IRS(IL,L,IS)-1 ) THEN
            IF (       XPL(IL) >= XPS(IL,L,IS,IR) &
                 .AND. XPL(IL)  < XPS(IL,L,IS,IR+1)              ) THEN
              IRXL(IL)=IR
            ENDIF
          ENDIF
        ENDDO
        ENDDO
!
        IRXMIN=ILARGE
        IRXMAX=ISMALL
        UPN=YNA
        DUT=YNA
        IRXU=INA
        DO IL=1,ILGA
          IRST=IRS(IL,L,IS)
          IF ( IRST > 0 ) THEN
            IRXLT=IRXL(IL)
!
!           * NORMALIZED START TIME AT BEGINNING OF TIME STEP.
!
            IF ( IRXLT /= IDEF ) THEN
              IRXMIN=MIN(IRXMIN,IRXLT)
              IRXMAX=MAX(IRXMAX,IRXLT)
!
!             * THE INITIAL PARTICLE SIZE IS WITHIN THE RANGE OF THE ARRAYS
!             * FOR THE GROWTH DATA.
!
              UP=TPS(IL,L,IS,IRXLT) &
                +(XP(IL,L,IS)-XPS(IL,L,IS,IRXLT)) &
                  /(XPS(IL,L,IS,IRXLT+1)-XPS(IL,L,IS,IRXLT)) &
                  *(TPS(IL,L,IS,IRXLT+1)-TPS(IL,L,IS,IRXLT))
            ELSE
!
!             * THE INITIAL PARTICLE IS BIGGER THAN THE BIGGEST SIZE
!             * ACCORDING TO THE GROWTH DATA. THIS IS BASED ON A SIMPLE
!             * EXTRAPOLATION OF THE GROWTH DATA UNDER THE ASSUMPTION
!             * OF A VANISHING SURFACE VPOUR CONCENTRATION, WHICH MEANS
!             * THAT THE DIFFERENCE IN NORMALIZED TIME EQUALS THE
!             * DIFFERENCE IN NORMALIZED PARTICLE SIZE.
!
              IF ( KGR(IL,L,IS) ) THEN
                UP=TPS(IL,L,IS,IRST)+(XP(IL,L,IS)-XPS(IL,L,IS,IRST))
              ELSE
                UP=TPS(IL,L,IS,IRST)-(XP(IL,L,IS)-XPS(IL,L,IS,IRST))
              ENDIF
            ENDIF
!
!           * NORMALIZED TIME AT END OF TIME STEP.
!
            UPN(IL)=UP+DU(IL,L,IS)
!
!           * DETERMINE FIRST SCALED TIME INCREMENT. DISTINGUISH BETWEEN
!           * SITUATIONS IN WHICH THE PARTICLE WILL BE LIMITED TO
!           * BE WITHIN THE BOUNDARIES OF THE GROWTH DATA AND OTHERWISE.
!
            IF ( KGR(IL,L,IS) ) THEN
              IF ( UPN(IL) >= TPS(IL,L,IS,IRST) ) THEN
!
!               * PARTICLES COULD GROW TO SIZES BIGGER THAN THE MAXIMUM SIZE
!               * FROM THE GROWTH DATA BECAUSE THE TIME INTERVAL FOR
!               * PARICLE GROWTH IS SUFFICIENTLY LONG. LIMIT GROWTH TO
!               * MAXIMUM SIZE IN CASE THE BOUNDARY AT THAT SIZE IS NOT
!               * OPEN.
!
                IF ( KOP(IL,L,IS) ) THEN
                  DUT(IL)=UPN(IL)-TPS(IL,L,IS,IRST)
                ELSE
                  DUT(IL)=0.
                ENDIF
                IRXU(IL)=IRST
              ELSE
!
!               * IF FINAL PARTICLE SIZES ARE SMALLER THAN MAXIMUM SIZE,
!               * LET PARTICLE GROW TO NEXT AVAILABLE SIZE.
!
                DUT(IL)=TPS(IL,L,IS,IRXLT+1)-UP
                IRXU(IL)=IRXLT
              ENDIF
            ELSE
              IF ( UPN(IL) <= TPS(IL,L,IS,IRST) ) THEN
!
!               * PARTICLES WILL SHRINK TO SIZES THAT ARE STILL BIGGER
!               * THAN THE MAXIMUM SIZE ACCORDING TO THE GROWTH DATA.
!
                DUT(IL)=DU(IL,L,IS)
                IRXU(IL)=IRST+1
              ELSE
                IF ( UP < TPS(IL,L,IS,IRST) ) THEN
!
!                 * THE PARTICLE WILL SHRINK AT LEAST TO THE MAXIMUM
!                 * SIZE ACCORDING TO THE GROWTH DATA. SAVE THE TIME
!                 * IT TAKES THE PARICLES TO SHRINK TO THAT SIZE.
!
                  DUT(IL)=TPS(IL,L,IS,IRST)-UP
                  IRXU(IL)=IRST
                ELSE
!
!                 * THE PARTICLES ARE INITIALLY SMALLER THAN THE MAXIMUM
!                 * SIZE AND WILL CONTINUE TO SHRINK. SAVE THE FIRST TIME
!                 * INTERVAL.
!
                  DUT(IL)=TPS(IL,L,IS,IRXLT)-UP
                  IRXU(IL)=IRXLT
                ENDIF
              ENDIF
            ENDIF
          ENDIF
        ENDDO
!
!-----------------------------------------------------------------------
!       * INCREMENT SCALED TIME UNTIL IT IS ALMOST REACHES THE TOTAL
!       * SCALED TIME FOR POSITIVE PARTICLE GROWTH.
!
        KGRL(:)=KGR(:,L,IS)
        IRSL(:)=IRS(:,L,IS)
        DUL(:)=DU(:,L,IS)
        DO IR=IRXMIN+1,IRSMAX(L,IS)-1
        DO IL=1,ILGA
          IF ( IRSL(IL) > 0 .AND. KGRL(IL) &
               .AND. IR >= IRXU(IL)+1 .AND. IR <= IRSL(IL)-1 &
               .AND. DUT(IL) < DUL(IL)                           ) THEN
            DUT(IL)=DUT(IL) &
                   +(TPS(IL,L,IS,IR+1)-TPS(IL,L,IS,IR))
            IRXU(IL)=IR
          ENDIF
        ENDDO
        ENDDO
!
!       * INCREMENT SCALED TIME UNTIL IT IS ALMOST REACHES THE TOTAL
!       * SCALED TIME FOR NEGATIVE PARTICLE GROWTH.
!
!        DO IR=IRXMAX,2,-1
        DO IR=IRXMAX,1,-1
        DO IL=1,ILGA
          IF ( IRSL(IL) > 0 .AND. .NOT.KGRL(IL) &
               .AND. IR <= IRXU(IL)-1 .AND. IR <= IRSL(IL)-1 &
               .AND. DUT(IL) < DUL(IL)                           ) THEN
            DUT(IL)=DUT(IL)+(TPS(IL,L,IS,IR)-TPS(IL,L,IS,IR+1))
            IRXU(IL)=IR
          ENDIF
        ENDDO
        ENDDO
!
!-----------------------------------------------------------------------
!       * FINAL SCALED PARTICLE SIZE.
!
        DO IL=1,ILGA
          IRST=IRS(IL,L,IS)
          IF ( IRST > 0 ) THEN
            IF ( IRXU(IL) < IRST ) THEN
!
!             * NEW SCALED PARTICLE SIZE FROM INTERPOLATION FOR POSITIVE
!             * PARTICLE GROWTH.
!
              IRT=IRXU(IL)
              DTIM=TPS(IL,L,IS,IRT+1)-TPS(IL,L,IS,IRT)
              IF ( ABS(DTIM) > YSMALL ) THEN
                XPNN=XPS(IL,L,IS,IRT) &
                  +(UPN(IL)-TPS(IL,L,IS,IRT)) &
                     /(TPS(IL,L,IS,IRT+1)-TPS(IL,L,IS,IRT)) &
                     *(XPS(IL,L,IS,IRT+1)-XPS(IL,L,IS,IRT))
              ELSE
                IF ( KGR(IL,L,IS) ) THEN
                  XPNN=XPS(IL,L,IS,IRT+1)
                ELSE
                  XPNN=XPS(IL,L,IS,IRT)
                ENDIF
              ENDIF
              XPN(IL,L,IS)=MAX(XPNN,XPS(IL,L,IS,1))
            ELSE
              IF ( KGR(IL,L,IS) ) THEN
!
!               * CONTINUE PARTICLE GROWTH UNDER ASSUMPTION OF VANISHING
!               * PARTICLE SURFACE SUPERSATURATION.
!
                XPN(IL,L,IS)=XPS(IL,L,IS,IRST)+DUT(IL)
              ELSE
!
!               * PARTICLE GROWTH UNDER ASSUMPTION OF VANISHING PARTICLE
!               * SURFACE CONCENTRATION.
!
                XPN(IL,L,IS)=XP(IL,L,IS)-DUT(IL)
              ENDIF
            ENDIF
          ELSE
!
!           * PARTICLE SIZE IS NEAR EQUILIBRIUM SIZE.
!
            XPN(IL,L,IS)=XP(IL,L,IS)
          ENDIF
        ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * DEALLOCATE TEMPORARY ARRAYS.
!
      DEALLOCATE(IRXU)
      DEALLOCATE(IRXL)
      DEALLOCATE(DUT)
      DEALLOCATE(UPN)
      DEALLOCATE(XPL)
      DEALLOCATE(KGRL)
      DEALLOCATE(IRSL)
      DEALLOCATE(DUL)
!
      END SUBROUTINE CNRCAL
