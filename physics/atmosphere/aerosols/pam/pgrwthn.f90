      SUBROUTINE PGRWTHN (TMAS,RESM,FRC,I0L,I0R,DP3L,DP3R, &
                          DP3S,PN0,DRYDN,ILGA,LEVA,ITRS,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PARTICLE GROWTH FROM CONDENSATION OF AEROSOL MASS FOR PASSIVE
!     AEROSOL SPECIES WITHOUT CHANGE IN SPECIES MASS.
!
!     IT IS ASSUMED THAT SECTIONS INITIALLY HAVE THE SAME SIZE AND THAT
!     GROWTH LEADS TO A AN EQUAL OR SMALLER WIDTH OF EACH SECTION.
!     THE SUBROUTINE TREATS GROWTH IN PARTICLE SIZE SPACE BASED ON
!     A LAGRANGIAN APPROACH.
!
!     HISTORY:
!     --------
!     * FEB 20/2005 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL(R8), INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC,ITRS) :: TMAS
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ITRS) :: RESM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC,ITRS) :: FRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: PN0,DRYDN
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: DP3L,DP3R,DP3S
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: I0L,I0R
!
!-----------------------------------------------------------------------
!     * INITIALIZATION.
!
      TMAS=0.
!
!-----------------------------------------------------------------------
!     * CALCULATE NUMBER AND MASS IN EACH SECTION.
!
      DO IT=1,ITRS
        DO ISI=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
!
!          * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE LEFT-HAND
!          * SIDE PART OF THE MODIFIED SECTION.
!
           IS=I0L(IL,L,ISI)
           IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                                                                )  THEN
!
!            * MASS SIZE DISTRIBUTION FROM
!            * M(PHI(T))=F*(R(T=0))**3*N(PHI(T=0))
!
             C0=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*FRC(IL,L,ISI,IT)*YCNST
             TMAS(IL,L,IS,IT)=TMAS(IL,L,IS,IT)+C0*DP3L(IL,L,ISI)
           ENDIF
!
!          * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE RIGHT-HAND
!          * SIDE PART OF THE MODIFIED SECTION.
!
           IS=I0R(IL,L,ISI)
           IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
             .AND. I0L(IL,L,ISI) /= IS                          )  THEN
!
!            * MASS SIZE DISTRIBUTION FROM
!            * M(PHI(T))=F*(R(T=0))**3*N(PHI(T=0))
!
             C0=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*FRC(IL,L,ISI,IT)*YCNST
             TMAS(IL,L,IS,IT)=TMAS(IL,L,IS,IT)+C0*DP3R(IL,L,ISI)
           ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!-----------------------------------------------------------------------
!       * RESIDUUM DUE TO PARTICLES GROWING TO SIZES LARGER THAN
!       * THE SPECTRUM CUTOFF.
!
        DO ISI=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
           IF ( (I0R(IL,L,ISI) == (ISEC+1) .OR. I0L(IL,L,ISI) == 0) &
               .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!            * MASS SIZE DISTRIBUTION FROM
!            * M(PHI(T))=F*(R(T=0))**3*N(PHI(T=0))
!
             C0=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*FRC(IL,L,ISI,IT)*YCNST
             RESM(IL,L,IT)=RESM(IL,L,IT)+C0*DP3S(IL,L,ISI)
           ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDDO
!
      END SUBROUTINE PGRWTHN
