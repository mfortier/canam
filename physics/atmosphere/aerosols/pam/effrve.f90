      SUBROUTINE EFFRVE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                        PEWETRC,PEDDN,TRE,TVE,TLOAD,ILGA,LEVA,IEXTP, &
                        KEXTTP,ISEXTTP)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     RADIATION PARAMETERS FOR EXTERNALLY MIXED TYPES OF AEROSOLS.
!
!     HISTORY:
!     --------
!     * JAN 17/2012 - K. VON SALZEN    BASED ON EFFRV
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
      REAL, INTENT(IN),  DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                      PENUM,PEMAS,PEWETRC,PEDDN, &
                                      PEN0,PEPSI,PEPHI0,PEPHIS0,PEDPHI0
      INTEGER, INTENT(IN), DIMENSION(ISEXTTP) :: IEXTP
      REAL, INTENT(OUT),  DIMENSION(ILGA,LEVA) :: TRE,TVE,TLOAD
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: &
                                      SENUM,SEMAS,SEWETRC,SEDDN, &
                                      SEN0,SEPSI,SEPHI0,SEPHIS0,SEDPHI0
      REAL, ALLOCATABLE, DIMENSION(:) :: SEDRYRC
!
!-----------------------------------------------------------------------
!     * ALLOCATE MEMORY.
!
      IF ( KEXTTP > 0 ) THEN
        ALLOCATE (SENUM  (ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEMAS  (ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEN0   (ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEPSI  (ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEPHI0 (ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEPHIS0(ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEDPHI0(ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEWETRC(ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEDDN  (ILGA,LEVA,ISEXTTP))
        ALLOCATE (SEDRYRC(ISEXTTP))
      ENDIF
!
!     * TYPE-SPECIFIC AEROSOL SIZE DISTRIBUTION PARAMETERS.
!
      IF ( KEXTTP > 0 ) THEN
        DO IS=1,ISEXTTP
          SENUM  (:,:,IS)=PENUM  (:,:,IEXTP(IS))
          SEMAS  (:,:,IS)=PEMAS  (:,:,IEXTP(IS))
          SEN0   (:,:,IS)=PEN0   (:,:,IEXTP(IS))
          SEPSI  (:,:,IS)=PEPSI  (:,:,IEXTP(IS))
          SEPHI0 (:,:,IS)=PEPHI0 (:,:,IEXTP(IS))
          SEPHIS0(:,:,IS)=PEPHIS0(:,:,IEXTP(IS))
          SEDPHI0(:,:,IS)=PEDPHI0(:,:,IEXTP(IS))
          SEWETRC(:,:,IS)=PEWETRC(:,:,IEXTP(IS))
          SEDDN  (:,:,IS)=PEDDN  (:,:,IEXTP(IS))
          SEDRYRC(IS) = PEDRYRC(IEXTP(IS))
        ENDDO
      ENDIF
!
!      EFFECTIVE RADIUS AND VARIANCE FOR WET AEROSOL.
!
      IF ( KEXTTP > 0 ) THEN
        CALL EFFRV(SENUM,SEMAS,SEN0,SEPHI0,SEPSI,SEPHIS0,SEDPHI0, &
                   SEWETRC,SEDRYRC,SEDDN,TLOAD,TRE,TVE,ILGA,LEVA, &
                   ISEXTTP)
      ENDIF
!
!     * DEALLOCATE MEMORY.
!
      IF ( KEXTTP > 0 ) THEN
        DEALLOCATE (SENUM  )
        DEALLOCATE (SEMAS  )
        DEALLOCATE (SEN0   )
        DEALLOCATE (SEPSI  )
        DEALLOCATE (SEPHI0 )
        DEALLOCATE (SEPHIS0)
        DEALLOCATE (SEDPHI0)
        DEALLOCATE (SEWETRC)
        DEALLOCATE (SEDRYRC)
        DEALLOCATE (SEDDN  )
      ENDIF
!
      END SUBROUTINE EFFRVE
