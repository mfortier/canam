      SUBROUTINE SULOXI (DXSIV,DXSVI,DXHP,ZHENRY,O3FRC,H2O2FRC,XSIV, &
                         XSVI,XO3,XHP,XNA,XAM,XSS,ZCLF,ZMLWC,TA,RHOA, &
                         CO2_PPM,DT,ILGA,LEVA,NEQP)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CHEMICAL IN-CLOUD OXIDATION CALCULATIONS (FOR BULK AEROSOL).
!
!     HISTORY:
!     --------
!     * DEC  6/2017 - K.VONSALZEN  CORRECTION UNITS FOR SULPHATE
!     * JUN 10/2014 - K.VONSALZEN  CORRECTION PRECISION OF CONSTANTS
!     * FEB 24/2012 - K.VONSALZEN  DIAGNOSIS OF OZONE OXIDATION FRACTION
!     * FEB 10/2010 - K.VONSALZEN  NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA):: DXSIV,DXSVI,DXHP, &
                                                ZHENRY,O3FRC,H2O2FRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: TA,RHOA,XSIV,XSVI,XO3, &
                                                XHP,XNA,XAM,XSS,ZMLWC, &
                                                ZCLF
      REAL, INTENT(IN) :: CO2_PPM,DT
      REAL(R8), DIMENSION(ILGA,LEVA)         :: OGTSO2,AGTSO4,AGTO3, &
                                                AGTHO2,AOH2O2,OGTHNO3, &
                                                OGTNH3,OGTCO2,ARESID, &
                                                WRK1,WRK2,ANTSO2, &
                                                ANTSO4,ANTHO2,ALWCVMR, &
                                                AGTSO2,AGTCO2,AGTHNO3, &
                                                AGTNH3,AMSO3,AMNH4, &
                                                AMNO3,AMSO4,AMSSSO4, &
                                                AMSSCL,AMSSANI,AMSSCAT, &
                                                AMCL,AMHCL,AGTHCL, &
                                                ASSMR,ACONVF,ADELTA, &
                                                AFOH,AMH,ATMP,ATVAL, &
                                                AFRAS,AFNH4,AFNO3,AFCL, &
                                                AFHSO,AFCO3,ATVO3,AF1, &
                                                AF2,AFRAH,APARD,APARA, &
                                                APARB,APARC,ATVALX, &
                                                ASRSO2,ADELTAX,XSIVT, &
                                                WRKTOT
      REAL(R8), DIMENSION(ILGA,LEVA,NEQP) :: ACHPA
      REAL, PARAMETER :: YCOM3L = 1.E+03_R8
      REAL, PARAMETER :: ZSEC   = 1.E-07_R8
      REAL(R8), PARAMETER :: AMHMIN = 1.E-10_R8
      REAL(R8), PARAMETER :: AMHMAX = 1.E-01_R8
      INTEGER, PARAMETER :: YSUB=2
      REAL, PARAMETER :: ATUNEX=1.
!
!-----------------------------------------------------------------------
!     * INITIALIZATIONS OF PARAMETERS.
!
      AFOH=1.E-14_R8
      DXSIV =0.
      DXSVI =0.
      DXHP  =0.
      ZHENRY=0.
      ARESID=0.
      WRK1  =0.
      WRK2  =0.
      ATIMST=DT/REAL(YSUB)
!
!     * HENRY'S LAW CONSTANTS AND REACTION RATES.
!
      LEVI=1
      ILGAS=1
      CALL EQUIP(ILGA,LEVA,NEQP,LEVI,ILGAS,ILGA,ACHPA,TA)
!
!     * CONVERSION OF UNITS TO MOL/LITRE FOR FOLLOWING INPUT UNITS:
!     * SO2: KG-S/KG-AIR
!     * SO4: KG-S/KG-AIR
!     * H2O2: KG-S/KG-AIR
!     * O3, HNO3, NH3, CO2: VOLUME MIXING RATIO
!
      OGTSO2 =XSIV*RHOA/(WS*YCOM3L)
      AGTSO4 =XSVI*RHOA/(WS*YCOM3L)
      AGTO3  =XO3*RHOA/(WA*YCOM3L)
      AGTHO2 =XHP*RHOA/(WS*YCOM3L)
      OGTHNO3=XNA*RHOA/(WA*YCOM3L)
      OGTNH3 =XAM*RHOA/(WA*YCOM3L)
      OGTCO2 =CO2_PPM*RHOA/(WA*YCOM3L)
!
!     * SAVE INPUT.
!
      AOH2O2=AGTHO2
!
!-----------------------------------------------------------------------
!     * S(IV) OXIDATION. CHEMICAL SOURCES/SINKS ARE CONSIDERED
!     * FOR SULPHUR DIOXIDE, HYDROGEN PEROXIDE, OZONE. THEY
!     * ARE ONLY APPLIED IF ALL OF THESE THREE TRACERS PLUS
!     * SULPHATE ARE AVAILABLE. THE CORRESPONDING SULPHATE
!     * PRODUCTION RATE IS GIVEN BY AF1*[O3]*[SO2]+AF2*[H2O2]*[SO2]
!     * (IN KG-SULPHUR/KG-AIR/SEC), WHERE THE CONCENTRATIONS
!     * REFER TO THE TOTAL (GAS PHASE + DISSOLVED) SPECIES (IN
!     * KG-SULPHUR/KG-AIR). THE INTERNAL TIME STEP IN THESE
!     * CALCULATIONS IS GIVEN BY: TIME_STEP(MODEL)/YSUB.
!
      DO INDTI = 1, YSUB
        WHERE ( ZMLWC > ZSEC .AND. ZCLF > ZSEC )
!
!         * INITIALIZATION.
!
          ANTSO2=OGTSO2
          ANTHO2=AGTHO2
          ANTSO4=AGTSO4
!
!         * LIQUID WATER VOLUME MIXING RATIO.
!
          ALWCVMR=ZMLWC*RHOA/RHOH2O
!
!         * TOTAL (AQUEOUS + GAS-PHASE) CONCENTRATIONS.
!
          AGTSO2 = OGTSO2
          AGTCO2 = OGTCO2
          AGTHNO3= OGTHNO3
          AGTNH3 = OGTNH3
!
!         * MOLARITIES.
!
          AMSO3= 0._R8
          AMNH4=AGTNH3 /ALWCVMR
          AMNO3=AGTHNO3/ALWCVMR
          AMSO4=AGTSO4 /ALWCVMR
!
!         * SEA SALT SPECIES.
!
          ASSMR=XSS
          ACONVF=RHOA/(ALWCVMR*YCOM3L)
!
!         * CALCULATE MOLARITIES OF SEA SALT SPECIES ASSUMING A TYPICAL
!         * COMPOSITION OF SEA WATER. NO ANIONS OTHER THAN SULPHATE AND
!         * CHLORIDE ARE CONSIDERED. ALL CATIONS, EXCEPT H+, ARE LUMPED
!         * TOGETHER. THE PH OF THE SEA WATER IS ASSUMED TO BE 8.2.
!         * H+ IS ACCOUNTED FOR IN THE ION BALANCE AND IN THE CALCULATION
!         * OF HYDROGEN CHLORINE IN THE GAS PHASE.
!
          AMSSSO4=0.077_R8*ACONVF/96.058E-03_R8*ASSMR
          AMSSCL =0.552_R8*ACONVF/35.453E-03_R8*ASSMR
          AMSSANI=0._R8
          AMSSCAT=(AMSSCL+2._R8*AMSSSO4+AMSSANI)*(1._R8-1.048E-08_R8)
          AMSO4=AMSO4+AMSSSO4
          AMCL=AMSSCL
          AMHCL=AMSSCL*6.31E-09_R8/ACHPA(:,:,10)
          AGTHCL=(AMSSCL+AMHCL)*ALWCVMR
          ADELTA=AMNO3+2._R8*(AMSO4+AMSO3)+AMCL+AMSSANI-AMNH4-AMSSCAT
          ATMP=ADELTA**2+4._R8*(AFOH &
                              +ACHPA(:,:,2)*AGTSO2+ACHPA(:,:,5)*AGTCO2)
        ELSEWHERE
          ATMP=0._R8
        ENDWHERE
!
!       * INITIAL ESTIMATE OF PROTON MOLARITY FROM INITIAL
!       * MOLARITIES.
!
        AMH=SQRT(ATMP)
        AMH=MAX(MIN(AMH,AMHMAX),AMHMIN)
        WHERE ( ZMLWC > ZSEC .AND. ZCLF > ZSEC )
!
!         * EQUILIBRIUM PARAMETERS FOR SOLUBLE SPECIES.
!
          ATVAL=ACHPA(:,:,1)+ACHPA(:,:,2)/AMH+ACHPA(:,:,3)/AMH**2
          AFRAS=1._R8/( 1._R8+ALWCVMR*ATVAL )
          AFNH4=1._R8/( 1._R8+AGTNH3/(1._R8/ACHPA(:,:,9)+ALWCVMR*AMH) )
          AFNO3=AGTHNO3/( 1._R8/ACHPA(:,:,8)+ALWCVMR/AMH )
!
!         * FIRST ITERATION IN PROTON MOLARITY CALCULATION.
!
          AFCL =AGTHCL/( 1._R8/ACHPA(:,:,10)+ALWCVMR/AMH )
          AFHSO=AGTSO2*AFRAS*ACHPA(:,:,2)
          AFCO3=AGTCO2*ACHPA(:,:,5)
          AMSO3=( AGTSO2*AFRAS*ACHPA(:,:,3) )/AMH**2
          ADELTA=AFNH4*( 2._R8*(AMSO4+AMSO3)+AMSSANI-AMSSCAT)
          AMH=0.5_R8*( ADELTA+                                         &!PROC-DEPEND
                              SQRT(ADELTA**2+4._R8*AFNH4*(AFOH &
                                            +AFCO3+AFHSO+AFNO3+AFCL)) )
          AMH=MAX(MIN(AMH,AMHMAX),AMHMIN)
          ATVAL=ACHPA(:,:,1)+ACHPA(:,:,2)/AMH+ACHPA(:,:,3)/AMH**2
          AFRAS=1._R8/( 1._R8+ALWCVMR*ATVAL )
          AFNH4=1._R8/( 1._R8+AGTNH3/(1._R8/ACHPA(:,:,9)+ALWCVMR*AMH) )
          AFNO3=AGTHNO3/( 1._R8/ACHPA(:,:,8)+ALWCVMR/AMH )
!
!         * SECOND ITERATION IN PROTON MOLARITY CALCULATION.
!
          AFCL =AGTHCL/( 1._R8/ACHPA(:,:,10)+ALWCVMR/AMH )
          AFHSO=AGTSO2*AFRAS*ACHPA(:,:,2)
          AFCO3=AGTCO2*ACHPA(:,:,5)
          AMSO3=( AGTSO2*AFRAS*ACHPA(:,:,3) )/AMH**2
          ADELTA=AFNH4*( 2._R8*(AMSO4+AMSO3)+AMSSANI-AMSSCAT)
          AMH=0.5_R8*( ADELTA+                                         &!PROC-DEPEND
                              SQRT(ADELTA**2+4._R8*AFNH4*(AFOH &
                                            +AFCO3+AFHSO+AFNO3+AFCL)) )
          AMH=MAX(MIN(AMH,AMHMAX),AMHMIN)
          ATVAL=ACHPA(:,:,1)+ACHPA(:,:,2)/AMH+ACHPA(:,:,3)/AMH**2
          AFRAS=1._R8/( 1._R8+ALWCVMR*ATVAL )
!
!         * OZONE OXIDATION PARAMETER (IN 1/SEC).
!
          ATVO3=1._R8/( 1._R8+ALWCVMR*ACHPA(:,:,7) )
          AF1=( ACHPA(:,:,11)+ACHPA(:,:,12)/AMH )*AFRAS &
                                      *ATVAL*ATVO3*ACHPA(:,:,7)*ALWCVMR
          AF1=AF1*AGTO3
          AF1=AF1*ATUNEX
!
!         * HYDROGEN PEROXIDE OXIDATION PARAMETER (IN LITRE-AIR/MOL/SEC).
!
          AFRAH=1._R8/( 1._R8+ALWCVMR*ACHPA(:,:,6) )
          AF2=( ACHPA(:,:,13)/(0.1_R8+AMH) )*AFRAS &
                               *ACHPA(:,:,1)*AFRAH*ACHPA(:,:,6)*ALWCVMR
          AF2=AF2*ATUNEX
!
!         * SEMI-IMPLICIT INTEGRATION.
!
          APARD=AGTHO2
          APARA=( 1._R8+ATIMST*AF1 )*ATIMST*AF2
          APARC=-AGTSO2
          APARB=1._R8+ATIMST*( AF1+AF2*(APARD+APARC) )
          ATVALX=-APARB/( 2._R8*APARA )
          ATMP=ATVALX**2-APARC/APARA
        ELSEWHERE
          ATMP=0._R8
        ENDWHERE
        ANTSO2=SQRT(ATMP)
        WHERE ( ZMLWC > ZSEC .AND. ZCLF > ZSEC )
          ANTSO2=ATVALX+ANTSO2
          ANTSO2=MAX(ANTSO2,0._R8)
          ANTSO2=MIN(ANTSO2,AGTSO2)
          ANTHO2=APARD/(1._R8+ATIMST*AF2*ANTSO2)
          ANTHO2=MAX(ANTHO2,0._R8)
          ANTHO2=MIN(ANTHO2,AGTHO2)
          ADELTA=AGTSO2-ANTSO2
          ANTSO4=AGTSO4+ADELTA
          ANTSO4=MAX(ANTSO4,0._R8)
          WRK1  =WRK1+AF1/REAL(YSUB)
          WRK2  =WRK2+AF2*AGTHO2/REAL(YSUB)
!
!         * SCAVENGING RATIOS, I.E. FRACTION OF TRACER THAT
!         * IS DISSOLVED. ONLY FOR SO2 SO FAR.
!
          ASRSO2=MAX(1._R8-AFRAS,0._R8)
          ZHENRY=ZHENRY+ASRSO2/REAL(YSUB)
!
!         * UPDATE FIELDS.
!
          ADELTA=OGTSO2-ANTSO2
          ARESID=ARESID+ADELTA
!
!         * FINAL CONCENTRATIONS AFTER ATIMST.
!
          OGTSO2=ANTSO2
          AGTHO2=ANTHO2
          AGTSO4=ANTSO4
        ENDWHERE
      ENDDO
      ADELTAX=ARESID*YCOM3L*WS/RHOA
      ADELTAX=MAX(ADELTAX,0._R8)
      XSIVT=XSIV
      ADELTAX=MIN(XSIVT,ADELTAX)
!
!-----------------------------------------------------------------------
!     * SULPHUR DIOXIDE.
!
      DXSIV=-ADELTAX
!
!     * SULPHATE.
!
      DXSVI=ADELTAX
!
!     * HYDROGEN PEROXIDE.
!
      ADELTAX=(AGTHO2-AOH2O2)*YCOM3L*WS/RHOA
      DXHP=ADELTAX
!
!     * OZONE AND HYDROGEN PEROXIDE OXIDATION RATES.
!
      WRKTOT=WRK1+WRK2
      WHERE ( WRKTOT > YTINY )
        O3FRC=MIN(MAX(WRK1/WRKTOT,0._R8),1._R8)*ADELTAX
        H2O2FRC=MIN(MAX(WRK2/WRKTOT,0._R8),1._R8)*ADELTAX
      ELSEWHERE
        O3FRC=0._R8
        H2O2FRC=0._R8
      ENDWHERE
!
      END SUBROUTINE SULOXI
