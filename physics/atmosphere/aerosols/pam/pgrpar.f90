      SUBROUTINE PGRPAR (DP0L,DP1L,DP2L,DP3L,DP4L,DP5L,DP6L, &
                         DM1L,DM2L,DM3L,DP0R,DP1R,DP2R,DP3R, &
                         DP4R,DP5R,DP6R,DM1R,DM2R,DM3R,DP0S, &
                         DP1S,DP2S,DP3S,DP4S,DP5S,DP6S,DM1S, &
                         DM2S,DM3S,I0L,I0R,CGR1,CGR2,PN0, &
                         DRYR,PHIS,DPHIS,PHI0,PSI,ILGA,LEVA, &
                         ISEC,IERR,IMODC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PARAMETERS FOR PARTICLE GROWTH FROM CONDENSATION OF SULPHURIC
!     ACID.
!
!     HISTORY:
!     --------
!     * JUN 12/2008 - K.VONSALZEN   ADD DIMENSIONS TO ARRAYS FOR PARICLE
!                                   SIZES
!     * FEB 20/2007 - K.VONSALZEN   MODIFIED FOR EXT./INT. MIXED
!                                   AEROSOL, BASED ON PGRWTH
!     * DEC 11/2005 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(INOUT) :: IERR
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: CGR1,CGR2,PN0,PSI, &
                                                     PHI0,PHIS,DPHIS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC+1) :: DRYR
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC,IMODC
      REAL(R8), INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: &
                                    DP0L,DP1L,DP2L,DP3L,DP4L,DP5L,DP6L, &
                                    DM1L,DM2L,DM3L, &
                                    DP0R,DP1R,DP2R,DP3R,DP4R,DP5R,DP6R, &
                                    DM1R,DM2R,DM3R, &
                                    DP0S,DP1S,DP2S,DP3S,DP4S,DP5S,DP6S, &
                                    DM1S,DM2S,DM3S
      INTEGER, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: I0L,I0R
      REAL, DIMENSION(ILGA,LEVA,ISEC) :: DPHIL,DPHIR,PHISR,TMP1,TMP2, &
                                         TCR1,TCR2
      REAL, DIMENSION(ILGA,LEVA) :: TERM1,TERM2,TERM3,TERM4
      INTEGER, PARAMETER :: ISKIP = -1
!
!-----------------------------------------------------------------------
!     * INITIALIZATION.
!
      DP0L=0.
      DP1L=0.
      DP2L=0.
      DP3L=0.
      DP4L=0.
      DP5L=0.
      DP6L=0.
      DM1L=0.
      DM2L=0.
      DM3L=0.
      DP0R=0.
      DP1R=0.
      DP2R=0.
      DP3R=0.
      DP4R=0.
      DP5R=0.
      DP6R=0.
      DM1R=0.
      DM2R=0.
      DM3R=0.
      DP0S=0.
      DP1S=0.
      DP2S=0.
      DP3S=0.
      DP4S=0.
      DP5S=0.
      DP6S=0.
      DM1S=0.
      DM2S=0.
      DM3S=0.
!
!-----------------------------------------------------------------------
!     * CRITICAL GROWTH FACTOR FOR COMPLETE SIZE SECTIONS OUTGROWING
!     * THE PARTICLE SIZE DISTRIBUTION
!
      DO IS=1,ISEC
        TMP1(:,:,IS)=PHIS(:,:,1)-(PHIS(:,:,IS)+DPHIS(:,:,IS))
        TMP1(:,:,IS)=0.99*TMP1(:,:,IS)
        TMP2(:,:,IS)=(PHIS(:,:,ISEC)+DPHIS(:,:,ISEC))-PHIS(:,:,IS)
        TMP2(:,:,IS)=1.01*TMP2(:,:,IS)
      ENDDO
      TCR1=EXP(TMP1)
      TCR2=EXP(TMP2)
!
!     * CHANGE IN SIZE AT SECTION BOUNDARIES DUE TO CONDENSATION
!     * OR CHEMICAL PRODUCTION. DIFFERENT GROWTH MODELS ARE AVAILABLE
!     * FOR THE DRY PARTICLE RADIUS (R) AS A FUNCTION OF TIME (T):
!     *   IMODC=1: R(T)=R(T=0)+C/R(T=0)
!     *   IMODC=2: R(T)=C*R(T=0)
!     * WITH C=CGR1+CGR2*R(T=0)
!
      IF ( IMODC==1 ) THEN
        DO IS=1,ISEC
          TERM1(:,:)=CGR1(:,:,IS)+CGR2(:,:,IS)*DRYR(:,:,IS  )
          TERM1(:,:)=1.+TERM1(:,:)/DRYR(:,:,IS  )**2
          TERM2(:,:)=CGR1(:,:,IS)+CGR2(:,:,IS)*DRYR(:,:,IS+1)
          TERM2(:,:)=1.+TERM2(:,:)/DRYR(:,:,IS+1)**2
          TERM1=MIN(MAX(TERM1,TCR1(:,:,IS)),TCR2(:,:,IS))
          TERM2=MIN(MAX(TERM2,TCR1(:,:,IS)),TCR2(:,:,IS))
          TERM3=LOG(TERM1)
          DPHIL(:,:,IS)=TERM3(:,:)
          WHERE ( ABS(CGR2(:,:,IS)) > YTINY .AND. TERM1 > YTINY )
            TERM4=TERM2/TERM1
          ELSEWHERE
            TERM4=1.
          ENDWHERE
          TERM3=LOG(TERM4)
          DPHIR(:,:,IS)=DPHIL(:,:,IS)+TERM3(:,:)
        ENDDO
      ELSE IF ( IMODC==2 ) THEN
        DO IS=1,ISEC
          TERM1(:,:)=CGR1(:,:,IS)+CGR2(:,:,IS)*DRYR(:,:,IS  )
          TERM2(:,:)=CGR1(:,:,IS)+CGR2(:,:,IS)*DRYR(:,:,IS+1)
          TERM1=MIN(MAX(TERM1,TCR1(:,:,IS)),TCR2(:,:,IS))
          TERM2=MIN(MAX(TERM2,TCR1(:,:,IS)),TCR2(:,:,IS))
          TERM3=LOG(TERM1)
          DPHIL(:,:,IS)=TERM3(:,:)
          WHERE ( ABS(CGR2(:,:,IS)) > YTINY .AND. TERM1 > YTINY )
            TERM4=TERM2/TERM1
          ELSEWHERE
            TERM4=1.
          ENDWHERE
          TERM3=LOG(TERM4)
          DPHIR(:,:,IS)=DPHIL(:,:,IS)+TERM3(:,:)
        ENDDO
      ELSE
        CALL XIT('PGRPAR',-1)
      ENDIF
!
!-----------------------------------------------------------------------
!     * DETERMINE INDEX OF SECTION IN WHICH THE BOUNDARY OF
!     * THE MODIFIED SECTION RESIDES AFTER CONDENSATION.
!
      I0L=ISKIP
      I0R=ISKIP
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         ANPHIL=PHIS(IL,L,ISI)+DPHIL(IL,L,ISI)
         ANPHIR=PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)+DPHIR(IL,L,ISI)
         IF ( ANPHIL < PHIS(IL,L,1) ) I0L(IL,L,ISI)=0
         IF ( ANPHIR < PHIS(IL,L,1) ) I0R(IL,L,ISI)=0
         IF ( ANPHIR > PHIS(IL,L,ISEC)+DPHIS(IL,L,ISEC) ) &
              I0R(IL,L,ISI)=ISEC+1
      ENDDO
      ENDDO
      ENDDO
      DO IS=1,ISEC
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         ANPHIL=PHIS(IL,L,ISI)+DPHIL(IL,L,ISI)
         ANPHIR=PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)+DPHIR(IL,L,ISI)
         IF (       (ANPHIL >= PHIS(IL,L,IS)) &
              .AND. (ANPHIL <  PHIS(IL,L,IS)+DPHIS(IL,L,IS)) ) &
             I0L(IL,L,ISI)=IS
         IF (       (ANPHIR >  PHIS(IL,L,IS)) &
              .AND. (ANPHIR <= PHIS(IL,L,IS)+DPHIS(IL,L,IS)) ) &
             I0R(IL,L,ISI)=IS
      ENDDO
      ENDDO
      ENDDO
      ENDDO
!
!     * TEST WHETHER INDICES ARE WITHIN ALLOWED RANGE.
!
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         IF     (I0R(IL,L,IS) < I0L(IL,L,IS) &
            .OR. I0R(IL,L,IS) > (I0L(IL,L,IS)+1) ) THEN
           IERR=-10
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * CALCULATE THE SIZE OF THE PARTICLE IN EACH SECTION THAT
!     * CORRESPONDS TO THE BOUNDARY OF THE CORRESPONDING SECTION FOR
!     * THE GROWN PARITCLES AT TIME T. RINIT IS THE INITIAL RADIUS OF
!     * THE PARTICLE (R(T=0)) AND IS CALCULATED FROM THE INVERSE
!     * OF R(T)=F(R(T=0)).
!
      DO IS=1,ISEC
         PHISR(:,:,IS)=PHIS(:,:,IS)+DPHIS(:,:,IS)
      ENDDO
      TERM1=PHIS(:,:,1)
      TERM4=EXP(TERM1)
      IF ( IMODC==1 ) THEN
        DO ISI=1,ISEC
          TERM1=YNA
          DO L=1,LEVA
          DO IL=1,ILGA
            IF ( I0L(IL,L,ISI) >= ISI ) THEN
              IS=I0L(IL,L,ISI)
              IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                  .AND. IS /= I0R(IL,L,ISI) ) THEN
                TERM1(IL,L)=DRYR(IL,L,IS+1)-CGR2(IL,L,ISI)
              ENDIF
            ELSE
              IS=I0R(IL,L,ISI)
              IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                  .AND. IS /= I0L(IL,L,ISI) ) THEN
                TERM1(IL,L)=DRYR(IL,L,IS)-CGR2(IL,L,ISI)
              ENDIF
            ENDIF
          ENDDO
          ENDDO
          WHERE ( ABS(TERM1-YNA) > YTINY )
            TERM2=MAX(TERM1**2-4.*CGR1(:,:,ISI),0.)
          ELSEWHERE
            TERM2=0.
          ENDWHERE
          TERM3=SQRT(TERM2)
          WHERE ( ABS(TERM1-YNA) > YTINY )
            TERM3=.5*(TERM1+TERM3)/R0
          ELSEWHERE
            TERM3=1.
          ENDWHERE
          TERM3=MAX(TERM3,TERM4)
          TERM2=LOG(TERM3)
          WHERE ( ABS(TERM1-YNA) > YTINY )
            PHISR(:,:,ISI)=TERM2
          ENDWHERE
          DO L=1,LEVA
          DO IL=1,ILGA
            PHISR(IL,L,ISI)=MAX(PHIS(IL,L,ISI),MIN(PHISR(IL,L,ISI), &
                                  PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)))
          ENDDO
          ENDDO
        ENDDO
      ELSE
        DO ISI=1,ISEC
          TERM1=YNA
          DO L=1,LEVA
          DO IL=1,ILGA
            CGRT=MIN(MAX(CGR1(IL,L,ISI),TCR1(IL,L,ISI)),TCR2(IL,L,ISI))
            IF ( I0L(IL,L,ISI) >= ISI ) THEN
              IS=I0L(IL,L,ISI)
              IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                  .AND. IS /= I0R(IL,L,ISI) ) THEN
                IF ( ABS(CGR2(IL,L,ISI)) > YTINY ) THEN
                  TERM1(IL,L)=CGR1(IL,L,ISI)**2 &
                             +4.*CGR2(IL,L,ISI)*DRYR(IL,L,IS+1)
                ELSE IF ( CGRT > YTINY ) THEN
                  TERM1(IL,L)=DRYR(IL,L,IS+1)/CGRT
               ENDIF
              ENDIF
            ELSE
              IS=I0R(IL,L,ISI)
              IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                  .AND. IS /= I0L(IL,L,ISI) ) THEN
                IF ( ABS(CGR2(IL,L,ISI)) > YTINY ) THEN
                  TERM1(IL,L)=CGR1(IL,L,ISI) &
                             +4.*CGR2(IL,L,ISI)*DRYR(IL,L,IS)
                ELSE IF ( CGRT > YTINY ) THEN
                  TERM1(IL,L)=DRYR(IL,L,IS)/CGRT
               ENDIF
              ENDIF
            ENDIF
          ENDDO
          ENDDO
          WHERE ( TERM1-YNA > YTINY .AND. ABS(CGR2(:,:,ISI)) > YTINY )
            TERM2=TERM1
          ELSEWHERE
            TERM2=0.
          ENDWHERE
          TERM3=SQRT(TERM2)
          WHERE ( ABS(TERM1-YNA) > YTINY )
            WHERE ( ABS(CGR2(:,:,ISI)) > YTINY )
              TERM3=.5*(-CGR1(:,:,ISI)+TERM3)/(CGR2(:,:,ISI)*R0)
            ELSEWHERE ( ABS(TERM1-YNA) > YTINY )
              TERM3=TERM1/R0
            ELSEWHERE
              TERM3=1.
            ENDWHERE
          ELSEWHERE
            TERM3=1.
          ENDWHERE
          TERM3=MAX(TERM3,TERM4)
          TERM2=LOG(TERM3)
          WHERE ( ABS(TERM1-YNA) > YTINY )
            PHISR(:,:,ISI)=TERM2
          ENDWHERE
          DO L=1,LEVA
          DO IL=1,ILGA
            PHISR(IL,L,ISI)=MAX(PHIS(IL,L,ISI),MIN(PHISR(IL,L,ISI), &
                                  PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)))
          ENDDO
          ENDDO
        ENDDO
      ENDIF
!
!     * TEST WHETHER THE BOUNDARY FROM INVERSE TRANFORMATION LIES
!     * WITHIN THE SECTION.
!
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         IF     (PHISR(IL,L,IS) < PHIS(IL,L,IS) &
            .OR. PHISR(IL,L,IS) > PHIS(IL,L,IS)+DPHIS(IL,L,IS) ) THEN
           IERR=-11
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * CALCULATE MOMENTS OF SIZE DISTRIBUTION.
!
      IF ( IMODC==1 ) THEN
        DO ISI=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
!
!          * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE LEFT-HAND
!          * SIDE PART OF THE MODIFIED SECTION.
!
           IS=I0L(IL,L,ISI)
           IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                                                                )  THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION AT T=0.
!
             ANPHIL=PHIS (IL,L,ISI)
             ANPHIR=PHISR(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0L(IL,L,ISI)=SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI), &
                                   ANPHIL,DPHI0)
             RMOM=3.
             DP3L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=2.
             DP2L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=1.
             DP1L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-1.
             DM1L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-2.
             DM2L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-3.
             DM3L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
           ENDIF
!
!          * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE RIGHT-HAND
!          * SIDE PART OF THE MODIFIED SECTION.
!
           IS=I0R(IL,L,ISI)
           IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
             .AND. I0L(IL,L,ISI) /= IS                          )  THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION AT T=0.
!
             ANPHIL=PHISR(IL,L,ISI)
             ANPHIR=PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0R(IL,L,ISI)=SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI), &
                                   ANPHIL,DPHI0)
             RMOM=3.
             DP3R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=2.
             DP2R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=1.
             DP1R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-1.
             DM1R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-2.
             DM2R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-3.
             DM3R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
           ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!-----------------------------------------------------------------------
!       * RESIDUUM DUE TO PARTICLES GROWING TO SIZES LARGER THAN
!       * THE SPECTRUM CUTOFF.
!
        DO ISI=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
           IS=I0R(IL,L,ISI)
           IF ( (IS == (ISEC+1) .OR. IS == 0) &
                                     .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION.
!
             ANPHIL=PHISR(IL,L,ISI)
             ANPHIR=PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0S(IL,L,ISI)=SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI), &
                                   ANPHIL,DPHI0)
             RMOM=3.
             DP3S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=2.
             DP2S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=1.
             DP1S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-1.
             DM1S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-2.
             DM2S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-3.
             DM3S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
           ENDIF
           IS=I0L(IL,L,ISI)
           IF ( (IS == (ISEC+1) .OR. IS == 0) &
                                     .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION.
!
             ANPHIL=PHIS (IL,L,ISI)
             ANPHIR=PHISR(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0S(IL,L,ISI)=SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI), &
                                   ANPHIL,DPHI0)
             RMOM=3.
             DP3S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=2.
             DP2S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=1.
             DP1S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-1.
             DM1S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-2.
             DM2S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=-3.
             DM3S(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
           ENDIF
        ENDDO
        ENDDO
        ENDDO
      ELSE
        DO ISI=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
!
!          * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE LEFT-HAND
!          * SIDE PART OF THE MODIFIED SECTION.
!
           IS=I0L(IL,L,ISI)
           IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                                                                )  THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION AT T=0.
!
             ANPHIL=PHIS (IL,L,ISI)
             ANPHIR=PHISR(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0L(IL,L,ISI)=SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI), &
                                   ANPHIL,DPHI0)
             RMOM=3.
             DP3L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=4.
             DP4L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=5.
             DP5L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=6.
             DP6L(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
           ENDIF
!
!          * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE RIGHT-HAND
!          * SIDE PART OF THE MODIFIED SECTION.
!
           IS=I0R(IL,L,ISI)
           IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
             .AND. I0L(IL,L,ISI) /= IS                          )  THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION AT T=0.
!
             ANPHIL=PHISR(IL,L,ISI)
             ANPHIR=PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0R(IL,L,ISI)=SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI), &
                                   ANPHIL,DPHI0)
             RMOM=3.
             DP3R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=4.
             DP4R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=5.
             DP5R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
             RMOM=6.
             DP6R(IL,L,ISI)=SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM, &
                                   ANPHIL,DPHI0)
           ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!-----------------------------------------------------------------------
!       * RESIDUUM DUE TO PARTICLES GROWING TO SIZES OUTSIDE
!       * THE SPECTRUM CUTOFF.
!
        DO ISI=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
           IS=I0R(IL,L,ISI)
           IF ( (IS == (ISEC+1) .OR. IS == 0) &
                                     .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION.
!
             ANPHIL=PHISR(IL,L,ISI)
             ANPHIR=PHIS(IL,L,ISI)+DPHIS(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0S(IL,L,ISI)=DP0S(IL,L,ISI) &
                   +SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI),ANPHIL,DPHI0)
             RMOM=3.
             DP3S(IL,L,ISI)=DP3S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
             RMOM=4.
             DP4S(IL,L,ISI)=DP4S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
             RMOM=5.
             DP5S(IL,L,ISI)=DP5S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
             RMOM=6.
             DP6S(IL,L,ISI)=DP6S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
           ENDIF
           IS=I0L(IL,L,ISI)
           IF ( (IS == (ISEC+1) .OR. IS == 0) &
                                     .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!            * BOUNDARIES AND WIDTH OF ORIGINAL SECTION.
!
             ANPHIL=PHIS (IL,L,ISI)
             ANPHIR=PHISR(IL,L,ISI)
             DPHI0=ANPHIR-ANPHIL
             DP0S(IL,L,ISI)=DP0S(IL,L,ISI) &
                   +SDINTB0(PHI0(IL,L,ISI),PSI(IL,L,ISI),ANPHIL,DPHI0)
             RMOM=3.
             DP3S(IL,L,ISI)=DP3S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
             RMOM=4.
             DP4S(IL,L,ISI)=DP4S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
             RMOM=5.
             DP5S(IL,L,ISI)=DP5S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
             RMOM=6.
             DP6S(IL,L,ISI)=DP6S(IL,L,ISI) &
               +SDINTB(PHI0(IL,L,ISI),PSI(IL,L,ISI),RMOM,ANPHIL,DPHI0)
           ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
      END SUBROUTINE PGRPAR
