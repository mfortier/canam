      SUBROUTINE NM2PARI(PIN0,PIPHI0,PIPSI,PIPHIS0,PIDPHI0,PIDDN, &
                         PINUM,PIMAS,PIFRC,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     TRANSFORMATION OF NUMBER AND MASS TO PLA PARAMETERS FOR INTERNALLY
!     MIXED TYPES OF AEROSOL, INCLUDING CALCULATION OF AEROSOL DENSITY AND
!     MASS AND NUMBER CORRECTIONS, IF NECESSARY.
!
!     HISTORY:
!     --------
!     * JUL 25/2015 - K.VONSALZEN   NEW, BASED ON NM2PAR.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PINUM,PIMAS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIN0,PIPHI0,PIPSI, &
                                                   PIDDN
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIPHIS0,PIDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      REAL, DIMENSION(ILGA,LEVA) :: CORN,CORM,RESN,RESM,CORNT,CORMT, &
                                    RESNT,RESMT
!
!-----------------------------------------------------------------------
!     * UPDATE DRY PARTICLE DENSITY (INTERNALLY MIXED AEROSOL).
!
      IF ( ISAINT > 0 ) CALL SDDENS(PIDDN,PIFRC,ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * INITIALIZATIONS.
!
      CORN=0.
      CORM=0.
      RESN=0.
      RESM=0.
!
!-----------------------------------------------------------------------
!     * AEROSOL NUMBER AND MASS ADJUSTMENTS.
!
      IF ( ISAINT > 0 ) THEN
        CALL CORNMI(RESMT,RESNT,CORMT,CORNT,PINUM,PIMAS,PIDDN, &
                    PIISMIN,PIISMAX,PIPHISS,PIDPHIS,ILGA,LEVA,ISAINT)
        CORN=CORN+CORNT
        CORM=CORM+CORMT
        RESN=RESN+RESNT
        RESM=RESM+RESMT
      ENDIF
!
!     * UPDATE BASIC PLA PARAMETERS.
!
      IF ( ISAINT > 0 ) THEN
        CALL NM2PLA(PIN0,PIPHI0,PIPSI,RESN,RESM,PINUM,PIMAS, &
                    PIDDN,PIPHISS,PIDPHIS,PIPHIS0,PIDPHI0, &
                    ILGA,LEVA,ISAINT,KPINT)
      ENDIF
!
      END SUBROUTINE NM2PARI
