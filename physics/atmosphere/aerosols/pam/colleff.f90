      SUBROUTINE COLLEFF (PNCEF,PMCEF,ANUM,AMAS,PN0,PHI0, &
                          PSI,PHIS0,DPHI0,WETRC,DRYRC,DRYDN, &
                          ILGA,LEVA,ISEC)
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: PNCEF,PMCEF
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                         ANUM,AMAS,PN0,PHI0,PSI,WETRC, &
                                         DRYDN,PHIS0,DPHI0
      REAL, INTENT(IN), DIMENSION(ISEC) :: DRYRC
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: AM0,AM3,AM0N,AM3N, &
                                             PHI0W,PHIS0W,WETDN
!
!---------------------------------------------------------------------
!     * ALLOCATE MEMORY.
!
      ALLOCATE(PHI0W (ILGA,LEVA,ISEC))
      ALLOCATE(PHIS0W(ILGA,LEVA,ISEC))
      ALLOCATE(WETDN (ILGA,LEVA,ISEC))
      ALLOCATE(AM0   (ILGA,LEVA,ISEC))
      ALLOCATE(AM3   (ILGA,LEVA,ISEC))
      ALLOCATE(AM0N  (ILGA,LEVA,ISEC))
      ALLOCATE(AM3N  (ILGA,LEVA,ISEC))
!
!---------------------------------------------------------------------
!     * HYGROSCOPIC GROWTH EFFECTS ON RADIUS AND PARTICLE DENSITY.
!
      PHI0W =PHI0
      PHIS0W=PHIS0
      WETDN=DRYDN
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ANUM(IL,L,IS) > YTINY .AND. AMAS(IL,L,IS) > YTINY ) THEN
          FGR=WETRC(IL,L,IS)/DRYRC(IS)
          ALFGR=LOG(FGR)
          PHI0W (IL,L,IS)=PHI0W (IL,L,IS)+ALFGR
          PHIS0W(IL,L,IS)=PHIS0W(IL,L,IS)+ALFGR
          FGR3I=1./FGR**3
          WETDN(IL,L,IS)=DNH2O*(1.-FGR3I)+DRYDN(IL,L,IS)*FGR3I
        ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!---------------------------------------------------------------------
!     * SIZE-DEPENDENT COLLECTION EFFICIENCY. FOR RADIUS SMALLER THAN
!     * THE LOW CUTOFF (RCUTL) AND LARGER THAN THE HIGH CUTOFF (RCUTH),
!     * A COLLECTION EFFICIENCY OF UNITY IS ASSUMED. BETWEEN THE CUTOFFS,
!     * A POWER LAW IS APPLIED (OFFS*R**SLOPE).
!
      SLOPE=-1.
      OFFS=5.E-11
      RCUTL=1./OFFS**(1./SLOPE)
      RCUTH=2.E-06
      PHIL=LOG(RCUTL/R0)
      PHIH=LOG(RCUTH/R0)
!
!---------------------------------------------------------------------
!     * INTEGRATION OVER WET SIZE DISTRIBUTION.
!
      AM0=SDINT0(PHI0W,PSI,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=3.
      AM3=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ANUM(IL,L,IS) > YTINY .AND. AMAS(IL,L,IS) > YTINY ) THEN
          PHIS=PHIS0W(IL,L,IS)
          PHIE=PHIS+DPHI0(IL,L,IS)
          PSIT=PSI(IL,L,IS)
          PHI0T=PHI0W(IL,L,IS)
          PHILI=MAX(PHIS,MIN(PHIL,PHIE))
          PHIHI=MIN(PHIE,MAX(PHIH,PHIS))
          DPHIT1=PHILI-PHIS
          AM0T1=SDINTB0(PHI0T,PSIT,PHIS,DPHIT1)
          AM3T1=SDINTB (PHI0T,PSIT,RMOM,PHIS,DPHIT1)
          DPHIT2=PHIHI-PHILI
          AM0T2=OFFS*SDINTB(PHI0T,PSIT,SLOPE,PHILI,DPHIT2)
          TMOM=RMOM+SLOPE
          AM3T2=OFFS*SDINTB(PHI0T,PSIT,TMOM,PHILI,DPHIT2)
          DPHIT3=PHIE-PHIHI
          AM0T3=SDINTB0(PHI0T,PSIT,PHIHI,DPHIT3)
          AM3T3=SDINTB (PHI0T,PSIT,RMOM,PHIHI,DPHIT3)
          AM0N(IL,L,IS)=AM0T1+AM0T2+AM0T3
          AM3N(IL,L,IS)=AM3T1+AM3T2+AM3T3
        ELSE
          AM0N(IL,L,IS)=0.
          AM3N(IL,L,IS)=0.
        ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!     * COLLECTION EFFICIENCIES FOR NUMBER AND MASS.
!
      WHERE ( AM0 > YTINY .AND. AM3 > YTINY .AND. ABS(PSI-YNA) > YTINY )
        PNCEF=AM0N/AM0
        PMCEF=AM3N/AM3
      ELSEWHERE
        PNCEF=1.
        PMCEF=1.
      ENDWHERE
!
!---------------------------------------------------------------------
!     * DEALLOCATE MEMORY.
!
      DEALLOCATE(PHIS0W)
      DEALLOCATE(PHI0W )
      DEALLOCATE(WETDN )
      DEALLOCATE(AM0   )
      DEALLOCATE(AM3   )
      DEALLOCATE(AM0N  )
      DEALLOCATE(AM3N  )
!
      END SUBROUTINE COLLEFF
