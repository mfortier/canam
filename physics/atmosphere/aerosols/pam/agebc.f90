      SUBROUTINE AGEBC(PEDNDT,PEDMDT,PIDNDT,PIDMDT,PIDFDT,PEDDN,PEN0, &
                       PEPHI0,PEPSI,PIFRC,PIMAS,ATAUS,DT,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     AGING OF EXTERNALLY MIXED, HYDROPHOBIC BLACK CARBON.
!
!     HISTORY:
!     --------
!     * MAY 10/2012 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAEXT) :: PEDNDT,PEDMDT
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT) :: PIDNDT,PIDMDT
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIDFDT
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: PEDDN,PEN0, &
                                                       PEPHI0,PEPSI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: PIMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      REAL, INTENT(IN), DIMENSION(ILGA) :: ATAUS
!
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: CIMAST,CIM0BC,CIM3BC, &
                                             CEM0BC,CEM3BC,CIM0OC, &
                                             CIM3OC,CEM0OC,CEM3OC
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: CIMAF
      REAL, DIMENSION(ILGA) :: AFRC,TERM
!
!-----------------------------------------------------------------------
!     * MEMORY ALLOCATION.
!
      IF ( KINT > 0 ) THEN
        ALLOCATE(CIMAST(ILGA,LEVA,ISAINT))
        ALLOCATE(CIMAF (ILGA,LEVA,ISAINT,KINT))
      ENDIF
      IF ( ISINTBC > 0 ) THEN
        ALLOCATE(CIM0BC(ILGA,LEVA,ISINTBC))
        ALLOCATE(CIM3BC(ILGA,LEVA,ISINTBC))
      ENDIF
      IF ( ISINTOC > 0 ) THEN
        ALLOCATE(CIM0OC(ILGA,LEVA,ISINTOC))
        ALLOCATE(CIM3OC(ILGA,LEVA,ISINTOC))
      ENDIF
      IF ( ISEXTBC > 0 ) THEN
        ALLOCATE(CEM0BC(ILGA,LEVA,ISEXTBC))
        ALLOCATE(CEM3BC(ILGA,LEVA,ISEXTBC))
      ENDIF
      IF ( ISEXTOC > 0 ) THEN
        ALLOCATE(CEM0OC(ILGA,LEVA,ISEXTOC))
        ALLOCATE(CEM3OC(ILGA,LEVA,ISEXTOC))
      ENDIF
!
!-----------------------------------------------------------------------
!     * FRACTION OF AEROSOL CONVERTED BY AGING PER TIME STEP.
!
      TERM=-DT/ATAUS
      AFRC=EXP(TERM)
      AFRC=MAX(1.-AFRC,YTINY)
!
!-----------------------------------------------------------------------
!
!     * INTEGRATE NUMBER AND MASS CONCENTRATIONS FOR EXTERNALLY
!     * BC MIXED AEROSOL OVER SIZE SECTIONS OF INTERNALLY MIXED
!     * BC AEROSOL.
!
      RMOM3=3.
      IF ( KEXTBC > 0 .AND. KINTBC > 0 ) THEN
        CIM0BC=0.
        CIM3BC=0.
        CEM0BC=0.
        CEM3BC=0.
        DO IS=1,ISEXTBC
          ISX=IEXBC(IS)
          DO ISD=1,ISINTBC
            ISDX=IINBC(ISD)
            DO L=1,LEVA
            DO IL=1,ILGA
              FPH1=PEPHISS(ISX)
              FPH2=PEPHISS(ISX)+PEDPHIS(ISX)
              FPH1D=PIPHISS(ISDX)
              FPH2D=PIPHISS(ISDX)+PIDPHIS(ISDX)
              IF (   (( FPH1D >= FPH1 .AND. FPH1D <= FPH2 ) &
                 .OR. ( FPH2D >= FPH1 .AND. FPH2D <= FPH2 )) &
                 .AND.  ABS(PEPSI(IL,L,ISX)-YNA) > YTINY &
                 .AND. PEN0(IL,L,ISX) > YTINY ) THEN
                FPHIS1=MAX(FPH1D,FPH1)
                FPHIE1=MIN(FPH2D,FPH2)
                FDPHI1=MAX(FPHIE1-FPHIS1,0.)
                IF ( FDPHI1 > YTINY ) THEN
                  DMOM0=PEN0(IL,L,ISX)*AFRC(IL) &
                                      *SDINTB0(PEPHI0(IL,L,ISX), &
                                         PEPSI(IL,L,ISX),FPHIS1,FDPHI1)
                  CIM0BC(IL,L,ISD)=CIM0BC(IL,L,ISD)+DMOM0
                  CEM0BC(IL,L,IS )=CEM0BC(IL,L,IS )+DMOM0
                  DMOM3=YCNST*PEDDN(IL,L,ISX)*AFRC(IL) &
                               *PEN0(IL,L,ISX)*SDINTB(PEPHI0(IL,L,ISX), &
                                   PEPSI(IL,L,ISX),RMOM3,FPHIS1,FDPHI1)
                  CIM3BC(IL,L,ISD)=CIM3BC(IL,L,ISD)+DMOM3
                  CEM3BC(IL,L,IS )=CEM3BC(IL,L,IS )+DMOM3
                ENDIF
              ENDIF
            ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( KEXTOC > 0 .AND. KINTOC > 0 ) THEN
        CIM0OC=0.
        CIM3OC=0.
        CEM0OC=0.
        CEM3OC=0.
        DO IS=1,ISEXTOC
          ISX=IEXOC(IS)
          DO ISD=1,ISINTOC
            ISDX=IINOC(ISD)
            DO L=1,LEVA
            DO IL=1,ILGA
              FPH1=PEPHISS(ISX)
              FPH2=PEPHISS(ISX)+PEDPHIS(ISX)
              FPH1D=PIPHISS(ISDX)
              FPH2D=PIPHISS(ISDX)+PIDPHIS(ISDX)
              IF (   (( FPH1D >= FPH1 .AND. FPH1D <= FPH2 ) &
                 .OR. ( FPH2D >= FPH1 .AND. FPH2D <= FPH2 )) &
                 .AND.  ABS(PEPSI(IL,L,ISX)-YNA) > YTINY &
                 .AND. PEN0(IL,L,ISX) > YTINY ) THEN
                FPHIS1=MAX(FPH1D,FPH1)
                FPHIE1=MIN(FPH2D,FPH2)
                FDPHI1=MAX(FPHIE1-FPHIS1,0.)
                IF ( FDPHI1 > YTINY ) THEN
                  DMOM0=PEN0(IL,L,ISX)*AFRC(IL) &
                                      *SDINTB0(PEPHI0(IL,L,ISX), &
                                         PEPSI(IL,L,ISX),FPHIS1,FDPHI1)
                  CIM0OC(IL,L,ISD)=CIM0OC(IL,L,ISD)+DMOM0
                  CEM0OC(IL,L,IS )=CEM0OC(IL,L,IS )+DMOM0
                  DMOM3=YCNST*PEDDN(IL,L,ISX)*AFRC(IL) &
                               *PEN0(IL,L,ISX)*SDINTB(PEPHI0(IL,L,ISX), &
                                   PEPSI(IL,L,ISX),RMOM3,FPHIS1,FDPHI1)
                  CIM3OC(IL,L,ISD)=CIM3OC(IL,L,ISD)+DMOM3
                  CEM3OC(IL,L,IS )=CEM3OC(IL,L,IS )+DMOM3
                ENDIF
              ENDIF
            ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * MASS AND NUMBER TENDENCIES FOR EXTERNALLY MIXED AEROSOL.
!
      IF ( KEXT > 0 ) THEN
        PEDNDT=0.
        PEDMDT=0.
      ENDIF
      IF ( KEXTBC > 0 ) THEN
        DO IS=1,ISEXTBC
          WHERE ( CEM0BC(:,:,IS) > YTINY .AND. CEM3BC(:,:,IS) > YTINY )
            PEDNDT(:,:,IEXBC(IS))=PEDNDT(:,:,IEXBC(IS)) &
                                 -CEM0BC(:,:,IS)/DT
            PEDMDT(:,:,IEXBC(IS))=PEDMDT(:,:,IEXBC(IS)) &
                                 -CEM3BC(:,:,IS)/DT
          ENDWHERE
        ENDDO
      ENDIF
      IF ( KEXTOC > 0 ) THEN
        DO IS=1,ISEXTOC
          WHERE ( CEM0OC(:,:,IS) > YTINY .AND. CEM3OC(:,:,IS) > YTINY )
            PEDNDT(:,:,IEXOC(IS))=PEDNDT(:,:,IEXOC(IS)) &
                                 -CEM0OC(:,:,IS)/DT
            PEDMDT(:,:,IEXOC(IS))=PEDMDT(:,:,IEXOC(IS)) &
                                 -CEM3OC(:,:,IS)/DT
          ENDWHERE
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * TOTAL AEROSOL MASS AFTER ACCOUNTING FOR CHANGES FROM AGING
!     * FOR INTERNALLY MIXED TYPES OF AEROSOL.
!
      IF ( KINT > 0 ) THEN
        CIMAST=PIMAS
      ENDIF
      IF ( KINTBC > 0 ) THEN
        DO IS=1,ISINTBC
          WHERE ( CIM0BC(:,:,IS) > YTINY .AND. CIM3BC(:,:,IS) > YTINY )
            CIMAST(:,:,IINBC(IS))=CIMAST(:,:,IINBC(IS))+CIM3BC(:,:,IS)
          ENDWHERE
        ENDDO
      ENDIF
      IF ( KINTOC > 0 ) THEN
        DO IS=1,ISINTOC
          WHERE ( CIM0OC(:,:,IS) > YTINY .AND. CIM3OC(:,:,IS) > YTINY )
            CIMAST(:,:,IINOC(IS))=CIMAST(:,:,IINOC(IS))+CIM3OC(:,:,IS)
          ENDWHERE
        ENDDO
      ENDIF
!
!     * NEW MASS FRACTIONS FOR INTERNALLY MIXED TYPES OF AEROSOL.
!
      IF ( KINT > 0 ) THEN
        DO IS=1,ISAINT
        DO L=1,LEVA
        DO IL=1,ILGA
          IF ( CIMAST(IL,L,IS) > YTINY ) THEN
            CIMAF(IL,L,IS,:)=PIFRC(IL,L,IS,:)*PIMAS(IL,L,IS) &
                            /CIMAST(IL,L,IS)
          ELSE
            CIMAF(IL,L,IS,:)=PIFRC(IL,L,IS,:)
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
      IF ( KINTBC > 0 ) THEN
        DO IS=1,ISINTBC
        DO L=1,LEVA
        DO IL=1,ILGA
          IF (           CIMAST(IL,L,IINBC(IS)) > YTINY &
                   .AND. CIM0BC(IL,L,IS) > YTINY &
                   .AND. CIM3BC(IL,L,IS) > YTINY ) THEN
            CIMAF(IL,L,IINBC(IS),KINTBC)=CIMAF(IL,L,IINBC(IS),KINTBC) &
                               +CIM3BC(IL,L,IS)/CIMAST(IL,L,IINBC(IS))
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
      IF ( KINTOC > 0 ) THEN
        DO IS=1,ISINTOC
        DO L=1,LEVA
        DO IL=1,ILGA
          IF (           CIMAST(IL,L,IINOC(IS)) > YTINY &
                   .AND. CIM0OC(IL,L,IS) > YTINY &
                   .AND. CIM3OC(IL,L,IS) > YTINY ) THEN
            CIMAF(IL,L,IINOC(IS),KINTOC)=CIMAF(IL,L,IINOC(IS),KINTOC) &
                               +CIM3OC(IL,L,IS)/CIMAST(IL,L,IINOC(IS))
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * MASS, NUMBER, AND MASS FRACTION TENDENCIES FOR INTERNALLY
!     * MIXED TYPES OF AEROSOL.
!
      IF ( KINT > 0 ) THEN
        PIDNDT=0.
        PIDMDT=0.
        PIDFDT=(CIMAF-PIFRC)/DT
      ENDIF
      IF ( KINTBC > 0 ) THEN
        DO IS=1,ISINTBC
          WHERE ( CIM0BC(:,:,IS) > YTINY .AND. CIM3BC(:,:,IS) > YTINY )
            PIDNDT(:,:,IINBC(IS))=PIDNDT(:,:,IINBC(IS)) &
                                 +CIM0BC(:,:,IS)/DT
            PIDMDT(:,:,IINBC(IS))=PIDMDT(:,:,IINBC(IS)) &
                                 +CIM3BC(:,:,IS)/DT
          ENDWHERE
        ENDDO
      ENDIF
      IF ( KINTOC > 0 ) THEN
        DO IS=1,ISINTOC
          WHERE ( CIM0OC(:,:,IS) > YTINY .AND. CIM3OC(:,:,IS) > YTINY )
            PIDNDT(:,:,IINOC(IS))=PIDNDT(:,:,IINOC(IS)) &
                                 +CIM0OC(:,:,IS)/DT
            PIDMDT(:,:,IINOC(IS))=PIDMDT(:,:,IINOC(IS)) &
                                 +CIM3OC(:,:,IS)/DT
          ENDWHERE
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * MEMORY DEALLOCATION.
!
      IF ( KINT > 0 ) THEN
        DEALLOCATE(CIMAST)
        DEALLOCATE(CIMAF )
      ENDIF
      IF ( ISINTBC > 0 ) THEN
        DEALLOCATE(CIM0BC)
        DEALLOCATE(CIM3BC)
      ENDIF
      IF ( ISINTOC > 0 ) THEN
        DEALLOCATE(CIM0OC)
        DEALLOCATE(CIM3OC)
      ENDIF
      IF ( ISEXTBC > 0 ) THEN
        DEALLOCATE(CEM0BC)
        DEALLOCATE(CEM3BC)
      ENDIF
      IF ( ISEXTOC > 0 ) THEN
        DEALLOCATE(CEM0OC)
        DEALLOCATE(CEM3OC)
      ENDIF
!
      END SUBROUTINE AGEBC
