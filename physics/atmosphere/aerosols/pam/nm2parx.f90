      SUBROUTINE NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                          PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                          PINUM,PIMAS,PIFRC, &
                          CNR,CMR,RN1R,RM1R,VN1R,VM1R, &
                          RN2R,RM2R,VN2R,VM2R,VI,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     TRANSFORMATION OF NUMBER AND MASS TO PLA PARAMETERS, INCLUDING
!     CALCULATION OF AEROSOL DENSITY FOR INTERNALLY MIXED AEROSOL AND
!     MASS AND NUMBER CORRECTIONS, IF NECESSARY.
!
!     HISTORY:
!     --------
!     * FEB 04/2015 - K.VONSALZEN   COSMETIC CHANGE FOR GCM18:
!     *                             PENUM,PEMAS AND PINUM,PIMAS DECLARED
!     *                             WITH INTENT(INOUT) INSTEAD OF INTENT(IN).
!     * MAY 26/2010 - K.VONSALZEN   CHANGE ARGUMENT LIST
!     * JAN 12/2009 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) ::   CNR,CMR,RN1R,RM1R, &
                                                     RN2R,RM2R
      REAL, INTENT(INOUT), DIMENSION(ILGA) ::        VN1R,VM1R,VN2R, &
                                                     VM2R
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PENUM,PEMAS
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PINUM,PIMAS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PEN0,PEPHI0,PEPSI
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIN0,PIPHI0,PIPSI, &
                                                   PIDDN
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: VI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PEDDN,PEPHIS0,PEDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIPHIS0,PIDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      REAL, DIMENSION(ILGA,LEVA) :: CORN,CORM,RESN,RESM,CORNT,CORMT, &
                                    RESNT,RESMT
      LOGICAL :: KARGS1,KARGS2
!
!-----------------------------------------------------------------------
!     * UPDATE DRY PARTICLE DENSITY (INTERNALLY MIXED AEROSOL).
!
      IF ( ISAINT > 0 ) CALL SDDENS(PIDDN,PIFRC,ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * INITIALIZATIONS.
!
      KARGS1=.TRUE.
      KARGS2=.TRUE.
!
      CORN=0.
      CORM=0.
      RESN=0.
      RESM=0.
!
!-----------------------------------------------------------------------
!     * AEROSOL NUMBER AND MASS ADJUSTMENTS.
!
      IF ( ISAEXT > 0 ) THEN
        CALL CORNMI(RESMT,RESNT,CORMT,CORNT,PENUM,PEMAS,PEDDN, &
                    PEISMIN,PEISMAX,PEPHISS,PEDPHIS,ILGA,LEVA,ISAEXT)
        CORN=CORN+CORNT
        CORM=CORM+CORMT
        RESN=RESN+RESNT
        RESM=RESM+RESMT
      ENDIF
      IF ( ISAINT > 0 ) THEN
        CALL CORNMI(RESMT,RESNT,CORMT,CORNT,PINUM,PIMAS,PIDDN, &
                    PIISMIN,PIISMAX,PIPHISS,PIDPHIS,ILGA,LEVA,ISAINT)
        CORN=CORN+CORNT
        CORM=CORM+CORMT
        RESN=RESN+RESNT
        RESM=RESM+RESMT
      ENDIF
!
!     * DIAGNOSE NUMBER AND MASS CORRECTION TERMS.
!
      IF ( KARGS1 ) THEN
        CNR=CNR+CORN
        CMR=CMR+CORM
        RN1R=RN1R+RESN
        RM1R=RM1R+RESM
        DO L=1,LEVA
          VN1R(:)=VN1R(:)+VI(:,L)*RESN(:,L)
          VM1R(:)=VM1R(:)+VI(:,L)*RESM(:,L)
        ENDDO
      ENDIF
!
!     * UPDATE BASIC PLA PARAMETERS.
!
      IF ( ISAEXT > 0 ) THEN
        CALL NM2PLA(PEN0,PEPHI0,PEPSI,RESN,RESM,PENUM,PEMAS, &
                    PEDDN,PEPHISS,PEDPHIS,PEPHIS0,PEDPHI0, &
                    ILGA,LEVA,ISAEXT,KPEXT)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        CALL NM2PLA(PIN0,PIPHI0,PIPSI,RESN,RESM,PINUM,PIMAS, &
                    PIDDN,PIPHISS,PIDPHIS,PIPHIS0,PIDPHI0, &
                    ILGA,LEVA,ISAINT,KPINT)
      ENDIF
!
!     * DIAGNOSE NUMBER AND MASS CORRECTION TERMS.
!
      IF ( KARGS2 ) THEN
        RN2R=RN2R+RESN
        RM2R=RM2R+RESM
        DO L=1,LEVA
          VN2R(:)=VN2R(:)+VI(:,L)*RESN(:,L)
          VM2R(:)=VM2R(:)+VI(:,L)*RESM(:,L)
        ENDDO
      ENDIF
!
      END SUBROUTINE NM2PARX
