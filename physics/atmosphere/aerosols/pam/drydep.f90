      SUBROUTINE DRYDEP(ILGA,LEVA,ZSPD,FCAN,CDML,CDMNL,FLND,ICAN, &
                        FCS,FGS,FC,FG,T,P,DP,DT,RHOA,GRAV, &
                        ANUM,AMAS,DRYDN,STICK,WETRC,DRYRC, &
                        PHI0,PHIS0,DPHI0,PSI,ISEC, &
                        ADNDT,ADMDT,FLXN,FLXM,VDN,VDM)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DRY DEPOSITION VELOCITIES FOR AEROSOL NUMBER AND MASS
!     CONCENTRATIONS. THE PARAMETERIZATION SCHME OF ZHANG ET AL.
!     REFERENCE: ZHANG L. (2001), ATMOSPHERIC ENVIORMENT,35,549-560
!
!     HISTORY:
!     --------
!     * APR 17/2015 - K.VONSALZEN   CHANGES TO SUPPORT SINGLE PRECISION
!     * FEB 05/2015 - M.LAZARE.     NEW VERSION FOR GCM18:
!     *                             - {FLND,CDML,CDMNL} PASSED IN AND
!     *                               USED IN CALL TO VSRAD INSTEAD OF
!     *                               {GC,CDM}, FOR FRACTIONAL LAND MASK
!     *                               SUPPORT.
!     * FEB 12/2010 - K.VONSALZEN   REMOVED ZLN AND SIM
!     * JAN 20/2008 - X.MA:         EXTRACT NECESSARY FIELDS FROM CLASS
!     * JUN 21/2007 - X.MA:
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA) :: CDML,CDMNL,FLND,ZSPD, &
                                           FCS,FGS,FC,FG
      REAL, INTENT(IN), DIMENSION(ILGA,ICAN+1) :: FCAN
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: T,RHOA,DP,P
!
      REAL, INTENT(IN), DIMENSION(ISEC) :: DRYRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) ::DRYDN,WETRC, &
                                         PHI0,PHIS0,DPHI0,PSI, &
                                         ANUM,AMAS,STICK
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: ADNDT,ADMDT
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: FLXN,FLXM
      REAL, INTENT(OUT), DIMENSION(ILGA,ISEC) :: VDN,VDM
      REAL(R8), DIMENSION(ILGA,LEVA,ISEC) :: AM0,AM1,AM2,AM3,AM4,AM5
      REAL, DIMENSION(ILGA,LEVA,ISEC) :: PHI0W,PHIS0W,WETDN
      REAL, DIMENSION(ILGA) :: VCFL
      REAL, DIMENSION(ILGA,LEVA,ISEC) :: RAWL,RADL,RAWR,RADR,ASCN,ASCM, &
                                         ASC
      REAL, DIMENSION(ILGA,ISEC) :: VDL,VDR
!
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEFN,PEFM,DVDN,DVDM
      REAL, ALLOCATABLE, DIMENSION(:,:) :: PESFN,PESFM
!
!-----------------------------------------------------------------------
!
      ALLOCATE(PEFN   (ILGA,LEVA,ISEC))
      ALLOCATE(PEFM   (ILGA,LEVA,ISEC))
      ALLOCATE(PESFN  (ILGA,ISEC))
      ALLOCATE(PESFM  (ILGA,ISEC))
      ALLOCATE(DVDN   (ILGA,LEVA,ISEC))
      ALLOCATE(DVDM   (ILGA,LEVA,ISEC))
!
!     * SIZE-RELATED PARAMETERS.
!
      PHI0W =PHI0
      PHIS0W=PHIS0
      WETDN=DRYDN
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ANUM(IL,L,IS) > YTINY .AND. AMAS(IL,L,IS) > YTINY &
                             .AND. ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
          FGR=WETRC(IL,L,IS)/DRYRC(IS)
          ALFGR=LOG(FGR)
          PHI0W (IL,L,IS)=PHI0W (IL,L,IS)+ALFGR
          PHIS0W(IL,L,IS)=PHIS0W(IL,L,IS)+ALFGR
          FGR3I=1./FGR**3
          WETDN(IL,L,IS)=DNH2O*(1.-FGR3I)+DRYDN(IL,L,IS)*FGR3I
        ENDIF
      ENDDO
      ENDDO
      ENDDO
!
      AM0=SDINT0(PHI0W,PSI,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=1.
      AM1=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=2.
      AM2=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=3.
      AM3=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=4.
      AM4=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
      RMOM=5.
      AM5=SDINT (PHI0W,PSI,RMOM,PHIS0W,DPHI0,ILGA,LEVA,ISEC)
!
!----------------------------------------------------------------------- &
!      DRY DEPOSITION VELOCITY.
!
      DO IS=1,ISEC
        RAWL(:,:,IS)=R0*EXP(PHIS0W(:,:,IS))
        RADL(:,:,IS)=R0*EXP(PHIS0 (:,:,IS))
      ENDDO
      CALL VSRAD(RAWL,RADL,STICK,T,P,RHOA,FLND,FCAN, &
                 FCS,FGS,FC,FG,ZSPD,CDML,CDMNL, &
                 VDL,GRAV,ILGA,LEVA,ISEC,ICAN)
!
      DO IS=1,ISEC
        RAWR(:,:,IS)=R0*EXP(PHIS0W(:,:,IS)+DPHI0(:,:,IS))
        RADR(:,:,IS)=R0*EXP(PHIS0 (:,:,IS)+DPHI0(:,:,IS))
      ENDDO
      CALL VSRAD(RAWR,RADR,STICK,T,P,RHOA,FLND,FCAN, &
                 FCS,FGS,FC,FG,ZSPD,CDML,CDMNL, &
                 VDR,GRAV,ILGA,LEVA,ISEC,ICAN)
!
      DVDM=0.
      DVDN=0.
      FLXN=0.
      FLXM=0.
      ADNDT=0.
      ADMDT=0.
!
      L=LEVA
      DO IS=1,ISEC
      DO IL=1,ILGA
        IF ( AM0(IL,L,IS) > YTINY8 .AND. AM3(IL,L,IS) > YTINY8 &
                             .AND. ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
          DVDP=VDR(IL,IS)-VDL(IL,IS)
          DEPHI=EXP(PHIS0W(IL,L,IS)+DPHI0(IL,L,IS)) &
               -EXP(PHIS0W(IL,L,IS))
          DVDN(IL,L,IS)=VDL(IL,IS) &
                 +DVDP/DEPHI*(AM1(IL,L,IS)/AM0(IL,L,IS))/R0 &
                 -DVDP/DEPHI*EXP(PHIS0W(IL,L,IS))
          DVDM(IL,L,IS)=VDL(IL,IS) &
                 +DVDP/DEPHI*(AM4(IL,L,IS)/AM3(IL,L,IS))/R0 &
                 -DVDP/DEPHI*EXP(PHIS0W(IL,L,IS))
        ENDIF
      ENDDO
      ENDDO
!
!     * MAXIMUM FALL VELOCITY ACCORDING TO CFL CRITERION.
!
      VCFL=.999*DP(:,LEVA)/(RHOA(:,LEVA)*GRAV*DT)
      ASCM=1.
      ASCN=1.
      DO IS=1,ISEC
      DO IL=1,ILGA
        IF ( DVDM(IL,LEVA,IS) > MAX(VCFL(IL)/YLARGE,YTINY) ) THEN
          ASCM(IL,LEVA,IS)=VCFL(IL)/DVDM(IL,LEVA,IS)
        END IF
        IF ( DVDN(IL,LEVA,IS) > MAX(VCFL(IL)/YLARGE,YTINY) ) THEN
          ASCN(IL,LEVA,IS)=VCFL(IL)/DVDN(IL,LEVA,IS)
        END IF
      ENDDO
      ENDDO
      ASC=MIN(1.,ASCM,ASCN)
      DVDM=MAX(DVDM*ASC,0.)
      DVDN=MAX(DVDN*ASC,0.)
!
!-----------------------------------------------------------------------
!     * SURFACE REMOVAL.
!
      L=LEVA
      DO IS=1,ISEC
        ADNDT(:,L,IS)=-ANUM(:,L,IS)*DVDN(:,L,IS)*RHOA(:,L)*GRAV/DP(:,L)
        ADMDT(:,L,IS)=-AMAS(:,L,IS)*DVDM(:,L,IS)*RHOA(:,L)*GRAV/DP(:,L)
      ENDDO
!
!     * DRY DEPOSITION FLUX.
!
      DO IS=1,ISEC
        FLXN(:,L,IS)=ANUM(:,L,IS)*RHOA(:,L)*DVDN(:,L,IS)
        FLXM(:,L,IS)=AMAS(:,L,IS)*RHOA(:,L)*DVDM(:,L,IS)
      ENDDO
!
!     * DRY DEPOSITION VELOCY (M/S).
!
      DO IS=1,ISEC
        VDN(:,IS)=DVDN(:,L,IS)
        VDM(:,IS)=DVDM(:,L,IS)
      ENDDO
!
      DEALLOCATE(PESFN  )
      DEALLOCATE(PESFM  )
      DEALLOCATE(PEFN   )
      DEALLOCATE(PEFM   )
      DEALLOCATE(DVDN   )
      DEALLOCATE(DVDM   )
!
      END SUBROUTINE DRYDEP
