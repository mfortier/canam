      SUBROUTINE COAGM(DMDTS,CTM,KERN,FIFRCS,FIDDN,FMOM0,FMOM3,FMOM6, &
                       FMOM0I,FMOM0D,FMOM3I,FMOM3D,VOLGF,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     VARIOUS TERMS IN COAGULATION EQUATION FOR AEROSOL MASS.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------

      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISFINT,ISAINT) :: DMDTS
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISFINT) :: FMOM0,FMOM3, &
                                                           FMOM6
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISFINT) ::  FIFRCS,FIDDN
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISFTRIM) :: &
                                                        FMOM0I,FMOM0D, &
                                                        FMOM3I,FMOM3D
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISFTRI) :: KERN,CTM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) ::  VOLGF
      REAL(R8), DIMENSION(ILGA,LEVA) ::          DMDTT
!
!-----------------------------------------------------------------------
!
      DMDTS=0.
!
!     * FIRST AND THIRD TERM IN MASS TENDENCY EQUATION.
!
      DO IS=2,ISFINT
        IR1=IS-1
        IR2=IR1
        ITS=ITR(IR1,IR2)
        DMDTT=FIFRCS(:,:,IR1)*CTM(:,:,ITS)
        IRX=IRO(IR1,IR2)
        DMDTS(:,:,IS-1,IRX)=DMDTS(:,:,IS-1,IRX)-DMDTT
        DMDTS(:,:,IS  ,IRX)=DMDTS(:,:,IS  ,IRX)+DMDTT
      ENDDO
      IS=ISFINT+1
      IR1=IS-1
      IR2=IR1
      ITS=ITR(IR1,IR2)
      DMDTT=FIFRCS(:,:,IR1)*CTM(:,:,ITS)
      IRX=IRO(IR1,IR2)
      DMDTS(:,:,IS-1,IRX)=DMDTS(:,:,IS-1,IRX)-DMDTT
!
!     * SECOND AND FOURTH TERM IN MASS TENDENCY EQUATION.
!
      DO IS=3,ISFINT
        IR1=IS-1
        DO IK=1,IS-2
          IR2=IK
          ITS=ITR(IR1,IR2)
          ITSM=ITRM(IR1,IR2)
          DMDTT= FIFRCS(:,:,IS-1) &
                 *(FMOM0(:,:,IK)*KERN(:,:,ITS)*FMOM3I(:,:,ITSM) &
                  +FMOM3(:,:,IK)*KERN(:,:,ITS)*FMOM3D(:,:,ITSM)) &
                +FIFRCS(:,:,IK) &
                 *(FMOM3(:,:,IK)*KERN(:,:,ITS)*FMOM0I(:,:,ITSM) &
                  +FMOM6(:,:,IK)*KERN(:,:,ITS)*FMOM0D(:,:,ITSM))
          IRX=IRO(IR1,IR2)
          DMDTS(:,:,IS-1,IRX)=DMDTS(:,:,IS-1,IRX)-DMDTT
          DMDTS(:,:,IS  ,IRX)=DMDTS(:,:,IS  ,IRX)+DMDTT
        ENDDO
      ENDDO
      IS=ISFINT+1
      IR1=IS-1
      DO IK=1,IS-2
        IR2=IK
        ITS=ITR(IR1,IR2)
        ITSM=ITRM(IR1,IR2)
        DMDTT= FIFRCS(:,:,IS-1) &
                 *(FMOM0(:,:,IK)*KERN(:,:,ITS)*FMOM3I(:,:,ITSM) &
                  +FMOM3(:,:,IK)*KERN(:,:,ITS)*FMOM3D(:,:,ITSM)) &
                +FIFRCS(:,:,IK) &
                 *(FMOM3(:,:,IK)*KERN(:,:,ITS)*FMOM0I(:,:,ITSM) &
                  +FMOM6(:,:,IK)*KERN(:,:,ITS)*FMOM0D(:,:,ITSM))
        IRX=IRO(IR1,IR2)
        DMDTS(:,:,IS-1,IRX)=DMDTS(:,:,IS-1,IRX)-DMDTT
      ENDDO
!
!     * FIFTH AND SIXTH TERM IN MASS TENDENCY EQUATION.
!
      IS=1
      IR1=IS
      DO IK=1,IS-1
        IR2=IK
        ITS=ITR(IR1,IR2)
        DMDTT=-FIFRCS(:,:,IR2)*CTM(:,:,ITS)
        IRX=IRO(IR1,IR2)
        DMDTS(:,:,IS,IRX)=DMDTS(:,:,IS,IRX)+DMDTT
      ENDDO
      DO IRX=1,ISAINT
      DO IS=1,ISFINT
        DMDTS(:,:,IS,IRX)=DMDTS(:,:,IS,IRX) &
                                        *YCNST*FIDDN(:,:,IS)/VOLGF(:,:)
      ENDDO
      ENDDO
!
      END SUBROUTINE COAGM
