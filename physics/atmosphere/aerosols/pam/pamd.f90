      SUBROUTINE PAMD(TRAC,TA,RHA,PHA,SSLD,RESS,VESS,MDLD,REMD,VEMD, &
                      BCLD,REBC,VEBC,OCLD,REOC,VEOC,AMLD,REAM,VEAM, &
                      FR1,FR2,SO4LDI,RESO4I,VESO4I,FR1SO4I,FR2SO4I, &
                      BCLDI,REBCI,VEBCI,FR1BCI,FR2BCI,OCLDI, &
                      REOCI,VEOCI,FR1OCI,FR2OCI,PM2P5,PM10,PM2P5D,PM10D, &
                      DPA,CLDA,RMUA,TNSS,TNMD,TNIA,TMSS,TMMD,TMOC, &
                      TMBC,TMSP,SNSS,SNMD,SNIA,SMSS,SMMD,SMOC,SMBC, &
                      SMSP,SIWH,SEWH,SDNU,SDMA,SDAC,SDCO,SSSN,SMDN, &
                      SIAN,SSSM,SMDM,SEWM,SSUM,SOCM,SBCM,SIWM,SDVL, &
                      LEVA,ILGA,IRFRC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DIAGNOSTIC AEROSOL PARAMETERS FOR RADIATION AND OTHER MODEL
!     COMPONENTS EXTERNAL TO PAM.
!
!     HISTORY:
!     --------
!     * MAR 5/2018  - K.VONSALZEN   ADD PM2.5 AND PM10
!     * JUN 10/2014 - K.VONSALZEN   UPPER BOUND ON RELATIVE HUMIDITY,
!     *                             ADDITION OF DIAGNOSTIC CODE FOR
!     *                             SIZE DISTRIBUTIONS FROM PAM
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDPHYS
      USE COMPAR
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
!     * PAM INPUT VARIABLES.
!
      REAL, INTENT(IN), DIMENSION(ILGA) :: CLDA,RMUA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: TA,RHA,DPA,PHA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,NTRACT) :: TRAC
!
!     * PAM OUTPUT VARIABLES.
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: SSLD,RESS,VESS, &
                                                 MDLD,REMD,VEMD,AMLD, &
                                                 REAM,VEAM,FR1,FR2, &
                                                 BCLD,REBC,VEBC,OCLD, &
                                                 REOC,VEOC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: SO4LDI,RESO4I, &
                                                 VESO4I,FR1SO4I,FR2SO4I, &
                                                 BCLDI,REBCI, &
                                                 VEBCI,FR1BCI,FR2BCI, &
                                                 OCLDI,REOCI, &
                                                 VEOCI,FR1OCI,FR2OCI
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: PM2P5,PM10,PM2P5D,PM10D
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISDNUM) :: &
                                                   SDNU,SDMA,SDAC,SDCO, &
                                                   SSSN,SMDN,SIAN,SSSM, &
                                                   SMDM,SEWM,SSUM,SOCM, &
                                                   SBCM,SIWM
      REAL, INTENT(OUT), DIMENSION(ILGA)        :: TNSS,TNMD,TNIA,TMSS, &
                                                   TMMD,TMOC,TMBC,TMSP
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDNUM) :: SNSS,SNMD,SNIA,SMSS, &
                                                   SMMD,SMOC,SMBC,SMSP, &
                                                   SIWH,SEWH
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDIAG) :: SDVL
!
!     * WORK ARRAYS.
!
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEDPHI0,PEPHIS0,PEDDN, &
                                             PEWETRC, &
                                             PIDPHI0,PIPHIS0,PIDDN, &
                                             PIWETRC
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEN0,PEPHI0,PEPSI,PENUM, &
                                             PEMAS, &
                                             PIN0,PIPHI0,PIPSI,PINUM, &
                                             PIMAS
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PIDM,PIDN,TIDDN,TIN0, &
                                             TIPHI0,TIPSI,TIWETRC, &
                                             TINUM,TIMAS
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PIFRC,PIDF,TIFRC
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PERE,PEVE,PELOAD
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: PIRE,PIVE,PILOAD
      REAL, ALLOCATABLE, DIMENSION(:,:) :: VITRM,DIRSMN,DIRSN, &
                                           DIRSMS
      REAL, ALLOCATABLE, DIMENSION(:,:)     :: RHOA
      REAL, ALLOCATABLE, DIMENSION(:,:)     :: VVOL
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: FNUMO,FMASO
      REAL, ALLOCATABLE, DIMENSION(:,:,:)   :: FMASS,FMASSD
      REAL, ALLOCATABLE, DIMENSION(:)   :: RADC
      CHARACTER(LEN=70) :: CNAME
!
!-----------------------------------------------------------------------
!     * ALLOCATE MEMORY FOR PLA SECTION SIZE PARAMETER ARRAYS.
!
      ALLOCATE(VITRM(ILGA,LEVA))
      ALLOCATE(DIRSMN(ILGA,LEVA))
      ALLOCATE(DIRSN(ILGA,LEVA))
      ALLOCATE(DIRSMS(ILGA,LEVA))
      ALLOCATE(RHOA(ILGA,LEVA))
      IF ( ISAEXT > 0 ) THEN
        ALLOCATE(PEPHIS0(ILGA,LEVA,ISAEXT))
        ALLOCATE(PEDPHI0(ILGA,LEVA,ISAEXT))
        ALLOCATE(PEDDN  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEN0   (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEPHI0 (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEPSI  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEWETRC(ILGA,LEVA,ISAEXT))
        ALLOCATE(PENUM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEMAS  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PERE   (ILGA,LEVA,KEXT))
        ALLOCATE(PEVE   (ILGA,LEVA,KEXT))
        ALLOCATE(PELOAD (ILGA,LEVA,KEXT))
      ENDIF
      IF ( ISAINT > 0 ) THEN
        ALLOCATE(PIPHIS0(ILGA,LEVA,ISAINT))
        ALLOCATE(PIDPHI0(ILGA,LEVA,ISAINT))
        ALLOCATE(PIDDN  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIN0   (ILGA,LEVA,ISAINT))
        ALLOCATE(PIPHI0 (ILGA,LEVA,ISAINT))
        ALLOCATE(PIPSI  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIWETRC(ILGA,LEVA,ISAINT))
        ALLOCATE(PINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIFRC  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIRE   (ILGA,LEVA))
        ALLOCATE(PIVE   (ILGA,LEVA))
        ALLOCATE(PILOAD (ILGA,LEVA))
        ALLOCATE(PIDM   (ILGA,LEVA,ISAINT))
        ALLOCATE(PIDN   (ILGA,LEVA,ISAINT))
        ALLOCATE(PIDF   (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(TIDDN  (ILGA,LEVA,ISAINT))
        ALLOCATE(TIN0   (ILGA,LEVA,ISAINT))
        ALLOCATE(TIPHI0 (ILGA,LEVA,ISAINT))
        ALLOCATE(TIPSI  (ILGA,LEVA,ISAINT))
        ALLOCATE(TIWETRC(ILGA,LEVA,ISAINT))
        ALLOCATE(TINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(TIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(TIFRC  (ILGA,LEVA,ISAINT,KINT))
      ENDIF
!
!-----------------------------------------------------------------------
!     * AEROSOL SIZE INFORMATION.
!
      DO IS=1,ISAEXT
        PEDPHI0(:,:,IS)=PEDPHIS(IS)
        PEPHIS0(:,:,IS)=PEPHISS(IS)
      ENDDO
      DO IS=1,ISAINT
        PIDPHI0(:,:,IS)=PIDPHIS(IS)
        PIPHIS0(:,:,IS)=PIPHISS(IS)
      ENDDO
!
!     * DRY PARTICLE DENSITIES FOR EXTERNALLY MIXED AEROSOL TYPES.
!
      DO IS=1,ISAEXT
        KX=SEXTF%ISAER(IS)%ITYP
        PEDDN  (:,:,IS)=AEXTF%TP(KX)%DENS
      ENDDO
!
!     * INITIALIZE DRY PARTICLE DENSITIES FOR INTERNALLY MIXED
!     * AEROSOL TYPES. THE DENSITIES ARE LATER CALCULATED AS A
!     * FUNCTION OF AEROSOL MASS.
!
      DO IS=1,ISAINT
        PIDDN  (:,:,IS)=YNA
      ENDDO
!
!     * EXTRACT PLA AEROSOL NUMBER AND MASS CONCENTRATIONS FROM BASIC
!     * TRACER ARRAYS IN THE GCM.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=TRAC(:,:,INSEX:INFEX)
        PEMAS=TRAC(:,:,IMSEX:IMFEX)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=TRAC(:,:,INSIN:INFIN)
      ENDIF
!
!     * TOTAL MASS AND MASS FRACTIONS FOR INTERNALLY MIXED TYPES
!     * OF AEROSOL.
!
      IF ( ISAINT > 0 ) PIMAS=0.
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        PIMAS(:,:,IS)=PIMAS(:,:,IS)+TRAC(:,:,IMLIN)
      ENDDO
      ENDDO
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        KX=SINTF%ISAER(IS)%ITYP(IST)
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        DO L=1,LEVA
        DO IL=1,ILGA
          XRTMP=TRAC(IL,L,IMLIN)
          IF ( PIMAS(IL,L,IS) > MAX(XRTMP/YLARGE,YTINY) ) THEN
            IF ( XRTMP > YTINY ) THEN
              PIFRC(IL,L,IS,KX)=MIN(MAX(XRTMP/PIMAS(IL,L,IS),0.),1.)
            ELSE
              PIFRC(IL,L,IS,KX)=0.
            ENDIF
          ELSE
            PIFRC(IL,L,IS,KX)=1./REAL(SINTF%ISAER(IS)%ITYPT)
          ENDIF
        ENDDO
        ENDDO
      ENDDO
      ENDDO
      RHOA=PHA/(RGAS*TA)
!
!-----------------------------------------------------------------------
!     * DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
      CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                   PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                   PINUM,PIMAS,PIFRC,ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * AEROSOL RADIATIVE PROPERTIES FOR EXTERNALLY MIXED AEROSOL SPECIES.
!
      IF ( KEXT > 0 ) THEN
        DO IS=1,ISAEXT
          PEWETRC(:,:,IS)=PEDRYRC(IS)
        ENDDO
        CALL RADPARE(PENUM,PEMAS,PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                     PEWETRC,PEDDN,PERE,PEVE,PELOAD, &
                     ILGA,LEVA)
      ENDIF
      SSLD=0.
      RESS=0.
      VESS=0.
      IF ( KEXTSS > 0 ) THEN
        SSLD=PELOAD(:,:,KEXTSS)
!!!!
! id  oaerrad
!        SSLD=0.
        RESS=PERE(:,:,KEXTSS)
        VESS=PEVE(:,:,KEXTSS)
      ENDIF
      MDLD=0.
      REMD=0.
      VEMD=0.
      IF ( KEXTMD > 0 ) THEN
        MDLD=PELOAD(:,:,KEXTMD)
!!!!
! id  oaerrad
!        MDLD=0.
        REMD=PERE(:,:,KEXTMD)
        VEMD=PEVE(:,:,KEXTMD)
      ENDIF
      BCLD=0.
      REBC=0.
      VEBC=0.
      IF ( KEXTBC > 0 ) THEN
        BCLD=PELOAD(:,:,KEXTBC)
!!!!
! id  oaerrad
!        BCLD=0.
        REBC=PERE(:,:,KEXTBC)
        VEBC=PEVE(:,:,KEXTBC)
      ENDIF
      OCLD=0.
      REOC=0.
      VEOC=0.
      IF ( KEXTOC > 0 ) THEN
        OCLD=PELOAD(:,:,KEXTOC)
!!!!
! id  oaerrad
!        OCLD=0.
        REOC=PERE(:,:,KEXTOC)
        VEOC=PEVE(:,:,KEXTOC)
      ENDIF
!
!-----------------------------------------------------------------------
!     * AEROSOL RADIATIVE PROPERTIES FOR INTERNALLY MIXED AEROSOL SPECIES.
!
      FR1=1.
      FR2=0.
      IF ( KINT > 0 ) THEN
        DO IS=1,ISAINT
          PIWETRC(:,:,IS)=PIDRYRC(IS)
        ENDDO
        CALL RADPARI(PINUM,PIMAS,PIN0,PIPHI0,PIPSI,PIPHIS0,PIDPHI0, &
                     PIWETRC,PIDDN,PIRE,PIVE,PILOAD, &
                     PIFRC,FR1,FR2,ILGA,LEVA)
      ENDIF
      AMLD=0.
      REAM=0.
      VEAM=0.
      IF ( KINT > 0 ) THEN
        AMLD=PILOAD
!!!!
! id  oaerrad
!        AMLD=0.
        REAM=PIRE
        VEAM=PIVE
      ENDIF
!
!-----------------------------------------------------------------------
!     * SIZE DISTRIBUTIONS FOR INTERNALLY MIXED AEROSOL WITH
!     * COMPONENTS REMOVED (FOR CALCULATION OF RADIATIVE FORCINGS).
!
      SO4LDI=0.
      RESO4I=0.
      VESO4I=0.
      FR1SO4I=0.
      FR2SO4I=0.
      OCLDI=0.
      REOCI=0.
      VEOCI=0.
      FR1OCI=0.
      FR2OCI=0.
      BCLDI=0.
      REBCI=0.
      VEBCI=0.
      FR1BCI=0.
      FR2BCI=0.
      IF ( IRFRC == 1 ) THEN
        IF ( KINTSO4 > 0 ) THEN
          CALL EXTAC (PIDN,PIDM,PIDF,DIRSMN,DIRSN,DIRSMS, &
                      PIMAS,PINUM,PIN0,PIPHI0,PIPSI,PIDDN,PIFRC, &
                      IINSO4,ITSO4,KINTSO4,ISINTSO4,ISO4S,ILGA,LEVA, &
                      IERR)
          TINUM=MAX(PINUM+PIDN,0.)
          TIMAS=MAX(PIMAS+PIDM,0.)
          TIFRC=MAX(PIFRC+PIDF,0.)
          CALL NM2PARI(TIN0,TIPHI0,TIPSI,PIPHIS0,PIDPHI0,TIDDN, &
                       TINUM,TIMAS,TIFRC,ILGA,LEVA)
          DO IS=1,ISAINT
            TIWETRC(:,:,IS)=PIDRYRC(IS)
          ENDDO
          CALL RADPARI(TINUM,TIMAS,TIN0,TIPHI0,TIPSI,PIPHIS0,PIDPHI0, &
                       TIWETRC,TIDDN,RESO4I,VESO4I,SO4LDI, &
                       TIFRC,FR1SO4I,FR2SO4I,ILGA,LEVA)
        ENDIF
        IF ( KINTOC > 0 ) THEN
          CALL EXTAC (PIDN,PIDM,PIDF,DIRSMN,DIRSN,DIRSMS, &
                      PIMAS,PINUM,PIN0,PIPHI0,PIPSI,PIDDN,PIFRC, &
                      IINOC,ITOC,KINTOC,ISINTOC,IOCS,ILGA,LEVA, &
                      IERR)
          TINUM=MAX(PINUM+PIDN,0.)
          TIMAS=MAX(PIMAS+PIDM,0.)
          TIFRC=MAX(PIFRC+PIDF,0.)
          CALL NM2PARI(TIN0,TIPHI0,TIPSI,PIPHIS0,PIDPHI0,TIDDN, &
                       TINUM,TIMAS,TIFRC,ILGA,LEVA)
          DO IS=1,ISAINT
            TIWETRC(:,:,IS)=PIDRYRC(IS)
          ENDDO
          CALL RADPARI(TINUM,TIMAS,TIN0,TIPHI0,TIPSI,PIPHIS0,PIDPHI0, &
                       TIWETRC,TIDDN,REOCI,VEOCI,OCLDI, &
                       TIFRC,FR1OCI,FR2OCI,ILGA,LEVA)
        ENDIF
        IF ( KINTBC > 0 ) THEN
          CALL EXTAC (PIDN,PIDM,PIDF,DIRSMN,DIRSN,DIRSMS, &
                      PIMAS,PINUM,PIN0,PIPHI0,PIPSI,PIDDN,PIFRC, &
                      IINBC,ITBC,KINTBC,ISINTBC,IBCS,ILGA,LEVA, &
                      IERR)
          TINUM=MAX(PINUM+PIDN,0.)
          TIMAS=MAX(PIMAS+PIDM,0.)
          TIFRC=MAX(PIFRC+PIDF,0.)
          CALL NM2PARI(TIN0,TIPHI0,TIPSI,PIPHIS0,PIDPHI0,TIDDN, &
                       TINUM,TIMAS,TIFRC,ILGA,LEVA)
          DO IS=1,ISAINT
            TIWETRC(:,:,IS)=PIDRYRC(IS)
          ENDDO
          CALL RADPARI(TINUM,TIMAS,TIN0,TIPHI0,TIPSI,PIPHIS0,PIDPHI0, &
                       TIWETRC,TIDDN,REBCI,VEBCI,BCLDI, &
                       TIFRC,FR1BCI,FR2BCI,ILGA,LEVA)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!       * PM2.5 AND PM10
!
      IF ( KXTRA1 ) THEN
        IF ( ISAINT > 0 .OR. ISAEXT > 0 ) THEN
          CALL SDAPROP (PEWETRC,PIWETRC,PEN0,PEPHI0,PEPSI,PEDDN, &
                        PIN0,PIPHI0,PIPSI,PIFRC,PIDDN,TA,RHA, &
                        ILGA,LEVA)
        ENDIF
        ISPM=2
        ALLOCATE(RADC(ISPM))
        ALLOCATE(FMASS(ILGA,LEVA,ISPM))
        ALLOCATE(FMASSD(ILGA,LEVA,ISPM))
        RADC(1)=0.5*2.5E-06
        RADC(2)=0.5*10.E-06
        PM2P5=0.
        PM10=0.
        PM2P5D=0.
        PM10D=0.
        IF ( ISAINT > 0 ) THEN
          CALL INTSDPM(FMASS,FMASSD,PINUM,PIMAS,PIN0,PIPHI0,PIPSI, &
                       PIPHIS0,PIDPHI0,PIDDN,PIWETRC,PIDRYRC,RADC, &
                       RHOA,ISPM,ILGA,LEVA,ISAINT)
          PM2P5=PM2P5+FMASS(:,:,1)
          PM10=PM10+FMASS(:,:,2)
          PM2P5D=PM2P5D+FMASSD(:,:,1)
          PM10D=PM10D+FMASSD(:,:,2)
        ENDIF
        IF ( ISAEXT > 0 ) THEN
          CALL INTSDPM(FMASS,FMASSD,PENUM,PEMAS,PEN0,PEPHI0,PEPSI, &
                       PEPHIS0,PEDPHI0,PEDDN,PEWETRC,PEDRYRC,RADC, &
                       RHOA,ISPM,ILGA,LEVA,ISAEXT)
          PM2P5=PM2P5+FMASS(:,:,1)
          PM10=PM10+FMASS(:,:,2)
          PM2P5D=PM2P5D+FMASSD(:,:,1)
          PM10D=PM10D+FMASSD(:,:,2)
        ENDIF
        DEALLOCATE(RADC)
        DEALLOCATE(FMASS)
        DEALLOCATE(FMASSD)
      ENDIF
!
!-----------------------------------------------------------------------
!       * DIAGNOSE AND FILTER SIZE DISTRIBUTION.
!
      VITRM=DPA/GRAV
      IF ( KXTRA2 ) THEN
        IF ( ISAINT > 0 .OR. ISAEXT > 0 .AND. ISDNUM > 1 ) THEN
!
!         * ARRAYS FOR TOTAL AEROSOL NUMBER SIZE DISTRIBUTION AND
!         * AEROSOL MASS FOR EACH SEPARATE AEROSOL SPECIES. AEROSOL
!         * WATER IS ALSO PROVIDED. THE FOLLOWING INDICES ARE USED
!         * FOR AEROSOL MASS:
!         * 1 - 1ST EXTERNALLY MIXED SPECIES
!         * 2 - 2ND EXTERNALLY MIXED SPECIES
!         * ...
!         * KEXT - LAST EXTERNALLY MIXED SPECIES
!         * KEXT+1 - AEROSOL WATER FOR EXTERNALLY MIXED AEROSOL
!         * KEXT+2 - 1ST INTERNALLY MIXED SPECIES
!         * ...
!         * KEXT+KINT+1 - LAST INTERNALLY MIXED SPECIES
!         * KEXT+KINT+2 - AEROSOL WATER FOR INTERNALLY MIXED AEROSOL
!
          ALLOCATE (FNUMO(ILGA,LEVA,ISDNUM,KEXT+1))
          ALLOCATE (FMASO(ILGA,LEVA,ISDNUM,KEXT+KINT+2))
!
          CALL SDFILT(FNUMO,FMASO,PEN0,PEPHI0,PEPSI,PEDDN,PEDPHI0, &
                      PEPHIS0,PEWETRC,PENUM,PEMAS,PIN0,PIPHI0,PIPSI, &
                      PIDDN,PIDPHI0,PIPHIS0,PIFRC,PIWETRC,PINUM, &
                      PIMAS,ILGA,LEVA)
!
!         * CONVERT UNITS SO THAT CONCENTRATIONS WILL BE PROVIDED
!         * INSTEAD OF MASS MIXING RATIOS.
!
          DO KX=1,KEXT+1
          DO IS=1,ISDNUM
            FNUMO(:,:,IS,KX)=FNUMO(:,:,IS,KX)*RHOA(:,:)
          ENDDO
          ENDDO
          DO KX=1,KEXT+KINT+2
          DO IS=1,ISDNUM
            FMASO(:,:,IS,KX)=FMASO(:,:,IS,KX)*RHOA(:,:)
          ENDDO
          ENDDO
!
!         * AEROSOL NUMBER FOR EXTERNALLY MIXED TYPES OF AEROSOL.
!
          IF ( KEXT > 0 ) THEN
            TNSS=0.
            TNMD=0.
            DO KX=1,KEXT
              CNAME=AEXTF%TP(KX)%NAME
              IF ( CNAME(1:7) == 'SEASALT'   ) THEN
                DO ISD=1,ISDNUM
                  SSSN(:,:,ISD)=FNUMO(:,:,ISD,KX)
                  SNSS(:,ISD)=FNUMO(:,LEVA,ISD,KX)
                  TNSS(:)=TNSS(:)+FNUMO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == 'MINDUST'   ) THEN
                DO ISD=1,ISDNUM
                  SMDN(:,:,ISD)=FNUMO(:,:,ISD,KX)
                  SNMD(:,ISD)=FNUMO(:,LEVA,ISD,KX)
                  TNMD(:)=TNMD(:)+FNUMO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
            ENDDO
          ENDIF
!
!         * AEROSOL NUMBER FOR INTERNALLY MIXED TYPES OF AEROSOL.
!
          IF ( KINT > 0 ) THEN
            TNIA=0.
            DO ISD=1,ISDNUM
              SIAN(:,:,ISD)=FNUMO(:,:,ISD,KEXT+1)
              SNIA(:,ISD)=FNUMO(:,LEVA,ISD,KEXT+1)
              TNIA(:)=TNIA(:)+FNUMO(:,LEVA,ISD,KEXT+1)
            ENDDO
          ENDIF
!
!         * AEROSOL MASS FOR EXTERNALLY MIXED TYPES OF AEROSOL.
!
          SSSM=0.
          SMSS=0.
          TMSS=0.
          SMDM=0.
          SMMD=0.
          TMMD=0.
          SSUM=0.
          SMSP=0.
          TMSP=0.
          SOCM=0.
          SMOC=0.
          TMOC=0.
          SBCM=0.
          SMBC=0.
          TMBC=0.
          IF ( KEXT > 0 ) THEN
            DO KX=1,KEXT
              CNAME=AEXTF%TP(KX)%NAME
              IF ( CNAME(1:7) == 'SEASALT'   ) THEN
                DO ISD=1,ISDNUM
                  SSSM(:,:,ISD)=FMASO(:,:,ISD,KX)
                  SMSS(:,ISD)=FMASO(:,LEVA,ISD,KX)
                  TMSS(:)=TMSS(:)+FMASO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == 'MINDUST'   ) THEN
                DO ISD=1,ISDNUM
                  SMDM(:,:,ISD)=FMASO(:,:,ISD,KX)
                  SMMD(:,ISD)=FMASO(:,LEVA,ISD,KX)
                  TMMD(:)=TMMD(:)+FMASO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == '(NH4)2SO4' ) THEN
                DO ISD=1,ISDNUM
                  SSUM(:,:,ISD)=FMASO(:,:,ISD,KX)
                  SMSP(:,ISD)=FMASO(:,LEVA,ISD,KX)
                  TMSP(:)=TMSP(:)+FMASO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == 'ORGCARB'   ) THEN
                DO ISD=1,ISDNUM
                  SOCM(:,:,ISD)=FMASO(:,:,ISD,KX)
                  SMOC(:,ISD)=FMASO(:,LEVA,ISD,KX)
                  TMOC(:)=TMOC(:)+FMASO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == 'BLCCARB'   ) THEN
                DO ISD=1,ISDNUM
                  SBCM(:,:,ISD)=FMASO(:,:,ISD,KX)
                  SMBC(:,ISD)=FMASO(:,LEVA,ISD,KX)
                  TMBC(:)=TMBC(:)+FMASO(:,LEVA,ISD,KX)
                ENDDO
              ENDIF
            ENDDO
          ENDIF
!
!         * AEROSOL MASS FOR INTERNALLY MIXED TYPES OF AEROSOL.
!
          IF ( KINT > 0 ) THEN
            DO KX=1,KINT
              IKF=KEXT+KX+1
              CNAME=AINTF%TP(KX)%NAME
              IF ( CNAME(1:9) == 'SEASALT'   ) THEN
                DO ISD=1,ISDNUM
                  SSSM(:,:,ISD)=SSSM(:,:,ISD)+FMASO(:,:,ISD,IKF)
                  SMSS(:,ISD)=SMSS(:,ISD)+FMASO(:,LEVA,ISD,IKF)
                  TMSS(:)=TMSS(:)+FMASO(:,LEVA,ISD,IKF)
                ENDDO
              ENDIF
              IF ( CNAME(1:9) == 'MINDUST'   ) THEN
                DO ISD=1,ISDNUM
                  SMDM(:,:,ISD)=SMDM(:,:,ISD)+FMASO(:,:,ISD,IKF)
                  SMMD(:,ISD)=SMMD(:,ISD)+FMASO(:,LEVA,ISD,IKF)
                  TMMD(:)=TMMD(:)+FMASO(:,LEVA,ISD,IKF)
                ENDDO
              ENDIF
              IF ( CNAME(1:9) == '(NH4)2SO4'   ) THEN
                DO ISD=1,ISDNUM
                  SSUM(:,:,ISD)=SSUM(:,:,ISD)+FMASO(:,:,ISD,IKF)
                  SMSP(:,ISD)=SMSP(:,ISD)+FMASO(:,LEVA,ISD,IKF)
                  TMSP(:)=TMSP(:)+FMASO(:,LEVA,ISD,IKF)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == 'ORGCARB'   ) THEN
                DO ISD=1,ISDNUM
                  SOCM(:,:,ISD)=SOCM(:,:,ISD)+FMASO(:,:,ISD,IKF)
                  SMOC(:,ISD)=SMOC(:,ISD)+FMASO(:,LEVA,ISD,IKF)
                  TMOC(:)=TMOC(:)+FMASO(:,LEVA,ISD,IKF)
                ENDDO
              ENDIF
              IF ( CNAME(1:7) == 'BLCCARB'   ) THEN
                DO ISD=1,ISDNUM
                  SBCM(:,:,ISD)=SBCM(:,:,ISD)+FMASO(:,:,ISD,IKF)
                  SMBC(:,ISD)=SMBC(:,ISD)+FMASO(:,LEVA,ISD,IKF)
                  TMBC(:)=TMBC(:)+FMASO(:,LEVA,ISD,IKF)
                ENDDO
              ENDIF
            ENDDO
          ENDIF
!
!         * AEROSOL WATER CONTENT.
!
          IF ( KEXT > 0 ) THEN
            DO ISD=1,ISDNUM
              SEWM(:,:,ISD)=FMASO(:,:,ISD,KEXT+1)
              SEWH(:,ISD)=FMASO(:,LEVA,ISD,KEXT+1)
            ENDDO
          ENDIF
          IF ( KINT > 0 ) THEN
            IKF=KEXT+KINT+2
            DO ISD=1,ISDNUM
              SIWM(:,:,ISD)=FMASO(:,:,ISD,IKF)
              SIWH(:,ISD)=FMASO(:,LEVA,ISD,IKF)
            ENDDO
          ENDIF
!
          DO ISD=1,ISDNUM
!
!           * AEROSOL NUMBER AND DRY MASS FOR ALL TYPES OF AEROSOL.
!
            SDNU(:,:,ISD)=SUM(FNUMO(:,:,ISD,1:KEXT),DIM=3) &
                         +FNUMO(:,:,ISD,KEXT+1)
            SDMA(:,:,ISD)=SUM(FMASO(:,:,ISD,1:KEXT),DIM=3) &
                         +SUM(FMASO(:,:,ISD,KEXT+2:KEXT+KINT+1),DIM=3)
!
!           * TOTAL DRY AEROSOL MASS FOR EXTERNALLY MIXED TYPES.
!
            DO KX=1,KEXT
              SDCO(:,:,ISD)=FMASO(:,:,ISD,KX)
            ENDDO
!
!           * TOTAL DRY AEROSOL MASS FOR INTERNALLY MIXED TYPES.
!
            DO KXX=1,KINT
              KX=KXX+KEXT+1
              SDAC(:,:,ISD)=FMASO(:,:,ISD,KX)
            ENDDO
          ENDDO
!
          DEALLOCATE (FNUMO)
          DEALLOCATE (FMASO)
!
!         * VERTICALLY INTEGRATED VOLUME SIZE DISTRIBUTION. 
!         * AERONET SIZE BINS ARE USED.
!
          RADS=0.05E-06
          RADE=15.E-06
          DRX=0.5*(LOG10(RADE)-LOG(RADS))/(REAL(ISDIAG)-1.)
          RADS=10.**(LOG10(RADS)-DRX)
          RADE=10.**(LOG10(RADE)+DRX)
!
          ALLOCATE (VVOL(ILGA,ISDIAG))
!
          CALL VINTSD (VVOL,PEN0,PEPHI0,PEPSI,PEDPHI0,PEPHIS0, &
                       PEWETRC,PENUM,PEMAS,PIN0,PIPHI0,PIPSI, &
                       PIDPHI0,PIPHIS0,PIWETRC,PINUM,PIMAS,VITRM, &
                       RADS,RADE,ISDIAG,ILGA,LEVA)
!
!         * SAVE VOLUME SIZE DISTRIBUTION FOR DAYTIME, 
!         * CLEAR CONDITIONS.
!
          SDVL=0.
          DO IL=1,ILGA
            IF ( RMUA(IL) > 0.001 .AND. CLDA(IL) < 0.5 ) THEN
              SDVL(IL,:)=VVOL(IL,:)
            ENDIF
          ENDDO
!
          DEALLOCATE (VVOL)
!
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * DEALLOCATION OF PLA ARRAYS.
!
      DEALLOCATE(VITRM)
      DEALLOCATE(DIRSMN)
      DEALLOCATE(DIRSN)
      DEALLOCATE(DIRSMS)
      DEALLOCATE(RHOA)
      IF ( ISAEXT > 0 ) THEN
        DEALLOCATE(PEPHIS0)
        DEALLOCATE(PEDPHI0)
        DEALLOCATE(PEDDN  )
        DEALLOCATE(PEN0   )
        DEALLOCATE(PEPHI0 )
        DEALLOCATE(PEPSI  )
        DEALLOCATE(PEWETRC)
        DEALLOCATE(PENUM  )
        DEALLOCATE(PEMAS  )
        DEALLOCATE(PERE   )
        DEALLOCATE(PEVE   )
        DEALLOCATE(PELOAD )
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DEALLOCATE(PIPHIS0)
        DEALLOCATE(PIDPHI0)
        DEALLOCATE(PIDDN  )
        DEALLOCATE(PIN0   )
        DEALLOCATE(PIPHI0 )
        DEALLOCATE(PIPSI  )
        DEALLOCATE(PIWETRC)
        DEALLOCATE(PINUM  )
        DEALLOCATE(PIMAS  )
        DEALLOCATE(PIFRC  )
        DEALLOCATE(PIRE   )
        DEALLOCATE(PIVE   )
        DEALLOCATE(PILOAD )
        DEALLOCATE(PIDM   )
        DEALLOCATE(PIDN   )
        DEALLOCATE(PIDF   )
        DEALLOCATE(TIDDN  )
        DEALLOCATE(TIN0   )
        DEALLOCATE(TIPHI0 )
        DEALLOCATE(TIPSI  )
        DEALLOCATE(TIWETRC)
        DEALLOCATE(TINUM  )
        DEALLOCATE(TIMAS  )
        DEALLOCATE(TIFRC  )
      ENDIF
!
      END SUBROUTINE PAMD
