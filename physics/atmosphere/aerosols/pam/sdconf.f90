      SUBROUTINE SDCONF(NUCOA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     INITIALIZATION OF BASIC PARAMETERS FOR AEROSOL CALCULATIONS.
!
!     HISTORY:
!     --------
!     * OCT 31/2011 - K.VONSALZEN   REMOVE PETYPE/PITYPE, ADD TEST
!     *                             PARTICLES FOR HYGROSCOPIC GROWTH
!     *                             CALCULATIONS.
!     * MAR 29/2006 - K.VONSALZEN   COMPLETELY REVISED FOR TREATMENT
!     *                             OF INTERNALLY AND EXTERNALLY MIXED
!     *                             AEROSOLS.
!     * DEC 11/2005 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDCODE
      USE SDPARM
      USE SDTMP
      USE SDTBLF
      USE COADAT
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: NUCOA
!
      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE
!
      INTEGER, ALLOCATABLE, DIMENSION(:) :: IOK,IOKI
      INTEGER, PARAMETER :: NTAB=4    ! NUMBER OF TABLES FOR PLA CONVERSIONS
      REAL, PARAMETER :: YPREC=0.1_R4
      REAL :: REA1
      REAL(R8) :: AX,AY,YTHR1,YTHR2,YTHR1T,YTHR2T
      CHARACTER(LEN=70) :: CNAME
      REAL(R8), PARAMETER :: TSTT2=1.
      REAL(R4), PARAMETER :: TSTT1=1.
      REAL, PARAMETER :: TSTT=1.
      CHARACTER(LEN=1) :: CCHCK
      INTEGER :: ICNT
!
      INTEGER, PARAMETER :: NUMTPE=2  ! NUMBER OF TEST PARTICLES
                                      ! FOR EXTERNALLY MIXED AEROSOL
      INTEGER, PARAMETER :: NUMTPI=6  ! NUMBER OF TEST PARTICLES
                                      ! FOR INTERNALLY MIXED AEROSOL
      INTEGER, PARAMETER :: IDP=100
      REAL, DIMENSION(IDP) ::  RDRY
      INTEGER, ALLOCATABLE, DIMENSION(:) ::  INTP
      REAL, ALLOCATABLE, DIMENSION(:) ::  FGRWS,RDRYS,RDRYM
      REAL, ALLOCATABLE, DIMENSION(:,:) ::  RDRYME,RDRYMO
      DATA  RDRY(1:100) /                                               &
      0.100000E-08,0.109750E-08,0.120450E-08,0.132194E-08,0.145083E-08, &
      0.159228E-08,0.174753E-08,0.191791E-08,0.210490E-08,0.231013E-08, &
      0.253537E-08,0.278256E-08,0.305386E-08,0.335160E-08,0.367838E-08, &
      0.403702E-08,0.443062E-08,0.486260E-08,0.533670E-08,0.585702E-08, &
      0.642807E-08,0.705480E-08,0.774264E-08,0.849753E-08,0.932602E-08, &
      0.102353E-07,0.112332E-07,0.123285E-07,0.135305E-07,0.148497E-07, &
      0.162975E-07,0.178865E-07,0.196304E-07,0.215444E-07,0.236449E-07, &
      0.259503E-07,0.284803E-07,0.312572E-07,0.343047E-07,0.376494E-07, &
      0.413201E-07,0.453487E-07,0.497702E-07,0.546228E-07,0.599484E-07, &
      0.657933E-07,0.722081E-07,0.792483E-07,0.869749E-07,0.954548E-07, &
      0.104762E-06,0.114976E-06,0.126186E-06,0.138489E-06,0.151991E-06, &
      0.166810E-06,0.183074E-06,0.200923E-06,0.220513E-06,0.242013E-06, &
      0.265609E-06,0.291505E-06,0.319927E-06,0.351119E-06,0.385353E-06, &
      0.422924E-06,0.464159E-06,0.509414E-06,0.559081E-06,0.613591E-06, &
      0.673415E-06,0.739072E-06,0.811130E-06,0.890216E-06,0.977010E-06, &
      0.107227E-05,0.117681E-05,0.129155E-05,0.141747E-05,0.155568E-05, &
      0.170735E-05,0.187382E-05,0.205651E-05,0.225702E-05,0.247708E-05, &
      0.271859E-05,0.298365E-05,0.327455E-05,0.359381E-05,0.394421E-05, &
      0.432876E-05,0.475081E-05,0.521401E-05,0.572237E-05,0.628030E-05, &
      0.689262E-05,0.756464E-05,0.830217E-05,0.911163E-05,0.100000E-04  &
      /
      REAL, DIMENSION(IDP) ::  FGRW
      DATA  FGRW(1:100) /                                               &
      0.979986E-02,0.108759E-01,0.120165E-01,0.132198E-01,0.144798E-01, &
      0.157902E-01,0.171394E-01,0.185155E-01,0.199008E-01,0.212778E-01, &
      0.226232E-01,0.239160E-01,0.251301E-01,0.262436E-01,0.272314E-01, &
      0.280761E-01,0.287576E-01,0.292659E-01,0.295900E-01,0.297283E-01, &
      0.296827E-01,0.294578E-01,0.290645E-01,0.285159E-01,0.278300E-01, &
      0.270215E-01,0.261123E-01,0.251190E-01,0.240619E-01,0.229575E-01, &
      0.218233E-01,0.206738E-01,0.195229E-01,0.183813E-01,0.172590E-01, &
      0.161643E-01,0.151054E-01,0.140829E-01,0.131066E-01,0.121742E-01, &
      0.112906E-01,0.104541E-01,0.966720E-02,0.892710E-02,0.823470E-02, &
      0.758752E-02,0.698431E-02,0.642395E-02,0.590274E-02,0.542033E-02, &
      0.497481E-02,0.456135E-02,0.418141E-02,0.382891E-02,0.350835E-02, &
      0.320883E-02,0.293619E-02,0.268525E-02,0.245569E-02,0.224390E-02, &
      0.205055E-02,0.187295E-02,0.171121E-02,0.156173E-02,0.142676E-02, &
      0.130112E-02,0.118763E-02,0.108393E-02,0.989789E-03,0.901607E-03, &
      0.823886E-03,0.750215E-03,0.685204E-03,0.624017E-03,0.569016E-03, &
      0.518852E-03,0.473861E-03,0.431008E-03,0.392879E-03,0.359698E-03, &
      0.326293E-03,0.298624E-03,0.271405E-03,0.246997E-03,0.225402E-03, &
      0.205156E-03,0.187835E-03,0.171638E-03,0.154992E-03,0.141607E-03, &
      0.129685E-03,0.117875E-03,0.107302E-03,0.983040E-04,0.884061E-04, &
      0.821075E-04,0.729969E-04,0.681604E-04,0.610745E-04,0.569128E-04  &
      /
!
!-----------------------------------------------------------------------
!     * MATHEMATICAL CONSTANTS.
!
      ILARGE=HUGE(1)
      YLARGE=1.E-01_R8*HUGE(REA1)
      YLARGES=0.99_R8*YLARGE
      YNA=-99999._R8
      INA=NINT(YNA)
      YTINY=10._R8*TINY(REA1)
      R0=1.E-06_R8
      YPI=3.141592653589793_R8
      SQRTPI=1.772453850905516_R8
      YCNST=4._R8*YPI/3._R8
      YLARGE8=1.E-09_R8*HUGE(TSTT2)
      YTINY8=10._R8*TINY(TSTT2)
!
!     * DETERMINE WHETHER AUTOPROMOTION TO DOUBLE PRECISION
!     * (8 BYTE REAL) IS USED AND CHECK FOR CODE COMPLIANCY.
!     * HERE AND IN ALL OTHER ROUTINES, FLAGS "D!PROC-DEPEND"
!     * (FOR 4 BYTE REAL) OR " !PROC-DEPEND" (FOR 8 BYTE REAL)
!     * INDICATE ASSOCIATED COMPILER-DEPENDENT PROGRAMMING
!     * OPTIONS. TO SELECT A DIFFERENT OPTION, REPLACE ONE FLAG
!     * BY THE OTHER. FOR EXAMPLE, AUTOPROMOTION USING THE
!     * XLF COMPILER NECESSITATES THE USE OF FUNCTIONS SUCH AS
!     * ERF() AND EXP() INSTEAD OF THEIR DOUBLE-PRECISION
!     * EQUIVALENTS DERF() AND DEXP(). THE LATTER MAY BE
!     * REQUIRED WHEN USING THE XLF COMPILER WITHOUT
!     * AUTOPROMOTION.
!
      IF ( DIGITS(TSTT)==DIGITS(TSTT2) ) THEN
        KDBL=.TRUE.
      ELSE IF ( DIGITS(TSTT)==DIGITS(TSTT1) ) THEN
        KDBL=.FALSE.
      ELSE
        CALL XIT('SDCONF',-1)
      ENDIF
!      CCHCK=                                                          'D!PROC-DEPEND
!     1'
!      CCHCK=                                                          ' !PROC-DEPEND
!     1'
!      IF ( .NOT.((KDBL.AND.CCHCK==' ').OR.(.NOT.KDBL.AND.CCHCK=='D')) )
!     1  CALL XIT('SDCONF',-2)
!
!-----------------------------------------------------------------------
!     * READ AND SAVE PARAMETERS FOR INDIVIDUAL AEROSOL CHEMICAL TYPES.
!
      CALL SDREAD
!
!-----------------------------------------------------------------------
!     * DETERMINE NUMBER OF EXTERNALLY MIXED AEROSOL TRACERS.
!
      ISAEXT=0
      DO KX=1,KEXT
        ISAEXT=ISAEXT+AEXTF%TP(KX)%ISEC
      ENDDO
      ISAEXTB=ISAEXT+KEXT
!
!     * AEROSOL TYPE AND SECTION INDICES FOR EXTERNALLY MIXED
!     * AEROSOL TRACERS.
!
      IF ( ISAEXT > 0 ) THEN
        IF( ALLOCATED(SEXTF%ISAER)) DEALLOCATE(SEXTF%ISAER)
        ALLOCATE(SEXTF%ISAER(ISAEXT))
      ENDIF
      ISI=0
      DO KX=1,KEXT
      DO IS=1,AEXTF%TP(KX)%ISEC
        ISI=ISI+1
        ISM=ISI
        SEXTF%ISAER(ISI)%ITYP=KX
        SEXTF%ISAER(ISI)%ISI=IS
        SEXTF%ISAER(ISI)%ISM=ISM
      ENDDO
      ENDDO
      ISAINT=0
      IF ( KINT > 0 ) THEN
!
!       * DETERMINE NUMBER OF INTERNALLY MIXED AEROSOL TRACERS.
!
        ALLOCATE(IOK(AINTF%ISEC))
        DO IST=1,AINTF%ISEC
          IOK(IST)=0
          DO KX=1,KINT
            IF (       IST >= AINTF%TP(KX)%ISCB &
                 .AND. IST <= AINTF%TP(KX)%ISCE                  ) THEN
              IOK(IST)=IOK(IST)+1
            ENDIF
          ENDDO
          IF ( IOK(IST) /= 0 ) THEN
            ISAINT=ISAINT+1
          ELSE
            CALL XIT('SDCONF',-3)
          ENDIF
        ENDDO
!
!       * AEROSOL TYPE AND SECTION INDICES FOR INTERNALLY MIXED
!       * AEROSOL TRACERS.
!
        IF ( ISAINT > 0 ) THEN
          IF( ALLOCATED(SINTF%ISAER))  DEALLOCATE(SINTF%ISAER)
          ALLOCATE(SINTF%ISAER(ISAINT))
          ISM=ISAEXT
          DO IS=1,AINTF%ISEC
            SINTF%ISAER(IS)%ISI=IS
            SINTF%ISAER(IS)%ITYPT=IOK(IS)
            IF( ALLOCATED(SINTF%ISAER(IS)%ITYP)) DEALLOCATE(SINTF%ISAER(IS)%ITYP)
            ALLOCATE(SINTF%ISAER(IS)%ITYP(IOK(IS)))
            IF( ALLOCATED(SINTF%ISAER(IS)%ISM))  DEALLOCATE(SINTF%ISAER(IS)%ISM)
            ALLOCATE(SINTF%ISAER(IS)%ISM (IOK(IS)))
            KXT=0
            DO KX=1,KINT
              IF (       IS >= AINTF%TP(KX)%ISCB &
                   .AND. IS <= AINTF%TP(KX)%ISCE                ) THEN
                KXT=KXT+1
                ISM=ISM+1
                SINTF%ISAER(IS)%ITYP(KXT)=KX
                SINTF%ISAER(IS)%ISM (KXT)=ISM
              ENDIF
            ENDDO
          ENDDO
        ENDIF
        DEALLOCATE(IOK)
        ISAINTB=ISAINT+1
      ENDIF
!
!-----------------------------------------------------------------------
!     * DETERMINE NUMBER OF WETTABLE, EXTERNALLY MIXED AEROSOL TRACERS.
!
      ISWEXT=0
      KWEXT=0
      DO KX=1,KEXT
        IF (     (ABS(AEXTF%TP(KX)%NUIO-YNA) > YTINY &
                                       .AND. AEXTF%TP(KX)%NUIO > YTINY) &
            .OR. (ABS(AEXTF%TP(KX)%KAPPA-YNA) > YTINY &
                               .AND. AEXTF%TP(KX)%KAPPA > YTINY) ) THEN
          ISWEXT=ISWEXT+AEXTF%TP(KX)%ISEC
          KWEXT=KWEXT+1
        ENDIF
      ENDDO
!
!     * AEROSOL TYPE AND SECTION INDICES FOR WETTABLE, EXTERNALLY MIXED
!     * AEROSOL TRACERS.
!
      IF ( KWEXT > 0 ) THEN
        IF( ALLOCATED(SEXTF%ISWET)) DEALLOCATE(SEXTF%ISWET)
        ALLOCATE(SEXTF%ISWET(ISWEXT))
      ENDIF
      ISI=0
      ISM=0
      DO KX=1,KEXT
        IF (     (ABS(AEXTF%TP(KX)%NUIO-YNA) > YTINY &
                                      .AND. AEXTF%TP(KX)%NUIO  > YTINY) &
            .OR. (ABS(AEXTF%TP(KX)%KAPPA-YNA) > YTINY &
                               .AND. AEXTF%TP(KX)%KAPPA > YTINY) ) THEN
          DO IS=1,AEXTF%TP(KX)%ISEC
            ISI=ISI+1
            ISM=ISM+1
            SEXTF%ISWET(ISI)%ITYP=KX
            SEXTF%ISWET(ISI)%ISI=IS
            SEXTF%ISWET(ISI)%ISM=ISM
          ENDDO
        ELSE
          DO IS=1,AEXTF%TP(KX)%ISEC
            ISM=ISM+1
          ENDDO
        ENDIF
      ENDDO
      ISWINT=0
      KWINT=0
      IF ( KINT > 0 ) THEN
!
!       * DETERMINE NUMBER OF WETTABLE, INTERNALLY MIXED AEROSOL TRACERS.
!
        ALLOCATE(IOK(AINTF%ISEC))
        IF ( ALLOCATED(IOKI) ) DEALLOCATE (IOKI)
        ALLOCATE(IOKI(AINTF%ISEC))
        DO IST=1,AINTF%ISEC
          IOK(IST)=0
          IOKI(IST)=0
          DO KX=1,KINT
            IF (       IST >= AINTF%TP(KX)%ISCB &
                 .AND. IST <= AINTF%TP(KX)%ISCE                  ) THEN
              IOK(IST)=IOK(IST)+1
              IF (     (ABS(AINTF%TP(KX)%NUIO-YNA) > YTINY &
                                      .AND. AINTF%TP(KX)%NUIO  > YTINY) &
                  .OR. (ABS(AINTF%TP(KX)%KAPPA-YNA) > YTINY &
                               .AND. AINTF%TP(KX)%KAPPA > YTINY) ) THEN
                IOKI(IST)=1
              ENDIF
            ENDIF
          ENDDO
          IF ( IOKI(IST) /= 0 ) THEN
            ISWINT=ISWINT+1
          ENDIF
        ENDDO
        IF ( ISWINT > 0 ) KWINT=1
!
!       * AEROSOL TYPE AND TRACER INDICES FOR WETTABLE, INTERNALLY MIXED
!       * AEROSOL TRACERS. AN AEROSOL IS CONSIDERED WETTABLE IF AT LEAST
!       * ONE CHEMICAL SPECIES IN A SECTION DISSOLVES INTO A NUMBER OF IONS
!       * THAT IS GREATER THAN 0.
!
        IF ( ISWINT > 0 ) THEN
          IF( ALLOCATED(SINTF%ISWET)) DEALLOCATE(SINTF%ISWET)
          ALLOCATE(SINTF%ISWET(ISWINT))
          IS=0
          ISM=ISAEXT
          DO IST=1,AINTF%ISEC
            IF ( IOKI(IST) /= 0 ) THEN
              IS=IS+1
              SINTF%ISWET(IS)%ISI=IST
              SINTF%ISWET(IS)%ITYPT=IOK(IST)
              IF( ALLOCATED(SINTF%ISWET(IS)%ITYP)) DEALLOCATE(SINTF%ISWET(IS)%ITYP)
              ALLOCATE(SINTF%ISWET(IS)%ITYP(IOK(IST)))
              IF( ALLOCATED(SINTF%ISWET(IS)%ISM)) DEALLOCATE(SINTF%ISWET(IS)%ISM)
              ALLOCATE(SINTF%ISWET(IS)%ISM (IOK(IST)))
              KXT=0
              DO KX=1,KINT
                IF (       IST >= AINTF%TP(KX)%ISCB &
                     .AND. IST <= AINTF%TP(KX)%ISCE              ) THEN
                  ISM=ISM+1
                  KXT=KXT+1
                  SINTF%ISWET(IS)%ITYP(KXT)=KX
                  SINTF%ISWET(IS)%ISM (KXT)=ISM
                ENDIF
              ENDDO
            ELSE
              DO KX=1,KINT
                IF (       IST >= AINTF%TP(KX)%ISCB &
                     .AND. IST <= AINTF%TP(KX)%ISCE              ) THEN
                  ISM=ISM+1
                ENDIF
              ENDDO
            ENDIF
          ENDDO
        ENDIF
        DEALLOCATE(IOK)
      ENDIF
!
!-----------------------------------------------------------------------
!     * CALCULATE NUMBER OF TRACERS FOR AEROSOL MASS.
!
      ISAERT=ISAEXT
      DO IS=1,ISAINT
        ISAERT=ISAERT+SINTF%ISAER(IS)%ITYPT
      ENDDO
      ISWETT=ISWEXT
      DO IS=1,ISWINT
        ISWETT=ISWETT+SINTF%ISWET(IS)%ITYPT
      ENDDO
!
!-----------------------------------------------------------------------
!     * TRACER INDEX FOR AEROSOL NUMBER CONCENTRATION FOR ALL AEROSOLS.
!     * AEROSOL NUMBER CONCENTRATIONS ARE SAVED AFTER AEROSOL MASS
!     * CONCENTRATIONS.
!
      ISI=ISAERT
      DO KX=1,KEXT
      DO IS=1,AEXTF%TP(KX)%ISEC
        ISI=ISI+1
        SEXTF%ISAER(ISI-ISAERT)%ISN=ISI
      ENDDO
      ENDDO
      DO IS=1,AINTF%ISEC
        ISI=ISI+1
        SINTF%ISAER(IS)%ISN=ISI
      ENDDO
!
!     * TRACER INDEX FOR AEROSOL NUMBER CONCENTRATION FOR WETTABLE
!     * AEROSOLS.
!
      ISI=ISAERT
      DO KX=1,KEXT
      DO IS=1,AEXTF%TP(KX)%ISEC
        ISI=ISI+1
        IF (     (ABS(AEXTF%TP(KX)%NUIO-YNA) > YTINY &
                                      .AND. AEXTF%TP(KX)%NUIO  > YTINY) &
            .OR. (ABS(AEXTF%TP(KX)%KAPPA-YNA) > YTINY &
                               .AND. AEXTF%TP(KX)%KAPPA > YTINY) ) THEN
          SEXTF%ISWET(ISI-ISAERT)%ISN=ISI
        ENDIF
      ENDDO
      ENDDO
      ISX=0
      DO IS=1,AINTF%ISEC
        ISI=ISI+1
        IF ( IOKI(IS) /= 0 ) THEN
          ISX=ISX+1
          SINTF%ISWET(ISX)%ISN=ISI
        ENDIF
      ENDDO
      IF ( ALLOCATED(IOKI) ) THEN
        DEALLOCATE(IOKI)
      ENDIF
!
!-----------------------------------------------------------------------
!     * INDEX RANGES FOR MAIN AEROSOL TRACER ARRAY.
!
      IF ( ISAEXT > 0 ) THEN
        INSEX=SEXTF%ISAER(1)%ISN
        INFEX=SEXTF%ISAER(ISAEXT)%ISN
        IMSEX=SEXTF%ISAER(1)%ISM
        IMFEX=SEXTF%ISAER(ISAEXT)%ISM
      ENDIF
      IF ( ISAINT > 0 ) THEN
        INSIN=SINTF%ISAER(1)%ISN
        INFIN=SINTF%ISAER(ISAINT)%ISN
      ENDIF
!
!-----------------------------------------------------------------------
!     * TOTAL NUMBER OF AEROSOL TRACERS FOR PAM.
!
      NTRACS=ISAERT+ISAEXT+ISAINT
!
!     * TRACER INDICES FOR GASES.
!
      JGS6=NTRACS+1
      JGSP=NTRACS+2
      JSO2=NTRACS+3
      JHPO=NTRACS+4
      NGAS=4
!
!     * TOTAL NUMBER OF AEROSOL + GAS TRACERS.
!
      NTRACT=NTRACS+NGAS
!
!-----------------------------------------------------------------------
!     * ACCESS AND SAVE TABULATED DATA FOR PLA TRANSFORMATIONS FOR
!     * ARBITRARY NUMBER OF DIFFERENT TABLES. NEW TABLES CAN BE ADDED
!     * BY CREATING AND CALLING A CORRESPONDING SUBROUTINE. THE TOTAL
!     * NUMBER OF TABLES (NTAB) NEEDS TO BE CONSISTENT WITH THE NUMBER
!     * OF SUBROUTINE CALLS IN THIS SECTION. CORRECT NTAB, IF NECESSARY.
!
      IF ( ALLOCATED(TBLT) ) DEALLOCATE(TBLT)
      ALLOCATE(TBLT(NTAB))
      YTHR1T=YNA
      YTHR2T=YNA
      ICNT=0
      CALL SDTBL0(ICNT,YTHR1T,YTHR2T,NTAB)
      CALL SDTBL1(ICNT,YTHR1T,YTHR2T,NTAB)
      CALL SDTBL2(ICNT,YTHR1T,YTHR2T,NTAB)
      CALL SDTBL3(ICNT,YTHR1T,YTHR2T,NTAB)
      IF ( ICNT /= NTAB ) CALL XIT ('SDCONF',-4)
!
!     * DETERMINE ACCURACY TO PREVENT FLOATING POINT OVER/UNDERFLOW
!     * IN CALCULATIONS INVOLVING ERF().
!
      AINCR=0.05
      AX=0.
      YTHR1=YNA
      DO IND=1,1000
        AX=AX+AINCR
        AY=                                                            &!PROC-DEPEND
           ERF(AX)
        IF ( ABS(AY-1.) <= EPSILON(AY) .AND. ABS(YTHR1-YNA) <= YTINY &
                                                                ) THEN
          YTHR1=AX-AINCR
        ENDIF
      ENDDO
!
!     * DETERMINE ACCURACY TO PREVENT FLOATING POINT OVER/UNDERFLOW
!     * IN CALCULATIONS INVOLVING ERFI().THE FIRST CHECK PREVENTS
!     * FLOATING POINT OVERFLOW IN CALCULATION OF EXP(ARG**2) IN ERFI(),
!     * THE SECOND FLOATING POINT OVERFLOW IN APPLICATIONS OF ERFI().
!     * NOTE THAT THE FIRST CHECK IS SPECIFIC TO THE METHOD THAT IS USED
!     * TO NUMERICALLY DETERMINE ERFI().
!
!      AX=0.
!      YTHR2=YNA
!      DO IND=1,1000
!        AX=AX+AINCR
!        IF ( AX**2 >= 88.7228 .AND. ABS(YTHR2-YNA) <= YTINY ) THEN
!          YTHR2=AX-AINCR
!        ENDIF
!        AY=ERFI(AX)
!        IF ( ABS(AY)**2 >= HUGE(AY) .AND. ABS(YTHR2-YNA) <= YTINY ) THEN
!          YTHR2=AX-AINCR
!        ENDIF
!      ENDDO
!
!      * USE THE FOLLOWING INSTEAD IF ERFI() IS NOT USED...
!
       YTHR2=YTHR2T
!
!     * COMPARE TO MAXINUM ACCURACY IN TABLES AND EXIT IF ACCURACY
!     * OF CALCULATIONS IN THE CURRENT CODE IS NOT SUFFICIENT.
!
      IF ( ABS(YTHR1-YTHR1T) > YPREC .OR. ABS(YTHR2-YTHR2T) > YPREC &
                                                                ) THEN
        CALL XIT('SDCONF',-5)
      ENDIF
!
!-----------------------------------------------------------------------
!     * FIND THE TABLE RESULT THAT CORRESPONDS TO (OR MATCHES CLOSELY) THE
!     * REQUESTED SECTION WIDTHS.
!
      DO KX=1,KEXT
        IF ( ABS(AEXTF%TP(KX)%DPST-YNA) > YTINY ) THEN
          ADST=YLARGE
          DO NT=1,NTAB
            ADSTT=ABS(AEXTF%TP(KX)%DPST-TBLT(NT)%DPSTAR0)
            IF ( ADSTT < ADST ) THEN
              ADST=ADSTT
              AEXTF%TP(KX)%NTBL=NT
            ENDIF
          ENDDO
        ELSE
          AEXTF%TP(KX)%NTBL=1
        ENDIF
      ENDDO
      IF ( KINT > 0 ) THEN
        IF ( ABS(AINTF%DPST-YNA) > YTINY ) THEN
          ADST=YLARGE
          DO NT=1,NTAB
            ADSTT=ABS(AINTF%DPST-TBLT(NT)%DPSTAR0)
            IF ( ADSTT < ADST ) THEN
              ADST=ADSTT
              AINTF%NTBL=NT
            ENDIF
          ENDDO
        ELSE
          AINTF%NTBL=1
        ENDIF
      ENDIF
      DO KX=1,KEXT
        NT=AEXTF%TP(KX)%NTBL
        AEXTF%TP(KX)%IDRB=TBLT(NT)%IDRM
        AEXTF%TP(KX)%DPSTAR=TBLT(NT)%DPSTAR0
      ENDDO
      IF ( KINT /= 0 ) THEN
        NT=AINTF%NTBL
        AINTF%IDRB=TBLT(NT)%IDRM
        AINTF%DPSTAR=TBLT(NT)%DPSTAR0
      ENDIF
!
!     * CHECK IF SECTION WIDTH IS MULTIPLE OF SECTION WITDTH
!     * CORRESPONDING TO DOUBLING OF PARTICLE VOLUME. THIS IS
!     * REQUIRED FOR COAGULATION CALCULATIONS AT PRESENT. THE FOLLOWING
!     * CAN BE REMOVED IF COAGULATION IS NOT ACCOUNTED FOR IN THE MODEL.
!
      IF ( KINT /= 0 .AND. KCOAG ) KCOAG=.TRUE.
      IF ( KCOAG ) THEN
        COAGP%DPSTAR=LOG(2.)/3.
        IF ( KINT /= 0 ) THEN
          COAGP%ISEC=NINT(AINTF%DPSTAR/COAGP%DPSTAR)
          REST=AINTF%DPSTAR-COAGP%ISEC*COAGP%DPSTAR
          IF ( ABS(REST) > YSECS*COAGP%DPSTAR ) THEN
            CALL XIT('SDCONF',-6)
          ENDIF
        ENDIF
        ISFINT =ISAINT*COAGP%ISEC
        ISFINTB=ISFINT+1
        ISFINTM=ISFINT-1
        RISFINT=REAL(ISFINT)
        ISFTRI =NINT(RISFINT*(RISFINT+1.)/2.)
        ISFTRIM=NINT(RISFINT*(RISFINT-1.)/2.)
!
!       * READ FOR COAGULATION COEFFICIENT.
!
        READ(NUCOA,'(4(1X,I3))') IDB,IDC,IDA,IDD
        IF( ALLOCATED(CKER))  DEALLOCATE(CKER)
        ALLOCATE(CKER(IDB,IDC,IDA,IDD))
        IF( ALLOCATED(CPRE)) DEALLOCATE(CPRE)
        ALLOCATE(CPRE(IDD))
        IF( ALLOCATED(CTEM)) DEALLOCATE(CTEM)
        ALLOCATE(CTEM(IDA))
        IF( ALLOCATED(CPHI)) DEALLOCATE(CPHI)
        ALLOCATE(CPHI(IDB))
        DO I4=1,IDD
        DO I3=1,IDA
        DO I2=1,IDC
        DO I1=1,IDB
          READ(NUCOA,'(1X,E12.6)') CKER(I1,I2,I3,I4)
        ENDDO
        ENDDO
        ENDDO
        ENDDO
        DO I4=1,IDD
          READ(NUCOA,'(1X,E12.6)') CPRE(I4)
        ENDDO
        DO I3=1,IDA
          READ(NUCOA,'(1X,E12.6)') CTEM(I3)
        ENDDO
        DO I1=1,IDB
          READ(NUCOA,'(1X,E12.6)') CPHI(I1)
        ENDDO
      ELSE
        COAGP%DPSTAR=YNA
        COAGP%ISEC=INA
      ENDIF
!
!-----------------------------------------------------------------------
!     * ALLOCATE AND INITIALIZE WORK ARRAYS FOR TABULATED PLA DATA.
!
      DO KX=1,KEXT
        IF( ALLOCATED(AEXTF%TP(KX)%FLG)) DEALLOCATE(AEXTF%TP(KX)%FLG)
        ALLOCATE (AEXTF%TP(KX)%FLG  (AEXTF%TP(KX)%ISEC &
                                                   ,AEXTF%TP(KX)%IDRB))
        IF( ALLOCATED(AEXTF%TP(KX)%PSIB)) DEALLOCATE(AEXTF%TP(KX)%PSIB)
        ALLOCATE (AEXTF%TP(KX)%PSIB (AEXTF%TP(KX)%ISEC &
                                                   ,AEXTF%TP(KX)%IDRB))
        IF( ALLOCATED(AEXTF%TP(KX)%PHIB)) DEALLOCATE(AEXTF%TP(KX)%PHIB)
        ALLOCATE (AEXTF%TP(KX)%PHIB (AEXTF%TP(KX)%ISEC &
                                                   ,AEXTF%TP(KX)%IDRB))
        IF( ALLOCATED(AEXTF%TP(KX)%DPHIB)) DEALLOCATE(AEXTF%TP(KX)%DPHIB)
        ALLOCATE (AEXTF%TP(KX)%DPHIB(AEXTF%TP(KX)%ISEC &
                                                   ,AEXTF%TP(KX)%IDRB))
        DO IS=1,AEXTF%TP(KX)%ISEC
        DO IR=1,AEXTF%TP(KX)%IDRB
          AEXTF%TP(KX)%FLG(IS,IR)     =.FALSE.
          AEXTF%TP(KX)%PSIB(IS,IR)%VL =YNA
          AEXTF%TP(KX)%PSIB(IS,IR)%VR =YNA
          AEXTF%TP(KX)%PHIB(IS,IR)%VL =YNA
          AEXTF%TP(KX)%PHIB(IS,IR)%VR =YNA
          AEXTF%TP(KX)%DPHIB(IS,IR)%VL=YNA
          AEXTF%TP(KX)%DPHIB(IS,IR)%VR=YNA
        ENDDO
        ENDDO
      ENDDO
      IF ( KINT /= 0 ) THEN
        IF( ALLOCATED(AINTF%FLG)) DEALLOCATE(AINTF%FLG)
        ALLOCATE (AINTF%FLG  (AINTF%ISEC,AINTF%IDRB))
        IF( ALLOCATED(AINTF%PSIB)) DEALLOCATE(AINTF%PSIB)
        ALLOCATE (AINTF%PSIB (AINTF%ISEC,AINTF%IDRB))
        IF( ALLOCATED(AINTF%PHIB)) DEALLOCATE(AINTF%PHIB)
        ALLOCATE (AINTF%PHIB (AINTF%ISEC,AINTF%IDRB))
        IF( ALLOCATED(AINTF%DPHIB)) DEALLOCATE(AINTF%DPHIB)
        ALLOCATE (AINTF%DPHIB(AINTF%ISEC,AINTF%IDRB))
        DO IS=1,AINTF%ISEC
        DO IR=1,AINTF%IDRB
          AINTF%FLG(IS,IR)     =.FALSE.
          AINTF%PSIB(IS,IR)%VL =YNA
          AINTF%PSIB(IS,IR)%VR =YNA
          AINTF%PHIB(IS,IR)%VL =YNA
          AINTF%PHIB(IS,IR)%VR =YNA
          AINTF%DPHIB(IS,IR)%VL=YNA
          AINTF%DPHIB(IS,IR)%VR=YNA
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * SIZES AND BOUNDARIES OF SECTIONS.
!
!     * ALLOCATE SIZE ARRAYS.
!
      DO KX=1,KEXT
        ISECT=AEXTF%TP(KX)%ISEC
        IF( ALLOCATED(AEXTF%TP(KX)%PHI)) DEALLOCATE(AEXTF%TP(KX)%PHI)
        ALLOCATE(AEXTF%TP(KX)%PHI(ISECT))
        IF( ALLOCATED(AEXTF%TP(KX)%DRYR)) DEALLOCATE(AEXTF%TP(KX)%DRYR)
        ALLOCATE(AEXTF%TP(KX)%DRYR(ISECT))
        IF( ALLOCATED(AEXTF%TP(KX)%DRYRC)) DEALLOCATE(AEXTF%TP(KX)%DRYRC)
        ALLOCATE(AEXTF%TP(KX)%DRYRC(ISECT))
        IF( ALLOCATED(AEXTF%TP(KX)%ISO)) DEALLOCATE(AEXTF%TP(KX)%ISO)
        ALLOCATE(AEXTF%TP(KX)%ISO(ISECT))
      ENDDO
      IF ( KINT > 0 ) THEN
        ISECT=AINTF%ISEC
        IF( ALLOCATED(AINTF%PHI))  DEALLOCATE(AINTF%PHI)
        ALLOCATE(AINTF%PHI(ISECT))
        IF( ALLOCATED(AINTF%DRYR)) DEALLOCATE(AINTF%DRYR)
        ALLOCATE(AINTF%DRYR(ISECT))
        IF( ALLOCATED(AINTF%DRYRC)) DEALLOCATE(AINTF%DRYRC)
        ALLOCATE(AINTF%DRYRC(ISECT))
        IF( ALLOCATED(AINTF%ISO)) DEALLOCATE(AINTF%ISO)
        ALLOCATE(AINTF%ISO(ISECT))
      ENDIF
!
!     * ASSIGN VALUES.
!
      ISI=0
      DO KX=1,KEXT
        APHIB=LOG(AEXTF%TP(KX)%RADB/R0)
        DPSTART=AEXTF%TP(KX)%DPSTAR
        ISECT=AEXTF%TP(KX)%ISEC
        APHIE=APHIB+REAL(ISECT)*DPSTART
        AEXTF%TP(KX)%RADE=R0*EXP(APHIE)
        AEXTF%TP(KX)%PHI(1)%VL=APHIB
        AEXTF%TP(KX)%PHI(1)%VR=APHIB+DPSTART
        DO IS=2,ISECT
          AEXTF%TP(KX)%PHI(IS)%VL=AEXTF%TP(KX)%PHI(IS-1)%VR
          AEXTF%TP(KX)%PHI(IS)%VR=AEXTF%TP(KX)%PHI(IS-1)%VR+DPSTART
        ENDDO
        DO IS=1,ISECT
          AEXTF%TP(KX)%DRYR(IS)%VL=R0*EXP(AEXTF%TP(KX)%PHI(IS)%VL)
          AEXTF%TP(KX)%DRYR(IS)%VR=R0*EXP(AEXTF%TP(KX)%PHI(IS)%VR)
          AEXTF%TP(KX)%DRYRC(IS)=R0*EXP(AEXTF%TP(KX)%PHI(IS)%VL &
                                +.5*DPSTART)
        ENDDO
        DO IS=1,AEXTF%TP(KX)%ISEC
          ISI=ISI+1
          AEXTF%TP(KX)%ISO(IS)=ISI
        ENDDO
      ENDDO
      IF ( KINT > 0 ) THEN
        APHIB=LOG(AINTF%RADB/R0)
        DPSTART=AINTF%DPSTAR
        ISECT=AINTF%ISEC
        APHIE=APHIB+REAL(ISECT)*DPSTART
        AINTF%RADE=R0*EXP(APHIE)
        AINTF%PHI(1)%VL=APHIB
        AINTF%PHI(1)%VR=APHIB+DPSTART
        DO IS=2,ISECT
          AINTF%PHI(IS)%VL=AINTF%PHI(IS-1)%VR
          AINTF%PHI(IS)%VR=AINTF%PHI(IS-1)%VR+DPSTART
        ENDDO
        DO IS=1,ISECT
          AINTF%DRYR(IS)%VL=R0*EXP(AINTF%PHI(IS)%VL)
          AINTF%DRYR(IS)%VR=R0*EXP(AINTF%PHI(IS)%VR)
          AINTF%DRYRC(IS)=R0*EXP(AINTF%PHI(IS)%VL+.5*DPSTART)
        ENDDO
        DO IS=1,AINTF%ISEC
          AINTF%ISO(IS)=IS
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * ALLOCATE MEMORY FOR PLA SECTION SIZE PARAMETER ARRAYS.
!
      IF ( ISAEXT > 0 ) THEN
        IF( ALLOCATED(PEPHISS)) DEALLOCATE(PEPHISS)
        ALLOCATE(PEPHISS(ISAEXT))
        IF( ALLOCATED(PEDPHIS))  DEALLOCATE(PEDPHIS)
        ALLOCATE(PEDPHIS(ISAEXT))
        IF( ALLOCATED(PEDRYR))  DEALLOCATE(PEDRYR)
        ALLOCATE(PEDRYR (ISAEXT))
        IF( ALLOCATED(PEDRYRC)) DEALLOCATE(PEDRYRC)
        ALLOCATE(PEDRYRC(ISAEXT))
        IF( ALLOCATED(PEISMIN)) DEALLOCATE(PEISMIN)
        ALLOCATE(PEISMIN(ISAEXT))
        IF( ALLOCATED(PEISMAX)) DEALLOCATE(PEISMAX)
        ALLOCATE(PEISMAX(ISAEXT))
        IF( ALLOCATED(PEISMNK)) DEALLOCATE(PEISMNK)
        ALLOCATE(PEISMNK(KEXT))
        IF( ALLOCATED(PEISMXK)) DEALLOCATE(PEISMXK)
        ALLOCATE(PEISMXK(KEXT))
      ENDIF
      IF ( ISAEXTB > 0 ) THEN
        IF( ALLOCATED(PEDRYRB)) DEALLOCATE(PEDRYRB)
        ALLOCATE(PEDRYRB(ISAEXTB))
      ENDIF
      IF ( ISAINT > 0 ) THEN
        IF( ALLOCATED(PIPHISS)) DEALLOCATE(PIPHISS)
        ALLOCATE(PIPHISS(ISAINT))
        IF( ALLOCATED(PIDPHIS)) DEALLOCATE(PIDPHIS)
        ALLOCATE(PIDPHIS(ISAINT))
        IF( ALLOCATED(PIDRYR))  DEALLOCATE(PIDRYR)
        ALLOCATE(PIDRYR (ISAINT))
        IF( ALLOCATED(PIDRYRC)) DEALLOCATE(PIDRYRC)
        ALLOCATE(PIDRYRC(ISAINT))
        IF( ALLOCATED(PIISMIN)) DEALLOCATE(PIISMIN)
        ALLOCATE(PIISMIN(ISAINT))
        IF( ALLOCATED(PIISMAX)) DEALLOCATE(PIISMAX)
        ALLOCATE(PIISMAX(ISAINT))
      ENDIF
      IF ( ISAINTB > 0 ) THEN
        IF( ALLOCATED(PIDRYRB)) DEALLOCATE(PIDRYRB)
        ALLOCATE(PIDRYRB(ISAINTB))
      ENDIF
!
!-----------------------------------------------------------------------
!     * BOUNDARIES AND SECTION WIDTHS FOR EXTERNALLY AND INTERNALLY
!     * MIXED AEROSOL TYPES.
!
      KXOLD=INA
      ISMIN=1
      DO IS=1,ISAEXT
        ISI=SEXTF%ISAER(IS)%ISI
        KX=SEXTF%ISAER(IS)%ITYP
        PEDPHIS(IS)=AEXTF%TP(KX)%DPSTAR
        PEPHISS(IS)=AEXTF%TP(KX)%PHI(ISI)%VL
        PEDRYR(IS)%VL=AEXTF%TP(KX)%DRYR(ISI)%VL
        PEDRYR(IS)%VR=AEXTF%TP(KX)%DRYR(ISI)%VR
        PEDRYRC(IS)=AEXTF%TP(KX)%DRYRC(ISI)
!
        IF ( KX /= KXOLD .AND. KXOLD /= INA ) THEN
          ISMAX=IS-1
          PEISMAX(ISMIN:ISMAX)=ISMAX
          PEISMIN(ISMIN:ISMAX)=ISMIN
          PEISMXK(KXOLD)=ISMAX
          PEISMNK(KXOLD)=ISMIN
          ISMIN=IS
        ENDIF
        KXOLD=KX
      ENDDO
      IF ( ISAEXT > 0 ) THEN
        KX=SEXTF%ISAER(ISAEXT)%ITYP
        ISMAX=ISAEXT
        PEISMAX(ISMIN:ISMAX)=ISMAX
        PEISMIN(ISMIN:ISMAX)=ISMIN
        PEISMXK(KX)=ISMAX
        PEISMNK(KX)=ISMIN
      ENDIF
      DO IS=1,ISAINT
        ISI=SINTF%ISAER(IS)%ISI
        PIDPHIS(IS)=AINTF%DPSTAR
        PIPHISS(IS)=AINTF%PHI(ISI)%VL
        PIDRYR(IS)%VL=AINTF%DRYR(ISI)%VL
        PIDRYR(IS)%VR=AINTF%DRYR(ISI)%VR
        PIDRYRC(IS)=AINTF%DRYRC(ISI)
        PIISMIN(IS)=1
        PIISMAX(IS)=ISAINT
      ENDDO
!
!     * DRY PARTICLE SIZES AT SECTION BOUNDARIES FOR EXTERNALLY
!     * MIXED AEROSOL TYPES.
!
      IF ( ISAEXTB > 0 ) THEN
        ISC=0
        ISX=0
        DO IS=1,ISAEXT-1
          KX=SEXTF%ISAER(IS)%ITYP
          KXP=SEXTF%ISAER(IS+1)%ITYP
          ISC=ISC+1
          ISX=ISX+1
          PEDRYRB(ISC)=PEDRYR(ISX)%VL
          IF ( KXP /= KX ) THEN
            ISC=ISC+1
            PEDRYRB(ISC)=PEDRYR(ISX)%VR
          ENDIF
        ENDDO
        ISC=ISC+1
        ISX=ISX+1
        PEDRYRB(ISC)=PEDRYR(ISX)%VL
        ISC=ISC+1
        PEDRYRB(ISC)=PEDRYR(ISX)%VR
      ENDIF
!
!     * DRY PARTICLE SIZES AT SECTION BOUNDARIES FOR INTERNALLY
!     * MIXED AEROSOL TYPE.
!
      IF ( ISAINTB > 0 ) THEN
        ISC=0
        ISX=0
        DO IS=1,ISAINT-1
          ISC=ISC+1
          ISX=ISX+1
          PIDRYRB(ISC)=PIDRYR(ISX)%VL
        ENDDO
        ISC=ISC+1
        ISX=ISX+1
        PIDRYRB(ISC)=PIDRYR(ISX)%VL
        ISC=ISC+1
        PIDRYRB(ISC)=PIDRYR(ISX)%VR
      ENDIF
!
!-----------------------------------------------------------------------
!     * SEARCH FOR NEAREST VALUE OF PSI RELATIVE TO THE REFERENCE
!     * (MASTER-PSI) FROM THE INPUT THAT IS NOT ASSOCIATED WITH ANY
!     * UNDEFINED VALUE OF PHI FOR GIVEN RATIO RAT IN THE TABLE. NO
!     * RESULT IS RETURNED IN CASE ONLY UNDEFINED VALUE OF PHI EXISTS
!     * IN THE TABLE. IN THAT CASE, A DELTA-FUNCTION WILL BE USED FOR
!     * THE SIZE DISTRIBUTION IN OTHER PARTS OF THE CODE.
!
      DO ISI=1,ISAEXT
        KX=SEXTF%ISAER(ISI)%ITYP
        IS=SEXTF%ISAER(ISI)%ISI
        NT=AEXTF%TP(KX)%NTBL
        IF ( AEXTF%TP(KX)%PSIM(IS) < YLARGES ) THEN
          IF ( AEXTF%TP(KX)%PSIM(IS) > YTINY ) THEN
!
!           * DETERMINE INDEX OF NEAREST POINT.
!
            IDPR=INA
            ADIST=YLARGE
            DO IP=1,TBLT(NT)%IDP
              ADTMP=ABS(TBLT(NT)%PSIP(IP)-AEXTF%TP(KX)%PSIM(IS))
              IF ( ADTMP < ADIST ) THEN
                ADIST=ADTMP
                IDPR=IP
              ENDIF
            ENDDO
!
!           * CHECK WHETHER THE RANGE OF PSI IS SUFFICIENT.
!
            IF ( IDPR == INA  ) THEN
              CALL XIT ('SDCONF',-7)
            ENDIF
            IF ( (IDPR == 1 &
            .AND. (TBLT(NT)%PSIP(IDPR)-AEXTF%TP(KX)%PSIM(IS)) > YTINY ) &
              .OR. (IDPR == TBLT(NT)%IDP &
            .AND. (TBLT(NT)%PSIP(IDPR)-AEXTF%TP(KX)%PSIM(IS)) <-YTINY ) &
                                                                 ) THEN
              PSIT=TBLT(NT)%PSIP(IDPR)
              IF ( MYNODE.EQ.0 ) THEN
                CALL WRN('SDCONF',-2)
                WRITE(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                  '  TYPE,IS,PSIM,PSIP = ',AEXTF%TP(KX)%NAME(1:12),IS, &
                                           AEXTF%TP(KX)%PSIM(IS),PSIT
              ENDIF
            ENDIF
          ELSE
!
!           * DETERMINE INDEX OF NEAREST POINT.
!
            IDPR=INA
            ADIST=YLARGE
            DO IP=1,TBLT(NT)%IDP
              ADTMP=ABS(TBLT(NT)%PSIN(IP)-AEXTF%TP(KX)%PSIM(IS))
              IF ( ADTMP < ADIST ) THEN
                ADIST=ADTMP
                IDPR=IP
              ENDIF
            ENDDO
!
!           * CHECK WHETHER THE RANGE OF PSI IS SUFFICIENT.
!
            IF ( IDPR == INA  ) THEN
              CALL XIT ('SDCONF',-8)
            ENDIF
            IF ( (IDPR == 1 &
             .AND. (TBLT(NT)%PSIN(IDPR)-AEXTF%TP(KX)%PSIM(IS)) < YTINY) &
              .OR. (IDPR == TBLT(NT)%IDP &
             .AND. (TBLT(NT)%PSIN(IDPR)-AEXTF%TP(KX)%PSIM(IS)) >-YTINY) &
                                                                 ) THEN
              PSIT=TBLT(NT)%PSIN(IDPR)
              IF ( MYNODE.EQ.0 ) THEN
                CALL WRN('SDCONF',-3)
                WRITE(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                  '  TYPE,IS,PSIM,PSIN = ',AEXTF%TP(KX)%NAME(1:12),IS, &
                                           AEXTF%TP(KX)%PSIM(IS),PSIT
              ENDIF
            ENDIF
          ENDIF
!
!         * FIRST SEARCH IN DIRECTION OF PSI > PSIM.
!
          IF ( AEXTF%TP(KX)%PSIM(IS) > YTINY ) THEN
            DO IR=1,TBLT(NT)%IDRM
            DO IDPRX=IDPR,TBLT(NT)%IDP
              IF ( &
                    NINT(TBLT(NT)%PHIP(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIP(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AEXTF%TP(KX)%FLG(IS,IR)                 ) THEN
                AEXTF%TP(KX)%PSIB(IS,IR)%VL=TBLT(NT)%PSIP(IDPRX)
                AEXTF%TP(KX)%PSIB(IS,IR)%VR=TBLT(NT)%PSIP(IDPRX)
                AEXTF%TP(KX)%PHIB(IS,IR)%VL=TBLT(NT)%PHIP(IDPRX,IR  )
                AEXTF%TP(KX)%PHIB(IS,IR)%VR=TBLT(NT)%PHIP(IDPRX,IR+1)
                AEXTF%TP(KX)%DPHIB(IS,IR)%VL= &
                                              TBLT(NT)%DPHIP(IDPRX,IR  )
                AEXTF%TP(KX)%DPHIB(IS,IR)%VR= &
                                              TBLT(NT)%DPHIP(IDPRX,IR+1)
                AEXTF%TP(KX)%FLG(IS,IR)=.TRUE.
              ENDIF
            ENDDO
            ENDDO
          ELSE
            DO IR=1,TBLT(NT)%IDRM
            DO IDPRX=IDPR,TBLT(NT)%IDP
              IF ( &
                    NINT(TBLT(NT)%PHIN(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIN(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AEXTF%TP(KX)%FLG(IS,IR)                 ) THEN
                AEXTF%TP(KX)%PSIB(IS,IR)%VL=TBLT(NT)%PSIN(IDPRX)
                AEXTF%TP(KX)%PSIB(IS,IR)%VR=TBLT(NT)%PSIN(IDPRX)
                AEXTF%TP(KX)%PHIB(IS,IR)%VL=TBLT(NT)%PHIN(IDPRX,IR  )
                AEXTF%TP(KX)%PHIB(IS,IR)%VR=TBLT(NT)%PHIN(IDPRX,IR+1)
                AEXTF%TP(KX)%DPHIB(IS,IR)%VL= &
                                              TBLT(NT)%DPHIN(IDPRX,IR  )
                AEXTF%TP(KX)%DPHIB(IS,IR)%VR= &
                                              TBLT(NT)%DPHIN(IDPRX,IR+1)
                AEXTF%TP(KX)%FLG(IS,IR)=.TRUE.
              ENDIF
            ENDDO
            ENDDO
          ENDIF
!
!         * THEN SEARCH IN DIRECTION OF PSI < PSIM, IF FIRST SEARCH
!         * UNSUCCESSFUL.
!
          IF ( AEXTF%TP(KX)%PSIM(IS) > YTINY ) THEN
            DO IR=1,TBLT(NT)%IDRM
            DO IDPRX=IDPR-1,1,-1
              IF ( &
                    NINT(TBLT(NT)%PHIP(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIP(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AEXTF%TP(KX)%FLG(IS,IR)                 ) THEN
                AEXTF%TP(KX)%PSIB(IS,IR)%VL=TBLT(NT)%PSIP(IDPRX)
                AEXTF%TP(KX)%PSIB(IS,IR)%VR=TBLT(NT)%PSIP(IDPRX)
                AEXTF%TP(KX)%PHIB(IS,IR)%VL=TBLT(NT)%PHIP(IDPRX,IR  )
                AEXTF%TP(KX)%PHIB(IS,IR)%VR=TBLT(NT)%PHIP(IDPRX,IR+1)
                AEXTF%TP(KX)%DPHIB(IS,IR)%VL= &
                                              TBLT(NT)%DPHIP(IDPRX,IR  )
                AEXTF%TP(KX)%DPHIB(IS,IR)%VR= &
                                              TBLT(NT)%DPHIP(IDPRX,IR+1)
                AEXTF%TP(KX)%FLG(IS,IR)=.TRUE.
              ENDIF
            ENDDO
            ENDDO
          ELSE
            DO IR=1,TBLT(NT)%IDRM
            DO IDPRX=IDPR-1,1,-1
              IF ( &
                    NINT(TBLT(NT)%PHIN(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIN(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AEXTF%TP(KX)%FLG(IS,IR)                 ) THEN
                AEXTF%TP(KX)%PSIB(IS,IR)%VL=TBLT(NT)%PSIN(IDPRX)
                AEXTF%TP(KX)%PSIB(IS,IR)%VR=TBLT(NT)%PSIN(IDPRX)
                AEXTF%TP(KX)%PHIB(IS,IR)%VL=TBLT(NT)%PHIN(IDPRX,IR  )
                AEXTF%TP(KX)%PHIB(IS,IR)%VR=TBLT(NT)%PHIN(IDPRX,IR+1)
                AEXTF%TP(KX)%DPHIB(IS,IR)%VL= &
                                              TBLT(NT)%DPHIN(IDPRX,IR  )
                AEXTF%TP(KX)%DPHIB(IS,IR)%VR= &
                                              TBLT(NT)%DPHIN(IDPRX,IR+1)
                AEXTF%TP(KX)%FLG(IS,IR)=.TRUE.
              ENDIF
            ENDDO
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      IF ( KINT > 0 ) THEN
        NT=AINTF%NTBL
        DO ISI=1,ISAINT
          IS=SINTF%ISAER(ISI)%ISI
          IF ( AINTF%PSIM(IS) < YLARGES ) THEN
            IF ( AINTF%PSIM(IS) > YTINY ) THEN
!
!             * DETERMINE INDEX OF NEAREST POINT.
!
              IDPR=INA
              ADIST=YLARGE
              DO IP=1,TBLT(NT)%IDP
                ADTMP=ABS(TBLT(NT)%PSIP(IP)-AINTF%PSIM(IS))
                IF ( ADTMP < ADIST ) THEN
                  ADIST=ADTMP
                  IDPR=IP
                ENDIF
              ENDDO
!
!             * CHECK WHETHER THE RANGE OF PSI IS SUFFICIENT.
!
              IF ( IDPR == INA  ) THEN
                CALL XIT ('SDCONF',-9)
              ENDIF
              IF ( (IDPR == 1 &
                .AND. (TBLT(NT)%PSIP(IDPR)-AINTF%PSIM(IS)) >  YTINY) &
              .OR. (IDPR == TBLT(NT)%IDP &
                .AND. (TBLT(NT)%PSIP(IDPR)-AINTF%PSIM(IS)) < -YTINY) &
                                                                 ) THEN
                PSIT=TBLT(NT)%PSIP(IDPR)
                IF ( MYNODE.EQ.0 ) THEN
                  CALL WRN('SDCONF',-4)
                  WRITE(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                    '  TYPE,IS,PSIM,PSIP = ','INTERNAL MIX',IS, &
                                             AINTF%PSIM(IS),PSIT
                ENDIF
              ENDIF
            ELSE
!
!             * DETERMINE INDEX OF NEAREST POINT.
!
              IDPR=INA
              ADIST=YLARGE
              DO IP=1,TBLT(NT)%IDP
                ADTMP=ABS(TBLT(NT)%PSIN(IP)-AINTF%PSIM(IS))
                IF ( ADTMP < ADIST ) THEN
                  ADIST=ADTMP
                  IDPR=IP
                ENDIF
              ENDDO
!
!             * CHECK WHETHER THE RANGE OF PSI IS SUFFICIENT.
!
              IF ( IDPR == INA  ) THEN
                CALL XIT ('SDCONF',-10)
              ENDIF
              IF ( (IDPR == 1 &
                .AND. (TBLT(NT)%PSIN(IDPR)-AINTF%PSIM(IS)) <  YTINY) &
              .OR. (IDPR == TBLT(NT)%IDP &
                .AND. (TBLT(NT)%PSIN(IDPR)-AINTF%PSIM(IS)) > -YTINY) &
                                                                 ) THEN
                PSIT=TBLT(NT)%PSIN(IDPR)
                IF ( MYNODE.EQ.0 ) THEN
                  CALL WRN('SDCONF',-5)
                  WRITE(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                    '  TYPE,IS,PSIM,PSIN = ','INTERNAL MIX' ,IS, &
                                             AINTF%PSIM(IS),PSIT
                ENDIF
              ENDIF
            ENDIF
!
!           * FIRST SEARCH IN DIRECTION OF PSI > PSIM.
!
            IF ( AINTF%PSIM(IS) > YTINY ) THEN

              DO IR=1,TBLT(NT)%IDRM
              DO IDPRX=IDPR,TBLT(NT)%IDP
                IF ( &
                    NINT(TBLT(NT)%PHIP(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIP(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AINTF%FLG(IS,IR)                        ) THEN
                  AINTF%PSIB(IS,IR)%VL=TBLT(NT)%PSIP(IDPRX)
                  AINTF%PSIB(IS,IR)%VR=TBLT(NT)%PSIP(IDPRX)
                  AINTF%PHIB(IS,IR)%VL=TBLT(NT)%PHIP(IDPRX,IR  )
                  AINTF%PHIB(IS,IR)%VR=TBLT(NT)%PHIP(IDPRX,IR+1)
                  AINTF%DPHIB(IS,IR)%VL=TBLT(NT)%DPHIP(IDPRX,IR  )
                  AINTF%DPHIB(IS,IR)%VR=TBLT(NT)%DPHIP(IDPRX,IR+1)
                  AINTF%FLG(IS,IR)=.TRUE.
                ENDIF
              ENDDO
              ENDDO
            ELSE
              DO IR=1,TBLT(NT)%IDRM
              DO IDPRX=IDPR,TBLT(NT)%IDP
                IF ( &
                    NINT(TBLT(NT)%PHIN(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIN(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AINTF%FLG(IS,IR)                        ) THEN
                  AINTF%PSIB(IS,IR)%VL=TBLT(NT)%PSIN(IDPRX)
                  AINTF%PSIB(IS,IR)%VR=TBLT(NT)%PSIN(IDPRX)
                  AINTF%PHIB(IS,IR)%VL=TBLT(NT)%PHIN(IDPRX,IR  )
                  AINTF%PHIB(IS,IR)%VR=TBLT(NT)%PHIN(IDPRX,IR+1)
                  AINTF%DPHIB(IS,IR)%VL=TBLT(NT)%DPHIN(IDPRX,IR  )
                  AINTF%DPHIB(IS,IR)%VR=TBLT(NT)%DPHIN(IDPRX,IR+1)
                  AINTF%FLG(IS,IR)=.TRUE.
                ENDIF
              ENDDO
              ENDDO
            ENDIF
!
!           * THEN SEARCH IN DIRECTION OF PSI < PSIM, IF FIRST SEARCH
!           * UNSUCCESSFUL.
!
            IF ( AINTF%PSIM(IS) > YTINY ) THEN
              DO IR=1,TBLT(NT)%IDRM
              DO IDPRX=IDPR-1,1,-1
                IF ( &
                    NINT(TBLT(NT)%PHIP(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIP(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AINTF%FLG(IS,IR)                        ) THEN
                  AINTF%PSIB(IS,IR)%VL=TBLT(NT)%PSIP(IDPRX)
                  AINTF%PSIB(IS,IR)%VR=TBLT(NT)%PSIP(IDPRX)
                  AINTF%PHIB(IS,IR)%VL=TBLT(NT)%PHIP(IDPRX,IR  )
                  AINTF%PHIB(IS,IR)%VR=TBLT(NT)%PHIP(IDPRX,IR+1)
                  AINTF%DPHIB(IS,IR)%VL=TBLT(NT)%DPHIP(IDPRX,IR  )
                  AINTF%DPHIB(IS,IR)%VR=TBLT(NT)%DPHIP(IDPRX,IR+1)
                  AINTF%FLG(IS,IR)=.TRUE.
                ENDIF
              ENDDO
              ENDDO
            ELSE
              DO IR=1,TBLT(NT)%IDRM
              DO IDPRX=IDPR-1,1,-1
                IF ( &
                    NINT(TBLT(NT)%PHIN(IDPRX,IR  )-TBLT(NT)%YNDEF) /= 0 &
              .AND. NINT(TBLT(NT)%PHIN(IDPRX,IR+1)-TBLT(NT)%YNDEF) /= 0 &
              .AND. .NOT.AINTF%FLG(IS,IR)                        ) THEN
                  AINTF%PSIB(IS,IR)%VL=TBLT(NT)%PSIN(IDPRX)
                  AINTF%PSIB(IS,IR)%VR=TBLT(NT)%PSIN(IDPRX)
                  AINTF%PHIB(IS,IR)%VL=TBLT(NT)%PHIN(IDPRX,IR  )
                  AINTF%PHIB(IS,IR)%VR=TBLT(NT)%PHIN(IDPRX,IR+1)
                  AINTF%DPHIB(IS,IR)%VL=TBLT(NT)%DPHIN(IDPRX,IR  )
                  AINTF%DPHIB(IS,IR)%VR=TBLT(NT)%DPHIN(IDPRX,IR+1)
                  AINTF%FLG(IS,IR)=.TRUE.
                ENDIF
              ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDDO
      ENDIF
!
!     * ALLOCATE ARRAYS FOR PLA TRANFORMATION PARAMETERS AND INITIALIZE.
!
      IDRBM=0
      IF ( ISAEXT > 0 ) THEN
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IDRBM=MAX(IDRBM,AEXTF%TP(KX)%IDRB)
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DO IS=1,ISAINT
          IDRBM=MAX(IDRBM,AINTF%IDRB)
        ENDDO
      ENDIF
      ISATR=MAX(ISAEXT,ISAINT)
      IF ( ISAEXT > 0 ) THEN
        IF( ALLOCATED(PEXT)) DEALLOCATE(PEXT)
        ALLOCATE(PEXT(ISATR))
        DO IS=1,ISATR
          IF( ALLOCATED(PEXT(IS)%FLG)) DEALLOCATE(PEXT(IS)%FLG)
          ALLOCATE(PEXT(IS)%FLG  (IDRBM))
          IF( ALLOCATED(PEXT(IS)%PSIB)) DEALLOCATE(PEXT(IS)%PSIB)
          ALLOCATE(PEXT(IS)%PSIB (IDRBM))
          IF( ALLOCATED(PEXT(IS)%PHIB)) DEALLOCATE(PEXT(IS)%PHIB)
          ALLOCATE(PEXT(IS)%PHIB (IDRBM))
          IF( ALLOCATED(PEXT(IS)%DPHIB)) DEALLOCATE(PEXT(IS)%DPHIB)
          ALLOCATE(PEXT(IS)%DPHIB(IDRBM))
        ENDDO
        DO IS=1,ISATR
          PEXT(IS)%IDRB    =INA
          PEXT(IS)%FLG  (:)=.FALSE.
          PEXT(IS)%PSIB (:)%VL=YNA
          PEXT(IS)%PSIB (:)%VR=YNA
          PEXT(IS)%PHIB (:)%VL=YNA
          PEXT(IS)%PHIB (:)%VR=YNA
          PEXT(IS)%DPHIB(:)%VL=YNA
          PEXT(IS)%DPHIB(:)%VR=YNA
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
        IF( ALLOCATED(PINT)) DEALLOCATE(PINT)
        ALLOCATE(PINT(ISATR))
        DO IS=1,ISATR
          IF( ALLOCATED(PINT(IS)%FLG)) DEALLOCATE(PINT(IS)%FLG)
          ALLOCATE(PINT(IS)%FLG(IDRBM))
          IF( ALLOCATED(PINT(IS)%PSIB)) DEALLOCATE(PINT(IS)%PSIB)
          ALLOCATE(PINT(IS)%PSIB (IDRBM))
          IF( ALLOCATED(PINT(IS)%PHIB)) DEALLOCATE(PINT(IS)%PHIB)
          ALLOCATE(PINT(IS)%PHIB (IDRBM))
          IF( ALLOCATED(PINT(IS)%DPHIB)) DEALLOCATE(PINT(IS)%DPHIB)
          ALLOCATE(PINT(IS)%DPHIB(IDRBM))
        ENDDO
        DO IS=1,ISATR
          PINT(IS)%IDRB    =INA
          PINT(IS)%FLG  (:)=.FALSE.
          PINT(IS)%PSIB (:)%VL=YNA
          PINT(IS)%PSIB (:)%VR=YNA
          PINT(IS)%PHIB (:)%VL=YNA
          PINT(IS)%PHIB (:)%VR=YNA
          PINT(IS)%DPHIB(:)%VL=YNA
          PINT(IS)%DPHIB(:)%VR=YNA
        ENDDO
      ENDIF
!
!     * COPY RESULTS FOR PLA TRANFORMATION PARAMETERS.
!
      IF ( ISAEXT > 0 ) THEN
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          PEXT(IS)%IDRB=AEXTF%TP(KX)%IDRB
        ENDDO
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          ISI=SEXTF%ISAER(IS)%ISI
          DO IR=1,PEXT(IS)%IDRB
            PEXT(IS)%FLG  (IR)=AEXTF%TP(KX)%FLG  (ISI,IR)
            PEXT(IS)%PSIB (IR)=AEXTF%TP(KX)%PSIB (ISI,IR)
            PEXT(IS)%PHIB (IR)=AEXTF%TP(KX)%PHIB (ISI,IR)
            PEXT(IS)%DPHIB(IR)=AEXTF%TP(KX)%DPHIB(ISI,IR)
          ENDDO
        ENDDO
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DO IS=1,ISAINT
          PINT(IS)%IDRB=AINTF%IDRB
        ENDDO
        DO IS=1,ISAINT
          ISI=SINTF%ISAER(IS)%ISI
          DO IR=1,PINT(IS)%IDRB
            PINT(IS)%FLG  (IR)=AINTF%FLG  (ISI,IR)
            PINT(IS)%PSIB (IR)=AINTF%PSIB (ISI,IR)
            PINT(IS)%PHIB (IR)=AINTF%PHIB (ISI,IR)
            PINT(IS)%DPHIB(IR)=AINTF%DPHIB(ISI,IR)
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * ENSEMBLE OF TEST PARTICLES FOR AEROSOL WATER CONTENT
!     * CALCULATIONS. A LARGER NUMBER OF TEST PARTICLES WILL
!     * BE USED AT SMALL PARTICLE SIZES TO INCREASE THE ACCURACY
!     * FOR THE KELVIN EFFECT.
!
      IF ( KEXT > 0 ) THEN
        IF( ALLOCATED(AEXTFG%SEC)) DEALLOCATE(AEXTFG%SEC)
        ALLOCATE (AEXTFG%SEC(ISAEXT))
        IF( ALLOCATED(RDRYME)) DEALLOCATE(RDRYME)
        ALLOCATE (RDRYME(NUMTPE,KEXT))
        DO KX=1,KEXT
!
!         * SELECT RANGE OF TABULATED PARTICLE DENSITY DATA CORRESPONDING
!         * TO SIMULATED PARTICLE SIZE SPECTRUM.
!
          ITS=1
          DO IT=1,IDP-1
            IF ( AEXTF%TP(KX)%RADB > RDRY(IT) ) ITS=IT+1
          END DO
          ITE=IDP
          DO IT=IDP,2,-1
            IF ( AEXTF%TP(KX)%RADE < RDRY(IT) ) ITE=IT-1
          END DO
          ITT=ITE-ITS+1
          IF ( ITT < 1 ) CALL XIT('SDCONF',-11)
!
!         * NORMALIZED PARTICLE DENSITY DATA.
!
          FGRWT=0.
          DO IT=ITS,ITE
            FGRWT=FGRWT+FGRW(IT)
          ENDDO
          ALLOCATE (FGRWS(ITT))
          ALLOCATE (RDRYS(ITT))
          ITX=0
          DO IT=ITS,ITE
            ITX=ITX+1
            FGRWS(ITX)=REAL(NUMTPE)*FGRW(IT)/FGRWT
            RDRYS(ITX)=RDRY(IT)
          ENDDO
!
!         * SELECT TEST PARTICLES BASED ON CRITERION OF EQUAL
!         * PARTICLE DENSITIES.
!
          RDRYB=RDRYS(1)
          ITX=0
          FGRWT=FGRWS(1)
          DO IT=2,ITT-1
            FGRWT=FGRWT+FGRWS(IT)
            IF ( FGRWT >= 1. ) THEN
              RDRYE=RDRYS(IT-1)
              ITX=ITX+1
              RDRYME(ITX,KX)=SQRT(RDRYB*RDRYE)
              RDRYB=RDRYS(IT)
              FGRWT=FGRWT-1.
            ENDIF
          ENDDO
          RDRYME(NUMTPE,KX)=SQRT(RDRYB*RDRYS(ITT))
          DEALLOCATE (FGRWS)
          DEALLOCATE (RDRYS)
        ENDDO
        ALLOCATE (INTP(ISAEXT))
        ALLOCATE (RDRYMO(NUMTPE,ISAEXT))
        INTP=0
        DO IS=1,ISAEXT
          ISI=SEXTF%ISAER(IS)%ISI
          KX=SEXTF%ISAER(IS)%ITYP
!
!         * CHECK WHETHER THERE IS AT LEAST ONE TEST PARTICLE
!         * PER PLA SIZE SECTION.
!
          DO IT=1,NUMTPE
            IF (    RDRYME(IT,KX) >= AEXTF%TP(KX)%DRYR(ISI)%VL &
              .AND. RDRYME(IT,KX) <  AEXTF%TP(KX)%DRYR(ISI)%VR ) THEN
              INTP(IS)=INTP(IS)+1
              RDRYMO(INTP(IS),IS)=RDRYME(IT,KX)
            ENDIF
          ENDDO
!
!         * IF THE METHOD ABOVE DOES NOT PRODUCE ANY TEST
!         * PARTICLES, ADD ONE PARTICLE PER SECTION.
!
          IF ( INTP(IS) == 0 ) THEN
            INTP(IS)=INTP(IS)+1
            RDRYMO(INTP(IS),IS)=SQRT(AEXTF%TP(KX)%DRYR(ISI)%VL &
                                    *AEXTF%TP(KX)%DRYR(ISI)%VR)
          ENDIF
          IF( ALLOCATED(AEXTFG%SEC(IS)%RADT)) DEALLOCATE(AEXTFG%SEC(IS)%RADT)
          ALLOCATE (AEXTFG%SEC(IS)%RADT(INTP(IS)))
          AEXTFG%SEC(IS)%NTSTP=INTP(IS)
          DO ITX=1,INTP(IS)
            AEXTFG%SEC(IS)%RADT(ITX)=RDRYMO(ITX,IS)
          ENDDO
        ENDDO
!
!       * DEALLOCATION OF WORK ARRAYS.
!
        DEALLOCATE (RDRYME)
        DEALLOCATE (INTP)
        DEALLOCATE (RDRYMO)
      ENDIF
      IF ( KINT > 0 ) THEN
        IF( ALLOCATED(AINTFG%SEC)) DEALLOCATE(AINTFG%SEC)
        ALLOCATE(AINTFG%SEC(ISAINT))
!
!       * SELECT RANGE OF TABULATED PARTICLE DENSITY DATA CORRESPONDING
!       * TO SIMULATED PARTICLE SIZE SPECTRUM.
!
        ITS=1
        DO IT=1,IDP-1
          IF ( AINTF%RADB > RDRY(IT) ) ITS=IT+1
        END DO
        ITE=IDP
        DO IT=IDP,2,-1
          IF ( AINTF%RADE < RDRY(IT) ) ITE=IT-1
        END DO
        ITT=ITE-ITS+1
        IF ( ITT < 1 ) CALL XIT('SDCONF',-12)
!
!       * NORMALIZED PARTICLE DENSITY DATA.
!
        FGRWT=0.
        DO IT=ITS,ITE
          FGRWT=FGRWT+FGRW(IT)
        ENDDO
        ALLOCATE (FGRWS(ITT))
        ALLOCATE (RDRYS(ITT))
        ALLOCATE (RDRYM(NUMTPI))
        ITX=0
        DO IT=ITS,ITE
          ITX=ITX+1
          FGRWS(ITX)=REAL(NUMTPI)*FGRW(IT)/FGRWT
          RDRYS(ITX)=RDRY(IT)
        ENDDO
!
!       * SELECT TEST PARTICLES BASED ON CRITERION OF EQUAL
!       * PARTICLE DENSITIES.
!
        RDRYB=RDRYS(1)
        ITX=0
        FGRWT=FGRWS(1)
        DO IT=2,ITT-1
          FGRWT=FGRWT+FGRWS(IT)
          IF ( FGRWT >= 1. ) THEN
            RDRYE=RDRYS(IT-1)
            ITX=ITX+1
            RDRYM(ITX)=SQRT(RDRYB*RDRYE)
            RDRYB=RDRYS(IT)
            FGRWT=FGRWT-1.
          ENDIF
        ENDDO
        RDRYM(NUMTPI)=SQRT(RDRYB*RDRYS(ITT))
!
!       * CHECK WHETHER THERE IS AT LEAST ONE TEST PARTICLE
!       * PER PLA SIZE SECTION.
!
        ALLOCATE (INTP(ISAINT))
        ALLOCATE (RDRYMO(NUMTPI,ISAINT))
        INTP=0
        DO IS=1,ISAINT
          ISI=SINTF%ISAER(IS)%ISI
          DO IT=1,NUMTPI
            IF (    RDRYM(IT) >= AINTF%DRYR(ISI)%VL &
              .AND. RDRYM(IT) <  AINTF%DRYR(ISI)%VR ) THEN
              INTP(IS)=INTP(IS)+1
              RDRYMO(INTP(IS),IS)=RDRYM(IT)
            ENDIF
          ENDDO
!
!         * IF THE METHOD ABOVE DOES NOT PRODUCE ANY TEST
!         * PARTICLES, ADD ONE PARTICLE PER SECTION.
!
          IF ( INTP(IS) == 0 ) THEN
            INTP(IS)=INTP(IS)+1
            RDRYMO(INTP(IS),IS)=SQRT(AINTF%DRYR(ISI)%VL &
                                    *AINTF%DRYR(ISI)%VR)
          ENDIF
          IF( ALLOCATED(AINTFG%SEC(IS)%RADT)) DEALLOCATE(AINTFG%SEC(IS)%RADT)
          ALLOCATE (AINTFG%SEC(IS)%RADT(INTP(IS)))
        ENDDO
        DO IS=1,ISAINT
          AINTFG%SEC(IS)%NTSTP=INTP(IS)
          DO ITX=1,INTP(IS)
            AINTFG%SEC(IS)%RADT(ITX)=RDRYMO(ITX,IS)
          ENDDO
        ENDDO
!
!       * DEALLOCATION OF WORK ARRAYS.
!
        DEALLOCATE (FGRWS)
        DEALLOCATE (RDRYS)
        DEALLOCATE (RDRYM)
        DEALLOCATE (INTP)
        DEALLOCATE (RDRYMO)
      ENDIF
!
!-----------------------------------------------------------------------
!     * DETERMINE INDEX FOR PRE-DETERMINED AEROSOL TYPES. THE INDEX FOR
!     * SULPHATE AEROSOL WILL BE USED TO IDENTIFY THE SULPHATE AEROSOL
!     * IN THE GAS-TO-PARTICLE CONVERSION CALCULATIONS. INDICES WILL
!     * ALSO BE USED IN THE AEROSOL GROWTH CALCULATIONS.
!
      DO KX=1,KEXT
        CNAME=AEXTF%TP(KX)%NAME
        IF ( CNAME(1:7) == 'SEASALT'   ) THEN
          KEXTSS =KX
        ENDIF
        IF ( CNAME(1:9) == '(NH4)2SO4' ) THEN
          KEXTSO4=KX
        ENDIF
        IF ( CNAME(1:6) == 'NH4NO3'    ) THEN
          KEXTNO3=KX
        ENDIF
        IF ( CNAME(1:5) == 'NH4CL'     ) THEN
          KEXTCL=KX
        ENDIF
        IF ( CNAME(1:7) == 'ORGCARB'   ) THEN
          KEXTOC =KX
        ENDIF
        IF ( CNAME(1:7) == 'BLCCARB'   ) THEN
          KEXTBC =KX
        ENDIF
        IF ( CNAME(1:7) == 'MINDUST'   ) THEN
          KEXTMD =KX
        ENDIF
        IF ( CNAME(1:5) == 'H2SO4' ) THEN
          KEXTSAC=KX
        ENDIF
      ENDDO
      DO KX=1,KINT
        CNAME=AINTF%TP(KX)%NAME
        IF ( CNAME(1:7) == 'SEASALT'   ) THEN
          KINTSS =KX
        ENDIF
        IF ( CNAME(1:9) == '(NH4)2SO4' ) THEN
          KINTSO4=KX
        ENDIF
        IF ( CNAME(1:6) == 'NH4NO3'    ) THEN
          KINTNO3=KX
        ENDIF
        IF ( CNAME(1:5) == 'NH4CL'     ) THEN
          KINTCL=KX
        ENDIF
        IF ( CNAME(1:7) == 'ORGCARB'   ) THEN
          KINTOC =KX
        ENDIF
        IF ( CNAME(1:7) == 'BLCCARB'   ) THEN
          KINTBC =KX
        ENDIF
        IF ( CNAME(1:7) == 'MINDUST'   ) THEN
          KINTMD =KX
        ENDIF
        IF ( CNAME(1:5) == 'H2SO4' ) THEN
          KINTSAC=KX
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * SEA SALT AEROSOL.
!
      ISEXTSS=0
      ISINTSS=0
      IF ( KEXTSS > 0 ) ISEXTSS=AEXTF%TP(KEXTSS)%ISEC
      IF ( KINTSS > 0 ) ISINTSS=AINTF%TP(KINTSS)%ISCE &
                               -AINTF%TP(KINTSS)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE SEA SALT AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * SEA SALT SIZE DISTRIBUTION.
!
      ISSS=INA
      IF ( KINTSS > 0 ) THEN
        IS=AINTF%TP(KINTSS)%ISCB
        ISSS=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTSS)%ISCB+1,AINTF%TP(KINTSS)%ISCE
          ISSST=SINTF%ISAER(IS)%ITYPT
          IF ( ISSST /= ISSS ) THEN
            CALL XIT('SDCONF',-13)
          ELSE
            DO IT=1,ISSST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-14)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        ISSS=ISSS-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTSS > 0 ) THEN
        IF( ALLOCATED(ITSS)) DEALLOCATE(ITSS)
        ALLOCATE(ITSS(ISSS))
        IS=AINTF%TP(KINTSS)%ISCB
        ITT=0
        DO IT=1,ISSS+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTSS ) THEN
            ITT=ITT+1
            ITSS(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF SEA SALT AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTSS > 0 ) THEN
        IF(ALLOCATED(IEXSS)) DEALLOCATE(IEXSS)
        ALLOCATE(IEXSS(ISEXTSS))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTSS ) THEN
            ISC=ISC+1
            IEXSS(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTSS > 0 ) THEN
        IF( ALLOCATED(IINSS)) DEALLOCATE(IINSS)
        ALLOCATE(IINSS(ISINTSS))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTSS ) THEN
              ISC=ISC+1
              IINSS(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * SULPHATE AEROSOL.
!
      ISEXTSO4=0
      ISINTSO4=0
      IF ( KEXTSO4 > 0 ) ISEXTSO4=AEXTF%TP(KEXTSO4)%ISEC
      IF ( KINTSO4 > 0 ) ISINTSO4=AINTF%TP(KINTSO4)%ISCE &
                                 -AINTF%TP(KINTSO4)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE SULPHATE AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * SULPHATE SIZE DISTRIBUTION.
!
      ISO4S=INA
      IF ( KINTSO4 > 0 ) THEN
        IS=AINTF%TP(KINTSO4)%ISCB
        ISO4S=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTSO4)%ISCB+1,AINTF%TP(KINTSO4)%ISCE
          ISO4ST=SINTF%ISAER(IS)%ITYPT
          IF ( ISO4ST /= ISO4S ) THEN
            CALL XIT('SDCONF',-15)
          ELSE
            DO IT=1,ISO4ST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-16)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        ISO4S=ISO4S-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTSO4 > 0 ) THEN
        IF( ALLOCATED(ITSO4)) DEALLOCATE(ITSO4)
        ALLOCATE(ITSO4(ISO4S))
        IS=AINTF%TP(KINTSO4)%ISCB
        ITT=0
        DO IT=1,ISO4S+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTSO4 ) THEN
            ITT=ITT+1
            ITSO4(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF SULPHATE AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTSO4 > 0 ) THEN
        IF(ALLOCATED(IEXSO4)) DEALLOCATE(IEXSO4)
        ALLOCATE(IEXSO4(ISEXTSO4))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTSO4 ) THEN
            ISC=ISC+1
            IEXSO4(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTSO4 > 0 ) THEN
        IF(ALLOCATED(IINSO4)) DEALLOCATE(IINSO4)
        ALLOCATE(IINSO4(ISINTSO4))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTSO4 ) THEN
              ISC=ISC+1
              IINSO4(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * NITRATE AEROSOL.
!
      ISEXTNO3=0
      ISINTNO3=0
      IF ( KEXTNO3 > 0 ) ISEXTNO3=AEXTF%TP(KEXTNO3)%ISEC
      IF ( KINTNO3 > 0 ) ISINTNO3=AINTF%TP(KINTNO3)%ISCE &
                                 -AINTF%TP(KINTNO3)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE NITRATE AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * NITRATE SIZE DISTRIBUTION.
!
      INO3S=INA
      IF ( KINTNO3 > 0 ) THEN
        IS=AINTF%TP(KINTNO3)%ISCB
        INO3S=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTNO3)%ISCB+1,AINTF%TP(KINTNO3)%ISCE
          INO3ST=SINTF%ISAER(IS)%ITYPT
          IF ( INO3ST /= INO3S ) THEN
            CALL XIT('SDCONF',-17)
          ELSE
            DO IT=1,INO3ST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-18)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        INO3S=INO3S-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTNO3 > 0 ) THEN
        IF( ALLOCATED(ITNO3)) DEALLOCATE(ITNO3)
        ALLOCATE(ITNO3(INO3S))
        IS=AINTF%TP(KINTNO3)%ISCB
        ITT=0
        DO IT=1,INO3S+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTNO3 ) THEN
            ITT=ITT+1
            ITNO3(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF NITRATE AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTNO3 > 0 ) THEN
        IF( ALLOCATED(IEXNO3)) DEALLOCATE(IEXNO3)
        ALLOCATE(IEXNO3(ISEXTNO3))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTNO3 ) THEN
            ISC=ISC+1
            IEXNO3(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTNO3 > 0 ) THEN
        IF( ALLOCATED(IINNO3)) DEALLOCATE(IINNO3)
        ALLOCATE(IINNO3(ISINTNO3))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTNO3 ) THEN
              ISC=ISC+1
              IINNO3(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * CHLORIDE AEROSOL.
!
      ISEXTCL=0
      ISINTCL=0
      IF ( KEXTCL > 0 ) ISEXTCL=AEXTF%TP(KEXTCL)%ISEC
      IF ( KINTCL > 0 ) ISINTCL=AINTF%TP(KINTCL)%ISCE &
                               -AINTF%TP(KINTCL)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE CHLORIDE AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * CHLORIDE SIZE DISTRIBUTION.
!
      ICLS=INA
      IF ( KINTCL > 0 ) THEN
        IS=AINTF%TP(KINTCL)%ISCB
        ICLS=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTCL)%ISCB+1,AINTF%TP(KINTCL)%ISCE
          ICLST=SINTF%ISAER(IS)%ITYPT
          IF ( ICLST /= ICLS ) THEN
            CALL XIT('SDCONF',-19)
          ELSE
            DO IT=1,ICLST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-20)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        ICLS=ICLS-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTCL > 0 ) THEN
        IF( ALLOCATED(ITCL)) DEALLOCATE(ITCL)
        ALLOCATE(ITCL(ICLS))
        IS=AINTF%TP(KINTCL)%ISCB
        ITT=0
        DO IT=1,ICLS+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTCL ) THEN
            ITT=ITT+1
            ITCL(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF CHLORIDE AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTCL > 0 ) THEN
        IF(ALLOCATED(IEXCL)) DEALLOCATE(IEXCL)
        ALLOCATE(IEXCL(ISEXTCL))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTCL ) THEN
            ISC=ISC+1
            IEXCL(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTCL > 0 ) THEN
        IF( ALLOCATED(IINCL)) DEALLOCATE(IINCL)
        ALLOCATE(IINCL(ISINTCL))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTCL ) THEN
              ISC=ISC+1
              IINCL(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * ORGANIC CARBON AEROSOL.
!
      ISEXTOC=0
      ISINTOC=0
      IF ( KEXTOC > 0 ) ISEXTOC=AEXTF%TP(KEXTOC)%ISEC
      IF ( KINTOC > 0 ) ISINTOC=AINTF%TP(KINTOC)%ISCE &
                               -AINTF%TP(KINTOC)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE ORGANIC CARBON AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * ORGANIC CARBON SIZE DISTRIBUTION.
!
      IOCS=INA
      IF ( KINTOC > 0 ) THEN
        IS=AINTF%TP(KINTOC)%ISCB
        IOCS=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTOC)%ISCB+1,AINTF%TP(KINTOC)%ISCE
          IOCST=SINTF%ISAER(IS)%ITYPT
          IF ( IOCST /= IOCS ) THEN
            CALL XIT('SDCONF',-21)
          ELSE
            DO IT=1,IOCST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-22)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        IOCS=IOCS-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTOC > 0 ) THEN
        IF( ALLOCATED(ITOC)) DEALLOCATE(ITOC)
        ALLOCATE(ITOC(IOCS))
        IS=AINTF%TP(KINTOC)%ISCB
        ITT=0
        DO IT=1,IOCS+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTOC ) THEN
            ITT=ITT+1
            ITOC(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF ORGANIC CARBON AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTOC > 0 ) THEN
        IF( ALLOCATED(IEXOC)) DEALLOCATE(IEXOC)
        ALLOCATE(IEXOC(ISEXTOC))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTOC ) THEN
            ISC=ISC+1
            IEXOC(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTOC > 0 ) THEN
        IF( ALLOCATED(IINOC)) DEALLOCATE(IINOC)
        ALLOCATE(IINOC(ISINTOC))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTOC ) THEN
              ISC=ISC+1
              IINOC(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * BLACK CARBON AEROSOL.
!
      ISEXTBC=0
      ISINTBC=0
      IF ( KEXTBC > 0 ) ISEXTBC=AEXTF%TP(KEXTBC)%ISEC
      IF ( KINTBC > 0 ) ISINTBC=AINTF%TP(KINTBC)%ISCE &
                               -AINTF%TP(KINTBC)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE BLACK CARBON AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * BLACK CARBON SIZE DISTRIBUTION.
!
      IBCS=INA
      IF ( KINTBC > 0 ) THEN
        IS=AINTF%TP(KINTBC)%ISCB
        IBCS=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTBC)%ISCB+1,AINTF%TP(KINTBC)%ISCE
          IBCST=SINTF%ISAER(IS)%ITYPT
          IF ( IBCST /= IBCS ) THEN
            CALL XIT('SDCONF',-23)
          ELSE
            DO IT=1,IBCST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-24)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        IBCS=IBCS-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTBC > 0 ) THEN
        IF( ALLOCATED(ITBC)) DEALLOCATE(ITBC)
        ALLOCATE(ITBC(IBCS))
        IS=AINTF%TP(KINTBC)%ISCB
        ITT=0
        DO IT=1,IBCS+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTBC ) THEN
            ITT=ITT+1
            ITBC(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF BLACK CARBON AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTBC > 0 ) THEN
        IF( ALLOCATED(IEXBC)) DEALLOCATE(IEXBC)
        ALLOCATE(IEXBC(ISEXTBC))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTBC ) THEN
            ISC=ISC+1
            IEXBC(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTBC > 0 ) THEN
        IF( ALLOCATED(IINBC)) DEALLOCATE(IINBC)
        ALLOCATE(IINBC(ISINTBC))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTBC ) THEN
              ISC=ISC+1
              IINBC(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * MIN. DUST AEROSOL.
!
      ISEXTMD=0
      ISINTMD=0
      IF ( KEXTMD > 0 ) ISEXTMD=AEXTF%TP(KEXTMD)%ISEC
      IF ( KINTMD > 0 ) ISINTMD=AINTF%TP(KINTMD)%ISCE &
                               -AINTF%TP(KINTMD)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE MIN. DUST AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * MIN. DUST SIZE DISTRIBUTION.
!
      IMDS=INA
      IF ( KINTMD > 0 ) THEN
        IS=AINTF%TP(KINTMD)%ISCB
        IMDS=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTMD)%ISCB+1,AINTF%TP(KINTMD)%ISCE
          IMDST=SINTF%ISAER(IS)%ITYPT
          IF ( IMDST /= IMDS ) THEN
            CALL XIT('SDCONF',-25)
          ELSE
            DO IT=1,IMDST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-26)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        IMDS=IMDS-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTMD > 0 ) THEN
        IF(ALLOCATED(ITMD)) DEALLOCATE(ITMD)
        ALLOCATE(ITMD(IMDS))
        IS=AINTF%TP(KINTMD)%ISCB
        ITT=0
        DO IT=1,IMDS+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTMD ) THEN
            ITT=ITT+1
            ITMD(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF MIN. DUST AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTMD > 0 ) THEN
        IF( ALLOCATED(IEXMD)) DEALLOCATE(IEXMD)
        ALLOCATE(IEXMD(ISEXTMD))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTMD ) THEN
            ISC=ISC+1
            IEXMD(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTMD > 0 ) THEN
        IF(ALLOCATED(IINMD)) DEALLOCATE(IINMD)
        ALLOCATE(IINMD(ISINTMD))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTMD ) THEN
              ISC=ISC+1
              IINMD(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * NUMBER OF SECTIONS FOR EXTERNALLY RESP. INTERNALLY MIXED
!     * SULPHURIC ACID AEROSOL.
!
      ISEXTSAC=0
      ISINTSAC=0
      IF ( KEXTSAC > 0 ) ISEXTSAC=AEXTF%TP(KEXTSAC)%ISEC
      IF ( KINTSAC > 0 ) ISINTSAC=AINTF%TP(KINTSAC)%ISCE &
                                 -AINTF%TP(KINTSAC)%ISCB+1
!
!     * NUMBER OF ADDITIONAL CHEMICAL SPECIES FOR INTERALLY MIXED AEROSOL
!     * WITHIN THE PARTICLE SIZE RANGE OF THE SULPHATE AEROSOL. MAKE SURE
!     * THAT THE OTHER INTERNAL AEROSOL SPECIES FULLY OVERLAP WITH THE
!     * SULPHATE SIZE DISTRIBUTION.
!
      ISACS=INA
      IF ( KINTSAC > 0 ) THEN
        IS=AINTF%TP(KINTSAC)%ISCB
        ISACS=SINTF%ISAER(IS)%ITYPT
        DO IS=AINTF%TP(KINTSAC)%ISCB+1,AINTF%TP(KINTSAC)%ISCE
          ISACST=SINTF%ISAER(IS)%ITYPT
          IF ( ISACST /= ISACS ) THEN
            CALL XIT('SDCONF',-15)
          ELSE
            DO IT=1,ISACST
              IF ( SINTF%ISAER(IS)%ITYP(IT) &
                                   /= SINTF%ISAER(IS-1)%ITYP(IT) ) THEN
                CALL XIT('SDCONF',-16)
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        ISACS=ISACS-1
      ENDIF
!
!     * AEROSOL TRACER INDEX FOR ADDITIONAL CHEMICAL SPECIES.
!
      IF ( KINTSAC > 0 ) THEN
        IF( ALLOCATED(ITSAC)) DEALLOCATE(ITSAC)
        ALLOCATE(ITSAC(ISACS))
        IS=AINTF%TP(KINTSAC)%ISCB
        ITT=0
        DO IT=1,ISACS+1
          KX=SINTF%ISAER(IS)%ITYP(IT)
          IF ( KX /= KINTSAC ) THEN
            ITT=ITT+1
            ITSAC(ITT)=KX
          ENDIF
        ENDDO
      ENDIF
!
!     * INDEX OF SULPHATE AEROSOL TRACERS IN AEROSOL MASS AND NUMBER
!     * ARRAYS.
!
      IF ( KEXTSAC > 0 ) THEN
        IF(ALLOCATED(IEXSAC)) DEALLOCATE(IEXSAC)
        ALLOCATE(IEXSAC(ISEXTSAC))
        ISC=0
        DO IS=1,ISAEXT
          KX=SEXTF%ISAER(IS)%ITYP
          IF ( KX == KEXTSAC ) THEN
            ISC=ISC+1
            IEXSAC(ISC)=IS
          ENDIF
        ENDDO
      ENDIF
      IF ( KINTSAC > 0 ) THEN
        IF(ALLOCATED(IINSAC)) DEALLOCATE(IINSAC)
        ALLOCATE(IINSAC(ISINTSAC))
        ISC=0
        DO IS=1,ISAINT
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTSAC ) THEN
              ISC=ISC+1
              IINSAC(ISC)=IS
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * ARRAYS FOR COAGULATION.
!
      IF ( KCOAG ) THEN
        IF ( ISFINT > 0 ) THEN
          IF( ALLOCATED(FIPHI)) DEALLOCATE(FIPHI)
          ALLOCATE(FIPHI  (ISFINT))
          IF( ALLOCATED(FIDRYR)) DEALLOCATE(FIDRYR)
          ALLOCATE(FIDRYR (ISFINT))
          IF( ALLOCATED(FIDRYRC)) DEALLOCATE(FIDRYRC)
          ALLOCATE(FIDRYRC(ISFINT))
          IF( ALLOCATED(IRO0))  DEALLOCATE(IRO0)
          ALLOCATE(IRO0   (ISFINT))
          IF( ALLOCATED(IRO)) DEALLOCATE(IRO)
          ALLOCATE(IRO    (ISFINT,ISFINT))
          IF( ALLOCATED(ITR)) DEALLOCATE(ITR)
          ALLOCATE(ITR    (ISFINT,ISFINT))
          IF( ALLOCATED(ITRM)) DEALLOCATE(ITRM)
          ALLOCATE(ITRM   (ISFINT,ISFINT))
        ENDIF
        IF ( ISFINTB > 0 ) THEN
          IF( ALLOCATED(FIDRYRB)) DEALLOCATE(FIDRYRB)
          ALLOCATE(FIDRYRB(ISFINTB))
          IF( ALLOCATED(FIDRYVB)) DEALLOCATE(FIDRYVB)
          ALLOCATE(FIDRYVB(ISFINTB))
          IF( ALLOCATED(FIPHIB0)) DEALLOCATE(FIPHIB0)
          ALLOCATE(FIPHIB0(ISFINT,ISFINT))
        ENDIF
!
!       * PLA SECTION INDEX CORRESPONDING TO INDICES FOR COAGULATION
!       * SIZE DISTRIBUTIONS.
!
        ISC=0
        DO IS=1,ISAINT
        DO IST=1,COAGP%ISEC
          ISC=ISC+1
          IRO0(ISC)=IS
        ENDDO
        ENDDO
        DO IS=1,ISAINT
          ISI=SINTF%ISAER(IS)%ISI
          ISCS=(ISI-1)*COAGP%ISEC+1
          ISCE=ISI*COAGP%ISEC
          IRO(ISCS:ISCE,ISCS:ISFINT)=ISI
          IRO(ISCS:ISFINT,ISCS:ISCE)=ISI
        ENDDO
        ITR=INA
        ITRM=INA
        ITRI=0
        ITRIM=0
        DO IS=1,ISFINT
          DO IK=1,IS-1
            ITRI=ITRI+1
            ITRIM=ITRIM+1
            ITR (IS,IK)=ITRI
            ITRM(IS,IK)=ITRIM
          ENDDO
          IK=IS
          ITRI=ITRI+1
          ITR (IS,IK)=ITRI
        ENDDO
!
!       * CALCULATE SIZES FOR DRY SIZE DISTRIBUTION FOR INTERNALLY MIXED
!       * AEROSOL TYPES FOR COAGULATION CALCULATIONS.
!
        DPHIC=COAGP%DPSTAR
        ISC=0
        DO IS=1,ISAINT
          ISI=SINTF%ISAER(IS)%ISI
          ISC=ISC+1
          FIPHI(ISC)%VL=AINTF%PHI(ISI)%VL
          FIPHI(ISC)%VR=AINTF%PHI(ISI)%VL+DPHIC
          DO IST=2,COAGP%ISEC
            ISC=ISC+1
            FIPHI(ISC)%VL=FIPHI(ISC-1)%VR
            FIPHI(ISC)%VR=FIPHI(ISC-1)%VR+DPHIC
          ENDDO
        ENDDO
!
!       * CALCULATE RADII FOR DRY SIZE DISTRIBUTION FOR INTERNALLY MIXED
!       * AEROSOL TYPES FOR COAGULATION CALCULATIONS.
!
        DO ISC=1,ISFINT
          FIDRYR(ISC)%VL=R0*EXP(FIPHI(ISC)%VL)
          FIDRYR(ISC)%VR=R0*EXP(FIPHI(ISC)%VR)
        ENDDO
        DO ISC=1,ISFINT
          DPHIC=FIPHI(ISC)%VR-FIPHI(ISC)%VL
          FIDRYRC(ISC)=R0*EXP(FIPHI(ISC)%VL+.5*DPHIC)
        ENDDO
        IF ( ISFINTB > 0 ) THEN
          ISC=0
          ISX=0
          DO IS=1,ISAINT-1
          DO IST=1,COAGP%ISEC
            ISC=ISC+1
            ISX=ISX+1
            FIDRYRB(ISC)=FIDRYR(ISX)%VL
          ENDDO
          ENDDO
          DO IST=1,COAGP%ISEC
            ISC=ISC+1
            ISX=ISX+1
            FIDRYRB(ISC)=FIDRYR(ISX)%VL
          ENDDO
          ISC=ISC+1
          FIDRYRB(ISC)=FIDRYR(ISX)%VR
        ENDIF
        FIDRYVB=FIDRYRB**3
!
!       * PROJECTED PARTICLE SIZES FOR DRY SIZES. IT IS ASSUMED
!       * THAT THE GROWTH FACTORS FOR THE WET PARTICLE SIZE ARE
!       * IDENTICAL FOR EACH SECTION SO THAT THE FOLLOWING PARTICLE
!       * SIZES CAN EASILY CONVERTED INTO PROJECTED WET PARTICLE
!       * SIZES.
!
        FIPHIB0=YNA
        DO IS=1,ISFINT
        DO IK=1,IS
          FIPHIB0(IS,IK)=FIPHI(1)%VL+(1./3.)*LOG(REAL(2**(IS)) &
                                   -EXP(3.*(FIPHI(IK)%VL-FIPHI(1)%VL)))
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * SUMMARIZE AEROSOL PARAMETERS.
!
      IF ( MYNODE.EQ.0 ) CALL SDSUM
!
      END SUBROUTINE SDCONF
