      SUBROUTINE CNRAT(RATP0,IRDS,AK,BK,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     SELECTION OF APPROPRIATE LOOK-UP-TABLE FOR GIVEN KOEHLER TERMS
!     FOR DROPLET GROWTH CALCULATIONS.
!
!     HISTORY:
!     --------
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE CNPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      LOGICAL, PARAMETER :: KIO=.FALSE.
!      LOGICAL, PARAMETER :: KIO=.TRUE.
      INTEGER, PARAMETER :: IOF=50
!
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: AK
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: BK
      INTEGER, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: IRDS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: RATP0
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: RAT0,RDIFF,RDIFFT
!
!-----------------------------------------------------------------------
!     * ALLOCATE WORK ARRAYS.
!
      ALLOCATE(RAT0  (ILGA,LEVA,ISEC))
      ALLOCATE(RDIFF (ILGA,LEVA,ISEC))
      ALLOCATE(RDIFFT(ILGA,LEVA,ISEC))
!
!-----------------------------------------------------------------------
!     * KOEHLER PARAMETER RATIO TO DETERMINE THE APPROPRIATE DATA TABLE.
!     * THE APPROACH IS TO SELECT THE TABLE THAT PRODUCES THE BEST
!     * AGREEMENT FOR THE PARAMETER RATIO.
!
      RAT0=YNA
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ABS(BK(IL,L,IS)-YNA) > YSMALL ) THEN
          RAT0(IL,L,IS)=BK(IL,L,IS)/(2.*AK(IL,L))
        ENDIF
      ENDDO
      ENDDO
      ENDDO
      RDIFF=YLARGE
      RATP0=YNA
      IRDS=IDEF
      DO IRAT=1,IRDP
      DO IS=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
        IF ( ABS(RAT0(IL,L,IS)-YNA) > YSMALL ) THEN
          RDIFFT(IL,L,IS)=ABS(RAT0(IL,L,IS)-RATP(IRAT))
          IF ( RDIFFT(IL,L,IS) < RDIFF(IL,L,IS) ) THEN
            RDIFF(IL,L,IS)=RDIFFT(IL,L,IS)
            IRDS(IL,L,IS)=IRAT
            RATP0(IL,L,IS)=RATP(IRAT)
          ENDIF
        ENDIF
      ENDDO
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * WRITE OUT POINTS.
!
      IF ( KIO ) THEN
        DO IS=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
          WRITE(IOF, &
            '(A30,I3,1X,I3,1X,I3,I3,E10.4,1X,E10.4,1X,E10.4,1X,E10.4)') &
                 'IL,L,IS,IRDS,RAT0,RATP0,AK,BK=',IL,L,IS, &
                 IRDS(IL,L,IS),RAT0(IL,L,IS),RATP0(IL,L,IS), &
                 AK(IL,L),BK(IL,L,IS)
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * DEALLOCATE WORK ARRAYS.
!
      DEALLOCATE(RAT0)
      DEALLOCATE(RDIFF)
      DEALLOCATE(RDIFFT)
!
      END SUBROUTINE CNRAT
