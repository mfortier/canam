      SUBROUTINE FILLH(VAVM,ZHA,VAREF,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     FILL HOLES IN VERTICAL PROFILE BY LINEAR INTERPOLATION AND
!     EXTRAPOLATION.
!
!     HISTORY:
!     --------
!     * DEC 29/2009 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN) :: VAREF
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: ZHA
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: VAVM
!
!-----------------------------------------------------------------------
!
      DO IL=1,ILGA
        LEVPH=INA
        DO L=LEVA,1,-1
          IF ( ABS(VAVM(IL,L)-YNA) > YTINY .AND. LEVPH == INA ) THEN
            LEVPH=L
          ENDIF
        ENDDO
        IF ( ABS(VAVM(IL,LEVA)-YNA) <= YTINY ) THEN
          IF ( LEVPH /= INA ) THEN
            VAVM(IL,LEVPH+1:LEVA)=VAVM(IL,LEVPH)
          ELSE
            VAVM(IL,:)=VAREF
          ENDIF
        ENDIF
        LEVPL=LEVPH
        LEVPL0=LEVPL
        LEVPH=INA
        IF ( LEVPL0 /= INA ) THEN
          DO L=LEVPL0-1,1,-1
            IF ( ABS(VAVM(IL,L)-YNA) > YTINY .AND. LEVPH == INA ) THEN
              LEVPH=L
            ENDIF
            IF ( LEVPH /= INA ) THEN
              DZT=ZHA(IL,LEVPH)-ZHA(IL,LEVPL)
              DO LX=LEVPL-1,LEVPH+1,-1
                WGT=(ZHA(IL,LEVPH)-ZHA(IL,LX))/DZT
                VAVM(IL,LX)=WGT*VAVM(IL,LEVPL)+(1.-WGT)*VAVM(IL,LEVPH)
              ENDDO
              LEVPL=LEVPH
              LEVPH=INA
            ENDIF
          ENDDO
          VAVM(IL,1:LEVPL)=VAVM(IL,LEVPL)
        ENDIF
      ENDDO
!
      END SUBROUTINE FILLH
