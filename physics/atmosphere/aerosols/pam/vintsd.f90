      SUBROUTINE VINTSD (VVOL,PEN0,PEPHI0,PEPSI,PEDPHI0,PEPHIS0, &
                         PEWETRC,PENUM,PEMAS,PIN0,PIPHI0,PIPSI, &
                         PIDPHI0,PIPHIS0,PIWETRC,PINUM,PIMAS,VITRM, &
                         RADS,RADE,ISDIAG,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     VOLUME AEROSOL SIZE DISTRIBUTION USING BIN SCHEME - FOR DIAGNOSTIC
!     PURPOSES.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: VITRM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PEN0,PEPHI0,PEPSI, &
                                                   PEDPHI0,PEPHIS0, &
                                                   PEWETRC,PENUM,PEMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIN0,PIPHI0,PIPSI, &
                                                   PIDPHI0,PIPHIS0, &
                                                   PIWETRC,PINUM,PIMAS
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDIAG) :: VVOL
      REAL, DIMENSION(ILGA,LEVA,ISDIAG) :: FVOLO,FIVOL,FEVOL,FPHIS0, &
                                           FDPHI0
!
!-----------------------------------------------------------------------
!     * WIDTH OF DIAGNOSTIC SECTIONS.
!
      FPHIS=LOG(RADS/R0)
      FPHIE=LOG(RADE/R0)
      FDPHI0S=(FPHIE-FPHIS)/REAL(ISDIAG)
      FDPHI0=FDPHI0S
!
!     * PARTICLE SIZES CORRESPONDING TO SECTION BOUNDARIES.
!
      FPHIS0(:,:,1)=FPHIS
      DO IS=2,ISDIAG
        FPHIS0(:,:,IS)=FPHIS0(:,:,1)+REAL(IS-1)*FDPHI0(:,:,IS-1)
      ENDDO
!
!-----------------------------------------------------------------------
!     * STRENGTH OF FILTER. NO FILTER, IF ZERO. THE PARTICLE SIZE
!     * RANGE OF THE FILTER IS ALWAYS SMALLER THAN THE SMALLEST
!     * SECTION WIDTH.
!
      IF ( ISAINT > 0 ) THEN
        NSUB=INT(AINTF%DPSTAR/FDPHI0S)
      ENDIF
      IF ( ISAEXT > 0 ) THEN
        DO KX=1,KEXT
          NSUB=MIN(NSUB,INT(AEXTF%TP(KX)%DPSTAR/FDPHI0S))
        ENDDO
      ENDIF
!      NFILT=INT(0.5*(REAL(NSUB)-1.))
      NFILT=1
!
!-----------------------------------------------------------------------
!     * INTEGRATE CONCENTRATIONS OVER SUB-SECTIONS AND FILTER.
!
      FVOLO=0.
      IF ( ISAINT > 0 ) THEN
        CALL INTSD(FIVOL,FPHIS0,FDPHI0,PINUM,PIMAS,PIN0,PIPHI0, &
                   PIPSI,PIPHIS0,PIDPHI0,PIWETRC,PIDRYRC, &
                   ISDIAG,NFILT,ILGA,LEVA,ISAINT)
        FVOLO=FVOLO+FIVOL
      ENDIF
      IF ( ISAEXT > 0 ) THEN
        CALL INTSD(FEVOL,FPHIS0,FDPHI0,PENUM,PEMAS,PEN0,PEPHI0, &
                   PEPSI,PEPHIS0,PEDPHI0,PEWETRC,PEDRYRC, &
                   ISDIAG,NFILT,ILGA,LEVA,ISAEXT)
        FVOLO=FVOLO+FEVOL
      ENDIF
!
!     * VERTICALLY INTEGRATED VOLUME SIZE DISTRIBUTION.
!
      DO IL=1,ILGA
        VVOL(IL,:)=0.
        DO L=1,LEVA
          VVOL(IL,:)=VVOL(IL,:)+FVOLO(IL,L,:)*VITRM(IL,L)
        ENDDO
      ENDDO
!
      END SUBROUTINE VINTSD
