C
      MODULE CNPARMT
C-----------------------------------------------------------------------
C     PURPOSE:
C     --------
C     BASIC TEMPORARY WORK PARAMETERS FOR CCN CALCULATIONS, RELEVANT
C     TO TABULATED GROWTH DATA.
C
C     HISTORY:
C     --------
C     * AUG 10/2006 - K.VONSALZEN   NEW.
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
C
      TYPE REGO
        INTEGER :: IBT,IRT
        REAL, ALLOCATABLE, DIMENSION(:,:) :: BS
        REAL, ALLOCATABLE, DIMENSION(:,:,:) :: XP,TP
      END TYPE REGO
      TYPE(REGO), ALLOCATABLE, DIMENSION(:) :: RGPNO
      TYPE PNTDPT
        REAL, ALLOCATABLE, DIMENSION(:) :: PNTL,DERL,PNTU,DERU
      END TYPE PNTDPT
      TYPE(PNTDPT), ALLOCATABLE, DIMENSION(:) :: RGBT
      TYPE REGT
        LOGICAL, ALLOCATABLE, DIMENSION(:,:,:) :: ICHKB
        INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: IPLNKA,IPLNKB,
     1                                            ISLNKA,ISLNKB
        INTEGER, ALLOCATABLE, DIMENSION(:,:) :: IPL,IPU,IPD,IPM,IPLNKI,
     1                                          IPNL
      END TYPE REGT
      TYPE(REGT), ALLOCATABLE, DIMENSION(:) :: ITMP
C
      END MODULE CNPARMT
