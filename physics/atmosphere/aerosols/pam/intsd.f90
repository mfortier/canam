      SUBROUTINE INTSD(FVOLO,FPHIS0,FDPHI0,PNUM,PMAS,PN0,PHI0, &
                       PSI,PHIS0,DPHI0,WETRC,DRYRC, &
                       ISDIAG,NFILT,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     INTEGRATION OF AEROSOL SIZE DISTRIBUTION FOR DIAGNOSTIC PURPOSES.
!     A FILTER IS AVAILABLE FOR REMOVING UNRESOLVED/UNREALISTIC FEATURES
!     OF THE SIZE DISTRIBUTION.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) ::  PHIS0,DPHI0,PNUM, &
                                                      PMAS,PN0,PSI, &
                                                      PHI0,WETRC
      REAL, INTENT(IN), DIMENSION(ISEC)              :: DRYRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISDIAG)  :: FPHIS0,FDPHI0
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISDIAG) :: FVOLO
      REAL, DIMENSION(ILGA,LEVA,ISDIAG) :: FMOM3
!
!-----------------------------------------------------------------------
!     * INTEGRATE PLA SIZE DISTRIBUTION OVER GIVEN SUB-SECTIONS.
!
      RMOM3=3.
      FMOM3=0.
      IF ( ISEC > 0 ) THEN
        DO IS=1,ISEC
        DO ISD=1,ISDIAG
        DO L=1,LEVA
        DO IL=1,ILGA
          FPH1=PHIS0(IL,L,IS)
          FPH2=PHIS0(IL,L,IS)+DPHI0(IL,L,IS)
          IF ( PNUM(IL,L,IS) > YTINY .AND. PMAS(IL,L,IS) > YTINY &
                             .AND. ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
            FGR=WETRC(IL,L,IS)/DRYRC(IS)
            ALFGR=LOG(FGR)
            FPH1=FPH1+ALFGR
            FPH2=FPH2+ALFGR
          ELSE
            ALFGR=0.
          ENDIF
          IF (    ( FPHIS0(IL,L,ISD) >= FPH1 &
              .AND. FPHIS0(IL,L,ISD) <= FPH2 ) &
            .OR. ( (FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD)) >= FPH1 &
             .AND. (FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD)) <= FPH2 ) ) THEN
!
!           * FOR FIRST PART OF THE SECTION.
!
            FPHIS1=MAX(FPHIS0(IL,L,ISD),FPH1)
            FPHIE1=MIN(FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD),FPH2)
            FDPHI1=MAX(FPHIE1-FPHIS1,0.)
            PHI0T=PHI0(IL,L,IS)+ALFGR
            IF ( ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
              FMOM3(IL,L,ISD)=FMOM3(IL,L,ISD)+YCNST*PN0(IL,L,IS) &
                      *SDINTB(PHI0T,PSI(IL,L,IS),RMOM3,FPHIS1,FDPHI1)
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * FILTER THE VOLUME SIZE DISTRIBUTION. THE NUMBER OF SUB-SECTIONS
!     * OVER THE WHICH THE FILTER IS APPLIED IS 2*NFILT+1.
!
      FRAC=1./REAL(2*NFILT+1)
      FVOLO=FMOM3
      DO ISX=1,ISDIAG
        DO ISC=1,NFILT
          IST=ISX+ISC
          IF ( IST <= ISDIAG ) THEN
            FVOLO(:,:,ISX)=FVOLO(:,:,ISX)+FMOM3(:,:,IST)
          ENDIF
        ENDDO
        DO ISC=1,NFILT
          IST=ISX-ISC
          IF ( IST >= 1 ) THEN
            FVOLO(:,:,ISX)=FVOLO(:,:,ISX)+FMOM3(:,:,IST)
          ENDIF
        ENDDO
        FVOLO(:,:,ISX)=FVOLO(:,:,ISX)*FRAC
      ENDDO
!
      END SUBROUTINE INTSD
