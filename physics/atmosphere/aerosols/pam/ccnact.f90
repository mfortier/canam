      SUBROUTINE CCNACT(SVB,SV,CECDNC,CICDNC,QTNA,HMNA,TA, &
                        ZHA,ZFA,PHA,PFA,RHOA,WCTA,PEN0,PEPHI0,PEPSI, &
                        PEPHIS0,PEDPHI0,PIN0,PIPHI0,PIPSI,PIPHIS0, &
                        PIDPHI0,CEWETRB,CEDDNSL,CEDDNIS,CENUIO,CEKAPPA, &
                        CEMOLW,CEPHIS0,CEDPHI0,CIWETRB,CIDDNSL,CIDDNIS, &
                        CINUIO,CIKAPPA,CIMOLW,CIEPSM,CIPHIS0,CIDPHI0, &
                        ZFSA,PFSA,ZCLF,CLWC,ZCTHR,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     MAXIMUM SUPERSATURATION IN CLOUD LAYER. A PARCEL MODEL IS
!     USED TO CALCULATE AEROSOL ACTIVATION AND VERTICAL PROFILES
!     OF SUPERSATURATION BASED ON DROPLET GROWTH EQUATION.
!
!     HISTORY:
!     --------
!     * JUN 10/2014 - K.VONSALZEN   EXTERNAL CLOUD THRESHOLD
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SCPARM
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) ::   SVB,SV
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,KEXT) :: CECDNC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) ::   CICDNC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) ::    ZCLF,CLWC, &
                                                   TA,ZHA,RHOA,PHA, &
                                                   WCTA,ZFA,QTNA, &
                                                   HMNA,PFA
      REAL, INTENT(IN), DIMENSION(ILGA) ::         ZFSA,PFSA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PEN0,PEPHI0,PEPSI, &
                                                   PEPHIS0,PEDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIN0,PIPHI0,PIPSI, &
                                                   PIPHIS0,PIDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXT) :: CEPHIS0,CEDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINT) :: CIPHIS0,CIDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXTB) :: &
                                                   CEWETRB,CEDDNSL, &
                                                   CEDDNIS,CENUIO, &
                                                   CEMOLW,CEKAPPA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINTB) :: &
                                                   CIWETRB,CIDDNSL, &
                                                   CIDDNIS,CINUIO, &
                                                   CIMOLW,CIKAPPA, &
                                                   CIEPSM
!
      REAL, DIMENSION(ILGA,LEVA+1) :: ZFAT,PFAT,RFA,TFA,WCF, &
                                      HLSTF,QRTF
      REAL, DIMENSION(ILGA,LEVA) :: HLST,QRT
      INTEGER, DIMENSION(ILGA,LEVA) :: IACTT,LBOT,LTOP,LMAX
      LOGICAL, DIMENSION(ILGA,LEVA) :: KFC,KFB
      INTEGER, DIMENSION(ILGA) :: IACT,IACTTO,LEVB
      INTEGER, DIMENSION(LEVA) :: LACTI
      LOGICAL, DIMENSION(ILGA) :: KFCO
!
      REAL, ALLOCATABLE, DIMENSION(:,:) ::   SVT,QRVT,RST,RHOAT, &
                                             PAT,TAT,QRTT,QRCT,DRDTT, &
                                             HLSTT,DHDTT,ZHT,WCT,DTCT, &
                                             CONVF,SVTT,TATT,RSVT, &
                                             DHDT,DRDT,QRC,DTC
      REAL, ALLOCATABLE, DIMENSION(:) ::     SVTL,SVTP
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TECDNC
      REAL, ALLOCATABLE, DIMENSION(:,:) ::   TECDNCL,TECDNCP
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TEWETRB,TEDDNSL,TEDDNIS, &
                                             TENUIO,TEMOLW,TERC,TESC, &
                                             TEN0,TEPHI0,TEPSI,TETSCL, &
                                             TEKAPPA,UEN0,UEPSI,UEPHI0, &
                                             UEPHIS0,UEDPHI0,TEWMASS
      REAL(R8), ALLOCATABLE, DIMENSION(:,:,:) :: &
                                             TEMOM0,TEMOM2,TEMOM3, &
                                             TEMOM4,TEMOM5,TEMOM6, &
                                             TEMOM7,TEMOM8
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: REWETRB,REDDNSL,REDDNIS, &
                                             RENUIO,REMOLW,REKAPPA, &
                                             REN0,REPHI0,REPSI, &
                                             VEN0,VEPSI,VEPHI0, &
                                             VEPHIS0,VEDPHI0
      REAL(R8), ALLOCATABLE, DIMENSION(:,:,:) :: &
                                             REMOM0,REMOM2,REMOM3, &
                                             REMOM4,REMOM5,REMOM6, &
                                             REMOM7,REMOM8
      REAL, ALLOCATABLE, DIMENSION(:,:) ::   TICDNC
      REAL, ALLOCATABLE, DIMENSION(:) ::     TICDNCL,TICDNCP
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TIWETRB,TIDDNSL,TIDDNIS, &
                                             TINUIO,TIMOLW,TIRC,TISC, &
                                             TIN0,TIPHI0,TIPSI,TITSCL, &
                                             TIKAPPA,UIN0,UIPSI,UIPHI0, &
                                             UIPHIS0,UIDPHI0,TIWMASS
      REAL(R8), ALLOCATABLE, DIMENSION(:,:,:) :: &
                                             TIMOM0,TIMOM2,TIMOM3, &
                                             TIMOM4,TIMOM5,TIMOM6, &
                                             TIMOM7,TIMOM8
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TIEPSM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: RIWETRB,RIDDNSL,RIDDNIS, &
                                             RINUIO,RIMOLW,RIKAPPA, &
                                             RIN0,RIPHI0,RIPSI, &
                                             VIN0,VIPSI,VIPHI0, &
                                             VIPHIS0,VIDPHI0
      REAL(R8), ALLOCATABLE, DIMENSION(:,:,:) :: &
                                             RIMOM0,RIMOM2,RIMOM3, &
                                             RIMOM4,RIMOM5,RIMOM6, &
                                             RIMOM7,RIMOM8
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: RIEPSM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TEPHIS0,TEDPHI0,SEN0, &
                                             SEPHI0,SEPSI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TIPHIS0,TIDPHI0,SIN0, &
                                             SIPHI0,SIPSI
      INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: TEMOD1,TEMOD2,TEMOD3, &
                                                TIMOD1,TIMOD2,TIMOD3
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: IACTI,IACTO
      INTEGER, ALLOCATABLE, DIMENSION(:) :: IACTIO
      LOGICAL, ALLOCATABLE, DIMENSION(:,:) :: LMSK
      LOGICAL :: FLG
!
!     * PARAMETERS.
!
      REAL, PARAMETER :: SVCR=0.00001
      REAL, PARAMETER :: WSCAL=1.
      REAL, PARAMETER :: FRER=0.E-03

      INTEGER, PARAMETER :: LEVAT=1
      INTEGER, PARAMETER :: LOFFS=1
      INTEGER, PARAMETER :: NSUB=10
      LOGICAL, PARAMETER :: KDIAG=.FALSE.
!
!-----------------------------------------------------------------------
!     * INTERPOLATE TEMPERATURES, VELOCITIES, AND AIR DENSITY.
!
      ZFAT(:,1:LEVA)=ZFA
      ZFAT(:,LEVA+1)=ZFSA
      PFAT(:,1:LEVA)=PFA
      PFAT(:,LEVA+1)=PFSA
      DO L=2,LEVA
      DO IL=1,ILGA
        WGT=(ZHA(IL,L-1)-ZFAT(IL,L))/(ZHA(IL,L-1)-ZHA(IL,L))
        TFA(IL,L)=(1.-WGT)*  TA(IL,L-1)+WGT*  TA(IL,L)
        WCF(IL,L)=(1.-WGT)*WCTA(IL,L-1)+WGT*WCTA(IL,L)
      ENDDO
      ENDDO
      TFA(:,1)=TA  (:,1)
      WCF(:,1)=WCTA(:,1)
      TFA(:,LEVA+1)=TA  (:,LEVA)
      WCF(:,LEVA+1)=WCTA(:,LEVA)
      RFA=PFAT/(RGAS*TFA)
!
!-----------------------------------------------------------------------
!     * LINEARLY INTERPOLATED INITIAL LIQUID WATER STATIC ENERGY
!     * AT GRID CELL INTERFACES FROM ATMOSPHERIC PROFILES IN
!     * THE ENVIRONMENT OF THE CLOUD.
!
      HLSTF(:,1)=HMNA(:,1)
      DO L=2,LEVA
      DO IL=1,ILGA
        WGT=(ZHA(IL,L-1)-ZFAT(IL,L))/(ZHA(IL,L-1)-ZHA(IL,L))
        HLSTF(IL,L)=(1.-WGT)*HMNA(IL,L-1)+WGT*HMNA(IL,L)
      ENDDO
      ENDDO
      HLSTF(:,LEVA+1)=HLSTF(:,LEVA)
!
!     * FIND BOTTOM AND TOP LEVELS OF THE CLOUD AND PARCEL LIFTING
!     * LEVELS FOR EACH INDIVIDUAL CLOUD LAYER.
!
      DO IL=1,ILGA
        FLG=.FALSE.
        LMAX(IL,:)=INA
        LBOT(IL,:)=LEVA
        LTOP(IL,:)=INA
        DO L=LEVA,1,-1
          IF( (1.-ZCLF(IL,L)) < ZCTHR .AND. CLWC(IL,L) > YSMALL ) THEN
            IF ( .NOT. FLG ) THEN
              LBI=L
              LBOT(IL,LBI)=LBI
              LMAX(IL,LBI)=MIN(LBOT(IL,LBI)+1,LEVA)
            ENDIF
            FLG=.TRUE.
            IF ( L==1 ) LTOP(IL,LBI)=L
          ELSE
            IF ( FLG ) THEN
              LTOP(IL,LBI)=L+1
            ENDIF
            FLG=.FALSE.
          ENDIF
        ENDDO
      ENDDO
      DO IL=1,ILGA
      DO L=LEVA,1,-1
        IF ( LTOP(IL,L) /= INA ) THEN
          LBI=LBOT(IL,L)+1
          LTI=MAX(LTOP(IL,L),2)
!
!         * ENERGY AT GRID CELL INTERFACE AT BOTTOM OF CLOUD LAYER.
!
          HLSTF(IL,LBI)=HMNA(IL,LMAX(IL,L))
!
!         * ENERGY AT HIGHER LEVELS FROM LIFTING OF PARCEL FROM CLOUD
!         * BASE AND ACCOUNTING FOR ENTRAINMENT USING A CONSTANT
!         * FRACTIONAL ENTRAINMENT RATE (FRER).
!
          DO LX=LBI-1,LTI,-1
            DZ=ZFAT(IL,LX)-ZFAT(IL,LX+1)
            HLSTF(IL,LX)=HMNA(IL,LX) &
                        +(HLSTF(IL,LX+1)-HMNA(IL,LX))*EXP(-FRER*DZ)
          ENDDO
          IF ( LTI == 2 ) HLSTF(IL,1)=HLSTF(IL,2)
        ENDIF
      ENDDO
      ENDDO
!
!     * ADJUST THE TOTAL WATER MIXING RATIO SO THAT CONDITIONS ARE
!     * JUST SATURATED SO THAT THE CLOUD BASE IN THE ACTIVATION
!     * CALCULATIONS WILL COINCIDE WITH THE ACTUAL CLOUD BASE IN
!     * THE ATMOSPHERIC MODEL FOR ZERO LIQUID WATER AT CLOUD BASE.
!     * A NON-ZERO AMOUNT OF LIQUID WATER WILL LATER BE ACCOUNTED
!     * FOR IN THE CALCULATION OF DROPLET GROWTH TO ACCOUNT FOR
!     * HYGROSCOPIC GROWTH OF THE AEROSOL AT CLOUD BASE.
!
      ALLOCATE(DHDT(ILGA,LEVA+1))
      ALLOCATE(DRDT(ILGA,LEVA+1))
      ALLOCATE(QRC (ILGA,LEVA+1))
      ALLOCATE(DTC (ILGA,LEVA+1))
      DHDT=0.
      DRDT=0.
      QRTF=0.
      DTC=0.
      QRC=0.
      ALLOCATE(SVTT(ILGA,LEVA+1))
      ALLOCATE(QRVT(ILGA,LEVA+1))
      ALLOCATE(RST (ILGA,LEVA+1))
      ALLOCATE(TATT(ILGA,LEVA+1))
      CALL CNSBND(SVTT,QRVT,RST,TATT,QRC,HLSTF,DHDT,QRTF, &
                  DRDT,ZFAT,PFAT,DTC,ILGA,LEVA+1)
      QRTF=RST
      DEALLOCATE(DHDT)
      DEALLOCATE(DRDT)
      DEALLOCATE(QRC )
      DEALLOCATE(DTC )
      DEALLOCATE(SVTT)
      DEALLOCATE(QRVT)
      DEALLOCATE(TATT)
!
!     * TOTAL WATER AT HIGHER LEVELS FROM LIFTING OF PARCEL FROM CLOUD
!     * BASE AND ACCOUNTING FOR ENTRAINMENT.
!
      DO IL=1,ILGA
      DO L=LEVA,1,-1
        IF ( LTOP(IL,L) /= INA ) THEN
          LBI=LBOT(IL,L)+1
          LTI=MAX(LTOP(IL,L),2)
!
!         * TOTAL WATER AT HIGHER LEVELS FROM LIFTING OF PARCEL FROM CLOUD
!         * BASE AND ACCOUNTING FOR ENTRAINMENT. DO NOT ALLOW ENTRAINMENT
!         * TO PRODUCE SUBSATURATED CONDITIONS.
!
          DO LX=LBI-1,LTI,-1
            DZ=ZFAT(IL,LX)-ZFAT(IL,LX+1)
            QRTF(IL,LX)=QTNA(IL,LX) &
                        +(QRTF(IL,LX+1)-QTNA(IL,LX))*EXP(-FRER*DZ)
            QRTF(IL,LX)=MAX(QRTF(IL,LX),RST(IL,LX))
          ENDDO
          IF ( LTI == 2 ) QRTF(IL,1)=QRTF(IL,2)
        ENDIF
      ENDDO
      ENDDO
      DEALLOCATE(RST)
!
!-----------------------------------------------------------------------
!     * DETERMINE NUMBER OF COLUMNS THAT CONTAIN LIQUID CLOUD WATER
!     * AND ARE THEREFORE SUBJECT TO ACTIVATION CALCULATIONS. ALSO
!     * DETERMINE THE VERTICAL EXTENT OF THE DOMAIN.
!
      JYES=0
      JNO =ILGA+1
      LEVT=LEVA
      DO IL=1,ILGA
        FLG=.FALSE.
        LTMP=LEVA
        DO L=LEVA,1,-1
          IF( (1.-ZCLF(IL,L)) < ZCTHR .AND. CLWC(IL,L) > YSMALL ) THEN
            FLG=.TRUE.
            LTMP=L
          ENDIF
        ENDDO
        LEVT=MIN(LEVT,LTMP)
        IF ( FLG ) THEN
          JYES       =JYES+1
          IACT (JYES)=IL
        ELSE
          JNO        =JNO-1
          IACT (JNO) =IL
       ENDIF
      ENDDO
      LACT=JYES
      LEVT=MAX(LEVT-1,1)
!
!-----------------------------------------------------------------------
!     * ALLOCATION OF WORK ARRAYS AND INITIALIZATIONS.
!
      IF ( LACT > 0 ) THEN
        ALLOCATE(RSVT  (LACT,LEVAT))
        IF ( ISEXTB > 0 ) THEN
          ALLOCATE(REWETRB(LACT,LEVAT,ISEXTB))
          ALLOCATE(REDDNSL(LACT,LEVAT,ISEXTB))
          ALLOCATE(REDDNIS(LACT,LEVAT,ISEXTB))
          ALLOCATE(RENUIO (LACT,LEVAT,ISEXTB))
          ALLOCATE(REKAPPA(LACT,LEVAT,ISEXTB))
          ALLOCATE(REMOLW (LACT,LEVAT,ISEXTB))
        ENDIF
        IF ( ISEXT > 0 ) THEN
          ALLOCATE(REN0   (LACT,LEVAT,ISEXT))
          ALLOCATE(REPHI0 (LACT,LEVAT,ISEXT))
          ALLOCATE(REPSI  (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM0 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM2 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM3 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM4 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM5 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM6 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM7 (LACT,LEVAT,ISEXT))
          ALLOCATE(REMOM8 (LACT,LEVAT,ISEXT))
        ENDIF
        IF ( ISAEXT > 0 ) THEN
          ALLOCATE(VEN0   (LACT,LEVAT,ISAEXT))
          ALLOCATE(VEPSI  (LACT,LEVAT,ISAEXT))
          ALLOCATE(VEPHI0 (LACT,LEVAT,ISAEXT))
          ALLOCATE(VEPHIS0(LACT,LEVAT,ISAEXT))
          ALLOCATE(VEDPHI0(LACT,LEVAT,ISAEXT))
        ENDIF
        IF ( ISINTB > 0 ) THEN
          ALLOCATE(RIWETRB(LACT,LEVAT,ISINTB))
          ALLOCATE(RIDDNSL(LACT,LEVAT,ISINTB))
          ALLOCATE(RIDDNIS(LACT,LEVAT,ISINTB))
          ALLOCATE(RINUIO (LACT,LEVAT,ISINTB))
          ALLOCATE(RIKAPPA(LACT,LEVAT,ISINTB))
          ALLOCATE(RIMOLW (LACT,LEVAT,ISINTB))
          ALLOCATE(RIEPSM (LACT,LEVAT,ISINTB))
        ENDIF
        IF ( ISINT > 0 ) THEN
          ALLOCATE(RIN0   (LACT,LEVAT,ISINT))
          ALLOCATE(RIPHI0 (LACT,LEVAT,ISINT))
          ALLOCATE(RIPSI  (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM0 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM2 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM3 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM4 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM5 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM6 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM7 (LACT,LEVAT,ISINT))
          ALLOCATE(RIMOM8 (LACT,LEVAT,ISINT))
        ENDIF
        IF ( ISAINT > 0 ) THEN
          ALLOCATE(VIN0   (LACT,LEVAT,ISAINT))
          ALLOCATE(VIPSI  (LACT,LEVAT,ISAINT))
          ALLOCATE(VIPHI0 (LACT,LEVAT,ISAINT))
          ALLOCATE(VIPHIS0(LACT,LEVAT,ISAINT))
          ALLOCATE(VIDPHI0(LACT,LEVAT,ISAINT))
        ENDIF
        IF ( KEXT > 0 ) THEN
          ALLOCATE (TECDNCP(ILGA,KEXT))
          ALLOCATE (TECDNCL(ILGA,KEXT))
          TECDNCP=YNA
          TECDNCL=YNA
        ENDIF
        IF ( KINT > 0 ) THEN
          ALLOCATE (TICDNCP(ILGA))
          ALLOCATE (TICDNCL(ILGA))
          TICDNCP=YNA
          TICDNCL=YNA
        ENDIF
        ALLOCATE (SVTP(ILGA))
        ALLOCATE (SVTL(ILGA))
        SVTP=YNA
        SVTL=YNA
        ALLOCATE (IACTI (LACT,LEVA))
        ALLOCATE (IACTO (LACT,LEVA))
        ALLOCATE (IACTIO(LACT))
        IACTO=INA
        IACTI=INA
        KFC  =.FALSE.
        KFCO =.FALSE.
        KFB  =.FALSE.
      ENDIF
      IF ( KEXT > 0 ) THEN
        CECDNC=0.
      ENDIF
      IF ( KINT > 0 ) THEN
        CICDNC=0.
      ENDIF
      SV=0.
      SVB=YNA
!
!-----------------------------------------------------------------------
!     * MAIN LOOP OVER ALL VERTICAL LEVELS.
!
      DO L=LEVA,LEVT+1,-1
!
!       * PARCEL LAUNCHING LEVEL BELOW CLOUD BASE AND NEXT HIGHER LEVEL.
!
        LM=L+1
        LU=L
        LT=MIN(L+LOFFS,LEVA)
!
!       * SEARCH FOR CLOUDY LAYERS AND THEIR BASES. LACTI REPRESENTS
!       * THE NUMBER OF GRID POINTS THAT ARE SUBJECT TO DROPLET GROWTH
!       * CALCULATIONS. IACTI REPRESENTS THE HOR. GRID POINT INDEX
!       * FROM THE NEXT LOWER LEVEL IN THE CLOUD.
!
        LACTI(L)=0
        IF ( LACT > 0 ) THEN
          IACTT(1:ILGA,L)=0
          DO ILL=1,LACT
            IL=IACT(ILL)
!
!           * FLAGS FOR REGION OF CLOUD SUBJECT TO PROGNOSTIC
!           * CALCULATIONS.
!
            IF ( LTOP(IL,L) /= INA ) THEN
              KFC(IL,L)=.TRUE.
              LACTI(L)=LACTI(L)+1
              IACTT(IL,L)=LACTI(L)
              IACTO(LACTI(L),L)=IL
              IF ( .NOT.KFCO(IL) ) THEN
!
!               * CLOUD BASE FLAG.
!
                KFB(IL,L)=.TRUE.
                LEVB(IL)=L
                IACTI(LACTI(L),L)=INA
                SVB(IL,L)=-YLARGE
              ELSE
                IACTI(LACTI(L),L)=IACTIO(IACTTO(IL))
              ENDIF
            ENDIF
            KFCO(IL)=KFC(IL,L)
          ENDDO
!
!         * SAVE INFORMATION FOR USE AT NEXT HIGHER LEVEL.
!
          IF ( LACTI(L)> 0 ) THEN
            DO ILL=1,LACTI(L)
              IACTIO(ILL)   =ILL
              IACTTO(1:ILGA)=IACTT(1:ILGA,L)
            ENDDO
          ENDIF
        ENDIF
!
!       * ALLOCATE TEMPORARY ARRAYS FOR ACTIVATION CALCULATIONS
!       * AND INITIALIZATION.
!
        IF ( LACTI(L) > 0 ) THEN
!
!         * ARRAYS FOR THERMODYANMIC AND DYNAMIC PROPERTIES.
!
          ALLOCATE(LMSK  (LACTI(L),LEVAT))
          ALLOCATE(SVT   (LACTI(L),LEVAT))
          ALLOCATE(SVTT  (LACTI(L),LEVAT))
          ALLOCATE(QRVT  (LACTI(L),LEVAT))
          ALLOCATE(RST   (LACTI(L),LEVAT))
          ALLOCATE(RHOAT (LACTI(L),LEVAT))
          ALLOCATE(PAT   (LACTI(L),LEVAT))
          ALLOCATE(TAT   (LACTI(L),LEVAT))
          ALLOCATE(TATT  (LACTI(L),LEVAT))
          ALLOCATE(QRTT  (LACTI(L),LEVAT))
          ALLOCATE(QRCT  (LACTI(L),LEVAT))
          ALLOCATE(DRDTT (LACTI(L),LEVAT))
          ALLOCATE(HLSTT (LACTI(L),LEVAT))
          ALLOCATE(DHDTT (LACTI(L),LEVAT))
          ALLOCATE(ZHT   (LACTI(L),LEVAT))
          ALLOCATE(WCT   (LACTI(L),LEVAT))
          ALLOCATE(DTCT  (LACTI(L),LEVAT))
          ALLOCATE(CONVF (LACTI(L),LEVAT))
!
!         * ARRAYS FOR ACTIVATION CALCULATIONS.
!
          IF ( KEXT > 0 ) THEN
            ALLOCATE(TECDNC (LACTI(L),LEVAT,KEXT))
          ENDIF
          IF ( ISEXTB > 0 ) THEN
            ALLOCATE(TEWETRB(LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TETSCL (LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TEDDNSL(LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TEDDNIS(LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TENUIO (LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TEKAPPA(LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TEMOLW (LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TERC   (LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TESC   (LACTI(L),LEVAT,ISEXTB))
          ENDIF
          IF ( ISEXT > 0 ) THEN
            ALLOCATE(TEN0   (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEPHI0 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEPSI  (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM0 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM2 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM3 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM4 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM5 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM6 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM7 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEMOM8 (LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEWMASS(LACTI(L),LEVAT,ISEXT))
          ENDIF
          IF ( ISAEXT > 0 ) THEN
            ALLOCATE(UEN0   (LACTI(L),LEVAT,ISAEXT))
            ALLOCATE(UEPSI  (LACTI(L),LEVAT,ISAEXT))
            ALLOCATE(UEPHI0 (LACTI(L),LEVAT,ISAEXT))
            ALLOCATE(UEPHIS0(LACTI(L),LEVAT,ISAEXT))
            ALLOCATE(UEDPHI0(LACTI(L),LEVAT,ISAEXT))
          ENDIF
          IF ( KINT > 0 ) THEN
            ALLOCATE(TICDNC (LACTI(L),LEVAT))
          ENDIF
          IF ( ISINTB > 0 ) THEN
            ALLOCATE(TIWETRB(LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TITSCL (LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIDDNSL(LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIDDNIS(LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TINUIO (LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIKAPPA(LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIMOLW (LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIRC   (LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TISC   (LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIEPSM (LACTI(L),LEVAT,ISINTB))
          ENDIF
          IF ( ISINT > 0 ) THEN
            ALLOCATE(TIN0   (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIPHI0 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIPSI  (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM0 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM2 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM3 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM4 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM5 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM6 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM7 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIMOM8 (LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIWMASS(LACTI(L),LEVAT,ISINT))
          ENDIF
          IF ( ISAINT > 0 ) THEN
            ALLOCATE(UIN0   (LACTI(L),LEVAT,ISAINT))
            ALLOCATE(UIPSI  (LACTI(L),LEVAT,ISAINT))
            ALLOCATE(UIPHI0 (LACTI(L),LEVAT,ISAINT))
            ALLOCATE(UIPHIS0(LACTI(L),LEVAT,ISAINT))
            ALLOCATE(UIDPHI0(LACTI(L),LEVAT,ISAINT))
          ENDIF
!
!         * DIAGNOSTIC ARRAYS.
!
          IF ( ISEXTB > 0 ) THEN
            ALLOCATE(TEMOD1(LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TEMOD2(LACTI(L),LEVAT,ISEXTB))
            ALLOCATE(TEMOD3(LACTI(L),LEVAT,ISEXTB))
          ENDIF
          IF ( ISINTB > 0 ) THEN
            ALLOCATE(TIMOD1(LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIMOD2(LACTI(L),LEVAT,ISINTB))
            ALLOCATE(TIMOD3(LACTI(L),LEVAT,ISINTB))
          ENDIF
!
!         * ARRAYS FOR CALCULATION OF MOMENTS.
!
          IF ( ISEXT > 0 ) THEN
            ALLOCATE(TEPHIS0(LACTI(L),LEVAT,ISEXT))
            ALLOCATE(TEDPHI0(LACTI(L),LEVAT,ISEXT))
          ENDIF
          IF ( ISAEXT > 0 ) THEN
            ALLOCATE(SEN0  (LACTI(L),LEVAT,ISAEXT))
            ALLOCATE(SEPHI0(LACTI(L),LEVAT,ISAEXT))
            ALLOCATE(SEPSI (LACTI(L),LEVAT,ISAEXT))
          ENDIF
          IF ( ISINT > 0 ) THEN
            ALLOCATE(TIPHIS0(LACTI(L),LEVAT,ISINT))
            ALLOCATE(TIDPHI0(LACTI(L),LEVAT,ISINT))
          ENDIF
          IF ( ISAINT > 0 ) THEN
            ALLOCATE(SIN0  (LACTI(L),LEVAT,ISAINT))
            ALLOCATE(SIPHI0(LACTI(L),LEVAT,ISAINT))
            ALLOCATE(SIPSI (LACTI(L),LEVAT,ISAINT))
          ENDIF
        ENDIF
!
!       * INITIAL PARCEL PROPERTIES BELOW BASE OF CLOUD.
!
        DO ILL=1,LACTI(L)
          IL=IACTO(ILL,L)
          IF ( KFB(IL,L) ) THEN
            SVT(ILL,1)=0.
          ENDIF
        ENDDO
!
!       * INITIALIZATION OF PARAMETERS FOR SUBSEQUENT CALCULATION
!       * OF MOMENTS.
!
        DO ILL=1,LACTI(L)
          IL=IACTO(ILL,L)
          IF ( ISEXT > 0 ) THEN
            TEPHIS0(ILL,1,:)=CEPHIS0(IL,LT,:)
            TEDPHI0(ILL,1,:)=CEDPHI0(IL,LT,:)
          ENDIF
          IF ( ISAEXT > 0 ) THEN
            SEN0  (ILL,1,:)=PEN0  (IL,LT,:)
            SEPHI0(ILL,1,:)=PEPHI0(IL,LT,:)
            SEPSI (ILL,1,:)=PEPSI (IL,LT,:)
          ENDIF
          IF ( ISINT > 0 ) THEN
            TIPHIS0(ILL,1,:)=CIPHIS0(IL,LT,:)
            TIDPHI0(ILL,1,:)=CIDPHI0(IL,LT,:)
          ENDIF
          IF ( ISAINT > 0 ) THEN
            SIN0  (ILL,1,:)=PIN0  (IL,LT,:)
            SIPHI0(ILL,1,:)=PIPHI0(IL,LT,:)
            SIPSI (ILL,1,:)=PIPSI (IL,LT,:)
          ENDIF
        ENDDO
!
!       * CALCULATE VARIOUS MOMENTS FOR THE DRY AEROSOL SIZE
!       * DISTRIBUTION BELOW CLOUD BASE. MOMENTS CALCULATED
!       * IN THIS SUBROUTINE WILL BE OVERWRITTEN LATER IF
!       * LAYER IS ABOVE CLOUD BASE.
!
        IF ( LACTI(L) > 0 ) THEN
          CALL SCMOM(TEN0,TEPHI0,TEPSI,TIN0,TIPHI0,TIPSI,TEMOM0, &
                     TEMOM2,TEMOM3,TEMOM4,TEMOM5,TEMOM6,TEMOM7,TEMOM8, &
                     TIMOM0,TIMOM2,TIMOM3,TIMOM4,TIMOM5,TIMOM6,TIMOM7, &
                     TIMOM8,TEPHIS0,TEDPHI0,TIPHIS0,TIDPHI0,SEN0, &
                     SEPHI0,SEPSI,SIN0,SIPHI0,SIPSI,LACTI(L),LEVAT)
        ENDIF
        DO ILL=1,LACTI(L)
          IL=IACTO(ILL,L)
          IF ( KFB(IL,L) ) THEN
!
!           * ARRAYS CHARACTERIZING CONDITIONS BELOW CLOUD BASE.
!
            IF ( ISEXTB > 0 ) THEN
              TEWETRB(ILL,1,:)=CEWETRB(IL,LT,:)
              TEDDNSL(ILL,1,:)=CEDDNSL(IL,LT,:)
              TEDDNIS(ILL,1,:)=CEDDNIS(IL,LT,:)
              TENUIO (ILL,1,:)=CENUIO (IL,LT,:)
              TEKAPPA(ILL,1,:)=CEKAPPA(IL,LT,:)
              TEMOLW (ILL,1,:)=CEMOLW (IL,LT,:)
            ENDIF
            IF ( ISAEXT > 0 ) THEN
              UEN0   (ILL,1,:)=PEN0   (IL,LT,:)
              UEPSI  (ILL,1,:)=PEPSI  (IL,LT,:)
              UEPHI0 (ILL,1,:)=PEPHI0 (IL,LT,:)
              UEPHIS0(ILL,1,:)=PEPHIS0(IL,LT,:)
              UEDPHI0(ILL,1,:)=PEDPHI0(IL,LT,:)
            ENDIF
            IF ( ISINTB > 0 ) THEN
              TIWETRB(ILL,1,:)=CIWETRB(IL,LT,:)
              TIDDNSL(ILL,1,:)=CIDDNSL(IL,LT,:)
              TIDDNIS(ILL,1,:)=CIDDNIS(IL,LT,:)
              TINUIO (ILL,1,:)=CINUIO (IL,LT,:)
              TIKAPPA(ILL,1,:)=CIKAPPA(IL,LT,:)
              TIMOLW (ILL,1,:)=CIMOLW (IL,LT,:)
              TIEPSM (ILL,1,:)=CIEPSM (IL,LT,:)
            ENDIF
            IF ( ISAINT > 0 ) THEN
              UIN0   (ILL,1,:)=PIN0   (IL,LT,:)
              UIPSI  (ILL,1,:)=PIPSI  (IL,LT,:)
              UIPHI0 (ILL,1,:)=PIPHI0 (IL,LT,:)
              UIPHIS0(ILL,1,:)=PIPHIS0(IL,LT,:)
              UIDPHI0(ILL,1,:)=PIDPHI0(IL,LT,:)
            ENDIF
          ELSE
!
!           * COPY RESULTS FROM CALCULATIONS FOR NEXT LOWER CLOUDY
!           * LAYER FOR INITIALIZATION OF CALCULATIONS IN CURRENT LAYER.
!
            ILO=IACTI(ILL,L)
            SVT   (ILL,1)=RSVT  (ILO,1)
            IF ( ISEXTB > 0 ) THEN
              TEWETRB(ILL,1,:)=REWETRB(ILO,1,:)
              TEDDNSL(ILL,1,:)=REDDNSL(ILO,1,:)
              TEDDNIS(ILL,1,:)=REDDNIS(ILO,1,:)
              TENUIO (ILL,1,:)=RENUIO (ILO,1,:)
              TEKAPPA(ILL,1,:)=REKAPPA(ILO,1,:)
              TEMOLW (ILL,1,:)=REMOLW (ILO,1,:)
            ENDIF
            IF ( ISEXT > 0 ) THEN
              TEN0   (ILL,1,:)=REN0   (ILO,1,:)
              TEPHI0 (ILL,1,:)=REPHI0 (ILO,1,:)
              TEPSI  (ILL,1,:)=REPSI  (ILO,1,:)
              TEMOM0 (ILL,1,:)=REMOM0 (ILO,1,:)
              TEMOM2 (ILL,1,:)=REMOM2 (ILO,1,:)
              TEMOM3 (ILL,1,:)=REMOM3 (ILO,1,:)
              TEMOM4 (ILL,1,:)=REMOM4 (ILO,1,:)
              TEMOM5 (ILL,1,:)=REMOM5 (ILO,1,:)
              TEMOM6 (ILL,1,:)=REMOM6 (ILO,1,:)
              TEMOM7 (ILL,1,:)=REMOM7 (ILO,1,:)
              TEMOM8 (ILL,1,:)=REMOM8 (ILO,1,:)
            ENDIF
            IF ( ISAEXT > 0 ) THEN
              UEN0   (ILL,1,:)=VEN0   (ILO,1,:)
              UEPSI  (ILL,1,:)=VEPSI  (ILO,1,:)
              UEPHI0 (ILL,1,:)=VEPHI0 (ILO,1,:)
              UEPHIS0(ILL,1,:)=VEPHIS0(ILO,1,:)
              UEDPHI0(ILL,1,:)=VEDPHI0(ILO,1,:)
            ENDIF
            IF ( ISINTB > 0 ) THEN
              TIWETRB(ILL,1,:)=RIWETRB(ILO,1,:)
              TIDDNSL(ILL,1,:)=RIDDNSL(ILO,1,:)
              TIDDNIS(ILL,1,:)=RIDDNIS(ILO,1,:)
              TINUIO (ILL,1,:)=RINUIO (ILO,1,:)
              TIKAPPA(ILL,1,:)=RIKAPPA(ILO,1,:)
              TIMOLW (ILL,1,:)=RIMOLW (ILO,1,:)
              TIEPSM (ILL,1,:)=RIEPSM (ILO,1,:)
            ENDIF
            IF ( ISINT > 0 ) THEN
              TIN0   (ILL,1,:)=RIN0   (ILO,1,:)
              TIPHI0 (ILL,1,:)=RIPHI0 (ILO,1,:)
              TIPSI  (ILL,1,:)=RIPSI  (ILO,1,:)
              TIMOM0 (ILL,1,:)=RIMOM0 (ILO,1,:)
              TIMOM2 (ILL,1,:)=RIMOM2 (ILO,1,:)
              TIMOM3 (ILL,1,:)=RIMOM3 (ILO,1,:)
              TIMOM4 (ILL,1,:)=RIMOM4 (ILO,1,:)
              TIMOM5 (ILL,1,:)=RIMOM5 (ILO,1,:)
              TIMOM6 (ILL,1,:)=RIMOM6 (ILO,1,:)
              TIMOM7 (ILL,1,:)=RIMOM7 (ILO,1,:)
              TIMOM8 (ILL,1,:)=RIMOM8 (ILO,1,:)
            ENDIF
            IF ( ISAINT > 0 ) THEN
              UIN0   (ILL,1,:)=VIN0   (ILO,1,:)
              UIPSI  (ILL,1,:)=VIPSI  (ILO,1,:)
              UIPHI0 (ILL,1,:)=VIPHI0 (ILO,1,:)
              UIPHIS0(ILL,1,:)=VIPHIS0(ILO,1,:)
              UIDPHI0(ILL,1,:)=VIDPHI0(ILO,1,:)
            ENDIF
          ENDIF
          IF ( .NOT. KFC(IL,L) ) THEN
            SVTP(IL)=YNA
            SVTL(IL)=YNA
            IF ( KEXT > 0 ) THEN
              TECDNCP(IL,:)=YNA
              TECDNCL(IL,:)=YNA
            ENDIF
            IF ( KINT > 0 ) THEN
              TICDNCP(IL)=YNA
              TICDNCL(IL)=YNA
            ENDIF
          ENDIF
        ENDDO
        DO NS=1,NSUB
          WH=REAL(NS-1)/REAL(NSUB)
          WL=1.-WH
          IF ( LACTI(L) > 0 ) LMSK=.TRUE.
          DO ILL=1,LACTI(L)
            IL=IACTO(ILL,L)
!
!           * EXTRACT CONVECTIVE CLOUD PROFILES FROM LINEAR INTERPOLATION
!           * BETWEEN ADJACENT POINTS.
!
            ZHT  (ILL,1)=WL*ZFAT (IL,LM)+WH*ZFAT (IL,LU)
            PAT  (ILL,1)=WL*PFAT (IL,LM)+WH*PFAT (IL,LU)
            TAT  (ILL,1)=WL*TFA  (IL,LM)+WH*TFA  (IL,LU)
            RHOAT(ILL,1)=WL*RFA  (IL,LM)+WH*RFA  (IL,LU)
            WCT  (ILL,1)=WL*WCF  (IL,LM)+WH*WCF  (IL,LU)
            QRTT (ILL,1)=WL*QRTF (IL,LM)+WH*QRTF (IL,LU)
            HLSTT(ILL,1)=WL*HLSTF(IL,LM)+WH*HLSTF(IL,LU)
!
!           * MAKE SURE THAT THE VERTICAL VELOCITY IN THE CLOUD
!           * IS LARGE ENOUGH AND SCALE.
!
            WCT(ILL,1)=WSCAL*MAX(WCT(ILL,1),0.01)
!
!           * TIME CORRESPONDING TO ASCEND OF PARCEL FROM ONE
!           * LAYER TO NEXT HIGHER ONE.
!
            WHT=REAL(NS)/REAL(NSUB)
            WLT=1.-WHT
            ZHTT=WLT*ZFAT(IL,LM)+WHT*ZFAT(IL,LU)
            DTCT(ILL,1)=(ZHTT-ZHT(ILL,1))/WCT(ILL,1)
!
!           * NO EXPLICIT CHANGES IN TOTAL WATER AND ENERGY.
!
            DRDTT(ILL,1)=0.
            DHDTT(ILL,1)=0.
          ENDDO
          IF ( LACTI(L) > 0 ) THEN
!
!           * SUPERSATURATION FROM EQUILIBRIUM CALCULATION UNDER
!           * ASSUMPTION OF DRY CONDITIONS BELOW CLOUD BASE.
!
!            QRCT=0.
!            CALL CNSBND(SVTT,QRVT,RST,TATT,QRCT,HLSTT,DHDTT,QRTT,
!     1                  DRDTT,ZHT,PAT,DTCT,LACTI(L),LEVAT)
!            WHERE ( SVTT < YTINY )
!              SVT=SVTT
!            ENDWHERE
!
!           * AEROSOL ACTIVATION.
!
            CALL CNGRW(SVT,QRVT,RST,QRCT,TAT,TEWMASS,TERC,TESC,TEWETRB, &
                       TETSCL,TIWMASS,TIRC,TISC,TIWETRB,TITSCL,LMSK, &
                       QRTT,DRDTT,HLSTT,DHDTT,ZHT,RHOAT,PAT,TEDDNSL, &
                       TEDDNIS,TENUIO,TEKAPPA,TEMOLW,TEN0,TEPSI,TEMOM2, &
                       TEMOM3,TEMOM4,TEMOM5,TEMOM6,TEMOM7,TEMOM8, &
                       TIDDNSL,TIDDNIS,TINUIO,TIKAPPA,TIMOLW,TIEPSM, &
                       TIN0,TIPSI,TIMOM2,TIMOM3,TIMOM4,TIMOM5,TIMOM6, &
                       TIMOM7,TIMOM8,DTCT,LACTI(L),LEVAT,TEMOD1,TEMOD2, &
                       TEMOD3,TIMOD1,TIMOD2,TIMOD3,CONVF,IEXKAP,IINKAP)
!
!           * DIAGNOSE CLOUD DROPLET NUMBER CONCENTRATION.
!
            IF ( KDIAG ) THEN
              CALL CNCDNCB(TECDNC,TICDNC,TEWETRB,TIWETRB,UEN0,UEPHI0, &
                           UEPSI,UEPHIS0,UEDPHI0,UIN0,UIPHI0,UIPSI, &
                           UIPHIS0,UIDPHI0,LACTI(L),LEVAT)
            ELSE
              DO ILL=1,LACTI(L)
                IF ( KEXT > 0 ) THEN
                  TECDNC(ILL,:,:)=YNA
                ENDIF
                IF ( KINT > 0 ) THEN
                  TICDNC(ILL,:)=YNA
                ENDIF
              ENDDO
            ENDIF
!
!           * AVERAGE CLOUD DROPLET NUMBER CONCENTRATION IN THE LAYER.
!           * ALSO SAVE RESULTS FROM THE LEVEL BELOW.
!
            IF ( KEXT > 0 ) THEN
              DO ILL=1,LACTI(L)
                IL=IACTO(ILL,L)
                IF ( KFC(IL,L) ) THEN
                  TECDNCP(IL,:)=TECDNCL(IL,:)
                  TECDNCL(IL,:)=TECDNC (ILL,1,:)
                  DO KX=1,KEXT
                    CECDNC(IL,L,KX)=CECDNC(IL,L,KX) &
                                          +TECDNC(ILL,1,KX)/REAL(NSUB)
                  ENDDO
                ENDIF
              ENDDO
            ENDIF
            IF ( KINT > 0 ) THEN
              DO ILL=1,LACTI(L)
                IL=IACTO(ILL,L)
                IF ( KFC(IL,L) ) THEN
                  TICDNCP(IL)=TICDNCL(IL)
                  TICDNCL(IL)=TICDNC (ILL,1)
                  CICDNC(IL,L)=CICDNC(IL,L) &
                                     +TICDNC(ILL,1)/REAL(NSUB)
                ENDIF
              ENDDO
            ENDIF
!
!           * SAVE LOCAL SUPERSATURATION AND FROM LEVEL BELOW.
!
            DO ILL=1,LACTI(L)
              IL=IACTO(ILL,L)
              IF ( KFC(IL,L) ) THEN
                SVTP(IL)=SVTL(IL)
                SVTL(IL)=SVT(ILL,1)
                SV(IL,L)=SV(IL,L)+SVT(ILL,1)/REAL(NSUB)
                SVB(IL,LEVB(IL))=MAX(SVT(ILL,1),SVB(IL,LEVB(IL)))
              ENDIF
            ENDDO
          ENDIF
!
!         * SAVE RESULTS FROM PROGNOSTIC DROPLET GROWTH CALCULATIONS
!         * FOR INITIALIZATION OF CALCULATIONS AT NEXT HIGHER LEVEL.
!
          DO ILL=1,LACTI(L)
            RSVT(ILL,1)=SVT(ILL,1)
            IF ( ISEXTB > 0 ) THEN
              REWETRB(ILL,1,:)=TEWETRB(ILL,1,:)
              REDDNSL(ILL,1,:)=TEDDNSL(ILL,1,:)
              REDDNIS(ILL,1,:)=TEDDNIS(ILL,1,:)
              RENUIO (ILL,1,:)=TENUIO (ILL,1,:)
              REKAPPA(ILL,1,:)=TEKAPPA(ILL,1,:)
              REMOLW (ILL,1,:)=TEMOLW (ILL,1,:)
            ENDIF
            IF ( ISEXT > 0 ) THEN
              REN0   (ILL,1,:)=TEN0   (ILL,1,:)
              REPHI0 (ILL,1,:)=TEPHI0 (ILL,1,:)
              REPSI  (ILL,1,:)=TEPSI  (ILL,1,:)
              REMOM0 (ILL,1,:)=TEMOM0 (ILL,1,:)
              REMOM2 (ILL,1,:)=TEMOM2 (ILL,1,:)
              REMOM3 (ILL,1,:)=TEMOM3 (ILL,1,:)
              REMOM4 (ILL,1,:)=TEMOM4 (ILL,1,:)
              REMOM5 (ILL,1,:)=TEMOM5 (ILL,1,:)
              REMOM6 (ILL,1,:)=TEMOM6 (ILL,1,:)
              REMOM7 (ILL,1,:)=TEMOM7 (ILL,1,:)
              REMOM8 (ILL,1,:)=TEMOM8 (ILL,1,:)
            ENDIF
            IF ( ISAEXT > 0 ) THEN
              VEN0   (ILL,1,:)=UEN0   (ILL,1,:)
              VEPSI  (ILL,1,:)=UEPSI  (ILL,1,:)
              VEPHI0 (ILL,1,:)=UEPHI0 (ILL,1,:)
              VEPHIS0(ILL,1,:)=UEPHIS0(ILL,1,:)
              VEDPHI0(ILL,1,:)=UEDPHI0(ILL,1,:)
            ENDIF
            IF ( ISINTB > 0 ) THEN
              RIWETRB(ILL,1,:)=TIWETRB(ILL,1,:)
              RIDDNSL(ILL,1,:)=TIDDNSL(ILL,1,:)
              RIDDNIS(ILL,1,:)=TIDDNIS(ILL,1,:)
              RINUIO (ILL,1,:)=TINUIO (ILL,1,:)
              RIKAPPA(ILL,1,:)=TIKAPPA(ILL,1,:)
              RIMOLW (ILL,1,:)=TIMOLW (ILL,1,:)
              RIEPSM (ILL,1,:)=TIEPSM (ILL,1,:)
            ENDIF
            IF ( ISINT > 0 ) THEN
              RIN0   (ILL,1,:)=TIN0   (ILL,1,:)
              RIPHI0 (ILL,1,:)=TIPHI0 (ILL,1,:)
              RIPSI  (ILL,1,:)=TIPSI  (ILL,1,:)
              RIMOM0 (ILL,1,:)=TIMOM0 (ILL,1,:)
              RIMOM2 (ILL,1,:)=TIMOM2 (ILL,1,:)
              RIMOM3 (ILL,1,:)=TIMOM3 (ILL,1,:)
              RIMOM4 (ILL,1,:)=TIMOM4 (ILL,1,:)
              RIMOM5 (ILL,1,:)=TIMOM5 (ILL,1,:)
              RIMOM6 (ILL,1,:)=TIMOM6 (ILL,1,:)
              RIMOM7 (ILL,1,:)=TIMOM7 (ILL,1,:)
              RIMOM8 (ILL,1,:)=TIMOM8 (ILL,1,:)
            ENDIF
            IF ( ISAINT > 0 ) THEN
              VIN0   (ILL,1,:)=UIN0   (ILL,1,:)
              VIPSI  (ILL,1,:)=UIPSI  (ILL,1,:)
              VIPHI0 (ILL,1,:)=UIPHI0 (ILL,1,:)
              VIPHIS0(ILL,1,:)=UIPHIS0(ILL,1,:)
              VIDPHI0(ILL,1,:)=UIDPHI0(ILL,1,:)
            ENDIF
          ENDDO
        ENDDO
!
!       * DEALLOCATE TEMPORARY ARRAYS FOR ACTIVATION CALCULATIONS.
!
        IF ( LACTI(L) > 0 ) THEN
          DEALLOCATE(LMSK  )
          DEALLOCATE(SVT   )
          DEALLOCATE(SVTT  )
          DEALLOCATE(QRVT  )
          DEALLOCATE(RST   )
          DEALLOCATE(RHOAT )
          DEALLOCATE(PAT   )
          DEALLOCATE(TAT   )
          DEALLOCATE(TATT  )
          DEALLOCATE(QRTT  )
          DEALLOCATE(QRCT  )
          DEALLOCATE(DRDTT )
          DEALLOCATE(HLSTT )
          DEALLOCATE(DHDTT )
          DEALLOCATE(ZHT   )
          DEALLOCATE(WCT   )
          DEALLOCATE(DTCT  )
          DEALLOCATE(CONVF )
          IF ( KEXT > 0 ) THEN
            DEALLOCATE(TECDNC )
          ENDIF
          IF ( ISEXTB > 0 ) THEN
            DEALLOCATE(TEWETRB)
            DEALLOCATE(TETSCL )
            DEALLOCATE(TEDDNSL)
            DEALLOCATE(TEDDNIS)
            DEALLOCATE(TENUIO )
            DEALLOCATE(TEKAPPA)
            DEALLOCATE(TEMOLW )
            DEALLOCATE(TERC   )
            DEALLOCATE(TESC   )
          ENDIF
          IF ( ISEXT > 0 ) THEN
            DEALLOCATE(TEN0   )
            DEALLOCATE(TEPHI0 )
            DEALLOCATE(TEPSI  )
            DEALLOCATE(TEMOM0 )
            DEALLOCATE(TEMOM2 )
            DEALLOCATE(TEMOM3 )
            DEALLOCATE(TEMOM4 )
            DEALLOCATE(TEMOM5 )
            DEALLOCATE(TEMOM6 )
            DEALLOCATE(TEMOM7 )
            DEALLOCATE(TEMOM8 )
            DEALLOCATE(TEWMASS)
          ENDIF
          IF ( ISAEXT > 0 ) THEN
            DEALLOCATE(UEN0   )
            DEALLOCATE(UEPSI  )
            DEALLOCATE(UEPHI0 )
            DEALLOCATE(UEPHIS0)
            DEALLOCATE(UEDPHI0)
          ENDIF
          IF ( KINT > 0 ) THEN
            DEALLOCATE(TICDNC )
          ENDIF
          IF ( ISINTB > 0 ) THEN
            DEALLOCATE(TIWETRB)
            DEALLOCATE(TITSCL )
            DEALLOCATE(TIDDNSL)
            DEALLOCATE(TIDDNIS)
            DEALLOCATE(TINUIO )
            DEALLOCATE(TIKAPPA)
            DEALLOCATE(TIMOLW )
            DEALLOCATE(TIRC   )
            DEALLOCATE(TISC   )
            DEALLOCATE(TIEPSM )
          ENDIF
          IF ( ISINT > 0 ) THEN
            DEALLOCATE(TIN0   )
            DEALLOCATE(TIPHI0 )
            DEALLOCATE(TIPSI  )
            DEALLOCATE(TIMOM0 )
            DEALLOCATE(TIMOM2 )
            DEALLOCATE(TIMOM3 )
            DEALLOCATE(TIMOM4 )
            DEALLOCATE(TIMOM5 )
            DEALLOCATE(TIMOM6 )
            DEALLOCATE(TIMOM7 )
            DEALLOCATE(TIMOM8 )
            DEALLOCATE(TIWMASS)
          ENDIF
          IF ( ISAINT > 0 ) THEN
            DEALLOCATE(UIN0   )
            DEALLOCATE(UIPSI  )
            DEALLOCATE(UIPHI0 )
            DEALLOCATE(UIPHIS0)
            DEALLOCATE(UIDPHI0)
          ENDIF
!
!         * DEALLOCATION OF DIAGNOSTIC ARRAYS.
!
          IF ( ISEXTB > 0 ) THEN
            DEALLOCATE(TEMOD1)
            DEALLOCATE(TEMOD2)
            DEALLOCATE(TEMOD3)
          ENDIF
          IF ( ISINTB > 0 ) THEN
            DEALLOCATE(TIMOD1)
            DEALLOCATE(TIMOD2)
            DEALLOCATE(TIMOD3)
          ENDIF
          IF ( ISEXT > 0 ) THEN
            DEALLOCATE(TEPHIS0)
            DEALLOCATE(TEDPHI0)
          ENDIF
          IF ( ISAEXT > 0 ) THEN
            DEALLOCATE(SEN0  )
            DEALLOCATE(SEPHI0)
            DEALLOCATE(SEPSI )
          ENDIF
          IF ( ISINT > 0 ) THEN
            DEALLOCATE(TIPHIS0)
            DEALLOCATE(TIDPHI0)
          ENDIF
          IF ( ISAINT > 0 ) THEN
            DEALLOCATE(SIN0  )
            DEALLOCATE(SIPHI0)
            DEALLOCATE(SIPSI )
          ENDIF
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!     * DEALLOCATE TEMPORARY ARRAYS.
!
      IF ( LACT > 0 ) THEN
        DEALLOCATE(RSVT  )
        IF ( ISEXTB > 0 ) THEN
          DEALLOCATE(REWETRB)
          DEALLOCATE(REDDNSL)
          DEALLOCATE(REDDNIS)
          DEALLOCATE(RENUIO )
          DEALLOCATE(REKAPPA)
          DEALLOCATE(REMOLW )
        ENDIF
        IF ( ISEXT > 0 ) THEN
          DEALLOCATE(REN0   )
          DEALLOCATE(REPHI0 )
          DEALLOCATE(REPSI  )
          DEALLOCATE(REMOM0 )
          DEALLOCATE(REMOM2 )
          DEALLOCATE(REMOM3 )
          DEALLOCATE(REMOM4 )
          DEALLOCATE(REMOM5 )
          DEALLOCATE(REMOM6 )
          DEALLOCATE(REMOM7 )
          DEALLOCATE(REMOM8 )
        ENDIF
        IF ( ISAEXT > 0 ) THEN
          DEALLOCATE(VEN0   )
          DEALLOCATE(VEPSI  )
          DEALLOCATE(VEPHI0 )
          DEALLOCATE(VEPHIS0)
          DEALLOCATE(VEDPHI0)
        ENDIF
        IF ( ISINTB > 0 ) THEN
          DEALLOCATE(RIWETRB)
          DEALLOCATE(RIDDNSL)
          DEALLOCATE(RIDDNIS)
          DEALLOCATE(RINUIO )
          DEALLOCATE(RIKAPPA)
          DEALLOCATE(RIMOLW )
          DEALLOCATE(RIEPSM )
        ENDIF
        IF ( ISINT > 0 ) THEN
          DEALLOCATE(RIN0   )
          DEALLOCATE(RIPHI0 )
          DEALLOCATE(RIPSI  )
          DEALLOCATE(RIMOM0 )
          DEALLOCATE(RIMOM2 )
          DEALLOCATE(RIMOM3 )
          DEALLOCATE(RIMOM4 )
          DEALLOCATE(RIMOM5 )
          DEALLOCATE(RIMOM6 )
          DEALLOCATE(RIMOM7 )
          DEALLOCATE(RIMOM8 )
        ENDIF
        IF ( ISAINT > 0 ) THEN
          DEALLOCATE(VIN0   )
          DEALLOCATE(VIPSI  )
          DEALLOCATE(VIPHI0 )
          DEALLOCATE(VIPHIS0)
          DEALLOCATE(VIDPHI0)
        ENDIF
        IF ( KEXT > 0 ) THEN
          DEALLOCATE(TECDNCP)
          DEALLOCATE(TECDNCL)
        ENDIF
        IF ( KINT > 0 ) THEN
          DEALLOCATE(TICDNCP)
          DEALLOCATE(TICDNCL)
        ENDIF
        DEALLOCATE(SVTP)
        DEALLOCATE(SVTL)
        DEALLOCATE(IACTI )
        DEALLOCATE(IACTO )
        DEALLOCATE(IACTIO)
      ENDIF
!
      END SUBROUTINE CCNACT
