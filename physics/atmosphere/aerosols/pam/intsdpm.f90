      SUBROUTINE INTSDPM(FMASS,FMASSD,PNUM,PMAS,PN0,PHI0,PSI, &
                         PHIS0,DPHI0,DRYDN,WETRC,DRYRC,RADC, &
                         RHOA,ISN,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PM2.5 AND PM10 CONCENTRATIONS.
!
!     HISTORY:
!     --------
!     * MAR 5/2018 - K.VONSALZEN   NEW
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) ::  PHIS0,DPHI0,PNUM, &
                                                      PMAS,PN0,PSI, &
                                                      PHI0,WETRC,DRYDN
      REAL, INTENT(IN), DIMENSION(ISEC)           :: DRYRC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA)      :: RHOA
      REAL, INTENT(IN), DIMENSION(ISN)            :: RADC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISN) :: FMASS,FMASSD
!
      REAL, DIMENSION(ILGA,LEVA,ISN) :: FMOM3,FMOM3D
      REAL, DIMENSION(:,:,:), ALLOCATABLE :: TPHIS0,TPHI0, &
                                                      WETDN,ALFGR
      REAL, DIMENSION(ISN) :: TPHIC
!
!-----------------------------------------------------------------------
!     * INTEGRATE PLA SIZE DISTRIBUTION OVER GIVEN SUB-SECTIONS.
!
      TPHIC=LOG(RADC/R0)
      RMOM3=3.
      FMASS=0.
      FMASSD=0.
      FMOM3=0.
      FMOM3D=0.
      IF ( ISEC > 0 ) THEN
!        ALLOCATE(TPHIS0(ILGA,LEVA,ISEC))
!        ALLOCATE(TPHI0(ILGA,LEVA,ISEC))
!        ALLOCATE(WETDN(ILGA,LEVA,ISEC))
        ALLOCATE(ALFGR(ILGA,LEVA,ISEC))
!
!       * SECTION BOUNDARIES AND DENSITY FOR WET SIZE DISTRIBUTION.
!
        DO IS=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
!          TPHIS0(IL,L,IS)=PHIS0(IL,L,IS)
!          TPHI0(IL,L,IS)=PHI0(IL,L,IS)
!          WETDN(IL,L,IS)=DRYDN(IL,L,IS)
          ALFGR(IL,L,IS)=0.
          IF ( PNUM(IL,L,IS) > YTINY .AND. PMAS(IL,L,IS) > YTINY &
                             .AND. ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
            FGR=WETRC(IL,L,IS)/DRYRC(IS)
            ALFGR(IL,L,IS)=LOG(FGR)
!            TPHIS0(IL,L,IS)=TPHIS0(IL,L,IS)+ALFGR(IL,L,IS)
!            TPHI0(IL,L,IS)=TPHI0(IL,L,IS)+ALFGR(IL,L,IS)
!            FGR3I=1./FGR(IL,L,IS)**3
!            WETDN(IL,L,IS)=DNH2O*(1.-FGR3I)+DRYDN(IL,L,IS)*FGR3I
          ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!       * INTEGRATE DRY MASS SIZE DISTRIBUTION BETWEEN SMALLEST
!       * AVAILABLE PARTICLE RADIUS AND DRY CUTOFF SIZE.
!
        DO ISD=1,ISN
        DO IS=1,ISEC
        DO L=1,LEVA
        DO IL=1,ILGA
          FPHIS=PHIS0(IL,L,IS)+ALFGR(IL,L,IS)
          FPHISD=PHIS0(IL,L,IS)
!
!         * THIS ASSUMES THAT THE AIR IN THE INSTRUMENT INLET IS 
!         * NOT PREHEATED AND PARTICLES ARE COLLECTED AT AMBIENT
!         * RELATIVE HUMDITY. FILTER SAMPLES ARE SUBJECT TO
!         * TO GRAVITMETRIC ANALYSIS IN A DRY LABORATORY.
!
          IF ( TPHIC(ISD) >= FPHIS ) THEN
            TCUTD=TPHIC(ISD)-ALFGR(IL,L,IS)
            FPHIED=MIN(PHIS0(IL,L,IS)+DPHI0(IL,L,IS), &
                       TCUTD)
            FDPHID=MAX(FPHIED-FPHISD,0.)
            IF ( ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
              FMOM3(IL,L,ISD)=FMOM3(IL,L,ISD) &
                      +YCNST*DRYDN(IL,L,IS)*PN0(IL,L,IS) &
                      *SDINTB(PHI0(IL,L,IS),PSI(IL,L,IS), &
                              RMOM3,FPHISD,FDPHID)
            ENDIF
          ENDIF
!
!         * THIS ASSUMES THAT THE AIR IN THE INSTRUMENT INLET IS 
!         * PREHEATED SO THAT ONLY DRY PARTICLES ARE SAMPLED.
!
          IF ( TPHIC(ISD) >= FPHISD ) THEN
            FPHIED=MIN(PHIS0(IL,L,IS)+DPHI0(IL,L,IS), &
                       TPHIC(ISD))
            FDPHID=MAX(FPHIED-FPHISD,0.)
            IF ( ABS(PSI(IL,L,IS)-YNA) > YTINY ) THEN
              FMOM3D(IL,L,ISD)=FMOM3D(IL,L,ISD) &
                      +YCNST*DRYDN(IL,L,IS)*PN0(IL,L,IS) &
                      *SDINTB(PHI0(IL,L,IS),PSI(IL,L,IS), &
                              RMOM3,FPHISD,FDPHID)
!
!              * TOTAL WET MASS, INCLUDING AEROSOL WATER
!
!              FPHIE=MIN(TPHIS0(IL,L,IS)+DPHI0(IL,L,IS),TPHIC(ISD))
!              FDPHI=MAX(FPHIE-FPHIS,0.)
!              FMOM3(IL,L,ISD)=FMOM3(IL,L,ISD) &
!                      +YCNST*WETDN(IL,L,IS)*PN0(IL,L,IS) &
!                      *SDINTB(TPHI0(IL,L,IS),PSI(IL,L,IS), &
!                              RMOM3,FPHIS,FDPHI)
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
        ENDDO
!        DEALLOCATE(TPHIS0)
!        DEALLOCATE(TPHI0)
!        DEALLOCATE(WETDN)
        DEALLOCATE(ALFGR)
      ENDIF
!
!     * CONVERT FROM MASS MIXING RATIO TO CONCENTRATION.
!
      DO IS=1,ISN
        FMASS(:,:,IS)=FMOM3(:,:,IS)*RHOA
        FMASSD(:,:,IS)=FMOM3D(:,:,IS)*RHOA
      ENDDO
!
      END SUBROUTINE INTSDPM
