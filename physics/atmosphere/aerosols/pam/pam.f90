      SUBROUTINE PAM(TRAC,TRRAT, &
                     ZHA,ZFA,PHA,PFA,DPA,TA,RVA,QTNA,HMNA,RHA,RHTA,WCTA, &
                     SPMREP,SZMLWC,SPFSNOW,SPFRAIN,SZCLF,SCLRFR,SCLRFS, &
                     SPFEVAP,SPFSUBL,SGPR,OGPR,DOCEM1S,DOCEM2S,DOCEM3S, &
                     DOCEM4S,DBCEM1S,DBCEM2S,DBCEM3S,DBCEM4S,DSUEM1S, &
                     DSUEM2S,DSUEM3S,DSUEM4S, &
                     XO3,XNA,XAM,XNH3,FCANA,PBLTS,SMFRDU,FCSA,FGSA,FCA, &
                     FGA,ZSPDDD,ZSPDSS,ZSPDDU,CDMLA,CDMNLA,USTARD, &
                     FLNDDU,FOCNDU,SICNDU,GTRDU,FNDU, &
                     SPOT,ST02,ST03,ST04,ST06,ST13,ST14,ST15, &
                     ST16,ST17,SUZ0,SLAI,ATAUS,ZFSA,PFSA, &
                     OEDNIO,OERCIO,OIDNIO,OIRCIO,SVVBIO,PSVVIO,SVMBIO, &
                     SVCBIO,PNVBIO,PNMBIO,PNCBIO,PSVBIO,PSMBIO,PSCBIO, &
                     QNVBIO,QNMBIO,QNCBIO,QSVBIO,QSMBIO,QSCBIO,QGVBIO, &
                     QGMBIO,QGCBIO,QDVBIO,QDMBIO,QDCBIO,ONVBIO,ONMBIO, &
                     ONCBIO,OSVBIO,OSMBIO,OSCBIO,OGVBIO,OGMBIO,OGCBIO, &
                     DEVBIO,PDEVIO,DEMBIO,DECBIO,DIVBIO,PDIVIO,DIMBIO, &
                     DICBIO,REVBIO,PREVIO,REMBIO,RECBIO,RIVBIO,PRIVIO, &
                     RIMBIO,RICBIO, &
                     CDNC,BCIC,ACAS,ACOA,ACBC,ACSS,ACMD, &
                     CORN,CORM,RSN1,RSM1,RSN2,RSM2,RSN3,RSM3,SNCN,SSUN, &
                     SCND,SGSP,CCN,CCNE,CC02,CC04,CC08,CC16,CNT,CN20, &
                     CN50,CN100,CN200,CNTW,CN20W,CN50W,CN100W,CN200W, &
                     RCRI,SUPS,VRN1,VRM1,VRN2,VRM2,VRN3,VRM3,VNCN,VASN, &
                     VCND,VGSP,DEFX,DEFN,VOAE,VBCE,VASE,VMDE,VSSE,VOAW, &
                     VBCW,VASW,VMDW,VSSW,VOAD,VBCD,VASD,VMDD,VSSD,VOAG, &
                     VBCG,VASG,VMDG,VSSG,VASI,VAS1,VAS2,VAS3,HENR,O3FRC, &
                     H2O2FRC,VCCN,VCNE,DMAC,DMCO,DNAC,DNCO,DUWD,DUST, &
                     DUTH,FALL,FA10,FA2,FA1,USMK,DEFA,DEFC,KAVGS,DT, &
                     SICN_CRT,CO2,ICANA,LEVA,ILGA,KNT,ISVDUST)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PLA AEROSOL MODEL (PAM).
!
!     HISTORY:
!     --------
!     * DEC  6/2017 - K.VONSALZEN   CONDENSATION OF SULPHURIC ACID
!                                   ONTO SEA SALT ADDED
!     * SEP 26/2017 - K.VONSALZEN   UPDATED FOR CANAM5
!     * JUL  3/2015 - K.VONSALZEN   MOVE DIAGNOSTIC CALCULATIONS TO
!     *                             PAMDI
!     * APR 17/2015 - K.VONSALZEN   ADDITION OF DIAGNOSTIC FIELDS
!     *                             FOR CN NUMBER CONCENTRATION
!     * JUN 18/2014 - K.VONSALZEN   VERSION 2 OF PAM FOR CANAM4.3:
!     *                             CHANGES RELATED TO LAND-SURFACE,
!     *                             CLOUD MICROPHYSICS, AND DIAGNOSTICS
!     * JUN 10/2014 - K.VONSALZEN   REVISED AVERAGING PROCEDURE FOR CLOUD
!     *                             RESULTS AND SUBRGRID SCALE TRACERS
!     * MAY 26/2010 - K.VONSALZEN   CHANGE CALL TO NM2PARX
!     * FEB 11/2010 - K.VONSALZEN   NEW, BASED ON GCM (VDO154+PLD030)
!
!-----------------------------------------------------------------------
!
      USE COMPAR
      USE SDPARM
      USE SDCODE
      USE SDEMI
      USE SCPARM
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
!     * LOCAL PARAMETERS.
!
      REAL, PARAMETER :: SVREF=0.002
      REAL, PARAMETER :: SVMIN=0.00001
      REAL, PARAMETER :: DNREF=50.E+06
      REAL, PARAMETER :: RCREF=0.1E-06
      REAL, PARAMETER :: ZCTHR=0.99
      INTEGER, PARAMETER :: ICFRQ=12     ! NUMBER OF TIME STEPS BETWEEN CALLS
                                         ! OF ACTIVATION CODE
!
!     * PAM IN/OUTPUT VARIABLES AND PARAMETERS
!
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD)      :: &
                                                                 QNCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD) :: &
                                                                 QSCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD)      :: &
                                                                 ONCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD) :: &
                                                                 OSCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD)      :: &
                                                                 PNCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD) :: &
                                                                 PSCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)             :: &
                                                                 QGCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)             :: &
                                                                 QDCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)             :: &
                                                                 OGCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)             :: &
                                                                 SVCBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT,NRMFLD)        :: &
                                                                 DECBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)             :: &
                                                                 DICBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT,NRMFLD)        :: &
                                                                 RECBIO
      INTEGER, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)             :: &
                                                                 RICBIO
!
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NTRACT)                :: &
                                                             TRAC,TRRAT
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT)                  :: &
                                                          OEDNIO,OERCIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA)                       :: &
                                            OIDNIO,OIRCIO,SVVBIO,PSVVIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)                :: &
                                                                 SVMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT)                :: &
                                                                 PNVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD)         :: &
                                                                 PNMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT)           :: &
                                                                 PSVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD)    :: &
                                                                 PSMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT)                :: &
                                                                 QNVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD)         :: &
                                                                 QNMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT)           :: &
                                                                 QSVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD)    :: &
                                                                 QSMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA)                       :: &
                                                                 QGVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)                :: &
                                                                 QGMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA)                       :: &
                                                                 QDVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)                :: &
                                                                 QDMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT)                :: &
                                                                 ONVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,NRMFLD)         :: &
                                                                 ONMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT)           :: &
                                                                 OSVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISAINT,KINT,NRMFLD)    :: &
                                                                 OSMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA)                       :: &
                                                                 OGVBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)                :: &
                                                                 OGMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT)                  :: &
                                                          DEVBIO,PDEVIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT,NRMFLD)           :: &
                                                                 DEMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA)                       :: &
                                                          DIVBIO,PDIVIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)                :: &
                                                                 DIMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT)                  :: &
                                                          REVBIO,PREVIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,KEXT,NRMFLD)           :: &
                                                                 REMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA)                       :: &
                                                          RIVBIO,PRIVIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,NRMFLD)                :: &
                                                                 RIMBIO
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: SGPR,OGPR
!
!     * PAM INPUT VARIABLES AND PARAMETERS.
!
      INTEGER, INTENT(IN) :: ICANA,LEVA,ILGA,KNT
      REAL, INTENT(IN) :: DT,SICN_CRT,CO2
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: ZHA,ZFA,PHA,PFA,DPA,TA, &
                                                RVA,QTNA,HMNA,RHA,RHTA, &
                                                WCTA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: SPMREP,SZMLWC,SPFSNOW, &
                                                SPFRAIN,SZCLF,SCLRFR, &
                                                SCLRFS,SPFEVAP,SPFSUBL
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: DOCEM1S,DOCEM2S,DOCEM3S, &
                                                DOCEM4S,DBCEM1S,DBCEM2S, &
                                                DBCEM3S,DBCEM4S,DSUEM1S, &
                                                DSUEM2S,DSUEM3S,DSUEM4S
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: XO3,XNA,XAM,XNH3
      REAL, INTENT(IN), DIMENSION(ILGA,ICANA+1) :: FCANA
      REAL, INTENT(IN), DIMENSION(ILGA)     ::  SMFRDU,FCSA,FGSA,FCA, &
                                                FGA,ZSPDDD,ZSPDSS, &
                                                ZSPDDU,CDMLA,CDMNLA, &
                                                USTARD
      REAL, INTENT(IN), DIMENSION(ILGA)     ::  FLNDDU,FOCNDU, &
                                                SICNDU,GTRDU,FNDU
      REAL, INTENT(IN), DIMENSION(ILGA)      :: SUZ0,SLAI,SPOT,ST02, &
                                                ST03,ST04,ST06,ST13, &
                                                ST14,ST15,ST16,ST17
      REAL, INTENT(IN), DIMENSION(ILGA)      :: ATAUS,ZFSA,PFSA
      INTEGER, INTENT(IN), DIMENSION(ILGA)   :: PBLTS
!
!     * PAM OUTPUT VARIABLES.
!
      LOGICAL, INTENT(OUT) :: KAVGS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA)   :: CORN,CORM,RSN1,RSM1, &
                                                   RSN2,RSM2,RSN3,RSM3, &
                                                   CDNC,BCIC
      REAL, INTENT(OUT), DIMENSION(ILGA)        :: VRN1,VRM1,VRN2,VRM2, &
                                                   VRN3,VRM3
      REAL, INTENT(OUT), DIMENSION(ILGA)        :: VNCN,VASN,VCND,VGSP
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA)   :: SNCN,SSUN,SCND,SGSP, &
                                                   CCN,CCNE,CC02,CC04, &
                                                   CC08,CC16,RCRI,SUPS, &
                                                   HENR,O3FRC,H2O2FRC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA)   :: CNT,CN20,CN50,CN100, &
                                                   CN200,CNTW,CN20W, &
                                                   CN50W,CN100W,CN200W
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDIAG) :: DEFX,DEFN
      REAL, INTENT(OUT), DIMENSION(ILGA)        :: VOAE,VBCE,VASE,VMDE, &
                                                   VSSE,VOAW,VBCW,VASW, &
                                                   VMDW,VSSW,VOAD,VBCD, &
                                                   VASD,VMDD,VSSD,VOAG, &
                                                   VBCG,VASG,VMDG,VSSG, &
                                                   VCCN,VCNE,VASI,VAS1, &
                                                   VAS2,VAS3
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDUST) :: DMAC,DMCO,DNAC,DNCO
      REAL, INTENT(OUT), DIMENSION(ILGA) ::        DUWD,DUST,DUTH,FALL, &
                                                   FA10,FA2,FA1,USMK, &
                                                   DEFA,DEFC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) ::   ACAS,ACOA,ACBC,ACSS, &
                                                   ACMD
!
!     * GENERAL PLA VARIABLES.
!
      REAL, DIMENSION(ILGA,LEVA)          :: DGDT,H2SO4,RESN,RESM,RHOA, &
                                             SV,PIHENR,PEHENR,VITRM, &
                                             SVB,SVF,SO2,H2O2,DSIVDT, &
                                             DHPDT,SO2FRC,HPFRC,SCF, &
                                             OGAS,O3FRT,RHAM,DGDTX,SOAP, &
                                             OGPRT,H2O2FRT
      REAL, DIMENSION(ILGA,LEVA)          :: DERSN,DERSM,DIRSN,DIRSMN, &
                                             DIRSMS,DENNCL,DECNCL, &
                                             DINNCL,DICNCL,DECND,DICND, &
                                             DSICP,DWR1N,DWR2N,DWR3N, &
                                             DWR1M,DWR2M,DWR3M,DWS1N, &
                                             DWS2N,DWS3N,DWS1M,DWS2M, &
                                             DWS3M,DNCLDT,DECNDX
      REAL, DIMENSION(ILGA,ISDUST*2)      :: DIAGMAS,DIAGNUM
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: DESIZE,DEFLUX,DEFNUM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEDPHI0,PEPHIS0,PEDDN, &
                                             PEWETRC,PENFRC, &
                                             PEMFRC,PESTICK, &
                                             PIDPHI0,PIPHIS0,PIDDN, &
                                             PIWETRC,PINFRC, &
                                             PISUM,PISTICK
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEN0,PEPHI0,PEPSI,PENUM, &
                                             PEMAS,PEDNDT,PEDMDT, &
                                             PIN0,PIPHI0,PIPSI,PINUM, &
                                             PIMAS,PIDNDT,PIDMDT
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PIFRC,PIMFRC,PIDFDT
      LOGICAL :: KAVGO
!
!     * DRY DEPOSITION.
!
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEFLXN,PEFLXM
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: PEVDRN,PEVDRM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PIFLXN,PIFLXM
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: PIVDRN,PIVDRM
!
!     * CCN-RELATED FIELDS.
!
      REAL, DIMENSION(ILGA,LEVA) :: TMPM
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PECDNC,PERCI
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: PICDNC,PIRCI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: CECDNC,CERCI
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: CICDNC,CIRCI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: SECDNC,SERCI
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: SICDNC,SIRCI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: CEDDNSL,CEDDNIS,CEWETRB, &
                                             CENUIO,CEEPSM,CEMOLW, &
                                             CEDDNC,CEKAPPA,CEPHIS0, &
                                             CEDPHI0
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: CIDDNSL,CIDDNIS,CIWETRB, &
                                             CINUIO,CIEPSM,CIMOLW, &
                                             CIDDNC,CIKAPPA,CIPHIS0, &
                                             CIDPHI0
!
!     * FOR EXTRA OUTPUT.
!
      REAL, DIMENSION(ILGA)                    :: VOAT,VBCT,VAST,VMDT, &
                                                  VSST
!
!-----------------------------------------------------------------------
!     * SWITCHES.
!
      IF ( KAVG .AND. KEXTSO4==0 .AND. KINTSO4>0 ) THEN
        KAVGS=.TRUE.
      ELSE
        KAVGS=.FALSE.
      ENDIF
      IF ( KAVG .AND. KEXTOC==0 .AND. KINTOC>0 ) THEN
        KAVGO=.TRUE.
      ELSE
        KAVGO=.FALSE.
      ENDIF
!
!     * ALLOCATE MEMORY FOR PLA SECTION SIZE PARAMETER ARRAYS.
!
      IF ( ISAEXT > 0 ) THEN
        ALLOCATE(PEPHIS0(ILGA,LEVA,ISAEXT))
        ALLOCATE(PEDPHI0(ILGA,LEVA,ISAEXT))
        ALLOCATE(PEDDN  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PESTICK(ILGA,LEVA,ISAEXT))
        ALLOCATE(PEN0   (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEPHI0 (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEPSI  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEWETRC(ILGA,LEVA,ISAEXT))
        ALLOCATE(PENUM  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEMAS  (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEDNDT (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEDMDT (ILGA,LEVA,ISAEXT))
        ALLOCATE(PENFRC (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEMFRC (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEFLXN (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEFLXM (ILGA,LEVA,ISAEXT))
        ALLOCATE(PEVDRN (ILGA,ISAEXT))
        ALLOCATE(PEVDRM (ILGA,ISAEXT))
      ENDIF
      IF ( ISAINT > 0 ) THEN
        ALLOCATE(PIPHIS0(ILGA,LEVA,ISAINT))
        ALLOCATE(PIDPHI0(ILGA,LEVA,ISAINT))
        ALLOCATE(PIDDN  (ILGA,LEVA,ISAINT))
        ALLOCATE(PISTICK(ILGA,LEVA,ISAINT))
        ALLOCATE(PIN0   (ILGA,LEVA,ISAINT))
        ALLOCATE(PIPHI0 (ILGA,LEVA,ISAINT))
        ALLOCATE(PIPSI  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIWETRC(ILGA,LEVA,ISAINT))
        ALLOCATE(PINUM  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIMAS  (ILGA,LEVA,ISAINT))
        ALLOCATE(PIDNDT (ILGA,LEVA,ISAINT))
        ALLOCATE(PIDMDT (ILGA,LEVA,ISAINT))
        ALLOCATE(PINFRC (ILGA,LEVA,ISAINT))
        ALLOCATE(PIFRC  (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIDFDT (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIMFRC (ILGA,LEVA,ISAINT,KINT))
        ALLOCATE(PIFLXN (ILGA,LEVA,ISAINT))
        ALLOCATE(PIFLXM (ILGA,LEVA,ISAINT))
        ALLOCATE(PIVDRN (ILGA,ISAINT))
        ALLOCATE(PIVDRM (ILGA,ISAINT))
      ENDIF
!
!     * CCN-RELATED FIELDS.
!
      IF ( KEXT > 0 ) THEN
        ALLOCATE(PECDNC(ILGA,LEVA,KEXT))
        ALLOCATE(PERCI (ILGA,LEVA,KEXT))
        ALLOCATE(CECDNC(ILGA,LEVA,KEXT))
        ALLOCATE(CERCI (ILGA,LEVA,KEXT))
        ALLOCATE(SECDNC(ILGA,LEVA,KEXT))
        ALLOCATE(SERCI (ILGA,LEVA,KEXT))
      ENDIF
      IF ( KINT > 0 ) THEN
        ALLOCATE(PICDNC(ILGA,LEVA))
        ALLOCATE(PIRCI (ILGA,LEVA))
        ALLOCATE(CICDNC(ILGA,LEVA))
        ALLOCATE(CIRCI (ILGA,LEVA))
        ALLOCATE(SICDNC(ILGA,LEVA))
        ALLOCATE(SIRCI (ILGA,LEVA))
      ENDIF
      IF ( ISEXT > 0 ) THEN
        ALLOCATE(CEPHIS0(ILGA,LEVA,ISEXT))
        ALLOCATE(CEDPHI0(ILGA,LEVA,ISEXT))
      ENDIF
      IF ( ISEXTB > 0 ) THEN
        ALLOCATE(CEWETRB(ILGA,LEVA,ISEXTB))
        ALLOCATE(CEDDNC (ILGA,LEVA,ISEXTB))
        ALLOCATE(CEDDNSL(ILGA,LEVA,ISEXTB))
        ALLOCATE(CEDDNIS(ILGA,LEVA,ISEXTB))
        ALLOCATE(CENUIO (ILGA,LEVA,ISEXTB))
        ALLOCATE(CEKAPPA(ILGA,LEVA,ISEXTB))
        ALLOCATE(CEMOLW (ILGA,LEVA,ISEXTB))
        ALLOCATE(CEEPSM (ILGA,LEVA,ISEXTB))
      ENDIF
      IF ( ISINT > 0 ) THEN
        ALLOCATE(CIPHIS0(ILGA,LEVA,ISINT))
        ALLOCATE(CIDPHI0(ILGA,LEVA,ISINT))
      ENDIF
      IF ( ISINTB > 0 ) THEN
        ALLOCATE(CIWETRB(ILGA,LEVA,ISINTB))
        ALLOCATE(CIDDNC (ILGA,LEVA,ISINTB))
        ALLOCATE(CIDDNSL(ILGA,LEVA,ISINTB))
        ALLOCATE(CIDDNIS(ILGA,LEVA,ISINTB))
        ALLOCATE(CINUIO (ILGA,LEVA,ISINTB))
        ALLOCATE(CIKAPPA(ILGA,LEVA,ISINTB))
        ALLOCATE(CIMOLW (ILGA,LEVA,ISINTB))
        ALLOCATE(CIEPSM (ILGA,LEVA,ISINTB))
      ENDIF
!
!-----------------------------------------------------------------------
!     * AEROSOL SIZE INFORMATION.
!
      DO IS=1,ISAEXT
        PEDPHI0(:,:,IS)=PEDPHIS(IS)
        PEPHIS0(:,:,IS)=PEPHISS(IS)
      ENDDO
      DO IS=1,ISAINT
        PIDPHI0(:,:,IS)=PIDPHIS(IS)
        PIPHIS0(:,:,IS)=PIPHISS(IS)
      ENDDO
!
!     * DRY PARTICLE DENSITIES FOR EXTERNALLY MIXED AEROSOL TYPES.
!
      DO IS=1,ISAEXT
        KX=SEXTF%ISAER(IS)%ITYP
        PEDDN  (:,:,IS)=AEXTF%TP(KX)%DENS
        PESTICK(:,:,IS)=AEXTF%TP(KX)%STICK
      ENDDO
!
!     * INITIALIZE DRY PARTICLE DENSITIES FOR INTERNALLY MIXED
!     * AEROSOL TYPES. THE DENSITIES ARE LATER CALCULATED AS A
!     * FUNCTION OF AEROSOL MASS.
!
      DO IS=1,ISAINT
        PIDDN  (:,:,IS)=YNA
        PISTICK(:,:,IS)=AINTF%STICK
      ENDDO
!
!-----------------------------------------------------------------------
!     * AEROSOL SIZE INFORMATION FOR CCN-RELATED CALCULATIONS.
!
      DO IS=1,ISEXT
      DO L=1,LEVA
      DO IL=1,ILGA
        CEPHIS0(IL,L,IS)=CEPHI(IS)%VL
        CEDPHI0(IL,L,IS)=CEPHI(IS)%VR-CEPHI(IS)%VL
      ENDDO
      ENDDO
      ENDDO
      DO IS=1,ISINT
      DO L=1,LEVA
      DO IL=1,ILGA
        CIPHIS0(IL,L,IS)=CIPHI(IS)%VL
        CIDPHI0(IL,L,IS)=CIPHI(IS)%VR-CIPHI(IS)%VL
      ENDDO
      ENDDO
      ENDDO
!
!     * CALCULATE DENSITIES FOR EXTERNALLY MIXED AEROSOL TYPES AT
!     * CENTRES OF SUB-SECTIONS.
!
      IF ( ISEXT > 0 ) THEN
        ISC=0
        DO IS=1,ISWEXT
          KX=SEXTF%ISWET(IS)%ITYP
          DO IST=1,ISESC
            ISC=ISC+1
            CEDDNC(:,:,ISC)=AEXTF%TP(KX)%DENS
          ENDDO
        ENDDO
      ENDIF
!
!     * CALCULATE DENSITIES, NUMBER OF IONS IN SOLUTION AND THEIR
!     * MOLECULAR WEIGHT FOR DRY SIZE DISTRIBUTION FOR EXTERNALLY
!     * MIXED AEROSOL TYPES AT BOUNDARIES OF SUB-SECTIONS.
!
      IF ( ISEXTB > 0 ) THEN
        ISC=0
        DO IS=1,ISWEXT-1
          KX=SEXTF%ISWET(IS)%ITYP
          KXP=SEXTF%ISWET(IS+1)%ITYP
          DO IST=1,ISESC
            ISC=ISC+1
            CEDDNSL(:,:,ISC)=AEXTF%TP(KX)%DENS
            CENUIO (:,:,ISC)=AEXTF%TP(KX)%NUIO
            CEKAPPA(:,:,ISC)=AEXTF%TP(KX)%KAPPA
            CEMOLW (:,:,ISC)=AEXTF%TP(KX)%MOLW
          ENDDO
          IF ( KXP /= KX ) THEN
            ISC=ISC+1
            CEDDNSL(:,:,ISC)=AEXTF%TP(KX)%DENS
            CENUIO (:,:,ISC)=AEXTF%TP(KX)%NUIO
            CEKAPPA(:,:,ISC)=AEXTF%TP(KX)%KAPPA
            CEMOLW (:,:,ISC)=AEXTF%TP(KX)%MOLW
          ENDIF
        ENDDO
        KX=SEXTF%ISWET(ISWEXT)%ITYP
        DO IST=1,ISESC
          ISC=ISC+1
          CEDDNSL(:,:,ISC)=AEXTF%TP(KX)%DENS
          CENUIO (:,:,ISC)=AEXTF%TP(KX)%NUIO
          CEKAPPA(:,:,ISC)=AEXTF%TP(KX)%KAPPA
          CEMOLW (:,:,ISC)=AEXTF%TP(KX)%MOLW
        ENDDO
        ISC=ISC+1
        CEDDNSL(:,:,ISC)=AEXTF%TP(KX)%DENS
        CENUIO (:,:,ISC)=AEXTF%TP(KX)%NUIO
        CEKAPPA(:,:,ISC)=AEXTF%TP(KX)%KAPPA
        CEMOLW (:,:,ISC)=AEXTF%TP(KX)%MOLW
      ENDIF
!
!     * FRACTION OF SOLUBLE MASS AND DENSITY OF INSOLUBLE MATERIAL
!     * FOR EXTERNALLY MIXED AEROSOL TYPES AT BOUNDARIES OF
!     * SUB-SECTIONS.
!
      IF ( ISAEXT > 0 ) THEN
        CEEPSM=1.
        CEDDNIS=YNA
      ENDIF
!
!-----------------------------------------------------------------------
!     * PLA DIAGNOSTIC FIELDS.
!
      IF ( KXTRA2 ) THEN
        CORN=0.
        CORM=0.
        RSN1=0.
        RSM1=0.
        VRN1=0.
        VRM1=0.
        RSN2=0.
        RSM2=0.
        VRN2=0.
        VRM2=0.
      ENDIF
!
!     * METEOROLOGICAL VARIABLES FOR PLA CALCULATIONS.
!
      SV =MIN(RHTA-1.,-0.01)
      RHOA=PHA/(RGAS*TA)
      VITRM=DPA/GRAV
!
!     * EXTRACT PLA AEROSOL NUMBER AND MASS CONCENTRATIONS FROM BASIC
!     * TRACER ARRAYS IN THE GCM.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=TRAC(:,:,INSEX:INFEX)
        PEMAS=TRAC(:,:,IMSEX:IMFEX)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=TRAC(:,:,INSIN:INFIN)
      ENDIF
!
!     * TOTAL MASS AND MASS FRACTIONS FOR INTERNALLY MIXED TYPES
!     * OF AEROSOL.
!
      IF ( ISAINT > 0 ) PIMAS=0.
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        PIMAS(:,:,IS)=PIMAS(:,:,IS)+TRAC(:,:,IMLIN)
      ENDDO
      ENDDO
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        KX=SINTF%ISAER(IS)%ITYP(IST)
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        DO L=1,LEVA
        DO IL=1,ILGA
          XRTMP=TRAC(IL,L,IMLIN)
          IF ( PIMAS(IL,L,IS) > MAX(XRTMP/YLARGE,YTINY) ) THEN
            IF ( XRTMP > YTINY ) THEN
              PIFRC(IL,L,IS,KX)=MIN(MAX(XRTMP/PIMAS(IL,L,IS),0.),1.)
            ELSE
              PIFRC(IL,L,IS,KX)=0.
            ENDIF
          ELSE
            PIFRC(IL,L,IS,KX)=1./REAL(SINTF%ISAER(IS)%ITYPT)
          ENDIF
        ENDDO
        ENDDO
      ENDDO
      ENDDO
!
!     * EXTRACT FRACTION OF CLOUDY-SKY/CLEAR-SKY FOR AEROSOL
!     * CONCENTRATIONS.
!
      IF ( ISAEXT > 0 ) THEN
        PENFRC=TRRAT(:,:,INSEX:INFEX)
        PEMFRC=TRRAT(:,:,IMSEX:IMFEX)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINFRC=TRRAT(:,:,INSIN:INFIN)
      ENDIF
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        KX=SINTF%ISAER(IS)%ITYP(IST)
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        DO L=1,LEVA
        DO IL=1,ILGA
          PIMFRC(IL,L,IS,KX)=PIFRC(IL,L,IS,KX)*TRRAT(IL,L,IMLIN)
        ENDDO
        ENDDO
      ENDDO
      ENDDO
!
!     * EXTRACT CONCENTRATIONS FOR GASES.
!
      SO2FRC=TRRAT(:,:,JSO2)
      HPFRC=TRRAT(:,:,JHPO)
      H2SO4=TRAC(:,:,JGS6)
      OGAS=TRAC(:,:,JGSP)
      SOAP=OGAS+OGPR*DT
      SO2=TRAC(:,:,JSO2)
      H2O2=TRAC(:,:,JHPO)
!
!-----------------------------------------------------------------------
!     * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
      IF ( KXTRA2 ) THEN
        CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                      PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                      PINUM,PIMAS,PIFRC, &
                      CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                      RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
      ELSE
        CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                     PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                     PINUM,PIMAS,PIFRC,ILGA,LEVA)
      ENDIF
!
!-----------------------------------------------------------------------
!     * WET PARTICLE SIZES.
!
      CALL SDAPROP (PEWETRC,PIWETRC,PEN0,PEPHI0,PEPSI,PEDDN, &
                    PIN0,PIPHI0,PIPSI,PIFRC,PIDDN,TA,RHTA, &
                    ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * AEROSOL MICROPHYSICAL PARAMETERS FOR CCN CALCULATIONS.
!
      CALL SCMICP(CIDDNC,CIDDNSL,CIDDNIS,CINUIO,CIKAPPA,CIEPSM, &
                  CIMOLW,CIWETRB,CEWETRB,PIFRC,PEWETRC,PIWETRC, &
                  ILGA,LEVA)
!
!-----------------------------------------------------------------------
!     * DIAGNOSE CCN AND CDN CONCENTRATIONS.
!
      SVB=YNA
      OEDNIO=YNA
      OERCIO=YNA
      OIDNIO=YNA
      OIRCIO=YNA
      IF ( MOD(KNT,ICFRQ) == 0 ) THEN
!
!       * DROPLET GROWTH CALCULATIONS TO OBTAIN SUPERSATURATION.
!
        CALL CCNACT(SVB,SV,CECDNC,CICDNC,QTNA,HMNA,TA, &
                    ZHA,ZFA,PHA,PFA,RHOA,WCTA,PEN0,PEPHI0,PEPSI, &
                    PEPHIS0,PEDPHI0,PIN0,PIPHI0,PIPSI,PIPHIS0, &
                    PIDPHI0,CEWETRB,CEDDNSL,CEDDNIS,CENUIO,CEKAPPA, &
                    CEMOLW,CEPHIS0,CEDPHI0,CIWETRB,CIDDNSL,CIDDNIS, &
                    CINUIO,CIKAPPA,CIMOLW,CIEPSM,CIPHIS0,CIDPHI0, &
                    ZFSA,PFSA,SZCLF,SZMLWC,ZCTHR,ILGA,LEVA)
!
!       * CLOUD CONDENSATION NUCLEI CONCENTRATIONS AND CRITICAL DROPLET
!       * RADIUS AT ACTUAL MAX. SUPERSATURATION IN THE CLOUD LAYERS.
!
        CALL CCNC(CECDNC,CERCI,CICDNC,CIRCI,CEWETRB,CEDDNSL, &
                  CEDDNIS,CENUIO,CEMOLW,CEKAPPA,CIWETRB,CIDDNSL, &
                  CIDDNIS,CINUIO,CIMOLW,CIKAPPA,CIEPSM,PEN0, &
                  PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                  PIPSI,PIPHIS0,PIDPHI0,SZCLF,SZMLWC,SVB,TA,ZCTHR, &
                  ILGA,LEVA)
!
!       * SAVE INSTANTANEOUS RESULTS FOR CCN CONCENTRATION
!       * AND CRITICAL RADIUS.
!
        IF ( KEXT > 0 ) THEN
          DO KX=1,KEXT
            OEDNIO(:,:,KX)=CECDNC(:,:,KX)
            OERCIO(:,:,KX)=CERCI (:,:,KX)
          ENDDO
        ENDIF
        IF ( KINT > 0 ) THEN
          OIDNIO=CICDNC
          OIRCIO=CIRCI
        ENDIF
      ENDIF
!
!     * LOOP OVER ALL FIELDS INVOLVED IN CALCULATION OF RUNNING
!     * MEAN RESULTS.
!
      NRMFLDL=CEILING(REAL(IAVGPRD)/REAL(IUPDATP))
      DO IND=1,NRMFLDL
!
!       * ACCUMULATE INSTANTANEOUS RESULT AND ADVANCE COUNTER.
!
        WHERE ( ABS(SVB(:,:)-YNA) > YTINY )
          SVMBIO(:,:,IND)=SVMBIO(:,:,IND)+MAX(SVB(:,:),SVMIN)
          SVCBIO(:,:,IND)=SVCBIO(:,:,IND)+1
        ENDWHERE
        WHERE ( ABS(OEDNIO(:,:,:)-YNA) > YTINY )
          DEMBIO(:,:,:,IND)=DEMBIO(:,:,:,IND)+OEDNIO(:,:,:)
          DECBIO(:,:,:,IND)=DECBIO(:,:,:,IND)+1
        ENDWHERE
        WHERE ( ABS(OIDNIO(:,:)-YNA) > YTINY )
          DIMBIO(:,:,IND)=DIMBIO(:,:,IND)+OIDNIO(:,:)
          DICBIO(:,:,IND)=DICBIO(:,:,IND)+1
        ENDWHERE
        WHERE ( ABS(OERCIO(:,:,:)-YNA) > YTINY )
          REMBIO(:,:,:,IND)=REMBIO(:,:,:,IND)+OERCIO(:,:,:)
          RECBIO(:,:,:,IND)=RECBIO(:,:,:,IND)+1
        ENDWHERE
        WHERE ( ABS(OIRCIO(:,:)-YNA) > YTINY )
          RIMBIO(:,:,IND)=RIMBIO(:,:,IND)+OIRCIO(:,:)
          RICBIO(:,:,IND)=RICBIO(:,:,IND)+1
        ENDWHERE
!
        IOFF=(KNT+IAVGPRD-1)-(IND-1)*IUPDATP
        IF ( IOFF >= 1 .AND. MOD(IOFF,IAVGPRD)==0 ) THEN
!
!         * RUNNING MEAN SUPERSATURATION.
!
          WHERE ( SVCBIO(:,:,IND) > 0 )
            SVVBIO(:,:)=SVMBIO(:,:,IND)/REAL(SVCBIO(:,:,IND))
          ELSEWHERE
            SVVBIO(:,:)=YNA
          ENDWHERE
!
!         * RUNNING MEAN CLOUD DROPLET NUMBER CONCENTRATION.
!
          WHERE ( DECBIO(:,:,:,IND) > 0 )
            DEVBIO(:,:,:)=DEMBIO(:,:,:,IND)/REAL(DECBIO(:,:,:,IND))
          ELSEWHERE
            DEVBIO(:,:,:)=YNA
          ENDWHERE
          WHERE ( DICBIO(:,:,IND) > 0 )
            DIVBIO(:,:)=DIMBIO(:,:,IND)/REAL(DICBIO(:,:,IND))
          ELSEWHERE
            DIVBIO(:,:)=YNA
          ENDWHERE
!
!         * RUNNING MEAN CRITICAL RADIUS.
!
          WHERE ( RECBIO(:,:,:,IND) > 0 )
            REVBIO(:,:,:)=REMBIO(:,:,:,IND)/REAL(RECBIO(:,:,:,IND))
          ELSEWHERE
            REVBIO(:,:,:)=YNA
          ENDWHERE
          WHERE ( RICBIO(:,:,IND) > 0 )
            RIVBIO(:,:)=RIMBIO(:,:,IND)/REAL(RICBIO(:,:,IND))
          ELSEWHERE
            RIVBIO(:,:)=YNA
          ENDWHERE
!
!         * FILL HOLES IN MEAN PROFILES, IF NO VALID RESULTS.
!
          CALL FILLH(SVVBIO,ZHA,YNA,ILGA,LEVA)
          DO KX=1,KEXT
            TMPM=DEVBIO(:,:,KX)
            CALL FILLH(TMPM,ZHA,YNA,ILGA,LEVA)
            DEVBIO(:,:,KX)=TMPM
            TMPM=REVBIO(:,:,KX)
            CALL FILLH(TMPM,ZHA,YNA,ILGA,LEVA)
            REVBIO(:,:,KX)=TMPM
          ENDDO
          CALL FILLH(DIVBIO,ZHA,YNA,ILGA,LEVA)
          CALL FILLH(RIVBIO,ZHA,YNA,ILGA,LEVA)
!
!         * RESET ACCUMULATED RESULT AND COUNTER.
!
          SVMBIO(:,:,  IND)=0.
          SVCBIO(:,:,  IND)=0
          DEMBIO(:,:,:,IND)=0.
          DECBIO(:,:,:,IND)=0
          DIMBIO(:,:,  IND)=0.
          DICBIO(:,:,  IND)=0
          REMBIO(:,:,:,IND)=0.
          RECBIO(:,:,:,IND)=0
          RIMBIO(:,:,  IND)=0.
          RICBIO(:,:,  IND)=0
        ENDIF
      ENDDO
!
!     * SAVE AVERAGED SUPERSATURATION, CLOUD DROPLET NUMBER,
!     * AND CRITICAL DROPLET RADIUS.
!
      WHERE ( ABS(SVVBIO-YNA) > YTINY )
        PSVVIO=SVVBIO
      ENDWHERE
      IF ( KEXT > 0 ) THEN
        WHERE ( ABS(DEVBIO-YNA) > YTINY )
          PDEVIO=DEVBIO
        ENDWHERE
        WHERE ( ABS(REVBIO-YNA) > YTINY )
          PREVIO=REVBIO
        ENDWHERE
      ENDIF
      IF ( KINT > 0 ) THEN
        WHERE ( ABS(DIVBIO-YNA) > YTINY )
          PDIVIO=DIVBIO
        ENDWHERE
        WHERE ( ABS(RIVBIO-YNA) > YTINY )
          PRIVIO=RIVBIO
        ENDWHERE
      ENDIF
!
!     * FINAL SUPERSATURATION, CLOUD DROPLET NUMBER AND CRITICAL DROPLET
!     * RADIUS. IF NO CURRENT AVERAGED VALUES ARE AVAILABLE OWING TO A
!     * LACK OF CLOUDY POINTS, USE SAVED VALUES FROM PREVIOUS
!     * CALCULATIONS (PERSISTENCE). IF NONE OF THESE ARE VALID, USE
!     * CONSTANTS.
!
      WHERE ( ABS(SVVBIO-YNA) > YTINY )
        SVF=SVVBIO
      ELSEWHERE ( ABS(PSVVIO-YNA) > YTINY )
        SVF=PSVVIO
      ELSEWHERE
        SVF=SVREF
      ENDWHERE
      IF ( KEXT > 0 ) THEN
        WHERE ( ABS(DEVBIO-YNA) > YTINY )
          PECDNC=DEVBIO
        ELSEWHERE ( ABS(PDEVIO-YNA) > YTINY )
          PECDNC=PDEVIO
        ELSEWHERE
          PECDNC=DNREF
        ENDWHERE
        WHERE ( ABS(REVBIO-YNA) > YTINY )
          PERCI=REVBIO
        ELSEWHERE ( ABS(PREVIO-YNA) > YTINY )
          PERCI=PREVIO
        ELSEWHERE
          PERCI=RCREF
        ENDWHERE
      ENDIF
      IF ( KINT > 0 ) THEN
        WHERE ( ABS(DIVBIO-YNA) > YTINY )
          PICDNC=DIVBIO
        ELSEWHERE ( ABS(PDIVIO-YNA) > YTINY )
          PICDNC=PDIVIO
        ELSEWHERE
          PICDNC=DNREF
        ENDWHERE
        WHERE ( ABS(RIVBIO-YNA) > YTINY )
          PIRCI=RIVBIO
        ELSEWHERE ( ABS(PRIVIO-YNA) > YTINY )
          PIRCI=PRIVIO
        ELSEWHERE
          PIRCI=RCREF
        ENDWHERE
      ENDIF
!
!     * MEAN CLOUD DROPLET NUMBER.
!
      CDNC=0.
      IF ( KEXT > 0 ) THEN
        DO KX=1,KEXT
          CDNC=CDNC+PECDNC(:,:,KX)*RHOA
        ENDDO
      ENDIF
      IF ( KINT > 0 ) THEN
        CDNC=CDNC+PICDNC*RHOA
      ENDIF
!
!     * SCALE CLOUD DROPLET NUMBER TO ACCOUNT FOR NON-ADIABATIC
!     * MIXING OF CLOUD CORE WITH ENVIRONMENT ABOVE CLOUD BASE.
!
      ADIFAC=1.
      CDNC=CDNC*ADIFAC
!
!     * GENERAL DIAGNOSTIC RESULTS, INCLUDING AEROSOL NUMBER, MASS,
!     * CCN CONCENTRATIONS.
!
      IF ( KXTRA1 ) THEN
        CALL PAMDI(CNT,CN20,CN50,CN100,CN200,CNTW,CN20W,CN50W, &
                   CN100W,CN200W,CC02,CC04,CC08,CC16,ACAS,ACOA, &
                   ACBC,ACSS,ACMD,SUPS,CCN,CCNE,RCRI,VCCN,VCNE, &
                   PEMAS,PENUM,PEN0,PEPHI0,PEPSI,PEPHIS0, &
                   PEDPHI0,PECDNC,PEDDN,PIMAS,PINUM,PIN0, &
                   PIPHI0,PIPSI,PIPHIS0,PIDPHI0,PICDNC,PIDDN, &
                   PIFRC,PIRCI,CEDDNIS,CEDDNSL,CENUIO,CEMOLW, &
                   CEKAPPA,CEEPSM,CEWETRB,CIDDNIS,CIDDNSL,CINUIO, &
                   CIMOLW,CIKAPPA,CIEPSM,CIWETRB,TA,RHOA,SZCLF, &
                   SZMLWC,SVF,VITRM,ZCTHR,SVREF,ILGA,LEVA)
      ENDIF
!
!-----------------------------------------------------------------------
!     * IN-CLOUD OXIDATION AND WET REMOVAL FOR AEROSOL.
!
      CALL SCLDSP(PEDNDT,PEDMDT,PIDNDT,PIDMDT,PIDFDT,DSIVDT, &
                  DHPDT,DERSN,DERSM,DIRSMN,DIRSN,DIRSMS,DSICP, &
                  DWR1N,DWR2N,DWR3N,DWR1M,DWR2M,DWR3M,DWS1N, &
                  DWS2N,DWS3N,DWS1M,DWS2M,DWS3M,PEHENR,PIHENR, &
                  PEMAS,PENUM,PEPHIS0,PEDPHI0,PEPSI,PEDDN,PENFRC, &
                  PEMFRC,PEWETRC,PERCI,PIMAS,PINUM,PIFRC,PIPHIS0, &
                  PIDPHI0,PIPSI,PINFRC,PIMFRC,PIWETRC,O3FRT, &
                  H2O2FRT,PIRCI,SO2FRC,HPFRC,BCIC,TA,RHOA,DPA, &
                  SPMREP,SZMLWC,SPFSNOW,SPFRAIN,SZCLF,SCLRFR,SCLRFS, &
                  SPFEVAP,SPFSUBL,SO2,H2O2,XO3,XNA,XAM,CO2,DT, &
                  ZCTHR,ILGA,LEVA,IERRSD)
!
!     * DIAGNOSE SULPHUR IN-CLOUD PRODUCTION AND WET REMOVAL TERMS.
!
      VASI=0.
      VAS1=0.
      VAS2=0.
      VAS3=0.
      DO L=1,LEVA
        VASI=VASI+VITRM(:,L)*DSICP(:,L)
        VAS1=VAS1+VITRM(:,L)*(DWS1M(:,L)+DWR1M(:,L))
        VAS2=VAS2+VITRM(:,L)*(DWS2M(:,L)+DWR2M(:,L))
        VAS3=VAS3+VITRM(:,L)*(DWS3M(:,L)+DWR3M(:,L))
      ENDDO
!
!     * DIAGNOSE WET DEPOSITION FROM NET CLOUD TENDENCIES.
!
      CALL DIAGFLX(VOAW,VBCW,VASW,VMDW,VSSW, &
                   PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                   VITRM,DT,ILGA,LEVA)
      VASW=VASW-VASI
      IF ( KXTRA1 ) THEN
!
!       * SCAVENGING EFFICIENCY FOR SULPHUR.
!
        HENR=0.
        IF ( ISAEXT > 0 ) THEN
          HENR=PEHENR
        ENDIF
        IF ( ISAINT > 0 ) THEN
          HENR=PIHENR
        ENDIF
!
!       * O3 AND H2SO2 OXIDATION RATES
!
        O3FRC=O3FRT
        H2O2FRC=H2O2FRT
      ENDIF
!
!     * UPDATE AEROSOL NUMBER AND MASS.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=MAX(PENUM+DT*PEDNDT,0.)
        PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=MAX(PINUM+DT*PIDNDT,0.)
        PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
      ENDIF
      SO2=SO2+DT*DSIVDT
      H2O2=H2O2+DT*DHPDT
!
!     * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
      IF ( KXTRA2 ) THEN
        CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                      PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                      PINUM,PIMAS,PIFRC, &
                      CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                      RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
      ELSE
        CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                     PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                     PINUM,PIMAS,PIFRC,ILGA,LEVA)
      ENDIF
!
!-----------------------------------------------------------------------
!     * GRAVITATIONAL SETTLING.
!
      CALL GRVSTL(PEDNDT,PEDMDT,PIDFDT,PIDNDT,PIDMDT,PENUM,PEMAS, &
                  PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PEDDN, &
                  PEWETRC,PIFRC,PINUM,PIMAS,PIN0,PIPHI0, &
                  PIPSI,PIPHIS0,PIDPHI0,PIDDN,PIWETRC, &
                  TA,PHA,DPA,RHOA,GRAV,DT,ILGA,LEVA)
!
!     * DIAGNOSE AEROSOL REMOVAL BY GRAVITATIONAL SETTLING.
!
      CALL DIAGFLX(VOAD,VBCD,VASD,VMDD,VSSD, &
                   PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                   VITRM,DT,ILGA,LEVA)
!
!     * UPDATE AEROSOL NUMBER AND MASS.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=MAX(PENUM+DT*PEDNDT,0.)
        PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=MAX(PINUM+DT*PIDNDT,0.)
        PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
      ENDIF
!
!     * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
      IF ( KXTRA2 ) THEN
        CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                      PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                      PINUM,PIMAS,PIFRC, &
                      CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                      RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
      ELSE
        CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                     PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                     PINUM,PIMAS,PIFRC,ILGA,LEVA)
      ENDIF
!
!-----------------------------------------------------------------------
!     * DRY DEPOSITION.
!
      IF ( ISAINT > 0 ) THEN
        CALL DRYDEP(ILGA,LEVA,ZSPDDD,FCANA,CDMLA,CDMNLA,FLNDDU,ICANA, &
                    FCSA,FGSA,FCA,FGA,TA,PHA,DPA,DT,RHOA,GRAV, &
                    PINUM,PIMAS,PIDDN,PISTICK,PIWETRC,PIDRYRC, &
                    PIPHI0,PIPHIS0,PIDPHI0,PIPSI,ISAINT, &
                    PIDNDT,PIDMDT,PIFLXN,PIFLXM,PIVDRN,PIVDRM)
        PIDFDT=0.
      ENDIF
      IF ( ISAEXT > 0 ) THEN
        CALL DRYDEP(ILGA,LEVA,ZSPDDD,FCANA,CDMLA,CDMNLA,FLNDDU,ICANA, &
                    FCSA,FGSA,FCA,FGA,TA,PHA,DPA,DT,RHOA,GRAV, &
                    PENUM,PEMAS,PEDDN,PESTICK,PEWETRC,PEDRYRC, &
                    PEPHI0,PEPHIS0,PEDPHI0,PEPSI,ISAEXT, &
                    PEDNDT,PEDMDT,PEFLXN,PEFLXM,PEVDRN,PEVDRM)
      ENDIF
!
!     * DIAGNOSE DRY DEPOSITION OF AEROSOL (TOGETHER WITH GRAVITATIONAL
!     * SETTLING).
!
      CALL DIAGFLX(VOAT,VBCT,VAST,VMDT,VSST, &
                   PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                   VITRM,DT,ILGA,LEVA)
      VOAD=VOAD+VOAT
      VBCD=VBCD+VBCT
      VASD=VASD+VAST
      VMDD=VMDD+VMDT
      VSSD=VSSD+VSST
!
!     * UPDATE AEROSOL NUMBER AND MASS.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=MAX(PENUM+DT*PEDNDT,0.)
        PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=MAX(PINUM+DT*PIDNDT,0.)
        PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
      ENDIF
!
!-----------------------------------------------------------------------
!     * SEA SALT CONCENTRATIONS IN FIRST MODEL LAYER.
!
      CALL SSGEN (PEDNDT,PEDMDT,PIDFDT,PIDNDT,PIDMDT,PEMAS, &
                  PENUM,PIMAS,PINUM,PEDPHI0,PEPHIS0,PIDPHI0, &
                  PIPHIS0,PIFRC,PIDDN,ZSPDSS,FOCNDU, &
                  SICNDU,DT,SICN_CRT,ILGA,LEVA)
!
!     * DIAGNOSE TOTAL EMISSIONS FOR SEA SALT. FOR EXTERNALLY MIXED AEROSOL.
!
      VSSE=0.
      IF ( ISEXTSS > 0 ) THEN
        DO IS=1,ISEXTSS
        DO L=1,LEVA
          VSSE(:)=VSSE(:)+VITRM(:,L)*PEDMDT(:,L,IEXSS(IS))
        ENDDO
        ENDDO
      ENDIF
!
!     * THE SAME AS ABOVE FOR INTERNALLY MIXED AEROSOL.
!
      IF ( ISINTSS > 0 ) THEN
        DO IS=1,ISINTSS
          ISX=IINSS(IS)
          DO L=1,LEVA
            VSSE(:)=VSSE(:)+VITRM(:,L)*(PIFRC(:,L,ISX,KINTSS) &
                  *PIDMDT(:,L,ISX)+PIMAS(:,L,ISX) &
                  *PIDFDT(:,L,ISX,KINTSS)+PIDMDT(:,L,ISX) &
                  *PIDFDT(:,L,ISX,KINTSS)*DT)
          ENDDO
        ENDDO
      ENDIF
!
!     * UPDATE AEROSOL NUMBER AND MASS.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=MAX(PENUM+DT*PEDNDT,0.)
        PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=MAX(PINUM+DT*PIDNDT,0.)
        PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
      ENDIF
!
!-----------------------------------------------------------------------
!     * PRIMARY EMISSIONS OF AEROSOL.
!
      CALL PRIAEM (PEDNDT,PEDMDT,PIDFDT,PIDNDT,PIDMDT,PIMAS,PIFRC, &
                   DOCEM1S,DOCEM2S,DOCEM3S,DOCEM4S, &
                   DBCEM1S,DBCEM2S,DBCEM3S,DBCEM4S, &
                   DSUEM1S,DSUEM2S,DSUEM3S,DSUEM4S,DT,ILGA,LEVA)
!
!     * DIAGNOSE EMISSIONS FOR PRIMARY OC, BC, AND SULPHATE.
!
      CALL DIAGFLX(VOAE,VBCE,VASE,VMDE,VSST, &
                   PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                   VITRM,DT,ILGA,LEVA)
!
!     * UPDATE AEROSOL NUMBER AND MASS.
!
      IF ( ISAEXT > 0 ) THEN
        PENUM=MAX(PENUM+DT*PEDNDT,0.)
        PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PINUM=MAX(PINUM+DT*PIDNDT,0.)
        PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
      ENDIF
!
!     * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
      IF ( KXTRA2 ) THEN
        CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                      PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                      PINUM,PIMAS,PIFRC, &
                      CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                      RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
      ELSE
        CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                     PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                     PINUM,PIMAS,PIFRC,ILGA,LEVA)
      ENDIF
!
!-----------------------------------------------------------------------
!     * MINERAL DUST EMISSIONS.
!
      IF ( KEXTMD>0 .OR. KINTMD>0 ) THEN
        ALLOCATE (DESIZE(ILGA,ISDIAG))
        ALLOCATE (DEFLUX(ILGA,ISDIAG))
        ALLOCATE (DEFNUM(ILGA,ISDIAG))
!
        CALL DUSTEM (PEDNDT,PEDMDT,PIDFDT,PIDNDT,PIDMDT,PIMAS, &
                     PEDPHI0,PEPHIS0,PIDPHI0,PIPHIS0, &
                     PIFRC,DPA,DT,GRAV,ILGA,LEVA, &
                     FLNDDU,GTRDU,SMFRDU,FNDU,ZSPDDU,USTARD, &
                     SUZ0,SLAI,SPOT,ST02,ST03,ST04,ST06, &
                     ST13,ST14,ST15,ST16,ST17, &
                     ISVDUST,DEFA,DEFC, &
                     FALL,FA10,FA2,FA1, &
                     DUWD,DUST,DUTH,USMK, &
                     DIAGMAS,DIAGNUM,ISDUST,DESIZE,DEFLUX,DEFNUM, &
                     ISDIAG)
!
!       * WRITE TO DUST EMISSION SIZE DISTRIBUTION DIAGNOSTICS
!
        IF ( KXTRA2 ) THEN
!
!         * EMISSION SIZE DISTRIBUTION INFORMATION.
!
          DO ISF=1,ISDUST
            DMAC(:,ISF)=DIAGMAS(:,ISF)
            DMCO(:,ISF)=DIAGMAS(:,ISF+ISDUST)
            DNAC(:,ISF)=DIAGNUM(:,ISF)
            DNCO(:,ISF)=DIAGNUM(:,ISF+ISDUST)
          ENDDO
          DEFX=DEFLUX
          DEFN=DEFNUM
        ENDIF
        DEALLOCATE (DESIZE)
        DEALLOCATE (DEFLUX)
        DEALLOCATE (DEFNUM)
!
!       * DIAGNOSE TOTAL EMISSIONS FOR DUST. FOR EXTERNALLY MIXED AEROSOL.
!
        IF ( ISEXTMD > 0 ) THEN
          DO IS=1,ISEXTMD
          DO L=1,LEVA
            VMDE(:)=VMDE(:)+VITRM(:,L)*PEDMDT(:,L,IEXMD(IS))
          ENDDO
          ENDDO
        ENDIF
!
!       * THE SAME AS ABOVE FOR INTERNALLY MIXED AEROSOL.
!
        IF ( ISINTMD > 0 ) THEN
          DO IS=1,ISINTMD
            ISX=IINMD(IS)
            DO L=1,LEVA
              VMDE(:)=VMDE(:)+VITRM(:,L)*(PIFRC(:,L,ISX,KINTMD) &
                    *PIDMDT(:,L,ISX)+PIMAS(:,L,ISX) &
                    *PIDFDT(:,L,ISX,KINTMD)+PIDMDT(:,L,ISX) &
                    *PIDFDT(:,L,ISX,KINTMD)*DT)
            ENDDO
          ENDDO
        ENDIF
!
!       * UPDATE AEROSOL NUMBER AND MASS.
!
        IF ( ISAEXT > 0 ) THEN
          PENUM=MAX(PENUM+DT*PEDNDT,0.)
          PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PINUM=MAX(PINUM+DT*PIDNDT,0.)
          PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
          PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
        ENDIF
!
!       * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
        IF ( KXTRA2 ) THEN
          CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                        PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                        PINUM,PIMAS,PIFRC, &
                        CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                        RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
        ELSE
          CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                       PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                       PINUM,PIMAS,PIFRC,ILGA,LEVA)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * CONVERT EXTERNALLY MIXED BLACK CARBON TO INTERNALLY MIXED
!     * AEROSOL TO MIMICK AGING OF HYDROPHOBIC BLACK CARBON.
!
      IF ( (KEXTBC>0 .AND. KINTBC>0) .OR. (KEXTOC>0 .AND. KINTOC>0) &
                                                                 ) THEN
        CALL AGEBC(PEDNDT,PEDMDT,PIDNDT,PIDMDT,PIDFDT,PEDDN,PEN0, &
                   PEPHI0,PEPSI,PIFRC,PIMAS,ATAUS,DT,ILGA,LEVA)
!
!       * UPDATE AEROSOL NUMBER AND MASS.
!
        IF ( ISAEXT > 0 ) THEN
          PENUM=MAX(PENUM+DT*PEDNDT,0.)
          PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PINUM=MAX(PINUM+DT*PIDNDT,0.)
          PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
          PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
        ENDIF
!
!       * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
        IF ( KXTRA2 ) THEN
          CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                        PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                        PINUM,PIMAS,PIFRC, &
                        CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                        RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
        ELSE
          CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                       PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                       PINUM,PIMAS,PIFRC,ILGA,LEVA)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * GAS-TO-PARTICLE TRANSFER FOR SULPHATE AEROSOL.
!
      IF ( KEXTSO4>0 .OR. KINTSO4>0 ) THEN
!
!       * SULPHURIC ACID MIXING RATIO AND PRODUCTION RATE.
!
        IF ( MOD(KNT,ICFRQ) == 0 .OR. .NOT.KAVGS ) THEN
!
!         * ENHANCED CLEAR-SKY RELATIVE HUMIDITY IN CLOUDY GRID CELLS.
!
          WHERE ( SZCLF > 0.2 )
            RHAM=1.
          ELSEWHERE
            RHAM=RHTA
          ENDWHERE
!
!         * NUCLEATION AND CONDENSATION.
!
          CALL GTPCNV(DGDT,PEDNDT,PEDMDT,PIDNDT,PIDMDT,PIDFDT, &
                      PEDDN,PEMAS,PEN0,PENUM,PEPHI0,PEPSI,PEWETRC, &
                      PIFRC,PIDDN,PIMAS,PIN0,PINUM,PIPHI0,PIPSI, &
                      PIWETRC,TA,RHTA,RHOA,H2SO4,XNH3,SGPR,SZCLF,PBLTS, &
                      DT,ILGA,LEVA,DERSN,DERSM,DIRSN,DIRSMN,DIRSMS, &
                      DENNCL,DECNCL,DINNCL,DICNCL,DECND,DICND,IERRSD)
          DNCLDT=DECNCL+DICNCL
        ENDIF
        IF ( KAVGS ) THEN
          SCF=1.
          CALL AVGTNDG(SCF,DGDT,SGPR,QGVBIO,QGCBIO,QGMBIO,H2SO4,DT, &
                       KNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
          CALL AVGTND(SCF,PIDNDT,PIDMDT,PIDFDT,QNVBIO,QSVBIO,QNCBIO, &
                      QSCBIO,QNMBIO,QSMBIO,PINUM,PIMAS,PIFRC,DT, &
                      KNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
          DGDT=DGDT*SCF+SGPR*(1.-SCF)
          IF ( ISAEXT > 0 ) THEN
            PEDMDT=0.
            PEDNDT=0.
          ENDIF
          CALL AVGTNDD(DNCLDT,QDVBIO,QDCBIO,QDMBIO, &
                       KNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
          DNCLDT=DNCLDT*SCF
        ENDIF
!
!       * DIAGNOSE GAS-TO-PARTICLE TRANSFER FOR OC, BC, AND SULPHATE.
!
        CALL DIAGFLX(VOAG,VBCG,VASG,VMDG,VSSG, &
                     PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                     VITRM,DT,ILGA,LEVA)
        IF ( KXTRA1 ) THEN
          SSUN=DNCLDT*RHOA
          VASN=0.
          DO L=1,LEVA
            VASN(:)=VASN(:)+VITRM(:,L)*DNCLDT(:,L)
          ENDDO
        ENDIF
        IF ( .NOT.KAVGS ) THEN
!
!         * UPDATE NUMBER AND MASS RESIDUALS.
!
          RESN=DERSN+DIRSN
          RESM=DERSM+DIRSMS+DIRSMN
          IF ( KXTRA1 ) THEN
!
!           * DIAGNOSE AEROSOL SOURCES AND SINKS.
!
            SNCN=(DENNCL+DINNCL)*RHOA
            SCND=(DECND+DICND)*RHOA
            SGSP=SGPR*RHOA
            VNCN=0.
            VCND=0.
            VGSP=0.
            DO L=1,LEVA
              VNCN(:)=VNCN(:)+VITRM(:,L)*(DENNCL(:,L)+DINNCL(:,L))
              VCND(:)=VCND(:)+VITRM(:,L)*(DECND (:,L)+DICND (:,L))
              VGSP(:)=VGSP(:)+VITRM(:,L)*SGPR(:,L)
            ENDDO
          ENDIF
          IF ( KXTRA2 ) THEN
!
!           * DIAGNOSE NUMBER AND MASS CORRECTION TERMS.
!
            RSN3=RESN*RHOA
            RSM3=RESM*RHOA
            VRN3=0.
            VRM3=0.
            DO L=1,LEVA
              VRN3(:)=VRN3(:)+VITRM(:,L)*RESN(:,L)
              VRM3(:)=VRM3(:)+VITRM(:,L)*RESM(:,L)
            ENDDO
          ENDIF
        ENDIF
!
!       * UPDATE AEROSOL NUMBER AND MASS.
!
        IF ( ISAEXT > 0 ) THEN
          PENUM=MAX(PENUM+DT*PEDNDT,0.)
          PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PINUM=MAX(PINUM+DT*PIDNDT,0.)
          PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
          PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
        ENDIF
        H2SO4=H2SO4+DT*DGDT
!
!       * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
        IF ( KXTRA2 ) THEN
          CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                        PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                        PINUM,PIMAS,PIFRC, &
                        CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                        RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
        ELSE
          CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                       PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                       PINUM,PIMAS,PIFRC,ILGA,LEVA)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * GAS-TO-PARTICLE TRANSFER FOR SECONDARY ORGANIC AEROSOL (SOA).
!
      IF ( KEXTOC>0 .OR. KINTOC>0 ) THEN
        OGPRT=0.
        IF ( MOD(KNT,ICFRQ) == 0 .OR. .NOT.KAVGO ) THEN
          CALL GTSOA (DGDT,PEDNDT,PEDMDT,PIDNDT,PIDMDT,PIDFDT, &
                      PEDDN,PEMAS,PEN0,PENUM,PEPHI0,PEPSI,PEWETRC, &
                      PIFRC,PIDDN,PIMAS,PIN0,PINUM,PIPHI0,PIPSI, &
                      PIWETRC,TA,RHOA,SOAP,OGPRT,DT,ILGA,LEVA, &
                      DERSN,DERSM,DIRSN,DIRSMN,DIRSMS,DECND,DICND, &
                      IERR)
        ENDIF
        IF ( KAVGO ) THEN
          SCF=1.
          CALL AVGTNDG(SCF,DGDT,OGPRT,OGVBIO,OGCBIO,OGMBIO,OGAS,DT, &
                       KNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
          CALL AVGTND(SCF,PIDNDT,PIDMDT,PIDFDT,ONVBIO,OSVBIO,ONCBIO, &
                      OSCBIO,ONMBIO,OSMBIO,PINUM,PIMAS,PIFRC,DT, &
                      KNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
          DGDT=DGDT*SCF+OGPRT*(1.-SCF)
          IF ( ISAEXT > 0 ) THEN
            PEDMDT=0.
            PEDNDT=0.
          ENDIF
        ENDIF
!
!       * DIAGNOSE GAS-TO-PARTICLE TRANSFER FOR OC, BC, AND SULPHATE.
!
        CALL DIAGFLX(VOAT,VBCT,VAST,VMDT,VSST, &
                     PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                     VITRM,DT,ILGA,LEVA)
        VOAG=VOAG+VOAT
        VBCG=VBCG+VBCT
        VASG=VASG+VAST
        VMDG=VMDG+VMDT
        VSSG=VSSG+VSST
!
!       * UPDATE AEROSOL NUMBER AND MASS.
!
        IF ( ISAEXT > 0 ) THEN
          PENUM=MAX(PENUM+DT*PEDNDT,0.)
          PEMAS=MAX(PEMAS+DT*PEDMDT,0.)
        ENDIF
        IF ( ISAINT > 0 ) THEN
          PINUM=MAX(PINUM+DT*PIDNDT,0.)
          PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
          PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
        ENDIF
        SOAP=SOAP+DT*DGDT
!
!       * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
        IF ( KXTRA2 ) THEN
          CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                        PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                        PINUM,PIMAS,PIFRC, &
                        CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                        RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
        ELSE
          CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                       PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                       PINUM,PIMAS,PIFRC,ILGA,LEVA)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * GAS-TO-PARTICLE TRANSFER FOR SEA SALT. SULPHURIC ACID AND
!     * SOA PRECURSOR GASES ARE CONDENSED AND INSTANTANEOUSLY REMOVED
!     * FROM THE ATMOSPHERE, ASSUMING EFFICIENT DRY DEPOSITION OF
!     * SEA SALT AEROSOL. IMPACTS ON CHEMICAL COMPOSITION OF SEA SALT
!     * ARE OMITTED.
!
      IF ( KEXTSS>0 ) THEN
        SGPR=0.
        OGPRT=0.
        CALL GTSS (DGDT,DGDTX,PEDDN,PEN0,PENUM,PEPHI0,PEPSI, &
                   PEWETRC,TA,RHOA,H2SO4,SOAP,SGPR,OGPRT,DT, &
                   ILGA,LEVA,DECND,DECNDX)
!
!       * DIAGNOSE GAS-TO-PARTICLE TRANSFER AND DEPOSITION RATES.
!
        DO L=1,LEVA
          VASG(:)=VASG(:)-VITRM(:,L)*DGDT(:,L)*WAMSUL/WH2SO4
          VOAG(:)=VOAG(:)-VITRM(:,L)*DGDTX(:,L)
          VASD(:)=VASD(:)+VITRM(:,L)*DGDT(:,L)*WAMSUL/WH2SO4
          VOAD(:)=VOAD(:)+VITRM(:,L)*DGDTX(:,L)
        ENDDO
        IF ( KXTRA1 ) THEN
          SCND=SCND+DECND*RHOA
          DO L=1,LEVA
            VCND(:)=VCND(:)+VITRM(:,L)*DECND(:,L)
          ENDDO
        ENDIF
!
!       * UPDATE SULPHURIC ACID CONCENTRATION.
!
        H2SO4=H2SO4+DT*DGDT
        SOAP=SOAP+DT*DGDTX
      ENDIF
!
!-----------------------------------------------------------------------
!     * COAGULATION. ONLY INTERNALLY MIXED AEROSOL IS CONSIDERED.
!
      IF ( ISAINT > 0 ) THEN
        IF ( MOD(KNT,ICFRQ) == 0 .OR. .NOT.KAVG ) THEN
          CALL COAG(PIDNDT,PIDMDT,PIDFDT,PINUM,PIMAS,PIFRC,PIN0, &
                    PIPHI0,PIPSI,PIDDN,PIWETRC,TA,PHA,DT,ILGA,LEVA)
        ENDIF
        IF ( KAVG ) THEN
          SCF=1.
          CALL AVGTND(SCF,PIDNDT,PIDMDT,PIDFDT,PNVBIO,PSVBIO,PNCBIO, &
                      PSCBIO,PNMBIO,PSMBIO,PINUM,PIMAS,PIFRC,DT, &
                      KNT,ICFRQ,IAVGPRD,IUPDATP,NRMFLD,ILGA,LEVA)
        ENDIF
!
!       * UPDATE AEROSOL NUMBER AND MASS.
!
        PINUM=MAX(PINUM+DT*PIDNDT,0.)
        PIMAS=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRC=MAX(PIFRC+DT*PIDFDT,0.)
!
!       * UPDATE DENSITY (INTERNAL MIXTURE) AND PLA PARAMETERS.
!
        IF ( KXTRA2 ) THEN
          CALL NM2PARX (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                        PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                        PINUM,PIMAS,PIFRC, &
                        CORN,CORM,RSN1,RSM1,VRN1,VRM1, &
                        RSN2,RSM2,VRN2,VRM2,VITRM,ILGA,LEVA)
        ELSE
          CALL NM2PAR (PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0,PIN0,PIPHI0, &
                       PIPSI,PIPHIS0,PIDPHI0,PIDDN,PENUM,PEMAS,PEDDN, &
                       PINUM,PIMAS,PIFRC,ILGA,LEVA)
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * COPY RESULTS INTO TRACER FIELDS.
!
      TRAC(:,:,JGS6)=H2SO4
      TRAC(:,:,JSO2)=SO2
      TRAC(:,:,JHPO)=H2O2
      TRAC(:,:,JGSP)=SOAP
      IF ( ISAEXT > 0 ) THEN
        TRAC(:,:,INSEX:INFEX)=PENUM
        TRAC(:,:,IMSEX:IMFEX)=PEMAS
      ENDIF
      IF ( ISAINT > 0 ) THEN
        TRAC(:,:,INSIN:INFIN)=PINUM
      ENDIF
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        KX=SINTF%ISAER(IS)%ITYP(IST)
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        TRAC(:,:,IMLIN)=PIMAS(:,:,IS)*PIFRC(:,:,IS,KX)
      ENDDO
      ENDDO
!
!     * COPY RESULTS FOR CLOUDY-SKY/CLEAR-SKY FRACTION FOR AEROSOL
!     * CONCENTRATIONS INTO CORRESPONDING MODEL FIELD.
!
      IF ( ISAEXT > 0 ) THEN
        TRRAT(:,:,INSEX:INFEX)=PENFRC
        TRRAT(:,:,IMSEX:IMFEX)=PEMFRC
      ENDIF
      IF ( ISAINT > 0 ) THEN
        TRRAT(:,:,INSIN:INFIN)=PINFRC
      ENDIF
      DO IS=1,ISAINT
      DO IST=1,SINTF%ISAER(IS)%ITYPT
        KX=SINTF%ISAER(IS)%ITYP(IST)
        IMLIN=SINTF%ISAER(IS)%ISM(IST)
        DO L=1,LEVA
        DO IL=1,ILGA
          TRRAT(IL,L,IMLIN)=PIMFRC(IL,L,IS,KX)
        ENDDO
        ENDDO
      ENDDO
      ENDDO
      IF ( ISAEXT > 0 .AND. ISAINT > 0 ) THEN
        TRRAT(:,:,JSO2)=SO2FRC
        TRRAT(:,:,JHPO)=HPFRC
      ENDIF
!
!-----------------------------------------------------------------------
!     * DEALLOCATION OF PLA ARRAYS.
!
      IF ( ISAEXT > 0 ) THEN
        DEALLOCATE(PEPHIS0)
        DEALLOCATE(PEDPHI0)
        DEALLOCATE(PEDDN  )
        DEALLOCATE(PESTICK)
        DEALLOCATE(PEN0   )
        DEALLOCATE(PEPHI0 )
        DEALLOCATE(PEPSI  )
        DEALLOCATE(PEWETRC)
        DEALLOCATE(PENUM  )
        DEALLOCATE(PEMAS  )
        DEALLOCATE(PEDNDT )
        DEALLOCATE(PEDMDT )
        DEALLOCATE(PENFRC )
        DEALLOCATE(PEMFRC )
        DEALLOCATE(PEFLXN )
        DEALLOCATE(PEFLXM )
        DEALLOCATE(PEVDRN )
        DEALLOCATE(PEVDRM )
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DEALLOCATE(PIPHIS0)
        DEALLOCATE(PIDPHI0)
        DEALLOCATE(PIDDN  )
        DEALLOCATE(PISTICK)
        DEALLOCATE(PIN0   )
        DEALLOCATE(PIPHI0 )
        DEALLOCATE(PIPSI  )
        DEALLOCATE(PIWETRC)
        DEALLOCATE(PINUM  )
        DEALLOCATE(PIMAS  )
        DEALLOCATE(PIDNDT )
        DEALLOCATE(PIDMDT )
        DEALLOCATE(PINFRC )
        DEALLOCATE(PIFRC  )
        DEALLOCATE(PIDFDT )
        DEALLOCATE(PIMFRC )
        DEALLOCATE(PIFLXN )
        DEALLOCATE(PIFLXM )
        DEALLOCATE(PIVDRN )
        DEALLOCATE(PIVDRM )
      ENDIF
!
!     * CCN-RELATED.
!
      IF ( KEXT > 0 ) THEN
        DEALLOCATE(PECDNC)
        DEALLOCATE(PERCI )
        DEALLOCATE(CECDNC)
        DEALLOCATE(CERCI )
        DEALLOCATE(SECDNC)
        DEALLOCATE(SERCI )
      ENDIF
      IF ( KINT > 0 ) THEN
        DEALLOCATE(PICDNC)
        DEALLOCATE(PIRCI )
        DEALLOCATE(CICDNC)
        DEALLOCATE(CIRCI )
        DEALLOCATE(SICDNC)
        DEALLOCATE(SIRCI )
      ENDIF
      IF ( ISEXT > 0 ) THEN
        DEALLOCATE(CEPHIS0)
        DEALLOCATE(CEDPHI0)
      ENDIF
      IF ( ISEXTB > 0 ) THEN
        DEALLOCATE(CEWETRB)
        DEALLOCATE(CEDDNC )
        DEALLOCATE(CEDDNSL)
        DEALLOCATE(CEDDNIS)
        DEALLOCATE(CENUIO )
        DEALLOCATE(CEKAPPA)
        DEALLOCATE(CEMOLW )
        DEALLOCATE(CEEPSM )
      ENDIF
      IF ( ISINT > 0 ) THEN
        DEALLOCATE(CIPHIS0)
        DEALLOCATE(CIDPHI0)
      ENDIF
      IF ( ISINTB > 0 ) THEN
        DEALLOCATE(CIWETRB)
        DEALLOCATE(CIDDNC )
        DEALLOCATE(CIDDNSL)
        DEALLOCATE(CIDDNIS)
        DEALLOCATE(CINUIO )
        DEALLOCATE(CIKAPPA)
        DEALLOCATE(CIMOLW )
        DEALLOCATE(CIEPSM )
      ENDIF
!
      END SUBROUTINE PAM
