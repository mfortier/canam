      SUBROUTINE CNDIAG (CNT,CN20,CN50,CN100,CN200,PERADC,PIRADC, &
                         PEN0,PEPHI0,PEPSI,PEPHIS0,PEDPHI0, &
                         PIN0,PIPHI0,PIPSI,PIPHIS0,PIDPHI0, &
                         PEINDS,PEINDE,NCAT,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DIAGNOSIS OF AEROSOL NUMBER CONCENTRATION BASED ON DRY SIZE
!     DISTRIBUTION AND THRESHOLD DROPLET SIZE. THE CN CONCENTRATION IS
!     CALCULATED AS THE CUMULATIVE DROPLET CONCENTRATION FOR ALL
!     DROPLETS BIGGER THAN THE THRESHOLD SIZE.
!
!     HISTORY:
!     --------
!     * FEB 26/2015 - K.VONSALZEN   NEW, BASED ON CNCDNCB
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
      USE SCPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILGA,LEVA,NCAT
      INTEGER, INTENT(IN), DIMENSION(KEXT) :: PEINDS,PEINDE
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,KEXT,NCAT) :: PERADC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,NCAT) :: PIRADC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                               PEN0,PEPHI0,PEPSI, &
                                               PEPHIS0,PEDPHI0
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                               PIN0,PIPHI0,PIPSI, &
                                               PIPHIS0,PIDPHI0
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: CNT,CN20,CN50,CN100, &
                                                 CN200
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: CIISI
      INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: CEISI
      REAL, ALLOCATABLE, DIMENSION(:,:) :: CIRCI
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: TIDPHIS,TIPHISS, &
                                             TISDF,TINUM,TIPN0,TIPHI0, &
                                             TIPSI, &
                                             CERCI,TEDPHIS,TEPHISS, &
                                             TESDF,TENUM,TEPN0,TEPHI0, &
                                             TEPSI
!
!-----------------------------------------------------------------------
!     * ALLOCATE WORK ARRAYS.
!
      IF ( KEXT > 0 ) THEN
        ALLOCATE(CERCI(ILGA,LEVA,KEXT))
        ALLOCATE(CEISI(ILGA,LEVA,KEXT))
      ENDIF
      IF ( ISAEXT > 0 ) THEN
        ALLOCATE(TEPN0  (ILGA,LEVA,ISAEXT))
        ALLOCATE(TEPHI0 (ILGA,LEVA,ISAEXT))
        ALLOCATE(TEPSI  (ILGA,LEVA,ISAEXT))
        ALLOCATE(TEDPHIS(ILGA,LEVA,ISAEXT))
        ALLOCATE(TEPHISS(ILGA,LEVA,ISAEXT))
        ALLOCATE(TESDF  (ILGA,LEVA,ISAEXT))
        ALLOCATE(TENUM  (ILGA,LEVA,ISAEXT))
      ENDIF
      IF ( KINT > 0 ) THEN
        ALLOCATE(CIRCI (ILGA,LEVA))
        ALLOCATE(CIISI (ILGA,LEVA))
      ENDIF
      IF ( ISAINT > 0 ) THEN
        ALLOCATE(TIPN0  (ILGA,LEVA,ISAINT))
        ALLOCATE(TIPHI0 (ILGA,LEVA,ISAINT))
        ALLOCATE(TIPSI  (ILGA,LEVA,ISAINT))
        ALLOCATE(TIDPHIS(ILGA,LEVA,ISAINT))
        ALLOCATE(TIPHISS(ILGA,LEVA,ISAINT))
        ALLOCATE(TISDF  (ILGA,LEVA,ISAINT))
        ALLOCATE(TINUM  (ILGA,LEVA,ISAINT))
      ENDIF
!
!     * INITIALIZATION OF OUTPUT FIELDS.
!
      CNT=0.
      CN20=0.
      CN50=0.
      CN100=0.
      CN200=0.
!
!-----------------------------------------------------------------------
!     * MATCH PARTICLE SIZE WITH SPECIFIED SIZE CUTOFF TO DETERMINE LOWER
!     * BOUND OF SIZE SPECTRUM FOR EACH EXTERNALLY MIXED AEROSOL TYPE.
!
      DO NC=1,NCAT
        IF ( ISAEXT > 0 ) THEN
          CERCI=YLARGE
          CEISI=INA
          DO KX=1,KEXT
            IS=PEINDS(KX)
            DO L=1,LEVA
            DO IL=1,ILGA
              IF ( PEDRYR(IS)%VL > PERADC(IL,L,KX,NC)  ) THEN
                CEISI(IL,L,KX)=IS
                CERCI(IL,L,KX)=PEDRYR(IS)%VL
              ENDIF
            ENDDO
            ENDDO
            DO IS=PEINDS(KX),PEINDE(KX)
            DO L=1,LEVA
            DO IL=1,ILGA
              IF (       PERADC(IL,L,KX,NC)>=PEDRYR(IS)%VL &
                   .AND. PERADC(IL,L,KX,NC)<=PEDRYR(IS)%VR &
                   .AND. CEISI(IL,L,KX)==INA             ) THEN
                APX=(PERADC(IL,L,KX,NC)-PEDRYR(IS)%VL) &
                   /(PEDRYR(IS)%VR-PEDRYR(IS)%VL)
                CEISI(IL,L,KX)=IS
                CERCI(IL,L,KX)=APX*PEDRYR(IS)%VR &
                              +(1.-APX)*PEDRYR(IS)%VL
              ENDIF
            ENDDO
            ENDDO
            ENDDO
          ENDDO
        ENDIF
!
!       * MATCH PARTICLE SIZE WITH SPECIFIED SIZE CUTOFF TO DETERMINE LOWER
!       * BOUND OF SIZE SPECTRUM FOR EACH INTERNALLY MIXED AEROSOL TYPE.
!
        IF ( ISAINT > 0 ) THEN
          CIRCI=YLARGE
          CIISI=INA
          IS=1
          DO L=1,LEVA
          DO IL=1,ILGA
            IF ( PIDRYR(IS)%VL > PIRADC(IL,L,NC) .AND. CIISI(IL,L)==INA &
                                                                 ) THEN
              CIISI(IL,L)=IS
              CIRCI(IL,L)=PIDRYR(IS)%VL
            ENDIF
          ENDDO
          ENDDO
          DO IS=1,ISAINT
          DO L=1,LEVA
          DO IL=1,ILGA
            IF (       PIRADC(IL,L,NC)>=PIDRYR(IS)%VL &
                 .AND. PIRADC(IL,L,NC)<=PIDRYR(IS)%VR &
                 .AND. CIISI(IL,L)==INA                          ) THEN
              APX=(PIRADC(IL,L,NC)-PIDRYR(IS)%VL) &
                 /(PIDRYR(IS)%VR-PIDRYR(IS)%VL)
              CIISI(IL,L)=IS
              CIRCI(IL,L)=APX*PIDRYR(IS)%VR+(1.-APX)*PIDRYR(IS)%VL
            ENDIF
          ENDDO
          ENDDO
          ENDDO
        ENDIF
!
!-----------------------------------------------------------------------
!       * PLA PARAMETERS FOR EXTERNALLY MIXED AEROSOL TYPES.
!
        DO IS=1,ISAEXT
          TEPN0  (:,:,IS)=PEN0   (:,:,IS)
          TEPHI0 (:,:,IS)=PEPHI0 (:,:,IS)
          TEPSI  (:,:,IS)=PEPSI  (:,:,IS)
          TEDPHIS(:,:,IS)=PEDPHI0(:,:,IS)
          TEPHISS(:,:,IS)=PEPHIS0(:,:,IS)
        ENDDO
!
!       * PLA PARAMETERS FOR INTERNALLY MIXED AEROSOL TYPES.
!
        DO IS=1,ISAINT
          TIPN0  (:,:,IS)=PIN0   (:,:,IS)
          TIPHI0 (:,:,IS)=PIPHI0 (:,:,IS)
          TIPSI  (:,:,IS)=PIPSI  (:,:,IS)
          TIDPHIS(:,:,IS)=PIDPHI0(:,:,IS)
          TIPHISS(:,:,IS)=PIPHIS0(:,:,IS)
        ENDDO
!
!       * COPY SIZE INFORMATION FOR ACTIVATION SIZE INTO SIZE INFORMATION
!       * ARRAYS FOR EXTERNALLY MIXED AEROSOL.
!
        IF ( ISAEXT > 0 ) THEN
          DO KX=1,KEXT
          DO L=1,LEVA
          DO IL=1,ILGA
            IS=CEISI(IL,L,KX)
            IF ( IS /= INA ) THEN
              PHISRT=TEPHISS(IL,L,IS)+TEDPHIS(IL,L,IS)
              PHISST=MIN(MAX(LOG(CERCI(IL,L,KX)/R0),TEPHISS(IL,L,IS)), &
                                                                PHISRT)
              DPHIST=PHISRT-PHISST
              TEPHISS(IL,L,IS)=PHISST
              TEDPHIS(IL,L,IS)=DPHIST
            ENDIF
          ENDDO
          ENDDO
          ENDDO
        ENDIF
!
!       * COPY SIZE INFORMATION FOR ACTIVATION SIZE INTO SIZE INFORMATION
!       * ARRAYS FOR INTERNALLY MIXED AEROSOL.
!
        IF ( ISAINT > 0 ) THEN
          DO L=1,LEVA
          DO IL=1,ILGA
            IS=CIISI(IL,L)
            IF ( IS /= INA ) THEN
              PHISRT=TIPHISS(IL,L,IS)+TIDPHIS(IL,L,IS)
              PHISST=MIN(MAX(LOG(CIRCI(IL,L)/R0),TIPHISS(IL,L,IS)), &
                                                                PHISRT)
              DPHIST=PHISRT-PHISST
              TIPHISS(IL,L,IS)=PHISST
              TIDPHIS(IL,L,IS)=DPHIST
            ENDIF
          ENDDO
          ENDDO
        ENDIF
!
!       * OBTAIN NUMBER CONCENTRATIONS FROM INTEGRATION OF SIZE DISRIBUTION
!       * FOR EXTERNALLY MIXED AEROSOLS.
!
        IF ( ISAEXT > 0 ) THEN
          TESDF=SDINT0(TEPHI0,TEPSI,TEPHISS,TEDPHIS,ILGA,LEVA,ISAEXT)
          WHERE ( ABS(TEPSI-YNA) > YTINY )
            TENUM=TEPN0*TESDF
          ELSEWHERE
            TENUM=0.
          ENDWHERE
        ENDIF
!
!       * OBTAIN NUMBER CONCENTRATIONS FROM INTEGRATION OF SIZE DISRIBUTION
!       * FOR INTERNALLY MIXED AEROSOLS.
!
        IF ( ISAINT > 0 ) THEN
          TISDF=SDINT0(TIPHI0,TIPSI,TIPHISS,TIDPHIS,ILGA,LEVA,ISAINT)
          WHERE ( ABS(TIPSI-YNA) > YTINY )
            TINUM=TIPN0*TISDF
          ELSEWHERE
            TINUM=0.
          ENDWHERE
        ENDIF
!
!-----------------------------------------------------------------------
!       * CLOUD DROPLET NUMBER CONCENTRATION FROM INTEGRATION OF NUMBER
!       * SIZE DISTRIBUTION FROM CRITICAL SIZE TO SIZE OF LARGEST PARTICLE
!       * IN THE SIZE DISTRIBUTION. FOR EXTERNALLY MIXED AEROSOLS.
!
        DO KX=1,KEXT
        DO L=1,LEVA
        DO IL=1,ILGA
          ISX=CEISI(IL,L,KX)
          IF ( ISX /= INA ) THEN
            DO IS=PEINDS(KX),PEINDE(KX)
              IF ( IS >= ISX ) THEN
                IF ( NC == 1 ) THEN
                  CNT(IL,L)=CNT(IL,L)+TENUM(IL,L,IS)
                ELSE IF ( NC == 2 ) THEN
                  CN20(IL,L)=CN20(IL,L)+TENUM(IL,L,IS)
                ELSE IF ( NC == 3 ) THEN
                  CN50(IL,L)=CN50(IL,L)+TENUM(IL,L,IS)
                ELSE IF ( NC == 4 ) THEN
                  CN100(IL,L)=CN100(IL,L)+TENUM(IL,L,IS)
                ELSE IF ( NC == 5 ) THEN
                  CN200(IL,L)=CN200(IL,L)+TENUM(IL,L,IS)
                ENDIF
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        ENDDO
        ENDDO
!
!       * CLOUD DROPLET NUMBER CONCENTRATION FROM INTEGRATION OF NUMBER
!       * SIZE DISTRIBUTION. FOR INTERNALLY MIXED AEROSOLS.
!
        DO IS=1,ISAINT
        DO L=1,LEVA
        DO IL=1,ILGA
          ISX=CIISI(IL,L)
          IF ( ISX /= INA ) THEN
            IF ( IS >= ISX ) THEN
              IF ( NC == 1 ) THEN
                CNT(IL,L)=CNT(IL,L)+TINUM(IL,L,IS)
              ELSE IF ( NC == 2 ) THEN
                CN20(IL,L)=CN20(IL,L)+TINUM(IL,L,IS)
              ELSE IF ( NC == 3 ) THEN
                CN50(IL,L)=CN50(IL,L)+TINUM(IL,L,IS)
              ELSE IF ( NC == 4 ) THEN
                CN100(IL,L)=CN100(IL,L)+TINUM(IL,L,IS)
              ELSE IF ( NC == 5 ) THEN
                CN200(IL,L)=CN200(IL,L)+TINUM(IL,L,IS)
              ENDIF
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * DEALLOCATE WORK ARRAYS.
!
      IF ( KEXT > 0 ) THEN
        DEALLOCATE(CERCI)
        DEALLOCATE(CEISI)
      ENDIF
      IF ( ISAEXT > 0 ) THEN
        DEALLOCATE(TEPN0)
        DEALLOCATE(TEPHI0)
        DEALLOCATE(TEPSI)
        DEALLOCATE(TEDPHIS)
        DEALLOCATE(TEPHISS)
        DEALLOCATE(TESDF)
        DEALLOCATE(TENUM)
      ENDIF
      IF ( KINT > 0 ) THEN
        DEALLOCATE(CIRCI)
        DEALLOCATE(CIISI)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DEALLOCATE(TIPN0)
        DEALLOCATE(TIPHI0)
        DEALLOCATE(TIPSI)
        DEALLOCATE(TIDPHIS)
        DEALLOCATE(TIPHISS)
        DEALLOCATE(TISDF)
        DEALLOCATE(TINUM)
      ENDIF
!
      END SUBROUTINE CNDIAG
