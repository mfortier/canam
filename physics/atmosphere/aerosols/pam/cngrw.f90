      SUBROUTINE CNGRW(SV,QRV,RS,QRC,TEMP,CEWMASS,CERC,CESC,CEWETRB, &
                       CETSCL,CIWMASS,CIRC,CISC,CIWETRB,CITSCL,LMSK, &
                       QR,DRDT,HLST,DHDT,ZH,RHOA,PRES,CEDDNSL, &
                       CEDDNIS,CENUIO,CEKAPPA,CEMOLW,CEN0,CEPSI,CEMOM2, &
                       CEMOM3,CEMOM4,CEMOM5,CEMOM6,CEMOM7,CEMOM8, &
                       CIDDNSL,CIDDNIS,CINUIO,CIKAPPA,CIMOLW,CIEPSM, &
                       CIN0,CIPSI,CIMOM2,CIMOM3,CIMOM4,CIMOM5,CIMOM6, &
                       CIMOM7,CIMOM8,DT,ILGA,LEVA,CEMOD1,CEMOD2, &
                       CEMOD3,CIMOD1,CIMOD2,CIMOD3,CONVF,IEXKAP,IINKAP)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     WET PARTICLE SIZE FROM APPLICATION OF KOEHLER THEORY AND
!     CONDENSATIONAL GROWTH/EVAPORATION BASED ON SCALED VERSION
!     OF THE GROWTH EQUATION.
!
!     HISTORY:
!     --------
!     * JUN 10/2014 - K.VONSALZEN   HYGROSCOPIC MOISTURE COMPONENT
!     *                             IN CLOUD UPDRAFT, HIGHER NUMBER
!     *                             OF ITERATIONS, MODIFIED INITIAL
!     *                             BRACKET RANGE
!     * AUG 21/2013 - K.VONSALZEN   FURTHER IMPROVEMENTS BASED ON
!     *                             OFF-LINE MODEL
!     * AUG 09/2013 - K.VONSALZEN   SAVE IMPLIED SUPERSATURATION AND
!     *                             RESET RESULTS FOR SUBSATURATED
!     *                             CONDITIONS
!     * DEC 08/2010 - K.VONSALZEN   CORRECT KAPPA APPROACH (TREATMENT
!     *                             OF INSOLUBLE CORE)
!     * DEC 03/2007 - K.VONSALZEN   IMPROVE ACCURACY AT LOW RESOLUTION.
!     * APR 20/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SCPARM
      USE CNPARM
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      LOGICAL, PARAMETER :: KADIAB=.FALSE.  ! SWITCH TO IMPROVE CONVERGENCE
      LOGICAL, PARAMETER :: KCALC=.TRUE.    ! SWITCH FOR DIAGNOSTIC MODE
      REAL, PARAMETER :: SVSPEC=0.0032
!      INTEGER, PARAMETER :: ITMAX=4
      INTEGER, PARAMETER :: ITMAX=7
!
      INTEGER, INTENT(IN) :: ILGA,LEVA
      LOGICAL, INTENT(IN) :: LMSK(ILGA,LEVA)
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: RHOA,PRES,HLST,DHDT, &
                                                DRDT,QR,ZH,DT
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXTB) :: &
                                               CEDDNSL,CEDDNIS,CENUIO, &
                                               CEKAPPA,CEMOLW
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINTB) :: &
                                               CIDDNSL,CIDDNIS,CINUIO, &
                                               CIKAPPA,CIMOLW,CIEPSM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEXT) :: CEN0,CEPSI
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISEXT) :: &
                                          CEMOM2,CEMOM3,CEMOM4,CEMOM5, &
                                          CEMOM6,CEMOM7,CEMOM8
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISINT) :: CIN0,CIPSI
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISINT) :: &
                                          CIMOM2,CIMOM3,CIMOM4,CIMOM5, &
                                          CIMOM6,CIMOM7,CIMOM8
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISEXTB) :: CEWETRB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA,ISINTB) :: CIWETRB
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: TEMP,SV
      INTEGER, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEXTB) :: &
                                          CEMOD1,CEMOD2,CEMOD3
      INTEGER, INTENT(OUT), DIMENSION(ILGA,LEVA,ISINTB) :: &
                                          CIMOD1,CIMOD2,CIMOD3
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA) :: RS,QRV,QRC,CONVF
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEXTB) :: CERC,CESC, &
                                                        CETSCL
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISINTB) :: CIRC,CISC, &
                                                        CITSCL
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEXT) :: CEWMASS
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISINT) :: CIWMASS
      INTEGER, ALLOCATABLE, DIMENSION(:,:,:) :: CIIRD,CEIRD
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: CIWETRN,CIBK,CIDVP,CITKP, &
                                             CIRAT,CIWETRI, &
                                             CEWETRN,CEBK,CEDVP,CETKP, &
                                             CERAT,CEWETRI
      REAL, ALLOCATABLE, DIMENSION(:,:) :: SFCTW,AK,DV,DVX,TK,TKX, &
                                           TMP1,TMP2,TMP3,ESW,SVI, &
                                           FRESL,FRESU,FRESN,FRESNT, &
                                           SVT,SVL,SVU,SVN,SVTT,QRCI, &
                                           QRCL,QRCU,QRCN,QRCT,FRAC, &
                                           TEMPI,RSN,TEMPN,QRVN,QRHYG, &
                                           HLSTHYG
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: IMODN
!
!-----------------------------------------------------------------------
!
      IF ( ITMAX < 1 ) CALL XIT('CNGRW',-1)
!
!     * ALLOCATE TEMPORARY ARRAYS.
!
      IF ( ISEXTB > 0 ) THEN
        ALLOCATE(CEWETRN(ILGA,LEVA,ISEXTB))
        ALLOCATE(CEWETRI(ILGA,LEVA,ISEXTB))
        ALLOCATE(CEBK   (ILGA,LEVA,ISEXTB))
        ALLOCATE(CEDVP  (ILGA,LEVA,ISEXTB))
        ALLOCATE(CETKP  (ILGA,LEVA,ISEXTB))
        ALLOCATE(CERAT  (ILGA,LEVA,ISEXTB))
        ALLOCATE(CEIRD  (ILGA,LEVA,ISEXTB))
      ENDIF
      IF ( ISINTB > 0 ) THEN
        ALLOCATE(CIWETRN(ILGA,LEVA,ISINTB))
        ALLOCATE(CIWETRI(ILGA,LEVA,ISINTB))
        ALLOCATE(CIBK   (ILGA,LEVA,ISINTB))
        ALLOCATE(CIDVP  (ILGA,LEVA,ISINTB))
        ALLOCATE(CITKP  (ILGA,LEVA,ISINTB))
        ALLOCATE(CIRAT  (ILGA,LEVA,ISINTB))
        ALLOCATE(CIIRD  (ILGA,LEVA,ISINTB))
      ENDIF
      ALLOCATE(AK     (ILGA,LEVA))
      ALLOCATE(SFCTW  (ILGA,LEVA))
      ALLOCATE(DV     (ILGA,LEVA))
      ALLOCATE(DVX    (ILGA,LEVA))
      ALLOCATE(TK     (ILGA,LEVA))
      ALLOCATE(TKX    (ILGA,LEVA))
      ALLOCATE(TMP1   (ILGA,LEVA))
      ALLOCATE(TMP2   (ILGA,LEVA))
      ALLOCATE(TMP3   (ILGA,LEVA))
      ALLOCATE(ESW    (ILGA,LEVA))
      ALLOCATE(SVI    (ILGA,LEVA))
      ALLOCATE(SVN    (ILGA,LEVA))
      ALLOCATE(RSN    (ILGA,LEVA))
      ALLOCATE(TEMPN  (ILGA,LEVA))
      ALLOCATE(QRVN   (ILGA,LEVA))
      ALLOCATE(QRHYG  (ILGA,LEVA))
      ALLOCATE(HLSTHYG(ILGA,LEVA))
      ALLOCATE(FRESN  (ILGA,LEVA))
      ALLOCATE(FRESL  (ILGA,LEVA))
      ALLOCATE(FRESU  (ILGA,LEVA))
      ALLOCATE(FRESNT (ILGA,LEVA))
      ALLOCATE(SVTT   (ILGA,LEVA))
      ALLOCATE(SVT    (ILGA,LEVA))
      ALLOCATE(SVL    (ILGA,LEVA))
      ALLOCATE(SVU    (ILGA,LEVA))
      ALLOCATE(TEMPI  (ILGA,LEVA))
      ALLOCATE(QRCI   (ILGA,LEVA))
      ALLOCATE(QRCU   (ILGA,LEVA))
      ALLOCATE(QRCL   (ILGA,LEVA))
      ALLOCATE(QRCN   (ILGA,LEVA))
      ALLOCATE(QRCT   (ILGA,LEVA))
      ALLOCATE(FRAC   (ILGA,LEVA))
      ALLOCATE(IMODN  (ILGA,LEVA))
!
!-----------------------------------------------------------------------
!     * SURFACE TENSION OF WATER.
!
      SFCTW=0.0761-1.55E-04*(TEMP-273.)
!
!     * A-TERM IN KOEHLER EQUATION (FOR CURVATURE EFFECT).
!
      AK=2.*WH2O*SFCTW/(RGASM*TEMP*RHOH2O)
!
!     * B-TERM IN KOEHLER EQUATION (FOR SOLUTE EFFECT).
!
      IF ( ISEXTB > 0 ) THEN
        CEBK=YNA
        IF ( IEXKAP /= 1 ) THEN
          CORU=1.
          DO IS=1,ISEXTB
          DO L=1,LEVA
          DO IL=1,ILGA
!
!           * NUMBER OF MOLES OF SOLUTE.
!
            AMSL=(4.*YPI/3.)*CEDDNSL(IL,L,IS)*CEDRYRB(IS)**3
            ANS=AMSL*CORU*CENUIO(IL,L,IS)/CEMOLW(IL,L,IS)
!
!           * B-TERM IN KOEHLER EQUATION (FOR SOLUTE EFFECT).
!
            CEBK(IL,L,IS)=0.75*ANS*WH2O/(YPI*RHOH2O)
          END DO
          END DO
          END DO
        ELSE
          DO IS=1,ISEXTB
          DO L=1,LEVA
          DO IL=1,ILGA
!
!           * B-TERM IN KOEHLER EQUATION (FOR SOLUTE EFFECT).
!
            CEBK(IL,L,IS)=CEKAPPA(IL,L,IS)*CEDRYRB(IS)**3
          END DO
          END DO
          END DO
        ENDIF
      ENDIF
      IF ( ISINTB > 0 ) THEN
        CIBK=YNA
        IF ( IINKAP /= 1 ) THEN
          DO IS=1,ISINTB
          DO L=1,LEVA
          DO IL=1,ILGA
            IF (     ABS(CIEPSM(IL,L,IS)-YNA) > YSMALL &
                   .AND. CIEPSM(IL,L,IS) > YSMALL                ) THEN
              ARAT=(1.-CIEPSM(IL,L,IS))/CIEPSM(IL,L,IS)
              IF ( ARAT > YSMALL ) THEN
                CORU=1./(1.+(CIDDNSL(IL,L,IS)/CIDDNIS(IL,L,IS))*ARAT)
              ELSE
                CORU=1.
              ENDIF
!
!             * NUMBER OF MOLES OF SOLUTE.
!
              AMSL=(4.*YPI/3.)*CIDDNSL(IL,L,IS)*CIDRYRB(IS)**3
              ANS=AMSL*CORU*CINUIO(IL,L,IS)/CIMOLW(IL,L,IS)
!
!             * B-TERM IN KOEHLER EQUATION (FOR SOLUTE EFFECT).
!
              CIBK(IL,L,IS)=0.75*ANS*WH2O/(YPI*RHOH2O)
            ENDIF
          END DO
          END DO
          END DO
        ELSE
          DO IS=1,ISINTB
          DO L=1,LEVA
          DO IL=1,ILGA
            IF (       ABS(CIKAPPA(IL,L,IS)-YNA) > YSMALL &
                     .AND. CIKAPPA(IL,L,IS) > YSMALL &
                 .AND. ABS(CIEPSM(IL,L,IS) -YNA) > YSMALL &
                     .AND. CIEPSM(IL,L,IS) > YSMALL              ) THEN
              ARAT=(1.-CIEPSM(IL,L,IS))/CIEPSM(IL,L,IS)
              IF ( ARAT > YSMALL ) THEN
                CORU=1./(1.+(CIDDNSL(IL,L,IS)/CIDDNIS(IL,L,IS))*ARAT)
              ELSE
                CORU=1.
              ENDIF
!
!             * B-TERM IN KOEHLER EQUATION (FOR SOLUTE EFFECT).
!
              CIBK(IL,L,IS)=CIKAPPA(IL,L,IS)*CORU*CIDRYRB(IS)**3
            ENDIF
          END DO
          END DO
          END DO
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!     * ASSOCIATE APPROPRIATE LOOK-UP DATA TABLES.
!
      IF ( ISEXTB > 0 ) THEN
        CALL CNRAT(CERAT,CEIRD,AK,CEBK,ILGA,LEVA,ISEXTB)
      ENDIF
      IF ( ISINTB > 0 ) THEN
        CALL CNRAT(CIRAT,CIIRD,AK,CIBK,ILGA,LEVA,ISINTB)
      ENDIF
!
!-----------------------------------------------------------------------
!     * DIFFUSIVITY OF WATER VAPOUR IN AIR (IN M**2/S).
!
      TMP1=(TEMP/273.)**1.94
      DV=(0.211E-04/(PRES*1.E-05))*TMP1
      TMP3=2.*YPI*WH2O/(RGASM*TEMP)
      TMP1=SQRT(TMP3)
      DVX=(DV/ALPHC)*TMP1
!
!     * THERMAL CONDUCTIVITY OF AIR (J/M/S/K).
!
      TK=(23.84+0.071*TEMP)*1.E-03
      TKX=(TK/(ALPHT*RHOA*CPRES))*TMP1*SQRT(WA/WH2O)
!
!     * SATURATION VAPOUR PRESSURE OVER WATER.
!
      IF ( KSATF ) THEN
        IF ( KDBL ) THEN
          TMP3=RW1+RW2/TEMP
          TMP1=EXP(TMP3)
          TMP3=RW3
          TMP2=TEMP**TMP3
        ELSE
          TMP3=RW1+RW2/TEMP
          TMP1=EXP(TMP3)
          TMP3=RW3
          TMP2=TEMP**TMP3
        ENDIF
        ESW=1.E+02*TMP1*TMP2
      ELSE
        DO L=1,LEVA
        DO IL=1,ILGA
          ESW(IL,L)=1.E+02*EXP(PPA-PPB/TEMP(IL,L))
        ENDDO
        ENDDO
      ENDIF
!
!     * MODIFIED DIFFUSIVITY, THERMAL CONDUCTIVITY, AND GROWTH TIME
!     * SCALE PARAMETER.
!
      IF ( ISEXTB > 0 ) THEN
        CALL CNTSCL(CETSCL,CEDVP,CETKP,CEWETRB,DVX,TKX,ESW,TEMP,DV,TK, &
                    ILGA,LEVA,ISEXTB)
      ENDIF
      IF ( ISINTB > 0 ) THEN
        CALL CNTSCL(CITSCL,CIDVP,CITKP,CIWETRB,DVX,TKX,ESW,TEMP,DV,TK, &
                    ILGA,LEVA,ISINTB)
      ENDIF
!
!-----------------------------------------------------------------------
!       * CALCULATE SUPERSATURATION AND PARTICLE SIZES.
!
      IF ( KCALC ) THEN
!
!       * CALCULATE INITIAL AMOUNT OF CONDENSED WATER.
!
        CALL SCWATM(QRC,CEWMASS,CIWMASS,CEN0,CEPSI,CEWETRB,CEMOM2, &
                    CEMOM3,CEMOM4,CEMOM5,CEMOM6,CEMOM7,CEMOM8, &
                    CIN0,CIPSI,CIWETRB,CIMOM2,CIMOM3,CIMOM4,CIMOM5, &
                    CIMOM6,CIMOM7,CIMOM8,ILGA,LEVA)
!
!       * SAVE INTITIAL VALUES.
!
        SVI=SV
        QRCI=QRC
        TEMPI=TEMP
        IF ( ISEXTB > 0 ) CEWETRI=CEWETRB
        IF ( ISINTB > 0 ) CIWETRI=CIWETRB
!
!       * MINIMUM POSSIBLE AMOUNT OF CONDENSED WATER AT ZERO
!       * SUPERSATURATION TO ACCOUNT FOR HYGROSCOPIC GROWTH.
!
        SVT=0.
        CALL CNIGWM(FRESN,SVN,SVT,QRCT,QRV,RS,TEMP,CERC,CESC, &
                    CEWMASS,CEWETRN,CEWETRB,CIWMASS,CIWETRN, &
                    CIWETRB,CIRC,CISC,CEBK,CETSCL,CERAT,CEIRD, &
                    CEN0,CEPSI,CEMOM2,CEMOM3,CEMOM4,CEMOM5,CEMOM6, &
                    CEMOM7,CEMOM8,CIBK,CITSCL,CIRAT,CIIRD,CIN0, &
                    CIPSI,CIMOM2,CIMOM3,CIMOM4,CIMOM5,CIMOM6, &
                    CIMOM7,CIMOM8,HLST,DHDT,QR,DRDT,ZH,PRES,AK, &
                    SVI,DT,ILGA,LEVA,CEMOD1,CEMOD2,CEMOD3,CIMOD1, &
                    CIMOD2,CIMOD3)
!
!       * ADJUST TOTAL WATER MIXING RATIO AND STATIC ENERGY TO
!       * ACCOUNT FOR MINIMUM CLOUD WATER AMOUNT. THE INITIAL
!       * TOTAL WATER MIXING RATIO ONLY ACCOUNTS FOR WATER VAPOUR
!       * LOAD, WHICH IS LOWER AND MAY BE INSUFFICIENT FOR FORMATION
!       * OF A CLOUD.
!
        QRHYG=QR+QRCT
        HLSTHYG=HLST
        IF ( .NOT.KNORM ) THEN
          HLSTHYG=HLSTHYG-RL*QRCT
        ENDIF
!
!       * IMPLIED SUPERSATURATION FOR GIVEN CLOUD WATER CONTENT.
!
!        CALL CNSBND(SVT,QRVN,RSN,TEMPN,QRC,HLSTHYG,DHDT,QRHYG,
!     1              DRDT,ZH,PRES,DT,ILGA,LEVA)
!
!       * ESTIMATED RANGE OF SUPERSATURATION (FROM SVL TO SVU).
!       * AND INITIAL GUESS FOR SUPERSATURATION (SVT).
!
!        WHERE ( SVT < -YSMALL )
!          SVL=-0.01
!          SVU=SVL*0.1
!        ELSEWHERE ( SVT > YSMALL )
!          SVU=SVT
!          SVL=SVT*0.1
!        ELSEWHERE
!          SVU=0.01
!          SVL=-0.01
!        ENDWHERE
!
!       * SELECT INITIAL SUPERSATURATION AS BEST GUESS FOR NEW VALUE
!       * AND DETERMINE RANGE OF VALUES BY SCALING BEST GUESS.
!
        SVT=MAX(SVI,1.E-04)
        SCLF=10.
!        SCLF=3.
        SVL=SVT/SCLF
        SVU=SVT*SCLF
!
!       * ENSURE PLAUSIBLE RANGE.
!
        WHERE ( .NOT. (SVT-SVL > YSMALL .AND. SVU-SVT > YSMALL) )
          FRAC=0.5
          SVT=SVL*FRAC+(1.-FRAC)*SVU
        ENDWHERE
        WHERE ( ABS(SVT) < YSMALL )
          SVT=YSMALL
        ENDWHERE
!
!       * RESIDUAL FOR INITIALLY HIGHEST SUPERSATURATION.
!
        IF ( ISEXTB > 0 ) CEWETRB=CEWETRI
        IF ( ISINTB > 0 ) CIWETRB=CIWETRI
        CALL CNIGWM(FRESU,SVN,SVU,QRCU,QRV,RS,TEMP,CERC,CESC, &
                    CEWMASS,CEWETRN,CEWETRB,CIWMASS,CIWETRN, &
                    CIWETRB,CIRC,CISC,CEBK,CETSCL,CERAT,CEIRD, &
                    CEN0,CEPSI,CEMOM2,CEMOM3,CEMOM4,CEMOM5,CEMOM6, &
                    CEMOM7,CEMOM8,CIBK,CITSCL,CIRAT,CIIRD,CIN0, &
                    CIPSI,CIMOM2,CIMOM3,CIMOM4,CIMOM5,CIMOM6, &
                    CIMOM7,CIMOM8,HLSTHYG,DHDT,QRHYG,DRDT,ZH,PRES,AK, &
                    SVI,DT,ILGA,LEVA,CEMOD1,CEMOD2,CEMOD3,CIMOD1, &
                    CIMOD2,CIMOD3)
!
!       * RESIDUAL FOR INITIALLY LOWEST SUPERSATURATION.
!
        IF ( ISEXTB > 0 ) CEWETRB=CEWETRI
        IF ( ISINTB > 0 ) CIWETRB=CIWETRI
        CALL CNIGWM(FRESL,SVN,SVL,QRCL,QRV,RS,TEMP,CERC,CESC, &
                    CEWMASS,CEWETRN,CEWETRB,CIWMASS,CIWETRN, &
                    CIWETRB,CIRC,CISC,CEBK,CETSCL,CERAT,CEIRD, &
                    CEN0,CEPSI,CEMOM2,CEMOM3,CEMOM4,CEMOM5,CEMOM6, &
                    CEMOM7,CEMOM8,CIBK,CITSCL,CIRAT,CIIRD,CIN0, &
                    CIPSI,CIMOM2,CIMOM3,CIMOM4,CIMOM5,CIMOM6, &
                    CIMOM7,CIMOM8,HLSTHYG,DHDT,QRHYG,DRDT,ZH,PRES,AK, &
                    SVI,DT,ILGA,LEVA,CEMOD1,CEMOD2,CEMOD3,CIMOD1, &
                    CIMOD2,CIMOD3)
!
!       * BI-SECTION OF RESIDUAL BASED ON THE PREVIOUS 2 RESULTS
!       * TO OBTAIN IMPROVED ESTIMATE OF SUPERSATURATION (ROOT FINDING).
!       * BASED ON WEIGHTED MEAN SO THAT THE IMPROVED ESTIMATE IS
!       * CLOSER TO THE SUPERSATURATION THAT PRODUCES THE SMALLEST
!       * RESIDUAL. ALLOW NEW ESTIMATE TO EXCEED THE INITIAL RANGE
!       * IF THE ROOT IS OUTSIDE THAT RANGE. THIS WILL BE THE CASE IF
!       * THE AMOUNT OF CONDENSED WATER FOR THE LOWER VALUE OF THE
!       * SUPERSATURATION EXCEEDS THE ADIABATIC VALUE OR IF THE
!       * RESIDUAL DOES NOT SWITCH SIGN.
!
        WHERE ( KADIAB .AND. QRCL >= QRCI .AND. SVL > YSMALL )
          SVT=(SVL/SVU)*SVL
        ELSEWHERE ( FRESU*FRESL > YSMALL )
          WHERE ( FRESL > YSMALL )
            SVT=(SVU/SVL)*SVU
          ELSEWHERE ( FRESL < -YSMALL )
            SVT=(SVL/SVU)*SVL
          ENDWHERE
        ENDWHERE
        WHERE ( ABS(SVT) < YSMALL )
          SVT=YSMALL
        ENDWHERE
!
!       * ITERATIONS TO FURTHER IMPROVE ESTIMATE OF SUPERSATURATION AND
!       * CORRESPONDING CONDENSATE DISTRIBUTION. THE MINIMIZATION OF THE
!       * RESIDUAL IS EQUIVALENT TO INCREASING THE ACCURACY OF THE
!       * WATER BUDGET.
!
        DO IIT=1,ITMAX
!
!         * RESIDUAL FROM TEMPORAL EVOLUTION OF CONDENSATE DISTRIBUTION.
!
          IF ( ISEXTB > 0 ) CEWETRB=CEWETRI
          IF ( ISINTB > 0 ) CIWETRB=CIWETRI
          CALL CNIGWM(FRESN,SVN,SVT,QRCN,QRV,RS,TEMP,CERC,CESC, &
                      CEWMASS,CEWETRN,CEWETRB,CIWMASS,CIWETRN, &
                      CIWETRB,CIRC,CISC,CEBK,CETSCL,CERAT,CEIRD, &
                      CEN0,CEPSI,CEMOM2,CEMOM3,CEMOM4,CEMOM5,CEMOM6, &
                      CEMOM7,CEMOM8,CIBK,CITSCL,CIRAT,CIIRD,CIN0, &
                      CIPSI,CIMOM2,CIMOM3,CIMOM4,CIMOM5,CIMOM6, &
                      CIMOM7,CIMOM8,HLSTHYG,DHDT,QRHYG,DRDT,ZH,PRES,AK, &
                      SVI,DT,ILGA,LEVA,CEMOD1,CEMOD2,CEMOD3,CIMOD1, &
                      CIMOD2,CIMOD3)
          IMODN=0
          DO IS=1,ISEXTB
            WHERE ( CEMOD2(:,:,IS)==2 ) IMODN(:,:)=1
          ENDDO
          DO IS=1,ISINTB
            WHERE ( CIMOD2(:,:,IS)==2 ) IMODN(:,:)=1
          ENDDO
!
!         * ESTABLISH CORRECT ORDER OF THE THREE SUPERSATURATION POINTS
!         * SO THAT SVL < SVT < SVU.
!
          WHERE ( SVT < SVL )
            SVTT=SVL
            FRESNT=FRESL
            QRCT=QRCL
            SVL=SVT
            FRESL=FRESN
            QRCL=QRCN
            SVT=SVTT
            FRESN=FRESNT
            QRCN=QRCT
          ELSEWHERE ( SVT > SVU )
            SVTT=SVU
            FRESNT=FRESU
            QRCT=QRCU
            SVU=SVT
            FRESU=FRESN
            QRCU=QRCN
            SVT=SVTT
            FRESN=FRESNT
            QRCN=QRCT
          ENDWHERE
!
!         * UPDATE LOCAL BOUNDS OF THE SUPERSATURATION INTERVAL BY
!         * REPLACING ONE OF THE BOUNDS FROM PREVIOUS CALCULATIONS
!         * WITH THE CURRENT RESULT.
!
          WHERE ( FRESN*FRESL < -YSMALL )
            SVU=SVT
            FRESU=FRESN
            QRCU=QRCN
          ELSEWHERE ( FRESN*FRESU < -YSMALL )
            SVL=SVT
            FRESL=FRESN
            QRCL=QRCN
          ELSEWHERE ( FRESL > YSMALL )
            SVL=SVT
            FRESL=FRESN
            QRCL=QRCN
          ELSEWHERE
            SVU=SVT
            FRESU=FRESN
            QRCU=QRCN
          ENDWHERE
!
!         * NEW GUESS FOR OPTIMAL SUPERSATURATION VALUE.
!
          WHERE ( KADIAB .AND. QRCL >= QRCI .AND. SVL > YSMALL )
            SVT=(SVL/SVU)*SVL
          ELSEWHERE ( FRESU*FRESL < YSMALL )
            FRAC=0.5
            SVT=SVL*FRAC+(1.-FRAC)*SVU
          ELSEWHERE ( FRESL > YSMALL )
            SVT=(SVU/SVL)*SVU
          ELSEWHERE ( FRESL < -YSMALL )
            SVT=(SVL/SVU)*SVL
          ENDWHERE
          WHERE ( ABS(SVT) < YSMALL )
            SVT=YSMALL
          ENDWHERE
        ENDDO
      ELSE
        SVT=SVSPEC
        SV=SVSPEC
      ENDIF
!
!-----------------------------------------------------------------------
!     * FINAL SUPERSATURATION AND LIQUID WATER CONTENT.
!
      IF ( KCALC ) SV=SVT
      QRC=QRCN
!
!-----------------------------------------------------------------------
!     * RESET RESULTS TO INITIAL VALUES FOR SUBSATURATED CONDITIONS.
!
      IF ( ISEXTB > 0 ) THEN
        DO IS=1,ISEXTB
          WHERE ( SVN < 0. )
            CEWETRB(:,:,IS)=CEWETRI(:,:,IS)
          ENDWHERE
        ENDDO
      ENDIF
      IF ( ISINTB > 0 ) THEN
        DO IS=1,ISINTB
          WHERE ( SVN < 0. )
            CIWETRB(:,:,IS)=CIWETRI(:,:,IS)
          ENDWHERE
        ENDDO
      ENDIF
      WHERE ( SVN < 0. )
        SV=0.
        QRC=QRCI
      ENDWHERE
!
!-----------------------------------------------------------------------
!     * DIAGNOSIS OF TRUNCATION ERROR OF THE ITERATIVE METHOD.
!
      CONVF=FRESN/MAX(SV,YSMALL)
!
!     * OVERWRITE RESULTS FLAGGED AS INVALID.
!
      WHERE ( .NOT.LMSK )
        QRC=QRCI
        TEMP=TEMPI
        RS=YNA
        SV=YNA
        QRV=YNA
        CONVF=YNA
      ENDWHERE
      IF ( ISEXT > 0 ) THEN
        DO IL=1,ILGA
        DO L=1,LEVA
          IF ( .NOT.LMSK(IL,L) ) THEN
            DO IS=1,ISEXTB
              CEWETRB(IL,L,IS)=CEWETRI(IL,L,IS)
              CEMOD1 (IL,L,IS)=INA
              CEMOD2 (IL,L,IS)=INA
              CEMOD3 (IL,L,IS)=INA
              CERC   (IL,L,IS)=YNA
              CESC   (IL,L,IS)=YNA
            ENDDO
            DO IS=1,ISEXT
              CEWMASS(IL,L,IS)=YNA
            ENDDO
          ENDIF
        ENDDO
        ENDDO
      ENDIF
      IF ( ISINT > 0 ) THEN
        DO IL=1,ILGA
        DO L=1,LEVA
          IF ( .NOT.LMSK(IL,L) ) THEN
            DO IS=1,ISINTB
              CIWETRB(IL,L,IS)=CIWETRI(IL,L,IS)
              CIMOD1 (IL,L,IS)=INA
              CIMOD2 (IL,L,IS)=INA
              CIMOD3 (IL,L,IS)=INA
              CIRC   (IL,L,IS)=YNA
              CISC   (IL,L,IS)=YNA
            ENDDO
            DO IS=1,ISINT
              CIWMASS(IL,L,IS)=YNA
            ENDDO
          ENDIF
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * DEALLOCATE TEMPORARY ARRAYS.
!
      IF ( ISEXTB > 0 ) THEN
        DEALLOCATE(CEWETRN)
        DEALLOCATE(CEWETRI)
        DEALLOCATE(CEBK)
        DEALLOCATE(CEDVP)
        DEALLOCATE(CETKP)
        DEALLOCATE(CERAT)
        DEALLOCATE(CEIRD)
      ENDIF
      IF ( ISINTB > 0 ) THEN
        DEALLOCATE(CIWETRN)
        DEALLOCATE(CIWETRI)
        DEALLOCATE(CIBK)
        DEALLOCATE(CIDVP)
        DEALLOCATE(CITKP)
        DEALLOCATE(CIRAT)
        DEALLOCATE(CIIRD)
      ENDIF
      DEALLOCATE(AK)
      DEALLOCATE(SFCTW)
      DEALLOCATE(DV)
      DEALLOCATE(DVX)
      DEALLOCATE(TK)
      DEALLOCATE(TKX)
      DEALLOCATE(TMP1)
      DEALLOCATE(TMP2)
      DEALLOCATE(TMP3)
      DEALLOCATE(ESW)
      DEALLOCATE(SVI)
      DEALLOCATE(SVN)
      DEALLOCATE(RSN)
      DEALLOCATE(TEMPN)
      DEALLOCATE(QRVN)
      DEALLOCATE(QRHYG)
      DEALLOCATE(HLSTHYG)
      DEALLOCATE(FRESN)
      DEALLOCATE(FRESL)
      DEALLOCATE(FRESU)
      DEALLOCATE(FRESNT)
      DEALLOCATE(SVTT)
      DEALLOCATE(SVT)
      DEALLOCATE(SVL)
      DEALLOCATE(SVU)
      DEALLOCATE(TEMPI)
      DEALLOCATE(QRCI)
      DEALLOCATE(QRCU)
      DEALLOCATE(QRCL)
      DEALLOCATE(QRCN)
      DEALLOCATE(QRCT)
      DEALLOCATE(FRAC)
      DEALLOCATE(IMODN)
!
      END SUBROUTINE CNGRW
