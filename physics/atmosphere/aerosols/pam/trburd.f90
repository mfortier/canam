      SUBROUTINE TRBURD (VOABRD,VBCBRD,VASBRD,VMDBRD,VSSBRD,XROW,IAIND, &
                         DSHJ,PRESSG,GRAV,NTRAC,NTRACP,IL1,IL2,ILG,ILEV, &
                         ILEVP1,MSG)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!
!
!     HISTORY:
!     --------
!     * FEB 10/2015 - K.VONSALZEN  CORRECTION OF VERTICAL LEVELS
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      INTEGER, INTENT(IN), DIMENSION(NTRACP) :: IAIND
      REAL, INTENT(IN), DIMENSION(ILG) :: PRESSG
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: DSHJ
      REAL, INTENT(IN), DIMENSION(ILG,ILEVP1,NTRAC) :: XROW
      REAL, INTENT(OUT), DIMENSION(ILG) :: VOABRD,VBCBRD,VASBRD, &
                                           VMDBRD,VSSBRD
!
!     * INITIALIZATION.
!
      IOFF=ILEVP1-ILEV
      VOABRD=0.
      VBCBRD=0.
      VASBRD=0.
      VMDBRD=0.
      VSSBRD=0.
!
!     * ORGANIC CARBON BURDEN.
!
      IF ( ISEXTOC > 0 ) THEN
        DO ISX=1,ISEXTOC
          IS=IEXOC(ISX)
          IMLEX=IAIND(SEXTF%ISAER(IS)%ISM)
          DO IL=IL1,IL2
          DO L=MSG+1,ILEV
            VOABRD(IL)=VOABRD(IL) &
                      +XROW(IL,L+IOFF,IMLEX)*PRESSG(IL)*DSHJ(IL,L)/GRAV
          ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTOC > 0 ) THEN
        DO ISX=1,ISINTOC
          IS=IINOC(ISX)
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTOC ) THEN
              IMLIN=IAIND(SINTF%ISAER(IS)%ISM(IST))
              DO IL=IL1,IL2
              DO L=MSG+1,ILEV
                VOABRD(IL)=VOABRD(IL) &
                      +XROW(IL,L+IOFF,IMLIN)*PRESSG(IL)*DSHJ(IL,L)/GRAV
              ENDDO
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!     * BLACK CARBON BURDEN.
!
      IF ( ISEXTBC > 0 ) THEN
        DO ISX=1,ISEXTBC
          IS=IEXBC(ISX)
          IMLEX=IAIND(SEXTF%ISAER(IS)%ISM)
          DO IL=IL1,IL2
          DO L=MSG+1,ILEV
            VBCBRD(IL)=VBCBRD(IL) &
                      +XROW(IL,L+IOFF,IMLEX)*PRESSG(IL)*DSHJ(IL,L)/GRAV
          ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTBC > 0 ) THEN
        DO ISX=1,ISINTBC
          IS=IINBC(ISX)
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTBC ) THEN
              IMLIN=IAIND(SINTF%ISAER(IS)%ISM(IST))
              DO IL=IL1,IL2
              DO L=MSG+1,ILEV
                VBCBRD(IL)=VBCBRD(IL) &
                      +XROW(IL,L+IOFF,IMLIN)*PRESSG(IL)*DSHJ(IL,L)/GRAV
              ENDDO
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!     * SULPHATE BURDEN.
!
      IF ( ISEXTSO4 > 0 ) THEN
        DO ISX=1,ISEXTSO4
          IS=IEXSO4(ISX)
          IMLEX=IAIND(SEXTF%ISAER(IS)%ISM)
          DO IL=IL1,IL2
          DO L=MSG+1,ILEV
            VASBRD(IL)=VASBRD(IL) &
                      +XROW(IL,L+IOFF,IMLEX)*PRESSG(IL)*DSHJ(IL,L)/GRAV
          ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTSO4 > 0 ) THEN
        DO ISX=1,ISINTSO4
          IS=IINSO4(ISX)
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTSO4 ) THEN
              IMLIN=IAIND(SINTF%ISAER(IS)%ISM(IST))
              DO IL=IL1,IL2
              DO L=MSG+1,ILEV
                VASBRD(IL)=VASBRD(IL) &
                      +XROW(IL,L+IOFF,IMLIN)*PRESSG(IL)*DSHJ(IL,L)/GRAV
              ENDDO
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!     * MINERAL DUST BURDEN.
!
      IF ( ISEXTMD > 0 ) THEN
        DO ISX=1,ISEXTMD
          IS=IEXMD(ISX)
          IMLEX=IAIND(SEXTF%ISAER(IS)%ISM)
          DO IL=IL1,IL2
          DO L=MSG+1,ILEV
            VMDBRD(IL)=VMDBRD(IL) &
                      +XROW(IL,L+IOFF,IMLEX)*PRESSG(IL)*DSHJ(IL,L)/GRAV
          ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTMD > 0 ) THEN
        DO ISX=1,ISINTMD
          IS=IINMD(ISX)
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTMD ) THEN
              IMLIN=IAIND(SINTF%ISAER(IS)%ISM(IST))
              DO IL=IL1,IL2
              DO L=MSG+1,ILEV
                VMDBRD(IL)=VMDBRD(IL) &
                      +XROW(IL,L+IOFF,IMLIN)*PRESSG(IL)*DSHJ(IL,L)/GRAV
              ENDDO
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
!     * SEA SALT BURDEN.
!
      IF ( ISEXTSS > 0 ) THEN
        DO ISX=1,ISEXTSS
          IS=IEXSS(ISX)
          IMLEX=IAIND(SEXTF%ISAER(IS)%ISM)
          DO IL=IL1,IL2
          DO L=MSG+1,ILEV
            VSSBRD(IL)=VSSBRD(IL) &
                      +XROW(IL,L+IOFF,IMLEX)*PRESSG(IL)*DSHJ(IL,L)/GRAV
          ENDDO
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTSS > 0 ) THEN
        DO ISX=1,ISINTSS
          IS=IINSS(ISX)
          DO IST=1,SINTF%ISAER(IS)%ITYPT
            KX=SINTF%ISAER(IS)%ITYP(IST)
            IF ( KX == KINTSS ) THEN
              IMLIN=IAIND(SINTF%ISAER(IS)%ISM(IST))
              DO IL=IL1,IL2
              DO L=MSG+1,ILEV
                VSSBRD(IL)=VSSBRD(IL) &
                      +XROW(IL,L+IOFF,IMLIN)*PRESSG(IL)*DSHJ(IL,L)/GRAV
              ENDDO
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDIF
!
      END SUBROUTINE TRBURD
