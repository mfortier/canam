      SUBROUTINE CDNCEM(EMCDNC,PEMAS,PEPSI,PIMAS,PIPSI,PIFRC, &
                        RHOA,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CLOUD DROPLET NUMBER CONCENTRATION BASED ON SIMPLE EMPIRICAL
!     RELATIONSHIP BETWEEN CDNC AND SULPHATE CONCENTRATION.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEWLY INSTALLED.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA)             :: RHOA
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT)      :: PEMAS,PEPSI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT)      :: PIMAS,PIPSI
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA)            :: EMCDNC
      REAL, DIMENSION(ILGA,LEVA)                         :: SULPH,TERM
!
!-----------------------------------------------------------------------
!     * SULPHATE CONCENTRATION (MG/M**3).
!
      SULPH=0.
      IF ( KINTSO4 > 0 ) THEN
        DO IS=1,ISINTSO4
          WHERE ( ABS(PIPSI(:,:,IINSO4(IS))-YNA) > YTINY )
            SULPH=SULPH+PIMAS(:,:,IINSO4(IS)) &
                                         *PIFRC(:,:,IINSO4(IS),KINTSO4)
          ENDWHERE
        ENDDO
      ENDIF
      IF ( KEXTSO4 > 0 ) THEN
        DO IS=1,ISEXTSO4
          WHERE ( ABS(PEPSI(:,:,IEXSO4(IS))-YNA) > YTINY )
            SULPH=SULPH+PEMAS(:,:,IEXSO4(IS))
          ENDWHERE
        ENDDO
      ENDIF
      WHERE ( SULPH > YSMALL )
        SULPH=SULPH*WSO4/WAMSUL*RHOA*1.E+09
      ELSEWHERE
        SULPH=0.
      ENDWHERE
!
!-----------------------------------------------------------------------
!     * CDNC CONCENTRATION (PER KG OF AIR).
!
      TERM=0.3
      EMCDNC=SULPH**TERM
      EMCDNC=EMCDNC*66.*1.E+06/RHOA
!
      END SUBROUTINE CDNCEM
