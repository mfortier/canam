      SUBROUTINE PGRWTH0 (TNUM,RESN,I0L,I0R,DP0L,DP0R,DP0S, &
                          PN0,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PARTICLE GROWTH FOR PARTICLE NUMBER.
!
!     IT IS ASSUMED THAT SECTIONS INITIALLY HAVE THE SAME SIZE AND THAT
!     GROWTH LEADS TO A AN EQUAL OR SMALLER WIDTH OF EACH SECTION.
!     THE SUBROUTINE TREATS GROWTH IN PARTICLE SIZE SPACE BASED ON
!     A LAGRANGIAN APPROACH.
!
!     HISTORY:
!     --------
!     * FEB 20/2005 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: TNUM
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: RESN
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: PN0
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: DP0L,DP0R,DP0S
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: I0L,I0R
!
!-----------------------------------------------------------------------
!     * INITIALIZATION.
!
      TNUM=0.
!
!-----------------------------------------------------------------------
!     * CALCULATE NUMBER AND MASS IN EACH SECTION.
!
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
!
!        * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE LEFT-HAND
!        * SIDE PART OF THE MODIFIED SECTION.
!
         IS=I0L(IL,L,ISI)
         IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                                                                )  THEN
!
!          * NUMBER SIZE DISTRIBUTION FROM N(PHI(T))=N(PHI(T=0))
!
           TNUM(IL,L,IS)=TNUM(IL,L,IS)+PN0(IL,L,ISI)*DP0L(IL,L,ISI)
         ENDIF
!
!        * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE RIGHT-HAND
!        * SIDE PART OF THE MODIFIED SECTION.
!
         IS=I0R(IL,L,ISI)
         IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
             .AND. I0L(IL,L,ISI) /= IS                          )  THEN
!
!          * NUMBER SIZE DISTRIBUTION FROM N(PHI(T))=N(PHI(T=0))
!
           TNUM(IL,L,IS)=TNUM(IL,L,IS)+PN0(IL,L,ISI)*DP0R(IL,L,ISI)
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * RESIDUUM DUE TO PARTICLES GROWING TO SIZES LARGER THAN
!     * THE SPECTRUM CUTOFF.
!
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         IF ( (I0R(IL,L,ISI) == (ISEC+1) .OR. I0L(IL,L,ISI) == 0) &
            .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!          * NUMBER SIZE DISTRIBUTION FROM N(PHI(T))=N(PHI(T=0))
!
           RESN(IL,L)=RESN(IL,L)+PN0(IL,L,ISI)*DP0S(IL,L,ISI)
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
      END SUBROUTINE PGRWTH0
