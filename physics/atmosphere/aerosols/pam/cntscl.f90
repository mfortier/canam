      SUBROUTINE CNTSCL(TSCL,DVP,TKP,WETRB,DVX,TKX,ESW,TEMP,DV,TK, &
                        ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PARAMETERS RELEVANT TO DROPLET GROWTH, I.E. DROPLET GROWTH TIME
!     SCALE, DIFFUSIVITY, AND THERMAL CONDUCTIVITY.
!
!     HISTORY:
!     --------
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: ESW,TEMP,DV,DVX,TK,TKX
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: WETRB
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: TSCL,DVP,TKP
      REAL, ALLOCATABLE, DIMENSION(:,:) :: TERM1,TERM2
!
!-----------------------------------------------------------------------
!
!     * ALLOCATE WORK ARRAYS.
!
      ALLOCATE(TERM1(ILGA,LEVA))
      ALLOCATE(TERM2(ILGA,LEVA))
!
!-----------------------------------------------------------------------
      DO IS=1,ISEC
!
!       * MODIFIED DIFFUSIVITY AND THERMAL CONDUCTIVITY.
!
        DVP(:,:,IS)=DV(:,:)/(1.+DVX(:,:)/WETRB(:,:,IS))
        TKP(:,:,IS)=TK(:,:)/(1.+TKX(:,:)/WETRB(:,:,IS))
!
!       * GROWTH TIME SCALE PARAMETER (SV-SP)/(R*DR/DT).
!
        TERM1=RHOH2O*RGASM*TEMP(:,:)/(ESW(:,:)*DVP(:,:,IS)*WH2O)
        TERM2=(RL*RHOH2O/(TKP(:,:,IS)*TEMP(:,:))) &
                                         *(RL*WA/(RGASM*TEMP(:,:))-1.)
        TSCL(:,:,IS)=TERM1+TERM2
      ENDDO
!
!-----------------------------------------------------------------------
!     * DEALLOCATE WORK ARRAYS.
!
      DEALLOCATE(TERM1)
      DEALLOCATE(TERM2)
!
      END SUBROUTINE CNTSCL
