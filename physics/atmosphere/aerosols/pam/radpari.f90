      SUBROUTINE RADPARI(PINUM,PIMAS,PIN0,PIPHI0,PIPSI,PIPHIS0,PIDPHI0, &
                         PIWETRC,PIDDN,PIRE,PIVE,PILOAD, &
                         PIFRC,FR1,FR2,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     RADIATION PARAMETERS (LOAD, EFFECTIVE RADIUS, EFFECTIVE VARIANCE).
!
!     HISTORY:
!     --------
!     * NOV 21/2012 - K. VON SALZEN    REMOVE EXTERNALLY MIXED AEROSOL
!     * JAN 17/2012 - K. VON SALZEN    BASED ON SSRAD
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
      REAL, INTENT(IN),  DIMENSION(ILGA,LEVA,ISAINT) :: &
                                      PINUM,PIMAS,PIWETRC,PIDDN, &
                                      PIN0,PIPSI,PIPHI0,PIPHIS0,PIDPHI0
      REAL, INTENT(IN),  DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIFRC
      REAL, INTENT(OUT),  DIMENSION(ILGA,LEVA) :: PIRE,PIVE,PILOAD,FR1, &
                                                  FR2
      REAL,  DIMENSION(ILGA,LEVA) :: TRE,TVE,TLOAD,ATMP1,ATMP2,ATMP3
!
!-----------------------------------------------------------------------
!     * INITIALIZATIONS.
!
      IF ( KINT > 0 ) THEN
        FR1=1.
        FR2=0.
        PILOAD=0.
        PIRE=0.
        PIVE=0.
      ENDIF
!
!     * INTERNALLY MIXED AEROSOL.
!
      IF ( KINT > 0 ) THEN
        CALL EFFRV(PINUM,PIMAS,PIN0,PIPHI0,PIPSI,PIPHIS0,PIDPHI0, &
                   PIWETRC,PIDRYRC,PIDDN,TLOAD,TRE,TVE,ILGA,LEVA, &
                   ISAINT)
        PILOAD=TLOAD
        PIRE=TRE
        PIVE=TVE
!
!       * MASS FRACTIONS FOR AMMONIUM SULPHATE AND BLACK CARBON.
!
        ATMP1=0.
        ATMP2=0.
        ATMP3=0.
        DO IS=1,ISAINT
          ATMP1=ATMP1+PIMAS(:,:,IS)
        ENDDO
        IF ( KINTSO4 > 0 ) THEN
          DO IS=1,ISAINT
            ATMP2=ATMP2+PIMAS(:,:,IS)*PIFRC(:,:,IS,KINTSO4)
          ENDDO
        ENDIF
        IF ( KINTBC > 0 ) THEN
          DO IS=1,ISAINT
            ATMP3=ATMP3+PIMAS(:,:,IS)*PIFRC(:,:,IS,KINTBC)
          ENDDO
        ENDIF
        WHERE ( ATMP1 > YTINY )
          FR1=MAX(ATMP2/ATMP1,0.)
          FR2=MAX(ATMP3/ATMP1,0.)
        ENDWHERE
        ATMP1=FR1+FR2
        WHERE ( ATMP1 > .9999 )
          ATMP2=1./ATMP1
          FR1=FR1*ATMP2
          FR2=FR2*ATMP2
        ENDWHERE
      ENDIF
!
      END SUBROUTINE RADPARI
