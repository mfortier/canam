      SUBROUTINE PGRWTHS (TMAS,RESM,FRC,I0L,I0R, &
                          DP0L,DP1L,DP2L,DP3L,DM1L,DM2L,DM3L, &
                          DP0R,DP1R,DP2R,DP3R,DM1R,DM2R,DM3R, &
                          DP0S,DP1S,DP2S,DP3S,DM1S,DM2S,DM3S, &
                          CGR1,CGR2,PN0,DRYDN,ILGA,LEVA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PARTICLE GROWTH FROM CONDENSATION OF AEROSOL MASS WITH AN
!     ASSOCIATED CHANGE IN PARTICLE RADIUS R -> R+C/R.
!
!     IT IS ASSUMED THAT SECTIONS INITIALLY HAVE THE SAME SIZE AND THAT
!     GROWTH LEADS TO A AN EQUAL OR SMALLER WIDTH OF EACH SECTION.
!     THE SUBROUTINE TREATS GROWTH IN PARTICLE SIZE SPACE BASED ON
!     A LAGRANGIAN APPROACH.
!
!     HISTORY:
!     --------
!     * FEB 19/2010 - K.VONSALZEN   CORRECT USE OF MASS FRACTION
!     * FEB 20/2005 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL(R8), INTENT(OUT), DIMENSION(ILGA,LEVA,ISEC) :: TMAS
      REAL, INTENT(INOUT), DIMENSION(ILGA,LEVA) :: RESM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                    CGR1,CGR2,PN0,DRYDN,FRC
      REAL(R8), INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: &
                                    DP0L,DP1L,DP2L,DP3L,DM1L,DM2L,DM3L, &
                                    DP0R,DP1R,DP2R,DP3R,DM1R,DM2R,DM3R, &
                                    DP0S,DP1S,DP2S,DP3S,DM1S,DM2S,DM3S
      INTEGER, INTENT(IN) :: ILGA,LEVA,ISEC
      INTEGER, INTENT(IN), DIMENSION(ILGA,LEVA,ISEC) :: I0L,I0R
      REAL(R8) :: C0,C1,C2,TP0L,TP1L,TP2L,TP3L,TP0R,TP1R,TP2R,TP3R, &
                  TP0S,TP1S,TP2S,TP3S,TM1L,TM2L,TM3L,TM1R,TM2R,TM3R, &
                  TM1S,TM2S,TM3S
!
!-----------------------------------------------------------------------
!     * INITIALIZATION.
!
      TMAS=0.
!
!-----------------------------------------------------------------------
!     * CALCULATE NUMBER AND MASS IN EACH SECTION.
!
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
!
!        * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE LEFT-HAND
!        * SIDE PART OF THE MODIFIED SECTION.
!
         IS=I0L(IL,L,ISI)
         IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
                                                                )  THEN
!
!          * MASS SIZE DISTRIBUTION FROM
!          * M(PHI(T))=F*(R(T=0)+C/R(T=0))**3*N(PHI(T=0))
!
           CT=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*YCNST
           C0=CT*FRC(IL,L,ISI)
           C1=CGR1(IL,L,ISI)
           C2=CGR2(IL,L,ISI)
           TP0L=DP0L(IL,L,ISI)
           TP1L=DP1L(IL,L,ISI)
           TP2L=DP2L(IL,L,ISI)
           TP3L=DP3L(IL,L,ISI)
           TM1L=DM1L(IL,L,ISI)
           TM2L=DM2L(IL,L,ISI)
           TM3L=DM3L(IL,L,ISI)
           TMAS(IL,L,IS)=TMAS(IL,L,IS)+C0*TP3L &
                                    +CT*(3._R8*TP2L*C2+3._R8*TP1L*(C1 &
                                    +C2**2)+TP0L*C2*(6._R8*C1+C2**2) &
                                    +3._R8*TM1L*C1*(C1+C2**2) &
                                    +3._R8*TM2L*(C1**2*C2)+TM3L*C1**3)
         ENDIF
!
!        * CALCULATE CONTRIBUTION IN EACH SECTION FROM THE RIGHT-HAND
!        * SIDE PART OF THE MODIFIED SECTION.
!
         IS=I0R(IL,L,ISI)
         IF ( IS >= 1 .AND. IS <= ISEC .AND. PN0(IL,L,ISI) > YTINY &
             .AND. I0L(IL,L,ISI) /= IS                          )  THEN
!
!          * MASS SIZE DISTRIBUTION FROM
!          * M(PHI(T))=F*(R(T=0)+C/R(T=0))**3*N(PHI(T=0))
!
           CT=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*YCNST
           C0=CT*FRC(IL,L,ISI)
           C1=CGR1(IL,L,ISI)
           C2=CGR2(IL,L,ISI)
           TP0R=DP0R(IL,L,ISI)
           TP1R=DP1R(IL,L,ISI)
           TP2R=DP2R(IL,L,ISI)
           TP3R=DP3R(IL,L,ISI)
           TM1R=DM1R(IL,L,ISI)
           TM2R=DM2R(IL,L,ISI)
           TM3R=DM3R(IL,L,ISI)
           TMAS(IL,L,IS)=TMAS(IL,L,IS)+C0*TP3R &
                                    +CT*(3._R8*TP2R*C2+3._R8*TP1R*(C1 &
                                    +C2**2)+TP0R*C2*(6._R8*C1+C2**2) &
                                    +3._R8*TM1R*C1*(C1+C2**2) &
                                    +3._R8*TM2R*(C1**2*C2)+TM3R*C1**3)
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!     * RESIDUUM DUE TO PARTICLES GROWING TO SIZES LARGER THAN
!     * THE SPECTRUM CUTOFF.
!
      DO ISI=1,ISEC
      DO L=1,LEVA
      DO IL=1,ILGA
         IF ( (I0R(IL,L,ISI) == (ISEC+1) .OR. I0L(IL,L,ISI) == 0) &
            .AND. PN0(IL,L,ISI) > YTINY ) THEN
!
!          * MASS SIZE DISTRIBUTION FROM
!          * M(PHI(T))=F*(R(T=0)+C/R(T=0))**3*N(PHI(T=0))
!
           CT=PN0(IL,L,ISI)*DRYDN(IL,L,ISI)*YCNST
           C0=CT*FRC(IL,L,ISI)
           C1=CGR1(IL,L,ISI)
           C2=CGR2(IL,L,ISI)
           TP0S=DP0S(IL,L,ISI)
           TP1S=DP1S(IL,L,ISI)
           TP2S=DP2S(IL,L,ISI)
           TP3S=DP3S(IL,L,ISI)
           TM1S=DM1S(IL,L,ISI)
           TM2S=DM2S(IL,L,ISI)
           TM3S=DM3S(IL,L,ISI)
           RESM(IL,L)=RESM(IL,L)+C0*TP3S &
                                    +CT*(3._R8*TP2S*C2+3._R8*TP1S*(C1 &
                                    +C2**2)+TP0S*C2*(6._R8*C1+C2**2) &
                                    +3._R8*TM1S*C1*(C1+C2**2) &
                                    +3._R8*TM2S*(C1**2*C2)+TM3S*C1**3)
         ENDIF
      ENDDO
      ENDDO
      ENDDO
!
      END SUBROUTINE PGRWTHS
