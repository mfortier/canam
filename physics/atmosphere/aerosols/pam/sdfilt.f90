      SUBROUTINE SDFILT(FNUMO,FMASO,PEN0,PEPHI0,PEPSI,PEDDN,PEDPHI0, &
                        PEPHIS0,PEWETRC,PENUM,PEMAS,PIN0,PIPHI0,PIPSI, &
                        PIDDN,PIDPHI0,PIPHIS0,PIFRC,PIWETRC,PINUM, &
                        PIMAS,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DIAGNOSE AND FILTER SIZE DISTRIBUTIONS FOR MASS AND NUMBER
!     USING BIN SCHEME.
!
!     HISTORY:
!     --------
!     * AUG 18/2014 - K.VONSALZEN  INTRODUCTION OF SUBROUTINE DIAGSDW
!     * AUG 31/2009 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
      USE COMPAR
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: &
                                                   PEN0,PEPHI0,PEPSI, &
                                                   PEDDN,PEDPHI0, &
                                                   PEPHIS0,PEWETRC, &
                                                   PENUM,PEMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: &
                                                   PIN0,PIPHI0,PIPSI, &
                                                   PIDDN,PIDPHI0, &
                                                   PIPHIS0,PIWETRC, &
                                                   PINUM,PIMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: &
                                                   PIFRC
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISDNUM,KEXT+1) :: &
                                                   FNUMO
      REAL, INTENT(OUT), DIMENSION(ILGA,LEVA,ISDNUM,KINT+KEXT+2) :: &
                                                   FMASO
      REAL, DIMENSION(ILGA,LEVA,ISDNUM) ::         FIMOM0,FIMOM3, &
                                                   FEMOM0,FEMOM3, &
                                                   FPHIS0,FDPHI0
      REAL, DIMENSION(ILGA,LEVA,ISDNUM) ::         FINUMI
      REAL, DIMENSION(ILGA,LEVA,ISDNUM,KEXT) ::    FENUMI
      REAL, DIMENSION(ILGA,LEVA,ISDNUM,KINT+1) ::  FIMASI
      REAL, DIMENSION(ILGA,LEVA,ISDNUM,KEXT+1) ::  FEMASI
!
!-----------------------------------------------------------------------
!     * PARTICLE SIZES CORRESPONDING TO SECTION BOUNDARIES.
!
      CALL DIAGSDW(FPHIS,FDPHI0S)
      FDPHI0=FDPHI0S
      FPHIS0(:,:,1)=FPHIS
      DO IS=2,ISDNUM
        FPHIS0(:,:,IS)=FPHIS0(:,:,1)+REAL(IS-1)*FDPHI0(:,:,IS-1)
      ENDDO
!
!-----------------------------------------------------------------------
!     * INTEGRATE CONCENTRATIONS OVER SUB-SECTIONS FOR INTERNALLY
!     * MIXED AEROSOL. THIS ASSUMES THAT THE DIAGNOSTIC SIZE
!     * DISTRIBUTION HAS SMALLER SECTION WIDTHS THAN THE ORIGINAL PLA
!     * SIZE DISRTIBUTION.
!
      RMOM3=3.
      FIMOM0=0.
      FIMOM3=0.
      FINUMI=0.
      FIMASI=0.
      IF ( ISAINT > 0 ) THEN
        DO IS=1,ISAINT
        DO ISD=1,ISDNUM
        DO L=1,LEVA
        DO IL=1,ILGA
          FPH1=PIPHIS0(IL,L,IS)
          FPH2=PIPHIS0(IL,L,IS)+PIDPHI0(IL,L,IS)
          IF (    ( FPHIS0(IL,L,ISD) >= FPH1 &
              .AND. FPHIS0(IL,L,ISD) <= FPH2 ) &
            .OR. ( (FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD)) >= FPH1 &
             .AND. (FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD)) <= FPH2 ) ) THEN
            FPHIS1=MAX(FPHIS0(IL,L,ISD),FPH1)
            FPHIE1=MIN(FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD),FPH2)
            FDPHI1=MAX(FPHIE1-FPHIS1,0.)
            FIMOM0(IL,L,ISD)=FIMOM0(IL,L,ISD)+PIN0(IL,L,IS) &
                 *SDINTB0(PIPHI0(IL,L,IS),PIPSI(IL,L,IS),FPHIS1,FDPHI1)
            FIMOM3(IL,L,ISD)=FIMOM3(IL,L,ISD)+YCNST*PIDDN(IL,L,IS) &
                            *PIN0(IL,L,IS)*SDINTB(PIPHI0(IL,L,IS), &
                                    PIPSI(IL,L,IS),RMOM3,FPHIS1,FDPHI1)
!
!           * AEROSOL NUMBER.
!
            FINUMI(IL,L,ISD)=FIMOM0(IL,L,ISD)
!
!           * MASS FOR EACH SPECIES.
!
            DO IK=1,KINT
              FIMASI(IL,L,ISD,IK)=FIMOM3(IL,L,ISD)*PIFRC(IL,L,IS,IK)
            ENDDO
!
!           * AEROSOL WATER.
!
            IF ( PINUM(IL,L,IS) > YTINY .AND. PIMAS(IL,L,IS) > YTINY &
                           .AND. ABS(PIPSI(IL,L,IS)-YNA) > YTINY ) THEN
              FGR=PIWETRC(IL,L,IS)/PIDRYRC(IS)
              FIMASI(IL,L,ISD,KINT+1)=FIMASI(IL,L,ISD,KINT+1) &
                                     +(FIMOM3(IL,L,ISD)/PIDDN(IL,L,IS)) &
                                               *DNH2O*MAX(FGR**3-1.,0.)
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * INTEGRATE CONCENTRATIONS OVER SUB-SECTIONS FOR EXTERNALLY
!     * MIXED AEROSOL.
!
      FEMOM0=0.
      FEMOM3=0.
      FENUMI=0.
      FEMASI=0.
      IF ( ISAEXT > 0 ) THEN
        DO IS=1,ISAEXT
        DO ISD=1,ISDNUM
        DO L=1,LEVA
        DO IL=1,ILGA
          FPH1=PEPHIS0(IL,L,IS)
          FPH2=PEPHIS0(IL,L,IS)+PEDPHI0(IL,L,IS)
          IF (    ( FPHIS0(IL,L,ISD) >= FPH1 &
              .AND. FPHIS0(IL,L,ISD) <= FPH2 ) &
            .OR. ( (FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD)) >= FPH1 &
             .AND. (FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD)) <= FPH2 ) ) THEN
            FPHIS1=MAX(FPHIS0(IL,L,ISD),FPH1)
            FPHIE1=MIN(FPHIS0(IL,L,ISD)+FDPHI0(IL,L,ISD),FPH2)
            FDPHI1=MAX(FPHIE1-FPHIS1,0.)
            FEMOM0(IL,L,ISD)=FEMOM0(IL,L,ISD)+PEN0(IL,L,IS) &
                 *SDINTB0(PEPHI0(IL,L,IS),PEPSI(IL,L,IS),FPHIS1,FDPHI1)
            FEMOM3(IL,L,ISD)=FEMOM3(IL,L,ISD)+YCNST*PEDDN(IL,L,IS) &
                            *PEN0(IL,L,IS)*SDINTB(PEPHI0(IL,L,IS), &
                                    PEPSI(IL,L,IS),RMOM3,FPHIS1,FDPHI1)
!
!           * AEROSOL NUMBER.
!
            KX=SEXTF%ISAER(IS)%ITYP
            FENUMI(IL,L,ISD,KX)=FEMOM0(IL,L,ISD)
!
!           * MASS FOR EACH SPECIES.
!
            FEMASI(IL,L,ISD,KX)=FEMOM3(IL,L,ISD)
!
!           * AEROSOL WATER.
!
            IF ( PENUM(IL,L,IS) > YTINY .AND. PEMAS(IL,L,IS) > YTINY &
                           .AND. ABS(PEPSI(IL,L,IS)-YNA) > YTINY ) THEN
              FGR=PEWETRC(IL,L,IS)/PEDRYRC(IS)
              FEMASI(IL,L,ISD,KEXT+1)=FEMASI(IL,L,ISD,KEXT+1) &
                                     +(FEMOM3(IL,L,ISD)/PEDDN(IL,L,IS)) &
                                               *DNH2O*MAX(FGR**3-1.,0.)
            ENDIF
          ENDIF
        ENDDO
        ENDDO
        ENDDO
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!     * STRENGTH OF FILTER. NO FILTER, IF ZERO. THE PARTICLE SIZE
!     * RANGE OF THE FILTER IS ALWAYS SMALLER THAN THE SMALLEST
!     * SECTION WIDTH.
!
      IF ( ISAINT > 0 ) THEN
        NSUB=INT(AINTF%DPSTAR/FDPHI0S)
      ENDIF
      IF ( ISAEXT > 0 ) THEN
        DO KX=1,KEXT
          NSUB=MIN(NSUB,INT(AEXTF%TP(KX)%DPSTAR/FDPHI0S))
        ENDDO
      ENDIF
!      NFILT=INT(0.5*(REAL(NSUB)-1.))
      NFILT=1
!
!     * FILTER THE NUMER SIZE DISTRIBUTION. THE NUMBER OF SUB-SECTIONS
!     * OVER THE WHICH THE FILTER IS APPLIED IS 2*NFILT+1.
!
      FRAC=1./REAL(2*NFILT+1)
      DO IK=1,KEXT
      DO ISX=1,ISDNUM
        FNUMO(:,:,ISX,IK)=FENUMI(:,:,ISX,IK)
        DO ISC=1,NFILT
          IST=ISX+ISC
          IF ( IST <= ISDNUM ) THEN
            FNUMO(:,:,ISX,IK)=FNUMO(:,:,ISX,IK)+FENUMI(:,:,IST,IK)
          ENDIF
        ENDDO
        DO ISC=1,NFILT
          IST=ISX-ISC
          IF ( IST >= 1 ) THEN
            FNUMO(:,:,ISX,IK)=FNUMO(:,:,ISX,IK)+FENUMI(:,:,IST,IK)
          ENDIF
        ENDDO
        FNUMO(:,:,ISX,IK)=FNUMO(:,:,ISX,IK)*FRAC
      ENDDO
      ENDDO
      IK=KEXT+1
      DO ISX=1,ISDNUM
        FNUMO(:,:,ISX,IK)=FINUMI(:,:,ISX)
        DO ISC=1,NFILT
          IST=ISX+ISC
          IF ( IST <= ISDNUM ) THEN
            FNUMO(:,:,ISX,IK)=FNUMO(:,:,ISX,IK)+FINUMI(:,:,IST)
          ENDIF
        ENDDO
        DO ISC=1,NFILT
          IST=ISX-ISC
          IF ( IST >= 1 ) THEN
            FNUMO(:,:,ISX,IK)=FNUMO(:,:,ISX,IK)+FINUMI(:,:,IST)
          ENDIF
        ENDDO
        FNUMO(:,:,ISX,IK)=FNUMO(:,:,ISX,IK)*FRAC
      ENDDO
!
!     * FILTER THE MASS SIZE DISTRIBUTIONS.
!
      DO IK=1,KEXT+1
      DO ISX=1,ISDNUM
        FMASO(:,:,ISX,IK)=FEMASI(:,:,ISX,IK)
        DO ISC=1,NFILT
          IST=ISX+ISC
          IF ( IST <= ISDNUM ) THEN
            FMASO(:,:,ISX,IK)=FMASO(:,:,ISX,IK)+FEMASI(:,:,IST,IK)
          ENDIF
        ENDDO
        DO ISC=1,NFILT
          IST=ISX-ISC
          IF ( IST >= 1 ) THEN
            FMASO(:,:,ISX,IK)=FMASO(:,:,ISX,IK)+FEMASI(:,:,IST,IK)
          ENDIF
        ENDDO
        FMASO(:,:,ISX,IK)=FMASO(:,:,ISX,IK)*FRAC
      ENDDO
      ENDDO
      DO IK=1,KINT+1
      DO ISX=1,ISDNUM
        IKF=KEXT+IK+1
        FMASO(:,:,ISX,IKF)=FIMASI(:,:,ISX,IK)
        DO ISC=1,NFILT
          IST=ISX+ISC
          IF ( IST <= ISDNUM ) THEN
            FMASO(:,:,ISX,IKF)=FMASO(:,:,ISX,IKF)+FIMASI(:,:,IST,IK)
          ENDIF
        ENDDO
        DO ISC=1,NFILT
          IST=ISX-ISC
          IF ( IST >= 1 ) THEN
            FMASO(:,:,ISX,IKF)=FMASO(:,:,ISX,IKF)+FIMASI(:,:,IST,IK)
          ENDIF
        ENDDO
        FMASO(:,:,ISX,IKF)=FMASO(:,:,ISX,IKF)*FRAC
      ENDDO
      ENDDO
!
      END SUBROUTINE SDFILT
