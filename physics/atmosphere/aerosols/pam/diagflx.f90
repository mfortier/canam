      SUBROUTINE DIAGFLX(VOA,VBC,VSU,VMD,VSS, &
                         PEDMDT,PIDMDT,PIDFDT,PEMAS,PIMAS,PIFRC, &
                         VITRM,DT,ILGA,LEVA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DIAGNOSIS OF VERICALLY INTEGRATED SOURCES AND SINKS OF AEROSOL
!     (OC, BC, SULPHATE, MINERAL DUST, AND SEA SALT) BASED ON 3D
!     TENDENCIES.
!
!     HISTORY:
!     --------
!     * FEB 11/2010 - K.VONSALZEN   NEW
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILGA) :: VOA,VBC,VSU,VMD,VSS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA) :: VITRM
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAEXT) :: PEDMDT,PEMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT) :: PIDMDT,PIMAS
      REAL, INTENT(IN), DIMENSION(ILGA,LEVA,ISAINT,KINT) :: PIDFDT, &
                                                            PIFRC
      REAL, DIMENSION(ILGA) :: TMP
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PEMAST
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: PIMAST
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: PIFRCT
!
!-----------------------------------------------------------------------
!     * ALLOCATE WORK SPACE.
!
      IF ( ISAEXT > 0 ) THEN
        ALLOCATE(PEMAST(ILGA,LEVA,ISAEXT))
      ENDIF
      IF ( ISAINT > 0 ) THEN
        ALLOCATE(PIMAST(ILGA,LEVA,ISAINT))
        ALLOCATE(PIFRCT(ILGA,LEVA,ISAINT,KINT))
      ENDIF
!
!     * UPDATE AEROSOL NUMBER AND MASS.
!
      IF ( ISAEXT > 0 ) THEN
        PEMAST=MAX(PEMAS+DT*PEDMDT,0.)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        PIMAST=MAX(PIMAS+DT*PIDMDT,0.)
        PIFRCT=MAX(PIFRC+DT*PIDFDT,0.)
      ENDIF
      VOA=0.
      VBC=0.
      VSU=0.
      VMD=0.
      VSS=0.
!
!     * DIAGNOSE COLUMN-INTEGRATED SOURCES AND SINKS FOR EXTERNALLY
!     * MIXED TYPES OF AEROSOL.
!
      IF ( ISEXTOC > 0 ) THEN
        DO IS=1,ISEXTOC
          ISX=IEXOC(IS)
          DO L=1,LEVA
            VOA(:)=VOA(:)+VITRM(:,L)*(PEMAST(:,L,ISX)-PEMAS(:,L,ISX))/DT
          ENDDO
        ENDDO
      ENDIF
      IF ( ISEXTBC > 0 ) THEN
        DO IS=1,ISEXTBC
          ISX=IEXBC(IS)
          DO L=1,LEVA
            VBC(:)=VBC(:)+VITRM(:,L)*(PEMAST(:,L,ISX)-PEMAS(:,L,ISX))/DT
          ENDDO
        ENDDO
      ENDIF
      IF ( ISEXTSO4 > 0 ) THEN
        DO IS=1,ISEXTSO4
          ISX=IEXSO4(IS)
          DO L=1,LEVA
            VSU(:)=VSU(:)+VITRM(:,L)*(PEMAST(:,L,ISX)-PEMAS(:,L,ISX))/DT
          ENDDO
        ENDDO
      ENDIF
      IF ( ISEXTMD > 0 ) THEN
        DO IS=1,ISEXTMD
          ISX=IEXMD(IS)
          DO L=1,LEVA
            VMD(:)=VMD(:)+VITRM(:,L)*(PEMAST(:,L,ISX)-PEMAS(:,L,ISX))/DT
          ENDDO
        ENDDO
      ENDIF
      IF ( ISEXTSS > 0 ) THEN
        DO IS=1,ISEXTSS
          ISX=IEXSS(IS)
          DO L=1,LEVA
            VSS(:)=VSS(:)+VITRM(:,L)*(PEMAST(:,L,ISX)-PEMAS(:,L,ISX))/DT
          ENDDO
        ENDDO
      ENDIF
!
!     * THE SAME AS ABOVE FOR INTERNALLY MIXED AEROSOL.
!
      IF ( ISINTOC > 0 ) THEN
        DO IS=1,ISINTOC
          ISX=IINOC(IS)
          DO L=1,LEVA
            TMP=(PIMAST(:,L,ISX)*PIFRCT(:,L,ISX,KINTOC) &
                -PIMAS (:,L,ISX)*PIFRC (:,L,ISX,KINTOC))/DT
            VOA(:)=VOA(:)+VITRM(:,L)*TMP(:)
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTBC > 0 ) THEN
        DO IS=1,ISINTBC
          ISX=IINBC(IS)
          DO L=1,LEVA
            TMP=(PIMAST(:,L,ISX)*PIFRCT(:,L,ISX,KINTBC) &
                -PIMAS (:,L,ISX)*PIFRC (:,L,ISX,KINTBC))/DT
            VBC(:)=VBC(:)+VITRM(:,L)*TMP(:)
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTSO4 > 0 ) THEN
        DO IS=1,ISINTSO4
          ISX=IINSO4(IS)
          DO L=1,LEVA
            TMP=(PIMAST(:,L,ISX)*PIFRCT(:,L,ISX,KINTSO4) &
                -PIMAS (:,L,ISX)*PIFRC (:,L,ISX,KINTSO4))/DT
            VSU(:)=VSU(:)+VITRM(:,L)*TMP(:)
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTMD > 0 ) THEN
        DO IS=1,ISINTMD
          ISX=IINMD(IS)
          DO L=1,LEVA
            TMP=(PIMAST(:,L,ISX)*PIFRCT(:,L,ISX,KINTMD) &
                -PIMAS (:,L,ISX)*PIFRC (:,L,ISX,KINTMD))/DT
            VMD(:)=VMD(:)+VITRM(:,L)*TMP(:)
          ENDDO
        ENDDO
      ENDIF
      IF ( ISINTSS > 0 ) THEN
        DO IS=1,ISINTSS
          ISX=IINSS(IS)
          DO L=1,LEVA
            TMP=(PIMAST(:,L,ISX)*PIFRCT(:,L,ISX,KINTSS) &
                -PIMAS (:,L,ISX)*PIFRC (:,L,ISX,KINTSS))/DT
            VSS(:)=VSS(:)+VITRM(:,L)*TMP(:)
          ENDDO
        ENDDO
      ENDIF
!
!     * DEALLOCATION.
!
      IF ( ISAEXT > 0 ) THEN
        DEALLOCATE(PEMAST)
      ENDIF
      IF ( ISAINT > 0 ) THEN
        DEALLOCATE(PIMAST)
        DEALLOCATE(PIFRCT)
      ENDIF
!
      END SUBROUTINE DIAGFLX
