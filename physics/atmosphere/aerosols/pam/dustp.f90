      SUBROUTINE DUSTP (PN0,PHI0,PSI,ILGA,ISEC, &
                        FLND,GT,SMFR,FN,ZSPD,USTARD, &
                        SUZ0,SLAI,SPOT,ST02,ST03,ST04,ST06, &
                        ST13,ST14,ST15,ST16,ST17, &
                        ISVDUST,DEFA,DEFC, &
                        FALL,FA10,FA2,FA1, &
                        DUWD,DUST,DUTH,USMK, &
                        DIAGMAS,DIAGNUM,ISDUST)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CACULATION OF SIZE-DEPENDENT DUST FLUX.
!
!     HISTORY:
!     --------
!     * FEB 05/2015 - M.LAZARE/     NEW VERSION FOR GCM18:
!     *               K.VONSALZEN.  - FLND PASSED IN INSTEAD OF {GC,MASK},
!     *                               FOR FRACTIONAL LAND MASK SUPPORT. THIS IS
!     *                               THEN PASSED ON IN CALL TO DUFCC.
!     *                               THIS ELIMINATES THE BUG WHERE HAD "MASK(IL).EQ.-0.5".
!     *                             - {ZSPD,USTARD} PASSED IN INSTEAD OF {SFCU,SFCV,GUST}
!     *                               BECAUSE THIS SCREEN-LEVEL QUANTITY
!     *                               ALREADY CALCULATED IN EARLIER ROUTINES.
!     * NOV 14/2013 - M.LAZARE      CUSCPLA PARAMETER VALUE DECLARATION
!     *                             REMOVED AND NO LONGER PASSED TO DUFCC.
!     * AUG 25/2009 - K.VONSALZEN   MODIFIED FOR AEROCOM SIZE PARAMETERS
!                                   AND EMISSION FLUX FROM BULK SCHEME.
!     * SEP  4/2008 - K.VONSALZEN   NEW
!
!-----------------------------------------------------------------------
!
      USE SDPARM
      USE SDCODE
      USE DUPARM1
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN),  DIMENSION(ILGA) :: FLND,GT,SMFR,FN, &
                                            ZSPD,USTARD,SUZ0,SLAI, &
                                            SPOT,ST02,ST03,ST04,ST06, &
                                            ST13,ST14,ST15,ST16,ST17
      REAL, INTENT(OUT), DIMENSION(ILGA,ISEC) :: PN0,PHI0,PSI
      REAL, INTENT(OUT), DIMENSION(ILGA,ISDUST*2) :: DIAGMAS,DIAGNUM
!
      REAL, DIMENSION(NCLASS) :: UTH
      REAL, DIMENSION(NATS,NCLASS) :: SREL,SRELV,SU_SRELV
      REAL, DIMENSION(ILGA,ISEC) :: TDFLX,TDSIZE,TDVAR
      REAL, DIMENSION(ILGA) :: TDMASK
!
      REAL, INTENT(OUT), DIMENSION(ILGA) :: DEFA,DEFC, &
                               FALL,FA10,FA2,FA1, &
                               DUWD,DUST,DUTH,USMK
!-----------------------------------------------------------------------
!     * DUST EMISSION FROM THE SURFACE
!
       CALL DUINIT1(UTH,SREL,SRELV,SU_SRELV,CUSCPLA)
!
       CALL DUFCC (FLND,GT,SMFR,FN,ZSPD,USTARD, &
                   SUZ0,SLAI,SPOT,ST02,ST03,ST04,ST06, &
                   ST13,ST14,ST15,ST16,ST17, &
                   UTH,SREL,SRELV,SU_SRELV, &
                   TDFLX,TDSIZE,TDVAR,TDMASK,ILGA,ISEC, &
                   ISVDUST,DEFA,DEFC, &
                   FALL,FA10,FA2,FA1, &
                   DUWD,DUST,DUTH,USMK, &
                   DIAGMAS,DIAGNUM,ISDUST)
!
!-----------------------------------------------------------------------
!     * INITIALIZATION OF PLA PARAMETERS.
!
      PN0 =0.
      PHI0=YNA
      PSI =YNA
!
!     * EMISSION FLUX EXPRESSED IN TERMS OF PLA PARAMETERS FOR
!       LOG-NORMAL SIZE DISTRIBUTION.
!
      DO IL=1,ILGA
        IF (TDMASK(IL)==1.) THEN
         DO IS=1,ISEC
          PN0 (IL,IS)=TDFLX(IL,IS)* &
                      1./(SQRT(2.*YPI)*LOG(TDVAR(IL,IS)))
          PHI0(IL,IS)=LOG(TDSIZE(IL,IS)/R0)
          PSI (IL,IS)=1./(2.*LOG(TDVAR(IL,IS))**2)
         ENDDO
        ENDIF
      ENDDO
!
      END SUBROUTINE DUSTP
