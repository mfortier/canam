      SUBROUTINE SSFCC (PN0,PHI0,PSI,ZSPD,FWAT,ILGA,ISEC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CACULATION OF SEA SALT CONCENTRATIONS IN FIRST MODEL LAYER
!
!     HISTORY:
!     --------
!     * FEB 05/2015 - M.LAZARE/     NEW VERSION FOR GCM18:
!     *               K.VONSALZEN.  - FWAT PASSED IN INSTEAD OF {GC,MASK},
!     *                             - CHANGE TO "PHI0" VALUE.
!     *                             - TUNESS INCREASED FROM 6 TO 12.
!     * NOV 14/2013 - M.LAZARE.     TUNESS CHANGED TO VALUE OF 6.
!     * JUN 14/2007 - K.VONSALZEN   REVISED FOR NEW PLA FRAMEWORK.
!     * APR 19/2005 - X.Y. MA       NEW
!
!-----------------------------------------------------------------------
!
      USE SDPARM
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN),  DIMENSION(ILGA) :: ZSPD,FWAT
      REAL, INTENT(OUT), DIMENSION(ILGA,ISEC) :: PN0,PHI0,PSI
!
      REAL, PARAMETER :: TUNESS=12.

!-----------------------------------------------------------------------
!     * INITIALIZATION OF PLA PARAMETERS.
!
      PN0 =0.
      PHI0=YNA
      PSI =YNA
!
!     * WIND-SPEED DEPENDENT PLA PARAMETERS BASED ON PARAMETERIZATION
!     * BY LEWIS AND SCHWARTZ. A GROWTH FACTOR OF 2.1859 IS ASSUMED
!     * TO CONVERT THE PARTICLE SIZE AT 80% TO 0% RELATIVE HUMIDITY.
!
      DO IL=1,ILGA
        IF (FWAT(IL).GT.0.) THEN
          PN0 (IL,:)=TUNESS*30400.*ZSPD(IL)**2
!          PHI0(IL,:)=-0.3085E+01
          PHI0(IL,:)=-0.2679E+01
!          PHI0(IL,:)=-0.2391E+01
!          PHI0(IL,:)=-0.1986E+01
          PSI (IL,:)=0.4716E+00
        ENDIF
      ENDDO
!
      END SUBROUTINE SSFCC
