!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INTHEM2(EBOCROW,EBOCROL,EBBCROW,EBBCROL,
     1                   EAOCROW,EAOCROL,EABCROW,EABCROL,
     2                   EASLROW,EASLROL,EASHROW,EASHROL,
     3                   ILG,IL1,IL2,LEVWF,DELT,GMT,IDAY,MDAY)
C
C     * KNUT VON SALZEN - JUL 27,2009. NEW VERSION FOR GCM15I:
C     *                                - REMOVE FBBC.
C     * KNUT VON SALZEN - FEB 07,2009. PREVIOUS VERSION INTHEM FOR GCM15H: 
C     *                                - DOES INTERPOLATION OF:
C     *                                  EBOC,EBBC,EAOC,EABC,EASL,EASH,FBBC.
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, DIMENSION(ILG) :: EBOCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EBOCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EBBCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EBBCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EAOCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EAOCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EABCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EABCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EASLROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EASLROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EASHROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EASHROL   !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C--------------------------------------------------------------------
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY.
C
      SEC0=REAL(IDAY)*86400. + GMT
C
      FMSEC=REAL(MDAY)*86400.
      IF(FMSEC.LT.SEC0) FMSEC=FMSEC+365.*86400.
C
      SECSM=FMSEC-SEC0
C
C     * GENERAL INTERPOLATION.
C
      DO 10 I=IL1,IL2
        EBOCROW(I) = ((SECSM-DELT)*EBOCROW(I) + DELT*EBOCROL(I))/SECSM
        EBBCROW(I) = ((SECSM-DELT)*EBBCROW(I) + DELT*EBBCROL(I))/SECSM
        EAOCROW(I) = ((SECSM-DELT)*EAOCROW(I) + DELT*EAOCROL(I))/SECSM
        EABCROW(I) = ((SECSM-DELT)*EABCROW(I) + DELT*EABCROL(I))/SECSM
        EASLROW(I) = ((SECSM-DELT)*EASLROW(I) + DELT*EASLROL(I))/SECSM
        EASHROW(I) = ((SECSM-DELT)*EASHROW(I) + DELT*EASHROL(I))/SECSM
  10  CONTINUE
C
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
