!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      MODULE DUPARM
C-----------------------------------------------------------------------
C     PURPOSE:
C     --------
C     BASIC CONSTANTS AND ARRAYS FOR DUST EMISSION.
C
C     HISTORY:
C     --------
C     * AUG 10/2007 - Y. PENG   NEW.
C
C-----------------------------------------------------------------------
C
        IMPLICIT NONE
C       
C       * array dimensions
C
        INTEGER, PARAMETER :: NTRACE=8            ! NUMBER OF TRACERS
        INTEGER, PARAMETER :: NBIN=24             ! NUMBER OF BINS PER TRACER
        INTEGER, PARAMETER :: NCLASS=NTRACE*NBIN  ! NUMBER OF PARTICLE CLASSES
        INTEGER, PARAMETER :: NATS=17             ! NUMBER OF SOIL TYPES
        INTEGER, PARAMETER :: NMODE=4             ! NUMBER OF SOIL POPULATION MODE
        INTEGER, PARAMETER :: NSPE=NMODE*3+2
C
C       * size indices for accu. and coarse modes /cutoff radius between AI and CI is 0.68um, cutoff for CI is 9.24um
C
        INTEGER, PARAMETER :: MINAI = 1
        INTEGER, PARAMETER :: MAXAI = 3
        INTEGER, PARAMETER :: MINCI = 4
        INTEGER, PARAMETER :: MAXCI = 6
C      
C
C       * soil particle size parameters (cm) /midified for GCM15I and PLA (0.05~54.83um) 
C
        REAL, PARAMETER :: DMIN = 0.00001              ! MINIMUM PARTICULES DIAMETER [CM]
        REAL, PARAMETER :: DMAX = 0.011                ! MAXIMUM PARTICULES DIAMETER [CM] 
        REAL, PARAMETER :: DSTEP = 0.0365              ! DIAMETER INCREMENT [CM]
C
C       * Reynolds constants and threshold value
C
        REAL, PARAMETER :: A_RNOLDS = 1331.647
        REAL, PARAMETER :: B_RNOLDS = 0.38194
        REAL, PARAMETER :: X_RNOLDS = 1.561228
        REAL, PARAMETER :: D_THRSLD = 0.00000231   
C
C       * air and soil particle density (g/cm3)
C
        REAL, PARAMETER :: ROA = 0.001227
        REAL, PARAMETER :: ROP = 2.65
C
        REAL, PARAMETER :: UMIN = 21.       ! MINIMUM THRESHOLD FRICTION WIND SPEED [CM/S]
C
        REAL, PARAMETER :: VK = 0.4         ! VON KARMAN CONSTANT
C
C       * efficient fraction 
C
        REAL, PARAMETER :: AEFF = 0.35
        REAL, PARAMETER :: XEFF = 10.
C
        REAL, PARAMETER :: ZZ = 1000.       ! WIND MEASURE HEIGHT [CM]
C
C       * surface roughness related parameters
C
        REAL, PARAMETER :: Z0S = 0.001
        REAL, PARAMETER :: Z01 = 0.001
        REAL, PARAMETER :: Z02 = 0.001
C
        REAL, PARAMETER :: CUSCALE = 0.85   ! SCALE FACTOR FOR WIND STRESS THRESHOLD
C
        REAL, PARAMETER :: PI = 3.141592653589793
        REAL, PARAMETER :: GRAVI = 9.8*100. ! GRAVITY ACCELERATION IN [CM/S2]
C
        REAL, PARAMETER :: W0 = 0.25        ! SOIL MOISTURE THRESHOLD
        LOGICAL         :: CCCMABF = .TRUE. ! USE CCCMA BARE SOIL FRACTION OR NOT
C
C      * SOIL TYPE DATA
C
C      SOLSPE --> SOIL CARACTERISTICS: 
C      ZOBLER texture classes
C      SOLSPE: for 4 populations : values = 3*(Dmed sig p);
C                                          ratio of fluxes;
C                                          residual moisture
C      Populations: Coarse sand, medium/fine sand, Silt, Clay
C
C       soil type 1 : Coarse
C       soil type 2 : Medium
C       soil type 3 : Fine
C       soil type 4 : Coarse Medium
C       soil type 5 : Coarse Fine
C       soil type 6 : Medium Fine
C       soil type 7 : Coarse_dp, Medium_dp, Fine
C       soil type 8 : Organic
C       soil type 9 : Ice
C       soil type 10 : Potential Lakes (additional)
C       soil type 11 : Potential Lakes (clay)
C       soil type 12 : Potential Lakes Australia
C
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

        REAL,  DIMENSION(NSPE,NATS) :: SOLSPE
C
        DATA SOLSPE /
     1     0.0707, 2.0, 0.43, 0.0158, 2.0, 0.40, 0.0015,
     &     2.0, 0.17, 0.0002, 2.0, 0.00, 2.1e-06, 0.20,
     2     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.37, 0.0015,
     &     2.0, 0.33, 0.0002, 2.0, 0.30, 4.0e-06, 0.25,
     3     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015,
     &     2.0, 0.33, 0.0002, 2.0, 0.67, 1.0e-07, 0.50,
     4     0.0707, 2.0, 0.10, 0.0158, 2.0, 0.50, 0.0015,
     &     2.0, 0.20, 0.0002, 2.0, 0.20, 2.7e-06, 0.23,
     5     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.50, 0.0015,
     &     2.0, 0.12, 0.0002, 2.0, 0.38, 2.8e-06, 0.25,
     6     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.27, 0.0015,
     &     2.0, 0.25, 0.0002, 2.0, 0.48, 1.0e-07, 0.36,
     7     0.0707, 2.0, 0.23, 0.0158, 2.0, 0.23, 0.0015,
     &     2.0, 0.19, 0.0002, 2.0, 0.35, 2.5e-06, 0.25,
     8     0.0707, 2.0, 0.25, 0.0158, 2.0, 0.25, 0.0015,
     &     2.0, 0.25, 0.0002, 2.0, 0.25, 0.0e-00, 0.50,
     9     0.0707, 2.0, 0.25, 0.0158, 2.0, 0.25, 0.0015,
     &     2.0, 0.25, 0.0002, 2.0, 0.25, 0.0e-00, 0.50,
     &     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015,
     &     2.0, 1.00, 0.0002, 2.0, 0.00, 1.0e-05, 0.25,
     1     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0015,
     &     2.0, 0.00, 0.0002, 2.0, 1.00, 1.0e-05, 0.25,
     2     0.0707, 2.0, 0.00, 0.0158, 2.0, 0.00, 0.0027,
     &     2.0, 1.00, 0.0002, 2.0, 0.00, 1.0e-05, 0.25,
     3     0.0442, 1.5, 0.03, 0.0084, 1.5, 0.85, 0.0015,
     &     2.0, 0.11, 0.0002, 2.0, 0.02, 1.9e-06, 0.12,
     4     0.0450, 1.5, 0.00, 0.0070, 1.5, 0.33, 0.0015,
     &     2.0, 0.50, 0.0002, 2.0, 0.17, 1.9e-04, 0.15, 
     5     0.0457, 1.8, 0.31, 0.0086, 1.5, 0.22, 0.0015, 
     &     2.0, 0.34, 0.0002, 2.0, 0.12, 3.9e-05, 0.13,
     6     0.0293, 1.8, 0.39, 0.0090, 1.5, 0.16, 0.0015,
     &     2.0, 0.35, 0.0002, 2.0, 0.10, 3.1e-05, 0.13,
     7     0.0305, 1.5, 0.46, 0.0101, 1.5, 0.41, 0.0015, 
     &     2.0, 0.10, 0.0002, 2.0, 0.03, 2.8e-06, 0.12/
C
      END MODULE DUPARM
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
