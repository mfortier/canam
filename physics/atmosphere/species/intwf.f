!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INTWF(EBWAROW,EBWAROL,EOWAROW,EOWAROL,ESWAROW,ESWAROL,
     1                 ILG,IL1,IL2,LEVWF,DELT,GMT,IDAY,LDAY)
C
C     * MAR 06/07 - X.MA.     INTERPOLATES AEROCOM WILDFIRE EMISSIONS 
C     *                       TO CUDRRENT TIMESTEP.
C     *                       (INTERPOLATES IN TERMS OF SECONDS
C     *                        INSTEAD OF TIMESTEPS, TO AVOID
C     *                        ROUNDOFF ERROR WHICH CAME FROM
C     *                        DIVIDING BY TIMESTEP.
C     *                        THIS AROSE FOLLOWING INVESTIGATION
C     *                        BY DAVID PLUMMER.)
C
C     * ESWAROW  = SULPHATE FROM WILD FIRE FIELD    FOR PREVIOUS TIMESTEP.
C     * ESWAROL  = SULPHATE FROM WILD FIRE FIELD    AT NEXT PHYSICS TIME.
C
C     * ILG    = SIZE OF LONGITUDE LOOP (CHAINED).
C     * IL1    = START OF LONGITUDE LOOP.
C     * IL2    = END   OF LONGITUDE LOOP.
C     * LEVWF  = NUMBER OF EMISSION LEVELS.
C     * DELT   = MODEL TIMESTEP IN SECONDS.
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.
C     * LDAY   = DATE OF TARGET OZONE.
C
C     * MULTI-LEVEL FOR EMISSION.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL     EBWAROW(ILG,LEVWF)       !<Variable description\f$[units]\f$
      REAL     EBWAROL(ILG,LEVWF)       !<Variable description\f$[units]\f$
      REAL     EOWAROW(ILG,LEVWF)       !<Variable description\f$[units]\f$
      REAL     EOWAROL(ILG,LEVWF)       !<Variable description\f$[units]\f$
      REAL     ESWAROW(ILG,LEVWF)       !<Variable description\f$[units]\f$
      REAL     ESWAROL(ILG,LEVWF)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C--------------------------------------------------------------------
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY.
C
      SEC0=REAL(IDAY)*86400. + GMT
C
      FLSEC=REAL(LDAY)*86400.
      IF(FLSEC.LT.SEC0) FLSEC=FLSEC+365.*86400.
C
      SECSL=FLSEC-SEC0
C
C     * GENERAL INTERPOLATION.
C
      DO 200 L=1,LEVWF
      DO 200 I=IL1,IL2
        EBWAROW(I,L) = ((SECSL-DELT)*EBWAROW(I,L) + DELT*EBWAROL(I,L))
     1                 /SECSL
        EOWAROW(I,L) = ((SECSL-DELT)*EOWAROW(I,L) + DELT*EOWAROL(I,L))
     1                 /SECSL
        ESWAROW(I,L) = ((SECSL-DELT)*ESWAROW(I,L) + DELT*ESWAROL(I,L))
     1                 /SECSL
  200 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
