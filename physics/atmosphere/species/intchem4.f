!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!               
      SUBROUTINE INTCHEM4(DMSOROW,DMSOROL,EDMSROW,EDMSROL,
     1                    SUZ0ROW,SUZ0ROL,PDSFROW,PDSFROL, 
     2                      OHROW,  OHROL,H2O2ROW,H2O2ROL,
     3                      O3ROW,  O3ROL, NO3ROW, NO3ROL,
     4                    HNO3ROW,HNO3ROL, NH3ROW, NH3ROL,
     5                     NH4ROW, NH4ROL,
     6                    IL1,IL2,ILG,LEVOX,DELT,GMT,IDAY,MDAY)
C
C     * APR 25,2012 - Y.PENG.    NEW VERSIONO FOR GCM16:
C     *                          ADD SUZ0,PDSF FOR NEW DUST SCHEME.
C     * FEB 07,09 - K.VONSALZEN. PREVIOUS VERSION INTCHEM3 FOR GCM15H/I:
C     *                          EOST REMOVED (NOW IN SEPARATE
C     *                          ROUTINE INTCAC).  
C     * JAN 17/08 - X.MA/     PREVIOUS VERSION INTCHEM2 FOR GCM15G:
C     *             M.LAZARE. 
C     * APR 23/04 - M.LAZARE. PREVIOUS VERSION INTCHEM UP TO GCM15F.

C     *                       INTERPOLATION OF CHEMISTRY FIELDS, BASED
C     *                       ON SUBROUTINE "GETGTIO" (FOR IMASK=0 CASE).
C
C     * XXROW  = CHEMISTRY FIELD    FOR PREVIOUS TIMESTEP. 
C     * XXROL  = CHEMISTRY FIELD    AT NEXT PHYSICS TIME.
C
C     * DELT   = MODEL TIMESTEP IN SECONDS. 
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.  
C     * MDAY   = DATE OF TARGET SST.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N) 
C 
C     * SURFACE EMISSIONS/CONCENTRATIONS.
C
      REAL   DMSOROW(ILG)       !<Variable description\f$[units]\f$
      REAL   DMSOROL(ILG)       !<Variable description\f$[units]\f$
      REAL   EDMSROW(ILG)       !<Variable description\f$[units]\f$
      REAL   EDMSROL(ILG)       !<Variable description\f$[units]\f$
      REAL   SUZ0ROW(ILG)       !<Variable description\f$[units]\f$
      REAL   SUZ0ROL(ILG)       !<Variable description\f$[units]\f$
      REAL   PDSFROW(ILG)       !<Variable description\f$[units]\f$
      REAL   PDSFROL(ILG)       !<Variable description\f$[units]\f$
C
C     * MULTI-LEVEL SPECIES.
C
      REAL     OHROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL     OHROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL   H2O2ROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL   H2O2ROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL     O3ROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL     O3ROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL    NO3ROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL    NO3ROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL   HNO3ROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL   HNO3ROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL    NH3ROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL    NH3ROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL    NH4ROW(ILG,LEVOX)  !<Variable description\f$[units]\f$
      REAL    NH4ROL(ILG,LEVOX)  !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY. 
C
      DAY=REAL(IDAY)+GMT/86400. 
      FMDAY=REAL(MDAY) 
      IF(FMDAY.LT.DAY) FMDAY=FMDAY+365. 
      DAYSM=FMDAY-DAY 
      ISTEPSM=NINT(DAYSM*86400./DELT)
      STEPSM=REAL(ISTEPSM)
C 
C     * GENERAL INTERPOLATION.
C 
      DO 210 I=IL1,IL2
        DMSOROW(I)  = ((STEPSM-1.) * DMSOROW(I) + DMSOROL(I)) / STEPSM 
        EDMSROW(I)  = ((STEPSM-1.) * EDMSROW(I) + EDMSROL(I)) / STEPSM 
        SUZ0ROW(I)  = ((STEPSM-1.) * SUZ0ROW(I) + SUZ0ROL(I)) / STEPSM  
        PDSFROW(I)  = ((STEPSM-1.) * PDSFROW(I) + PDSFROL(I)) / STEPSM
  210 CONTINUE
C 
      DO 250 L=1,LEVOX
      DO 250 I=IL1,IL2
        OHROW  (I,L) = ((STEPSM-1.)*OHROW  (I,L) + OHROL  (I,L)) /STEPSM 
        H2O2ROW(I,L) = ((STEPSM-1.)*H2O2ROW(I,L) + H2O2ROL(I,L)) /STEPSM
        O3ROW  (I,L) = ((STEPSM-1.)*O3ROW  (I,L) + O3ROL  (I,L)) /STEPSM
        NO3ROW (I,L) = ((STEPSM-1.)*NO3ROW (I,L) + NO3ROL (I,L)) /STEPSM 
        HNO3ROW(I,L) = ((STEPSM-1.)*HNO3ROW(I,L) + HNO3ROL(I,L)) /STEPSM 
        NH3ROW (I,L) = ((STEPSM-1.)*NH3ROW (I,L) + NH3ROL (I,L)) /STEPSM 
        NH4ROW (I,L) = ((STEPSM-1.)*NH4ROW (I,L) + NH4ROL (I,L)) /STEPSM 
  250 CONTINUE
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
