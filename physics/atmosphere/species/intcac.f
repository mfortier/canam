!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!       
      SUBROUTINE INTCAC(EOSTROW,EOSTROL,
     1                  IL1,IL2,ILG,ILEV,DELT,GMT,IDAY,MDAY)
C
C     * KNUT VON SALZEN - FEB 07,2009. NEW ROUTINE FOR GCM15H TO DO 
C     *                                INTERPOLATION OF EOST 
C     *                                (USED TO BE DONE BEFORE
C     *                                IN INTCHEM2).
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N) 
C 
C     * SURFACE EMISSIONS/CONCENTRATIONS.
C
      REAL   EOSTROW(ILG)       !<Variable description\f$[units]\f$
      REAL   EOSTROL(ILG)       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY. 
C
      DAY=REAL(IDAY)+GMT/86400. 
      FMDAY=REAL(MDAY) 
      IF(FMDAY.LT.DAY) FMDAY=FMDAY+365. 
      DAYSM=FMDAY-DAY 
      ISTEPSM=NINT(DAYSM*86400./DELT)
      STEPSM=REAL(ISTEPSM)
C 
C     * GENERAL INTERPOLATION.
C 
      DO 210 I=IL1,IL2
        EOSTROW(I)  = ((STEPSM-1.) * EOSTROW(I) + EOSTROL(I)) / STEPSM 
  210 CONTINUE
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
