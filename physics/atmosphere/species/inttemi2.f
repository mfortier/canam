!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INTTEMI2(SAIRROW,SAIRROL,SSFCROW,SSFCROL,SBIOROW,
     1                    SBIOROL,SSHIROW,SSHIROL,SSTKROW,SSTKROL,
     2                    SFIRROW,SFIRROL,OAIRROW,OAIRROL,OSFCROW,
     3                    OSFCROL,OBIOROW,OBIOROL,OSHIROW,OSHIROL,
     4                    OSTKROW,OSTKROL,OFIRROW,OFIRROL,BAIRROW,
     5                    BAIRROL,BSFCROW,BSFCROL,BBIOROW,BBIOROL,
     6                    BSHIROW,BSHIROL,BSTKROW,BSTKROL,BFIRROW,
     7                    BFIRROL,ILG,IL1,IL2,DELT,GMT,IDAY,MDAY)
C
C     * JUN 18/2013 - K.VONSALZEN. NEW VERSION FOR GCM17+:
C     *                            - ADD {SBIO,SSHI,OBIO,OSHI,BBIO,BSHI}.  
C     * APR 20/2010 - K.VONSALZEN. PREVIOUS VERSION INTTEMI FOR GCM15I+.
C
C     * INTERPOLATE TRANSIENT EMISSION DATA.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
      REAL, DIMENSION(ILG) :: SAIRROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SAIRROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SSFCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SSFCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SBIOROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SBIOROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SSHIROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SSHIROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SSTKROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SSTKROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SFIRROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SFIRROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OAIRROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OAIRROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OSFCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OSFCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OBIOROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OBIOROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OSHIROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OSHIROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OSTKROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OSTKROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OFIRROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: OFIRROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BAIRROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BAIRROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BSFCROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BSFCROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BBIOROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BBIOROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BSHIROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BSHIROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BSTKROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BSTKROL   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BFIRROW   !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BFIRROL   !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C---------------------------------------------------------------------
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY.
C
      SEC0=REAL(IDAY)*86400. + GMT
C
      FMSEC=REAL(MDAY)*86400.
      IF(FMSEC.LT.SEC0) FMSEC=FMSEC+365.*86400.
C
      SECSM=FMSEC-SEC0
C
C     * GENERAL INTERPOLATION.
C
      DO I=IL1,IL2
        SAIRROW(I)=((SECSM-DELT)*SAIRROW(I)+DELT*SAIRROL(I))/SECSM
        SSFCROW(I)=((SECSM-DELT)*SSFCROW(I)+DELT*SSFCROL(I))/SECSM
        SBIOROW(I)=((SECSM-DELT)*SBIOROW(I)+DELT*SBIOROL(I))/SECSM
        SSHIROW(I)=((SECSM-DELT)*SSHIROW(I)+DELT*SSHIROL(I))/SECSM
        SSTKROW(I)=((SECSM-DELT)*SSTKROW(I)+DELT*SSTKROL(I))/SECSM
        SFIRROW(I)=((SECSM-DELT)*SFIRROW(I)+DELT*SFIRROL(I))/SECSM
        OAIRROW(I)=((SECSM-DELT)*OAIRROW(I)+DELT*OAIRROL(I))/SECSM
        OSFCROW(I)=((SECSM-DELT)*OSFCROW(I)+DELT*OSFCROL(I))/SECSM
        OBIOROW(I)=((SECSM-DELT)*OBIOROW(I)+DELT*OBIOROL(I))/SECSM
        OSHIROW(I)=((SECSM-DELT)*OSHIROW(I)+DELT*OSHIROL(I))/SECSM
        OSTKROW(I)=((SECSM-DELT)*OSTKROW(I)+DELT*OSTKROL(I))/SECSM
        OFIRROW(I)=((SECSM-DELT)*OFIRROW(I)+DELT*OFIRROL(I))/SECSM
        BAIRROW(I)=((SECSM-DELT)*BAIRROW(I)+DELT*BAIRROL(I))/SECSM
        BSFCROW(I)=((SECSM-DELT)*BSFCROW(I)+DELT*BSFCROL(I))/SECSM
        BBIOROW(I)=((SECSM-DELT)*BBIOROW(I)+DELT*BBIOROL(I))/SECSM
        BSHIROW(I)=((SECSM-DELT)*BSHIROW(I)+DELT*BSHIROL(I))/SECSM
        BSTKROW(I)=((SECSM-DELT)*BSTKROW(I)+DELT*BSTKROL(I))/SECSM
        BFIRROW(I)=((SECSM-DELT)*BFIRROW(I)+DELT*BFIRROL(I))/SECSM
      ENDDO
C
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
