!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE XTESOA3(XEMIS,PRESSG,DSHJ,EOSTROW,IPAM,DOC,
     1                   IOCO,IOCY,ILG,IL1,IL2,ILEV,LEV,
     2                   NTRAC,ZTMST)
C
C     * JUN 18/2013 - K.VONSALZEN/ NEW VERSION FOR GCM17+:
C     *               M.LAZARE.    - ADD CONDITION TO SET WGT=1. FOR
C     *                              SPECIAL CASE WHERE HSO2=LSO2.
C     *                            - ADD SUPPORT FOR THE PLA OPTION
C     *                              "DOC" ARRAY, "IPAM" SWITCH NOW PASSED.  
C     * APR 26/10 - K.VONSALZEN. PREVIOUS VERSION XTESOA2 FOR GCM15I+:
C     *                          - ZF AND PF CALCULATED ONE-TIME AT
C     *                            BEGINNING OF PHYSICS AND ZF
C     *                            PASSED IN, WITH INTERNAL CALCULATION
C     *                            THUS REMOVED AND NO NEED TO PASS
C     *                            IN SHBJ,T.
C     *                          - EMISSIONS ACCUMULATED INTO GENERAL
C     *                            "XEMIS" ARRAY RATHER THAN UPDATING
C     *                            XROW (XROW UPDATED FROM XEMIS IN
C     *                            THE PHYSICS). 
C     *                          - REMOVAL OF DIAGNOSTIC FIELDS
C     *                            CALCULATION, SO NO NEED TO PASS
C     *                            IN "SAVERAD" OR "ISVCHEM".
C     *                          - WGT=0 (IE NO EMISSIONS IN LAYER)
C     *                            IF IT IS ABOVE TOP OR BELOW BASE
C     *                            OF EMISSIONS.
C     * KNUT VON SALZEN - FEB 07,2009. PREVIOUS ROUTINE XTESOA FOR 
C     *                                GCM15H TO APPLY EMISSIONS FOR ESOT.
C     *                                THIS USED TO BE IN OLD XTEAERO.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, DIMENSION(ILG) :: EOSTROW           !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: EOBBROW           !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: PRESSG            !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,ILEV) :: DSHJ         !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,ILEV) :: DOC          !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: XEMIS  !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      COMMON /PARAMS/ WW,     TW,    A,     ASQ,  GRAV, RGAS,  RGOCP,
     1                RGOASQ, CPRES, RGASV, CPRESV
C-------------------------------------------------------------------
      POM2OC=1./1.4
      DO 30 IL=IL1,IL2
        FACT = ZTMST*GRAV/(DSHJ(IL,ILEV)*PRESSG(IL))*POM2OC
        EOC=EOSTROW(IL)
        IF ( IPAM.EQ.0 ) THEN
          XEMIS(IL,ILEV,IOCO)=XEMIS(IL,ILEV,IOCO)+0.5*EOC*FACT
          XEMIS(IL,ILEV,IOCY)=XEMIS(IL,ILEV,IOCY)+0.5*EOC*FACT
        ELSE
          DOC(IL,ILEV)=DOC(IL,ILEV)+EOC*FACT/POM2OC/ZTMST
        ENDIF
   30 CONTINUE      
C
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
