!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INTPRF(FBBCROW,FBBCROL,FAIRROW,FAIRROL,
     1                  ILG,IL1,IL2,LEVWF,LEVAIR,DELT,GMT,IDAY,MDAY)
C
C     * KNUT VON SALZEN - JUL 27,2009. NEW ROUTINE TO DO INTERPOLATION
C     *                                OF FBBC AND FAIR, BASED ON INTHEM.
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL, DIMENSION(ILG,LEVWF)  :: FBBCROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,LEVWF)  :: FBBCROL    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,LEVAIR) :: FAIRROW    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG,LEVAIR) :: FAIRROL    !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C--------------------------------------------------------------------
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY.
C
      SEC0=REAL(IDAY)*86400. + GMT
C
      FMSEC=REAL(MDAY)*86400.
      IF(FMSEC.LT.SEC0) FMSEC=FMSEC+365.*86400.
C
      SECSM=FMSEC-SEC0
C
C     * GENERAL INTERPOLATION.
C
      DO 20 L=1,LEVWF
      DO 20 I=IL1,IL2
        FBBCROW(I,L) = ((SECSM-DELT)*FBBCROW(I,L) + DELT*FBBCROL(I,L))
     1                 /SECSM
  20  CONTINUE
      DO 30 L=1,LEVAIR
      DO 30 I=IL1,IL2
        FAIRROW(I,L) = ((SECSM-DELT)*FAIRROW(I,L) + DELT*FAIRROL(I,L))
     1                 /SECSM
  30  CONTINUE
C
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
