!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE OZINT3(OZROW,OZROL,ILG,IL1,IL2,LEVOZ,DELT,
     1                  GMT,IDAY,LDAY)
C
C     * MAY 06/05 - M.LAZARE. NEW VERSION FOR GCM15E ONWARD:
C     *                       - INTERPOLATES IN TERMS OF SECONDS
C     *                         INSTEAD OF TIMESTEPS, TO AVOID
C     *                         ROUNDOFF ERROR WHICH CAME FROM
C     *                         DIVIDING BY TIMESTEP.
C     *                         THIS AROSE FOLLOWING INVESTIGATION
C     *                         BY DAVID PLUMMER.
C     * APR 28/03 - M.LAZARE. PREVIOUS VERSION OZINT2 PRIOR TO GCM15E.
C
C     * OZROW  = OZONE FIELD    FOR PREVIOUS TIMESTEP. 
C     * OZROL  = OZONE FIELD    AT NEXT PHYSICS TIME.
C
C     * ILG    = SIZE OF LONGITUDE LOOP (CHAINED).
C     * IL1    = START OF LONGITUDE LOOP.
C     * IL2    = END   OF LONGITUDE LOOP. 
C     * LEVOZ  = NUMBER OF OZONE LEVELS.
C     * DELT   = MODEL TIMESTEP IN SECONDS. 
C     * GMT    = NUMBER OF SECONDS IN CURRENT DAY.
C     * IDAY   = CURRENT JULIAN DAY.  
C     * LDAY   = DATE OF TARGET OZONE.
C 
C     * MULTI-LEVEL PPMV FOR OZONE.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)                                         

      REAL     OZROW(ILG,LEVOZ) !<Variable description\f$[units]\f$
      REAL     OZROL(ILG,LEVOZ) !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-------------------------------------------------------------------- 
C     * COMPUTE THE NUMBER OF TIMESTEPS FROM HERE TO MDAY. 
C
      SEC0=REAL(IDAY)*86400. + GMT
C
      FLSEC=REAL(LDAY)*86400. 
      IF(FLSEC.LT.SEC0) FLSEC=FLSEC+365.*86400. 
C
      SECSL=FLSEC-SEC0
C 
C     * GENERAL INTERPOLATION.
C 
      DO 200 L=1,LEVOZ
      DO 200 I=IL1,IL2
        OZROW(I,L) = ((SECSL-DELT)*OZROW(I,L) + DELT*OZROL(I,L))/SECSL 
  200 CONTINUE
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
