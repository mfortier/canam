!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE COMPUTE_CLD_TAU(WRKB, CLDA,                           ! OUTPUT
     1                           CLW, CIC, REL, REI, DZ,               ! INPUT  
     2                           WRKA, CLDWATMIN, ILG, IL1, IL2, ILEV, 
     3                           NXLOC,NBS) 

!     18 APR 2018 - JASON COLE  INITIAL VERSION
!
! COMPUTE THE CLOUD-MEAN CLOUD OPTICAL THICKNESS AT 550 NM AND TO IT ADD
! THE AEROSOL OPTICAL THICKNESS FROM THE FIRST (VISIBLE) BAND, AKA 
! WRKA.

      IMPLICIT NONE

!
! OUTPUT 
!

      REAL, DIMENSION(ILG,NBS),INTENT(OUT) :: WRKB!<CLOUD PLUS AEROSOL VISIBLE OPITCAL THICKNESS

      REAL, DIMENSION(ILG),INTENT(OUT) :: CLDA !<TOTAL CLOUD FRACTION THAT IS CONSISTENT WITH WRKB

!
! INPUT 
!
      
      REAL, DIMENSION(ILG,ILEV,NXLOC),INTENT(IN) :: CLW !<CLOUD LIQUID WATER                  (UNITS)\f$[units]\f$
      REAL, DIMENSION(ILG,ILEV,NXLOC),INTENT(IN) :: CIC !<CLOUD ICE WATER                     (UNITS)\f$[units]\f$
      REAL, DIMENSION(ILG,ILEV,NXLOC),INTENT(IN) :: REL !<CLOUD LIQUID WATER EFFECTIVE RADIUS (MICRONS)\f$[units]\f$
      REAL, DIMENSION(ILG,ILEV,NXLOC),INTENT(IN) :: REI !<CLOUD ICE WATER EFFECTIVE DIAMETER  (MICRONS)\f$[units]\f$

      REAL, DIMENSION(ILG,NBS),INTENT(IN) :: WRKA !<AEROSOL OPTICAL THICKNESS FOR EACH BAND\f$[units]\f$

      REAL, DIMENSION(ILG,ILEV),INTENT(IN) :: DZ !<LAYER THICKNESS (M)\f$[units]\f$
 
      REAL, INTENT(IN) :: CLDWATMIN !<THRESHOLD WATER CONTENT\f$[units]\f$
      
      INTEGER, INTENT(IN) :: ILG   !<NUMBER OF GCM COLUMNS\f$[units]\f$
      INTEGER, INTENT(IN) :: IL1   !<STARTING GCM COLUMN\f$[units]\f$
      INTEGER, INTENT(IN) :: IL2   !<ENDING GCM COLUMN\f$[units]\f$
      INTEGER, INTENT(IN) :: ILEV  !<NUMBER OF VERTICAL LAYERS\f$[units]\f$
      INTEGER, INTENT(IN) :: NXLOC !<NUMBER OF SUBCOLUMNS\f$[units]\f$
      INTEGER, INTENT(IN) :: NBS   !<NUMBER OF SOLAR BANDS\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================      

!
! LOCAL
!
      
      REAL, PARAMETER :: 
     1 R_ONE   = 1.0,
     2 R_ZERO  = 0.0
 
      REAL, DIMENSION(ILG) ::
     1 CUMTAU,                ! ACCUMULATE CLOUD OPTICAL THICKNESS IN VERTICAL
     2 SUMTAU,                ! ACCUMULATE CLOUD OPTICAL THICKNESS FROM EACH SUBCOLUMN
     2 COUNT_CLDY,
     3 CLDT
 
      REAL ::
     1 INVREL,     
     2 REI_LOC,        
     3 INVREI,
     4 TAU_VIS,
     5 TAULIQVIS,
     6 TAUICEVIS,
     7 CLW_PATH,
     8 CIC_PATH,
     9 R_NXLOC

      INTEGER ::
     1 I,
     2 L,
     3 IB,
     3 ICOL

! INITIALIZE VARIABLES

      R_NXLOC = REAL(NXLOC)

      DO IB = 1, NBS
         DO I = IL1, IL2
            WRKB(I,IB) = R_ZERO
         END DO ! I
      END DO ! IB

      DO I = IL1, IL2
         CLDT(I)   = R_ZERO
         CLDA(I)   = R_ZERO
         SUMTAU(I) = R_ZERO
      END DO

      DO ICOL = 1, NXLOC

        DO I = IL1, IL2
           COUNT_CLDY(I) = R_ZERO
           CUMTAU(I)     = R_ZERO
        END DO

        DO L = 1, ILEV
           DO I = IL1, IL2

              TAU_VIS  = R_ZERO
              CLW_PATH = CLW(I,L,ICOL)*DZ(I,L)
              CIC_PATH = CIC(I,L,ICOL)*DZ(I,L)

! CALCULATION FOR CLDT
              IF (CLW_PATH .GT. CLDWATMIN) THEN
                 COUNT_CLDY(I) = R_ONE
              END IF
              IF (CIC_PATH .GT. CLDWATMIN) THEN
                 COUNT_CLDY(I) = R_ONE
              END IF

! CALCULATION OF CLOUD OPTICAL THICKNESS

              IF ((CLW_PATH .GT. CLDWATMIN  .OR.
     1             CIC_PATH .GT. CLDWATMIN)) THEN

! COMPUTE CLOUD OPTICAL THICKNESS FOR THIS VOLUME

                 INVREL = R_ONE/REL(I,L,ICOL)

                 REI_LOC = 1.5396*REI(I,L,ICOL) ! CONVERT TO GENERALIZED DIMENSION
                 INVREI  = R_ONE/REI_LOC

                 IF (CLW_PATH .GT. CLDWATMIN) THEN
                    TAULIQVIS = CLW(I,L,ICOL)
     1                        * (4.483e-04 + INVREL * (1.501 + INVREL
     2                        * (7.441e-01 - INVREL * 9.620e-01)))
                 ELSE
                    TAULIQVIS = R_ZERO
                 END IF
                 
                 IF (CIC_PATH .GT. CLDWATMIN) THEN
                    TAUICEVIS = CIC(I,L,ICOL) 
     1                        * (-0.303108e-04 + 0.251805e+01 * INVREI)  
                 ELSE
                    TAUICEVIS = R_ZERO
                 END IF

                 TAU_VIS = (TAULIQVIS+TAUICEVIS)*DZ(I,L)
              END IF

              CUMTAU(I) = CUMTAU(I) + TAU_VIS

           END DO ! I
        END DO ! L

        DO I = IL1, IL2
           CLDT(I)   = CLDT(I) + COUNT_CLDY(I)
           SUMTAU(I) = SUMTAU(I) + CUMTAU(I)
        END DO ! I

      END DO ! ICOL

      DO IB = 1, NBS
         DO I = IL1, IL2
            IF (CLDT(I) > R_ZERO) THEN
               WRKB(I,IB) = SUMTAU(I)/CLDT(I)
            ELSE
               WRKB(I,IB) = R_ZERO
            END IF
            
            CLDA(I) = CLDT(I)/R_NXLOC

! ADD IN THE AEROSOLS FROM BAND 1 (VISIBLE)
            WRKB(I,IB) = WRKB(I,IB) + WRKA(I,1)
         END DO ! I
      END DO ! IB
      
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
