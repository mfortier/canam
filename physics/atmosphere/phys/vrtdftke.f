!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE VRTDFTKE(    TIO,    QIO,    UIO,    VIO,    XIO,
     1                        UTG,    VTG,
     2                    UFSIROL,VFSIROL,UFSOROL,VFSOROL,     
     3                     UFSROL, VFSROL,    UFS,    VFS,
     4                    UFSINST,VFSINST, 
     5                       PBLT, 
     6                     CNDROL, DEPROL,   WSUB, RKMROL,RKHROL,RKQROL,
     7                     XLMTKE, TKEITM, SVAR, XLMROT,  
C
C    -------------- OUTPUTS OR UPDATED INPUTS ARE ABOVE THIS LINE,
C    -------------- INPUTS ARE BELOW.
C
     9                      GTROT,  QGROT, CDMROT, QFSROT, HFSROT,     
     A                      GTAGG,  QGAGG, CDMAGG, QFSAGG, HFSAGG,
     B                    FAREROT, PRESSG,    CRH,   ZSPD,   FLND,
     C                    CQFXROW,CHFXROW,UTENDCV,VTENDCV,
     D                    UTENDGW,VTENDGW,   TSGB,     TF,    SGJ,
     E                       SGBJ, SHTXKJ,  SHXKJ,   SHTJ,   SHBJ, 
     F                        SHJ,   DSGJ,   DSHJ,   CVSG,
     G                     ITRPHS,   ILWC,   IIWC,  IOWAT,  IOSIC,
     H                      ZTMST,   DELT,    ILG,    IL1,    IL2,
     I                      NTILE,   ILEV,    LEV,   LEVS,  NTRAC,
     J                       IPAM, ISAVLS,SAVERAD,SAVEBEG              )
C
C     * JUN 2020    - Y.HE and N. MCFARLANE, new TKE scheme version 1.
C                                                                              
C     * CALCULATE THE TENDENCIES OF U,V,LWSE,QT,X DUE TO VERTICAL DIFFUSION.
C     *                                                                        
C     *            MOMENTUM                         THERMODYNAMICS             
C     *            ========                         ==============             
C     *                                   D2SG(1)=0.                           
C     * SIGMA=0. /////////////////////////////////////////////////// SIGMA=0.  
C     *                A                B                A                     
C     * SG (1) . . . . A . . . . . . . .B                A                     
C     *                A=DSG(1)         B=D1SG(1)        A=DSH(1)              
C     *                A                B. . . . . . . . A . . . . . SH (1)    
C     * SGB(1) --------------------------                A                     
C     *                B                A=D2SG(2)        A                     
C     *                B                ---------------------------- SHB(1)    
C     * SG (2) . . . . B . . . . . . . .B                B                     
C     *                B=DSG(2)         B=D1SG(2)        B=DSH(2)              
C     *                B                B. . . . . . . . B . . . . . SH (2)    
C     * SGB(2) --------------------------                B                     
C     *                C                A=D2SG(3)        B                     
C     *                C                ---------------------------- SHB(2)    
C     * SG (3) . . . . C . . . . . . . .B                C                     
C     *                C=DSG(3)         B=D1SG(3)        C=DSH(3)              
C     *                C                B. . . . . . . . C . . . . . SH (3)    
C     * SGB(3) --------------------------                C                     
C     *                D                A=D2SG(4)        C                     
C     *                D                ---------------------------- SHB(3)    
C     * SG (4) . . . . D . . . . . . . .B                D                     
C     *                D=DSG(4)         B=D1SG(4)        D=DSH(4)              
C     *                D                B. . . . . . . . D . . . . . SH (4)    
C     * SGB(4) --------------------------                D                     
C     *                E                A=D2SG(5)        D                     
C     *                E                ---------------------------- SHB(4)    
C     * SG (5) . . . . E . . . . . . . .B                E                     
C     *                E=DSG(5)         B=D1SG(5)        E=DSH(5)              
C     *                E                B. . . . . . . . E . . . . . SH (5)    
C     *                E                B                E                     
C     * SGB(5)=1. ////////////////////////////////////////////////// SHB(5)=1. 
C     *                                                                       
      IMPLICIT NONE
C                       
C     * I/O ARRAYS.                                                           
C
      REAL   TIO    (ILG,ILEV),  QIO    (ILG,ILEV)
      REAL   UIO    (ILG,ILEV),  VIO    (ILG,ILEV)
      REAL   XIO    (ILG,LEV,NTRAC)

      REAL   UTG    (ILG,ILEV),  VTG    (ILG,ILEV)

      REAL   UFSIROL(ILG),       VFSIROL(ILG)
      REAL   UFSOROL(ILG),       VFSOROL(ILG)

      REAL   UFSROL (ILG),       VFSROL (ILG)                  
      REAL   UFSINST (ILG),      VFSINST (ILG)
      REAL   UFS    (ILG),       VFS    (ILG)
      REAL   PBLT   (ILG)

      REAL   CNDROL (ILG,ILEV),  DEPROL (ILG,ILEV),  WSUB   (ILG,ILEV)
      REAL   RKMROL (ILG,ILEV),  RKHROL (ILG,ILEV),  RKQROL (ILG,ILEV)
C
C     * INPUT FIELDS.
C
      REAL   GTROT  (ILG,NTILE), QGROT  (ILG,NTILE)
      REAL   CDMROT (ILG,NTILE), QFSROT (ILG,NTILE)
      REAL   HFSROT (ILG,NTILE), FAREROT(ILG,NTILE)

      REAL   GTAGG  (ILG),       QGAGG  (ILG)
      REAL   CDMAGG (ILG),       QFSAGG (ILG),     HFSAGG  (ILG)
      REAL   PRESSG (ILG),       CRH    (ILG),     ZSPD    (ILG)
      REAL   CQFXROW(ILG),       CHFXROW(ILG),     FLND    (ILG)

      REAL   UTENDGW(ILG,ILEV),  VTENDGW(ILG,ILEV)
      REAL   UTENDCV(ILG,ILEV),  VTENDCV(ILG,ILEV)
      REAL   TSGB   (ILG,ILEV),  TF     (ILG,ILEV)
      REAL   SGJ    (ILG,ILEV),  SGBJ   (ILG,ILEV)
      REAL   SHTXKJ (ILG,ILEV),  SHXKJ  (ILG,ILEV)
      REAL   SHTJ   (ILG, LEV),  SHBJ   (ILG,ILEV)
      REAL   SHJ    (ILG,ILEV),  DSGJ   (ILG,ILEV)
      REAL   DSHJ   (ILG,ILEV),  CVSG   (ILG,ILEV)
c TKEProg
      REAL   TKEITM (ILG,ILEV),  XLMTKE (ILG,ILEV),  SVAR  (ILG,ILEV)
      REAL   XLMROT (ILG,NTILE,ILEV)
                                                                              
      INTEGER ITRPHS(NTRAC)
C                                                                           
C     * INTERNAL WORK ARRAYS.
C
      REAL  ,  DIMENSION(ILG, LEV,NTRAC) :: XROW,CHGX
      REAL  ,  DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL  ,  DIMENSION(ILG,ILEV)  :: TH,Q,U,V,CHGT,CHGQ,CHGU,CHGV
      REAL  ,  DIMENSION(ILG,ILEV)  :: DVDS,TEND,DTTDS,RI,A,B,C
      REAL  ,  DIMENSION(ILG,ILEV)  :: WORK,RKM,RKQ,RKH,ZF,P
      REAL  ,  DIMENSION(ILG,ILEV)  :: RDZ,CHI,Z,QT,RRL,HMN
      REAL  ,  DIMENSION(ILG,ILEV)  :: QL,DST,SIGMA,DSR,CPM,PF
      REAL  ,  DIMENSION(ILG,ILEV)  :: ALMXT
      REAL  ,  DIMENSION(ILG,ILEV)  :: TKEITMIN,TKEITMX,XLMTKEX
      REAL  ,  DIMENSION(ILG,ILEV)  :: CNDROLX,DEPROLX,XLWROL,XICROL
      REAL  ,  DIMENSION(ILG,ILEV)  :: WSUBX
      REAL  ,  DIMENSION(ILG,NTRAC) :: XINT,SXFLX,XREF

      REAL  ,  DIMENSION(ILG)   :: GT,QG,CDM,QFS,HFS,FARE
      REAL  ,  DIMENSION(ILG)   :: DSMIX,HINT,CDVLH,CDVLM,VMODL,QINT
      REAL  ,  DIMENSION(ILG)   :: QTG,BCR,HMNG,CL,DVDZ,X,PBLTX
      REAL  ,  DIMENSION(ILG)   :: HEAT,RAUS,VINT,XINGLF,TVREF
      REAL  ,  DIMENSION(ILG)   :: CDVLT,ZER,UN,WRKRL,WRKR
      REAL  ,  DIMENSION(ILG)   :: UCVTS,VCVTS,UGWTS,VGWTS
      REAL  ,  DIMENSION(ILG)   :: QCW,ZCLF,ZCRAUT,SSH
      REAL  ,  DIMENSION(ILG)   :: ALMIX,DHLDZ,DRWDZ,ATUNE
      REAL  ,  DIMENSION(NTRAC) :: RKXMIN

      REAL   VPTFLX_he(ILG)
C
      REAL  ,  DIMENSION(ILG,ILEV)  :: RI_dry 
      REAL  ,  DIMENSION(ILG,ILEV)  :: TKETOT_he 
      REAL  ,  DIMENSION(ILG,ILEV)  :: PRANDTL_HE
      REAL  ,  DIMENSION(ILG,ILEV)  :: THETV_he,Fm_he 
      REAL  ,  DIMENSION(ILG,ILEV)  :: DVDZ_he,DTDZ_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: entfcn_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: zol_he, gsl_he, fim_he, fih_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: QCWX1_he
c
      REAL  ,  DIMENSION(ILG)       :: AFAMAX
      REAL  ,  DIMENSION(ILG,ILEV)  :: estar_he,el_he,yc_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: xinglsl,xinglm_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: afa_he,afa0
      REAL  ,  DIMENSION(ILG,LEV)   :: buoyf_nl    ! should this now be ILEV instead of LEV???

      REAL  ,  DIMENSION(ILG)       :: MOLENINV,tkesf_he,tausf_he
      REAL  ,  DIMENSION(ILG)       :: aN2cld_he 
      REAL  ,  DIMENSION(ILG)       :: slscalfac, afascalfac
c** elevated us layer base
      REAL  ,  DIMENSION(ILG)       :: ZUSBASE
c**

c cloud-tke 
      REAL  ,  DIMENSION(ILG,ILEV)  :: CLD_he,Ch_he,Cq_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: CLDmid
      REAL  ,  DIMENSION(ILG,ILEV)  :: xwslsum,xwqsum 

c buoyancy flux
      REAL  ,  DIMENSION(ILG,ILEV)  :: qt_he,HMN1_he
      REAL  ,  DIMENSION(ILG,ILEV)  :: Rent_he 
c DYCOMS
      REAL  ,  DIMENSION(ILG,ILEV)  :: THLIQ_he,QL_he
c
      INTEGER, DIMENSION(ILG) :: IMIX_he,IPBL_he,L_ent,L_bnd
      INTEGER, DIMENSION(ILG) :: IFIND_ENT

c p15 (TKE switch for Diagnostic=0, Prognostic=1) 
      INTEGER ITKEP
C 
      INTEGER, DIMENSION(ILG) :: IPBL,IPBLC
C
C     * SCALARS PASSED IN CALL:
C
      REAL      ZTMST,DELT,SAVEBEG,SAVERAD
      INTEGER   ILWC,IIWC,ILG,IL1,IL2,NTILE,ILEV,LEV,LEVS,NTRAC
      INTEGER   IOWAT,IOSIC,IPAM,ISAVLS
C
C     * LOCAL SCALARS.
C
      REAL      ZSECFRL,ZTHOMI,TAUADJ,ROG,ALWC,THETVP,THETVM,PBLX,
     1          FACT,RINEUT,RIINF,FACT0,SARI,ALL,XIMIN,XIMINT,
     2          ALU,ALD,FAC,XINGLH,XINGLM,ELH,ELM,EPSSH,EPSSM,
     3          FACTH,FACTM,XH,XM,ATMP,PRANDTL,SCALF,FACTS,RKMN,
     4          RKHMN,RKQMN,TODT,EST,QCWX,DZ,FRACW,FACMOM,UTMP,VTMP,
     5          QCWI,QCWL,FARETOT,ACOR,ACORT,ACOR1
      INTEGER   NTILEND,L,I,IL,ILEVM,LPBL,N,NT,ICVSG,ISUBG,IDUM,ICALL
C
C     * LOCAL SCALARS FOR TKE.
C
      INTEGER   ii,iter0,itmax,LLTOP,nrswitch
      REAL      a0,a1,a2,a3,aa,aa1,ae1,alfind,angl,ap1,ap2,app,asfact,
     1          atke,awgt,ax1,ax2,b2,bb,bb1,bb2,bcndfac,betafac,beta,
     2          cc,dd,denom,denoqq,destar,discmod,dsig_nm,
     3          dthvl,dtke,dthvu,elfac1,entfac,entscal,estar_old,
     4          estar_new,etaa,fact1,fact2,ff1,gs1,hh,pcub,prineff,qcub,
     5          qfac,ratio,rbulkl,rbulku,rcrl,rcrmean,rcru,
     4          restar,risl,ribyprsl,sfact,sfcfac,taueff,ustar,velref,
     5          vellsq,velusq,wstar,x1,x2,x3,x4,xwq,xwsl,
     5          xwslmin,xx1,yymax,zdef,zofc,zz
      REAL      aDVDS,AFAMAX_min,aN2,aN2min,Cbeta,D_afa,Rent1,
     1          RKentfac,RKH_ent,xRI2
      REAL      Cafa_he,c0_he,D_he,
     1          DQTDZ_he,DQTZ_he,DSLD_he,DSLDZ_he,DVADZ_he,DZ_he,
     2          gamma_he,HMN2_he,qt1_he,qt2_he,Sv_he,w0_he,y_he
      REAL      Chs1d_he,Chu1d_he,Cqs1d_he,Cqu1d_he
      REAL      ALDEN,ALDEN1,ALFC1,BNF,DBAR,DFAC,DFFAC,DFRES,
     1          DISC,DPBL,DRSSDT,DRSST,FRES,OMEGA1,OMEST,PRDEN,
     2          PRDFIN,PRFAC,PRNFIN,PRNUM,QCWX2,QQY,RFX,RHO,TAUMAX,
     3          TKEADJ,TKEFC,TKEFMIN,TKESCAL,TPOT,TVPOT,TX,WGT,XINF,
     4          ZBLR,ZBMAX,ZETAEST,ZETFAC,ZETFIN
C      
C     * COMMON BLOCK SCALARS.
C     
      REAL WW,TWX,AX,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES,RGASV,CPRESV
      COMMON /PARAMS/ WW,     TWX,   AX,     ASQ,  GRAV, RGAS,  RGOCP,
     1                RGOASQ, CPRES, RGASV, CPRESV

      REAL PI,RVORD,TFREZ,HS,HV,DAYLNT
      COMMON /PARAM1/ PI,     RVORD, TFREZ, HS,   HV,   DAYLNT

      REAL RGASX,RGASVX,GRAVX,SBC,VKC,CT,VMIN
      COMMON /CLASS2/ RGASX,RGASVX,GRAVX,SBC,VKC,CT,VMIN

      REAL AP,BP,EPS1,EPS2
      COMMON /EPS   / AP,BP,EPS1,EPS2

      REAL T1S,T2S,AI,BI,AW,BW,SLP
      COMMON /HTCP   / T1S, T2S, AI, BI, AW, BW, SLP      

      REAL RW1,RW2,RW3,RI1,RI2,RI3
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
C     * THERE MUST BE NO CLOUD WITHIN FIRST "LEV1" LAYERS FOR RADIATION
C     * TO WORK PROPERLY. THIS IS DEFINED IN THE "TOPLW" SUBROUTINE
C     * CALLED AT THE BEGINNING OF THE MODEL.
C
      INTEGER LEV1
      COMMON /ITOPLW/ LEV1
C
      REAL    GAMRH,GAMRM,BEEM,ALFAH,RAYON,DVMINS,TVFA,TICE
      REAL    AUNS,BETAM,BETAH
      INTEGER MSG
      DATA      GAMRH,      GAMRM,      BEEM,       ALFAH
     1       /  4.00,       4.00,       10.0,       1.0    /                  
      DATA RAYON/6.37122E06/,  DVMINS/7.9/                              
      DATA TVFA / 0.608 /, TICE/253./, MSG/0/
      DATA AUNS, BETAM, BETAH
     1    /5.  , 8.   , 2.38  / 
C
C     * STATEMENT FUNCTION TO CALCULATE SATURATION VAPOUR PRESSURE
C     * OVER WATER OR ICE.
C
      REAL    TTT,UUU,ESW,ESI
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)    = EXP(RI1+RI2/TTT)*TTT**RI3
C
      REAL ZERO,ONE,PRANDTL_MIN,PRANDTL_MAX,XLMIN,XLINF
      DATA ZERO,ONE /0., 1./
      DATA PRANDTL_MIN,PRANDTL_MAX /0.5, 3./ 
c*1204a
      DATA XLMIN,XLINF /10.,30./
c     DATA XLMIN,XLINF /5.,10./

      DATA ZBMAX /2500./
C
C     * SWITCH TO CONTROL TILING (GENERAL).
C
      INTEGER ITILVRT
      DATA ITILVRT /1/
C---------------------------------------------------------------
c itke
C
c     * NOTE:!!! In order to consider using ITKEP=1 (prognostic TKE):
c     *          1. Must add prognostic tracer with name ITKE which must be tiled.
c
      ITKEP = 0 
c*mar19      c0_he = 3.75 
      c0_he = 5.88 
c 
C*jun2018      entfac=0.30/(c0_he**1.5) 
      entfac=0.7/(c0_he**1.5)    
      aN2min = 5.e-6
      TKEFMIN=1.e-3
      D_afa=10. 
C
C     * DEFINE A COPY OF INPUT TKEITM, THEN INITIALIZE (XLM,TKEM) FOR SUBSEQUENT AGGREGATION OVER TILES FOR OUTPUT.
C     * INITIALIZE SVAR FOR SUBSEQUENT AGGREGATION FROM SIGMA OVER TILES.
C      
      DO L=1,ILEV
      DO IL=IL1,IL2
	XLMTKE (IL,L)=0.
        SVAR   (IL,L)=0.
C
        if(ITKEP.eq.0) then 
          TKEITMIN(IL,L)=0.
	else
	  TKEITMIN(IL,L)=TKEITM(IL,L)
        end if
	TKEITM (IL,L)=0.
      ENDDO
      ENDDO
C      
C     * ABORT IF "ITILVRT" IS ZERO (NO LONGER SUPPORTED).
C
      IF(ITILVRT.EQ.1) THEN
         NTILEND=NTILE
      ELSE
         CALL XIT('VRTDFTKE',-1)
      ENDIF
C
      ROG=RGAS/GRAV 
      ILEVM=ILEV-1                                                    
C
      DO IL=IL1,IL2
        UN   (IL)=1.                                                          
        ZER  (IL)=0.    
        VMODL(IL)=MAX(VMIN,ZSPD(IL))
      ENDDO
C
C     * CALCULATE LOCAL PRESSURE (MBS) AND HEIGHTS (M).
C                             
      DO 10 IL=IL1,IL2                                                  
        P (IL,1) = SHJ (IL,1)*PRESSG(IL)*0.01                       
        PF(IL,1) = SHTJ(IL,1)*PRESSG(IL)*0.01
   10 CONTINUE

      DO 20 L=2,ILEV                                                    
      DO 20 IL=IL1,IL2                                                  
        P (IL,L) = SHJ (IL,L  )*PRESSG(IL)*0.01                   
        PF(IL,L) = SHBJ(IL,L-1)*PRESSG(IL)*0.01  
   20 CONTINUE                                                          
C
      DO 30 IL=IL1,IL2                                                  
        Z (IL,ILEV) = ROG*GTAGG(IL)   *LOG(0.01*PRESSG(IL)/P (IL,ILEV))  
        ZF(IL,ILEV) = ROG*TIO(IL,ILEV)*LOG(0.01*PRESSG(IL)/PF(IL,ILEV))  
   30 CONTINUE                                                          

      DO 40 L=ILEVM,1,-1
      DO 40 IL=IL1,IL2                                                  
        Z (IL,L)=Z (IL,L+1)+ROG*TF(IL,L+1)*LOG(P (IL,L+1)/P (IL,L))   
        ZF(IL,L)=ZF(IL,L+1)+ROG*TIO(IL,L) *LOG(PF(IL,L+1)/PF(IL,L))   
   40 CONTINUE
  
C
C     * PARAMETERS FOR MOIST PHYSICS.
C
      ZSECFRL=1.E-7
      ZTHOMI=238.16
      DO L=1,ILEV
      DO IL=IL1,IL2 
        DSR(IL,L)=1.
        IF( XIO(IL,L+1,IIWC).GT.ZSECFRL ) THEN
          CHI(IL,L)=1./(1.+XIO(IL,L+1,ILWC)/XIO(IL,L+1,IIWC))
        ELSE
          IF ( XIO(IL,L+1,ILWC).GT.ZSECFRL ) THEN
            CHI(IL,L)=0.
          ELSE
C 
C           * COMPUTE THE FRACTIONAL PROBABILITY OF WATER PHASE      
C           * EXISTING AS A FUNCTION OF TEMPERATURE (FROM ROCKEL,     
C           * RASCHKE AND WEYRES, BEITR. PHYS. ATMOSPH., 1991.)       
C
            FRACW = MERGE( 1.,    
     1                  0.0059+0.9941*EXP(-0.003102*(T1S-TIO(IL,L))**2),
     2                  TIO(IL,L).GE.T1S )  
            CHI(IL,L)=1.-FRACW
          ENDIF
        ENDIF 
        RRL(IL,L)=(1.-CHI(IL,L))*HV+CHI(IL,L)*HS
        CPM(IL,L)=CPRES
      ENDDO
      ENDDO
C
C     * CALCULATE VERTICALLY-INTEGRATED GRAVITY-WAVE DRAG EFFECTS.
C     * INITIALIZE OTHER FIELDS.
C
      DO I=IL1,IL2
         UGWTS(I)=0.
         VGWTS(I)=0.
         UCVTS(I)=0.
         VCVTS(I)=0.
         RAUS (I)=PRESSG(I)/GRAV
         CDVLH(I)=0.
         CDVLT(I)=0.
         PBLT (I)=0.
      ENDDO
C
      DO L=1,ILEV
      DO I=IL1,IL2
         UGWTS(I) = UGWTS(I)+DSGJ(I,L)*UTENDGW(I,L)*RAUS(I)
         VGWTS(I) = VGWTS(I)+DSGJ(I,L)*VTENDGW(I,L)*RAUS(I)
         UCVTS(I) = UCVTS(I)+DSGJ(I,L)*UTENDCV(I,L)*RAUS(I)
         VCVTS(I) = VCVTS(I)+DSGJ(I,L)*VTENDCV(I,L)*RAUS(I)
         UTG(I,L) = UTENDGW(I,L)+UTENDCV(I,L)
         VTG(I,L) = VTENDGW(I,L)+VTENDCV(I,L)
      ENDDO
      ENDDO
C
C     * INITIALIZE DIAGNOSED STRESSES TO THAT FROM GWD AND CONVECTION ABOVE.
C
      DO I=IL1,IL2
         UFS   (I)  = UFS   (I) + (UGWTS(I)+UCVTS(I))*SAVERAD
         VFS   (I)  = VFS   (I) + (VGWTS(I)+VCVTS(I))*SAVERAD
         UFSROL(I)  = UFSROL(I) + UCVTS(I)*SAVEBEG
         VFSROL(I)  = VFSROL(I) + VCVTS(I)*SAVEBEG
         UFSINST(I) = UCVTS(I)
         VFSINST(I) = VCVTS(I)
      ENDDO
C
C     * INITIALIZE "CHG" ARRAYS.
C
      DO N=1,NTRAC
      DO L=1,LEV
      DO IL=IL1,IL2
        CHGX(IL,L,N)= 0.
      ENDDO
      ENDDO
      ENDDO

      DO L=1,ILEV
      DO IL=IL1,IL2
        CHGT(IL,L)  = 0.
        CHGQ(IL,L)  = 0.
        CHGU(IL,L)  = 0.
        CHGV(IL,L)  = 0.
      ENDDO
      ENDDO
C
      DO L=1,ILEV
      DO IL=IL1,IL2
        WSUB  (IL,L) = 0.
        RKMROL(IL,L) = 0.
        RKHROL(IL,L) = 0.
        RKQROL(IL,L) = 0.        
      ENDDO
      ENDDO
C
C     * INITIALIZE CND/DEP ARRAYS.
C
      IF ( ISAVLS.NE.0 ) THEN
        DO L=1,ILEV
        DO IL=IL1,IL2 
          CNDROL(IL,L) = 0.
          DEPROL(IL,L) = 0.
        ENDDO
        ENDDO
      ENDIF
C
C     * NOTE THAT THE FOLLOWING IS ALL CONTAINED IN THE TILING LOOP,
C     * DUE TO DEPENDANCE ON TILED INPUT FIELDS!
C
      DO 800 NT=1,NTILEND
C
C     * SET INPUT FIELDS.
C
      FARETOT=0.
      DO IL=IL1,IL2
        FARE(IL) = FAREROT(IL,NT)
        IF(FARE(IL).EQ.0.) THEN
C
C         * USE AGGREGATE VALUES AS DUMMY INPUT TO AVOID REQUIRING
C         * "IF" CONDITIONS ON "FARE" FOR EACH SUBSEQUENT DO-LOOP.
C         * ANY ZERO-TILE VALUES SUBSEQUENTLY COMPUTED WILL BE IGNORED.
C
          HFS (IL) = HFSAGG (IL)
          QFS (IL) = QFSAGG (IL)
          CDM (IL) = CDMAGG (IL)
          GT  (IL) = GTAGG  (IL)
          QG  (IL) = QGAGG  (IL)
        ELSE
          HFS (IL) = HFSROT (IL,NT)
          QFS (IL) = QFSROT (IL,NT)
          CDM (IL) = CDMROT (IL,NT)
          GT  (IL) = GTROT  (IL,NT)
          QG  (IL) = QGROT  (IL,NT)
        ENDIF
        FARETOT  = FARETOT + FARE(IL)
C
C       * INITIALIZE TKE FIELDS.
C
        DO L=1,ILEV
          TKEITMX(IL,L) = TKEITMIN(IL,L)
          XLMTKEX(IL,L) = 0.
          IF(FARE(IL).GT.0.) THEN
            XLMTKEX(IL,L) = XLMROT(IL,NT,L)
          ENDIF   
        ENDDO
      ENDDO
C
C     * SKIP CALCULATIONS IF NO TILED POINTS IN THIS PASS.
C
      IF(FARETOT.EQ.0.) GO TO 800     
C
C     * RESET WORK FIELDS TO INPUT FIELDS.
C
      DO N=1,NTRAC
      DO L=1,LEV
      DO IL=IL1,IL2
        XROW(IL,L,N)= XIO(IL,L,N)
      ENDDO
      ENDDO
      ENDDO

      DO L=1,ILEV
      DO IL=IL1,IL2
        TH  (IL,L)  = TIO(IL,L)
        Q   (IL,L)  = QIO(IL,L)
        U   (IL,L)  = UIO(IL,L)
        V   (IL,L)  = VIO(IL,L)
      ENDDO
      ENDDO
C
C     * PERFORM NON-LOCAL MIXING.
C
c-------------------- Norm's added -------------------------------
C TKE initialization
c 0122Pv3
       DO 48 L=1,ILEV
       DO 48 IL=IL1,IL2
          RI_dry(IL,L)=0.
          TKETOT_he(IL,L)=0.
          PRANDTL_HE(IL,L)=PRANDTL_MIN
          THETV_he(IL,L)=0.
          Fm_he(IL,L)=0.
          DVDZ_he(IL,L)=0.
          DTDZ_he(IL,L)=0.
          entfcn_he(IL,L)=0.
          zol_he(IL,L)=0.
          gsl_he(IL,L)=0.
          fim_he(IL,L)=0.
          fih_he(IL,L)=0.
          QCWX1_he(IL,L)=0.
          estar_he(IL,L)=0.
          el_he(IL,L)=0.
          yc_he(IL,L)=0.
          xinglsl(IL,L)=0.
          xinglm_he(IL,L)=XLMIN 
          afa_he(IL,L)=0.
          afa0(IL,L)=0.
          CLD_he(IL,L)=0.
          Ch_he(IL,L)=0.
          Cq_he(IL,L)=0.
          CLDmid(IL,L)=0.
          xwslsum(IL,L)=0.
          xwqsum(IL,L)=0.
          Rent_he(IL,L)=0.
c
          buoyf_nl(IL,L)=0.
c itke20, make sure Q>=0. (no negative value for Q)
          a1=Q(IL,L)
          Q(IL,L)=max(a1,0.)
  48   CONTINUE

       DO 50 IL=IL1,IL2
          MOLENINV(IL)=0.
          tkesf_he(IL)=0.
          tausf_he(IL)=0.
          aN2cld_he(IL)=0.
          slscalfac(IL)=0.
          afascalfac(IL)=0.
C 
          IMIX_he(IL)=ILEV
          L_bnd(IL)=ILEV

          IFIND_ENT(IL)=1 
          L_ent(IL) = ILEV  
c** elevated US layer base initialized to surface
          ZUSBASE(IL)=0.
          buoyf_nl(IL,LEV)=0.
c**
  50   CONTINUE

      DO 60 L=1,ILEV
      DO 60 IL=IL1,IL2
        ALWC=XROW(IL,L+1,ILWC)+XROW(IL,L+1,IIWC)
        aa = 1.+TVFA*Q(IL,L)/(1.-Q(IL,L))-(1.+TVFA)*ALWC
        THETV_he(IL,L)=TH(IL,L)*aa/SHXKJ(IL,L)
c prepare THLIQ_he and qt_he for buoyancy calculation
        QCWX1_he(IL,L)=(XROW(IL,L+1,ILWC)+XROW(IL,L+1,IIWC))*DSR(IL,L)
        HMN1_he(IL,L)=CPM(IL,L)*TH(IL,L)+GRAV*Z(IL,L)
     1               -RRL(IL,L)*QCWX1_he(IL,L)
c DYCOMS
        THLIQ_he(IL,L)=HMN1_he(IL,L)/CPM(IL,L)
        QL_he(IL,L)=XROW(IL,L+1,ILWC)
        qt_he(IL,L)=Q(IL,L)+QCWX1_he(IL,L)
   60 CONTINUE

c calculate DTTDS
      DO 70 I=IL1,IL2
      DO 70 L=2,ILEV
        ALWC=XROW(I,L+1,ILWC)+XROW(I,L+1,IIWC)
        THETVP=TH(I,L)*(1.+TVFA*Q(I,L)/(1.-Q(I,L))-(1.+TVFA)*ALWC)
     1        /SHXKJ(I,L)
c        ALWC1=XROW(I,L,ILWC)+XROW(I,L,IIWC) 
c        a1 = 1.+TVFA*Q(I,L-1)/(1.-Q(I,L-1))-(1.+TVFA)*ALWC1
        a1 = 1.+TVFA*Q(I,L-1)/(1.-Q(I,L))-(1.+TVFA)*ALWC
        THETVM=TH(I,L-1)*a1/SHXKJ(I,L-1)
        DTTDS(I,L)=(THETVP-THETVM)/(SHJ(I,L)-SHJ(I,L-1))
   70 CONTINUE
C
C     * PUT SURFACE FLUXES INTO SURFACE LAYER (FIRST MODEL LAYER).
C
      DO 90 IL=IL1,IL2
        RHO = 100.*P(IL,ILEV)/(RGAS*TH(IL,ILEV))
        RFX = QFS(IL)/RHO
        TPOT = TH(IL,ILEV)/SHXKJ(IL,ILEV)
        aa = 1.+TVFA*Q(IL,ILEV)
        VPTFLX_he(IL)=(HFS(IL)/(RHO*CPM(IL,ILEV)))*aa
     1           +TVFA*RFX*TPOT
   90  CONTINUE
C
C     * PERFORM NON-LOCAL MIXING.
C
c-----------------------------------------------------------------
      CALL NLCLMXTKE(XROW,Q,TH,P,Z,ZF,DSHJ,SHXKJ,
     1               QFS,CQFXROW,HFS,CHFXROW,CDM,CVSG,SIGMA,
     2               PRESSG,CRH,ITRPHS,ZTMST,GRAV,RGAS,TVFA,TFREZ,
     3               TICE,HV,HS,IIWC,ILWC,MSG,IL1,IL2,ILG,ILEV,
     4               LEV,NTRAC,QT,HMN,DSR,CHI,RRL,CPM,DSMIX,HINT,
     5               QINT,WRKR,WRKRL,XINT,SXFLX,
     6               BCR,ZER,QTG,HMNG,
     7               QCW,ZCLF,ZCRAUT,SSH,VMODL,
     8               ALMIX,DHLDZ,DRWDZ,WORK(1,1),WORK(1,2),
     9               IPBL,CNDROLX,DEPROLX,XLWROL,XICROL,ISAVLS,
     A               XLMTKEX,CLD_he)
c    
         DO I=IL1,IL2
             IMIX_he(I) = min(IPBL(I),ILEV)
             L_ent(I) = min(IPBL(I),ILEV)
         END DO
c      END DO ! istep
c
c interpolate CLD to mid-point top-down (BP2009,p3427)
         DO 98 L=2,ILEV-1 
         DO 98 I=IL1,IL2 
            a1=CLD_he(I,L-1) 
            a2=CLD_he(I,L)
            CLDmid(I,L)=min(a1,(a1+a2)*0.5)
  98   CONTINUE
         DO 100 I=IL1,IL2
            CLDmid(I,ILEV)=CLD_he(I,ILEV)
  100  CONTINUE
C
         DO 104 L=2,ILEV
         DO 104 I=IL1,IL2
         HMN2_he=HMN(I,L) 
         QCWX2=(XROW(I,L+1,ILWC)+XROW(I,L+1,IIWC))*1.0
         qt1_he=qt_he(I,L)
         qt2_he=QT(I,L)
         awgt=RGAS*dshj(I,L)/SHJ(I,L) 
c ? 03232017
c         xwsl=awgt*(HMN2_he-HMN1_he(I,L))/ZTMST
c         xwq=awgt*(qt2_he-qt1_he)/ZTMST
         xwsl=awgt*(HMN2_he-HMN1_he(I,L))/ZTMST
         xwq=awgt*(qt2_he-qt1_he)/ZTMST
         aa=1.+0.61*QT(I,L)-1.61*QCWX2
         Sv_he=CPM(I,L)*TH(I,L)*aa+GRAV*Z(I,L)  
c Ch_he and Cq_he (BP2009, p3427)
         TX=(HMN(I,L)-GRAV*Z(I,L))/CPM(I,L)
         EST=(1.-CHI(I,L))*ESW(TX)+CHI(I,L)*ESI(TX)
         DRSSDT=EPS1*P(I,L)
     1          *((1.-CHI(I,L))*ESW(TX)*(RW3-RW2/TX)
     2          +CHI(I,L) *ESI(TX)*(RI3-RI2/TX))
     3          /(TX*(P(I,L)-EPS2*EST)**2)
         gamma_he=(RRL(I,L)/CPM(I,L))*DRSSDT 
         a1=CPM(I,L)*TH(I,L)/RRL(I,L)
         Cbeta=(1.+gamma_he*a1*(1+TVFA))/(1.+gamma_he)
         Cafa_he=GRAV/Sv_he
c 
         Chs1d_he=Cafa_he*Cbeta
         Chu1d_he=Cafa_he
         Cqs1d_he=RRL(I,L)*Cafa_he
         Cqu1d_he=1.0*Cafa_he*CPM(I,L)*TVFA 
         Ch_he(I,L)=CLDmid(I,L)*Chs1d_he+(1.-CLDmid(I,L))*Chu1d_he 
         Cq_he(I,L)=CLDmid(I,L)*Cqs1d_he+(1.-CLDmid(I,L))*Cqu1d_he 
         bb=TH(I,L)/GRAV ! (T/g) 
  104 CONTINUE     
C
C-----------------------------------------------------------------------      
C     * CALCULATE THE VERTICAL DIFFUSION COEFFICIENTS, (RKH,RKM).             
C       --------------------------------------------------------              
C
      TAUADJ=ZTMST
      DO 110 I=IL1,IL2
         CDVLM(I)=CDM(I)*VMODL(I)         
         HEAT(I)=TAUADJ*GRAV*SHXKJ(I,ILEV)/(RGAS*TH(I,ILEV))
c itke99 DVDS
         asfact = 1./(0.4*MAX(Z(I,ILEV),10.))
         sfact = asfact*RGAS*TF(I,ILEV)/(GRAV*SHBJ(I,ILEVM))
         DVDS(I,ILEV)=sfact*VMODL(I)*SQRT(CDM(I))
c
         RHO=100.*P(I,ILEV)/(RGAS*TH(I,ILEV))
         RFX = QFS(I)/RHO
         RHO=100.*P(I,ILEV)/(RGAS*TH(I,ILEV))
         TPOT = TH(I,ILEV)/SHXKJ(I,ILEV)
         TVPOT=TPOT*(1.+TVFA*Q(I,ILEV))/(1.-Q(I,ILEV))
         aa = CDM(I)*(VMODL(I)**2)
         MOLENINV(I)= -VKC*GRAV*VPTFLX_he(I)/(TVPOT*(aa**1.5))
C
c  surface scaling and matching
C
         if(MOLENINV(I).ge.0.) then
            slscalfac(I)=1.
            afascalfac(I)=1.  ! 09182017 Exp3
c            afascalfac(I)=0.5 ! 09142017
            ustar=sqrt(CDM(I))*VMODL(I)
            tkesf_he(I)=c0_he*ustar**2
            velref=sqrt(c0_he*ustar**2)
            tausf_he(I)=ZF(I,L_ent(I))/velref
         else
            DPBL=ZF(I,L_ent(I))
            aa = GRAV*VPTFLX_he(I)/(TVPOT/DPBL)
            wstar=max(aa,ZERO)**(1./3.)
            ustar=sqrt(CDM(I))*VMODL(I)
            bcndfac=(1.+0.2*(wstar**2)/(3.75*ustar**2))
c            slscalfac(I)=1./(bcndfac**1.5)
            slscalfac(I)=1. ! 1010ref2
c
c            denom=sqrt(c0_he*ustar**2+0.2*wstar**2)
            denom=sqrt(c0_he*ustar**2) ! 1109ref1
            velref=max(ustar+0.6*wstar, 1.e-3)
            afascalfac(I)=1.0 ! 09182017 Exp3
c            afascalfac(I)=0.5 ! 09142017
            tkesf_he(I)=denom**2
            tausf_he(I)=DPBL/denom
         end if
       Do 108 L=1,ILEV
          zol_he(I,L) = ZF(I,L)*MOLENINV(I) 
         if(MOLENINV(I).ge.0.) then 
          aa = 4. 
          bb = 0.167 
          cc = 5.
          dd = 0.35
          a1=aa 
          fim_he(I,L) = 1.+zol_he(I,L)*a1 
          a2 = aa*(1.+aa*zol_he(I,L)*2./3.)**0.5
          b2 = bb*(1.+cc-dd*zol_he(I,L))*exp(-dd*zol_he(I,L))
          fih_he(I,L) = 1. + zol_he(I,L)*a2 
        else 
          fim_he(I,L) = (1.-16.*zol_he(I,L))**(-0.25)
          fih_he(I,L) = (1.-8.*zol_he(I,L))**(-0.5) 
        endif
          risl=zol_he(I,L)*fih_he(I,L)/(fim_he(I,L)**2) 
          ribyprsl = zol_he(I,L)/fim_he(I,L)
c 1205a
         zofc=1.
          IF(MOLENINV(I).GE.0.) THEN
c*sep10   DENOM=1.+2.*ribyprsl/(1.-2.875*ribyprsl)
          DENOM=(1.-4.*ribyprsl/3.)/(1.-10.*ribyprsl/3.)             
          gs1=(1.-ribyprsl)/(DENOM**2)
          gsl_he(I,L)=(gs1**(3./4.))*(1-ribyprsl)**(1./4.)
          ELSE
          gsl_he(I,L)=1./fim_he(I,L)
          ENDIF
  108 CONTINUE
c itke99 DVDS
         aDVDS = DVDS(I,ILEV)
         DVDS(I,ILEV)=aDVDS*fim_he(I,ILEV)
         THETV_he(I,1) = TH(I,1)/SHXKJ(I,1)
  110 CONTINUE                             
C                                                                             
C     * EVALUATE DTTDS=D(THETA)/D(SIGMA) AT INTERFACE OF THERMODYNAMIC        
C     * LAYERS.                                                               
C
      xwslmin=-5.0 
      DO 120 L=MSG+2,ILEV  
      DO 115 I=IL1,IL2   
c TKEv1 Yanping p15
         QCWX2=(XROW(I,L+1,ILWC)+XROW(I,L+1,IIWC))*DSR(I,L) 
         HMN2_he=HMN(I,L)
         aa=1.+0.61*QT(I,L)-1.61*QCWX2
         Sv_he=CPM(I,L)*TH(I,L)*aa+GRAV*Z(I,L) 
         qt1_he=qt_he(I,L)
         qt2_he=QT(I,L)
c*****
c*****         awgt=RGAS*dshj(I,L-1)/SHXKJ(I,L)
         awgt=dshj(I,L)
c*****
         xwsl=awgt*(HMN2_he-HMN1_he(I,L))/ZTMST 
         xwq=awgt*(qt2_he-qt1_he)/ZTMST 
c
c**      evaluate the nonlocal component of the buoyancy flux at the
c        *lower* interfaces of the layers affected by nonlocal mixing**
c         
         IF(L.LT.ILEV) THEN
         bb=RGAS*TF(I,L)/(GRAV*SHTJ(I,L))
         ELSE
         bb=RGAS*TH(I,L)/GRAV
         ENDIF 
c         xwslsum(I,L)=xwslsum(I,L-1)+max(xwsl,xwslmin)
         xwslsum(I,L)=xwslsum(I,L-1)+xwsl ! 1109ref1
         xwqsum(I,L)=xwqsum(I,L-1)+xwq
         aa=Cq_he(I,L)*xwqsum(I,L) 
         buoyf_nl(I,L+1)=(Ch_he(I,L)*xwslsum(I,L)+ aa )*bb       
  115 CONTINUE
  120 CONTINUE
c 
c     * DTTDS
c
c      DO 128 L=2,ILEV
      DO 128 L=MSG+2,ILEV 
      DO 128 I=IL1,IL2
          ALWC=XROW(I,L+1,ILWC)+XROW(I,L+1,IIWC)
          a1=1.+TVFA*Q(I,L)/(1.-Q(I,L))-(1.+TVFA)*ALWC
          THETVP=TH(I,L)*a1/SHXKJ(I,L)
          ALWC=XROW(I,L,ILWC)+XROW(I,L,IIWC)
          a1 = 1.+TVFA*Q(I,L-1)/(1.-Q(I,L-1))-(1.+TVFA)*ALWC
          THETVM=TH(I,L-1)*a1/SHXKJ(I,L-1)
          DTTDS(I,L)=(THETVP-THETVM)/(SHJ(I,L)-SHJ(I,L-1))
          THETV_he(I,L)=THETVP
  128 CONTINUE
C DVDS calculated in 107-loop 04052017
C     * DVDS = MOD(DV/DSIGMA) AT THE **BOTTOM** INTERFACE OF MOMENTUM LAYERS. 
C
C itke99 DVDS
c      DO 130 L=1,ILEV-2
      DO 130 L=1,ILEVM
      DO 130 I=IL1,IL2                                                 
         DVDS(I,L)=SQRT((U(I,L+1)-U(I,L))**2+                          
     1                  (V(I,L+1)-V(I,L))**2)/(SGJ(I,L+1)-SGJ(I,L))    
  130 CONTINUE                                                         
C                    
      DO 140 I=IL1,IL2                                                 
         PBLTX(I) = REAL(IPBL(I))
         IPBLC(I)=IPBL(I)
         IPBL(I)=1   
c itke99 DVDS comment out
c         DVDS(I,ILEV)=DVDS(I,ILEVM)      
  140 CONTINUE       
C                                                                             
C     * INTERPOLATE DVDS TO ** TOP ** INTERFACE OF THERMODYNAMIC LAYERS.      
C     * RMS WIND SHEAR, DVMINS=(R*T/G)*(DV/DZ)=7.9 FOR DV/DZ=1.MS-1KM-1.      
C     * DETERMINE PLANETARY BOUNDARY LAYER TOP AS LEVEL INDEX ABOVE           
C     * WHICH THE RICHARDSON NUMBER EXCEEDS THE CRITICAL VALUE OF 1.00.       
C 
      DO 150 L=ILEV,MSG+2,-1
      DO 150 I=IL1,IL2    
         aa=DVDS(I,L)
         DVDS(I,L)=MAX( DVMINS/SHBJ(I,L-1),
     1                   (DVDS(I,L-1)*(SGBJ(I,  L)-SHBJ(I,L-1)) 
     2                   +DVDS(I,L  )*(SHBJ(I,L-1)-SGBJ(I,L-1)))
     3                               / DSGJ(I,  L) )
         DZ_he=Z(I,L-1)-Z(I,L)
         DSLDZ_he=(HMN(I,L-1)-HMN(I,L))/DZ_he
         DQTDZ_he=(QT(I,L-1)-QT(I,L))/DZ_he
c method: A and B (JAS1993, v50,p3906)
         aN2cld_he(I)=Ch_he(I,L)*DSLDZ_he+Cq_he(I,L)*DQTDZ_he
         DVADZ_he=DVDS(I,L)*GRAV*SHBJ(I,L-1)/(RGAS*TF(I,L))
         xRI2=aN2cld_he(I)/(DVADZ_he**2)
         RI_dry(I,L)=-RGAS*SHTXKJ(I,L)*DTTDS(I,L)
     1              /(SHBJ(I,L-1)*DVDS(I,L)**2)
         RI(I,L)=xRI2
c
         IF(RI(I,L).LT.1.00 .AND. IPBL(I).EQ.1) THEN
            PBLX=REAL(L-1)
c**rev            PBLTX(I) = MIN(PBLTX(I), PBLX)
c           PBLTX(I) = PBLX 
         ELSE
            IPBL(I)=0
         ENDIF
  150 CONTINUE       
C      
      FACT=VKC*RGAS*273.                        
      RINEUT=1.
      RIINF=0.25
      XINF=75.0
      AFAMAX_min=1.e-3
      betafac=1.
      EPSSH = 1./(RIINF*0.5*BEEM)
      EPSSM = 1./(RIINF*0.5*BEEM)

      DO 160 I=IL1,IL2
         IPBL_he(I)=NINT(PBLTX(I))  
c**rev         L_ent(I)=IPBL_he(I)
c**rev         IMIX_he(I)=L_ent(I)
         AFAMAX(I)=AFAMAX_min 
  160 CONTINUE
c p15
       DO 220 L=ILEV,2,-1 
C                                                                              
C        * CALCULATE THE DIFFUSION COEFFICIENTS (RKH,RKM)                    
C        * AT THE ** TOP ** INTERFACE OF THERMODYNAMIC LAYERS.             
C                                                                             
C        * HEAT: FINITE STABILITY CUTOFF.                                     
C        * LOW XINGL USED SINCE PREVIOUSLY MIXED "INSTANTANEOUSLY".         
C
C        * MOMENTUM: FINITE STABILITY CUTOFF.
C        * HIGH XINGL USED SINCE NOT PREVIOUSLY MIXED "INSTANTANEOUSLY".     
C
C        * UNSTABLE CASE (RI <= 0.) - STABLE CASE (RI > 0.) .                  
C
         DO 180 I=IL1,IL2                                               
            DVDZ(I)=DVDS(I,L)*GRAV*SHBJ(I,L-1) / (RGAS*TF(I,L))        
            DVDZ_he(I,L) = DVDZ(I) 
            DTDZ_he(I,L) = DTTDS(I,L)*GRAV*SHBJ(I,L-1)/(RGAS*TF(I,L)) 
c track 04052017
c               if(L.gt.48) then
c            write(*,*)L,'DVDZ=',DVDZ(I)
c               end if
  180    CONTINUE
C
        DO 200 I=IL1,IL2                       
            FACT0=FACT*LOG(SHBJ(I,L-1))                             
            SARI=SQRT(ABS(RI(I,L)))
            LPBL=NINT(PBLTX(I))
            if(MOLENINV(I).LT.0.) then
              sfcfac=slscalfac(I)
            else
              sfcfac=1.
            endif
c*sep10     IF(RI(I,L).GT.0.) THEN
c*sep10       OMEGA1=RI(I,L)/RIINF
c*sep10       OMEST=OMEGA1*(1.+1.5*OMEGA1)
c*may4
c*sep10       PRFAC=OMEST*(sqrt(1.+2.*OMEST/3.)-1.)
c*sep10       PRANDTL=1.+PRFAC/(1.+OMEST)
c*sep10     ELSE
c*sep10       PRNUM=1.+16.*abs(RI(I,L))
c*sep10       PRDEN=1.+8.*abs(RI(I,L))
c*sep10       ZETAEST=RI(I,L)*sqrt(PRDEN/PRNUM)
c*sep10       ZETFAC=sqrt((1.-16.*ZETAEST)/(1.-8.*ZETAEST))
c*sep10       FRES=ZETAEST*ZETFAC-RI(I,L)
c*sep10       DFFAC=4.*ZETAEST/((1.-16.*ZETAEST)*(1.-8.*ZETAEST))
c*sep10       DFRES=ZETFAC*(1.-DFFAC)
c*sep10       ZETFIN=ZETAEST-FRES/DFRES
c*sep10       PRNFIN=(1.-16.*ZETFIN)**.25
c* sep10      PRDFIN=sqrt(1.-8.*ZETFIN)
c itke2
c             PRANDTL=PRNFIN/PRDFIN
c*sep10       PRANDTL=max(PRANDTL_MIN,PRNFIN/PRDFIN)
c*sep10     ENDIF
c*sep10     RANDTL_HE(I,L)=PRANDTL
            IF(RI(I,L).GT.0.) THEN
              OMEGA1=RI(I,L)/RIINF
              OMEST=OMEGA1*(1.+1.5*OMEGA1)
              PRFAC=OMEST*(sqrt(1.+2.*OMEST/3.)-1.)
              PRANDTL=1.+PRFAC/(1.+OMEST)
              PRANDTL_HE(I,L)=PRANDTL
              a1=RI(I,L)/PRANDTL
c***          DENOM=1.+2.*a1/(1.-2.875*a1)
              DENOM=(3.-4.*a1)/(3.-10.*a1)
              FM_he(I,L)=(1.-a1)/(DENOM**2)

           else
              PRNUM=1.-AUNS*RI(I,L)/(1.-BETAM*RI(I,L))
              PRDEN=1.-AUNS*RI(I,L)/(1.-BETAH*RI(I,L))
              PRANDTL= (PRNUM/PRDEN)**1.5
              PRANDTL_HE(I,L)=PRANDTL
              FM_he(I,L) = PRNUM
           end if
  200  CONTINUE
  220  CONTINUE
 
c prepare iteration
       ii = 0
       DO 280 I=IL1,IL2
c
c TKEv1: diagnose L_ent based on IMIX and Ri
c
c*rev        L_bnd(I)=L_ent(I)
        L_bnd(I)=MIN(L_ent(I),ILEVM)
c
          DO 245 L=ILEV,2,-1
       IF(IFIND_ENT(I).eq.1) THEN
         aa=THETV_he(I,L-1)/THETV_he(I,ILEV)
         dthvu=GRAV*(aa-1.)
         velusq=U(I,L-1)**2+V(I,L-1)**2
c 1025ref3
         rbulku=dthvu*Z(I,L-1)/max(velusq,.01)
         dthvl=GRAV*(THETV_he(I,L)/THETV_he(I,ILEV)-1.)
         vellsq=U(I,L)**2+V(I,L)**2
         rbulkl=dthvl*Z(I,L)/max(vellsq,.01) 
         aa=ZF(I,L)*max(MOLENINV(I),1.e-3) 
         rcrmean=min(0.045*aa,0.4)
         rcru=max(rcrmean,0.01) 
         rcrl=max(rcrmean,0.01)
         if((rbulkl.lt.rcrl).and.(rbulku.ge.rcru)) then
         L_bnd(I)=min(L_ent(I),L) 
           IFIND_ENT(I)=2
         end if
         if(Z(I,L).GE.ZBMAX) then
           L_bnd(I)=L
           IFIND_ENT(I)=3
         end if        
c end 1025ref3
       END IF
  245  CONTINUE
  246  CONTINUE
c
       ZUSBASE(I)=ZF(I,L_bnd(I))
       if(IFIND_ENT(I).lt.2) then
          write(*,*)'can not find L_bnd up to 3rd level'
          stop
       end if
c
       IPBL_he(I)=L_ent(I)

c 1214 (using L_bnd not L_ent for alfind control (using L_ent will turn off TKE transport when 
c no non-local mixing)
       if((HFS(I).lt.0.).or.(L_bnd(I).ge.(ILEV-3))) then
c       if((HFS(I).lt.0.).or.(L_ent(I).ge.(ILEV-3))) then 
          alfind=1.
       else
          alfind=0.
       end if
       DO 250 L=ILEV,1,-1
          afa0(I,L)=afascalfac(I)*(1.-alfind)
          entscal=entfac*afa0(I,L)
          entfcn_he(I,L)=entscal*c0_he**1.5
  250  CONTINUE
c
c TKEv1: define inner (surface layer) mixing length
c
          DO 260 L=ILEV,2,-1
            ALU=VKC*ZF(I,L)
	    DPBL=ZF(I,L_ent(I))
c TKEv1: merge with B&H sfcl formulation instead
c            xinglsl(I,L)=ALU/(gsl_he(I,L)*fim_he(I,L))
           aa=1.0
           xinglsl(I,L)=aa*ALU/(gsl_he(I,L)*fim_he(I,L))
  260    CONTINUE
c
          hh=max(ZF(I,L_bnd(I)),ZF(I,ILEVM))
          yymax=1.
c 1204c
          DO 265 L=ILEV,2,-1
            ratio=ZF(I,L)/hh
            ALDEN1=1.
            ALDEN=1.+ALDEN1*ratio
            ALD=(XLMIN*(ALDEN-1.)+.26*hh)/ALDEN
            xinglm_he(I,L)=xinglsl(I,L)*ALD/(xinglsl(I,L)+ALD)
c TKEv1: define boundary layer mixing length.
c**set local equilibrium TKE ***
            cc= Fm_he(I,L)*(1.-RI(I,L)/PRANDTL_HE(I,L))
            bb = sqrt(c0_he)*xinglm_he(I,L)*DVDZ_he(I,L)
            el_he(I,L) = max(0.,bb*bb*cc)
  265  CONTINUE
c*apr20
c*apr20 revised version of the alpha profile
c*apr20
       DO 255 L=ILEV,2,-1
        LLTOP=L_bnd(I)-2
        IF(L.ge.LLTOP) THEN
          zdef=xinglm_he(I,L)/xinglm_he(I,ILEVM)
          afa_he(I,L)=afa0(I,L)*tanh(zdef)
        ELSE
          afa_he(I,L)=0.
        END IF
        AFAMAX(I)=MAX(AFAMAX(I),afa_he(I,L))       
  255  CONTINUE

         DO 268 L=ILEV,2,-1
            aa=1.+afa_he(I,L)*(c0_he**1.5)
            yc_he(I,L)=sqrt(1./aa)
            QQY=0.
            denoqq=1.
c
c TKEv1: local equlibrium solution for TKE with Kh enhancement
           cc= Fm_he(I,L)*(1.-RI(I,L)/PRANDTL_HE(I,L))
           bb = sqrt(c0_he)*xinglm_he(I,L)*DVDZ_he(I,L)
           el_he(I,L) = max(0.,bb*bb*cc)
           TAUMAX=0.
           bb=TAUMAX*sqrt(c0_he*DVDZ_he(I,L)**2)
           elfac1=bb*bb*max(RI(I,L),0.)
           dz_he=Z(I,L)-Z(I,ILEV)
           aa1=1.-exp(-dz_he/100.)
           etaa=(3.-2.*aa1)*(aa1**2)
           taueff=etaa*TAUMAX/sqrt(1.+elfac1)
c
           aN2 = RI(I,L)*(DVDZ_he(I,L)**2)
           D_he = (1./yc_he(I,L))**2
           ax1=taueff*aN2*c0_he*xinglm_he(I,L)*Fm_he(I,L)/D_he
           ax2=xinglm_he(I,L)*sqrt(c0_he*max(aN2,0.))/D_he
           if(aN2.gt.aN2min) then
             aa1 = afa_he(I,L)*entfcn_he(I,L)*yc_he(I,L)**2
c in prognostic TKE eq.,add TKEITM term
             if(ITKEP.eq.1) then 
                 app = xinglm_he(I,L)*(c0_he**1.5)/DELT/D_he
             else
                 app=0.
             end if
c 1115ref1
c             xx1 = aa1*sqrt(c0_he*aN2)*xinglm_he(I,L) + app 
             xx1 = aa1*sqrt(aN2)*xinglm_he(I,L) + app
c
           else
             xx1 = 0.
           end if
c
           a1=afa_he(I,L)*tkesf_he(I)*c0_he**1.5
           TKETOT_he(I,L)=(el_he(I,L)+a1)/D_he
           ae1=TKETOT_he(I,L)+0.25*xx1**2
           estar_he(I,L)=(-0.5*xx1+sqrt(ae1))**2
c
           if(ITKEP.eq.1) then
             app = TKEITMX(I,L)/DELT
           else
             app = 0.
           end if
           BNF=max(buoyf_nl(I,L),0.) + app           
c      
          TKEFC = xinglm_he(I,L)*(c0_he**(1.5))*BNF/D_he
          TKESCAL=TKETOT_he(I,L)**1.5
          aa = max(TKEFMIN,0.05*TKESCAL)
          IF(abs(TKEFC).gt.aa) THEN
            qfac=TKETOT_he(I,L)+2.*xx1*xx1/9.
            qcub=TKEFC-xx1*qfac/3.
            pcub=TKETOT_he(I,L)+xx1*xx1/3.
            DISC=0.25*qcub**2-(pcub**3)/27.
            angl=0.
            discmod=0.
            if(DISC.gt.0.) then
              x1 = qcub/2. + sqrt(DISC)
              x2 = qcub/2. - sqrt(DISC)
              zz = x1**(1./3.) + max(0.,x2)**(1./3.)
              TKEADJ = zz - xx1/3.
            else
              discmod=sqrt(max(-DISC,0.))
              pi=4.*atan(1.)
              if(qcub.gt.0.) then
                angl=atan(2.*discmod/abs(qcub))
              else
                angl=pi-atan(2.*discmod/abs(qcub))
              end if 
              zz = (2./sqrt(3.))*sqrt(pcub)*cos(angl/3.)
              TKEADJ =  zz - xx1/3.
            end if 
            estar_he(I,L)=TKEADJ**2
          END IF
  268  CONTINUE

c       initial estimates of kstar, improve later

        itmax=25  
        nrswitch=1 

c     no TKE transport related estar search if the transport term
c     is small or the mixed layer depth only includes the bottom level

       IF((AFAMAX(I).gt.0.1).and.(L_ent(I).lt.ILEV)) THEN 
          iter0 = 0 
          estar_old=tkesf_he(I)
 1010   continue
          x1 = 0.
          x2 = 0.
          x3 = 0.
          x4 = 0.
          
c          write(*,*)'v bef loop-270'
          DO 270 L=ILEV,2,-1
            dsig_nm =Z(I,L-1)-Z(I,L) 
            a1 = 0.
            aa = 1. + afa_he(I,L)*c0_he**1.5 + a1 
            a1 = afa_he(I,L)*c0_he**1.5
            a3 = afa_he(I,L)*dsig_nm/xinglm_he(I,L)
            w0_he = a3*sqrt(estar_he(I,L))
            x1 = x1 + w0_he
            x2 = x2 + w0_he*estar_he(I,L)
c TKEv1: switch to Newton-Raphson iteration after the 3rd iteration if not converged
c
            IF(nrswitch.eq.1) THEN
c
c
c            if((iter0.gt.5).and.(estar_old.gt.1.e-6)) then
            if((iter0.gt.2).and.(estar_old.gt.1.e-6)) then
              TAUMAX=0.
              bb=TAUMAX*sqrt(c0_he*DVDZ_he(I,L)**2)
              elfac1=bb*bb*max(RI(I,L),0.)
              dz_he=Z(I,L)-Z(I,ILEV)
              aa1=1.-exp(-dz_he/100.)
              etaa=(3.-2.*aa1)*(aa1**2)
              taueff=etaa*TAUMAX/sqrt(1.+elfac1)
              aN2 = RI(I,L)*(DVDZ_he(I,L)**2)
              D_he = (1./yc_he(I,L))**2
              ax1=taueff*aN2*c0_he*xinglm_he(I,L)*Fm_he(I,L)/D_he
              ax2=xinglm_he(I,L)*sqrt(c0_he*max(aN2,0.))/D_he
              if(aN2.gt.aN2min) then
                aa1 = afa_he(I,L)*entfcn_he(I,L)*yc_he(I,L)**2
                if(ITKEP.eq.1) then 
                  app = xinglm_he(I,L)*(c0_he**1.5)/DELT/D_he
                else
                  app = 0.
                end if
c 1115ref1
c                xx1 = aa1*sqrt(c0_he*aN2)*xinglm_he(I,L) + app 
                xx1 = aa1*sqrt(aN2)*xinglm_he(I,L) + app
c
              else
                xx1 = 0.
              end if
              bb1=3.*aa*estar_he(I,L)
              bb2=2.*aa*xx1*estar_he(I,L)**0.5
              ff1=bb1+bb2-TKETOT_he(I,L)*aa
              fact1=a1/max(ff1,1.e-6)
              fact2=estar_old-3*estar_he(I,L)
              x3=x3+w0_he*(1.+fact1*fact2)
              x4=x4+w0_he*(estar_old-estar_he(I,L))
            end if 

           ENDIF

  270  CONTINUE
c
         IF(x1.gt.0.) THEN
c 0229Pv2
           if(nrswitch.eq.1) then
c           if(nrswtich.eq.1) then
             if((iter0.gt.2).and.(min(x1,x3).gt.0.)) then
               estar_new=estar_old-x4/x3
             else
               estar_new = x2/x1
             end if
           else
             estar_new=x2/x1
           end if 
         ELSE
           estar_new=tkesf_he(I)
         END IF
c end NR iteration
c
c x4: vertical integrated TKE transport term
c         write(*,*)'v loop-275 ok'

         DO 275 L=ILEV,2,-1
           cc=Fm_he(I,L)*(1.-RI(I,L)/PRANDTL_HE(I,L))
           bb=sqrt(c0_he)*xinglm_he(I,L)*DVDZ_he(I,L)
           el_he(I,L)=max(0.,bb*bb*cc)
           TAUMAX=0.
           bb=TAUMAX*sqrt(c0_he*DVDZ_he(I,L)**2)
           elfac1=bb*bb*max(RI(I,L),0.)
           dz_he=Z(I,L)-Z(I,ILEV)
           aa1=1.-exp(-dz_he/100.)
           etaa=(3.-2.*aa1)*(aa1**2)
           taueff=etaa*TAUMAX/sqrt(1.+elfac1)
           bb1=sqrt(c0_he)*DVDZ_he(I,L)*yc_he(I,L)
           aN2 = RI(I,L)*(DVDZ_he(I,L)**2)
           D_he = (1./yc_he(I,L))**2
           ax1=taueff*aN2*c0_he*xinglm_he(I,L)*Fm_he(I,L)/D_he
           ax2=xinglm_he(I,L)*sqrt(c0_he*max(aN2,0.))/D_he
           IF(aN2.gt.aN2min) THEN
              aa1 = afa_he(I,L)*entfcn_he(I,L)*yc_he(I,L)**2
              if(ITKEP.eq.1) then 
                app = xinglm_he(I,L)*(c0_he**1.5)/DELT/D_he
              else
                app = 0.
              end if
c 1115ref1
c              xx1 = aa1*sqrt(c0_he*aN2)*xinglm_he(I,L) + app 
             xx1 = aa1*sqrt(aN2)*xinglm_he(I,L) + app
           ELSE
             xx1 = 0.
           END IF
           a1=afa_he(I,L)*estar_new*c0_he**1.5
           TKETOT_he(I,L)=(yc_he(I,L)**2)*(el_he(I,L)+a1)
           ae1 = TKETOT_he(I,L)+0.25*xx1**2
           estar_he(I,L)=(-0.5*xx1+sqrt(ae1))**2
           BNF=max(buoyf_nl(I,L),0.)
c 
          TKEFC = xinglm_he(I,L)*(c0_he**(1.5))*BNF/D_he
          TKESCAL=TKETOT_he(I,L)**1.5
          angl=0.
          discmod=0.
          IF(abs(TKEFC).gt.TKEFMIN) THEN
            qfac=TKETOT_he(I,L)+2.*xx1*xx1/9.
            qcub=TKEFC-xx1*qfac/3.
            pcub=TKETOT_he(I,L)+xx1*xx1/3.
            DISC=0.25*qcub**2-(pcub**3)/27.
            if(DISC.gt.0.) then
              x1 = qcub/2. + SQRT(DISC)
              x2 = qcub/2. - SQRT(DISC)
              zz = x1**(1./3.) + max(0.,x2)**(1./3.)
              TKEADJ = zz - xx1/3.
            else
                discmod=sqrt(max(-DISC,0.))
                pi=4.*atan(1.)
                if(qcub.gt.0.) then
                  angl=atan(2.*discmod/abs(qcub))
                else
                  angl=pi-atan(2.*discmod/abs(qcub))
                end if 
                zz = (2./sqrt(3.))*sqrt(pcub)*cos(angl/3.)
                TKEADJ =  zz - xx1/3.
            end if 
            estar_he(I,L)=TKEADJ**2
          END IF

  275  CONTINUE
c
         destar = abs(estar_new - estar_old)
         restar = destar/max(abs(estar_new),0.1)
         if(iter0.ge.20) then
            a1=estar_new
            a2=estar_old
         end if
c 0226Pv2
         if((iter0.lt.itmax).and.(restar.gt.0.05)) then 
c         if((iter0.lt.itmax).and.(destar.gt.0.1)) then ! 0503
           iter0 = iter0 + 1
           estar_old=estar_new
           go to 1010
         else if((iter0.ge.itmax).and.(restar.gt.0.05)) then 
         write(*,*)I,iter0,'v: estar= ',estar_new,estar_old,restar
         write(*,*)I,'HFS, Lent, GT= ',HFS(I),L_ent(I),GT(I)
          stop
         end if
       END IF
C
C update LPBL based on L_ent
        PBLX =L_ent(I)*1.0
        IPBLC(I) = L_ent(I)
        IPBL_he(I) = L_ent(I)
c itke13Lent
c        PBLTX(I) = L_ent(I)*1.0
c        LPBL = L_ent(I)
c itke13Lbnd
        PBLTX(I) = L_bnd(I)*1.0
        LPBL = L_bnd(I)
c
  280  CONTINUE
C
C     * SET A LOWER BOUND TO RK.
C
      RKMN=0.1
      RKHMN=0.1
      RKQMN=0.1
c
      DO 290 I=IL1,IL2
         RKM(I,1)=RKMN
         RKH(I,1)=RKHMN
         RKQ(I,1)=RKQMN
c
      DO 285 L=ILEV,2,-1
         TKETOT_he(I,L)=estar_he(I,L)
c
         cc = sqrt(TKETOT_he(I,L)/c0_he)*Fm_he(I,L)
         RKM(I,L)=max(xinglm_he(I,L)*cc,0.)
         a0=xinglm_he(I,L)
         a1=TKETOT_he(I,L)
         a2=RI(I,L)
         a3=Fm_he(I,L)
         atke = TKETOT_he(I,L)
         aN2 = RI(I,L)*(DVDZ_he(I,L)**2)
         aa=max(aN2,aN2min)*xinglm_he(I,L) 
c itke44 reduce entfac effect when PBL deep
         ZBLR = max(Z(I,L_ent(I))/200.,1.0)
c         ENTR=2.*ZBLR/(1.+ZBLR**2)
         RKentfac=afa_he(I,L)*entfac
         Rent1 = RKentfac*TKETOT_he(I,L)*sqrt(1./max(aN2,aN2min))
c 1110ref1
         RKH_ent = Rent1
         Rent_he(I,L)=RKH_ent 
c
         TAUMAX = 0.
         bb=TAUMAX*sqrt(c0_he*DVDZ_he(I,L)**2)
         elfac1=bb*bb*max(RI(I,L),0.)
         dz_he=Z(I,L)-Z(I,ILEV)
         aa1=1.-exp(-dz_he/100.)
         etaa=(3.-2.*aa1)*(aa1**2)
         taueff=etaa*TAUMAX/sqrt(1.+elfac1)
         ap1=1./PRANDTL_HE(I,L) 
         ap2=taueff*sqrt(TKETOT_he(I,L))/xinglm_he(I,L)
         prineff = ap1 + ap2
         RKH(I,L)=RKM(I,L)*prineff + RKH_ent
c
         ALFC1=SQRT((c0_he**3)/MAX(TKETOT_he(I,L),1.E-6))
         ALMXT(I,L)=SQRT(ALFC1*xinglm_he(I,L)*RKH(I,L))
C---------------------------
c Update XLMTKE and TKEITM; ZERO OUT VALUES FOR TILES WITH NO FRACTION FOR OUTPUT PURPOSES.            
c
         IF(FARE(I).GT.0.) THEN                                                                                                                                   
           TKEITMX(I,L)=TKETOT_he(I,L)
c**        XLMTKEX(I,L)=xinglm_he(I,L)
           XLMTKEX(I,L)=ALMXT(I,L)
         ELSE
           TKEITMX(I,L)=0.
           XLMTKEX(I,L)=0.
           SIGMA  (I,L)=0.
         ENDIF   
         XLMROT(I,NT,L)=XLMTKEX(I,L)
  285  CONTINUE
  290  CONTINUE
C
c======= end p15===========
C                                                                             
      DO 300 I=IL1,IL2                                                        
         RKM(I,1)=RKM(I,2)                
         RKH(I,1)=RKH(I,2)                           
  300 CONTINUE          
C                                                       
C     * DEFINE RKQ ALSO AT ** TOP ** INTERFACE OF THERMODYNAMIC LAYERS.       
C                                                                             
      DO 315 L=1,ILEV                                                         
      DO 315 I=IL1,IL2                                             
         RKQ(I,L)=RKH(I,L)                                                
  315 CONTINUE                  
C
C     * DIAGNOSE SUBGRID-SCALE COMPONENT OF THE VERTICAL VELOCITY
C     * (STANDARD DEVIATION, GHAN ET AL., 1997). ACCORDING TO PENG
C     * ET AL. (2005), THE REPRESENTATIVE VERTICAL VELOCITY FOR
C     * AEROSOL ACTIVATION IS OBTAINED BY APPLYING A SCALING FACTOR 
C     * TO THE VELOCITY STANDARD DEVIATION (SCALF).
C
      ATUNE=FLND*1.+(1.-FLND)*0.05
      SCALF=0.7
      FACTS=SCALF*SQRT(2.*PI)
      DO L=1,ILEVM
        WSUBX(IL1:IL2,L)=FACTS*ATUNE(IL1:IL2)*RKH(IL1:IL2,L)
     1                  /(ZF(IL1:IL2,L)-ZF(IL1:IL2,L+1))
      ENDDO
      WSUBX(IL1:IL2,ILEV)=FACTS*ATUNE(IL1:IL2)*RKH(IL1:IL2,ILEV)
     1                   /ZF(IL1:IL2,ILEV)
      WSUBX(IL1:IL2,:)=MIN(MAX(0.01,WSUBX(IL1:IL2,:)),.5)
C
C     * INTERPOLATE RKM TO ** BOTTOM ** INTERFACE OF MOMENTUM LAYERS.
C     * THIS IS REQUIRED FOR USE IN THE ROUTINE "ABCVDM6" WHICH WAS
C     * ORIGINALLY WRITTEN FOR GCMI STAGGERED LAYER SCHEME AND WHICH
C     * REQUIRES RKM TO BE DEFINED AT THE BASE OF THE MOMENTUM LAYER.
C
      DO 321 L=1,ILEVM                                                    
         DO 320 I=IL1,IL2                                                 
            RKM(I,L)=(RKM(I,L  )*(SHTJ  (I,L+1)-SGBJ  (I,L))               
     1               +RKM(I,L+1)*(SGBJ  (I,  L)-SHTJ  (I,L)))    
     2                          / DSHJ  (I,  L)                 
  320    CONTINUE                                                         
  321 CONTINUE                                                            
C                                                                          
C     * SET A LOWER BOUND TO RK'S.                                         
C
      RKMN=0.1
      RKHMN=0.1
      RKQMN=0.1
      RKXMIN(:)=0.1
C                                                                         
      DO 330 L=1,ILEV                                                     
      DO 330 I=IL1,IL2                                                     
         RKM(I,L)=MAX(RKMN ,RKM(I,L))
         RKH(I,L)=MAX(RKHMN,RKH(I,L))
         RKQ(I,L)=MAX(RKQMN,RKQ(I,L))
! Save the eddy coefficients for output diagnostics
         RKMROL(I,L) = RKMROL(I,L) + RKM(I,L)*FARE(I)
         RKHROL(I,L) = RKHROL(I,L) + RKH(I,L)*FARE(I)
         RKQROL(I,L) = RKQROL(I,L) + RKQ(I,L)*FARE(I)
  330 CONTINUE
C
C     * DEFINE THE MINIUMUM ("BACKGROUND") DIFFUSIVITIES FOR ALL THE
C     * TRACERS.
C
      DO 350 N=1,NTRAC
        DO L=1,ILEV
        DO I=IL1,IL2
           RKX(I,L,N)=MAX(RKXMIN(N),RKQ(I,L))
        ENDDO
        ENDDO
 350  CONTINUE
C-----------------------------------------------------------------------
C     * CALCULATE THE TENDENCIES AND REFINE EVALUATION OF SFC FLUXES.
C       -------------------------------------------------------------

C     * CALCULATE THE COEFFICIENT MATRIX FOR MOMENTUM DIFFUSION.
C     * GET TENDENCIES FROM MOMENTUM VERTICAL DIFFUSION.
C     * EVALUATE UFS AND VFS; SINCE THESE ARE TO BE SCALED BY THE SAVING
C     * INTERVAL, THEIR ACCUMULATION DONE INSIDE IMPLVDH MUST BE RE-
C     * DONE OUTSIDE.

      TODT=ZTMST
      CALL ABCVDM6 (A,B,C,CL,CDVLM,GRAV,IL1,IL2,ILG,ILEV,
     1              RGAS,RKM,SGJ,SGBJ,SHJ,TSGB,TODT,.TRUE.)

      CALL IMPLVD7 (A,B,C,CL,U,CL,IL1,IL2,ILG,ILEV,TODT,
     1              TEND,DSGJ,RAUS,WORK,VINT)
C
C     * COMBINE THE TENDENCIES DUE TO MOMENTUM DIFFUSION.
C
      DO 498 I=IL1,IL2
        IF(FARE(I).GT.0.)                                      THEN
          FACT       = VINT(I)*RAUS(I)
          UFS    (I) = UFS    (I)  + FARE(I)*FACT*SAVERAD
          UFSROL (I) = UFSROL (I)  + FARE(I)*FACT*SAVEBEG
          UFSINST(I) = UFSINST (I) + FARE(I)*FACT
          IF(NT.EQ.IOWAT)                                THEN
            UFSOROL(I) = UFSOROL(I) + (UCVTS(I)+FACT)*SAVEBEG
          ELSE IF(NT.EQ.IOSIC)                           THEN
            UFSIROL(I) = UFSIROL(I) + (UCVTS(I)+FACT)*SAVEBEG      
          ENDIF
        ENDIF
  498 CONTINUE

      DO 500 L=1,ILEV
      DO 500 I=IL1,IL2
         UTG(I,L) = UTG(I,L) + TEND(I,L)*FARE(I)
         U  (I,L) = U  (I,L) + TODT*TEND(I,L)
  500 CONTINUE

      CALL IMPLVD7 (A,B,C,CL,V,CL,IL1,IL2,ILG,ILEV,TODT,
     1              TEND,DSGJ,RAUS,WORK,VINT)

      DO 505 I=IL1,IL2
        IF(FARE(I).GT.0.)                                      THEN
          FACT       = VINT(I)*RAUS(I)
          VFS    (I) = VFS    (I)  + FARE(I)*FACT*SAVERAD
          VFSROL (I) = VFSROL (I)  + FARE(I)*FACT*SAVEBEG
          VFSINST(I) = VFSINST (I) + FARE(I)*FACT
          IF(NT.EQ.IOWAT)                                THEN
            VFSOROL(I) = VFSOROL(I) + (VCVTS(I)+FACT)*SAVEBEG
          ELSE IF(NT.EQ.IOSIC)                           THEN
            VFSIROL(I) = VFSIROL(I) + (VCVTS(I)+FACT)*SAVEBEG      
          ENDIF
        ENDIF
  505 CONTINUE

      DO 510 L=1,ILEV
      DO 510 I=IL1,IL2
         VTG(I,L) = VTG(I,L) + TEND(I,L)*FARE(I)
         V  (I,L) = V  (I,L) + TODT*TEND(I,L)
  510 CONTINUE 
C--------------------------------------------------------------
C     * LIQUID WATER STATIC ENERGY AND TOTAL WATER PROFILES. 
C
      DO L=1,ILEV
      DO IL=IL1,IL2 
        EST=(1.-CHI(IL,L))*ESW(TH(IL,L))+CHI(IL,L)*ESI(TH(IL,L))
        IF ( EST < P(IL,L) ) THEN 
          QCWX=(XROW(IL,L+1,ILWC)+XROW(IL,L+1,IIWC))*DSR(IL,L)
          QT(IL,L)=Q(IL,L)*DSR(IL,L)+QCWX
          HMN(IL,L)=CPM(IL,L)*TH(IL,L)+GRAV*Z(IL,L)-RRL(IL,L)*QCWX
        ELSE
          QT(IL,L)=Q(IL,L)*DSR(IL,L)
          HMN(IL,L)=CPM(IL,L)*TH(IL,L)+GRAV*Z(IL,L)
        ENDIF
        IF (QT(IL,L).LE.0.) CALL WRN('VRTDFTKE',-1)
        QT(IL,L)=MAX(QT(IL,L),0.)
      ENDDO
      ENDDO
C
C     * LIQUID WATER STATIC ENERGY AND TOTAL WATER AT SURFACE.
C
      L=ILEV
      DO IL=IL1,IL2 
        QTG(IL)=QG(IL)*DSR(IL,L)
        HMNG(IL)=CPM(IL,L)*GT(IL)
      ENDDO
C
C     * CALCULATE THE COEFFICIENT MATRIX FOR TRACER DIFFUSION.            
C     * GET TENDENCIES FROM VERTICAL TRACER DIFFUSION.                    
C     * EVALUATE TRACSFS.                                                 
C
      DO 532 N=1,NTRAC  
       IF (ITRPHS(N).NE.0 .OR. N.EQ.ILWC .OR. N.EQ.IIWC)    THEN
                                                                              
         CALL ABCVDQ6 (                                                    
     1                 A,B,C,CL, UN ,CDVLT,GRAV,                           
     2                 IL1,IL2,ILG,ILEV,ILEV+1,ILEV,                       
     3                 RGAS,RKX(1,1,N),SHTJ,SHJ,DSHJ,                      
     4                 TH(1,ILEV),TF,TODT)                                 
                                                                      
         CALL IMPLVD7(A,B,C,CL,XROW(1,2,N),ZER,IL1,IL2,ILG,ILEV,           
     1                TODT,TEND,DSHJ,RAUS,WORK,VINT)                       
C                                                                              
C        * NOW APPLY THE TENDENCY COMPUTED IN IMPLVD7:
C
         DO L=1,ILEV
         DO I=IL1,IL2
            XROW(I,L+1,N)=XROW(I,L+1,N)+TODT*TEND(I,L)
         ENDDO
         ENDDO
       ENDIF
 532  CONTINUE
C
C     * SAVE CLOUD WATER/ICE PROFILES AFTER MIXING AS PASSIVE TRACERS.
C
      IF ( ISAVLS.NE.0 ) THEN
        DO L=1,ILEV
        DO IL=IL1,IL2 
          XLWROL(IL,L)=XROW(IL,L+1,ILWC)
          XICROL(IL,L)=XROW(IL,L+1,IIWC)
        ENDDO
        ENDDO
      ENDIF
C                                                                            
C     * CALCULATE THE COEFFICIENT MATRIX FOR DIFFUSION OF LIQUID
C     * WATER STATIC ENERGY AND TOTAL WATER. GET TENDENCIES FROM 
C     * VERTICAL DIFFUSION.     
C
      CALL ABCVDQ6 (                                                    
     1              A,B,C,CL,ZER ,CDVLH,GRAV,                           
     2              IL1,IL2,ILG,ILEV,ILEV+1,ILEV,                       
     3              RGAS,RKH,SHTJ,SHJ,DSHJ,                      
     4              TH(1,ILEV),TF,TODT)                                 
                                                                      
      CALL IMPLVD7(A,B,C,CL,HMN,HMNG,IL1,IL2,ILG,ILEV,           
     1             TODT,TEND,DSHJ,RAUS,WORK,VINT)                       
C                                                                              
C     * NOW APPLY THE TENDENCY COMPUTED IN IMPLVD7:
C
      DO L=1,ILEV                                                   
      DO IL=IL1,IL2                                                  
        HMN(IL,L)=HMN(IL,L)+TODT*TEND(IL,L)                  
      ENDDO
      ENDDO
      CALL ABCVDQ6 (                                                    
     1              A,B,C,CL,ZER ,CDVLH,GRAV,                           
     2              IL1,IL2,ILG,ILEV,ILEV+1,ILEV,                       
     3              RGAS,RKH,SHTJ,SHJ,DSHJ,                      
     4              TH(1,ILEV),TF,TODT)                                 
                                                                      
      CALL IMPLVD7(A,B,C,CL,QT,QTG,IL1,IL2,ILG,ILEV,           
     1             TODT,TEND,DSHJ,RAUS,WORK,VINT)                       
C                                                                              
C     * NOW APPLY THE TENDENCY COMPUTED IN IMPLVD7:
C
      DO L=1,ILEV
      DO IL=IL1,IL2
        ACOR=1.
        IF(QT(IL,L)+TODT*TEND(IL,L).LT.0.) THEN
        ACORT=-QT(IL,L)/(TEND(IL,L)*TODT)
        ACOR1=MAX(MIN(ACOR,ACORT),0.)
        ACOR=(3.-2.*ACOR1)*ACOR1**3
        ENDIF
        QT(IL,L)=QT(IL,L)+TODT*ACOR*TEND(IL,L)
      ENDDO
      ENDDO
C
C     * UNRAVEL LIQUID WATER STATIC ENERGY AND TOTAL WATER TO OBTAIN
C     * SPECIFIC HUMIDITY, TEMPERATURE, AND CLOUD WATER.
C
      ICVSG=0
      ISUBG=1
      DO L=1,ILEV
        DO IL=IL1,IL2
          IF(L.GT.1)                            THEN
            DZ       =Z   (IL,L-1)-Z  (IL,L)
            DHLDZ(IL)=(HMN(IL,L-1)-HMN(IL,L))/DZ
            DRWDZ(IL)=(QT (IL,L-1)-QT (IL,L))/DZ
            ALMIX(IL)=ALMXT(IL,L)
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
            ALMIX(IL)=10.
          ENDIF 
C
          EST=(1.-CHI(IL,L))*ESW(TH(IL,L))+CHI(IL,L)*ESI(TH(IL,L))
          IF(L.GT.LEV1 .AND.
     1       EST.LT.P(IL,L) .AND. P(IL,L).GE.10.)    THEN
            X(IL)=1.
          ELSE
            X(IL)=0.
          ENDIF 
        ENDDO
C
        IDUM=0
        ICALL=2       
        CALL STATCLD5(QCW,ZCLF,SIGMA(1,L),ZCRAUT,WORK(1,1),SSH,
     1                CVSG(1,L),QT(1,L),HMN(1,L),CHI(1,L),CPM(1,L),
     2                P(1,L),Z(1,L),RRL(1,L),ZER,X,
     3                ALMIX,DHLDZ,DRWDZ,IDUM,
     4                GRAV,ZTMST,ILEV,ILG,IL1,IL2,ICVSG,ISUBG,
     5                L,ICALL                                )
C
        DO IL=IL1,IL2
         IF(X(IL).GT.0.)                                  THEN 
          QCWI=CHI(IL,L)*QCW(IL)/DSR(IL,L)
          QCWL=(1.-CHI(IL,L))*QCW(IL)/DSR(IL,L)
C
C         * UPDATE TEMPERATURE, SPECIFIC HUMIDITY, AND CLOUD WATER.
C
          TH(IL,L)=(HMN(IL,L)-GRAV*Z(IL,L)+RRL(IL,L)*QCW(IL))/CPM(IL,L)
          Q (IL,L)=(QT(IL,L)-QCW(IL))/DSR(IL,L)
          XROW(IL,L+1,ILWC)=QCWL
          XROW(IL,L+1,IIWC)=QCWI
         ELSE
C
C         * UPDATE TEMPERATURE, SPECIFIC HUMIDITY, AND CLOUD WATER IN DRY
C         * LAYER NEAR TOP OF MODEL DOMAIN ASSUMING THAT THERE IS NO
C         * CONDENSATE.
C
          TH(IL,L)=(HMN(IL,L)-GRAV*Z(IL,L))/CPM(IL,L)
          Q (IL,L)=QT(IL,L)
          XROW(IL,L+1,ILWC)=0.
          XROW(IL,L+1,IIWC)=0.
         ENDIF
        ENDDO
      ENDDO
C
C     * SAVE CONDENSATION/DEPOSITION TENDENCIES.
C
      IF ( ISAVLS.NE.0 ) THEN
        DO 575 L=1,ILEV
        DO 575 IL=IL1,IL2 
          CNDROLX(IL,L)=CNDROLX(IL,L)
     1                +(XROW(IL,L+1,ILWC)-XLWROL(IL,L))/TODT
          DEPROLX(IL,L)=DEPROLX(IL,L)
     1                +(XROW(IL,L+1,IIWC)-XICROL(IL,L))/TODT
  575   CONTINUE
      ENDIF
C
C     * AGGREGATE CHANGES OVER ALL TILES.
C
      DO N=1,NTRAC
      DO L=1,LEV
      DO IL=IL1,IL2
        CHGX(IL,L,N)= CHGX(IL,L,N) + (XROW(IL,L,N)-XIO(IL,L,N))*FARE(IL)
      ENDDO
      ENDDO
      ENDDO

      DO L=1,ILEV
      DO IL=IL1,IL2
        CHGT(IL,L)  = CHGT(IL,L) + (TH(IL,L)-TIO(IL,L))*FARE(IL)
        CHGQ(IL,L)  = CHGQ(IL,L) + (Q (IL,L)-QIO(IL,L))*FARE(IL)
        CHGU(IL,L)  = CHGU(IL,L) + (U (IL,L)-UIO(IL,L))*FARE(IL)
        CHGV(IL,L)  = CHGV(IL,L) + (V (IL,L)-VIO(IL,L))*FARE(IL)
C
	XLMTKE(IL,L)= XLMTKE(IL,L) + XLMTKEX(IL,L)*FARE(IL)
	TKEITM(IL,L)= TKEITM(IL,L) + TKEITMX(IL,L)*FARE(IL)
        SVAR  (IL,L)= SVAR  (IL,L) + SIGMA  (IL,L)*FARE(IL)
        WSUB(IL,L)  = WSUB(IL,L) + WSUBX(IL,L)*FARE(IL)
        IF ( ISAVLS.NE.0 ) THEN
          CNDROL(IL,L) = CNDROL(IL,L) + CNDROLX(IL,L)*FARE(IL)
          DEPROL(IL,L) = DEPROL(IL,L) + DEPROLX(IL,L)*FARE(IL)
        ENDIF
      ENDDO
      ENDDO
C
C     * CALCULATE THE AGGREGATE PBL VERTICAL INDEX.
C
      DO IL=IL1,IL2
        PBLT(IL) = PBLT(IL) + PBLTX(IL)*FARE(IL)
      ENDDO

 800  CONTINUE   ! end of tiling loop!
C
C     * CALCULATE THE NEW PROGNOSTIC THERMODYNAMIC VALUES.
C
      DO N=1,NTRAC
      DO L=1,LEV
      DO IL=IL1,IL2
        XIO(IL,L,N) = XIO(IL,L,N) + CHGX(IL,L,N)
      ENDDO
      ENDDO
      ENDDO

      DO L=1,ILEV
      DO IL=IL1,IL2
        TIO(IL,L)  = TIO(IL,L) + CHGT(IL,L)
        QIO(IL,L)  = QIO(IL,L) + CHGQ(IL,L)
        UIO(IL,L)  = UIO(IL,L) + CHGU(IL,L)
        VIO(IL,L)  = VIO(IL,L) + CHGV(IL,L)
      ENDDO
      ENDDO
                                                                                  
      RETURN                                                                      
C-----------------------------------------------------------------------          
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
