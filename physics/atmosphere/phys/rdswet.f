!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RDSWET(SNOW,WSNOW,REFF,GCROW,REFF0,DELT,ILG,IL1,IL2)
C
C     * M. Namazi & K. VON SALZEN, SNOW GRAIN DUE TO WET AGING.
C     * ONLY OVER LAND HAPPENS. 
C     * FOR THE REST OF REGIONS KEEP THE SAME VALUE,
C
      IMPLICIT NONE

C     * INTEGER AND REAL CONSTANTS.
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER IL1       !<Variable description\f$[units]\f$
      INTEGER IL2       !<Variable description\f$[units]\f$
      INTEGER I
      REAL :: REFF0     !<Variable description\f$[units]\f$
      REAL, PARAMETER :: PI=3.1415927,C1=4.22E-13

C     * INPUT ARRAYS.
      REAL, DIMENSION(ILG) :: SNOW      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: WSNOW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: GCROW     !<Variable description\f$[units]\f$
      REAL  DELT        !<Variable description\f$[units]\f$

C     * INPUT/OUTPUT ARRAYS.
      REAL, DIMENSION(ILG) :: REFF      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
                                                                        
C     * INTERNAL WORK ARRAYS.
      REAL, DIMENSION(ILG) :: FLIQ
C----------------------------------------------------------------------
      DO 100 I=IL1,IL2
          IF(GCROW(I).LT.-0.5.AND.SNOW(I).GT.0.)                THEN
C           IF(REFF(I).EQ.0)  REFF(I)=REFF0
            REFF(I)=MAX(REFF0,REFF(I))
            REFF(I)=REFF(I)*1.E+6
            FLIQ(I)=WSNOW(I)/(SNOW(I)+WSNOW(I))
            REFF(I)=((0.75E+18*C1/PI)*(FLIQ(I)**3)*DELT
     1             + REFF(I)**3)**(1./3.)
            REFF(I)=REFF(I)*1.E-6 
          ENDIF
  100 CONTINUE  
 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
