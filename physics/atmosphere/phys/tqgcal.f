!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE TQGCAL (TSG,QSG,TSH,QSH,SG,SH,
     1                   ILEV,ILG,IS,IF)
C 
C     * JUN 24/2014 - M.LAZARE. DERIVED FROM TGCAL2.
C
C     * SET TEMPERATURE AND MOISTURE ARRAYS FOR PHYSICS ON MOMENTUM LEVELS. 
C     * FOR HYBRID VERSION OF MODEL.
C     * SGB ARE MOMENTUM LAYER INTERFACES,
C     * SG  ARE MOMENTUM MID LAYER POSITIONS. 
C     * SH  ARE THERMODYNAMICS MID LAYER POSITIONS. 
C
      IMPLICIT NONE
C
C     * CONTROL PARAMETERS.
C
      INTEGER IS        !<Variable description\f$[units]\f$
      INTEGER IF        !<Variable description\f$[units]\f$
      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER ILEV      !<Variable description\f$[units]\f$
      INTEGER I         !<Variable description\f$[units]\f$
      INTEGER L         !<Variable description\f$[units]\f$
C
C     * OUTPUT.
C
      REAL   TSG(ILG,ILEV)      !<Variable description\f$[units]\f$
      REAL   QSG(ILG,ILEV)      !<Variable description\f$[units]\f$
C
C     * INPUT.
C
      REAL   TSH(ILG,ILEV)      !<Variable description\f$[units]\f$
      REAL   QSH(ILG,ILEV)      !<Variable description\f$[units]\f$
      REAL    SG(ILG,ILEV)      !<Variable description\f$[units]\f$
      REAL    SH(ILG,ILEV)      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
      DO I=IS,IF
        TSG(I,1)=TSH(I,1) 
        QSG(I,1)=QSH(I,1)
      ENDDO
C 
      DO L=2,ILEV 
      DO I=IS,IF
        TSG(I,L)   = (TSH(I,L-1)*LOG(SH(I  ,L)/SG(I  ,L))
     1               +TSH(I,L  )*LOG(SG(I  ,L)/SH(I,L-1))) 
     2                          /LOG(SH(I  ,L)/SH(I,L-1))
C
        QSG(I,L)   = (QSH(I,L-1)*LOG(SH(I  ,L)/SG(I  ,L))
     1               +QSH(I,L  )*LOG(SG(I  ,L)/SH(I,L-1))) 
     2                          /LOG(SH(I  ,L)/SH(I,L-1))
      ENDDO
      ENDDO
C 
      RETURN
C-----------------------------------------------------------------------
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
