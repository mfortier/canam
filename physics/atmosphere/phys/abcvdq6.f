!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE ABCVDQ6 (A,B,C,CL, EF ,CDVLH,GRAV, 
     1                    IL1,IL2,ILG,ILEV,LEV,LEVS,
     2                    RGAS,RKQ,SHTJ,SHJ,DSHJ, 
     3                    THL,TF,TODT)
  
C     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS. 
C     * MAR 14/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H. 
  
C     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL MATRIX FOR
C     * THE IMPLICIT VERTICAL DIFFUSION OF MOISTURE OH HYBRID VERSION OF
C     * MODEL. A IS THE LOWER DIAGONAL, B IS THE MAIN DIAGONAL
C     * AND C IS THE UPPER DIAGONAL.
C     * ILEV = NUMBER OF MODEL LEVELS,
C     * LEVS = NUMBER OF MOISTURE LEVELS. 
  
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
      REAL   A   (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   B   (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   C   (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   RKQ (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   SHTJ(ILG, LEV)     !<Variable description\f$[units]\f$
      REAL   SHJ (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   DSHJ(ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   TF  (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL        THL (ILG)     !<Variable description\f$[units]\f$
      REAL         CL (ILG)     !<Variable description\f$[units]\f$
      REAL         EF (ILG)     !<Variable description\f$[units]\f$
      REAL      CDVLH (ILG)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
      MSG=ILEV-LEVS 
      L=MSG+1 
      M=L+1 
  
      DO 50 I=IL1,IL2 
         OVDS   = (GRAV*SHTJ(I,M)/RGAS)**2
     1           /( DSHJ(I,L) * (SHJ(I,M)-SHJ(I,L)) ) 
         C(I,L) = OVDS*RKQ(I,M)*(1./TF(I,M))**2 
   50 CONTINUE
  
      DO 75 I=IL1,IL2 
         B(I,L) = -C(I,L) 
   75 CONTINUE
  
      DO 100 L=MSG+2,ILEV 
      DO 100 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,L)/RGAS)**2
     1           /( (SHJ(I,L)-SHJ(I,L-1)) * DSHJ(I,L) ) 
         A(I,L) = OVDS*(1./TF(I,L))**2*RKQ(I,L) 
  100 CONTINUE
  
      DO 200 L=MSG+2,ILEV-1 
      DO 200 I=IL1,IL2
         D      = DSHJ(I,L+1) / DSHJ(I,L) 
         C(I,L) = A(I,L+1)*D
         B(I,L) =-A(I,L+1)*D - A(I,L) 
  200 CONTINUE
  
      L=ILEV
      DO 250 I=IL1,IL2
         CL(I)=GRAV*SHJ(I,L)*EF(I)*CDVLH(I)/(RGAS*THL(I)*DSHJ(I,L)) 
  250 CONTINUE
  
      DO 300 I=IL1,IL2
         B(I,L) = -A(I,L) -CL(I)
  300 CONTINUE
  
C     * DEFINE MATRIX TO INVERT = I-2*DT*MAT(A,B,C).
  
      DO 500 L=MSG+1,ILEV-1 
      DO 500 I=IL1,IL2
         A(I,L+1) = -TODT*A(I,L+1)
         C(I,L  ) = -TODT*C(I,L  )
  500 CONTINUE
  
      DO 550 L=MSG+1,ILEV 
      DO 550 I=IL1,IL2
         B(I,L) = 1.-TODT*B(I,L)
  550 CONTINUE
  
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
