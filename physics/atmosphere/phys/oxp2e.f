!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE OXP2E(OHPHS,H2O2PHS,O3PHS,NO3PHS,HNO3PHS,NH3PHS,NH4PHS,
     1                 OHROW,H2O2ROW,O3ROW,NO3ROW,HNO3ROW,NH3ROW,NH4ROW,
     2                 PRESSG,SHJ,LEVOX,ILEV,IL1,IL2,ILG,IOXTYP)      
C
C     * THIS ROUTINE INTERPOLATES THE INPUT OXIDANTS FIELDS TO MODEL
C     * THERMODYNAMIC LEVELS.       
C
      implicit none
C
C     * OUTPUT ARRAYS...
C
      REAL, INTENT(out) :: ohphs  (ilg,ilev)    !<Variable description\f$[units]\f$
      REAL, INTENT(out) :: h2o2phs(ilg,ilev)    !<Variable description\f$[units]\f$
      REAL, INTENT(out) :: o3phs  (ilg,ilev)    !<Variable description\f$[units]\f$
      REAL, INTENT(out) :: no3phs (ilg,ilev)    !<Variable description\f$[units]\f$
      REAL, INTENT(out) :: hno3phs(ilg,ilev)    !<Variable description\f$[units]\f$
      REAL, INTENT(out) :: nh3phs (ilg,ilev)    !<Variable description\f$[units]\f$
      REAL, INTENT(out) :: nh4phs (ilg,ilev)    !<Variable description\f$[units]\f$      
C     
C     * INPUT
C
      INTEGER, INTENT(in) :: levox              !<Variable description\f$[units]\f$
      INTEGER, INTENT(in) :: ilev               !<Variable description\f$[units]\f$
      INTEGER, INTENT(in) :: il1                !<Variable description\f$[units]\f$
      INTEGER, INTENT(in) :: il2                !<Variable description\f$[units]\f$
      INTEGER, INTENT(in) :: ilg                !<Variable description\f$[units]\f$
      INTEGER, INTENT(in) :: ioxtyp             !<Variable description\f$[units]\f$
C
      REAL, INTENT(in) :: ohrow  (ilg,levox)    !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: h2o2row(ilg,levox)    !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: o3row  (ilg,levox)    !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: no3row (ilg,levox)    !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: hno3row(ilg,levox)    !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: nh3row (ilg,levox)    !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: nh4row (ilg,levox)    !<Variable description\f$[units]\f$            
      REAL, INTENT(in) :: pressg (ilg)          !<Variable description\f$[units]\f$
      REAL, INTENT(in) :: shj    (ilg,ilev)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C     * LOCAL WORK SPACE
C     * ya and ym are dimensioned with number of oxidants, so parameter statement
C     * NOX must always reflect this.
C     * NOX(1-7) is ordered for {OH,H2O2,O3,NO3,HNO3,NH3,NH4}      

      INTEGER, PARAMETER :: NOX=7
      INTEGER :: i, k, l, lstart
      REAL, DIMENSION(ILG,NOX) :: ya, ym, slope, tercep
      REAL, DIMENSION(ILG) :: xa, xm, ploc, ris
      REAL :: pref(200)
C
C     * PRESSURE-LEVEL DATA FOR IOXTYP=1.
C     
      REAL, PARAMETER, DIMENSION(19) ::
     &  PREF1 = (/ 100.,    500.,     1000.,   2000.,   3000.,
     &             5000.,   7000.,    10000.,  15000.,  20000.,
     &             25000.,  30000.,   40000.,  50000.,  60000.,
     &             70000.,  85000.,   92500.,  100000. /)
C----------------------------------------------------------------------
C
C     * DEFINE WORKING PRESSURE LEVEL REFERENCE VERTICAL GRID, BASED ON
C     * CHOICE OF IOXTYP.
C
      IF(IOXTYP.EQ.1)    THEN
       IF(LEVOX.NE.19) THEN
        PRINT *, '0WRONG VALUE FOR LEVOX!: IOXTYP,LEVOX = ',IOXTYP,LEVOX
        CALL XIT('OXVINT',-1)
       ENDIF
C
       DO K=1,19
         PREF(K)=PREF1(K)
       ENDDO
      ELSE
       CALL XIT('OXVINT',-2)
      ENDIF
C
C     * LOCATE INDICES FOR INTERPOLATION AT FULL PRESSURE LEVELS. 
C
      LSTART = 1
      DO 180 L = 1, ILEV 
          DO 120 I = IL1, IL2 
              RIS(I) = 0.
  120     CONTINUE
  
          DO 140 K = LSTART, LEVOX
              DO 130 I = IL1, IL2
                 PLOC(I)=SHJ(I,L)*PRESSG(I)
                 IF ( PREF(1).GE.PLOC(I).AND.RIS(I).EQ.0.) THEN
                    RIS(I)=1.
                    XA (I)=PREF(2)
                    XM (I)=PREF(1)
                    YA (I,1)=OHROW(I,1)
                    YM (I,1)=OHROW(I,1)
                    YA (I,2)=H2O2ROW(I,1)
                    YM (I,2)=H2O2ROW(I,1)
                    YA (I,3)=O3ROW(I,1)
                    YM (I,3)=O3ROW(I,1)
                    YA (I,4)=NO3ROW(I,1)
                    YM (I,4)=NO3ROW(I,1)
                    YA (I,5)=HNO3ROW(I,1)
                    YM (I,5)=HNO3ROW(I,1)
                    YA (I,6)=NH3ROW(I,1)
                    YM (I,6)=NH3ROW(I,1)
                    YA (I,7)=NH4ROW(I,1)
                    YM (I,7)=NH4ROW(I,1)                    
                 ENDIF
                 IF ( PREF(K).GE.PLOC(I).AND.RIS(I).EQ.0. )THEN
                    RIS(I)=REAL(K)
                    XA (I)=PREF(K)
                    XM (I)=PREF(K-1)
                    YA (I,1)=OHROW(I,K)
                    YM (I,1)=OHROW(I,K-1)
                    YA (I,2)=H2O2ROW(I,K)
                    YM (I,2)=H2O2ROW(I,K-1)
                    YA (I,3)=O3ROW(I,K)
                    YM (I,3)=O3ROW(I,K-1)
                    YA (I,4)=NO3ROW(I,K)
                    YM (I,4)=NO3ROW(I,K-1)
                    YA (I,5)=HNO3ROW(I,K)
                    YM (I,5)=HNO3ROW(I,K-1)
                    YA (I,6)=NH3ROW(I,K)
                    YM (I,6)=NH3ROW(I,K-1)
                    YA (I,7)=NH4ROW(I,K)
                    YM (I,7)=NH4ROW(I,K-1)                    
                 ENDIF
                 IF ( PREF(LEVOX).LE.PLOC(I).AND.RIS(I).EQ.0. ) THEN
                    RIS(I)=REAL(LEVOX)
                    XA (I)=PREF(LEVOX)
                    XM (I)=PREF(LEVOX-1)
                    YA (I,1)=OHROW(I,LEVOX)
                    YM (I,1)=OHROW(I,LEVOX)
                    YA (I,2)=H2O2ROW(I,LEVOX)
                    YM (I,2)=H2O2ROW(I,LEVOX)
                    YA (I,3)=O3ROW(I,LEVOX)
                    YM (I,3)=O3ROW(I,LEVOX)
                    YA (I,4)=NO3ROW(I,LEVOX)
                    YM (I,4)=NO3ROW(I,LEVOX)
                    YA (I,5)=HNO3ROW(I,LEVOX)
                    YM (I,5)=HNO3ROW(I,LEVOX)
                    YA (I,6)=NH3ROW(I,LEVOX)
                    YM (I,6)=NH3ROW(I,LEVOX)
                    YA (I,7)=NH4ROW(I,LEVOX)
                    YM (I,7)=NH4ROW(I,LEVOX)                    
                 ENDIF
  130         CONTINUE
  140     CONTINUE
C
C         * LINEAR INTERPOLATION (VOLUME MIXING RATIO)
C
          LSTART=LEVOX
          DO 160 I=IL1,IL2
              LSTART = MIN(LSTART, NINT(RIS(I)))
              SLOPE  (I,1) = (YA(I,1) - YM(I,1)) / (XA(I) - XM(I))
              SLOPE  (I,2) = (YA(I,2) - YM(I,2)) / (XA(I) - XM(I))
              SLOPE  (I,3) = (YA(I,3) - YM(I,3)) / (XA(I) - XM(I))
              SLOPE  (I,4) = (YA(I,4) - YM(I,4)) / (XA(I) - XM(I))
              SLOPE  (I,5) = (YA(I,5) - YM(I,5)) / (XA(I) - XM(I))
              SLOPE  (I,6) = (YA(I,6) - YM(I,6)) / (XA(I) - XM(I))
              SLOPE  (I,7) = (YA(I,7) - YM(I,7)) / (XA(I) - XM(I))
C              
              TERCEP (I,1) = YM(I,1) - SLOPE(I,1) * XM(I)
              TERCEP (I,2) = YM(I,2) - SLOPE(I,2) * XM(I)
              TERCEP (I,3) = YM(I,3) - SLOPE(I,3) * XM(I)
              TERCEP (I,4) = YM(I,4) - SLOPE(I,4) * XM(I)
              TERCEP (I,5) = YM(I,5) - SLOPE(I,5) * XM(I)
              TERCEP (I,6) = YM(I,6) - SLOPE(I,6) * XM(I)
              TERCEP (I,7) = YM(I,7) - SLOPE(I,7) * XM(I)
C              
              OHPHS  (I,L) = SLOPE(I,1) * PLOC(I) + TERCEP(I,1)
              H2O2PHS(I,L) = SLOPE(I,2) * PLOC(I) + TERCEP(I,2)
              O3PHS  (I,L) = SLOPE(I,3) * PLOC(I) + TERCEP(I,3)
              NO3PHS (I,L) = SLOPE(I,4) * PLOC(I) + TERCEP(I,4)
              HNO3PHS(I,L) = SLOPE(I,5) * PLOC(I) + TERCEP(I,5)
              NH3PHS (I,L) = SLOPE(I,6) * PLOC(I) + TERCEP(I,6)
              NH4PHS (I,L) = SLOPE(I,7) * PLOC(I) + TERCEP(I,7)              
  160     CONTINUE
  180 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
