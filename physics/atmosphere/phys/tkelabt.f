      SUBROUTINE TKELABT(LTKET,ILEV,IM)

C     * Sep 13/20 - M.Lazare. New routine for TKE.
C 
C     * DEFINES LEVEL INDEX VALUES FOR TKE TILED FIELD "XLMPAT".
C 
      IMPLICIT NONE

      INTEGER ILEV,IM,L,M,MLTK

      INTEGER LTKET(IM*ILEV)
C-----------------------------------------------------------------------
      MLTK=0
      DO 100 L=1,ILEV
      DO 100 M=1,IM
        MLTK       = MLTK+1
        LTKET(MLTK) = 1000*L + M 
  100 CONTINUE
  
      RETURN
      END
