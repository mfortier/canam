!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE HOLEFIL3(QROW,QADDROL,QMIN,ILG,LONSL,IL1,IL2)
C
C     * JUN 27/2006 - M.LAZARE.  NEW VERSION FOR GCM15F:
C     *                          - QADDROL AND QMIN ARE NATIVE REAL.
C     *                          - USE VARIABLE INSTEAD OF CONSTANT
C     *                            IN INTRINSICS SUCH AS "MAX", SO
C     *                            THAT CAN COMPILE IN 32-BIT MODE
C     *                            WITH REAL*8.  
C     * NOV 01/04 - M.LAZARE.    PREVIOUS VERSION HOLEFIL2.
C
C     * SUBROUTINE TO FILL HOLES FOR A GIVEN LEVEL, ALONG A
C     * SINGLE LONGITUDE PASS (CHAINED LATITUDES ARE TAKEN INTO
C     * ACCOUNT).
C
C     * IF THERE IS SUFFICIENT MOISTURE TO BORROW FROM OTHER POINTS
C     * IN THE LONGITUDE PASS, THIS IS DONE TO MINIMIZE GLOBAL
C     * CORRECTION SUBSEQUENTLY DONE IN SPECTRAL SPACE.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C  
      REAL   QROW(ILG)          !<Variable description\f$[units]\f$	
      REAL   QADDROL(ILG)       !<Variable description\f$[units]\f$	
      REAL   QMIN               !<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C
      DATA ZERO /0./
C----------------------------------------------------------------
C     * CALCULATE NUMBER OF CHAINED LATITUDES.
C
      NLATJ = IL2/LONSL
C
C     * HOLEFILLING CORRECTION FOR EACH LATITUDE.
C
      DO J=1,NLATJ 
        SUMQEX=0.0
        SUMQDEF=0.0
C
        DO K=1,LONSL
          I       = (J-1)*LONSL+K
          QD1     = QROW(I)-QMIN
          QD2     = QMIN-QROW(I)
          QEXC    = MAX(QD1,ZERO)
          QDEF    = MAX(QD2,ZERO)
          SUMQEX  = SUMQEX  + QEXC
          SUMQDEF = SUMQDEF + QDEF
        ENDDO
C
        IF(SUMQEX.EQ.0.)                                   THEN
          DO K=1,LONSL
            I          = (J-1)*LONSL+K
            QOLD       = QROW(I)
            QROW(I)    = QMIN
            QADDROL(I) = QROW(I)-QOLD
          ENDDO
        ELSE IF(SUMQEX.GT.SUMQDEF)                         THEN
          RATIO=MAX(SUMQEX-SUMQDEF,ZERO)/SUMQEX
          DO K=1,LONSL
            I          = (J-1)*LONSL+K
            QOLD       = QROW(I)
            QDIF       = QROW(I)-QMIN
            QROW(I)    = QMIN+RATIO*MAX(QDIF,ZERO)
            QADDROL(I) = QROW(I)-QOLD
          ENDDO
        ELSE IF(SUMQEX.LE.SUMQDEF)                         THEN
          DO K=1,LONSL
            I          = (J-1)*LONSL+K
            QOLD       = QROW(I)
            QNEW       = QMIN
            QROW(I)    = MAX(QROW(I),QNEW)
            QADDROL(I) = QROW(I)-QOLD
          ENDDO
        ENDIF
      ENDDO
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
