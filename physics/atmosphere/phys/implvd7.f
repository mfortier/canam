!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE IMPLVD7 (A,B,C, CL,X,XG, IL1,IL2,ILG,ILEV,TODT,
     1                    TEND,DELSIG,RAUS,WORK,VINT) 
  
C     * DEC 07/89 - D.VERSEGHY: SAME AS PREVIOUS VERSION IMPLVD6
C     *                         EXCEPT FOR REMOVAL OF UPDATE OF 
C     *                         SURFACE FLUXES. 
  
C     * CALCULATE TENDENCIES DUE TO VERTICAL DIFFUSION IN HYBRID MODEL. 
C     * THE SCHEME IS IMPLICIT BACKWARD.
C     * A,B,C ARE RESPECTIVELY THE LOWER, MAIN AND UPPER DIAG.
  
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL A     (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL B     (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL C     (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL X     (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL WORK  (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL CL    (ILG)          !<Variable description\f$[units]\f$
      REAL XG    (ILG)          !<Variable description\f$[units]\f$
      REAL VINT  (ILG)          !<Variable description\f$[units]\f$
      REAL TEND  (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL DELSIG(ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL RAUS  (ILG)          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
C     * GET X+ (IN ARRAY TEND) FROM X-. SAVE X(I,ILEV) IN VINT(I).
  
      DO 100 I=IL1,IL2
         VINT(I)   = X(I,ILEV)
         X(I,ILEV) = X(I,ILEV) +TODT*CL(I)*XG(I)
  100 CONTINUE
  
      CALL VROSSR(TEND, A,B,C,X,WORK,ILG,IL1,IL2,ILEV)
  
      DO 150 I=IL1,IL2
         X(I,ILEV) = VINT(I)
  150 CONTINUE
  
C     * GET TENDENCY FROM X- AND X+.
  
      DO 200 L=1,ILEV 
      DO 200 I=IL1,IL2
         TEND(I,L) = (TEND(I,L)-X(I,L))*(1./TODT) 
  200 CONTINUE
  
C     * CALCULATE VERTICAL INTEGRAL VINT. 
  
      DO 300 I=IL1,IL2
         VINT(I) = 0. 
  300 CONTINUE
  
      DO 400 L=1,ILEV 
      DO 400 I=IL1,IL2
         VINT(I) = VINT(I) +TEND(I,L)*DELSIG(I,L) 
  400 CONTINUE
  
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
