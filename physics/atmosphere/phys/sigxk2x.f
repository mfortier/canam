!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE SIGXK2X(SHTXKJ,SHXKJ, SHJ,SHTJ, NK,NK1,ILG,IL1,IL2,
     1                   RGOCP)
C
C     * JUN 10/03 - MLAZARE.  CHANGE 1,LON TO IL1,IL2 (PASSED IN).  
C     * JUL 15/88 - M.LAZARE. PREVIOUS VERSION SIGXK2.
C
C     * DEFINE SIGMA**(RGAS/CPRES) AT LAYERS CENTRES AND TOPS.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
  
      REAL   SHTXKJ (ILG,NK )   !<Variable description\f$[units]\f$	
      REAL    SHXKJ (ILG,NK )   !<Variable description\f$[units]\f$	
      REAL   SHTJ   (ILG,NK1)   !<Variable description\f$[units]\f$	
      REAL    SHJ   (ILG,NK )   !<Variable description\f$[units]\f$	
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
      DO 500  K=1,NK
         DO 100 I=IL1,IL2
            SHXKJ (I,K)=SHJ (I,K)**RGOCP
            SHTXKJ(I,K)=SHTJ(I,K)**RGOCP
  100    CONTINUE 
  500 CONTINUE
  
      RETURN
C-----------------------------------------------------------------------
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
