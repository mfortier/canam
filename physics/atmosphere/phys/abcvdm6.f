!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE ABCVDM6 (A,B,C, CL,CDVLM,GRAV,IL1,IL2,ILG,ILEV,RGAS, 
     1                    RKM,SGJ,SGBJ,SHJ,TSGB,TODT,NOGWDRG) 
  
C     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS. 
C     * JAN 29/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H. 
  
C     * GENERALIZE FOR HYBRID VERSION OF MODEL
C     * MADE FROM ABCVDMU.
C     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL MATRIX
C     * FOR THE IMPLICIT VERTICAL DIFFUSION OF MOMENTUM ON FULL LEVELS. 
C     * A,B,C ARE THE LOWER,MAIN,UPPER DIAGONAL RESPECTIVELY. 
  
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL NOGWDRG 
  
      REAL   A   (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   B   (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   C   (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   RKM (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   TSGB(ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL       CDVLM(ILG)     !<Variable description\f$[units]\f$
      REAL        CL  (ILG)     !<Variable description\f$[units]\f$
      REAL   SGJ (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL  SGBJ (ILG,ILEV)     !<Variable description\f$[units]\f$
      REAL   SHJ (ILG,ILEV)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

C-----------------------------------------------------------------------
  
      DO 50 I=IL1,IL2 
         OVDS  =SGBJ(I,1)*(GRAV/RGAS)**2/(SGJ(I,2)-SGJ(I,1))
         C(I,1)= OVDS * (1./TSGB(I,1))**2 * RKM(I,1)
         CL(I) = 0. 
   50 CONTINUE
  
      DO 75 I=IL1,IL2 
         B(I,1)=-C(I,1) 
   75 CONTINUE
  
      DO 100 L=2,ILEV 
      DO 100 I=IL1,IL2
         OVDS  = (GRAV*SGBJ(I,L-1)/RGAS)**2 
     1          /( (SGBJ(I,L)-SGBJ(I,L-1)) * (SGJ(I,L)-SGJ(I,L-1)) )
         A(I,L)= OVDS * (1./TSGB(I,L-1))**2 * RKM(I,L-1)
  100 CONTINUE
  
      DO 200 L=2,ILEV-1 
      DO 200 I=IL1,IL2
         D     =(SGBJ(I,L+1)-SGBJ(I,L)) / (SGBJ(I,L)-SGBJ(I,L-1)) 
         C(I,L)= A(I,L+1)*D 
         B(I,L)=-A(I,L+1)*D - A(I,L)
  200 CONTINUE
  
      L=ILEV
      DO 250 I=IL1,IL2
         B(I,L)=  - A(I,L)
     1            - GRAV*CDVLM(I)*SHJ(I,L)
     2           /( RGAS*TSGB(I,L)*(SGBJ(I,L)-SGBJ(I,L-1)) )
  250 CONTINUE
  
C     * DEFINE MATRIX TO INVERT = I-2 *DT *MAT(A,B,C).
  
      IF (NOGWDRG) THEN 
  
          DO 500 L=1,ILEV-1 
          DO 500 I=IL1,IL2
             A(I,L+1)=-TODT*A(I,L+1)
             C(I,L)=  -TODT*C(I,L)
  500     CONTINUE
  
          DO 550 L=1,ILEV 
          DO 550 I=IL1,IL2
             B(I,L)=1.-TODT*B(I,L)
  550     CONTINUE
  
      ENDIF 
  
      RETURN
C-----------------------------------------------------------------------
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
