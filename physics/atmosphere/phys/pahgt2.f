!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PAHGT2(PF,PH,ZF,ZH,THROW,TFROW,SHJ,SHTJ,PRESSG,RGAS,
     1                  GRAV,ILG,ILEV,LEV,IL1,IL2,ILEVP1)
C
C     * MAY 24/2017 - M.LAZARE.        FOR CONVERSION TO NEW XC40:
C     *                                - REPLACE IBM INTRINSICS BY GENERIC
C     *                                  FOR PORTABILITY.
C     * KNUT VON SALZEN - JUN 03,2013. NEW VERSION FOR GCM17+ WHICH
C     *                                ALSO CALCULATES MID-LAYER
C     *                                INFORMATION (PH,ZH).
C     * KNUT VON SALZEN - JUL 27,2009. PREVIOUS VERSION PAHGT2 FOR GCM15H+
C     *                                FOR CALCULATION OF PRESSURE AND 
C     *                                HEIGHT AT MODEL LEVELS.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N) 
C
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV)   :: PF            !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV)   :: PH            !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV)   :: ZF            !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV)   :: ZH            !<Variable description\f$[units]\f$
      REAL, INTENT(IN),  DIMENSION(ILG,ILEVP1) :: THROW         !<Variable description\f$[units]\f$
      REAL, INTENT(IN),  DIMENSION(ILG,ILEVP1) :: TFROW         !<Variable description\f$[units]\f$
      REAL, INTENT(IN),  DIMENSION(ILG,LEV)    :: SHTJ          !<Variable description\f$[units]\f$
      REAL, INTENT(IN),  DIMENSION(ILG,ILEV)   :: SHJ           !<Variable description\f$[units]\f$
      REAL, INTENT(IN),  DIMENSION(ILG)        :: PRESSG        !<Variable description\f$[units]\f$
      REAL, ALLOCATABLE, DIMENSION(:,:)        :: TERM1         !<Variable description\f$[units]\f$
      REAL, ALLOCATABLE, DIMENSION(:,:)        :: TERM2         !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      INTEGER*4 LEN
      INTEGER MACHINE,INTSIZE
C
C     * COMMON BLOCK TO HOLD WORD SIZE.
C
      COMMON /MACHTYP/ MACHINE,INTSIZE
C
C---------------------------------------------------------------------
C     * CALCULATE HEIGHT AND PRESSURE.
C
      DO L=1,ILEV
        PF(IL1:IL2,L)=SHTJ(IL1:IL2,L+1)*PRESSG(IL1:IL2)
        PH(IL1:IL2,L)=SHJ (IL1:IL2,L  )*PRESSG(IL1:IL2)
      ENDDO
      ROG=RGAS/GRAV
      ILEVM=ILEV-1
      ZF(IL1:IL2,ILEV) = 0.
      ILT=IL2-IL1+1
      ALLOCATE(TERM1(ILT,ILEVM))
      ALLOCATE(TERM2(ILT,ILEVM))
      DO L=1,ILEVM
        TERM1(1:ILT,L)=PF(IL1:IL2,L+1)/PF(IL1:IL2,L)
      ENDDO
      DO L=1,ILEVM
        DO I=1,ILT
          TERM2(I,L) = LOG(TERM1(I,L))
        ENDDO
      ENDDO
      DO L=ILEVM,1,-1
        ZF(IL1:IL2,L)=ZF(IL1:IL2,L+1)+ROG*THROW(IL1:IL2,L+2)
     1                                                *TERM2(1:ILT,L)
      ENDDO
      DO L=1,ILEVM
        TERM1(1:ILT,L)=PH(IL1:IL2,L+1)/PH(IL1:IL2,L)
      ENDDO
      DO L=1,ILEVM
        DO I=1,ILT
          TERM2(I,L) = LOG(TERM1(I,L))
        ENDDO
      ENDDO
      L=ILEV
      ZH(IL1:IL2,L)=ROG*TFROW(IL1:IL2,L+1)
     1                         *LOG(PRESSG(IL1:IL2)/PH(IL1:IL2,ILEV))
      DO L=ILEVM,1,-1
        ZH(IL1:IL2,L)=ZH(IL1:IL2,L+1)+ROG*TFROW(IL1:IL2,L+1)
     1                                                *TERM2(1:ILT,L)
      ENDDO
      DEALLOCATE (TERM1)
      DEALLOCATE (TERM2)
C
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
