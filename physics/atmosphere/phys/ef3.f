!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      FUNCTION EF3(RK,ARG)
C
C     * JUN 15/2013 - M.LAZARE.   NEW VERSION FOR GCM17+:
C     *                           - USES {RF3,RD3} INSTEAD OF {RF2,RD2}.
C     * SEP 11/2006 - F.MAJAESS.  PREVIOUS VERSION EF2, HARD-CODING
C     * SEP 11/2006 - F.MAJAESS.  NEW VERSION IN "MODEL" HARD-CODING
C     *                           REAL*8.
C     * MAR 24/1999 - J SCINOCCA. ORIGINAL VERSION EF IN "COMM".
C
C     * FUNCTION CALLED BY GWLOOKU2 
C     * USED TO EVALUATE INCOMPLETE ELLIPTIC INTEGRALS
C     * BASED ON ELLE OF NUMERICAL RECIPES
C

      IMPLICIT NONE
      REAL*8 RK !<Variable description\f$[units]\f$
      REAL*8 ARG!<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      REAL*8 EF3
      REAL*8 X,Y,Z,RKSQ

      REAL*8 RF3,RD3
      EXTERNAL RF3,RD3
C====================================================================
      RKSQ=RK**2

      X=COS(ARG)**2
      Y=1.E0-RKSQ*SIN(ARG)**2
      Z=1.E0

      EF3=SIN(ARG)*RF3(X,Y,Z)
     1        -RKSQ*SIN(ARG)**3*RD3(X,Y,Z)/3.E0

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
