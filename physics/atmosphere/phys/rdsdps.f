!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE RDSDPS(SNOW,ZSNOW,SPCP,RHOSNO,REFF,GCROW,
     1                  REFF0,DELT,ZSNMIN,ZSNMAX,ILG,IL1,IL2,GCMIN,
     2                  GCMAX) 
C
C     * M. Namazi &  K. V. SALZEN,SNOW GRAIN DUE TO DEPOSITION.
C 
      IMPLICIT NONE

      REAL :: REFF0
      INTEGER ILG,IL1,IL2,I
C
C     * INPUT/OUTPUT ARRAYS.
C
      REAL, DIMENSION(ILG) :: SNOW      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: ZSNOW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SPCP      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: RHOSNO    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: REFF      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: GCROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: TAUD      !<Variable description\f$[units]\f$
      REAL, PARAMETER :: SPCMIN=1.157407407E-8,
     1                   TAUINF=1.E+20      
      REAL  DELT        !<Variable description\f$[units]\f$
      REAL  ZSNMIN      !<Variable description\f$[units]\f$
      REAL  ZSNMAX      !<Variable description\f$[units]\f$
      REAL  GCMIN       !<Variable description\f$[units]\f$
      REAL  GCMAX       !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C------------------------------------------------------------------------------
      DO 100 I=IL1,IL2
          IF(GCROW(I).GT.GCMIN.AND.GCROW(I).LT.GCMAX)              THEN 
             IF(SNOW(I).GT.0.)                                    THEN
                REFF(I)=MAX(REFF0,REFF(I))
                IF(SPCP(I).GT.SPCMIN)                   THEN
                  IF(ZSNOW(I).LT.ZSNMAX.AND.ZSNOW(I).GT.ZSNMIN)   THEN
                     TAUD(I)=(ZSNOW(I)*RHOSNO(I))/SPCP(I)
                     REFF(I)=REFF0+(REFF(I)-REFF0)*EXP(-DELT/TAUD(I))
                  ELSEIF(ZSNOW(I).GT.ZSNMAX)                      THEN
                     TAUD(I)=(ZSNMAX*RHOSNO(I))/SPCP(I)
                     REFF(I)=REFF0+(REFF(I)-REFF0)*EXP(-DELT/TAUD(I))
                  ELSE
                     TAUD(I)=0.
                     REFF(I)=0.  
                  ENDIF
                ELSE
                  TAUD(I)=TAUINF 
                ENDIF
             ELSE
                TAUD(I)=0. 
                REFF(I)=0.
             ENDIF
          ENDIF
  100 CONTINUE 

      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
