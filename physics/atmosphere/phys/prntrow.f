!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PRNTROW (FLD,NAME,LOC,NLEV,LOFF,ILG,IL1,IL2,JLAT)
C 
C     * M.LAZARE. - AUG 13/90. ADD SECOND DIMENSION TO USE OF "FLD" FOR 
C     *                        SINGLE-LEVEL FIELDS, AS SHOULD BE. 
C     * M.LAZARE. - OCT 20/88.
C 
C     * PRINTS OUT ROW VALUES (BETWEEN IL1 AND IL2) OF ARRAY FLD, 
C     * FOR EACH OF NLEV LEVELS AT "JLAT" LATITUDE ROW INSIDE ROUTINE.
C     * 
C     *      FLD  = ARRAY TO BE PRINTED OUT.
C     *      NAME = NAME OF ARRAY TO BE INCLUDED IN PRINTOUT, 
C     *             I.E. "TH" FOR THROW.
C     *      LOC  = LOCATION WHERE PRINTOUT IS OCCURRING, I.E.
C     *             "AFTER VRTDFS". 
C     *             BEING DISPLAYED.
C     *      NLEV = NUMBER OF LEVELS FOR VARIABLE.
C     *      LOFF = NUMBER OF LEVELS (INCLUDING MOON LAYER) BEFORE
C     *             VARIABLE HAS VALUES, I.E. LOFF=MSG+1 FOR ESTG,
C     *             LOFF=0 FOR THROW, LOFF=1 FOR UROW. FOR SINGLE-
C     *             LEVEL FIELDS, SOME LEVEL INDICATOR (I.E. COULD BE 
C     *             DONE IN A VERTICAL LEVEL LOOP). 
C     *             MOON LAYER REFERENCED TO AS LEVEL "0" IN PRINTOUT.
C     *      ILG  = NUMBER OF EQUALLY-SPACED LONGITUDES TO BE PRINTED OUT.
C     *      IL1  = STARTING LONGITUDE NUMBER FOR PRINTOUT. 
C     *      IL2  = FINISHING LONGITUDE NUMBER FOR PRINTOUT.
C     *      JLAT = CURRENT LATITUDE ROW FOR PRINTOUT.
C 
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)

      REAL   FLD(ILG,NLEV)      !<Variable description\f$[units]\f$
      CHARACTER*14 LOC          !<Variable description\f$[units]\f$
      CHARACTER*7 NAME          !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C-----------------------------------------------------------------------
C 
C     * DISTINGUISH BETWEEN SINGLE-LEVEL AND MULTI-LEVEL FIELDS.
C 
      IF(NLEV.EQ.1) THEN
             WRITE(6,6100) LOC,LOFF,NAME,JLAT,(FLD(IL,1),IL=IL1,IL2)
 6100        FORMAT ('0',A14,', LEVEL',I3,' FOR VARIABLE ', 
     1               A7,' AT LATITUDE ROW',I3,' :',/,(5(1X,1PE24.16)))
      ELSE
         LIND=LOFF-1
         WRITE(6,6200)
 6200    FORMAT ('1') 
         DO 200 L=1,NLEV
             WRITE(6,6300) LOC,L+LIND,NAME,JLAT,(FLD(IL,L),IL=IL1,IL2)
 6300        FORMAT ('0',A14,', LEVEL',I3,' FOR VARIABLE ', 
     1               A7,' AT LATITUDE ROW',I3,' :',/,(5(1X,1PE24.16)))
  200    CONTINUE 
      ENDIF 
C 
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
