!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE NLCLMXTKE(XROW,QH,TH,P,Z,ZF,DSHJ,SHXKJ,SQFLX,TQFLX,
     1                     SHFLX,THFLX,CDM,CVSG,SIGMA,PRESSG,X,
     2                     ITRPHS,DT,GRAV,RGAS,TVFA,TFREZ,
     2                     TICE,HV,HS,IIWC,ILWC,MSG,IL1,IL2,ILG,ILEV,
     3                     LEV,NTRAC,QT,HMN,DSR,CHI,RRL,CPM,DSMIX,HINT,
     4                     QINT,TVPPBL,TAU,XINT,SXFLX,
     5                     BCR,ZER,QTN,HMNN,
     6                     QCW,ZCLF,ZCRAUT,SSH,VMODL,
     7                     ALMIX,  DHLDZ,   DRWDZ,   HMNMIN,    QTMIN, 
     8                     IMXL,CNDROL,DEPROL,XLWROL,XICROL,ISAVLS,
     9                     XLMTKE,CLD_he)      
C
C     * SEP 2020 - Y.HE,M.LAZARE,N. MCFARLANE. Updated with TKE related mixing length                                                                                      C     *                                        and multi-layer clouds, et. al.        
C     * MAY 03/2018 - N.MCFARLANE. Added code to prevent statcld crashes.
C     *                            Removed unused {ESW,ESI} statement functions.
C     * FEB 10/2015 - M.LAZARE/      New version for gcm18:
C     *               K.VONSALZEN:   - Comment-out lower bound on total
C     *                                water (don't think we need it
C     *                                any more due to implemented
C     *                                improvements and bugfixes elsewhere).
C     * JUL 31/2013 - M.LAZARE.    Previous version NLCLMX7 for gcm17:
C     *                            Cosmetic changes (since QTN.ge.0. in
C     *                            our tests) to:
C     *                            - Print out warning if QTN<0 before
C     *                              calling statcld5, then limit
C     *                              it to be non-negative (2x).
C     *                            - Pass in LVL and ICALL to statcld5
C     *                              to aid in future debugging.
C     * JUN 26/2013 - K.VONSALZEN. NEW VERSION FOR GCM17:
C     *                            - CALLS NEW STATCLD5.
C     * APR 29/2012 - K.VONSALZEN/ PREVIOUS VERSION NLCLMX6 FOR GCM16:
C     *               M.LAZARE.    - CALLS NEW STATCLD4.
C     * JAN 17/2008 - K.VONSALZEN/ PREVIOUS VERSION NLCLMX5 FOR GCM15G/H/I:
C     *               M.LAZARE.    - CALLS NEW STATCLD3.
C     *                            - BUGFIX IN EXPRESSION FOR USTAR TO
C     *                              PROPERLY INCLUDE WIND SPEED. 
C     *                            - MODIFIED CALCULATION OF ADJUSTMENT
C     *                              TIMESCALE IN MIXED LAYER (TAU).
C     *                            - USES CLEAR-SKY AND ALL-SKY MIXING
C     *                              LENGTHS (ALMC AND ALMX, RESPECTIVELY)
C     *                              TO DEFINE A CONSISTENT TOTAL
C     *                              MIXING LENGTH (ALMIX) PASSED TO
C     *                              STATCLD3.
C     *                            - PASSES DT(=ADELT FROM PHYSICS)
C     *                              TO NEW STATCLD3 IN PLACE OF
C     *                              HARD-CODED 2.*DELT.
C     *                            - GUSTINESS EFFECT NOW INCLUDED 
C     *                              SINCE IS NOW ALREADY CONTAINED IN ZSPD
C     *                              FROM PHYSICS.
C     * JAN 13/2007 - K.VONSALZEN. PREVIOUS VERSION NLCLMX4 FOR GCM15F.
C     *                            - MODIFIED CALL TO STATCLD2, IN
C     *                              CONJUNCTION WITH CHANGES TO ADD
C     *                              "QCWVAR".  
C     * NOV 28/2006 - M.LAZARE/    - ADD CALCULATION FOR CNDROL,DEPROL
C     *               K.VONSALZEN.   UNDER CONTROL OF "ISAVLS".
C     *                            - MOVE MIXING OF TRACERS TO BEFORE 
C     *                              CALCULATION OF MLSE AND TOTAL
C     *                              WATER, SO PROFILES ARE WELL-MIXED
C     *                              BEFORE BEING PROCESSED.  
C     *                              NOTE THAT NOW CLOUD WATER AND ICE
C     *                              ARE MIXED AS WELL.
C     * JUN 19/2006 - M.LAZARE.    - CALLS NEW VERSION STATCLD2.
C     *                            - COSMETIC: USE VARIABLE INSTEAD OF 
C     *                              CONSTANT IN INTRINSICS SUCH AS "MAX",
C     *                              SO THAT CAN COMPILE IN 32-BIT MODE
C     *                              WITH REAL*8.  
C     * MAY 06/06 - K.VONSALZEN/ PREVIOUS VERSION NLCLMX3 FOR GCM15E:
C     *             M.LAZARE.    - USE NEW COMMON SUBROUTINE STATCLD FOR 
C     *                            UNRAVELLING.
C     *                          - SINCE SIMILAR MIXING NOW ALSO DONE IN NEW
C     *                            SUBROUTINE VRTDF14 (IE ALSO CALLS
C     *                            STATCLD), PASS IN REQUIRED INPUT FIELDS
C     *                            INSTEAD OF RE-DERIVING THEM HERE.
C     *                          - REORDERING OF TERMS AND LIMITATION OF
C     *                            FLUXES AT TOP OF MIXED LAYER TO IMPROVE
C     *                            NUMERICAL ROBUSTNESS.
C     *                          - REVISIONS TO ALMIX AND CONVECTIVE
C     *                            CONTRIBUTION TO VARIANCE, CONSISTENT WITH
C     *                            COND3.
C     * DEC 14/05 - K.VONSALZEN/ PREVIOUS VERSION NLCLMX2 FOR GCM15D.
C     *             M.LAZARE.    
C     *
C     * PERFORMS NON-LOCAL MIXING IN PLANETARY BOUNDARY LAYER BASED
C     * ON SIMPLE ADJUSTMENT SCHEME. THE MIXING IS PERFORMED FOR LIQUID 
C     * WATER STATIC ENERGY AND TOTAL WATER. TEMPERATURE AND SPECIFIC 
C     * HUMIDITY ARE OBTAINED FROM UNRAVELLING PROCEDURE. A 
C     * SEMI-IMPLICIT METHOD IS USED.
C
      IMPLICIT REAL   (A-H,O-Z),
     +INTEGER (I-N)
C 
      REAL   XROW(ILG,LEV,NTRAC)!<Variable description\f$[units]\f$
      REAL   QH   (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   TH   (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   DSHJ (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   Z    (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   P    (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   ZF   (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   SHXKJ(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   CVSG (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   SIGMA(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   SQFLX  (ILG)       !<Variable description\f$[units]\f$
      REAL   TQFLX  (ILG)       !<Variable description\f$[units]\f$
      REAL   SHFLX  (ILG)       !<Variable description\f$[units]\f$
      REAL   THFLX  (ILG)       !<Variable description\f$[units]\f$
      REAL   CDM    (ILG)       !<Variable description\f$[units]\f$
      REAL   PRESSG (ILG)       !<Variable description\f$[units]\f$
      REAL   X      (ILG)       !<Variable description\f$[units]\f$
      REAL   ZER    (ILG)       !<Variable description\f$[units]\f$
      REAL   QTN    (ILG)       !<Variable description\f$[units]\f$
      REAL   HMNN   (ILG)       !<Variable description\f$[units]\f$
      REAL   QCW    (ILG)       !<Variable description\f$[units]\f$
      REAL   ZCLF   (ILG)       !<Variable description\f$[units]\f$
      REAL   ZCRAUT (ILG)       !<Variable description\f$[units]\f$
      REAL   SSH    (ILG)       !<Variable description\f$[units]\f$
      REAL   ALMIX  (ILG)       !<Variable description\f$[units]\f$
      REAL   DHLDZ  (ILG)       !<Variable description\f$[units]\f$
      REAL   DRWDZ  (ILG)       !<Variable description\f$[units]\f$
      REAL   HMNMIN (ILG)       !<Variable description\f$[units]\f$
      REAL   QTMIN  (ILG)       !<Variable description\f$[units]\f$
      REAL   VMODL  (ILG)       !<Variable description\f$[units]\f$
      REAL   DSR  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   CHI  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   CPM  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   RRL  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   QT   (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL   HMN  (ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  CNDROL(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  DEPROL(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  XLWROL(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  XICROL(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  XLMTKE(ILG,ILEV)    !<Variable description\f$[units]\f$
      REAL  CLD_he(ILG,ILEV)    !<Variable description\f$[units]\f$
C
C     INTERNAL WORK ARRAYS.
C
      REAL   WC(ILG,ILEV)       !<Variable description\f$[units]\f$
      REAL   FCWGT (ILG,ILEV)   !<Variable description\f$[units]\f$      
      REAL   DSMIX  (ILG)       !<Variable description\f$[units]\f$
      REAL   HINT   (ILG)       !<Variable description\f$[units]\f$
      REAL   QINT   (ILG)       !<Variable description\f$[units]\f$
      REAL   TVPPBL (ILG)       !<Variable description\f$[units]\f$
      REAL   TAU    (ILG)       !<Variable description\f$[units]\f$
      REAL   BCR    (ILG)       !<Variable description\f$[units]\f$
      REAL   XINT(ILG,NTRAC)    !<Variable description\f$[units]\f$
      REAL  SXFLX(ILG,NTRAC)    !<Variable description\f$[units]\f$
      REAL   ACOR   (ILG)       !<Variable description\f$[units]\f$
      REAL   QCWVAR (ILG)       !<Variable description\f$[units]\f$
      REAL   WCINV  (ILG)       !<Variable description\f$[units]\f$
      REAL   QTNEF  (ILG)       !<Variable description\f$[units]\f$
      REAL   HMNNEF (ILG)       !<Variable description\f$[units]\f$            

      INTEGER IMXL(ILG)         !<Variable description\f$[units]\f$
      INTEGER ITRPHS(NTRAC)     !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      PARAMETER ( QCWCRIT=0. )
      PARAMETER ( TAUPRP=1.0 )
      PARAMETER ( YSEC=0.999 )
C
      COMMON /EPS  / A,B,EPS1,EPS2                                     
      COMMON /ESTWI/ RW1,RW2,RW3,RI1,RI2,RI3
C
      DATA ZERO,ONE /0., 1./
      DATA PZ3  /0.03/
C=======================================================================
C     * CONSTANTS.
C
      EXPM1=EXP(-1.)
      CSIGMA=0.2
      ICVSG=0
      ISUBG=1
      YFR=0.50
      ZSECFRL=1.E-7
      ZEPCLC=1.E-3
C
C     * INITIAL PROFILES OF THERMODYNAMIC PROPERTIES:
C     * TOTAL WATER MIXING RATIO AND LIQUID WATER STATIC ENERGY,
C  
      DO 50 L=MSG+1,ILEV
      DO 50 IL=IL1,IL2 
        QCWX=(XROW(IL,L+1,ILWC)+XROW(IL,L+1,IIWC))*DSR(IL,L)
        QV=QH(IL,L)*DSR(IL,L)
        QT(IL,L)=QV+QCWX
        DST=CPM(IL,L)*TH(IL,L)+GRAV*Z(IL,L)
        HMN(IL,L)=DST-RRL(IL,L)*QCWX
        FCWGT(IL,L)=0.
        WC(IL,L)=PZ3
 50   CONTINUE  
C
C=======================================================================
C     * DETERMINE DEPTH OF MIXED LAYER FROM ADJUSTMENT SCHEME FOR
C     * MIXED LAYER WITH FLUXES ACROSS TOP AND BOTTOM LAYERS.
C
      L=ILEV
      DO 100 IL=IL1,IL2
        TVPOT=TH(IL,L)*(1.+TVFA*QH(IL,L))/SHXKJ(IL,L)
        TVPPBL(IL)=TVPOT*ZF(IL,L)
        HINT(IL)=HMN(IL,L)*DSHJ(IL,L)
        QINT(IL)=QT (IL,L)*DSHJ(IL,L)
        DSMIX(IL)=DSHJ(IL,L)
        IMXL(IL)=L
        BCR(IL)=9.E+20
        HMNMIN(IL)=HMN(IL,L)
        QTMIN(IL)=QT(IL,L)
        WCINV(IL)=0.        
 100  CONTINUE
C
C     * PERFORM SEMI-IMPLICIT TIME INTEGRATION TO DETERMINE
C     * THERMODYNAMIC PROPERTIES AT THE TOP OF THE VIRTUAL MIXED 
C     * LAYER. USE BUOYANCY IN THAT LAYER (RELATIVE TO LAYER ABOVE)
C     * TO DETERMINE DEPTH OF MIXED LAYER.
C
      DO 200 L=ILEV-1,MSG+2,-1
       DO 125 IL=IL1,IL2
        IF ( IMXL(IL).EQ.(L+1) ) THEN
C
C         * ADJUSTMENT TIME SCALE IN MIXED LAYER.
C
          RHO=100.*P(IL,ILEV)/(RGAS*TH(IL,ILEV))
          DRDQV=1.
          RFX=SQFLX(IL)*DRDQV/RHO
          TPOT=TH(IL,ILEV)/SHXKJ(IL,ILEV)
          VPTFLX=(SHFLX(IL)/(RHO*CPM(IL,ILEV)))*(1.+TVFA*QH(IL,ILEV))
     1           +TVFA*RFX*TPOT
          DPBL=ZF(IL,L+1)
          WSTAR=MAX(GRAV*VPTFLX*DPBL/(TVPPBL(IL)/DPBL),ZERO)**(1./3.)
          USTAR=SQRT(CDM(IL))*VMODL(IL)
c*FC      TAU(IL)=TAUPRP*DPBL/MAX(SQRT(0.6*WSTAR**2+USTAR**2),PZ3)
          WC(IL,L+1)=MAX((USTAR**3
     1                  +0.6*(Z(IL,L+1)/DPBL)*WSTAR**3)**(1./3.),PZ3)
          WCINV(IL)=WCINV(IL)
     1                  +RGAS*TH(IL,ILEV)*DSHJ(IL,L+1)/(GRAV*WC(IL,L+1))
          TAU(IL)=TAUPRP*WCINV(IL)
          FCWGT(IL,L+1)=(1./WC(IL,L+1)
     1                  -WCINV(IL)*GRAV/(RGAS*TH(IL,ILEV)*DSMIX(IL)))
C     
C         * LIQ. WATER STATIC ENERGY AND TOTAL WATER AT TOP OF MIXED LAYER.
C
          FAC=GRAV/(PRESSG(IL)*DSMIX(IL))
c*FC      WEIGH=MIN(MAX(TAU(IL)/(TAU(IL)+DT),ZERO),ONE)
          WEIGH=EXP(-DT/TAU(IL))
c*FC          
          HMNMIN(IL)=MIN(HMNMIN(IL),HMN(IL,L+1))
          THFLXT=MIN(THFLX(IL),SHFLX(IL)+YSEC*HMNMIN(IL)/(DT*FAC))
          QTMIN(IL)=MIN(QTMIN(IL),QT(IL,L+1))
          TQFLXT=MIN(TQFLX(IL),SQFLX(IL)+YSEC*QTMIN(IL)/(DT*FAC))
          HMNN(IL)=WEIGH*HMN(IL,L+1)+(1.-WEIGH)*HINT(IL)/DSMIX(IL)
     1            +DT*FAC*(SHFLX(IL)-THFLXT)
          QTN (IL)=WEIGH*QT (IL,L+1)+(1.-WEIGH)*QINT(IL)/DSMIX(IL)
     1            +DT*FAC*(SQFLX(IL)-TQFLXT)
          IF ( QTN(IL).LE.0. ) CALL WRN('NLCLMXTKE',-1)
c         QT(IL,L)=MAX(QT(IL,L),0.) 
C
          DZ       =Z   (IL,L-1)-Z  (IL,L)
          DHLDZ(IL)=(HMN(IL,L-1)-HMN(IL,L))/DZ
          DRWDZ(IL)=(QT (IL,L-1)-QT (IL,L))/DZ
C
          ALMIX(IL)=XLMTKE(IL,L)
          X(IL)=1.
        ELSE
          X(IL)=0. 
       ENDIF
c*FC
       QTNEF (IL)=QTN (IL)+(1.-WEIGH)*SQFLX(IL)*FCWGT(IL,L+1)
       HMNNEF(IL)=HMNN(IL)+(1.-WEIGH)*SHFLX(IL)*FCWGT(IL,L+1)
c*FC       
  125  CONTINUE 
C
       IDUM=0
       ICALL=3
       CALL STATCLD5(QCW,ZCLF,SIGMA(1,L),ZCRAUT,QCWVAR,SSH,
     1               CVSG(1,L),QTNEF,HMNNEF,CHI(1,L),CPM(1,L),
     2               P(1,L),Z(1,L),RRL(1,L),ZER,X,
     3               ALMIX,DHLDZ,DRWDZ,IDUM,
     4               GRAV,DT,ILEV,ILG,IL1,IL2,ICVSG,ISUBG,
     5               L,ICALL                                 )
C
       DO 150 IL=IL1,IL2
         IF ( IMXL(IL).EQ.(L+1) ) THEN
C
C         * CLOUD LIQUID AND ICE WATER CONTENTS FROM DIAGNOSTIC
C         * RELATIONSHIP.
C
          QLWC=(1.-CHI(IL,L))*QCW(IL)
          QIWC=CHI(IL,L)*QCW(IL)
C
C         * ADJUSTED WATER VAPOUR MIXING RATIO AND TEMPERATURE.
C
          QVN=QTNEF(IL)-QCW(IL)
          TN =(HV*QLWC+HS*QIWC-GRAV*Z(IL,L)+HMNNEF(IL))/CPM(IL,L)
C
C         * BUOYANCY FOR AIR AT TOP OF VIRTUAL MIXED LAYER IN AMBIENT
C         * AIR AT THAT LEVEL.
C
          QCWE=(XROW(IL,L+1,ILWC)+XROW(IL,L+1,IIWC))*DSR(IL,L)
          QVE=QH(IL,L)*DSR(IL,L)
          TV =TH(IL,L)*(1.+TVFA*QVE-(1.+TVFA)*QCWE)
          TPV=TN      *(1.+TVFA*QVN-(1.+TVFA)*QCW(IL) )
          BUOY=GRAV*( TPV-TV )/TV
C
C         * TEST FOR BUOYANCY.
C
          IF ( BUOY.GE.0. .AND. 
     1        .NOT.(BUOY.GT.BCR(IL).AND.QCW(IL).GT.QCWCRIT)   ) THEN
            IMXL(IL)=L
C
C         * VERTICALLY INTEGRATED LIQ. WATER ENERGY AND TOTAL WATER,
C         * AND DEPTH OF MIXED LAYER.
C
            TVPOT=TH(IL,L)*(1.+TVFA*QH(IL,L))/SHXKJ(IL,L)
            TVPPBL(IL)=TVPPBL(IL)+TVPOT*(ZF(IL,L)-ZF(IL,L+1))
            HINT(IL)=HINT(IL)+HMN(IL,L)*DSHJ(IL,L)
            QINT(IL)=QINT(IL)+QT (IL,L)*DSHJ(IL,L)
            DSMIX(IL)=DSMIX(IL)+DSHJ(IL,L)
            BCR(IL)=BUOY
          ENDIF
        ENDIF
  150  CONTINUE
  200  CONTINUE
c*FC
c*FC update the free convective increment profile function.
c*FC
      DO 210 L=ILEV,MSG+1,-1
      DO 210 IL=IL1,IL2
        IF(L.GE.IMXL(IL)) THEN
            FCWGT(IL,L)=(1./WC(IL,L)
     1        -WCINV(IL)*GRAV/(RGAS*TH(IL,ILEV)*DSMIX(IL)))
        ENDIF
 210  CONTINUE
c*FC      
C
C     TRACERS.
C
      DO 400 N=1,NTRAC 
        IF ( ITRPHS(N).NE.0 .OR. N.EQ.ILWC .OR. N.EQ.IIWC ) THEN
          DO 420 IL=IL1,IL2 
            XINT (IL,N)=0.
            SXFLX(IL,N)=0.
 420      CONTINUE
          DO 440 L=ILEV,MSG+1,-1 
          DO 440 IL=IL1,IL2 
            IF ( L.GE.IMXL(IL) ) THEN
              XINT(IL,N)=XINT(IL,N)+XROW(IL,L+1,N)*DSHJ(IL,L)
            ENDIF
 440      CONTINUE
          DO 460 L=ILEV,MSG+1,-1 
          DO 460 IL=IL1,IL2 
            IF ( L.GE.IMXL(IL) ) THEN
C
C             * PERFORM SEMI-IMPLICIT TIME INTEGRATION TO DETERMINE
C             * THERMODYNAMIC PROPERTIES.
C
              FAC=GRAV/(PRESSG(IL)*DSMIX(IL))
c*FC          WEIGH=MIN(MAX(TAU(IL)/(TAU(IL)+DT),0.),1.)
c*FC          XROW(IL,L+1,N)=WEIGH*XROW(IL,L+1,N)+(1.-WEIGH)*XINT(IL,N)
c*FC     1                                /DSMIX(IL)+DT*FAC*SXFLX(IL,N)
              WEIGH=EXP(-DT/TAU(IL))
              XINTEF=XINT(IL,N)+ SXFLX(IL,N)*FCWGT(IL,L)*DSMIX(IL)
              XROW(IL,L+1,N)=WEIGH*XROW(IL,L+1,N)+(1.-WEIGH)*XINTEF
     1                               /DSMIX(IL)+DT*FAC*SXFLX(IL,N)
            END IF         
 460      CONTINUE
        ENDIF
 400  CONTINUE
C
C     * SAVE CLOUD WATER/ICE PROFILES AFTER MIXING AS PASSIVE TRACERS.
C
      IF ( ISAVLS.NE.0 ) THEN
        DO 470 L=MSG+1,ILEV
        DO 470 IL=IL1,IL2 
          XLWROL(IL,L)=XROW(IL,L+1,ILWC)
          XICROL(IL,L)=XROW(IL,L+1,IIWC)
 470    CONTINUE
      ENDIF
C
      DO 480 IL=IL1,IL2
        ACOR(IL)=1.
 480  CONTINUE

      DO 485 L=ILEV,MSG+1,-1
      DO 485 IL=IL1,IL2
        IF ( L.GE.IMXL(IL) ) THEN
          FAC=GRAV/(PRESSG(IL)*DSMIX(IL))
c*FC      WEIGH=MIN(MAX(TAU(IL)/(TAU(IL)+DT),ZERO),ONE)
          WEIGH=EXP(-DT/TAU(IL))
          DQ1=DT*FAC*SQFLX(IL)
          DQ2=(1.-WEIGH)*(QINT(IL)/DSMIX(IL)-QT(IL,L))
          DQSUM=DQ1+DQ2
          IF(DQSUM+QT(IL,L).LT.0.) THEN
            CALL WRN('NLCLMXTKE',-3)
            ACORT=-MAX(QT(IL,L),0.)/DQSUM
            ACOR1=MAX(MIN(ACOR(IL),ACORT),0.)
            ACOR(IL)=(3.-2.*ACOR1)*ACOR1**3
          ENDIF
        ENDIF
 485  CONTINUE
C
C=======================================================================
C     * CALCULATE NEW THERMODYNAMIC VALUES IN MIXED LAYER FOR GIVEN
C     * MIXED LAYER DEPTH AND SURFACE FLUXES. EFFECTS OF FLUXES
C     * ACROSS TOP OF MIXED LAYER ARE ACCOUNTED FOR IN PARAMETERIZATIONS
C     * OF CONVECTION AND THEREFORE DO NOT AFFECT THESE CALCULATION.
C
      DO 550 L=ILEV,MSG+1,-1
       DO 525 IL=IL1,IL2
        IF ( L.GE.IMXL(IL) ) THEN
C
C         * LIQ. WATER STATIC ENERGY AND TOTAL WATER.
C
          FAC=GRAV/(PRESSG(IL)*DSMIX(IL))
c*FC      WEIGH=MIN(MAX(TAU(IL)/(TAU(IL)+DT),ZERO),ONE)
          WEIGH=EXP(-DT/TAU(IL))
          HMNN(IL)=WEIGH*HMN(IL,L)+(1.-WEIGH)*HINT(IL)/DSMIX(IL)
     1            +DT*FAC*SHFLX(IL)
          QTNADJ=QT(IL,L)+ACOR(IL)*((1.-WEIGH)*(QINT(IL)/
     1           DSMIX(IL)-QT(IL,L))+DT*FAC*SQFLX(IL))
          QTN (IL)=QTNADJ
          IF ( QTN(IL).LE.0. ) CALL WRN('NLCLMXTKE',-2)
c         QTN(IL)=MAX(QTN(IL),0.) 
C
          DZ       =Z   (IL,L-1)-Z  (IL,L)
          DHLDZ(IL)=(HMN(IL,L-1)-HMN(IL,L))/DZ
          DRWDZ(IL)=(QT (IL,L-1)-QT (IL,L))/DZ
          ALMIX(IL)=XLMTKE(IL,L)
          X(IL)=1.
        ELSE
          X(IL)=0.
       ENDIF
c*GC
       QTNEF (IL)=QTN (IL)+(1.-WEIGH)*SQFLX(IL)*FCWGT(IL,L)
       HMNNEF(IL)=HMNN(IL)+(1.-WEIGH)*SHFLX(IL)*FCWGT(IL,L)
c*FC       
  525 CONTINUE 
C
      IDUM=0
      ICALL=4
      CALL STATCLD5(QCW,ZCLF,SIGMA(1,L),ZCRAUT,QCWVAR,SSH,
     1               CVSG(1,L),QTNEF,HMNNEF,CHI(1,L),CPM(1,L),
     2               P(1,L),Z(1,L),RRL(1,L),ZER,X,
     3               ALMIX,DHLDZ,DRWDZ,IDUM,
     4               GRAV,DT,ILEV,ILG,IL1,IL2,ICVSG,ISUBG,
     5               L,ICALL                                 )
C
      DO 540 IL=IL1,IL2
c***
        CLD_he(IL,L)=ZCLF(IL)  
c***                  
        IF ( L.GE.IMXL(IL) ) THEN
C
C         * CLOUD LIQUID AND ICE WATER CONTENTS FROM DIAGNOSTIC
C         * RELATIONSHIP.
C
          QLWC=(1.-CHI(IL,L))*QCW(IL)
          QIWC=CHI(IL,L)*QCW(IL)
          XROW(IL,L+1,ILWC)=QLWC/DSR(IL,L)
          XROW(IL,L+1,IIWC)=QIWC/DSR(IL,L)
C
C         * ADJUSTED WATER VAPOUR MIXING RATIO AND TEMPERATURE.
C
c*FC      QVN=QTN(IL)-QCW(IL)
c*FC      QH(IL,L)=QVN/DSR(IL,L)
c*FC      TH(IL,L)=(HV*QLWC+HS*QIWC-GRAV*Z(IL,L)+HMNN(IL))/CPM(IL,L)
c*FC
          WEIGH=EXP(-DT/TAU(IL))
          QTNEF (IL)=QTN (IL)+(1.-WEIGH)*SQFLX(IL)*FCWGT(IL,L)
          HMNNEF(IL)=HMNN(IL)+(1.-WEIGH)*SHFLX(IL)*FCWGT(IL,L)
          QVNEF=QTNEF(IL)-QCW(IL)          
          QH(IL,L)=QVNEF/DSR(IL,L)
          TH(IL,L)=(HV*QLWC+HS*QIWC-GRAV*Z(IL,L)+HMNNEF(IL))/CPM(IL,L)
        ENDIF
  540  CONTINUE
  550 CONTINUE
C
C     * SAVE CONDENSATION/DEPOSITION TENDENCIES.
C
      IF ( ISAVLS.NE.0 ) THEN
        DO 600 L=MSG+1,ILEV
        DO 600 IL=IL1,IL2 
          CNDROL(IL,L)=(XROW(IL,L+1,ILWC)-XLWROL(IL,L))/DT
          DEPROL(IL,L)=(XROW(IL,L+1,IIWC)-XICROL(IL,L))/DT
  600   CONTINUE
      ENDIF
C
      RETURN
C-----------------------------------------------------------------------
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
