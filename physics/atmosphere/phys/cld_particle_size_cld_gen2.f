!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CLD_PARTICLE_SIZE_CLD_GEN2(REL_SUB, REI_SUB,      ! OUTPUT
     1                                      CDD, RADEQVW, RADEQVI, ! INPUT
     3                                      CLW_SUB, CIC_SUB,
     4                                      IL1, IL2, ILG, LAY, LEV, 
     5                                      NX_LOC, IPPH_RE)

!      * FEB 12/2009 - JASON COLE.  NEW VERSION FOR GCM15H:
!      *                            - HARD-CODED VALUE OF 75.46 FOR
!      *                              LIQUID EQUIVILENT RADIUS IS
!      *                              NOW PROPERLY DECOMPOSED INTO
!      *                              PIFAC=62.035 AND THE "TUNABLE"
!      *                              FACTOR BETA (INCREASED TO 1.3
!      *                              TO GIVE BETTER AGREEMENT WITH
!      *                              OBSERVATIONS OF LIQUID EQUIVILENT
!      *                              RADIUS). 
!      * DEC 12/2007 - JASON COLE.  PREVIOUS VERSION 
!      *                            CLD_PARTICLE_SIZE_CLD_GEN FOR GCM15G:
!      *                            - INITIAL VERSION OF CLOUD GENERATOR
C      *                              FOR MCICA.

      IMPLICIT NONE

!
! INPUT DATA
!

      REAL, INTENT(IN) :: CLW_SUB(ILG,LAY,NX_LOC) ! Subgrid cloud liquid water\f$[units]\f$
      REAL, INTENT(IN) :: CIC_SUB(ILG,LAY,NX_LOC) !<Subgrid cloud ice water\f$[units]\f$
      REAL, INTENT(IN) :: CDD(ILG,LAY)            !<Cloud liquid water droplet number concentration\f$[units]\f$
      REAL, INTENT(IN) :: RADEQVW(ILG,LAY)        !<Liquid cloud particle size\f$[units]\f$
      REAL, INTENT(IN) :: RADEQVI(ILG,LAY)        !<Ice cloud particle size\f$[units]\f$

      INTEGER, INTENT(IN) :: IL1                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: IL2                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: ILG                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LAY                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LEV                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: NX_LOC             !<Variable description\f$[units]\f$

      INTEGER, INTENT(IN) :: IPPH_RE            !<Variable description\f$[units]\f$

!
! OUTPUT DATA
!

      REAL, INTENT(OUT) :: REL_SUB(ILG,LAY,NX_LOC)      !<Variable description\f$[units]\f$
      REAL, INTENT(OUT) :: REI_SUB(ILG,LAY,NX_LOC)      !<Variable description\f$[units]\f$

C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
!
! LOCAL DATA
!

      REAL :: 
     1 WCDW,
     2 WCDI,
     3 REI_LOC,
     4 REL_LOC,
     5 CIC_LOC,
     6 PCDNC,
     7 ROG

      INTEGER ::
     1 IL,
     2 ILAY,
     3 ICOL

!
! PARAMETERS
!

      REAL, PARAMETER :: THIRD    = 0.3333333333333

! THE FOLLOWING PARAMETERS ARE DEFINED IN CLOUDS16. MAKE SURE THESE ARE
! CONSISTENT!

      REAL, PARAMETER :: CICMIN   = 0.00001  
      REAL, PARAMETER :: CLWMIN   = 0.00001         
      REAL, PARAMETER :: RIEFFMAX = 50.0
      REAL, PARAMETER :: RWEFFMIN = 2.0
      REAL, PARAMETER :: RWEFFMAX = 30.0 
      REAL, PARAMETER :: PIFAC   = 62.035
      REAL, PARAMETER :: BETA    = 1.3   
!-------------------------------------------------------------------------
      IF (IPPH_RE .EQ. 0) THEN  ! HORIZONTALLY HOMOGENEOUS CLOUD PARTICLE EFFECTIVE SIZE

! ASSIGN THE HORIZONTALLY CONSTANT PARTICLE SIZES TO EACH CLOUDY CELL
! IN THE GENERATED SUBCOLUMNS

         DO ICOL = 1, NX_LOC
            DO ILAY = 1, LAY
               DO IL = IL1, IL2
                  REL_SUB(IL,ILAY,ICOL) = RADEQVW(IL,ILAY)
                  REI_SUB(IL,ILAY,ICOL) = RADEQVI(IL,ILAY)
               END DO ! IL
            END DO ! ILAY
         END DO ! ICOL

      ELSEIF (IPPH_RE .EQ. 1) THEN !HORIZONTALLY INHOMOGENEOUS CLOUD PARTICLE SIZES

         DO ICOL = 1, NX_LOC
            DO ILAY = 1, LAY
               DO IL = IL1, IL2
                     REL_LOC = BETA*PIFAC
     1                    * (CLW_SUB(IL,ILAY,ICOL)/CDD(IL,ILAY))**THIRD
                     REL_SUB(IL,ILAY,ICOL) = 
     1                             MAX(MIN(REL_LOC,RWEFFMAX),RWEFFMIN)
                     CIC_LOC = MAX(CIC_SUB(IL,ILAY,ICOL),CICMIN)
                     REI_LOC = 83.8*CIC_LOC**0.216
                     REI_SUB(IL,ILAY,ICOL) = MIN(REI_LOC,RIEFFMAX)
               END DO ! IL
            END DO ! ILAY
         END DO ! ICOL
      END IF
      
      RETURN
      END 
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
