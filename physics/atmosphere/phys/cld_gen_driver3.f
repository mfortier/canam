!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CLD_GEN_DRIVER3(NCLDY, CLW_SUB,CIC_SUB,                ! OUTPUT
     1                           RLC_CF, RLC_CW, SIGMA_QCW, IPPH,       ! INPUT
     2                           IOVERLAP, CLD, SHJ, SHTJ, CLW, CIC, T,                                  
     3                           PS, ISEEDROW,
     4                           IL1, IL2, ILG, LAY, LEV, NX_LOC, MTOP )

C      * APR 30/2012 - JASON COLE.   NEW VERSION FOR GCM16:
C      *                             REMOVE UNUSED INTERNAL ARRAYS
C      *                             {X,X1,X2,Y,Y1,Y2}, INCLUDING CALL
C      *                             IN NEW CLD_GENERATOR3.
C      * DEC 12/2007 - JASON COLE.   PREVIOUS VERSION CLD_GEN_DRIVER2 FOR 
C      *                             GCM15G/H/I:
C      *                             - HANDLE 3 DIFFERENT OVERLAP
C      *                               METHODS (DEFINED USING IOVERLAP).
C      *                             - REMOVE CODE RELATED TO SETTING
C      *                               LPPH AND IMAXRAN SINCE NOW USING
C      *                               IPPH AND IOVERLAP.
C      *                             - CALLS NEW VERSION CLD_GENERATOR2.
C      * JAN 09/2007 - JASON COLE.   PREVIOUS VERSION CLD_GEN_DRIVER FOR GCM15F.
! --------------------------------------------------------------------
! Driver to call stochastic cloud generator
! --------------------------------------------------------------------   
! JNSC GCM15D NOW CARRIES EXTRA FIELDS FOR THE SHALLOW CONVECTION CLOUD AMOUNT,
! JNSC CLOUD WATER CONTENT AND CLOUD DROPLET NUMBER CONCENTRATION.  RIGHT NOW
! JNSC I AM JUST USING THE DEFACTO METHOD FROM GCM15D.  THIS METHOD CAN LIKELY 
! JNSC BE IMPROVED USING MCICA AND SAMPLING

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

! Note: LAY    => Number of layers
! Note: ILG    => Number of GCM columns
! Note: NX_LOC => Number of subcolumns to generate

!
! PARAMETER
!

      REAL, PARAMETER :: 
     1 M2KM = 1.0/1000.0, ! Convert meters to kilometers
     2 CUT  = 0.001

!     
! INPUT DATA
!

      REAL, INTENT(IN) :: SHJ(ILG, LAY)         !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: T(ILG,LAY)            !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: CLD(ILG,LAY)          !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: PS(ILG)               !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: RLC_CF(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: RLC_CW(ILG,LAY)       !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: SIGMA_QCW(ILG,LAY)    !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: CLW(ILG,LAY)          !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: CIC(ILG,LAY)          !<Variable description\f$[units]\f$
      REAL, INTENT(IN) :: SHTJ(ILG,LEV)         !<Variable description\f$[units]\f$

      INTEGER, INTENT(IN) :: ILG                !<Counters and array sizes\f$[units]\f$
      INTEGER, INTENT(IN) :: IL1                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: IL2                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LAY                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LEV                !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: NX_LOC             !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: MTOP               !<Variable description\f$[units]\f$

      INTEGER, INTENT(IN) :: IOVERLAP            !<Overlap flag\f$[units]\f$
      INTEGER, INTENT(IN) :: IPPH                !<Plane-parallel homogeneous flag\f$[units]\f$

!
! OUTPUT DATA
!

      REAL, INTENT(OUT) :: CLW_SUB(ILG,LAY,NX_LOC)  !<Subcolumn of cloud liquid water contents       (g/m^3)\f$[units]\f$
      REAL, INTENT(OUT) :: CIC_SUB(ILG,LAY,NX_LOC)  !<Subcolumn of cloud ice water contents          (g/m^3)\f$[units]\f$
      INTEGER,INTENT(OUT) :: NCLDY(ILG)             !<Number of cloudy subcolumns\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
!
! WORK ARRAYS
!

      REAL ::
     1 QI_PROF(ILG,LAY),
     2 QC_PROF(ILG,LAY),
     3 ZM(ILG,LAY)

      REAL ::
     1 ALPHA(ILG,LAY), ! Fraction of maximum/random cloud overlap 
     2 RCORR(ILG,LAY)  ! Fraction of maximum/random cloud condensate overlap

      INTEGER ::
     1 I_LOC(ILG) ! Place the new subcolumns into the arrays starting from the front

      LOGICAL ::
     1 L_CLD(ILG)

      INTEGER(4) :: ISEEDROW(ILG,4) ! Seed for kissvec RNG

!
! LOCAL DATA (SCALAR)
!

      INTEGER ::
     1 IL,       ! Counter over GCM columns
     2 II,       ! Counter over NX subcolumns
     3 KK        ! Counter over lay vertical layers

      REAL ::
     1 RHO,      ! Density of air                               (g/m^3)           
     2 P,        ! GCM column pressure at layer midpoint        (Pa)
     3 ROG,      ! Total gas constant/gravity
     4 DMULT

      COMMON /PARAMS / WW, TWW, RAYON, ASQ, GRAV, RGAS, RGOCP, 
     1                 RGOASQ, CPRES, RGASV, CPRESV
      COMMON /EPS    / A, B, EPS1, EPS2 
      
! Zero out fields
      DO IL = IL1,IL2
         NCLDY(IL)  = 0
      END DO ! IL

      DO KK = 1, LAY 
         DO IL = IL1,IL2
            QC_PROF(IL,KK) = 0.0
            QI_PROF(IL,KK) = 0.0
            ALPHA(IL,KK)   = 0.0
            RCORR(IL,KK)   = 0.0
         END DO ! IL
      END DO ! KK

      DO II = 1, NX_LOC
         DO KK = 1 , LAY
            DO IL = IL1, IL2
               CIC_SUB(IL,KK,II) = 0.0
               CLW_SUB(IL,KK,II) = 0.0
            END DO
         END DO
      END DO

! Compute the heights of mid-layers
! Needed if decorrelation lengths are used
      ROG=RGAS/GRAV    

      DO IL = IL1, IL2
         P          = SHJ(IL,LAY)*PS(IL) ! Since using ratios
         ZM(IL,LAY) = ROG*T(IL,LAY)*LOG(PS(IL)/P)*M2KM
      END DO ! IL

      DO KK = LAY-1, 1, -1
         DO IL = IL1, IL2
            P1 = SHJ(IL,KK)*PS(IL)
            P2 = SHJ(IL,KK+1)*PS(IL)
            ZM(IL,KK) = ZM(IL,KK+1) + ROG*T(IL,KK)*LOG(P2/P1)*M2KM
         END DO ! IL
      END DO ! KK

! Compute the cloud fraction and layer mean liquid cloud condensate accounting for shallow
! convection

! Convert the cloud condensate from kg/kg to g/m^3 since these are the units used to generate
! cloud optical properties
!
! Compute the cloud fraction and layer mean liquid cloud condensate accounting for shallow
! convection
      DO KK = 1, LAY
         DO IL = IL1, IL2
            P = SHJ(IL,KK)*PS(IL)
            IF (CLD(IL,KK) .GE. CUT) THEN
! RHO in g/m^3                  
               RHO            = 1000.0*P/(RGAS*T(IL,KK))
               DMULT          = RHO /CLD(IL,KK)
               QI_PROF(IL,KK) = CIC(IL,KK)*DMULT
               QC_PROF(IL,KK) = CLW(IL,KK)*DMULT
            END IF
         END DO ! IL
      END DO ! KK

! Call cloud generator
      CALL CLD_GENERATOR3(CIC_SUB, CLW_SUB, NCLDY,
     1                    ZM, CLD, QC_PROF, QI_PROF,
     2                    RLC_CF, RLC_CW, SIGMA_QCW,
     3                    ILG, IL1, IL2, LAY, NX_LOC,
     4                    ALPHA, RCORR, ISEEDROW,
     5                    I_LOC, L_CLD, IPPH, IOVERLAP)

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
