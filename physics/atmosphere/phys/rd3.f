!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      FUNCTION RD3(X,Y,Z)
C
C     * JUN 15/2013 - M.LAZARE.   NEW VERSION FOR GCM17+:
C     *                           - DEFINE "TINY" AND "BIG" IN
C     *                             DOUBLE-PRECISION FORMAT, TO 
C     *                             AVOID INACCURACY WHO SHOWED UP
C     *                             (ONLY) IN RCM.
C     * SEP 11/2006 - F.MAJAESS.  PREVIOUS VERSION RD2, HARD-CODING
C     *                           REAL*8.
C     * MAR 24/1999 - J SCINOCCA. ORIGINAL VERSION RD IN "COMM".
C
C     * NUMERICAL RECIPES ROUTINE USED TO EVALUATE INCOMPLETE
C     * ELLIPTIC INTEGRALS. CALLED BY GWLOOKU2 THROUGH FUNCTIONS
C     * EF KF 
C     * "PAUSE" STATEMENT IS REPLACED WITH WITH A CALL TO THE 
C     * "XIT" SUBROUTINE.
C     (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ
C
      IMPLICIT NONE
      REAL*8 RD3,ERRTOL,TINY,BIG,C1,C2,C3,C4,C5,C6
      REAL*8 X  !<Variable description\f$[units]\f$
      REAL*8 Y  !<Variable description\f$[units]\f$
      REAL*8 Z  !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      PARAMETER (ERRTOL=0.05E0,TINY=1.D-50,BIG=4.5D42,
     1           C1=3.E0/14.E0,C2=1.E0/6.E0,C3=9.E0/22.E0,C4=3.E0/26.E0,
     2           C5=0.25E0*C3,C6=1.5E0*C4)

      REAL*8 ALAMB,AVE,DELX,DELY,DELZ,EA,EB,EC,ED,EE,FAC,
     1       SQRTX,SQRTY,SQRTZ,SUM,XT,YT,ZT
C======================================================================
      IF(MIN(X,Y).LT.0.E0.OR.MIN(X+Y,Z).LT.TINY
     1                 .OR.MAX(X,Y,Z).GT.BIG) THEN
        WRITE(6,'(A)') ' INVALID ARGUMENTS IN RD'
        CALL                                       XIT ('RD3',-1)
      ENDIF

      XT=X
      YT=Y
      ZT=Z
      SUM=0.E0
      FAC=1.E0

    1 CONTINUE
        SQRTX=SQRT(XT)
        SQRTY=SQRT(YT)
        SQRTZ=SQRT(ZT)
        ALAMB=SQRTX*(SQRTY+SQRTZ)+SQRTY*SQRTZ
        SUM=SUM+FAC/(SQRTZ*(ZT+ALAMB))
        FAC=0.25E0*FAC
        XT=0.25E0*(XT+ALAMB)
        YT=0.25E0*(YT+ALAMB)
        ZT=0.25E0*(ZT+ALAMB)
        AVE=0.2E0*(XT+YT+3.E0*ZT)
        DELX=(AVE-XT)/AVE
        DELY=(AVE-YT)/AVE
        DELZ=(AVE-ZT)/AVE
      IF(MAX(ABS(DELX),ABS(DELY),ABS(DELZ)).GT.ERRTOL)GOTO 1

      EA=DELX*DELY
      EB=DELZ*DELZ
      EC=EA-EB
      ED=EA-6.E0*EB
      EE=ED+EC+EC
      RD3=3.E0*SUM+FAC*(1.E0+ED*(-C1+C5*ED-C6*DELZ*EE)
     1                 +DELZ*(C2*EE+
     2                  DELZ*(-C3*EC+DELZ*C4*EA)))
     3                 /(AVE*SQRT(AVE))

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
