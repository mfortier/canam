!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE PREP_CLD_GEN2(SIGMA_QCW, RLC_CF, RLC_CW, ! OUTPUT
     2                         ANU,                       ! INPUT
     3                         IL1, IL2, ILG, LAY)
C
C      * DEC 12/2007 - JASON COLE.   NEW VERSION FOR GCM15G:
C      *                             - HANDLE 3 DIFFERENT OVERLAP
C      *                               METHODS (DEFINED USING IOVERLAP).
C      *                             - REMOVE CODE RELATED TO SETTING
C      *                               LPPH AND IMAXRAN SINCE NOW USING
C      *                               IPPH AND IOVERLAP.
C   * J. COLE. OCT. 4, 2006  STRIPPED DOWN VERSION FOR GCM15E.2 AND GCM15.
C   * J. COLE, K. VON SALZEN. JUNE 5, 2006. MODIFY SUBROUTINE TO USE NEW METHOD TO COMPUTE 
C                                           NU IN GCM.  IT IS COMPUTED IN COND4 AND SO IS
C                                           SIMPLY PASSED INTO THIS SUBROUTINE.
C   * J. COLE. JAN 16, 2006. SUBROUTINE TO SET THE VARIOUS PARAMETERS NEEDED 
C                            IN THE STOCHASTIC CLOUD GENERATOR.  ALSO SET
C                            VARIOUS FIELDS DIAGNOSED FROM CLOUD PROPERTIES
C                            SUCH AS CLOUD AMOUNT, CLOUD WATER CONTENT

      IMPLICIT NONE

!
! INPUT DATA
!

      REAL, INTENT(IN) :: ANU(ILG,LAY)      !<SQUARE OF MEAN OVER STANDARD DEVIATION OF TAU (OR CLOUD WATER)\f$[units]\f$

      INTEGER, INTENT(IN) :: IL1        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: IL2        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: ILG        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LAY        !<Variable description\f$[units]\f$

!
! OUTPUT DATA
!
      REAL, INTENT(OUT) :: SIGMA_QCW(ILG,LAY) !<NORMALIZED STANDARD DEVIATION OF CLOUD CONDENSATE\f$[units]\f$
      REAL, INTENT(OUT) :: RLC_CF(ILG,LAY)    !<CLOUD FRACTION DECORRELATION LENGTHS IN (KM)\f$[units]\f$
      REAL, INTENT(OUT) :: RLC_CW(ILG,LAY)    !<CLOUD CONDENSATE DECORRELATION LENGTHS IN (KM)\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


!
! LOCAL DATA
!

      INTEGER :: ! COUNTERS
     1 IL,      
     2 KK

! ZERO OUT THE OUTPUT ARRAYS
      DO KK = 1, LAY
         DO IL = IL1, IL2
            SIGMA_QCW(IL,KK) = 0.0
            RLC_CF(IL,KK)    = 0.0
            RLC_CW(IL,KK)    = 0.0
         END DO ! IL
      END DO ! KK


! FOR NOW, SET THE DECORRELATION LENGTHS TO BE REASONABLE ESTIMATES
! UPDATE WHEN HAVE SOME CLUE ABOUT HOW TO DO THIS BASED ON LARGE-SCALE
! VARIABLES

      DO KK = 1, LAY
         DO IL = IL1, IL2
            RLC_CF(IL,KK) = 2.0 ! IN KILOMETERS
            RLC_CW(IL,KK) = 1.0 ! IN KILOMETERS
         END DO
      END DO

! COMPUTE THE NORMALIZED STANDARD DEVIATION OF CLOUD CONDENSATE
! USING ANU FROM CLDIFM3

      DO KK = 1, LAY
         DO IL = IL1, IL2
            IF (ANU(IL,KK) .LE. 0.0) THEN
               SIGMA_QCW(IL,KK) = 0.0
            ELSE
               SIGMA_QCW(IL,KK) = 1.0/SQRT(ANU(IL,KK))
            END IF
         END DO ! IL
      END DO ! KK
      
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
