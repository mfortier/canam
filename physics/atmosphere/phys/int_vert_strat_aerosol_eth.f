!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INT_VERT_STRAT_AEROSOL_ETH(sw_ext_sa_gcm,    ! OUTPUT
     &                                      sw_ssa_sa_gcm, 
     &                                      sw_g_sa_gcm,
     &                                      lw_abs_sa_gcm,
     &                                      w055_vtau_sa_gcm,
     &                                      w110_vtau_sa_gcm,
     &                                      w055_ext_sa_gcm,
     &                                      w110_ext_sa_gcm,
     &                                      sw_ext_sa_in,     ! INPUT
     &                                      sw_ssa_sa_in,
     &                                      sw_g_sa_in,
     &                                      lw_ext_sa_in,
     &                                      lw_ssa_sa_in,
     &                                      w055_ext_sa_in,
     &                                      w110_ext_sa_in,
     &                                      pres_sa_in,
     &                                      shtj,
     &                                      shj,
     &                                      dz,
     &                                      trop_pres,
     &                                      pres_sfc,
     &                                      ilgz,
     &                                      il1z,
     &                                      il2z,
     &                                      ilg,
     &                                      il1,
     &                                      il2,
     &                                      nlev_gcm,
     &                                      nlay_gcm,
     &                                      nlev_sa,
     &                                      nbs,
     &                                      nbl)


C
C     * JAN 10/17 - J. COLE   
C
C     * INTERPOLATE STRATOSPHERIC AEROSOL DATA FROM ETHZ VERTICAL
C     * GRID (PRESSURES) ONTO THE CURRENT GCM VERTICAL LEVELS AND
C     * GENERATE SOME DIAGNOSTIC OUTPUT.
C
C     METHOD:
C
C     INTERPOLATE DATA FROM INPUT VERTICAL GRID ONTO GCM VERTICAL GRID.
C     THE STRATOSPHERIC AEROSOL DATASET EXTENDS RATHER LOW DOWN INTO THE ATMOSPHERE,
C     SO WE FOLLOW THE ETHZ ADVICE AND ONLY USE THE AEROSOL OPTICAL PROPERTIES ABOVE
C     THE LOCAL TROPOPAUSE.
C
C     FOR DIAGNOSTICS WE SAVE THE VERTICALLY INTEGRATED OPTICAL THICKNESS ABOVE
C     THE LOCAL TROPOPAUSE FOR 0.55 AND 11 MICRONS.
C
C     NOTE THAT THE GCM PRESSURE GOES FROM TOP TO BOTTOM WHILE ETHZ PRESSURES
C     GO FROM BOTTOM TO TOP.

      IMPLICIT NONE

! PARAMETERS

      REAL, PARAMETER ::
     & R_ZERO = 0.0,
     & R_ONE  = 1.0,
     & M2KM   = 0.001

! INPUT

      INTEGER, INTENT(IN) :: ilgz       !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: il1z       !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: il2z       !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: ilg        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: il1        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: il2        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: nlev_gcm   !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: nlay_gcm   !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: nlev_sa    !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: nbs        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: nbl        !<Variable description\f$[units]\f$

      REAL, INTENT(IN) :: sw_ext_sa_in(ilgz,nlev_sa,nbs) !<(1/km)\f$[units]\f$
      REAL, INTENT(IN) :: sw_ssa_sa_in(ilgz,nlev_sa,nbs) !<(unitless)\f$[units]\f$
      REAL, INTENT(IN) :: sw_g_sa_in(ilgz,nlev_sa,nbs)   !<(unitless)\f$[units]\f$
      REAL, INTENT(IN) :: lw_ext_sa_in(ilgz,nlev_sa,nbl) !<(1/km)\f$[units]\f$
      REAL, INTENT(IN) :: lw_ssa_sa_in(ilgz,nlev_sa,nbl) !<(unitless)\f$[units]\f$
      REAL, INTENT(IN) :: w055_ext_sa_in(ilgz,nlev_sa)   !<(1/km)\f$[units]\f$
      REAL, INTENT(IN) :: w110_ext_sa_in(ilgz,nlev_sa)   !<(1/km)\f$[units]\f$
      REAL, INTENT(IN) :: pres_sa_in(ilgz,nlev_sa)       !<(Pa)\f$[units]\f$
      REAL, INTENT(IN) :: trop_pres(ilg)                 !<(hPa)\f$[units]\f$
      REAL, INTENT(IN) :: shtj(ilg,nlev_gcm)             !<(unitless)\f$[units]\f$
      REAL, INTENT(IN) :: shj(ilg,nlay_gcm)              !<(unitless)\f$[units]\f$
      REAL, INTENT(IN) :: pres_sfc(ilg)                  !<(hPa)\f$[units]\f$
      REAL, INTENT(IN) :: dz(ilg,nlay_gcm)               !<(meters)\f$[units]\f$

! OUTPUT
      REAL, INTENT(OUT) :: sw_ext_sa_gcm(ilg,nlay_gcm,nbs) !<(1/km)\f$[units]\f$
      REAL, INTENT(OUT) :: sw_ssa_sa_gcm(ilg,nlay_gcm,nbs) !<(unitless)\f$[units]\f$
      REAL, INTENT(OUT) :: sw_g_sa_gcm(ilg,nlay_gcm,nbs)   !<(unitless)\f$[units]\f$
      REAL, INTENT(OUT) :: lw_abs_sa_gcm(ilg,nlay_gcm,nbl) !<(1/km)\f$[units]\f$
      REAL, INTENT(OUT) :: w055_vtau_sa_gcm(ilg)           !<(unitless)\f$[units]\f$
      REAL, INTENT(OUT) :: w110_vtau_sa_gcm(ilg)           !<(unitless)\f$[units]\f$
      REAL, INTENT(OUT) :: w055_ext_sa_gcm(ilg,nlay_gcm)   !<(unitless)\f$[units]\f$
      REAL, INTENT(OUT) :: w110_ext_sa_gcm(ilg,nlay_gcm)   !<(unitless)\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

! LOCAL

      INTEGER :: 
     & ilay_sa,
     & ilay_gcm,
     & ilev_gcm,
     & ilz,
     & il,
     & ib,
     & ip,
     & nlev_sa_m1

      REAL ::
     & pres_lev1,
     & pres_lev2,
     & sum_wgt,
     & w055_ext,
     & w110_ext,
     & dz_loc

      INTEGER ::
     & lay_ind_trop(ilg),
     & lp(2)

      REAL ::
     & pres_lay(ilg,nlay_gcm),
     & w(2),
     & sw_ext(nbs),
     & sw_ssa(nbs),
     & sw_g(nbs),
     & lw_abs(nbl)

      nlev_sa_m1 = nlev_sa - 1

!
! Zero out the output, values are only added if above the tropopause.
!

      sw_ext_sa_gcm(:,:,:) = R_ZERO
      sw_ssa_sa_gcm(:,:,:) = R_ZERO
      sw_g_sa_gcm(:,:,:)   = R_ZERO
      lw_abs_sa_gcm(:,:,:) = R_ZERO
      w055_vtau_sa_gcm(:)  = R_ZERO
      w110_vtau_sa_gcm(:)  = R_ZERO
      w055_ext_sa_gcm(:,:) = R_ZERO
      w110_ext_sa_gcm(:,:) = R_ZERO

!
! Compute the pressure of the GCM layers.
!
      
      DO ilay_gcm = 1, nlay_gcm
         DO il = il1, il2
            pres_lay(il,ilay_gcm) = pres_sfc(il)*shj(il,ilay_gcm)
         END DO ! il
      END DO ! ilay_gcm

!
! Find the local model layer that contains the tropopause.
!      

      DO ilay_gcm = 1, nlay_gcm
         DO il = il1, il2
            pres_lev1 = pres_sfc(il)*shtj(il,ilay_gcm)
            pres_lev2 = pres_sfc(il)*shtj(il,ilay_gcm+1)
            
            IF (trop_pres(il) .GT. pres_lev1 .AND.
     &          trop_pres(il) .LE. pres_lev2) THEN ! Tropopause is somewhere in this layer
               lay_ind_trop(il) = ilay_gcm
            END IF

         END DO ! il
      END DO ! ilay_gcm
      
!
! Loop over the model levels and interpolate the stratospheric aerosols
! from the ETHZ levels to the GCM levels.  This is only done for levels
! that are above the tropopause.
!
      
      ilz = 1

      DO ilay_gcm = 1, nlay_gcm
         DO il = il1, il2
            IF (ilay_gcm .LT. lay_ind_trop(il)) THEN
               pres_lev1 =  pres_lay(il,ilay_gcm)

               sw_ext(:) = R_ZERO
               sw_ssa(:) = R_ZERO
               sw_g(:)   = R_ZERO
               lw_abs(:) = R_ZERO
               w055_ext  = R_ZERO
               w110_ext  = R_ZERO
               sum_wgt   = R_ONE

               DO ilay_sa = 1, nlev_sa_m1

                  IF (pres_sa_in(ilz,ilay_sa)   .GE. pres_lev1 .AND.
     &                pres_sa_in(ilz,ilay_sa+1) .LT. pres_lev1) THEN

                     w(1)    = pres_lev1 - pres_sa_in(ilz,ilay_sa+1)
                     w(2)    = pres_sa_in(ilz,ilay_sa) - pres_lev1
                     lp(1)   = ilay_sa+1
                     lp(2)   = ilay_sa
                     sum_wgt = SUM(w)

                     DO ip = 1, 2
                        sw_ext(:) = sw_ext(:)
     &                            + sw_ext_sa_in(ilz,lp(ip),:)*w(ip)
                        
                        sw_ssa(:) = sw_ssa(:)
     &                            + sw_ssa_sa_in(ilz,lp(ip),:)
     &                             *sw_ext_sa_in(ilz,lp(ip),:)*w(ip)

                        sw_g(:)   = sw_g(:)
     &                            + sw_ssa_sa_in(ilz,lp(ip),:)
     &                             *sw_ext_sa_in(ilz,lp(ip),:)
     &                             *sw_g_sa_in(ilz,lp(ip),:)*w(ip)

                        lw_abs(:) = lw_abs(:)
     &                            + lw_ext_sa_in(ilz,lp(ip),:)
     &                             *(R_ONE-lw_ssa_sa_in(ilz,lp(ip),:))
     &                            *w(ip)
                        
                        w055_ext  = w055_ext
     &                            + w055_ext_sa_in(ilz,lp(ip))*w(ip)

                        w110_ext  = w110_ext
     &                            + w110_ext_sa_in(ilz,lp(ip))*w(ip)

                     END DO ! ip
                 END IF ! pres_sa_in
               END DO ! ilay_sa

               IF (SUM(sw_ext) > R_ZERO) THEN
                  sw_g_sa_gcm(il,ilay_gcm,:)   = sw_g(:)/sw_ssa(:)
                  sw_ssa_sa_gcm(il,ilay_gcm,:) = sw_ssa(:)/sw_ext(:)
                  sw_ext_sa_gcm(il,ilay_gcm,:) = sw_ext(:)/sum_wgt
               ELSE
                  sw_g_sa_gcm(il,ilay_gcm,:)   = R_ZERO
                  sw_ssa_sa_gcm(il,ilay_gcm,:) = R_ZERO
                  sw_ext_sa_gcm(il,ilay_gcm,:) = R_ZERO
               END IF
               
               IF (SUM(lw_abs) > R_ZERO) THEN
                  lw_abs_sa_gcm(il,ilay_gcm,:) = lw_abs(:)/sum_wgt
               ELSE
                  lw_abs_sa_gcm(il,ilay_gcm,:) = R_ZERO
               END IF
               
               IF (w055_ext > R_ZERO) THEN
                  w055_ext_sa_gcm(il,ilay_gcm) = w055_ext/sum_wgt
               ELSE
                  w055_ext_sa_gcm(il,ilay_gcm) = R_ZERO
               END IF

               IF (w110_ext > R_ZERO) THEN
                  w110_ext_sa_gcm(il,ilay_gcm) = w110_ext/sum_wgt
               ELSE
                  w110_ext_sa_gcm(il,ilay_gcm) = R_ZERO
               END IF

!
! Diagnose the vertically integerate optical thickness above the local
! tropopause.
!
               dz_loc = dz(il,ilay_gcm)*M2KM ! Layer thickness in km

               w055_vtau_sa_gcm(il) = w055_vtau_sa_gcm(il)
     &                              + (w055_ext/sum_wgt)*dz_loc

               w110_vtau_sa_gcm(il) = w110_vtau_sa_gcm(il)
     &                              + (w110_ext/sum_wgt)*dz_loc
               
            END IF ! trop
         END DO ! il
      END DO ! ilay_gcm


      RETURN

      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
