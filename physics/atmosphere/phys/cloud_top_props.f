!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CLOUD_TOP_PROPS(RELIQ,REICE,CLDLIQ,                ! OUTPUT
     1                           CLDICE,CLDTOPLNC,
     2                           COL_LNC,
     2                           CLW_SUB, CIC_SUB, REL_SUB,         ! INPUT
     3                           REI_SUB, CDD, DZ,
     4                           CLDWATMIN,ILG,IL1,IL2,ILEV,NXLOC)

! June 02, 2013 - Jason Cole.
! This subroutine computes the cloud top effective radius for liquid and ice.
! This subroutine only works when the stochastic cloud generator is turned on.
! The code is very simple and works subcolumn-by-subcolumn
!  - First it finds the uppermost cloud by going downward from TOA until
!    encountering a layer with ice or liquid
!  - A decision needs to be made whether the layer is ice or liquid (it can 
!    have both present) the one with the higher concentration is used to 
!    select the dominant cloud phase.
!  - Once this this decided we can accumulate things to compute the
!    mean cloud radius and cloud fraction for liquid and ice clouds.
!  - We also use this to compute the particle number concentrations.  They
!    request it to be computed in two manners.  One is to use the concentration
!    in the uppermost cloud and the second is to provide the vertical.

      IMPLICIT NONE

!
! INPUT DATA
!

      REAL, INTENT(IN), DIMENSION(ILG,ILEV,NXLOC) :: CLW_SUB    !<Variable description\f$[units]\f$
      REAL, INTENT(IN), DIMENSION(ILG,ILEV,NXLOC) :: CIC_SUB    !<Variable description\f$[units]\f$
      REAL, INTENT(IN), DIMENSION(ILG,ILEV,NXLOC) :: REL_SUB    !<Variable description\f$[units]\f$
      REAL, INTENT(IN), DIMENSION(ILG,ILEV,NXLOC) :: REI_SUB    !<Variable description\f$[units]\f$
        
      REAL, INTENT(IN), DIMENSION(ILG,ILEV)       :: CDD,DZ     !<Variable description\f$[units]\f$
      
      REAL, INTENT(IN)                            :: CLDWATMIN  !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN)                         :: ILG        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN)                         :: IL1        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN)                         :: IL2        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN)                         :: ILEV       !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN)                         :: NXLOC      !<Variable description\f$[units]\f$

!
! OUTPUT DATA
!

      REAL, INTENT(OUT), DIMENSION(ILG) :: RELIQ        !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG) :: REICE        !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG) :: CLDLIQ       !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG) :: CLDICE       !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG) :: CLDTOPLNC    !<Variable description\f$[units]\f$
      REAL, INTENT(OUT), DIMENSION(ILG) :: COL_LNC      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================


!
! LOCAL DATA
!
      
      INTEGER                       :: IL,L,ICOL
      REAL                          :: CWT,R_NXLOC
      REAL, DIMENSION(ILG,NXLOC)    :: NCONC_LIQ
      LOGICAL, DIMENSION(ILG,NXLOC) :: L_CLD_TOP
      INTEGER, DIMENSION(ILG,NXLOC) :: I_CLD_TOP,
     1                                 PHASE_CLD_TOP ! 1 = LIQUID, 2 = ICE

!
! UNITS
!

      R_NXLOC = 1.0/REAL(NXLOC)

! INITIALIZE FIELDS
      
      DO ICOL = 1, NXLOC
         DO IL = IL1, IL2
            L_CLD_TOP(IL,ICOL)     = .FALSE.
            I_CLD_TOP(IL,ICOL)     = -999
            PHASE_CLD_TOP(IL,ICOL) = -999
	    NCONC_LIQ(IL,ICOL)     = 0.0
         END DO ! IL
      END DO ! ICOL

      DO IL = IL1, IL2
         RELIQ(IL)     = 0.0
         REICE(IL)     = 0.0
         CLDLIQ(IL)    = 0.0
         CLDICE(IL)    = 0.0
	 CLDTOPLNC(IL) = 0.0
	 COL_LNC(IL)   = 0.0
      END DO ! IL

! WORK OUR WAY DOWN FROM MODEL TOP AND LOOK FOR THE UPPERMOST CLOUD AND
! ASSIGN A CLOUD PHASE BASED ON CLOUD WATER CONTENT.
! WHILE WORKING THROUGH THE LOOP COMPUTE THE VERTICAL INTEGRAL OF CLOUD 
! PARTICLE NUMBER CONCENTRATION.

      DO ICOL = 1, NXLOC
         DO L = 1,ILEV
            DO IL = IL1, IL2
               CWT = CLW_SUB(IL,L,ICOL)+CIC_SUB(IL,L,ICOL)
               IF (.NOT. L_CLD_TOP(IL,ICOL)) THEN ! No cloud top found
                  IF (CWT .GT. CLDWATMIN) THEN
                     L_CLD_TOP(IL,ICOL) = .TRUE.
                     I_CLD_TOP(IL,ICOL) = L

                     IF ((CIC_SUB(IL,L,ICOL) .LE. 0.0) .OR. 
     1               (CLW_SUB(IL,L,ICOL) .GE. CIC_SUB(IL,L,ICOL))) THEN ! LIQUID
                        PHASE_CLD_TOP(IL,ICOL) = 1
                     ELSEIF ((CLW_SUB(IL,L,ICOL) .LE. 0.0) .OR. 
     1               (CIC_SUB(IL,L,ICOL) .GT. CLW_SUB(IL,L,ICOL))) THEN ! ICE
                        PHASE_CLD_TOP(IL,ICOL) = 2
                     END IF

                  END IF
               END IF

! COMPUTE VERTICAL INTEGRAL
               IF (CWT .GT. CLDWATMIN) THEN
                  IF ((CIC_SUB(IL,L,ICOL) .LE. 0.0) .OR. 
     1                (CLW_SUB(IL,L,ICOL) .GE. CIC_SUB(IL,L,ICOL))) THEN ! LIQUID
                     NCONC_LIQ(IL,ICOL) = NCONC_LIQ(IL,ICOL)
     1                                  + CDD(IL,L)*DZ(IL,L)
                  END IF
               END IF	       
            END DO ! IL
         END DO ! L
      END DO ! ICOL
                     
! NOW LOOP OVER COLUMNS AND ACCUMULATE CLOUD PROPERTIES

      DO ICOL = 1, NXLOC
         DO IL = IL1, IL2
            IF (L_CLD_TOP(IL,ICOL)) THEN
               L = I_CLD_TOP(IL,ICOL)
               IF (PHASE_CLD_TOP(IL,ICOL) .EQ. 1) THEN ! LIQUID
                  RELIQ(IL)     = RELIQ(IL) + REL_SUB(IL,L,ICOL)
		  CLDTOPLNC(IL) = CLDTOPLNC(IL) + CDD(IL,L)
                  COL_LNC(IL)   = COL_LNC(IL) + NCONC_LIQ(IL,ICOL)
                  CLDLIQ(IL) = CLDLIQ(IL) + 1.0
               END IF
               IF (PHASE_CLD_TOP(IL,ICOL) .EQ. 2) THEN ! ICE
                  REICE(IL)    = REICE(IL) + REI_SUB(IL,L,ICOL)
                  CLDICE(IL)   = CLDICE(IL) + 1.0
               END IF
            END IF
         END DO ! IL
      END DO ! ICOL

! COMPUTE THE APPROPRIATE MEANS (RADIUS IS CLOUD-MEAN, CLOUD-FRACTION IS GRID-MEAN)

      DO IL = IL1, IL2
         IF (CLDLIQ(IL) .GT. 0.0) THEN
            RELIQ(IL)     = RELIQ(IL)/CLDLIQ(IL)
	    CLDTOPLNC(IL) = CLDTOPLNC(IL)/CLDLIQ(IL)
            COL_LNC(IL)   = COL_LNC(IL)/CLDLIQ(IL)
            CLDLIQ(IL)    = CLDLIQ(IL)*R_NXLOC
         ELSE
            RELIQ(IL)     = 0.0
	    CLDTOPLNC(IL) = 0.0
            COL_LNC(IL)   = 0.0
            CLDLIQ(IL)    = 0.0
         END IF
         IF (CLDICE(IL) .GT. 0.0) THEN
            REICE(IL)     = REICE(IL)/CLDICE(IL)
            CLDICE(IL)    = CLDICE(IL)*R_NXLOC
         ELSE
            REICE(IL)     = 0.0
            CLDICE(IL)    = 0.0
         END IF
      END DO ! IL
      
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
