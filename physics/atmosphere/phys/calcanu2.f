!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE CALCANU2(ANU,                                  ! OUTPUT
     1                    CLD, QCWVAR, IL1, IL2, ILG, LAY, LEV, ! INPUT
     2                    MCICA)                                ! INPUT

!     * FEB 13/2008 - JASON COLE. NEW VERSION FOR GCM15H:
!     *                           NO CLOUD BLOCK MINIMUM ANU IF MCICA.
!     * DEC 05/2007 - JASON COLE. PREVIOUS VERSION CALCANU FOR GCM15G:
!     *                           CALCULATES CLOUD INHOMOGENEITY
!     *                           FACTOR "ANU".
!     *

      IMPLICIT NONE
!
!     * OUTPUT DATA
!
      REAL, DIMENSION(ILG,LAY), INTENT(OUT) ::ANU(ILG,LAY) !<Variable description\f$[units]\f$
!
!     * INPUT DATA
!
      REAL, DIMENSION(ILG,LAY), INTENT(IN) :: CLD    !<Cloud amount\f$[units]\f$
      REAL, DIMENSION(ILG,LAY), INTENT(IN) :: QCWVAR  !<Variance of total cloud water\f$[units]\f$

      INTEGER, INTENT(IN) :: IL1        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: IL2        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: ILG        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LAY        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: LEV        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: MCICA      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
!
! LOCAL DATA
!
      INTEGER :: ! Loop counters
     1 IL,
     2 K,
     3 KM1,
     4 KP1,
     5 LAYM1
!
! PARAMETERS/DATA
!
      REAL, PARAMETER ::
     1 CUT    = 0.001 ! MUST BE CONSISTENT WITH VALUE IN RADIATION
!--------------------------------------------------------------------
!     * COMPUTE THE LAYER BY LAYER VALUES OF ANU
!
      DO K = 1, LAY
         DO IL = IL1, IL2
            IF (CLD(IL,K) .GE. CUT )                                THEN
               ANU(IL,K)=1.0/MIN(MAX(0.25,QCWVAR(IL,K)),2.0)
            ELSE
               ANU(IL,K) = 1000.0
            ENDIF            
         END DO ! IL
      END DO ! K

      IF (MCICA .EQ. 0) THEN      
!===================================================================
!       * FOLLOWING NEEDED FOR THE 1D RADIATION CODE OF LI (NON_MCICA!)
!       * MINIMUM ANU, IT IS EXTREMELY IMPORTANT TO ENSURE CONSISTENCY
!       * BETWEEN THE DEFINITIONS OF ANU HERE AND THEIR SUBSEQUENT USE IN
!       * LWTRAN4
!===================================================================

!       * COMPUTE THE MINIMUM VALUE OF ANU FOR A CLOUD BLOCK
!       * GOING FROM TOP TO BOTTOM

        DO K = 1, LAY
          KM1 = K - 1
          DO IL = IL1, IL2
            IF (CLD(IL,K) .LT. CUT)                                 THEN
               ANU(IL,K) =  1000.0
            ELSE
               IF (K .GT. 1)                                        THEN 
                  ANU(IL,K)  =  MIN (ANU(IL,KM1), ANU(IL,K))
               ENDIF
            END IF
          END DO ! IL
        END DO ! K
      
!       * COMPUTE THE MINIMUM VALUE OF ANU FOR A CLOUD BLOCK
!       * GOING FROM BOTTOM TO TOP

        LAYM1=LAY-1
        DO K = LAYM1, 1, -1
          KP1 = K + 1
          DO IL = IL1, IL2
            IF (CLD(IL,K) .GE. CUT)                                 THEN
               ANU(IL,K)  =  MIN (ANU(IL,KP1), ANU (IL,K))
            ENDIF
          END DO ! IL
        END DO ! K
      END IF ! MCICA 

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
