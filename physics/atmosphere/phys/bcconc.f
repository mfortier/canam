!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE BCCONC(ZSNOW,GCROW,BCSNO,DEPB,SPCP,ROFN,RHOSNO,
     1                  DELT,ZSNMIN,ZSNMAX,ILG,IL1,IL2,GCMIN,GCMAX)

C     * Feb 10/2015 - J.Cole.   New version for gcm18:
C                               - Truncate BC concentrations to
C                                 avoid excessively large values
C                                 that may occur under special
C                                 circumstances given the simplicity 
C                                 of the current bc/snow parameterization.
C                                 This will ensure that subsequent 
C                                 albedo calculations will be
C                                 numerically robust. 
C     * Nov 15/2013 - M.Lazare. Previous version for gcm17.
C     *                         - no SNOW (wasn't being used).
C     *                         - two IF branches combined into one
C     *                           for ZSNOW>ZSNMIN.
C     *                         - K decreased from 0.5 to 0.1 (tuning).
C     *                    
C     * Aug 06/2013 - M. Namazi &  K. V. SALZEN.
C     *              BC CONCENTRATION IN SNOW DUE TO DEPOSITION
C     *              OF BC AND SCAVENGING THROUGH MELTING.

      IMPLICIT NONE                                                             

      INTEGER ILG       !<Variable description\f$[units]\f$
      INTEGER IL1       !<Variable description\f$[units]\f$
      INTEGER IL2       !<Variable description\f$[units]\f$
      INTEGER I         !<Variable description\f$[units]\f$
      INTEGER J         !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: BCSNO     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: GCROW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: DEPB      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: SPCP      !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: RHOSNO    !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: ZSNOW     !<Variable description\f$[units]\f$
      REAL, DIMENSION(ILG) :: ROFN      !<Variable description\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      REAL  DELT,COEFF1,COEFF2,ZSNMIN,ZSNMAX,GCMIN,GCMAX,ZSN
C
      REAL, PARAMETER :: K=1.E-1, EPS1=1.E-5, DELM=1. 
C--------------------------------------------------------------------
      DO 100 I=IL1,IL2
        IF(GCROW(I).GT.GCMIN.AND.GCROW(I).LT.GCMAX)            THEN 
           IF(ZSNOW(I).GT.ZSNMIN)                            THEN
              ZSN=MIN(ZSNOW(I),ZSNMAX)
              COEFF1=ZSN+DELT*SPCP(I)/RHOSNO(I)
              BCSNO(I)=BCSNO(I)*ZSN+DELT*DEPB(I)
              BCSNO(I)=BCSNO(I)/COEFF1
              COEFF2=RHOSNO(I)*ZSN-(1-K)*DELT*DELM*ROFN(I)
              BCSNO(I)=RHOSNO(I)*ZSN*BCSNO(I)/COEFF2
C
C             * TRUNCATE BC CONCENTRATIONS TO AVOID EXCESSIVELY LARGE VALUES
C             * THAT MAY OCCUR UNDER SPECIAL CIRCUMSTANCES GIVEN THE SIMPLICITY 
C             * OF THE CURRENT BC/SNOW PARAMETERIZATION.
C             * THIS WILL ENSURE THAT SUBSEQUENT ALBEDO CALCULATIONS WILL BE
C             * NUMERICALLY ROBUST. 
C
              BCSNO(I)=MIN(BCSNO(I),1.)
           ELSE
              BCSNO(I)=0.
           ENDIF
        ENDIF
  100 CONTINUE

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
