!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE MSLP(PMSL,TL,PRESSG,PHIS,SHJL,ILG,IL1,IL2)
c
c     * JULY 27, 2005 - M.LAZARE.
C
C     * COMPUTES M.S.L. PRESSURE (IN MBS).
C     * THE CODE IS PULLED FROM THE DIAGNOSTIC CALCULATION TO MAINTAIN
C     * CONSISTENCY (PROGRAM GSMSLPH CALLED BY DECK MSLPR_ETA).
C
C     * WE ASSUME A UNIFORM LAPSE RATE OF (DT/DZ)=-GAM, FROM REFERENCE
C     * LEVEL (LOWEST LAYER) TO THE SURFACE, AND THEN FROM THE SURFACE TO SEA LEVEL.
C     * THE HYDROSTATIC EQUATION AND CONSTANT-LAPSE RATE EQUATION ARE
C     * USED FIRST TO DETERMINE "SURFACE TEMPERATURE".
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      REAL PMSL(ILG)    !<Variable description\f$[units]\f$
      REAL TL(ILG)      !<Variable description\f$[units]\f$
      REAL PRESSG(ILG)  !<Variable description\f$[units]\f$
      REAL PHIS(ILG)    !<Variable description\f$[units]\f$
      REAL SHJL(ILG)    !<Variable description\f$[units]\f$\
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================
C
      COMMON /PARAMS/ WW,TW,RAYON,ASQ,GRAV,RGAS,RGOCP,RGOASQ,CPRES
      COMMON /PARAMS/ RGASV,CPRESV
C  
      DATA GAM/0.0065/
C==========================================================================
      GAMRGOG = GAM*RGAS/GRAV
      FACT    = 0.5*GAM /GRAV
      DO 200 IL=IL1,IL2
        TS       = TL(IL) / SHJL(IL)**GAMRGOG
        TBAR     = TS + FACT*PHIS(IL)
        PMSL(IL) = 0.01 * PRESSG(IL) * EXP(PHIS(IL)/(RGAS*TBAR))
  200 CONTINUE
C
      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
