!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
      SUBROUTINE INITSPECSAMPL2(NSAMPLE_SW,NSAMPLE_LW,NBS,NBL,
     1                          MAXNG, IVERS, IRADFORCE, MAX_SAM)
C
C APR 30/2012  - J.COLE. NEW VERSION FOR GCM16:
C                        ADD EXTRA CHECK, WHICH REQUIRES PASSING
C                        IN "IRADFORCE" AND "MAX_SAM".
C JUNE 5, 2006 - J.COLE. PREVIOUS VERSION INITSPECSAMPL.
C
C UPDATE SUBROUTINE SO THAT THE VARIABLE VERS DEFINES THE LEVEL OF NOISE 
C TO USE: 1 -> HIGH    (CLDS)
C         2 -> MEDIUM  (SPEC)
C         3 -> LOW     (REF)
C
C THE COMPUTATIONAL TIME INCREASES DRAMATICALLY GOING FROM 1 TO 3.  VERSION
C 1 SHOULD (HOPEFULLY) BE NEARLY AS FAST AS THE CURRENT CODE WHILE 3 SHOULD 
C BE JUST ABLE TO COMPLETE ONE MONTH IN THE WALLCLOCK TIME LIMIT.
C
C APR. 14, 2005 - J. COLE
C
C THIS CODE GENERATES THE SPECTRAL SAMPLING FOR EACH IG CORRELATED
C K-DISTRIBUTION INTEGRATION POINT WITHIN EACH IB WAVELENGTH INTERVAL.
C THE SAMPLING WAS DETERMINED BY TESTS BY PETRI RAISANEN.
C
C FOR EACH INTERVAL IB THE SPECTRAL SAMPLING IS GENERATED FIRST FOR
C THE KGS POINTS AND THEN THE KGSGH POINTS.  I.E., FOR THE FIRST BAND
C THERE ARE 7 VALUES, 6 FOR KGS AND 1 FOR KGSGH.  SINCE FOR THE KGSGH
C POINTS CLOUDS ARE IGNORED DUE TO LARGE GAS OPTICAL THICKNESSES, THE
C NUMBER OF SPECTRAL SAMPLES IS ALWAYS 1.
C
C THERE IS MORE ADVANCED SAMPLING TO BE DONE BASED ON LOCATION, I.E.,
C OVER LAND, OVER OCEAN, OVER SNOW, MINIMIZED ERROR IN ATM. HEATING,
C SURFACE FORCING, ETC..

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: NBS        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: NBL        !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: MAXNG      !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: IVERS      !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: IRADFORCE  !<Variable description\f$[units]\f$
      INTEGER, INTENT(IN) :: MAX_SAM    !<Variable description\f$[units]\f$

      INTEGER, INTENT(OUT) :: NSAMPLE_SW(NBS,MAXNG,2)!<NSAMPLE_*(:,:,1) is for GH=.FALSE. and NSAMPLE_*(:,:,2) is for GH = .TRUE.\f$[units]\f$
      INTEGER, INTENT(OUT) :: NSAMPLE_LW(NBL,MAXNG,2)!<NSAMPLE_*(:,:,1) is for GH=.FALSE. and NSAMPLE_*(:,:,2) is for GH = .TRUE.\f$[units]\f$
C==================================================================
C PHYSICAL (ADJUSTABLE) PARAMETERS
C
C Define and document here any adjustable parameters.
C This should be variable described using the Doxygen format above as
C well as a description of its minimum/default/maximum.
C
C Here is an example,
C
C REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
C           !! It is compute differently when using bulk or PAM aerosols.
C           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
C==================================================================

      INTEGER ::
     1 IB,
     2 IG,
     3 IGH

C SET ALL TO 1 SPECTRAL SAMPLE INITIALLY
      DO IGH = 1, 2
         DO IG = 1, MAXNG
            DO IB = 1, NBS
               NSAMPLE_SW(IB,IG,IGH) = 1
            END DO
            DO IB = 1, NBL
               NSAMPLE_LW(IB,IG,IGH) = 1
            END DO ! IB
         END DO ! IG
      END DO  ! IGH

      IF (IVERS .EQ. 1) THEN
! DO NOTHING SINCE WE WILL USE 1 SAMPLE 

      ELSEIF (IVERS .EQ. 2) THEN

C SET THE SPECTRAL SAMPLES SO THAT A TOTAL OF 45 CALCULATIONS
C ARE MADE IN THE SHORTWAVE THAT MINIMIZE ERROR FOR LAND AND ATM.
         
         NSAMPLE_SW(1,1,1) = 4
         NSAMPLE_SW(1,2,1) = 2
         NSAMPLE_SW(2,1,1) = 4
         NSAMPLE_SW(3,1,1) = 4

C SET THE SPECTRAL SAMPLES SO THAT A TOTAL OF 71 CALCULATIONS
C ARE MADE IN THE LONGWAVE THAT MINIMIZE ERROR FOR THE ATM.
         
         
         NSAMPLE_LW(4,1,1) = 3
         NSAMPLE_LW(4,2,1) = 2
         NSAMPLE_LW(5,1,1) = 3
         NSAMPLE_LW(6,1,1) = 11
         NSAMPLE_LW(7,1,1) = 3
         NSAMPLE_LW(7,2,1) = 3
         NSAMPLE_LW(7,3,1) = 2
         NSAMPLE_LW(8,2,1) = 2
         NSAMPLE_LW(8,3,1) = 2
         NSAMPLE_LW(8,4,1) = 2
         NSAMPLE_LW(9,1,1) = 2
         
      ELSEIF (IVERS. EQ. 3) THEN

C SET THE SPECTRAL SAMPLES SO THAT A TOTAL OF ~700 CALCULATIONS
C ARE MADE IN THE SHORTWAVE THAT MINIMIZE ERROR FOR LAND AND ATM.

                         ! ~450  ! ~700
         NSAMPLE_SW(1,1,1) = 106   !171
         NSAMPLE_SW(1,2,1) = 63    !101
         NSAMPLE_SW(1,3,1) = 20    !32
         NSAMPLE_SW(1,4,1) = 2     !2
         NSAMPLE_SW(2,1,1) = 113   !183
         NSAMPLE_SW(2,2,1) = 15    !24
         NSAMPLE_SW(2,3,1) = 4     !6
         NSAMPLE_SW(3,1,1) = 91    !147
         NSAMPLE_SW(3,2,1) = 11    !18
         NSAMPLE_SW(4,1,1) = 14    !22
         NSAMPLE_SW(4,2,1) = 3     !5
         
C SET THE SPECTRAL SAMPLES SO THAT A TOTAL OF ~940 CALCULATIONS
C ARE MADE IN THE LONGWAVE THAT MINIMIZE ERROR FOR THE ATM.

                         !~500 !~940
         NSAMPLE_LW(1,1,1) = 3   !3
         NSAMPLE_LW(2,1,1) = 5   !7
         NSAMPLE_LW(4,1,1) = 96  !181
         NSAMPLE_LW(4,2,1) = 5   !7
         NSAMPLE_LW(5,1,1) = 85  !167
         NSAMPLE_LW(5,2,1) = 26  !348
         NSAMPLE_LW(6,1,1) = 220 !449
         NSAMPLE_LW(7,1,1) = 41  !82
         NSAMPLE_LW(7,2,1) = 2   !3
         NSAMPLE_LW(7,3,1) = 2   !2

      ELSE
         WRITE(*,*) 'UNKNOWN VALUE FOR IVERS ',IVERS
         WRITE(*,*) 'MUST USE 1, 2, OR 3!!'
      ENDIF
C
C     * EXTRA CHECKS.
C
      IF (IRADFORCE .NE. 0) THEN
       IF(SUM(NSAMPLE_LW) .GT. MAX_SAM .OR.
     1    SUM(NSAMPLE_LW) .GT. MAX_SAM) THEN
         CALL XIT('INITSPECSAMPL2/RADDRIV8',-999)
       END IF
      END IF

      RETURN
      END
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.  
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}      
